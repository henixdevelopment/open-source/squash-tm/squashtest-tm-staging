/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.domain.EntityType.CAMPAIGN_FOLDER;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_CAMPAIGN_LIBRARY_OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_CAMPAIGN_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_CAMPFOLDER_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Provider;
import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.campaign.export.CampaignExportCSVModel;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.library.NewFolderDto;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.exception.campaign.IllegalSprintGroupHierarchyException;
import org.squashtest.tm.exception.library.NameAlreadyExistsAtDestinationException;
import org.squashtest.tm.exception.sync.PathContainsASprintGroupException;
import org.squashtest.tm.exception.sync.PathValidationDomainException;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.CheckBlockingMilestones;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.annotation.PreventConcurrents;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.campaign.CampaignStatisticsService;
import org.squashtest.tm.service.campaign.CustomCampaignModificationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.campaign.IterationStatisticsService;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.copier.StrategyCopierService;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.deletion.CampaignLibraryNodesToDelete;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.campaign.coercers.CLNAndParentIdsCoercerForArray;
import org.squashtest.tm.service.internal.campaign.coercers.CLNAndParentIdsCoercerForList;
import org.squashtest.tm.service.internal.campaign.coercers.CampaignLibraryIdsCoercerForArray;
import org.squashtest.tm.service.internal.campaign.coercers.CampaignLibraryIdsCoercerForList;
import org.squashtest.tm.service.internal.campaign.coercers.IterationToCampaignIdsCoercer;
import org.squashtest.tm.service.internal.campaign.coercers.TestSuiteToIterationCoercerForList;
import org.squashtest.tm.service.internal.campaign.export.CampaignExportCSVFullModelImpl;
import org.squashtest.tm.service.internal.campaign.export.CampaignExportCSVModelImpl;
import org.squashtest.tm.service.internal.campaign.export.SimpleCampaignExportCSVModelImpl;
import org.squashtest.tm.service.internal.campaign.export.WritableCampaignCSVModel;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.library.AbstractLibraryNavigationService;
import org.squashtest.tm.service.internal.library.NodeDeletionHandler;
import org.squashtest.tm.service.internal.library.PasteStrategy;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.CampaignFolderDao;
import org.squashtest.tm.service.internal.repository.CampaignLibraryDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.SprintDao;
import org.squashtest.tm.service.internal.repository.display.CampaignDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintGroupDisplayDao;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateSprintGroupDao;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.milestone.MilestoneMembershipManager;
import org.squashtest.tm.service.remotesynchronisation.RemoteSynchronisationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;

@Service("squashtest.tm.service.CampaignLibraryNavigationService")
@Transactional
public class CampaignLibraryNavigationServiceImpl
        extends AbstractLibraryNavigationService<CampaignLibrary, CampaignFolder, CampaignLibraryNode>
        implements CampaignLibraryNavigationService {
    public static final String SPRINT_SYNCHRONISATION_PATH = "sprintSynchronisationPath";
    private static final String DESTINATION_ID = "destinationId";
    private static final String TARGET_ID = "targetId";

    @Inject private CampaignLibraryDao campaignLibraryDao;

    @Inject private CampaignFolderDao campaignFolderDao;

    @Inject
    @Qualifier("squashtest.tm.repository.CampaignLibraryNodeDao")
    private LibraryNodeDao<CampaignLibraryNode> campaignLibraryNodeDao;

    @Inject private ProjectDao projectDao;

    @Inject private CampaignDao campaignDao;

    @Inject private CampaignDisplayDao campaignDisplayDao;

    @Inject private SprintDao sprintDao;

    @Inject private HibernateSprintGroupDao sprintGroupDao;

    @Inject private SprintGroupDisplayDao sprintGroupDisplayDao;

    @Inject private IterationDao iterationDao;

    @Inject private IterationModificationService iterationModificationService;

    @Inject private CampaignNodeDeletionHandler deletionHandler;

    @Inject private Provider<SimpleCampaignExportCSVModelImpl> simpleCampaignExportCSVModelProvider;

    @Inject private Provider<CampaignExportCSVModelImpl> standardCampaignExportCSVModelProvider;

    @Inject private Provider<CampaignExportCSVFullModelImpl> fullCampaignExportCSVModelProvider;

    @Inject private CampaignStatisticsService campaignStatisticsService;

    @Inject private IterationStatisticsService iterationStatisticsService;

    @Inject private StrategyCopierService strategyCopierService;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToCampaignFolderStrategy")
    private Provider<PasteStrategy<CampaignFolder, CampaignLibraryNode>>
            pasteToCampaignFolderStrategyProvider;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToSprintGroupStrategy")
    private Provider<PasteStrategy<SprintGroup, CampaignLibraryNode>>
            pasteToSprintGroupStrategyProvider;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToCampaignLibraryStrategy")
    private Provider<PasteStrategy<CampaignLibrary, CampaignLibraryNode>>
            pasteToCampaignLibraryStrategyProvider;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToCampaignStrategy")
    private Provider<PasteStrategy<Campaign, Iteration>> pasteToCampaignStrategyProvider;

    @Inject private MilestoneMembershipManager milestoneManager;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private CustomFieldBindingFinderService customFieldBindingFinderService;

    @Inject private PrivateCustomFieldValueService customFieldValueService;

    @Inject private RemoteSynchronisationService remoteSynchronisationService;

    @Inject private CustomCampaignModificationService customCampaignModificationService;

    @Override
    protected NodeDeletionHandler<CampaignLibraryNode, CampaignFolder> getDeletionHandler() {
        return deletionHandler;
    }

    @Override
    protected PasteStrategy<CampaignFolder, CampaignLibraryNode> getPasteToFolderStrategy() {
        return pasteToCampaignFolderStrategyProvider.get();
    }

    @Override
    protected PasteStrategy<CampaignLibrary, CampaignLibraryNode> getPasteToLibraryStrategy() {
        return pasteToCampaignLibraryStrategyProvider.get();
    }

    @Override
    @PreAuthorize(
            "(hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'CREATE')) "
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public void copyIterationsToCampaign(
            @Id long campaignId, Long[] iterationsIds, ClipboardPayload clipboardPayload) {
        NodeType nodePaste = strategyCopierService.verifyPermissionAndGetNodePaste(clipboardPayload);
        strategyCopierService.copyNodeToCampaign(campaignId, clipboardPayload, nodePaste);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.campaign.Campaign', 'WRITE') "
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class, paramName = DESTINATION_ID)
    public void moveIterationsWithinCampaign(
            @Id(DESTINATION_ID) long destinationId, Long[] nodeIds, int position) {
        /*
         * because : 1 - iteration is not a campaign library node 2 - an
         * iteration will move only within the same campaign,
         *
         * we can't use the TreeNodeMover and we don't need it anyway.
         */

        List<Long> iterationIds = Arrays.asList(nodeIds);

        Campaign c = campaignDao.findById(destinationId);
        List<Iteration> iterations = iterationDao.findAllByIds(iterationIds);

        c.moveIterations(position, iterations);
    }

    @Override
    protected CampaignLibraryDao getLibraryDao() {
        return campaignLibraryDao;
    }

    @Override
    protected CampaignFolderDao getFolderDao() {
        return campaignFolderDao;
    }

    @Override
    protected LibraryNodeDao<CampaignLibraryNode> getLibraryNodeDao() {
        return campaignLibraryNodeDao;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.campaign.CampaignLibrary', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public Campaign addCampaignToCampaignLibrary(
            @Id long libraryId, Campaign campaign, Map<Long, RawValue> customFieldValues) {
        return addCampaignToCampaignLibraryUnsecured(libraryId, campaign, customFieldValues);
    }

    @Override
    public Campaign addCampaignToCampaignLibraryUnsecured(
            long libraryId, Campaign campaign, Map<Long, RawValue> customFieldValues) {
        doAddCampaignToCampaignLibrary(libraryId, campaign);
        customFieldValueService.initCustomFieldValues(campaign, customFieldValues);
        createAttachmentsFromLibraryNode(campaign, campaign);
        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        if (activeMilestone.isPresent()) {
            milestoneManager.bindCampaignToMilestone(campaign.getId(), activeMilestone.get().getId());
        }

        return campaign;
    }

    private void doAddCampaignToCampaignLibrary(long libraryId, Campaign newCampaign) {
        CampaignLibrary library = campaignLibraryDao.findById(libraryId);

        if (!library.isContentNameAvailable(newCampaign.getName())) {
            throw new DuplicateNameException(newCampaign.getName(), newCampaign.getName());
        } else {
            library.addContent(newCampaign);
            campaignDao.persist(newCampaign);
            createCustomFieldValues(newCampaign);
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.campaign.CampaignLibrary', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public void addSprintToCampaignLibrary(@Id long libraryId, Sprint newSprint) {
        CampaignLibrary library = campaignLibraryDao.findById(libraryId);
        library.addContent(newSprint);
        sprintDao.persist(newSprint);
        createCustomFieldValues(newSprint);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.campaign.CampaignLibrary', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public void addSprintToCampaignLibrary(
            @Id long libraryId, Sprint sprint, Map<Long, RawValue> customFieldValues) {
        addSprintToCampaignLibrary(libraryId, sprint);
        customFieldValueService.initCustomFieldValues(sprint, customFieldValues);
        createAttachmentsFromLibraryNode(sprint, sprint);
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public SprintGroup addSprintGroupToCampaignLibrary(
            @Id long libraryId, SprintGroup newSprintGroup) {
        CampaignLibrary library = campaignLibraryDao.findById(libraryId);

        if (!library.isContentNameAvailable(newSprintGroup.getName())) {
            throw new DuplicateNameException(newSprintGroup.getName(), newSprintGroup.getName());
        } else {
            library.addContent(newSprintGroup);
            sprintGroupDao.persist(newSprintGroup);
            createCustomFieldValues(newSprintGroup);
        }
        return newSprintGroup;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.campaign.CampaignLibrary', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public void addSprintGroupToCampaignLibrary(
            @Id long libraryId, SprintGroup sprintGroup, Map<Long, RawValue> customFieldValues) {
        addSprintGroupToCampaignLibrary(libraryId, sprintGroup);
        customFieldValueService.initCustomFieldValues(sprintGroup, customFieldValues);
        createAttachmentsFromLibraryNode(sprintGroup, sprintGroup);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#folderId, 'org.squashtest.tm.domain.campaign.CampaignFolder', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public Campaign addCampaignToCampaignFolder(
            @Id long folderId, Campaign campaign, Map<Long, RawValue> customFieldValues) {
        return addCampaignToCampaignFolderUnsecured(folderId, campaign, customFieldValues);
    }

    @Override
    public Campaign addCampaignToCampaignFolderUnsecured(
            long folderId, Campaign campaign, Map<Long, RawValue> customFieldValues) {
        doAddCampaignToCampaignFolder(folderId, campaign);
        customFieldValueService.initCustomFieldValues(campaign, customFieldValues);
        createAttachmentsFromLibraryNode(campaign, campaign);

        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        if (activeMilestone.isPresent()) {
            milestoneManager.bindCampaignToMilestone(campaign.getId(), activeMilestone.get().getId());
        }
        return campaign;
    }

    private void doAddCampaignToCampaignFolder(long folderId, Campaign newCampaign) {
        CampaignFolder folder = campaignFolderDao.findById(folderId);
        if (!folder.isContentNameAvailable(newCampaign.getName())) {
            throw new DuplicateNameException(newCampaign.getName(), newCampaign.getName());
        } else {
            folder.addContent(newCampaign);
            campaignDao.persist(newCampaign);
            createCustomFieldValues(newCampaign);
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#folderId, 'org.squashtest.tm.domain.campaign.CampaignFolder', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public void addSprintToCampaignFolder(@Id long folderId, Sprint newSprint) {
        CampaignFolder folder = campaignFolderDao.findById(folderId);
        folder.addContent(newSprint);
        sprintDao.persist(newSprint);
        createCustomFieldValues(newSprint);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#folderId, 'org.squashtest.tm.domain.campaign.CampaignFolder', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public void addSprintToCampaignFolder(
            @Id long folderId, Sprint sprint, Map<Long, RawValue> customFieldValues) {
        addSprintToCampaignFolder(folderId, sprint);
        customFieldValueService.initCustomFieldValues(sprint, customFieldValues);
        createAttachmentsFromLibraryNode(sprint, sprint);
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public SprintGroup addSprintGroupToCampaignFolder(@Id long folderId, SprintGroup newSprintGroup) {
        CampaignFolder folder = campaignFolderDao.findById(folderId);
        if (!folder.isContentNameAvailable(newSprintGroup.getName())) {
            throw new DuplicateNameException(newSprintGroup.getName(), newSprintGroup.getName());
        } else {
            folder.addContent(newSprintGroup);
            sprintGroupDao.persist(newSprintGroup);
            createCustomFieldValues(newSprintGroup);
        }
        return newSprintGroup;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#folderId, 'org.squashtest.tm.domain.campaign.CampaignFolder', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public SprintGroup addSprintGroupToCampaignFolder(
            @Id long folderId, SprintGroup sprintGroup, Map<Long, RawValue> customFieldValues) {
        addSprintGroupToCampaignFolder(folderId, sprintGroup);
        customFieldValueService.initCustomFieldValues(sprintGroup, customFieldValues);
        createAttachmentsFromLibraryNode(sprintGroup, sprintGroup);
        return sprintGroup;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#sprintGroupId, 'org.squashtest.tm.domain.campaign.SprintGroup', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public void addSprintToSprintGroup(@Id long sprintGroupId, Sprint newSprint) {
        SprintGroup sprintGroup = sprintGroupDao.findById(sprintGroupId);
        sprintGroup.addContent(newSprint);
        sprintDao.persist(newSprint);
        createCustomFieldValues(newSprint);
        createAttachmentsFromLibraryNode(newSprint, newSprint);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#sprintGroupId, 'org.squashtest.tm.domain.campaign.SprintGroup', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public void addSprintToSprintGroup(
            @Id long sprintGroupId, Sprint sprint, Map<Long, RawValue> customFieldValues) {
        SprintGroup sprintGroup = sprintGroupDao.findById(sprintGroupId);
        sprintGroup.addContent(sprint);
        sprintDao.persist(sprint);
        customFieldValueService.initCustomFieldValues(sprint, customFieldValues);
        createAttachmentsFromLibraryNode(sprint, sprint);
    }

    @Override
    @UsedInPlugin("Xsquash4Jira")
    public Long createSprintGroupAndFoldersHierarchy(String folderPath) {

        List<String> paths = PathUtils.scanPath(folderPath);
        String[] splits = PathUtils.splitPath(folderPath);
        Project project = projectDao.findByName(PathUtils.unescapePathPartSlashes(splits[0]));

        if (splits.length < 2) {
            throw new IllegalArgumentException(
                    "Folder path must contain at least a valid /projectName/folder. "
                            + "This is incorrect: "
                            + folderPath);
        }
        if (project == null) {
            throw new IllegalArgumentException("Folder path must concern an existing project");
        }

        Long campaignLibraryId = project.getCampaignLibrary().getId();

        PermissionsUtils.checkPermission(
                permissionService,
                Collections.singletonList(campaignLibraryId),
                Permissions.CREATE.name(),
                CampaignLibrary.class.getName());

        if (splits.length == 2) {
            return createOnlyASprintGroup(project.getId(), splits[1], campaignLibraryId);
        } else {
            return createFoldersAndASprintGroup(paths, campaignLibraryId);
        }
    }

    private Long createOnlyASprintGroup(Long projectId, String sprintGroupName, Long libraryId) {
        Long existingSprintGroupId =
                sprintGroupDao.findRootNodeIdByProjectIdAndName(projectId, sprintGroupName);
        if (existingSprintGroupId != null) {
            return existingSprintGroupId;
        }

        SprintGroup sprintGroup = new SprintGroup();
        sprintGroup.setName(sprintGroupName);
        addSprintGroupToCampaignLibrary(libraryId, sprintGroup);
        return sprintGroup.getId();
    }

    private Long createFoldersAndASprintGroup(List<String> paths, Long libraryId) {
        List<Long> existingNodeIds = campaignLibraryNodeDao.findNodeIdsByPath(paths);

        List<String> pathSplitsWithoutProjectName =
                PathUtils.getSplitPathWithoutProjectNameFromAPath(paths.get(paths.size() - 1));

        if (existingNodeIds.size() == pathSplitsWithoutProjectName.size()) {
            return existingNodeIds.get(existingNodeIds.size() - 1);
        }

        int startIndex = existingNodeIds.isEmpty() ? 0 : existingNodeIds.size();
        // Here, the size - 1 represents the last index of pathSplitsWithoutProjectName. The sublist's
        // endIndex is exclusive,
        // and since we don't take the last element because it will be the SprintGroup, this is the
        // right value
        int endIndex = pathSplitsWithoutProjectName.size() - 1;
        List<String> foldersToCreate = pathSplitsWithoutProjectName.subList(startIndex, endIndex);

        CampaignFolder startingCampaignFolder = null;
        if (!existingNodeIds.isEmpty()) {
            startingCampaignFolder = campaignFolderDao.findById(existingNodeIds.get(startIndex - 1));
        }
        CampaignFolder currentTreeFolder = startingCampaignFolder;

        for (String folder : foldersToCreate) {
            CampaignFolder campaignFolder = new CampaignFolder();
            campaignFolder.setName(folder);
            if (Objects.isNull(currentTreeFolder)) {
                currentTreeFolder = addFolderToLibrary(libraryId, campaignFolder);
            } else {
                currentTreeFolder = addFolderToFolder(currentTreeFolder.getId(), campaignFolder);
            }
        }
        // Here is the last element of the path: the SprintGroup
        SprintGroup sprintGroup = new SprintGroup();
        sprintGroup.setName(pathSplitsWithoutProjectName.get(pathSplitsWithoutProjectName.size() - 1));

        Long sprintGroupId = null;
        if (Objects.nonNull(currentTreeFolder)) {
            SprintGroup createdSprintGroup =
                    addSprintGroupToCampaignFolder(currentTreeFolder.getId(), sprintGroup);
            sprintGroupId = createdSprintGroup.getId();
        }
        return sprintGroupId;
    }

    @Override
    public List<SuppressionPreviewReport> simulateIterationDeletion(List<Long> targetIds) {
        return deletionHandler.simulateIterationDeletion(targetIds);
    }

    @Override
    @BatchPreventConcurrent(
            entityType = CampaignLibraryNode.class,
            coercer = IterationToCampaignIdsCoercer.class)
    public OperationReport deleteIterations(@Ids List<Long> targetIds) {
        return deletionHandler.deleteIterations(targetIds);
    }

    @Override
    public List<SuppressionPreviewReport> simulateSuiteDeletion(List<Long> targetIds) {
        return deletionHandler.simulateSuiteDeletion(targetIds);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign' ,'EXPORT')"
                    + OR_HAS_ROLE_ADMIN)
    public CampaignExportCSVModel exportCampaignToCSV(Long campaignId, String exportType) {

        Campaign campaign = campaignDao.findById(campaignId);

        WritableCampaignCSVModel model;

        if ("L".equals(exportType)) {
            model = simpleCampaignExportCSVModelProvider.get();
        } else if ("F".equals(exportType)) {
            model = fullCampaignExportCSVModelProvider.get();
        } else {
            model = standardCampaignExportCSVModelProvider.get();
        }

        model.setCampaign(campaign);
        model.init();

        return model;
    }

    private Map<Long, String> findCampaignIdsAndComputePathAsNameFromMultiSelection(
            Set<Long> libraryIds, Set<Long> nodeIds, Set<Long> campaignIds) {
        /*
         * first, let's check the permissions on those root nodes By
         * transitivity, if the user can read them then it will be allowed to
         * read the campaigns below
         */
        Collection<Long> readLibIds =
                securityFilterIds(libraryIds, CampaignLibrary.class.getName(), "READ");
        Collection<Long> readNodeIds =
                securityFilterIds(nodeIds, CampaignLibraryNode.class.getName(), "READ");
        Collection<Long> readCampIds =
                securityFilterIds(campaignIds, CampaignLibraryNode.class.getName(), "READ");

        // now we can collect the campaigns
        Map<Long, String> campaignNameMap = new HashMap<>();

        if (!readLibIds.isEmpty()) {
            campaignNameMap.putAll(
                    campaignDao.findAllCampaignIdsAndComputePathAsNameByLibraries(readLibIds));
        }
        if (!readNodeIds.isEmpty()) {
            campaignNameMap.putAll(
                    campaignDao.findAllCampaignIdsAndComputePathAsNameByNodeIds(readNodeIds));
        }
        if (!readCampIds.isEmpty()) {
            campaignNameMap.putAll(
                    campaignDao.findAllCampaignIdsAndNameByNodeIds(new ArrayList<>(readCampIds)));
        }

        appendMissingCampaignReferencesToCampaignNameMap(campaignNameMap);

        return campaignNameMap;
    }

    private void appendMissingCampaignReferencesToCampaignNameMap(Map<Long, String> campaignNameMap) {
        campaignNameMap
                .keySet()
                .forEach(
                        campaignIdKey -> {
                            String campaignPath = campaignNameMap.get(campaignIdKey);
                            Campaign campaign = campaignDao.findById(campaignIdKey);

                            if (!campaignPath.contains(campaign.getFullName())) {
                                campaignPath = campaignPath.replace(campaign.getName(), campaign.getFullName());
                                campaignNameMap.replace(campaignIdKey, campaignPath);
                            }
                        });
    }

    @Override
    public StatisticsBundle compileStatisticsFromSelection(
            NodeReferences nodeReferences,
            List<EntityReference> entityReferences,
            boolean lastExecutionScope) {
        boolean selectionHasDifferentNodeTypes =
                doesSelectionContainDifferentNodeTypes(entityReferences);
        boolean hasOnlyLibraryOrFolderNodeTypes =
                doesSelectionContainOnlyLibraryOrFolderNodeTypes(entityReferences);
        boolean hasIterationNodes =
                entityReferences.stream()
                        .anyMatch(entityReference -> entityReference.getType().equals(EntityType.ITERATION));
        return getStatisticsForSelectionNodeTypes(
                nodeReferences,
                selectionHasDifferentNodeTypes,
                hasIterationNodes,
                hasOnlyLibraryOrFolderNodeTypes,
                lastExecutionScope);
    }

    private Boolean doesSelectionContainDifferentNodeTypes(List<EntityReference> entityReferences) {
        EntityReference firstNode = entityReferences.get(0);
        List<EntityReference> sameTypeNodes =
                entityReferences.stream()
                        .filter(node -> node.getType().equals(firstNode.getType()))
                        .toList();
        return sameTypeNodes.size() != entityReferences.size();
    }

    private Boolean doesSelectionContainOnlyLibraryOrFolderNodeTypes(
            List<EntityReference> entityReferences) {
        List<EntityReference> libraryFolderTypeNodes =
                entityReferences.stream()
                        .filter(
                                node ->
                                        EntityType.CAMPAIGN_LIBRARY.equals(node.getType())
                                                || CAMPAIGN_FOLDER.equals(node.getType()))
                        .toList();
        return libraryFolderTypeNodes.size() == entityReferences.size();
    }

    private StatisticsBundle getStatisticsForSelectionNodeTypes(
            NodeReferences nodeReferences,
            boolean selectionHasDifferentNodeTypes,
            boolean hasIterationNodes,
            boolean hasOnlyLibraryOrFolderNodeTypes,
            boolean lastExecutionScope) {
        StatisticsBundle statisticsBundle = new StatisticsBundle();

        if (((selectionHasDifferentNodeTypes) && (!hasIterationNodes))
                || Boolean.TRUE.equals(hasOnlyLibraryOrFolderNodeTypes)) {
            // selection does not include iteration nodes, it is some combination of Library, folder and
            // campaign nodes
            statisticsBundle =
                    getCampaignMultiSelectionStatisticsBundle(nodeReferences, lastExecutionScope);

        } else if ((!selectionHasDifferentNodeTypes) && (Boolean.TRUE.equals(hasIterationNodes))) {
            // selected nodes are exclusively iteration nodes
            statisticsBundle = getIterationSelectionStatisticsBundle(nodeReferences, lastExecutionScope);

        } else if (Boolean.FALSE.equals(selectionHasDifferentNodeTypes)) {
            // selected nodes are exclusively campaign nodes
            statisticsBundle = getCampaignSelectionStatisticsBundle(nodeReferences, lastExecutionScope);
        }

        return statisticsBundle;
    }

    private StatisticsBundle getCampaignMultiSelectionStatisticsBundle(
            NodeReferences nodeReferences, boolean lastExecutionScope) {
        StatisticsBundle statisticsBundle = new StatisticsBundle();

        // get all descendant campaignIds and set their path as name in a map where key = campaignId and
        // value = campaignName
        Set<Long> selectedCampaignIds = extractCampaignIdsFromNodeReferences(nodeReferences);
        Map<Long, String> multiCampaignNameMap =
                findCampaignIdsAndComputePathAsNameFromMultiSelection(
                        nodeReferences.extractLibraryIds(),
                        nodeReferences.extractNonLibraryIds(),
                        selectedCampaignIds);

        if (!multiCampaignNameMap.isEmpty()) {
            return campaignStatisticsService.gatherMultiStatisticsBundle(
                    multiCampaignNameMap, lastExecutionScope);
        }

        return statisticsBundle;
    }

    private StatisticsBundle getIterationSelectionStatisticsBundle(
            NodeReferences nodeReferences, Boolean lastExecutionScope) {
        StatisticsBundle statisticsBundle = new StatisticsBundle();

        List<Long> iterationIds = new ArrayList<>(nodeReferences.extractNonLibraryIds());
        if (!iterationIds.isEmpty()) {
            PermissionsUtils.checkPermission(
                    permissionService, iterationIds, Permissions.READ.name(), Iteration.class.getName());

            return iterationStatisticsService.gatherIterationStatisticsBundle(
                    iterationIds, lastExecutionScope);
        }

        return statisticsBundle;
    }

    private StatisticsBundle getCampaignSelectionStatisticsBundle(
            NodeReferences nodeReferences, Boolean lastExecutionScope) {
        StatisticsBundle statisticsBundle = new StatisticsBundle();
        List<Long> campaignIds = new ArrayList<>(nodeReferences.extractNonLibraryIds());

        if (!campaignIds.isEmpty()) {
            PermissionsUtils.checkPermission(
                    permissionService, campaignIds, Permissions.READ.name(), Campaign.class.getName());

            return campaignStatisticsService.gatherCampaignStatisticsBundle(
                    campaignIds, lastExecutionScope);
        }

        return statisticsBundle;
    }

    private Set<Long> extractCampaignIdsFromNodeReferences(NodeReferences nodeReferences) {
        return nodeReferences.extractNonLibraries().stream()
                .filter(nodeReference -> nodeReference.getNodeType().equals(NodeType.CAMPAIGN))
                .map(NodeReference::getId)
                .collect(Collectors.toSet());
    }

    @Override
    @BatchPreventConcurrent(
            entityType = Iteration.class,
            coercer = TestSuiteToIterationCoercerForList.class)
    @CheckBlockingMilestones(entityType = TestSuite.class)
    public OperationReport deleteSuites(@Ids List<Long> suiteIds, boolean removeFromIter) {

        return deletionHandler.deleteSuites(suiteIds, removeFromIter);
    }

    // ####################### STATISTICS ############################
    @Override
    public StatisticsBundle gatherCampaignStatisticsBundleByMilestone(boolean lastExecutionScope) {
        return campaignStatisticsService.gatherMilestoneStatisticsBundle(lastExecutionScope);
    }

    /**
     * Gather the dashboard statistics for all child campaigns of a given libraryId presumed to be a
     * CampaignLibraryId
     *
     * @param libraryId CampaignLibraryId
     * @return StatisticsBundle contains all the compiled stats to generate dashboard graphs
     */
    @Override
    @PreAuthorize(READ_CAMPAIGN_LIBRARY_OR_HAS_ROLE_ADMIN)
    public StatisticsBundle gatherLibraryStatisticsBundle(
            long libraryId, boolean lastExecutionScope) {
        // get all descendant campaignIds
        Map<Long, String> campaignNameMap =
                campaignDao.findAllCampaignIdsAndNameByLibraryIds(Arrays.asList(libraryId));
        return campaignStatisticsService.gatherMultiStatisticsBundle(
                campaignNameMap, lastExecutionScope);
    }

    @Override
    @PreAuthorize(READ_CAMPAIGN_OR_ROLE_ADMIN)
    public StatisticsBundle gatherCampaignStatisticsBundle(
            long campaignId, boolean lastExecutionScope) {
        return campaignStatisticsService.gatherCampaignStatisticsBundle(
                Arrays.asList(campaignId), lastExecutionScope);
    }

    @Override
    @PreAuthorize(READ_CAMPFOLDER_OR_ROLE_ADMIN)
    public StatisticsBundle gatherFolderStatisticsBundle(
            Long campFolderId, boolean lastExecutionScope) {
        Map<Long, String> campaignNameMap =
                campaignDao.findAllCampaignIdsAndNameByNodeIds(Arrays.asList(campFolderId));
        return campaignStatisticsService.gatherMultiStatisticsBundle(
                campaignNameMap, lastExecutionScope);
    }

    // ####################### PREVENT CONCURRENCY OVERRIDES
    // ############################

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public CampaignFolder addFolderToFolder(@Id long destinationId, CampaignFolder newFolder) {
        super.addFolderToFolder(destinationId, newFolder);
        generateCUF(newFolder);
        createAttachmentsFromLibraryNode(newFolder, newFolder);
        return newFolder;
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public CampaignFolder addFolderToFolder(
            @Id long destinationId, CampaignFolder newFolder, Map<Long, RawValue> customFields) {
        return super.addFolderToFolder(destinationId, newFolder, customFields);
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public CampaignFolder addFolderToFolder(@Id long destinationId, NewFolderDto folderDto) {
        CampaignFolder newFolder = (CampaignFolder) folderDto.toFolder(CAMPAIGN_FOLDER);
        return addFolderToFolder(destinationId, newFolder, folderDto.getCustomFields());
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public CampaignFolder addFolderToLibrary(@Id long destinationId, CampaignFolder newFolder) {
        super.addFolderToLibrary(destinationId, newFolder);
        generateCUF(newFolder);
        createAttachmentsFromLibraryNode(newFolder, newFolder);
        return newFolder;
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public CampaignFolder addFolderToLibrary(
            @Id long destinationId, CampaignFolder newFolder, Map<Long, RawValue> customFields) {
        return super.addFolderToLibrary(destinationId, newFolder, customFields);
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibrary.class)
    public CampaignFolder addFolderToLibrary(@Id long destinationId, NewFolderDto folderDto) {
        CampaignFolder newFolder = (CampaignFolder) folderDto.toFolder(CAMPAIGN_FOLDER);
        return addFolderToLibrary(destinationId, newFolder, folderDto.getCustomFields());
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public void addFolderToSprintGroup(@Id long destinationId, CampaignFolder newFolder) {
        SprintGroup sprintGroup = sprintGroupDao.findById(destinationId);
        sprintGroup.addContent(newFolder);
        getFolderDao().persist(newFolder);
        generateCUF(newFolder);
        createAttachmentsFromLibraryNode(newFolder, newFolder);
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public CampaignFolder addFolderToSprintGroup(
            @Id long destinationId, CampaignFolder newFolder, Map<Long, RawValue> customFields) {
        SprintGroup sprintGroup = sprintGroupDao.findById(destinationId);
        sprintGroup.addContent(newFolder);
        getFolderDao().persist(newFolder);
        createCustomFieldValues(newFolder);
        if (customFields != null && !customFields.isEmpty()) {
            customFieldValueService.initCustomFieldValues(newFolder, customFields);
        }
        createAttachmentsFromLibraryNode(newFolder, newFolder);

        return newFolder;
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    public CampaignFolder addFolderToSprintGroup(@Id long destinationId, NewFolderDto folderDto) {
        CampaignFolder newFolder = (CampaignFolder) folderDto.toFolder(CAMPAIGN_FOLDER);
        return addFolderToSprintGroup(destinationId, newFolder, folderDto.getCustomFields());
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibrary.class)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public TestSuite addTestSuiteToIteration(
            @Id Long iterationId, TestSuite suite, Map<Long, RawValue> customFieldValues) {
        return addTestSuiteToIterationUnsecured(iterationId, suite, customFieldValues);
    }

    @Override
    public TestSuite addTestSuiteToIterationUnsecured(
            @Id Long iterationId, TestSuite suite, Map<Long, RawValue> customFieldValues) {
        iterationModificationService.addTestSuite(iterationId, suite);

        createCustomFieldValues(suite, customFieldValues);

        createAttachmentForTestSuite(suite);

        return suite;
    }

    private void createAttachmentForTestSuite(TestSuite suite) {
        String description = suite.getDescription();

        if (description == null || description.isEmpty()) {
            return;
        }

        String html =
                attachmentManagerService.handleRichTextAttachments(description, suite.getAttachmentList());

        if (!description.equals(html)) {
            suite.setDescription(html);
        }
    }

    @Override
    public OperationReport deleteNodes(CampaignLibraryNodesToDelete deletionNodes, boolean iter) {

        unbindMilestonesFromNodes(deletionNodes.getCampaignIds());

        OperationReport nodesReport = deleteNodes(deletionNodes.getCampaignLibraryNodeIds());
        OperationReport suiteReport = deleteSuites(deletionNodes.getSuiteIds(), iter);
        OperationReport iterationReport = deleteIterations(deletionNodes.getIterationIds());

        OperationReport totalReport = new OperationReport();

        totalReport.addRemoved(nodesReport.getRemoved());
        totalReport.addRemoved(suiteReport.getRemoved());
        totalReport.addRemoved(iterationReport.getRemoved());
        return totalReport;
    }

    @Override
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    public void validatePathForSync(String projectName, String sprintSynchronisationPath) {
        if (StringUtils.isBlank(sprintSynchronisationPath)) {
            throw new RequiredFieldException("path");
        }

        if (!PathUtils.isPathSyntaxValid(sprintSynchronisationPath)) {
            throw new PathValidationDomainException(SPRINT_SYNCHRONISATION_PATH);
        }

        remoteSynchronisationService.checkSprintPathAvailability(
                projectName, sprintSynchronisationPath);

        String fullSprintSynchronisationPath =
                PathUtils.appendPathToProjectName(projectName, sprintSynchronisationPath);
        List<Long> nodeIds =
                campaignLibraryNodeDao.findNodeIdsByPath(
                        Collections.singletonList(fullSprintSynchronisationPath));

        boolean pathContainsASprintGroup = !sprintGroupDao.findIdsByClnIds(nodeIds).isEmpty();
        if (pathContainsASprintGroup) {
            throw new PathContainsASprintGroupException(SPRINT_SYNCHRONISATION_PATH);
        }
    }

    private void unbindMilestonesFromNodes(List<Long> nodesIds) {
        nodesIds.forEach(
                campaignId -> {
                    Collection<Milestone> milestones =
                            customCampaignModificationService.findAllMilestones(campaignId);
                    Collection<Long> milestonesIds = milestones.stream().map(Milestone::getId).toList();
                    milestoneManager.unbindCampaignFromMilestonesUnsecured(campaignId, milestonesIds);
                });
    }

    private void generateCUF(CampaignFolder newFolder) {
        List<CustomFieldBinding> projectsBindings =
                customFieldBindingFinderService.findCustomFieldsForProjectAndEntity(
                        newFolder.getProject().getId(), BindableEntity.CAMPAIGN_FOLDER);
        for (CustomFieldBinding binding : projectsBindings) {
            customFieldValueService.cascadeCustomFieldValuesCreationNotCreatedFolderYet(
                    binding, newFolder);
        }
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = "sourceNodesIds",
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = "sourceNodesIds",
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void copyNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids("sourceNodesIds") Long[] sourceNodesIds,
            ClipboardPayload clipboardPayload) {
        checkIllegalSprintGroupHierarchy(destinationId, sourceNodesIds);
        NodeType nodePaste = strategyCopierService.verifyPermissionAndGetNodePaste(clipboardPayload);
        strategyCopierService.copyNodeToCampaignFolder(destinationId, clipboardPayload, nodePaste);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void copyNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetIds,
            ClipboardPayload clipboardPayload) {
        NodeType nodePaste = strategyCopierService.verifyPermissionAndGetNodePaste(clipboardPayload);
        strategyCopierService.copyNodeToCampaignLibrary(destinationId, clipboardPayload, nodePaste);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void copyNodesToSprintGroup(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetIds,
            ClipboardPayload clipboardPayload) {
        if (targetIds.length == 0) {
            return;
        }
        checkCanAddSelectionToSprintGroup(targetIds);
        NodeType nodePaste = strategyCopierService.verifyPermissionAndGetNodePaste(clipboardPayload);
        strategyCopierService.copyNodeToSpringGroup(destinationId, clipboardPayload, nodePaste);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetIds,
            ClipboardPayload clipboardPayload) {
        checkIllegalSprintGroupHierarchy(destinationId, targetIds);
        super.moveNodesToFolder(destinationId, targetIds, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetIds,
            int position,
            ClipboardPayload clipboardPayload) {
        checkIllegalSprintGroupHierarchy(destinationId, targetIds);
        super.moveNodesToFolder(destinationId, targetIds, position, clipboardPayload);
    }

    private void checkIllegalSprintGroupHierarchy(long destinationId, Long[] targetIds) {
        if (!isInSprintGroup(destinationId)) {
            return;
        }

        final List<Long> targetIdsList = Arrays.asList(targetIds);

        if (doesSelectionContainSprintGroup(targetIdsList)
                || doesSelectionContainCampaign(targetIdsList)) {
            throw new IllegalSprintGroupHierarchyException();
        }
    }

    private boolean isInSprintGroup(long folderId) {
        return sprintGroupDisplayDao.findParentSprintGroupId(folderId) != null;
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetIds,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToLibrary(destinationId, targetIds, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId, @Ids(TARGET_ID) Long[] targetIds) {
        super.moveNodesToLibrary(
                destinationId, targetIds, ClipboardPayload.withWhiteListIgnored(Arrays.asList(targetIds)));
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetIds,
            int position,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToLibrary(destinationId, targetIds, position, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId, @Ids(TARGET_ID) Long[] targetIds, int position) {
        super.moveNodesToLibrary(
                destinationId,
                targetIds,
                position,
                ClipboardPayload.withWhiteListIgnored(Arrays.asList(targetIds)));
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = CampaignLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = TARGET_ID,
                        coercer = CampaignLibraryIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = CLNAndParentIdsCoercerForArray.class)
            })
    public void moveNodesToSprintGroup(
            @Id(DESTINATION_ID) Long destinationId,
            @Ids(TARGET_ID) Long[] movedNodeIds,
            ClipboardPayload clipboardPayload) {
        if (movedNodeIds.length == 0) {
            return;
        }

        checkCanAddSelectionToSprintGroup(movedNodeIds);

        try {
            PasteStrategy<SprintGroup, CampaignLibraryNode> pasteStrategy =
                    pasteToSprintGroupStrategyProvider.get();
            makeMoverStrategy(pasteStrategy);
            pasteStrategy.pasteNodes(destinationId, clipboardPayload);
        } catch (NullArgumentException | DuplicateNameException dne) {
            throw new NameAlreadyExistsAtDestinationException(dne);
        }
    }

    private void checkCanAddSelectionToSprintGroup(Long[] movedNodeIds) {
        if (doesSelectionContainSprintGroup(List.of(movedNodeIds))) {
            throw new IllegalSprintGroupHierarchyException();
        }

        if (doesSelectionContainCampaign(List.of(movedNodeIds))) {
            throw new IllegalSprintGroupHierarchyException();
        }
    }

    private boolean doesSelectionContainSprintGroup(List<Long> movedNodeIds) {
        return !sprintGroupDisplayDao.findContainedSprintGroupIds(movedNodeIds).isEmpty();
    }

    private boolean doesSelectionContainCampaign(List<Long> movedNodeIds) {
        return !campaignDisplayDao.findContainedCampaignIds(movedNodeIds).isEmpty();
    }

    @Override
    @PreventConcurrents(
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = CampaignLibrary.class,
                        paramName = "targetIds",
                        coercer = CampaignLibraryIdsCoercerForList.class),
                @BatchPreventConcurrent(
                        entityType = CampaignLibraryNode.class,
                        paramName = "targetIds",
                        coercer = CLNAndParentIdsCoercerForList.class)
            })
    @CheckBlockingMilestones(entityType = Campaign.class)
    public OperationReport deleteNodes(@Ids("targetIds") List<Long> targetIds) {
        return super.deleteNodes(targetIds);
    }

    private record Child(Long id, String path) {}

    // ###################### /PREVENT CONCURRENCY OVERIDES
    // ############################

}
