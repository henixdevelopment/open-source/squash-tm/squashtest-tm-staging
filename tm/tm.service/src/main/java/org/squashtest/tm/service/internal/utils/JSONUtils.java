/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Objects;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;

public final class JSONUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(JSONUtils.class);
    private static final ObjectMapper objMapper = new ObjectMapper();

    private JSONUtils() {}

    /**
     * Returns the value from a key inside a String json.
     *
     * @param json The whole JSON object in String
     * @param key The key from which the value will be returned
     * @return The value, or empty String if value is null (key not found, or null value).
     */
    public static String getValueFromKey(String json, String key) {
        try {
            JsonNode node = objMapper.readTree(json);
            JsonNode valueNode = node.get(key);
            return Objects.isNull(valueNode) ? "" : valueNode.asText();
        } catch (JsonProcessingException e) {
            LOGGER.debug("Could not transform {} in JSON", json, e);
        }
        return null;
    }
}
