/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.tree.ImportedRequirementNode;
import org.squashtest.tm.service.internal.library.LibraryUtils;

public final class RequirementImportDto {

    private final Requirement root;

    // Contains all versions generated within the tree
    private final List<RequirementVersionImportDto> generatedVersions = new ArrayList<>();

    private RequirementImportDto(Requirement root) {
        this.root = root;
    }

    public static RequirementImportDto fromRequirementNode(
            ImportedRequirementNode requirementNode,
            Map<RequirementTarget, List<RequirementVersionImportDto>> versionsByRequirementTarget,
            Project project) {
        List<RequirementVersionImportDto> requirementVersions =
                versionsByRequirementTarget.getOrDefault(
                        requirementNode.getTarget(),
                        List.of(requirementNode.toRequirementVersionImportDto(project)));

        RequirementImportDto root = initializeRoot(requirementVersions, project);

        addChildren(requirementNode, versionsByRequirementTarget, root, root.getRequirement());

        return root;
    }

    private static RequirementImportDto initializeRoot(
            List<RequirementVersionImportDto> versions, Project project) {
        if (versions == null || versions.isEmpty()) {
            throw new IllegalArgumentException("versions cannot be null or empty");
        }

        RequirementVersionImportDto currentVersionDto = versions.get(0);
        RequirementVersion currentVersion = currentVersionDto.requirementVersion();
        currentVersion.updateStatusWithoutCheck(currentVersionDto.finalStatus());

        Requirement createdRequirement =
                currentVersionDto.isHighLevelRequirement()
                        ? new HighLevelRequirement(currentVersion)
                        : new Requirement(currentVersion);

        createdRequirement.notifyAssociatedWithProject(project);

        RequirementImportDto root = new RequirementImportDto(createdRequirement);

        root.addGeneratedVersion(currentVersionDto);

        createRequirementVersions(root, versions, createdRequirement);

        return root;
    }

    private static void createRequirementVersions(
            RequirementImportDto root,
            List<RequirementVersionImportDto> versions,
            Requirement createdRequirement) {
        versions.stream()
                .skip(1)
                .forEach(
                        versionDto -> {
                            RequirementVersion version = versionDto.requirementVersion();
                            createdRequirement.addExistingRequirementVersion(version);

                            version.updateStatusWithoutCheck(versionDto.finalStatus());

                            root.addGeneratedVersion(versionDto);
                        });
    }

    private static void addChild(
            ImportedRequirementNode current,
            Requirement parent,
            List<String> parentContentNames,
            Map<RequirementTarget, List<RequirementVersionImportDto>> versionsByRequirementTarget,
            RequirementImportDto root) {
        List<RequirementVersionImportDto> versions =
                versionsByRequirementTarget.getOrDefault(
                        current.getTarget(),
                        List.of(current.toRequirementVersionImportDto(parent.getProject())));

        Requirement subRequirement = createRequirement(versions, root);

        LibraryUtils.fixConflictNames(parentContentNames, subRequirement);

        parent.addContent(subRequirement);

        addChildren(current, versionsByRequirementTarget, root, subRequirement);
    }

    private static void addChildren(
            ImportedRequirementNode current,
            Map<RequirementTarget, List<RequirementVersionImportDto>> versionsByRequirementTarget,
            RequirementImportDto root,
            Requirement currentRequirement) {
        if (current.getContents().isEmpty()) {
            return;
        }

        List<String> contentNames = new ArrayList<>(currentRequirement.getContentNames());

        for (ImportedRequirementNode child : current.getContents()) {
            addChild(child, currentRequirement, contentNames, versionsByRequirementTarget, root);
        }
    }

    private static Requirement createRequirement(
            List<RequirementVersionImportDto> versions, RequirementImportDto root) {
        if (versions == null || versions.isEmpty()) {
            throw new IllegalArgumentException("versions cannot be null or empty");
        }

        RequirementVersionImportDto currentVersionDto = versions.get(0);
        RequirementVersion currentVersion = currentVersionDto.requirementVersion();

        Requirement createdRequirement = new Requirement(currentVersion);

        root.addGeneratedVersion(currentVersionDto);

        createRequirementVersions(root, versions, createdRequirement);

        return createdRequirement;
    }

    public Requirement getRequirement() {
        return root;
    }

    public List<RequirementVersionImportDto> getGeneratedVersions() {
        return generatedVersions;
    }

    private void addGeneratedVersion(RequirementVersionImportDto versionDto) {
        this.generatedVersions.add(versionDto);
    }
}
