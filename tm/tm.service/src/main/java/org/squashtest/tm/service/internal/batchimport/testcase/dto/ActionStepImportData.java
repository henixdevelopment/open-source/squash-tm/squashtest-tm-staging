/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase.dto;

import java.util.HashMap;
import java.util.Map;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;

public class ActionStepImportData {

    ActionTestStep actionTestStep;

    TestStepTarget target;

    Integer index;

    Map<Long, RawValue> acceptableCustomFields = new HashMap<>();

    public ActionStepImportData(ActionTestStep actionTestStep, TestStepTarget target, Integer index) {
        this.actionTestStep = actionTestStep;
        this.target = target;
        this.index = index;
    }

    public void addCustomFields(Map<Long, RawValue> customFields) {
        acceptableCustomFields.putAll(customFields);
    }

    public ActionTestStep getActionTestStep() {
        return actionTestStep;
    }

    public Map<Long, RawValue> getCustomFields() {
        return acceptableCustomFields;
    }

    public TestStepTarget getTarget() {
        return target;
    }

    public Integer getIndex() {
        return index;
    }
}
