/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.infolist.SystemListItem;
import org.squashtest.tm.service.internal.display.dto.InfoListAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.InfoListDto;
import org.squashtest.tm.service.internal.display.dto.InfoListItemAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.InfoListItemDto;
import org.squashtest.tm.service.internal.repository.display.InfoListDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class InfoListDisplayDaoImpl implements InfoListDisplayDao {

    @Inject private DSLContext dsl;

    @Override
    public List<InfoListDto> findAllWithItems() {

        List<InfoListDto> infoListDtos = new ArrayList<>();
        Map<Record, ? extends Result<? extends Record>> record =
                dsl.select(
                                INFO_LIST.LABEL,
                                INFO_LIST.CODE,
                                INFO_LIST.DESCRIPTION,
                                INFO_LIST.INFO_LIST_ID,
                                INFO_LIST_ITEM.LABEL,
                                INFO_LIST_ITEM.CODE,
                                INFO_LIST_ITEM.IS_DEFAULT,
                                INFO_LIST_ITEM.ICON_NAME,
                                INFO_LIST_ITEM.ITEM_ID,
                                INFO_LIST_ITEM.ITEM_TYPE)
                        .from(INFO_LIST)
                        .innerJoin(INFO_LIST_ITEM)
                        .on(INFO_LIST.INFO_LIST_ID.eq(INFO_LIST_ITEM.LIST_ID))
                        .orderBy(INFO_LIST.INFO_LIST_ID, INFO_LIST_ITEM.ITEM_INDEX)
                        .fetch()
                        .intoGroups(
                                Arrays.asList(
                                                INFO_LIST.LABEL,
                                                INFO_LIST.CODE,
                                                INFO_LIST.DESCRIPTION,
                                                INFO_LIST.INFO_LIST_ID)
                                        .toArray(new Field[0]));

        record.entrySet().stream()
                .forEach(
                        entry -> {
                            InfoListDto infoList = new InfoListDto();
                            infoList.setLabel(entry.getKey().get(INFO_LIST.LABEL));
                            infoList.setId(entry.getKey().get(INFO_LIST.INFO_LIST_ID));
                            infoList.setDescription(entry.getKey().get(INFO_LIST.DESCRIPTION));
                            infoList.setCode(entry.getKey().get(INFO_LIST.CODE));
                            infoListDtos.add(infoList);
                            entry.getValue().stream()
                                    .forEach(
                                            value -> {
                                                InfoListItemDto item = new InfoListItemDto();
                                                item.setLabel(value.get(INFO_LIST_ITEM.LABEL));
                                                item.setCode(value.get(INFO_LIST_ITEM.CODE));
                                                item.setIconName(value.get(INFO_LIST_ITEM.ICON_NAME));
                                                item.setId(value.get(INFO_LIST_ITEM.ITEM_ID));
                                                item.setDefault(value.get(INFO_LIST_ITEM.IS_DEFAULT));
                                                if (SystemListItem.SYSTEM_INFO_LIST_IDENTIFIER.equals(
                                                        value.get(INFO_LIST_ITEM.ITEM_TYPE))) {
                                                    item.setSystem(true);
                                                }
                                                infoList.getItems().add(item);
                                            });
                        });
        return infoListDtos;
    }

    @Override
    public InfoListAdminViewDto getInfoListById(long infoListId) {
        InfoListAdminViewDto dto =
                dsl.select(
                                INFO_LIST.INFO_LIST_ID.as(RequestAliasesConstants.ID),
                                INFO_LIST.CODE,
                                INFO_LIST.LABEL,
                                INFO_LIST.DESCRIPTION,
                                INFO_LIST.CREATED_BY,
                                INFO_LIST.CREATED_ON,
                                INFO_LIST.LAST_MODIFIED_BY,
                                INFO_LIST.LAST_MODIFIED_ON)
                        .from(INFO_LIST)
                        .where(INFO_LIST.INFO_LIST_ID.eq(infoListId))
                        .fetchOne()
                        .into(InfoListAdminViewDto.class);

        dto.setItems(getInfoListItemsByListId(infoListId));
        return dto;
    }

    private List<InfoListItemAdminViewDto> getInfoListItemsByListId(long infoListId) {
        return dsl.select(
                        INFO_LIST_ITEM.ITEM_ID.as(RequestAliasesConstants.ID),
                        INFO_LIST_ITEM.CODE,
                        INFO_LIST_ITEM.COLOUR,
                        INFO_LIST_ITEM.LABEL,
                        INFO_LIST_ITEM.ICON_NAME,
                        INFO_LIST_ITEM.ITEM_INDEX,
                        INFO_LIST_ITEM.IS_DEFAULT,
                        DSL.field(INFO_LIST_ITEM.ITEM_TYPE.eq(SystemListItem.SYSTEM_INFO_LIST_IDENTIFIER))
                                .as("SYSTEM"))
                .from(INFO_LIST_ITEM)
                .where(INFO_LIST_ITEM.LIST_ID.eq(infoListId))
                .groupBy(INFO_LIST_ITEM.ITEM_ID)
                .orderBy(INFO_LIST_ITEM.ITEM_INDEX)
                .fetchInto(InfoListItemAdminViewDto.class);
    }
}
