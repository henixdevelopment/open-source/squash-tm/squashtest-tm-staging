/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

import static org.squashtest.tm.domain.bdd.ActionWordParameterValue.REGEX_PARAM_VALUE_LINKED_TO_TEST_CASE_PARAM;
import static org.squashtest.tm.domain.bdd.util.ActionWordUtil.isNumber;
import static org.squashtest.tm.domain.bdd.util.ActionWordUtil.removeEscapeCharacters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.text.StringEscapeUtils;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;

public class ActionWordParameterValueDto extends ActionWordFragmentValueDto {

    private static final String EMPTY_QUOTES = "\"\"";
    private static final String QUOTED_VALUE_FORMAT = "\"%s\"";
    private static final String STYLED_VALUE_FORMAT =
            "<span style=\"color: blue; font-family: monospace; font-style: italic;\">%s</span>";

    public ActionWordParameterValueDto(Long keywordStepId, String value) {
        super(keywordStepId, value);
    }

    @Override
    public String getUnstyledAction() {
        if (EMPTY_QUOTES.equals(value) || referencesTestCaseParameter() || isNumber(value)) {
            return value;
        } else {
            return String.format(QUOTED_VALUE_FORMAT, value);
        }
    }

    @Override
    @CleanHtml
    public String getStyledAction() {
        final String withEscapeCharsRemoved = removeEscapeCharacters(value);
        final String htmlValue = StringEscapeUtils.escapeHtml4(withEscapeCharsRemoved);
        return String.format(STYLED_VALUE_FORMAT, htmlValue);
    }

    private boolean referencesTestCaseParameter() {
        Pattern pattern = Pattern.compile(REGEX_PARAM_VALUE_LINKED_TO_TEST_CASE_PARAM);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
}
