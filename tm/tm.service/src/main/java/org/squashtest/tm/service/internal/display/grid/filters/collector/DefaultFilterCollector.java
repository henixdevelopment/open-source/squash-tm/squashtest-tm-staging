/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.filters.collector;

import static java.util.Collections.emptySet;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import org.jooq.Condition;
import org.jooq.Field;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.filters.GridFilterConditionBuilder;

/**
 * Basic implementation of a collector that reduce a list of {@link GridFilterValue} to a single
 * chain of {@link Condition}. Aka it reduce a list of filters to a {@link Condition} like
 * condition1.and(condition2).and(condition3)...
 */
public class DefaultFilterCollector
        implements Collector<GridFilterValue, List<GridFilterValue>, Condition> {

    protected final Map<String, GridColumn> aliasToFieldDictionary;
    protected final boolean searchingOnMultiColumns;

    public DefaultFilterCollector(
            Map<String, GridColumn> aliasToFieldDictionary, boolean searchingOnMultiColumns) {
        this.aliasToFieldDictionary = aliasToFieldDictionary;
        this.searchingOnMultiColumns = searchingOnMultiColumns;
    }

    @Override
    public Supplier<List<GridFilterValue>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<GridFilterValue>, GridFilterValue> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<GridFilterValue>> combiner() {
        return (x, y) -> {
            throw new UnsupportedOperationException();
        };
    }

    @Override
    public Function<List<GridFilterValue>, Condition> finisher() {
        return gridFilterValues -> {
            Queue<GridFilterValue> filterStack = new ArrayDeque<>(gridFilterValues);
            Condition condition = convertFilterToCondition(requireNonNull(filterStack.poll()));
            while (nonNull(filterStack.peek())) {
                if (searchingOnMultiColumns) {
                    condition = condition.or(convertFilterToCondition(filterStack.poll()));
                } else {
                    condition = condition.and(convertFilterToCondition(filterStack.poll()));
                }
            }
            return condition;
        };
    }

    protected Condition convertFilterToCondition(GridFilterValue gridFilterValue) {
        String id = gridFilterValue.getId();
        GridColumn gridColumn = requireNonNull(aliasToFieldDictionary.get(id));
        Field<?> field = gridColumn.getNativeField();
        return GridFilterConditionBuilder.getConditionBuilder(field, gridFilterValue).build();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return emptySet();
    }
}
