/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_GROUP;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_CHILD_ENTITY_DTO_SPRINT_GROUPS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_SPRINT_GROUP_ATTACHMENT_BY_ID;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_SPRINT_GROUP_CONTENT_BY_ID;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_SPRINT_GROUP_FOR_CONTENT_ADDITION;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

import java.util.Collection;
import java.util.List;
import javax.inject.Provider;
import org.hibernate.annotations.QueryHints;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.internal.copier.ChildEntityDtoResult;
import org.squashtest.tm.service.internal.repository.SprintGroupDao;

@Repository
public class HibernateSprintGroupDao extends HibernateEntityDao<SprintGroup>
        implements SprintGroupDao {

    private final DSLContext dslContext;
    private final Provider<HibernateCampaignFolderDao> campaignFolderDaoProvider;
    private final Provider<HibernateSprintDao> sprintDaoProvider;

    @Autowired
    public HibernateSprintGroupDao(
            DSLContext dslContext,
            Provider<HibernateCampaignFolderDao> campaignFolderDaoProvider,
            Provider<HibernateSprintDao> sprintDaoProvider) {
        this.dslContext = dslContext;
        this.campaignFolderDaoProvider = campaignFolderDaoProvider;
        this.sprintDaoProvider = sprintDaoProvider;
    }

    @Override
    public void setRemoteSyncIdsToNull(List<Long> syncIds) {
        dslContext
                .update(SPRINT_GROUP)
                .set(SPRINT_GROUP.REMOTE_SYNCHRONISATION_ID, (Long) null)
                .where(SPRINT_GROUP.REMOTE_SYNCHRONISATION_ID.in(syncIds))
                .execute();
    }

    @UsedInPlugin("Xsquash4jira & Xsquash4GitLab")
    @Override
    public void updateRemoteSyncId(Long clnId, Long syncId) {
        dslContext
                .update(SPRINT_GROUP)
                .set(SPRINT_GROUP.REMOTE_SYNCHRONISATION_ID, syncId)
                .where(SPRINT_GROUP.CLN_ID.eq(clnId))
                .execute();
    }

    @Override
    public Long findRootNodeIdByProjectIdAndName(Long projectId, String rootFolderName) {
        return dslContext
                .select(SPRINT_GROUP.CLN_ID)
                .from(SPRINT_GROUP)
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(SPRINT_GROUP.CLN_ID))
                .innerJoin(CAMPAIGN_LIBRARY_CONTENT)
                .on(CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID.eq(SPRINT_GROUP.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.NAME.eq(rootFolderName))
                .and(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(projectId))
                .fetchOneInto(Long.class);
    }

    @UsedInPlugin("Xsquash4jira & Xsquash4GitLab")
    @Override
    public Long findIdByProjectIdAndRemoteSynchronisationId(
            Long projectId, Long remoteSynchronisationId) {
        return dslContext
                .select(SPRINT_GROUP.CLN_ID)
                .from(SPRINT_GROUP)
                .join(CAMPAIGN_LIBRARY_NODE)
                .on(SPRINT_GROUP.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .join(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(SPRINT_GROUP.REMOTE_SYNCHRONISATION_ID.eq(remoteSynchronisationId))
                .and(PROJECT.PROJECT_ID.eq(projectId))
                .fetchOneInto(Long.class);
    }

    @UsedInPlugin("Xsquash4jira & Xsquash4GitLab")
    @Override
    public List<Long> findIdsByClnIds(List<Long> ids) {
        return dslContext
                .select(SPRINT_GROUP.CLN_ID)
                .from(SPRINT_GROUP)
                .where(SPRINT_GROUP.CLN_ID.in(ids))
                .fetchInto(Long.class);
    }

    public <LN extends LibraryNode> NodeContainer<LN> findByContent(LN node) {
        SetQueryParametersCallback callback = new SetNodeContentParameter(node);
        return executeEntityNamedQuery("sprintGroup.findByContent", callback);
    }

    @Override
    public SprintGroup loadContainerForPaste(long id) {
        return entityManager
                .createQuery(FIND_SPRINT_GROUP_FOR_CONTENT_ADDITION, SprintGroup.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<SprintGroup> loadContainersForPaste(Collection<Long> ids) {
        return entityManager
                .createQuery(FIND_DISTINCT_SPRINT_GROUP_CONTENT_BY_ID, SprintGroup.class)
                .setParameter(IDS, ids)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public List<SprintGroup> loadNodeForPaste(Collection<Long> ids) {
        return entityManager
                .createQuery(FIND_DISTINCT_SPRINT_GROUP_ATTACHMENT_BY_ID, SprintGroup.class)
                .setParameter(IDS, ids)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public ChildEntityDtoResult loadChildForPaste(
            Collection<Long> ids, int maxResult, int offset, ClipboardPayload clipboardPayload) {
        return getChildEntityDtoForPaste(
                FIND_CHILD_ENTITY_DTO_SPRINT_GROUPS_BY_IDS,
                ids,
                maxResult,
                offset,
                clipboardPayload,
                (k, v) -> {
                    switch (k) {
                        case CAMPAIGN_FOLDER -> campaignFolderDaoProvider.get().loadNodeForPaste(v);
                        case SPRINT -> sprintDaoProvider.get().loadNodeForPaste(v);
                        default -> throw new UnsupportedOperationException("Unsupported entity type: " + k);
                    }
                });
    }
}
