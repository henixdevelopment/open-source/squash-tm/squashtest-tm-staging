/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType;
import org.squashtest.tm.domain.environmentvariable.DenormalizedEnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;

public interface DenormalizedEnvironmentVariableDao
        extends JpaRepository<DenormalizedEnvironmentVariable, Long> {

    List<DenormalizedEnvironmentVariable> findAllByHolderIdAndHolderType(
            Long holderId, DenormalizedEnvironmentHolderType holderType);

    List<DenormalizedEnvironmentVariable> findAllByEnvironmentVariable(
            EnvironmentVariable environmentVariable);

    @Query("select dev from DenormalizedEnvironmentVariable dev where dev.id in :ids")
    List<DenormalizedEnvironmentVariable> findAllByIds(@Param("ids") List<Long> ids);
}
