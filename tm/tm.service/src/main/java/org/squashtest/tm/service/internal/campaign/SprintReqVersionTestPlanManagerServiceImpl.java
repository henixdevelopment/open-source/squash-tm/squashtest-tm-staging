/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.IdentifiersOrderComparator;
import org.squashtest.tm.domain.campaign.SprintReqVersion;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.exception.campaign.SprintNotLinkableException;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.campaign.SprintReqVersionTestPlanManagerService;
import org.squashtest.tm.service.display.sprint.SprintDisplayService;
import org.squashtest.tm.service.internal.display.dto.sprint.AvailableTestPlanItemIdsDto;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.testcase.TestCaseNodeWalker;
import org.squashtest.tm.service.security.Authorizations;

@Service("squashtest.tm.service.SprintReqVersionTestPlanManagerService")
@Transactional
public class SprintReqVersionTestPlanManagerServiceImpl
        implements SprintReqVersionTestPlanManagerService {

    private static final String SPRINT_REQ_VERSION_ID = "sprintReqVersionId";

    private final SprintReqVersionDao sprintReqVersionDao;
    private final SprintDisplayService sprintDisplayService;
    private final LibraryNodeDao<TestCaseLibraryNode> testCaseLibraryNodeDao;
    private final TestCaseDao testCaseDao;

    public SprintReqVersionTestPlanManagerServiceImpl(
            SprintReqVersionDao sprintReqVersionDao,
            SprintDisplayService sprintDisplayService,
            @Qualifier("squashtest.tm.repository.TestCaseLibraryNodeDao")
                    LibraryNodeDao<TestCaseLibraryNode> testCaseLibraryNodeDao,
            TestCaseDao testCaseDao) {
        this.sprintReqVersionDao = sprintReqVersionDao;
        this.sprintDisplayService = sprintDisplayService;
        this.testCaseLibraryNodeDao = testCaseLibraryNodeDao;
        this.testCaseDao = testCaseDao;
    }

    @Override
    @PreventConcurrent(entityType = SprintReqVersion.class, paramName = SPRINT_REQ_VERSION_ID)
    @PreAuthorize(Authorizations.LINK_SPRINT_REQ_VERSION_OR_ADMIN)
    public void addTestCaseLibraryNodesToTestPlan(
            @Id Long sprintReqVersionId, List<Long> testCaseIds) {
        final SprintReqVersion sprintReqVersion =
                sprintReqVersionDao.findByIdWithTestPlanAndSprint(sprintReqVersionId);

        if (SprintStatus.CLOSED.equals(sprintReqVersion.getSprint().getStatus())) {
            throw new SprintNotLinkableException();
        }

        addTestCaseLibraryNodesToTestPlan(sprintReqVersion, testCaseIds);
    }

    private void addTestCaseLibraryNodesToTestPlan(
            SprintReqVersion sprintReqVersion, List<Long> tclnIds) {
        // Fetch selected nodes
        final List<TestCaseLibraryNode> nodes = testCaseLibraryNodeDao.findAllByIds(tclnIds);

        // Sort them according to the order in which IDs were provided
        nodes.sort(new IdentifiersOrderComparator(tclnIds));

        // Collect test cases in selection
        final List<TestCase> testCases = new TestCaseNodeWalker().walk(nodes);

        // Add to test plan
        sprintReqVersion.getTestPlan().createAndAddTestPlanItems(testCases);

        // Update auditable fields
        sprintReqVersion.updateLastModificationWithCurrentUser();
    }

    @Override
    public void addTestCasesWithDatasetsToTestPlan(
            Long sprintReqVersionId, Map<Long, List<Long>> datasetIdsByTestCaseId) {
        final SprintReqVersion sprintReqVersion =
                sprintReqVersionDao.findByIdWithTestPlanAndSprint(sprintReqVersionId);

        if (SprintStatus.CLOSED.equals(sprintReqVersion.getSprint().getStatus())) {
            throw new SprintNotLinkableException();
        }

        final Map<TestCase, List<Dataset>> datasetsByTestCase =
                testCaseDao.findTestCaseAndDatasets(datasetIdsByTestCaseId);

        sprintReqVersion.getTestPlan().createAndAddTestPlanItems(datasetsByTestCase);
    }

    @Override
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    public void addAllTestPlanItemsToSprintReqVersion(Long sprintReqVersionId) {
        List<AvailableTestPlanItemIdsDto> availableTestPlanItemIds =
                sprintDisplayService.findAvailableTestPlanItemIds(sprintReqVersionId);
        Map<Long, List<Long>> datasetIdsByTestCaseId = new HashMap<>();
        for (AvailableTestPlanItemIdsDto availableTPI : availableTestPlanItemIds) {
            Long testCaseId = availableTPI.testCaseId();
            if (Objects.isNull(testCaseId)) {
                continue;
            }
            List<Long> datasetIds =
                    datasetIdsByTestCaseId.computeIfAbsent(testCaseId, key -> new ArrayList<>());
            datasetIds.add(availableTPI.datasetId());
        }

        if (!datasetIdsByTestCaseId.isEmpty()) {
            addTestCasesWithDatasetsToTestPlan(sprintReqVersionId, datasetIdsByTestCaseId);
        }
    }
}
