/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCM_REPOSITORY;
import static org.squashtest.tm.jooq.domain.Tables.SCM_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SERVER_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.URL;

import java.util.Arrays;
import java.util.List;
import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

public class ScmServerGrid extends AbstractGrid {

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(SERVER_ID)),
                new GridColumn(DSL.field(NAME)),
                new GridColumn(DSL.field(KIND)),
                new GridColumn(DSL.field(URL)),
                new GridColumn(DSL.isnull(DSL.field(PROJECT_COUNT), 0).as(PROJECT_COUNT)));
    }

    @Override
    protected Table<?> getTable() {
        return DSL.select(
                        THIRD_PARTY_SERVER.SERVER_ID.as(SERVER_ID),
                        THIRD_PARTY_SERVER.NAME.as(NAME),
                        SCM_SERVER.KIND.as(KIND),
                        THIRD_PARTY_SERVER.URL.as(URL),
                        DSL.countDistinct(PROJECT.PROJECT_ID).as(PROJECT_COUNT))
                .from(THIRD_PARTY_SERVER)
                .join(SCM_SERVER)
                .on(SCM_SERVER.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .leftJoin(SCM_REPOSITORY)
                .on(SCM_REPOSITORY.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .leftJoin(PROJECT)
                .on(PROJECT.SCM_REPOSITORY_ID.eq(SCM_REPOSITORY.SCM_REPOSITORY_ID))
                .groupBy(THIRD_PARTY_SERVER.SERVER_ID, SCM_REPOSITORY.SERVER_ID, SCM_SERVER.SERVER_ID)
                .asTable();
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(SERVER_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return DSL.upper(DSL.field(NAME, String.class)).asc();
    }
}
