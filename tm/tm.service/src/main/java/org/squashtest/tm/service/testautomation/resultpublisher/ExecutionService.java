/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus;
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.FailureDetail;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.execution.automatedexecution.AutomatedExecutionFailureDetailService;
import org.squashtest.tm.service.execution.automatedexecution.AutomatedExecutionFlagService;
import org.squashtest.tm.service.internal.dto.AutomatedExecutionUpdateData;
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.FailureDetailDao;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.testautomation.AutomatedExecutionManagerService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState;
import org.squashtest.tm.service.testautomation.model.TfTestExecutionStatus;

@Service
@Transactional
public class ExecutionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionService.class);

    @Inject private ExecutionDao executionDao;

    @Inject private AutomatedSuitePublisherService automatedSuitePublisherService;

    @Inject private AutomatedExecutionManagerService automatedExecutionManager;

    @Inject private AutomatedExecutionFailureDetailService failureDetailService;

    @Inject private AutomatedExecutionExtenderDao automatedExecutionExtenderDao;

    @Inject private PluginFinderService pluginFinderService;

    @Inject private FailureDetailDao failureDetailDao;

    @Inject private AutomatedExecutionFlagService automatedExecutionFlagService;

    @PersistenceContext private EntityManager entityManager;

    public void updateExecutionAndClearSession(
            Long itemId, String newSuiteId, AutomatedExecutionState stateChange) {
        AutomatedExecutionUpdateData executionData =
                executionDao.findAutomatedExecutionUpdateData(itemId, newSuiteId);

        if (executionData == null) {
            throw new NoSuchElementException("No execution found with requested id : " + itemId);
        }

        addAttachments(stateChange, executionData);
        updateExecutionStatus(stateChange, executionData);
        updateExecutionStepsStatuses(stateChange, executionData);
        if (pluginFinderService.isPremiumPluginInstalled()) {
            updateExecutionFailureDetails(stateChange, executionData);
            automatedExecutionFlagService.updateExecutionFlag(stateChange, executionData);
        }

        entityManager.flush();
        entityManager.clear();
    }

    private void addAttachments(
            AutomatedExecutionState stateChange, AutomatedExecutionUpdateData updater) {
        Optional.ofNullable(stateChange.getAttachments())
                .ifPresent(
                        attachments ->
                                attachments.forEach(
                                        attachment ->
                                                automatedSuitePublisherService.updateAttachments(
                                                        updater.attachmentId(), attachment, EntityType.EXECUTION)));
    }

    protected void updateExecutionFailureDetails(
            AutomatedExecutionState stateChange, AutomatedExecutionUpdateData updater) {
        List<String> failureDetailMessages = stateChange.getTfTestExecutionStatus().getFailureDetails();
        if (failureDetailMessages == null || failureDetailMessages.isEmpty()) {
            return;
        }

        List<String> uniqueFailureMessages = failureDetailMessages.stream().distinct().toList();

        AutomatedExecutionExtender extender =
                automatedExecutionExtenderDao.findById(updater.extenderId()).orElseThrow();

        IterationTestPlanItem itpi = extender.getIterationTestPlanItem();

        List<FailureDetail> matchingFailureDetails =
                failureDetailDao.findByItpiIdAndMessageIn(itpi.getId(), uniqueFailureMessages);

        for (String failureDetailMessage : uniqueFailureMessages) {
            failureDetailService.upsertFailureDetail(
                    failureDetailMessage, matchingFailureDetails, extender, itpi);
        }
    }

    private void updateExecutionStatus(
            AutomatedExecutionState stateChange, AutomatedExecutionUpdateData updater) {
        TestExecutionStatus status =
                stateChange.getTfTestExecutionStatus().tfTestExecutionStatusToTmTestExecutionStatus();
        if (shouldUpdateStatus(updater.extenderId(), status)) {
            automatedExecutionManager.changeExecutionState(updater.extenderId(), status);
        }
    }

    private boolean shouldUpdateStatus(Long automExecExtenderId, TestExecutionStatus status) {
        ExecutionStatus executionStatus = status.getStatus();
        String formerExecutionStatus =
                automatedExecutionManager.getExecutionStatus(automExecExtenderId);
        return executionStatus != ExecutionStatus.RUNNING
                || Objects.equals(formerExecutionStatus, "READY");
    }

    private void updateExecutionStepsStatuses(
            AutomatedExecutionState stateChange, AutomatedExecutionUpdateData updater) {
        List<ExecutionStep> executionSteps = executionDao.findSteps(updater.id());
        Map<Integer, TfTestExecutionStatus> testStepExecutionStatuses =
                stateChange.getTestStepExecutionStatuses();
        setStepExecutionStatuses(executionSteps, testStepExecutionStatuses);
    }

    /**
     * Sets the execution statuses of the given execution steps based on the provided test step
     * execution statuses.
     *
     * @param executionSteps the list of execution steps
     * @param testStepExecutionStatuses the map of test step execution statuses
     */
    private void setStepExecutionStatuses(
            List<ExecutionStep> executionSteps,
            Map<Integer, TfTestExecutionStatus> testStepExecutionStatuses) {
        if (shouldPublishStatuses(executionSteps, testStepExecutionStatuses)) {
            for (int i = 0; i < executionSteps.size(); i++) {
                TfTestExecutionStatus tfStepStatus = testStepExecutionStatuses.get(i + 1);
                if (tfStepStatus != null) {
                    executionSteps
                            .get(i)
                            .setExecutionStatus(
                                    org.squashtest.tm.domain.execution.ExecutionStatus.valueOf(
                                            tfStepStatus.getStatus().name()));
                }
            }
        }
    }

    /**
     * Determines whether the statuses should be published based on the given execution steps and test
     * execution statuses.
     *
     * @param executionSteps the list of execution steps
     * @param tfTestExecutionStatuses the map of test execution statuses
     * @return true if the statuses should be published, false otherwise. False if test is not BDD, of
     *     test steps are empty, or autom-publisher send no statuses, or if the number of execution
     *     steps does not match the number of statuses (i.e. the test technology report is wrong)
     */
    private boolean shouldPublishStatuses(
            List<ExecutionStep> executionSteps,
            Map<Integer, TfTestExecutionStatus> tfTestExecutionStatuses) {
        if (tfTestExecutionStatuses == null) {
            return false;
        } else if (executionSteps.isEmpty()) {
            return false;
        } else if (executionSteps.get(0).getReferencedTestStep().getTestCase().getKind()
                != TestCaseKind.KEYWORD) {
            return false;
        } else if (executionSteps.size() != tfTestExecutionStatuses.size()) {
            LOGGER.warn(
                    "Mismatch between the number of test steps in Squash and in the test report: "
                            + "{} steps were defined in Squash, but {} steps statuses were send by Squash Orchestrator. "
                            + "No step status will be published in Squash.",
                    executionSteps.size(),
                    tfTestExecutionStatuses.size());
            return false;
        }
        return true;
    }
}
