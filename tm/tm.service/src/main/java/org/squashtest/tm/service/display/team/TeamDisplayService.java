/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.team;

import java.util.List;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.TeamAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TeamMemberDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

public interface TeamDisplayService {

    GridResponse findAll(GridRequest request);

    TeamAdminViewDto getTeamView(long teamId);

    List<ProjectPermissionDto> getProjectPermissions(Long teamId);

    List<TeamMemberDto> getTeamMembers(Long teamId);

    boolean hasCustomProfile(long teamId);
}
