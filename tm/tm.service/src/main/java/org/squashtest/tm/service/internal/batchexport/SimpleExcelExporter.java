/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport;

import static org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration.CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS;
import static org.squashtest.tm.service.grid.ColumnIds.COLUMN_IDS_LINKED_TO_MILESTONES;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.feature.FeatureManager.Feature;
import org.squashtest.tm.service.grid.ColumnIds;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;
import org.squashtest.tm.service.internal.batchexport.models.TestCaseModel;
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet;
import org.squashtest.tm.service.internal.dto.NumericCufHelper;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.project.ProjectFinder;

/**
 * @author bflessel
 */
@Component
@Scope("prototype")
class SimpleExcelExporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleExcelExporter.class);

    private static final String DATA_EXCEED_MAX_CELL_SIZE_MESSAGE =
            "some data exceed the maximum size of an excel cell";
    private static final String TC_SHEET = TemplateWorksheet.TEST_CASES_SHEET.sheetName;
    private static final String STATUS = "test-case.status.";
    private static final String IMPORTANCE = "test-case.importance.";
    private static final String AUTOMATABLE = "test-case.automatable.";
    private static final String REQUEST_STATUS = "automation-request.request_status.";
    private static final String LABEL_NO = "label.No";
    private static final String LABEL_YES = "label.Yes";

    List<ColumnIds> customColumnOrder =
            new ArrayList<>(
                    Arrays.asList(
                            ColumnIds.PROJECT_NAME,
                            ColumnIds.ID,
                            ColumnIds.REFERENCE,
                            ColumnIds.NAME,
                            ColumnIds.TEST_CASE_STATUS,
                            ColumnIds.TEST_CASE_IMPORTANCE,
                            ColumnIds.TEST_CASE_NATURE,
                            ColumnIds.TEST_CASE_TYPE,
                            ColumnIds.TEST_CASE_AUTOMATION_INDICATOR,
                            ColumnIds.CREATED_BY,
                            ColumnIds.LAST_MODIFIED_BY));

    List<ColumnIds> optionalColumns =
            new ArrayList<>(
                    Arrays.asList(
                            ColumnIds.MILESTONES,
                            ColumnIds.NUMBER_OF_ATTACHMENTS,
                            ColumnIds.COVERAGE_OF_ASSOCIATED_REQUIREMENTS,
                            ColumnIds.NUMBER_OF_TEST_STEPS,
                            ColumnIds.DATASETS,
                            ColumnIds.NUMBER_OF_ASSOCIATED_ITERATIONS));

    List<ColumnIds> premiumColumnList =
            new ArrayList<>(
                    Arrays.asList(
                            ColumnIds.PROJECT_NAME,
                            ColumnIds.ID,
                            ColumnIds.REFERENCE,
                            ColumnIds.NAME,
                            ColumnIds.TEST_CASE_STATUS,
                            ColumnIds.TEST_CASE_IMPORTANCE,
                            ColumnIds.TEST_CASE_NATURE,
                            ColumnIds.TEST_CASE_TYPE,
                            ColumnIds.TEST_CASE_KIND,
                            ColumnIds.DESCRIPTION,
                            ColumnIds.TEST_CASE_AUTOMATION_INDICATOR,
                            ColumnIds.TEST_CASE_AUTOMATION_PRIORITY,
                            ColumnIds.TRANSMITTED_ON,
                            ColumnIds.REQUEST_STATUS,
                            ColumnIds.TEST_CASE_HASAUTOSCRIPT,
                            ColumnIds.AUTOMATED_TEST_TECHNOLOGY,
                            ColumnIds.TEST_CASE_HAS_BOUND_SCM_REPOSITORY,
                            ColumnIds.TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE,
                            ColumnIds.MILESTONES_LABELS,
                            ColumnIds.MILESTONES_STATUS,
                            ColumnIds.MILESTONES_END_DATE,
                            ColumnIds.CREATED_ON,
                            ColumnIds.CREATED_BY,
                            ColumnIds.LAST_MODIFIED_ON,
                            ColumnIds.LAST_MODIFIED_BY,
                            ColumnIds.MILESTONES,
                            ColumnIds.NUMBER_OF_ATTACHMENTS,
                            ColumnIds.COVERAGE_OF_ASSOCIATED_REQUIREMENTS,
                            ColumnIds.NUMBER_OF_TEST_STEPS,
                            ColumnIds.PARAM_COUNT,
                            ColumnIds.DATASETS,
                            ColumnIds.CALL_STEP_COUNT,
                            ColumnIds.NUMBER_OF_ASSOCIATED_ITERATIONS,
                            ColumnIds.EXECUTION_COUNT,
                            ColumnIds.ISSUE_COUNT,
                            ColumnIds.DRAFTED_BY_AI));

    protected Workbook workbook;

    protected boolean milestonesEnabled;

    private MessageSource messageSource;

    private String errorCellTooLargeMessage;

    private Integer projectsAllowWorkflow;

    @Inject private ProjectFinder projectFinder;

    private final GridConfigurationService gridConfigurationService;

    private final PluginFinderService pluginFinderService;

    private final Map<Long, Integer> cufColumnsById = new HashMap<>();

    public SimpleExcelExporter(
            FeatureManager featureManager,
            MessageSource messageSource,
            GridConfigurationService gridConfigurationService,
            PluginFinderService pluginFinderService) {
        super();
        milestonesEnabled = featureManager.isEnabled(Feature.MILESTONE);
        this.gridConfigurationService = gridConfigurationService;
        this.pluginFinderService = pluginFinderService;
        getMessageSource(messageSource);
    }

    @PostConstruct // So these methods are not called directly by constructor
    public void init() {
        createWorkbook();
        getProjectsAllowWorkflow();
    }

    void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
        errorCellTooLargeMessage =
                this.messageSource.getMessage(
                        "test-case.export.errors.celltoolarge", null, LocaleContextHolder.getLocale());
    }

    public void simpleAppendToWorkbook(
            ExportModel model, boolean keepRteFormat, boolean simplifiedColumnDisplayForTest) {

        if (!keepRteFormat) {
            removeRteSimpleFormat(model);
        }
        appendSimpleTestCases(model, simplifiedColumnDisplayForTest);
    }

    private void removeRteSimpleFormat(ExportModel model) {
        removeRteFormatFromTestCases(model.getTestCases());
    }

    private void removeRteFormatFromTestCases(List<TestCaseModel> testCases) {
        for (TestCaseModel tc : testCases) {
            tc.setDescription(removeHtml(tc.getDescription()));
            tc.setPrerequisite(removeHtml(tc.getPrerequisite()));
            for (CustomField cf : tc.getCufs()) {
                cf.setValue(removeHtml(cf.getValue()));
            }
        }
    }

    private String removeHtml(String html) {
        if (StringUtils.isBlank(html)) {
            return "";
        }
        return html.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", "");
    }

    public File print() {
        try {
            File temp = File.createTempFile("tc_export_", "xls");
            temp.deleteOnExit();

            FileOutputStream fos = new FileOutputStream(temp);
            workbook.write(fos);
            fos.close();

            return temp;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void appendSimpleTestCases(ExportModel model, boolean simplifiedColumnDisplayForTest) {
        List<TestCaseModel> testCaseModels = model.getTestCases();
        Sheet testCaseSheet = workbook.getSheet(TC_SHEET);
        int rowIndex = testCaseSheet.getLastRowNum() + 1;
        List<String> activeColumns =
                gridConfigurationService.findActiveColumnIdsForUser("test-case-search");

        List<CustomField> allCufs =
                testCaseModels.stream()
                        .flatMap(testCaseModel -> testCaseModel.getCufs().stream())
                        .sorted(Comparator.comparing((CustomField cuf) -> cuf.getLabel().toLowerCase()))
                        .toList();

        for (TestCaseModel testCaseModel : testCaseModels) {
            Row row = testCaseSheet.createRow(rowIndex);

            try {
                insertValues(row, testCaseModel, activeColumns, simplifiedColumnDisplayForTest);
                appendCustomFields(row, testCaseModel.getCufs(), activeColumns, allCufs);
                rowIndex++;
            } catch (IllegalArgumentException e) {

                LOGGER.warn(
                        "cannot export content for test case '{}': {}",
                        testCaseModel.getId(),
                        DATA_EXCEED_MAX_CELL_SIZE_MESSAGE);
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("", e);
                }
                testCaseSheet.removeRow(row);
                row = testCaseSheet.createRow(rowIndex);

                row.createCell(0).setCellValue(errorCellTooLargeMessage);
            }
        }
    }

    // for now we care only of Excel 2003
    private void createWorkbook() {
        Workbook wb = new HSSFWorkbook();
        wb.createSheet(TC_SHEET);
        this.workbook = wb;
    }

    public void createHeaders(boolean simplifiedColumnDisplayForTest) {
        Sheet dsSheet = workbook.getSheet(TC_SHEET);
        Row h = dsSheet.createRow(0);
        int cIdx = 0;
        List<String> activeColumns =
                gridConfigurationService.findActiveColumnIdsForUser("test-case-search");
        List<String> columnsToProcess = columnList(activeColumns, simplifiedColumnDisplayForTest);
        Map<String, String> customLabels = createColumnIdsMap();
        for (String columnName : columnsToProcess) {
            createTcSimpleSheetHeadersForActiveColumns(h, cIdx, columnName, customLabels);
            cIdx++;
        }
    }

    private List<String> columnList(
            List<String> activeColumns, boolean simplifiedColumnDisplayForTest) {

        List<ColumnIds> columnsToProcess = new ArrayList<>(customColumnOrder);
        List<ColumnIds> initialList =
                genericColumnList(activeColumns, columnsToProcess, simplifiedColumnDisplayForTest);

        List<String> columnIdStrings =
                initialList.stream().map(ColumnIds::getColumnId).collect(Collectors.toList());
        if (pluginFinderService.isPremiumPluginInstalled() && !activeColumns.isEmpty()) {
            columnIdStrings.retainAll(activeColumns);

            activeColumns.forEach(
                    column -> {
                        if (column.startsWith(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS)) {
                            columnIdStrings.add(column);
                        }
                    });
        }
        return columnIdStrings;
    }

    private List<ColumnIds> genericColumnList(
            List<String> activeColumns,
            List<ColumnIds> columnsToProcess,
            boolean simplifiedColumnDisplayForTest) {

        if (!pluginFinderService.isPremiumPluginInstalled()) {
            if (!simplifiedColumnDisplayForTest) {
                columnsToProcess.addAll(optionalColumns);
            }
            columnsToProcess.add(ColumnIds.DRAFTED_BY_AI);
        } else if (!activeColumns.isEmpty()) {
            columnsToProcess = premiumColumnList;
        }
        if (projectsAllowWorkflow <= 0) {
            columnsToProcess.remove(ColumnIds.TEST_CASE_AUTOMATION_INDICATOR);
        }
        if (!milestonesEnabled) {
            columnsToProcess.removeAll(COLUMN_IDS_LINKED_TO_MILESTONES);
        }
        return columnsToProcess;
    }

    private void createTcSimpleSheetHeadersForActiveColumns(
            Row h, int cIdx, String activeColumn, Map<String, String> customLabels) {
        if (customLabels.containsKey(activeColumn)) {
            h.createCell(cIdx).setCellValue(getMessage(customLabels.get(activeColumn)));
        } else if (activeColumn.startsWith(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS)) {
            gridConfigurationService.registerCuf(h, cIdx, activeColumn, cufColumnsById);
        }
    }

    private Map<String, String> createColumnIdsMap() {
        Map<String, String> customLabels = new HashMap<>();
        List<ColumnIds> columnsToProcess = new ArrayList<>(premiumColumnList);

        if (!milestonesEnabled) {
            columnsToProcess.removeAll(COLUMN_IDS_LINKED_TO_MILESTONES);
        }
        if (projectsAllowWorkflow <= 0) {
            columnsToProcess.remove(ColumnIds.TEST_CASE_AUTOMATION_INDICATOR);
        }
        for (ColumnIds columnName : columnsToProcess) {
            putCustomLabel(customLabels, columnName);
        }
        return customLabels;
    }

    private void putCustomLabel(Map<String, String> customLabels, ColumnIds columnId) {
        customLabels.put(columnId.getColumnId(), columnId.getLabel());
    }

    private void insertValues(
            Row row,
            TestCaseModel tcm,
            List<String> activeColumns,
            Boolean simplifiedColumnDisplayForTest) {
        Map<String, String> fieldValues = new HashMap<>();
        fieldValues.put("Importance", handleMessages(IMPORTANCE + tcm.getWeight().toString()));
        fieldValues.put("Nature", handleMessages(tcm.getNature().getLabel()));
        fieldValues.put("Type", handleMessages(tcm.getType().getLabel()));
        fieldValues.put("Status", handleMessages(STATUS + tcm.getStatus().toString()));
        fieldValues.put("Automatable", handleMessages(AUTOMATABLE + tcm.getAutomatable().name()));
        fieldValues.put(
                "Description", handleMessages("".equals(tcm.getDescription()) ? LABEL_NO : LABEL_YES));
        fieldValues.put(
                "RequestStatus",
                tcm.getAutomationRequestStatus() != null
                        ? handleMessages(REQUEST_STATUS + tcm.getAutomationRequestStatus())
                        : "");
        fieldValues.put(
                "HasAutoScript", handleMessages(tcm.getHasAutoScript() != null ? LABEL_YES : LABEL_NO));
        fieldValues.put(
                "AutomatedTestTechno",
                tcm.getAutomatedTestTechno() != null
                        ? tcm.getAutomatedTestTechno()
                        : handleMessages("search.requirement.association.childRequirement.none"));
        fieldValues.put(
                "HasBoundScmRepository",
                handleMessages(tcm.getHasBoundScmRepository() != null ? LABEL_YES : LABEL_NO));
        fieldValues.put(
                "HasBoundAutomatedTestReference",
                handleMessages(tcm.getHasBoundAutomatedTestReference() != null ? LABEL_YES : LABEL_NO));
        fieldValues.put("DraftedByAi", handleMessages(tcm.getDraftedByAi() ? LABEL_YES : LABEL_NO));

        Map<String, BiConsumer<Row, Integer>> columnValueHandlers =
                createColumnValueHandlers(tcm, fieldValues);
        List<String> columnsToProcess = columnList(activeColumns, simplifiedColumnDisplayForTest);

        int cIdx = 0;
        for (String columnName : columnsToProcess) {
            if (columnValueHandlers.containsKey(columnName)) {
                columnValueHandlers.get(columnName).accept(row, cIdx);
            }
            cIdx++;
        }
    }

    private Map<String, BiConsumer<Row, Integer>> createColumnValueHandlers(
            TestCaseModel tcm, Map<String, String> fieldValues) {
        Map<String, BiConsumer<Row, Integer>> handlers = new HashMap<>();
        putCustomHandlers(handlers, ColumnIds.PROJECT_NAME, tcm.getProjectName());
        putCustomHandlers(handlers, ColumnIds.ID, tcm.getId());
        putCustomHandlers(handlers, ColumnIds.REFERENCE, tcm.getReference());
        putCustomHandlers(handlers, ColumnIds.NAME, tcm.getName());
        putCustomHandlers(handlers, ColumnIds.TEST_CASE_STATUS, fieldValues.get("Status"));
        putCustomHandlers(handlers, ColumnIds.TEST_CASE_IMPORTANCE, fieldValues.get("Importance"));
        putCustomHandlers(handlers, ColumnIds.TEST_CASE_NATURE, fieldValues.get("Nature"));
        putCustomHandlers(handlers, ColumnIds.TEST_CASE_TYPE, fieldValues.get("Type"));
        putCustomHandlers(
                handlers,
                ColumnIds.CREATED_ON,
                gridConfigurationService.reformatSimpleDateForExport(tcm.getCreatedOn()));
        putCustomHandlers(handlers, ColumnIds.CREATED_BY, tcm.getCreatedBy());
        putCustomHandlers(handlers, ColumnIds.LAST_MODIFIED_BY, tcm.getLastModifiedBy());
        putCustomHandlers(handlers, ColumnIds.NUMBER_OF_ATTACHMENTS, tcm.getNbAttachments());
        putCustomHandlers(handlers, ColumnIds.DATASETS, tcm.getDatasetCount());
        putCustomHandlers(handlers, ColumnIds.NUMBER_OF_TEST_STEPS, tcm.getTestStepCount());
        putCustomHandlers(handlers, ColumnIds.COVERAGE_OF_ASSOCIATED_REQUIREMENTS, tcm.getNbReq());
        putCustomHandlers(handlers, ColumnIds.NUMBER_OF_ASSOCIATED_ITERATIONS, tcm.getNbIterations());
        putCustomHandlers(handlers, ColumnIds.PARAM_COUNT, tcm.getParameter());
        putCustomHandlers(handlers, ColumnIds.EXECUTION_COUNT, tcm.getExecutionCount());
        putCustomHandlers(
                handlers, ColumnIds.TEST_CASE_KIND, handleMessages(tcm.getTestCaseKind().getI18nKey()));
        putCustomHandlers(handlers, ColumnIds.DESCRIPTION, fieldValues.get("Description"));
        putCustomHandlers(handlers, ColumnIds.TEST_CASE_AUTOMATION_PRIORITY, tcm.getPriority());
        putCustomHandlers(
                handlers,
                ColumnIds.TRANSMITTED_ON,
                gridConfigurationService.reformatSimpleDateForExport(tcm.getTransmissionDate()));
        putCustomHandlers(handlers, ColumnIds.REQUEST_STATUS, fieldValues.get("RequestStatus"));
        putCustomHandlers(
                handlers, ColumnIds.TEST_CASE_HASAUTOSCRIPT, fieldValues.get("HasAutoScript"));
        putCustomHandlers(
                handlers, ColumnIds.AUTOMATED_TEST_TECHNOLOGY, fieldValues.get("AutomatedTestTechno"));
        putCustomHandlers(
                handlers,
                ColumnIds.TEST_CASE_HAS_BOUND_SCM_REPOSITORY,
                fieldValues.get("HasBoundScmRepository"));
        putCustomHandlers(
                handlers,
                ColumnIds.TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE,
                fieldValues.get("HasBoundAutomatedTestReference"));
        putCustomHandlers(
                handlers,
                ColumnIds.LAST_MODIFIED_ON,
                gridConfigurationService.reformatSimpleDateForExport(tcm.getLastModifiedOn()));
        putCustomHandlers(handlers, ColumnIds.CALL_STEP_COUNT, tcm.getCalledStepCount());
        putCustomHandlers(handlers, ColumnIds.ISSUE_COUNT, tcm.getIssueCount());
        if (milestonesEnabled) {
            putCustomHandlers(
                    handlers,
                    ColumnIds.MILESTONES_LABELS,
                    gridConfigurationService.reformatLabelForExport(tcm.getMilestone()));
            putCustomHandlers(
                    handlers, ColumnIds.MILESTONES_STATUS, reformatStatusForExport(tcm.getMilestoneStatus()));
            putCustomHandlers(
                    handlers,
                    ColumnIds.MILESTONES_END_DATE,
                    gridConfigurationService.reformatMultipleDateForExport(tcm.getMilestoneEndDate()));
            putCustomHandlers(handlers, ColumnIds.MILESTONES, tcm.getMilestonesCount());
        }
        handlers.put(
                ColumnIds.TEST_CASE_AUTOMATION_INDICATOR.getColumnId(),
                (row, colIndex) -> {
                    if (projectsAllowWorkflow > 0) {
                        if (tcm.getAllowAutomationWorkflow()) {
                            row.createCell(colIndex).setCellValue(fieldValues.get("Automatable"));
                        } else {
                            row.createCell(colIndex).setCellValue("-");
                        }
                    }
                });
        putCustomHandlers(handlers, ColumnIds.DRAFTED_BY_AI, fieldValues.get("DraftedByAi"));
        return handlers;
    }

    private void appendCustomFields(
            Row row, List<CustomField> cufs, List<String> activeColumns, List<CustomField> allCufs) {
        for (CustomField allCuf : allCufs) {
            Long cufId = allCuf.getCufId();
            String columnName = GridColumnDisplayConfiguration.getCufColumnNameFromCufId(cufId);

            if (activeColumns.contains(columnName)) {
                Integer index = cufColumnsById.get(cufId);
                if (index != null) {
                    setCufCellValue(row, cufs, cufId, index);
                }
            }
        }
    }

    private void setCufCellValue(Row row, List<CustomField> cufs, Long cufId, Integer index) {
        CustomField cuf = gridConfigurationService.findCufById(cufs, cufId, index);
        if (cuf != null) {
            setCellValue(cuf, index, row);
        }
    }

    private void setCellValue(CustomField cuf, Integer idx, Row r) {
        Cell c = r.createCell(idx);
        String value = gridConfigurationService.nullSafeValue(cuf);
        switch (cuf.getType()) {
            case NUMERIC:
                value = NumericCufHelper.formatOutputNumericCufValue(value);
                break;
            case CHECKBOX:
                value = handleMessages("label." + StringUtils.capitalize(value));
                break;
            case TAG:
                value = gridConfigurationService.reformatLabelForExport(value);
                break;
            default:
                break;
        }
        c.setCellValue(value);
    }

    private void putCustomHandlers(
            Map<String, BiConsumer<Row, Integer>> handlers, ColumnIds columnId, Object value) {
        handlers.put(
                columnId.getColumnId(),
                (row, colIndex) -> {
                    Cell cell = row.createCell(colIndex);
                    if (value instanceof String) {
                        cell.setCellValue((String) value);
                    } else if (value instanceof Integer) {
                        cell.setCellValue((Integer) value);
                    } else if (value instanceof Long) {
                        cell.setCellValue((Long) value);
                    } else if (value instanceof Date) {
                        cell.setCellValue((Date) value);
                    }
                });
    }

    private String reformatStatusForExport(String concatenatedStatus) {
        List<String> formattedStatus = new ArrayList<>();
        if (StringUtils.isNotBlank(concatenatedStatus)) {
            String[] statuses = concatenatedStatus.split("\\|");
            formattedStatus =
                    Arrays.stream(statuses).map(st -> handleMessages("milestone.status." + st)).toList();
        }
        return String.join(", ", formattedStatus);
    }

    public void getMessageSource(MessageSource source) {
        this.messageSource = source;
    }

    public Integer getProjectsAllowWorkflow() {
        projectsAllowWorkflow = projectFinder.countProjectsAllowAutomationWorkflow();
        return projectsAllowWorkflow;
    }

    private String getMessage(String key) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(key, null, locale);
    }

    private String handleMessages(String key) {
        try {
            return getMessage(key);
        } catch (NoSuchMessageException e) {
            LOGGER.debug("No corresponding message for key: {}", key, e);
            return key;
        }
    }
}
