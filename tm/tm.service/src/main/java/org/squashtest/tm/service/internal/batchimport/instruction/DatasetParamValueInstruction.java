/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.instruction;

import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.service.internal.batchimport.DatasetValue;
import org.squashtest.tm.service.internal.batchimport.Facility;
import org.squashtest.tm.service.internal.batchimport.LogTrain;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;

public class DatasetParamValueInstruction extends Instruction<DatasetTarget> {

    private final DatasetValue datasetValue;

    private ParameterTarget parameterTarget;

    public DatasetParamValueInstruction(
            @NotNull DatasetTarget target, @NotNull DatasetValue datasetValue) {
        super(target);
        this.datasetValue = datasetValue;
    }

    public void initParameterTarget() {
        ParameterTarget paramTarget = new ParameterTarget();
        setParameterOwnerPath(paramTarget);
        paramTarget.setName(datasetValue.getParameterName());

        this.parameterTarget = paramTarget;
    }

    private void setParameterOwnerPath(ParameterTarget parameterTarget) {
        String parameterOwnerPath = datasetValue.getParameterOwnerPath();
        if (StringUtils.isBlank(parameterOwnerPath)) {
            parameterTarget.setPath(getTarget().getTestCase().getPath());
        } else {
            parameterTarget.setPath(parameterOwnerPath);
        }
    }

    /**
     * @return the datasetParamValue
     */
    public DatasetValue getDatasetValue() {
        return datasetValue;
    }

    public ParameterTarget getParameterTarget() {
        return parameterTarget;
    }

    /**
     * @see Instruction#executeUpdate(org.squashtest.tm.service.internal.batchimport.Facility)
     */
    @Override
    protected LogTrain executeUpdate(Facility facility) {

        return facility.failsafeUpdateParameterValue(
                getTarget(), parameterTarget, datasetValue.getValue(), true);
    }

    /**
     * @see Instruction#executeDelete(org.squashtest.tm.service.internal.batchimport.Facility)
     */
    @Override
    protected LogTrain executeDelete(Facility facility) {

        /*
         * NOOP : As for TM 1.11.0 the DATASET sheet is now split in two phases : the datasets and the parameter values
         * are now treated in two distinct phases. Please see Facility#ENTITIES_ORDERED_BY_INSTRUCTION_ORDER to see
         * how.
         *
         * In particular the deletion of a dataset now happens in the DATASET phase so we don't double process it in the
         * param value phase.
         */
        return new LogTrain();
    }

    /**
     * @see Instruction#executeCreate(org.squashtest.tm.service.internal.batchimport.Facility)
     */
    @Override
    protected LogTrain executeCreate(Facility facility) {
        throw new UnsupportedOperationException("This operation is no longer supported ");
    }
}
