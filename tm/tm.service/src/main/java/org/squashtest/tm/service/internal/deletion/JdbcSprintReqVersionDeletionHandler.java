/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import javax.persistence.EntityManager;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.api.repository.HibernateSessionClearing;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;
import org.squashtest.tm.service.internal.deletion.jdbc.AbstractSprintReqVersionDeletionHandler;
import org.squashtest.tm.service.internal.deletion.jdbc.JdbcBatchReorderHelper;

public class JdbcSprintReqVersionDeletionHandler extends AbstractSprintReqVersionDeletionHandler {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(JdbcSprintReqVersionDeletionHandler.class);

    private final Collection<Long> sprintReqVersionIds;

    protected JdbcSprintReqVersionDeletionHandler(
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper,
            String operationId,
            Collection<Long> sprintReqVersionIds) {
        super(dslContext, entityManager, attachmentRepository, reorderHelper, operationId);
        this.sprintReqVersionIds = sprintReqVersionIds;
    }

    @HibernateSessionClearing
    public void deleteSprintRequirementVersions() {
        logStartProcess();
        clearPersistenceContext();
        storeEntitiesToDeleteIntoWorkingTable();
        performDeletions();
        cleanWorkingTable();
        logEndProcess();
    }

    private void logStartProcess() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    String.format(
                            "Init deletion process of sprint requirement versions %s. Operation:  %s",
                            sprintReqVersionIds, operationId));
        }
    }

    private void storeEntitiesToDeleteIntoWorkingTable() {
        storeExecutionsToDeleteIntoWorkingTable();
        addSprintReqVersions();
        addSprintReqVersionsTestPlans();
        addSprintReqVersionsTestPlanItems();
        addSprintRequirementSyncExtenders();
        addCustomFieldValues();
        addAttachmentList();
        logReferenceEntitiesComplete();
    }

    private void performDeletions() {
        performExecutionDeletions();
        deleteSprintVersionSyncExtenders();
        deleteSprintReqVersionTestPlansItems();
        deleteSprintReqVersions();
        deleteSprintReqVersionTestPlans();
        deleteCustomFieldValues();
        deleteAttachmentLists();
        deleteAttachmentContents();
    }

    private void logEndProcess() {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(
                    String.format(
                            "Deleted sprint requirement versions %s. Time elapsed %s",
                            sprintReqVersionIds, startDate.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
        }
    }

    @Override
    protected SelectConditionStep<Record3<Long, String, String>>
            selectSprintRequirementSyncExtenders() {
        return makeSelectClause(SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_SYNC_ID)
                .from(SPRINT_REQUIREMENT_SYNC_EXTENDER)
                .where(SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_VERSION_ID.in(sprintReqVersionIds));
    }

    @Override
    protected Condition getSprintReqVersionPredicate() {
        return getPredicate();
    }

    @Override
    protected Condition getPredicate() {
        return SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.in(sprintReqVersionIds);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
