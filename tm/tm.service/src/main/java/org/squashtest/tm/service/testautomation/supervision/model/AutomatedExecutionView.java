/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.supervision.model;

import org.squashtest.tm.domain.execution.ExecutionStatus;

/**
 * !Careful: the id can represent different things according to the execution method. Using TF
 * execution, it represents the id of the execution, but using squash-autom plugin it represents the
 * id of the Itpi holding the execution. This comes from the different execution creation mechanism,
 * in TF use case, we create all the executions immediately, whereas in SquashAutom they are created
 * later when the ExpectedSuiteDefinition is received by the tm-publisher service.
 */
public interface AutomatedExecutionView {

    Long id();

    String name();

    ExecutionStatus status();

    String automatedServerName();
}
