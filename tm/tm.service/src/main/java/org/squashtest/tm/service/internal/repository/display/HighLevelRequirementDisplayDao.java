/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto.LinkedLowLevelRequirementDto;

public interface HighLevelRequirementDisplayDao {
    List<LinkedLowLevelRequirementDto> findLinkedLowLevelRequirements(Long requirementId);

    /**
     * Retrieve all requirement ids by given library ids. If the scope is extended for high level
     * requirements in selection, it will also retrieve their linked low level requirements in other
     * libraries.
     *
     * @param readLibIds the selected library ids
     * @param isExtendedHighLvlReqScope the boolean to know if the scope is extended for high level
     *     requirement
     * @return requirement ids
     */
    Set<Long> findRequirementIdsByLibraryIds(
            Collection<Long> readLibIds, boolean isExtendedHighLvlReqScope);

    Set<Long> findRequirementIdsByNodeIds(
            Collection<Long> readNodeIds, boolean isExtendedHighLvlReqScope);

    Set<Long> findStandardRequirementsByHighLvlReqId(Long highLevelRequirementId);

    Set<Long> findStandardRequirementsByRequirementIdsAndProjectIds(
            List<Long> requirementIds, List<Long> projectIds);

    Set<Long> findLinkedLowLevelReqVersionIdsByReqVersionIdsAndProjectIds(
            List<Long> requirementVersionIds, List<Long> projectIds);

    Map<Long, List<Long>> findAllLinkedLowLevelReqIdsMappedByHighLevelReqIdFromVersionIds(
            List<Long> versionIds);
}
