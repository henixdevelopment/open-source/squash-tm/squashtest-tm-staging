/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customfield;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.Paging;
import org.squashtest.tm.core.foundation.collection.PagingBackedPagedCollectionHolder;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.event.CreateCustomFieldBindingEvent;
import org.squashtest.tm.event.DeleteCustomFieldBindingEvent;
import org.squashtest.tm.exception.project.LockedParameterException;
import org.squashtest.tm.service.customfield.CustomFieldBindingModificationService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomFieldCustomExportDto;
import org.squashtest.tm.service.internal.dto.CustomFieldBindingModel;
import org.squashtest.tm.service.internal.repository.CustomFieldBindingDao;
import org.squashtest.tm.service.internal.repository.CustomFieldDao;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.SecurityCheckableObject;

@Service("squashtest.tm.service.CustomFieldBindingService")
@Transactional
public class CustomFieldBindingModificationServiceImpl
        implements CustomFieldBindingModificationService {

    private static final Transformer BINDING_ID_COLLECTOR =
            new Transformer() {
                @Override
                public Object transform(Object input) {
                    return ((CustomFieldBinding) input).getId();
                }
            };

    @Inject private CustomFieldDao customFieldDao;
    @Inject private CustomFieldBindingDao customFieldBindingDao;
    @Inject private PrivateCustomFieldValueService customValueService;
    @Inject private GenericProjectDao genericProjectDao;
    @Inject private ProjectDao projectDao;
    @Inject private ApplicationEventPublisher eventPublisher;
    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Override
    @Transactional(readOnly = true)
    public List<CustomField> findAvailableCustomFields() {
        return customFieldDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomField> findAvailableCustomFields(long projectId, BindableEntity entity) {
        return customFieldDao.findAllBindableCustomFields(projectId, entity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomField> findBoundCustomFields(long projectId, BindableEntity entity) {
        return customFieldDao.findAllBoundCustomFields(projectId, entity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomFieldBinding> findCustomFieldsForGenericProject(long projectId) {
        return customFieldBindingDao.findAllForGenericProject(projectId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomFieldBinding> findCustomFieldsForProjectAndEntity(
            long projectId, BindableEntity entity) {
        return customFieldBindingDao.findAllForProjectAndEntity(projectId, entity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomFieldBinding> findCustomFieldsForBoundEntity(BoundEntity boundEntity) {
        return customFieldBindingDao.findAllForProjectAndEntity(
                boundEntity.getProject().getId(), boundEntity.getBoundEntityType());
    }

    @Override
    @Transactional(readOnly = true)
    public PagedCollectionHolder<List<CustomFieldBinding>> findCustomFieldsForProjectAndEntity(
            long projectId, BindableEntity entity, Paging paging) {

        List<CustomFieldBinding> bindings =
                customFieldBindingDao.findAllForProjectAndEntity(projectId, entity, paging);
        Long count = customFieldBindingDao.countAllForProjectAndEntity(projectId, entity);

        return new PagingBackedPagedCollectionHolder<>(paging, count, bindings);
    }

    @Override
    public Map<Long, String> findCustomFieldLabelsByIdsFromProjectIds(List<Long> projectIds) {

        Map<Long, String> availableCufMap = new HashMap<>();

        List<CustomFieldCustomExportDto> availableExportCustomFields =
                customValueService.findAllAvailableCufsForCustomExport(projectIds);

        availableExportCustomFields.forEach(
                availableCuf -> appendAvailableCustomFields(availableCufMap, availableCuf));
        return availableCufMap;
    }

    private void appendAvailableCustomFields(
            Map<Long, String> availableCufMap, CustomFieldCustomExportDto availableCuf) {
        if (Objects.nonNull(availableCuf.getCufId())) {
            availableCufMap.put(availableCuf.getCufId(), availableCuf.getLabel());
        } else {
            throw new NullPointerException(
                    "Custom field id cannot be null in CustomFieldCustomExportDto entity.");
        }
    }

    @Override
    public void createNewBindings(CustomFieldBindingModel[] bindingModels) {
        if (genericProjectDao.isBoundToATemplate(bindingModels[0].getProjectId())) {
            throw new LockedParameterException();
        }

        GenericProject project =
                genericProjectDao.findById(bindingModels[0].getProjectId()).orElseThrow();
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                new SecurityCheckableObject(project, Permissions.MANAGE_PROJECT.name()));

        for (CustomFieldBindingModel model : bindingModels) {
            long cufId = model.getCustomField().getId();
            BindableEntity entity = model.getBoundEntity().toDomain();
            CustomFieldBinding newBinding = createBinding(project, entity, cufId);

            if (!genericProjectDao.isProjectTemplate(project.getId())) {
                customValueService.cascadeCustomFieldValuesCreation(newBinding);
            } else {
                propagateCufBindingCreationToBoundProjects(project, entity, cufId);
            }
        }
    }

    private void propagateCufBindingCreationToBoundProjects(
            GenericProject project, BindableEntity entity, long cufId) {
        Collection<Long> boundProjectsIds = projectDao.findAllIdsBoundToTemplate(project.getId());
        for (Long boundProjectId : boundProjectsIds) {
            if (!customFieldBindingDao.cufBindingAlreadyExists(boundProjectId, entity, cufId)) {
                addNewCustomFieldBinding(boundProjectId, entity, cufId);
            }
        }
    }

    @Override
    public void addNewCustomFieldBinding(long projectId, BindableEntity entity, long customFieldId) {
        addNewCustomFieldBindingUnsecured(projectId, entity, customFieldId);
    }

    @Override
    public void addNewCustomFieldBindingUnsecured(
            long projectId, BindableEntity entity, long customFieldId) {
        GenericProject genericProject = genericProjectDao.findById(projectId).orElseThrow();
        CustomFieldBinding newBinding = createBinding(genericProject, entity, customFieldId);

        /* Create all the cufValues for the existing Entities. */
        if (!genericProjectDao.isProjectTemplate(projectId)) {
            customValueService.cascadeCustomFieldValuesCreation(newBinding);
        }
    }

    @Override
    public void removeCustomFieldBindings(List<Long> bindingIds) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        if (!bindingIds.isEmpty()) {
            if (genericProjectDao.oneIsBoundToABoundProject(bindingIds)) {
                throw new LockedParameterException();
            }
            doRemoveCustomFieldBindings(bindingIds);
        }
    }

    @Override
    public void doRemoveCustomFieldBindings(List<Long> bindingIds) {
        /* If the given bindings are removed from a ProjectTemplate, we have to propagate the deletion to the
         * equivalent bindings in the bound Projects. */
        if (!bindingIds.isEmpty()) {
            List<Long> bindingIdsToRemove = new ArrayList<>(bindingIds);
            bindingIdsToRemove.addAll(
                    customFieldBindingDao.findEquivalentBindingsForBoundProjects(bindingIds));
            customValueService.cascadeCustomFieldValuesDeletion(bindingIdsToRemove);
            customFieldBindingDao.removeCustomFieldBindings(bindingIdsToRemove);
            eventPublisher.publishEvent(new DeleteCustomFieldBindingEvent(bindingIdsToRemove));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void removeCustomFieldBindings(Long projectId) {
        List<CustomFieldBinding> bindings = customFieldBindingDao.findAllForGenericProject(projectId);
        List<Long> bindingIds =
                new LinkedList<>(CollectionUtils.collect(bindings, BINDING_ID_COLLECTOR));
        doRemoveCustomFieldBindings(bindingIds);
    }

    private CustomFieldBinding createBinding(
            GenericProject genericProject, BindableEntity entity, long customFieldId) {

        CustomFieldBinding newBinding = new CustomFieldBinding();
        CustomField field = customFieldDao.getReferenceById(customFieldId);
        long newIndex =
                customFieldBindingDao.countAllForProjectAndEntity(genericProject.getId(), entity) + 1;

        newBinding.setBoundProject(genericProject);
        newBinding.setBoundEntity(entity);
        newBinding.setCustomField(field);
        newBinding.setPosition((int) newIndex);
        // RenderingLocation is unused and will be removed soon, think about deleting this.
        newBinding.setRenderingLocations(null);

        customFieldBindingDao.save(newBinding);
        eventPublisher.publishEvent(new CreateCustomFieldBindingEvent(newBinding));

        return newBinding;
    }

    /**
     * @see CustomFieldBindingModificationService#copyCustomFieldsSettingsFromTemplate(GenericProject,
     *     GenericProject)
     */
    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void copyCustomFieldsSettingsFromTemplate(GenericProject target, GenericProject source) {

        List<CustomFieldBinding> templateCustomFieldBindings =
                findCustomFieldsForGenericProject(source.getId());
        for (CustomFieldBinding templateCustomFieldBinding : templateCustomFieldBindings) {
            long projectId = target.getId();
            BindableEntity entity = templateCustomFieldBinding.getBoundEntity();
            long customFieldId = templateCustomFieldBinding.getCustomField().getId();

            if (!customFieldBindingDao.cufBindingAlreadyExists(projectId, entity, customFieldId)) {
                addNewCustomFieldBinding(projectId, entity, customFieldId);
            }
        }
    }
}
