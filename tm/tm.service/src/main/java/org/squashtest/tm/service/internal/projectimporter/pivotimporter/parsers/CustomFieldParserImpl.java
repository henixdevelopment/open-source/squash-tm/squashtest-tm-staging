/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter.parsers;

import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.service.internal.dto.CustomFieldFormModel;
import org.squashtest.tm.service.internal.dto.projectimporter.CustomFieldToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.projectimporter.pivotimporter.PivotFormatLoggerHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.CustomFieldParser;

@Service("CustomFieldParser")
public class CustomFieldParserImpl implements CustomFieldParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomFieldParserImpl.class);

    @Override
    public CustomFieldToImport parseCustomField(
            JsonParser jsonParser, PivotFormatImport pivotFormatImport) {

        CustomFieldToImport customFieldToImport = new CustomFieldToImport();
        CustomFieldFormModel cufFormModel = new CustomFieldFormModel();

        List<BindableEntity> boundEntities = new ArrayList<>();
        try {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> customFieldToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.INPUT_TYPE ->
                            cufFormModel.setInputType(InputType.valueOf(jsonParser.getText()));
                    case JsonImportField.NAME -> cufFormModel.setName(jsonParser.getText());
                    case JsonImportField.LABEL -> cufFormModel.setLabel(jsonParser.getText());
                    case JsonImportField.CODE -> cufFormModel.setCode(jsonParser.getText());
                    case JsonImportField.DEFAULT_VALUE -> {
                        String cufValue = jsonParser.getValueAsString();
                        cufFormModel.setDefaultValue(Objects.nonNull(cufValue) ? cufValue : "");
                    }
                    case JsonImportField.OPTIONAL -> cufFormModel.setOptional(jsonParser.getBooleanValue());
                    case JsonImportField.OPTIONS -> handleCustomFieldOptions(jsonParser, cufFormModel);
                    case JsonImportField.BOUND_ENTITIES -> handleBoundEntities(jsonParser, boundEntities);
                    default -> {
                        // continue parsing
                    }
                }
                PivotFormatLoggerHelper.logParsingSuccessForEntity(
                        LOGGER,
                        PivotFormatLoggerHelper.CUSTOM_FIELD,
                        cufFormModel.getName(),
                        customFieldToImport.getInternalId(),
                        pivotFormatImport);
            }
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.CUSTOM_FIELD,
                    customFieldToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }

        CustomField customField = cufFormModel.getCustomField();
        customFieldToImport.setCustomField(customField);
        customFieldToImport.setBoundEntities(boundEntities);

        return customFieldToImport;
    }

    private void handleCustomFieldOptions(JsonParser jsonParser, CustomFieldFormModel cufFormModel)
            throws IOException {
        List<CufOption> cufOptions = new ArrayList<>();
        if (PivotJsonParsingHelper.isStartingToParseNewArray(jsonParser)) {
            cufOptions = parseCufOptions(jsonParser);
        }
        cufFormModel.setOptions(convertCufOptionsToListOfArrays(cufOptions));
    }

    private static List<CufOption> parseCufOptions(JsonParser jsonParser) throws IOException {
        List<CufOption> optionsList = new ArrayList<>();

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                optionsList.add(parseCufOption(jsonParser));
            }
        }

        return optionsList;
    }

    private static CufOption parseCufOption(JsonParser jsonParser) throws IOException {
        String name = null;
        String code = null;
        String color = null;

        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();

            // move to the value token
            jsonParser.nextToken();

            switch (fieldName) {
                case JsonImportField.NAME -> name = jsonParser.getValueAsString();
                case JsonImportField.CODE -> code = jsonParser.getValueAsString();
                case JsonImportField.COLOR -> color = jsonParser.getValueAsString();
                default -> {
                    // continue parsing
                }
            }
        }

        return new CufOption(name, code, color);
    }

    private static String[][] convertCufOptionsToListOfArrays(List<CufOption> options) {
        String[][] result = new String[options.size()][3];

        for (int i = 0; i < options.size(); i++) {
            CufOption option = options.get(i);
            result[i][0] = option.name();
            result[i][1] = option.code();
            result[i][2] = option.color();
        }

        return result;
    }

    private void handleBoundEntities(JsonParser jsonParser, List<BindableEntity> boundEntities)
            throws IOException {
        if (JsonImportField.BOUND_ENTITIES.equals(jsonParser.getCurrentName())) {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
                boundEntities.add(BindableEntity.valueOf(jsonParser.getText()));
            }
        }
    }
}

record CufOption(String name, String code, String color) {}
