/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import javax.inject.Inject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.milestone.MilestoneMember;
import org.squashtest.tm.exception.requirement.MilestoneForbidModificationException;
import org.squashtest.tm.service.internal.repository.MilestoneDao;

/** This aspect manages {@linkplain CheckBlockingMilestone} annotation. */
@Aspect
@Component
public class CheckBlockingMilestoneAspect {

    private static final String ADVISING_METHOD = "Advising method {}{}.";
    private static final String FOUND_ID_IN_METHOD = "Found required @{} on arg #{} of method {}.";
    private static final String LOCKED_MILESTONE_MESSAGE =
            "This element is bound to a locked milestone. It can't be modified";
    private static final String LOCKED_MILESTONES_MESSAGE =
            "At least one element is bound to a locked milestone. It can't be modified";
    private static final String MISSING_ID_PARAMETER =
            "Could not find any argument annotated @Id in @CheckBlockingMilestone method %s%s. This must be a structural programming error.";

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckBlockingMilestone.class);

    @Inject private MilestoneDao milestoneDao;

    @Around(
            value =
                    "execution(@org.squashtest.tm.service.annotation.CheckBlockingMilestone * * (..)) && @annotation(args)",
            argNames = "args")
    public Object checkBlockingMilestone(ProceedingJoinPoint pjp, CheckBlockingMilestone args)
            throws Throwable {
        long id = findEntityId(pjp);
        Class entityType = args.entityType();

        boolean isEntityBoundToBlockingMilestone =
                switch (entityType.getSimpleName()) {
                    case "Requirement" -> milestoneDao.isRequirementBoundToBlockingMilestone(id);
                    case "RequirementVersion" ->
                            milestoneDao.isRequirementVersionBoundToBlockingMilestone(id);
                    case "TestCase" -> milestoneDao.isTestCaseMilestoneModifiable(id);
                    case "TestStep" -> milestoneDao.isTestStepBoundToBlockingMilestone(id);
                    case "Parameter" -> milestoneDao.isParameterBoundToBlockingMilestone(id);
                    case "Dataset" -> milestoneDao.isDatasetBoundToBlockingMilestone(id);
                    case "DatasetParamValue" -> milestoneDao.isDatasetParamValueBoundToBlockingMilestone(id);
                    case "AttachmentList" -> milestoneDao.isAttachmentListBoundToBlockingMilestone(id);
                    case "Attachment" -> milestoneDao.isAttachmentBoundToBlockingMilestone(id);
                    case "Campaign" -> milestoneDao.isCampaignBoundToBlockingMilestone(id);
                    case "Iteration" -> milestoneDao.isIterationBoundToBlockingMilestone(id);
                    case "TestSuite" -> milestoneDao.isTestSuiteBoundToBlockingMilestone(id);
                    case "CampaignTestPlanItem" ->
                            milestoneDao.isCampaignTestPlanItemBoundToBlockingMilestone(id);
                    case "Execution" -> milestoneDao.isExecutionBoundToBlockingMilestone(id);
                    case "ExecutionStep" -> milestoneDao.isExecutionStepBoundToBlockingMilestone(id);
                    case "IterationTestPlanItem" ->
                            milestoneDao.isIterationTestPlanItemBoundToBlockingMilestone(id);
                    default ->
                            throw new UnsupportedOperationException(
                                    "Cannot check locked milestones for entity type " + entityType.getSimpleName());
                };
        if (isEntityBoundToBlockingMilestone) {
            throw new MilestoneForbidModificationException(LOCKED_MILESTONE_MESSAGE);
        }
        return pjp.proceed();
    }

    private long findEntityId(ProceedingJoinPoint pjp) {
        return findAnnotatedParamValue(pjp, Id.class);
    }

    @Around(
            value =
                    "execution(@org.squashtest.tm.service.annotation.CheckBlockingMilestones * * (..)) && @annotation(args)",
            argNames = "args")
    public Object checkBlockingMilestoneMultiple(
            ProceedingJoinPoint pjp, CheckBlockingMilestones args) throws Throwable {
        Collection<Long> ids = findEntitiesIds(pjp);
        Class<? extends MilestoneMember> entityType = args.entityType();

        boolean areEntitiesBoundToBlockingMilestone =
                switch (entityType.getSimpleName()) {
                    case "Requirement" -> milestoneDao.areRequirementsBoundToBlockingMilestone(ids);
                    case "TestCase" -> milestoneDao.areTestCasesBoundToBlockingMilestone(ids);
                    case "Campaign" -> milestoneDao.areCampaignsBoundToBlockingMilestone(ids);
                    case "TestSuite" -> milestoneDao.areTestSuitesBoundToBlockingMilestone(ids);
                    default -> throw new UnsupportedOperationException();
                };
        if (areEntitiesBoundToBlockingMilestone) {
            throw new MilestoneForbidModificationException(LOCKED_MILESTONES_MESSAGE);
        }
        return pjp.proceed();
    }

    private Collection<Long> findEntitiesIds(ProceedingJoinPoint pjp) {
        return findAnnotatedParamValue(pjp, Ids.class);
    }

    private <T> T findAnnotatedParamValue(
            ProceedingJoinPoint pjp, Class<? extends Annotation> idAnnotation) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        LOGGER.trace(ADVISING_METHOD, signature.getDeclaringTypeName(), method.getName());

        Annotation[][] annotations = method.getParameterAnnotations();

        for (int iArg = 0; iArg < annotations.length; iArg++) {
            Annotation[] currentArgAnnotations = annotations[iArg];
            for (Annotation annotation : currentArgAnnotations) {
                if (idAnnotation.equals(annotation.annotationType())) {
                    LOGGER.trace(FOUND_ID_IN_METHOD, idAnnotation.getSimpleName(), iArg, method.getName());
                    return (T) pjp.getArgs()[iArg];
                }
            }
        }

        throw new IllegalArgumentException(
                String.format(MISSING_ID_PARAMETER, signature.getDeclaringTypeName(), method.getName()));
    }
}
