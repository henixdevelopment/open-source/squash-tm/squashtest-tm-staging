/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.TEST_STEP;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_ID;

import java.util.List;
import java.util.Set;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionView;
import org.squashtest.tm.service.internal.display.dto.execution.ExploratorySessionOverviewDto;
import org.squashtest.tm.service.internal.display.dto.execution.ModificationDuringExecutionView.ExecutionStepActionTestStepPair;
import org.squashtest.tm.service.internal.repository.display.ExecutionDisplayDao;

@Repository
public class ExecutionDisplayDaoImpl implements ExecutionDisplayDao {

    private final DSLContext dsl;

    public ExecutionDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public ExecutionView findExecutionView(Long executionId) {
        Long testPlanItemId =
                dsl.select(EXECUTION.TEST_PLAN_ITEM_ID)
                        .from(EXECUTION)
                        .where(EXECUTION.EXECUTION_ID.eq(executionId))
                        .fetchSingle(EXECUTION.TEST_PLAN_ITEM_ID);

        if (testPlanItemId != null) {
            return findExecutionLinkedToTestPlanItem(executionId);
        } else {
            return findExecutionLinkedToIteration(executionId);
        }
    }

    public ExecutionView findExecutionLinkedToIteration(Long executionId) {
        Record record =
                dsl.select(
                                EXECUTION.EXECUTION_ID,
                                EXECUTION.NAME,
                                EXECUTION.PREREQUISITE,
                                EXECUTION.DESCRIPTION,
                                EXECUTION.ATTACHMENT_LIST_ID,
                                EXECUTION.TCLN_ID,
                                EXECUTION.TC_DESCRIPTION,
                                EXECUTION.IMPORTANCE,
                                EXECUTION.TC_STATUS,
                                EXECUTION.TC_NAT_LABEL,
                                EXECUTION.TC_NAT_ICON_NAME,
                                EXECUTION.TC_TYP_LABEL,
                                EXECUTION.TC_TYP_ICON_NAME,
                                EXECUTION.DATASET_LABEL,
                                EXECUTION.EXECUTION_MODE,
                                EXECUTION.LAST_EXECUTED_ON,
                                EXECUTION.LAST_EXECUTED_BY,
                                EXECUTION.EXECUTION_STATUS,
                                ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER,
                                CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
                                TEST_AUTOMATION_SERVER.KIND,
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID,
                                AUTOMATED_EXECUTION_EXTENDER.RESULT_URL,
                                AUTOMATED_EXECUTION_EXTENDER.RESULT_SUMMARY,
                                AUTOMATED_EXECUTION_EXTENDER.DURATION,
                                ITEM_TEST_PLAN_LIST.ITERATION_ID,
                                KEYWORD_EXECUTION.EXECUTION_ID,
                                SCRIPTED_EXECUTION.EXECUTION_ID,
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                                when(
                                                EXPLORATORY_EXECUTION.EXECUTION_ID.isNotNull(),
                                                EXPLORATORY_EXECUTION.ASSIGNEE_ID)
                                        .otherwise(ITERATION_TEST_PLAN_ITEM.USER_ID)
                                        .as(ASSIGNEE_ID),
                                EXPLORATORY_EXECUTION.REVIEWED,
                                EXPLORATORY_EXECUTION.TASK_DIVISION)
                        .from(EXECUTION)
                        .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                        .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                        .innerJoin(ITERATION)
                        .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                        .innerJoin(CAMPAIGN_ITERATION)
                        .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
                        .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                        .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .leftJoin(PROJECT)
                        .on(PROJECT.PROJECT_ID.eq(CAMPAIGN_LIBRARY_NODE.PROJECT_ID))
                        .leftJoin(TEST_AUTOMATION_SERVER)
                        .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
                        .leftJoin(KEYWORD_EXECUTION)
                        .on(KEYWORD_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .leftJoin(SCRIPTED_EXECUTION)
                        .on(SCRIPTED_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .leftJoin(EXPLORATORY_EXECUTION)
                        .on(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .where(EXECUTION.EXECUTION_ID.eq(executionId))
                        .fetchOne();

        Long count =
                dsl.selectCount()
                        .from(ITEM_TEST_PLAN_EXECUTION)
                        .where(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                        record.get(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)))
                        .fetchOneInto(Long.class);

        ExecutionView executionView = new ExecutionView();
        executionView.setId(record.get(EXECUTION.EXECUTION_ID));
        executionView.setName(record.get(EXECUTION.NAME));
        executionView.setExecutionOrder(record.get(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER));
        executionView.setProjectId(record.get(CAMPAIGN_LIBRARY_NODE.PROJECT_ID));
        executionView.setPrerequisite(record.get(EXECUTION.PREREQUISITE));
        executionView.setTcDescription(record.get(EXECUTION.TC_DESCRIPTION));
        executionView.setTcImportance(record.get(EXECUTION.IMPORTANCE));
        executionView.setTcStatus(record.get(EXECUTION.TC_STATUS));
        executionView.setTcNatLabel(record.get(EXECUTION.TC_NAT_LABEL));
        executionView.setTcNatIconName(record.get(EXECUTION.TC_NAT_ICON_NAME));
        executionView.setTcTypeLabel(record.get(EXECUTION.TC_TYP_LABEL));
        executionView.setTcTypeIconName(record.get(EXECUTION.TC_TYP_ICON_NAME));
        executionView.setDatasetLabel(record.get(EXECUTION.DATASET_LABEL));
        executionView.setAttachmentListId(record.get(EXECUTION.ATTACHMENT_LIST_ID));
        executionView.setTestCaseId(record.get(EXECUTION.TCLN_ID));
        executionView.setExecutionMode(record.get(EXECUTION.EXECUTION_MODE));
        executionView.setLastExecutedOn(record.get(EXECUTION.LAST_EXECUTED_ON));
        executionView.setLastExecutedBy(record.get(EXECUTION.LAST_EXECUTED_BY));
        executionView.setExecutionStatus(record.get(EXECUTION.EXECUTION_STATUS));
        executionView.setExtenderId(record.get(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID));
        executionView.setTestAutomationServerKind(record.get(TEST_AUTOMATION_SERVER.KIND));
        executionView.setAutomatedExecutionResultUrl(
                record.get(AUTOMATED_EXECUTION_EXTENDER.RESULT_URL));
        executionView.setAutomatedExecutionResultSummary(
                record.get(AUTOMATED_EXECUTION_EXTENDER.RESULT_SUMMARY));
        executionView.setAutomatedExecutionDuration(record.get(AUTOMATED_EXECUTION_EXTENDER.DURATION));
        // Description = comment in execution...
        executionView.setComment(record.get(EXECUTION.DESCRIPTION));
        executionView.setIterationId(record.get(ITEM_TEST_PLAN_LIST.ITERATION_ID));
        executionView.setKeywordExecutionId(record.get(KEYWORD_EXECUTION.EXECUTION_ID));
        executionView.setScriptedExecutionId(record.get(SCRIPTED_EXECUTION.EXECUTION_ID));
        executionView.setTestPlanItemId(record.get(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID));
        executionView.setExecutionsCount(count);
        executionView.setAssigneeId(record.get(ASSIGNEE_ID, Long.class));
        executionView.setReviewed(record.get(EXPLORATORY_EXECUTION.REVIEWED));
        executionView.setTaskDivision(record.get(EXPLORATORY_EXECUTION.TASK_DIVISION));
        return executionView;
    }

    public ExecutionView findExecutionLinkedToTestPlanItem(Long executionId) {
        Record record =
                dsl.select(
                                EXECUTION.EXECUTION_ID,
                                EXECUTION.NAME,
                                EXECUTION.PREREQUISITE,
                                EXECUTION.DESCRIPTION,
                                EXECUTION.ATTACHMENT_LIST_ID,
                                EXECUTION.TCLN_ID,
                                EXECUTION.TC_DESCRIPTION,
                                EXECUTION.IMPORTANCE,
                                EXECUTION.TC_STATUS,
                                EXECUTION.TC_NAT_LABEL,
                                EXECUTION.TC_NAT_ICON_NAME,
                                EXECUTION.TC_TYP_LABEL,
                                EXECUTION.TC_TYP_ICON_NAME,
                                EXECUTION.DATASET_LABEL,
                                EXECUTION.EXECUTION_MODE,
                                EXECUTION.LAST_EXECUTED_ON,
                                EXECUTION.LAST_EXECUTED_BY,
                                EXECUTION.EXECUTION_STATUS,
                                EXECUTION.EXECUTION_ORDER,
                                TEST_PLAN_ITEM.TEST_PLAN_ID,
                                TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID,
                                when(
                                                EXPLORATORY_EXECUTION.EXECUTION_ID.isNotNull(),
                                                EXPLORATORY_EXECUTION.ASSIGNEE_ID)
                                        .otherwise(TEST_PLAN_ITEM.ASSIGNEE_ID)
                                        .as(ASSIGNEE_ID),
                                PROJECT.PROJECT_ID,
                                TEST_AUTOMATION_SERVER.KIND,
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID,
                                AUTOMATED_EXECUTION_EXTENDER.RESULT_URL,
                                AUTOMATED_EXECUTION_EXTENDER.RESULT_SUMMARY,
                                AUTOMATED_EXECUTION_EXTENDER.DURATION,
                                KEYWORD_EXECUTION.EXECUTION_ID,
                                SCRIPTED_EXECUTION.EXECUTION_ID,
                                EXPLORATORY_EXECUTION.REVIEWED,
                                EXPLORATORY_EXECUTION.TASK_DIVISION,
                                SPRINT.STATUS)
                        .from(EXECUTION)
                        .innerJoin(TEST_PLAN_ITEM)
                        .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXECUTION.TEST_PLAN_ITEM_ID))
                        .innerJoin(TEST_PLAN)
                        .on(TEST_PLAN.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                        .innerJoin(CAMPAIGN_LIBRARY)
                        .on(CAMPAIGN_LIBRARY.CL_ID.eq(TEST_PLAN.CL_ID))
                        .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                        .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .leftJoin(PROJECT)
                        .on(PROJECT.CL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                        .leftJoin(TEST_AUTOMATION_SERVER)
                        .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
                        .leftJoin(KEYWORD_EXECUTION)
                        .on(KEYWORD_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .leftJoin(SCRIPTED_EXECUTION)
                        .on(SCRIPTED_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .leftJoin(EXPLORATORY_EXECUTION)
                        .on(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .leftJoin(SPRINT_REQ_VERSION)
                        .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN.TEST_PLAN_ID))
                        .leftJoin(SPRINT)
                        .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                        .where(EXECUTION.EXECUTION_ID.eq(executionId))
                        .fetchOne();

        Long executionCount =
                dsl.selectCount()
                        .from(TEST_PLAN_ITEM)
                        .innerJoin(EXECUTION)
                        .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXECUTION.TEST_PLAN_ITEM_ID))
                        .where(
                                TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(record.get(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID)))
                        .fetchOneInto(Long.class);

        ExecutionView executionView = new ExecutionView();
        executionView.setId(record.get(EXECUTION.EXECUTION_ID));
        executionView.setName(record.get(EXECUTION.NAME));
        executionView.setExecutionOrder(record.get(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER));
        executionView.setProjectId(record.get(PROJECT.PROJECT_ID));
        executionView.setPrerequisite(record.get(EXECUTION.PREREQUISITE));
        executionView.setTcDescription(record.get(EXECUTION.TC_DESCRIPTION));
        executionView.setTcImportance(record.get(EXECUTION.IMPORTANCE));
        executionView.setTcStatus(record.get(EXECUTION.TC_STATUS));
        executionView.setTcNatLabel(record.get(EXECUTION.TC_NAT_LABEL));
        executionView.setTcNatIconName(record.get(EXECUTION.TC_NAT_ICON_NAME));
        executionView.setTcTypeLabel(record.get(EXECUTION.TC_TYP_LABEL));
        executionView.setTcTypeIconName(record.get(EXECUTION.TC_TYP_ICON_NAME));
        executionView.setDatasetLabel(record.get(EXECUTION.DATASET_LABEL));
        executionView.setAttachmentListId(record.get(EXECUTION.ATTACHMENT_LIST_ID));
        executionView.setTestCaseId(record.get(EXECUTION.TCLN_ID));
        executionView.setExecutionMode(record.get(EXECUTION.EXECUTION_MODE));
        executionView.setLastExecutedOn(record.get(EXECUTION.LAST_EXECUTED_ON));
        executionView.setLastExecutedBy(record.get(EXECUTION.LAST_EXECUTED_BY));
        executionView.setExecutionStatus(record.get(EXECUTION.EXECUTION_STATUS));
        executionView.setExtenderId(record.get(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID));
        executionView.setTestAutomationServerKind(record.get(TEST_AUTOMATION_SERVER.KIND));
        executionView.setAutomatedExecutionResultUrl(
                record.get(AUTOMATED_EXECUTION_EXTENDER.RESULT_URL));
        executionView.setAutomatedExecutionResultSummary(
                record.get(AUTOMATED_EXECUTION_EXTENDER.RESULT_SUMMARY));
        executionView.setAutomatedExecutionDuration(record.get(AUTOMATED_EXECUTION_EXTENDER.DURATION));
        executionView.setComment(record.get(EXECUTION.DESCRIPTION));
        executionView.setKeywordExecutionId(record.get(KEYWORD_EXECUTION.EXECUTION_ID));
        executionView.setScriptedExecutionId(record.get(SCRIPTED_EXECUTION.EXECUTION_ID));
        executionView.setTestPlanItemId(record.get(EXECUTION.TEST_PLAN_ITEM_ID));
        executionView.setExecutionsCount(executionCount);
        executionView.setAssigneeId(record.get(ASSIGNEE_ID, Long.class));
        executionView.setReviewed(record.get(EXPLORATORY_EXECUTION.REVIEWED));
        executionView.setTaskDivision(record.get(EXPLORATORY_EXECUTION.TASK_DIVISION));

        if (record.get(SPRINT.STATUS) != null) {
            executionView.setParentSprintStatus(SprintStatus.valueOf(record.get(SPRINT.STATUS)));
        }
        return executionView;
    }

    @Override
    public List<ExecutionStepActionTestStepPair> findExecutionStepActionStepPairs(Long executionId) {
        return this.dsl
                .select(EXECUTION_STEP.EXECUTION_STEP_ID, TEST_STEP.TEST_STEP_ID)
                .from(EXECUTION_EXECUTION_STEPS)
                .innerJoin(EXECUTION_STEP)
                .on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                .innerJoin(TEST_STEP)
                .on(EXECUTION_STEP.TEST_STEP_ID.eq(TEST_STEP.TEST_STEP_ID))
                .where(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(executionId))
                .orderBy(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER.asc())
                .fetchInto(ExecutionStepActionTestStepPair.class);
    }

    @Override
    public Set<Long> findAllTestCaseInExecution(Long executionId) {
        return this.dsl
                .selectDistinct(TEST_CASE_STEPS.TEST_CASE_ID)
                .from(EXECUTION_EXECUTION_STEPS)
                .innerJoin(EXECUTION_STEP)
                .on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                .innerJoin(TEST_STEP)
                .on(EXECUTION_STEP.TEST_STEP_ID.eq(TEST_STEP.TEST_STEP_ID))
                .innerJoin(TEST_CASE_STEPS)
                .on(TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
                .where(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(executionId))
                .fetchSet(TEST_CASE_STEPS.TEST_CASE_ID);
    }

    @Override
    public Long findAttachmentList(Long executionId) {
        return dsl.select(EXECUTION.ATTACHMENT_LIST_ID)
                .from(EXECUTION)
                .where(EXECUTION.EXECUTION_ID.eq(executionId))
                .fetchOneInto(Long.class);
    }

    @Override
    public ExploratorySessionOverviewDto findSessionByExecutionId(Long executionId) {
        final ExploratorySessionOverviewDto dto =
                dsl.select(
                                EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.as("sessionOverviewId"),
                                EXPLORATORY_SESSION_OVERVIEW.SESSION_DURATION,
                                EXPLORATORY_SESSION_OVERVIEW.CHARTER)
                        .from(EXPLORATORY_SESSION_OVERVIEW)
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID))
                        .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                        .on(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .where(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(executionId))
                        .fetchOneInto(ExploratorySessionOverviewDto.class);

        if (dto != null) {
            return dto;
        } else {
            return dsl.select(
                            EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.as("sessionOverviewId"),
                            EXPLORATORY_SESSION_OVERVIEW.SESSION_DURATION,
                            EXPLORATORY_SESSION_OVERVIEW.CHARTER)
                    .from(EXPLORATORY_SESSION_OVERVIEW)
                    .innerJoin(EXECUTION)
                    .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID))
                    .where(EXECUTION.EXECUTION_ID.eq(executionId))
                    .fetchOneInto(ExploratorySessionOverviewDto.class);
        }
    }
}
