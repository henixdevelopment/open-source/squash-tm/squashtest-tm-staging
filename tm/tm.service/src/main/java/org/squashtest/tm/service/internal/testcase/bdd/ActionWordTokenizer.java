/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.bdd.util.ActionWordUtil;
import org.squashtest.tm.exception.actionword.InvalidActionWordInputNameException;
import org.squashtest.tm.exception.actionword.InvalidActionWordTextException;

/**
 * Builds an intermediate representation of an ActionWord (a list of tokens for each fragment) from
 * an input string. This tries to be as decoupled as possible from the two possible implementations
 * (one from the TestCaseWorkspace where TestCase parameters are a thing, and another in the
 * ActionWordWorkspace where angled values are forbidden).
 *
 * <p>Inspired on Robert Nystrom's "Crafting Interpreters" first chapters.
 */
public class ActionWordTokenizer {
    private static final List<Character> DELIMITERS = Arrays.asList('"', '<', '>');

    private final String source;
    private final boolean allowsNamedParameters;
    private int start = 0;
    private int current = 0;
    private final List<AWToken> tokens = new ArrayList<>();

    /**
     * @param source the raw text to build tokens from
     * @param allowsAngledValues are "<" and ">" delimiters allowed
     */
    ActionWordTokenizer(String source, boolean allowsAngledValues) {
        this.source = source;
        this.allowsNamedParameters = allowsAngledValues;
    }

    public List<AWToken> tokenize() {
        while (!isAtEnd()) {
            start = current;
            scanToken();
        }

        return tokens;
    }

    private boolean isAtEnd() {
        return current >= source.length();
    }

    private void scanToken() {
        char c = peek();

        switch (c) {
            case '"' -> parseQuotedValue();
            case '<' -> parseAngledValue();
            case '>' -> handleHangingCloseAngle();
            default -> parsePlainTextAndNumbers();
        }
    }

    private void handleHangingCloseAngle() {
        if (allowsNamedParameters) {
            throw new InvalidActionWordTextException(
                    "Expected opening delimiter '<' before closing '>' delimiter.");
        } else {
            throw new InvalidActionWordInputNameException();
        }
    }

    private char peek() {
        if (isAtEnd()) {
            return '\0';
        }

        return source.charAt(current);
    }

    private char peekNext() {
        if (current + 1 >= source.length()) {
            return '\0';
        }

        return source.charAt(current + 1);
    }

    private void advance() {
        current++;
    }

    private void parseQuotedValue() {
        advance(); // Opening delimiter

        final int openPosition = current;

        consumeQuotedText();

        if (peek() != '"' || isAtEnd()) {
            throw new UnclosedQuotedValue(openPosition, source);
        }

        advance(); // Closing delimiter

        String text = source.substring(start, current);
        tokens.add(new AWToken(AWTokenType.QUOTED_VALUE, text));
    }

    private void parseAngledValue() {
        if (!allowsNamedParameters) {
            throw new ForbiddenAngleException(current + 1, source);
        }

        advance(); // Opening delimiter

        final int openPosition = current;

        while (peek() != '>' && !isAtEnd()) {
            advance();
        }

        if (isAtEnd()) {
            throw new UnclosedAngledValue(openPosition, source);
        }

        advance(); // Closing delimiter

        final String parameterName = source.substring(start, current);
        tokens.add(new AWToken(AWTokenType.ANGLED_VALUE, parameterName));
    }

    private void parsePlainTextAndNumbers() {
        final String valueSoFar = consumeText();
        addTextAndNumbers(valueSoFar);
    }

    // Discriminate between text and number parts
    // e.g. "hello -1.23 goodbye" => ["hello ", "-1.23", " goodbye"]
    private void addTextAndNumbers(String valueSoFar) {
        // If only whitespaces, just add a text section with this whitespace
        if (StringUtils.isBlank(valueSoFar)) {
            tokens.add(new AWToken(AWTokenType.PLAIN_TEXT, valueSoFar));
            return;
        }

        // Split on whitespaces but keep them in resulting array
        // e.g. "hello  there" gets split as ["hello", " ", " ", there]
        final String[] split = valueSoFar.split("((?<=\\s)|(?=\\s+))");

        // Collector for text fragments
        final StringBuilder textBuilder = new StringBuilder();

        for (String part : split) {
            if (ActionWordUtil.isNumber(part)) {
                // Found a number. Everything that was collected before can be added as text
                if (textBuilder.length() > 0) {
                    tokens.add(new AWToken(AWTokenType.PLAIN_TEXT, textBuilder.toString()));
                    textBuilder.setLength(0);
                }

                // Add number
                tokens.add(new AWToken(AWTokenType.NUMBER, part));
            } else {
                // Collect text
                textBuilder.append(part);
            }
        }

        // If there's still collected text, add it
        if (textBuilder.length() > 0) {
            tokens.add(new AWToken(AWTokenType.PLAIN_TEXT, textBuilder.toString()));
        }
    }

    // Advance until a delimiter is found
    private String consumeText() {
        while (!isDelimiter(peek()) && !isAtEnd()) {
            // Handle escaped characters
            if (peek() == '\\' && ActionWordUtil.isEscapableCharacter(peekNext())) {
                advance();
            }

            advance();
        }

        return source.substring(start, current);
    }

    // Advance until a double-quote is found
    private String consumeQuotedText() {
        while (peek() != '"' && !isAtEnd()) {
            // Handle escaped characters
            if (peek() == '\\' && ActionWordUtil.isEscapableCharacter(peekNext())) {
                advance();
            }

            advance();
        }

        return source.substring(start, current);
    }

    private static boolean isDelimiter(char c) {
        return DELIMITERS.contains(c);
    }

    public static class AWToken {
        private final AWTokenType type;

        /** The actual code this token represents. */
        private final String lexeme;

        public AWToken(AWTokenType type, String lexeme) {
            this.type = type;
            this.lexeme = lexeme;
        }

        public AWTokenType getType() {
            return type;
        }

        public String getLexeme() {
            return lexeme;
        }
    }

    /** Possible token types. */
    public enum AWTokenType {
        /** Plain text */
        PLAIN_TEXT,

        /** Numeric value found in plain text */
        NUMBER,

        /** Value delimited by double quotes (") */
        QUOTED_VALUE,

        /** Value delimited by angle brackets (<>) */
        ANGLED_VALUE
    }

    static class UnclosedQuotedValue extends ContextualizedActionWordException {
        UnclosedQuotedValue(int delimiterPosition, String textInput) {
            super("Found unclosed quote (\") delimiter.", textInput, delimiterPosition);
        }

        @Override
        public String getI18nKey() {
            return "sqtm-core.error.action-word.unclosed-quoted-value";
        }
    }

    static class UnclosedAngledValue extends ContextualizedActionWordException {
        UnclosedAngledValue(int delimiterPosition, String textInput) {
            super("Found unclosed angle bracket (<) delimiter.", textInput, delimiterPosition);
        }

        @Override
        public String getI18nKey() {
            return "sqtm-core.error.action-word.unclosed-angled-value";
        }
    }

    static class ForbiddenAngleException extends ContextualizedActionWordException {
        ForbiddenAngleException(int delimiterPosition, String textInput) {
            super(
                    "The action word name cannot contain the < and > symbols.", textInput, delimiterPosition);
        }

        @Override
        public String getI18nKey() {
            return "sqtm-core.error.action-word.input.name";
        }
    }
}
