/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState;

/**
 * @author lpoma
 */
@Service
@Transactional
public class RestITPIManagerService {

    @Inject private IterationTestPlanDao itpiDao;

    @Inject private UserDao userDao;

    @Inject private CustomTestSuiteModificationService customTestSuiteModificationService;

    @PreAuthorize(
            "hasRole('ROLE_TA_API_CLIENT') or hasRole('ROLE_ADMIN') or hasPermission(#id, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem' ,'EXECUTE')")
    public void forceExecutionStatus(long id, @NotNull AutomatedExecutionState stateChange) {
        IterationTestPlanItem item = getById(id);
        String login = UserContextHolder.getUsername();
        User user = userDao.findUserByLogin(login);
        Date date = new Date();
        TestExecutionStatus testExecutionStatus =
                stateChange.getTfTestExecutionStatus().tfTestExecutionStatusToTmTestExecutionStatus();
        item.setExecutionStatus(ExecutionStatus.valueOf(testExecutionStatus.getStatus().name()));
        arbitraryUpdateMetadata(item, user, date);
        List<TestSuite> testSuitesToUpdate = new ArrayList<>();
        testSuitesToUpdate.addAll(item.getTestSuites());
        customTestSuiteModificationService.updateExecutionStatus(testSuitesToUpdate);
    }

    public IterationTestPlanItem getById(long id) {
        return itpiDao.findByIdWithTestCase(id);
    }

    /**
     * Instead of using data from an actual execution, we update those data according to given
     * parameters.
     *
     * @param item: the iteration test plan item to update the metadatas of
     * @param user : the user that will be set as last executor and assigne
     * @param date : the date to set lastExecutedOn property
     */
    private void arbitraryUpdateMetadata(IterationTestPlanItem item, User user, Date date) {
        item.setLastExecutedBy(user.getLogin());
        item.setLastExecutedOn(date);
        item.setUser(user);
    }
}
