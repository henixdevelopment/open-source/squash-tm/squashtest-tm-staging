/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_REQUIREMENT_LIBRARY_CONTENT_NAMES;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_REQUIREMENT_LIBRARY_FOR_NODE_ADDITION;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.service.internal.repository.RequirementLibraryDao;

@SuppressWarnings("rawtypes")
@Repository
public class HibernateRequirementLibraryDao
        extends HibernateLibraryDao<RequirementLibrary, RequirementLibraryNode>
        implements RequirementLibraryDao {

    @Override
    public List<String> findContentNamesByLibraryId(Long libraryId) {
        return entityManager
                .createQuery(FIND_REQUIREMENT_LIBRARY_CONTENT_NAMES, String.class)
                .setParameter(ID, libraryId)
                .getResultList();
    }

    @Override
    public RequirementLibrary loadForNodeAddition(Long libraryId) {
        return entityManager
                .createQuery(FIND_REQUIREMENT_LIBRARY_FOR_NODE_ADDITION, RequirementLibrary.class)
                .setParameter(ID, libraryId)
                .getSingleResult();
    }
}
