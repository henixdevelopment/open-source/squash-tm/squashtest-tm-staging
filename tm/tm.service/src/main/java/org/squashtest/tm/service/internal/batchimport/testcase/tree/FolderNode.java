/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseImportData;
import org.squashtest.tm.service.internal.library.LibraryUtils;

public class FolderNode {

    private final String name;
    private final String path;
    private final Map<String, FolderNode> children;
    private final List<TestCaseImportData> contents = new ArrayList<>();

    public FolderNode(String name, String path) {
        this.name = name;
        this.path = path;
        this.children = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public Map<String, FolderNode> getChildren() {
        return children;
    }

    public void addChild(FolderNode child) {
        this.children.put(child.getName(), child);
    }

    public FolderNode getChild(String name) {
        return children.get(name);
    }

    public void addContents(List<TestCaseImportData> importData) {
        contents.addAll(importData);
    }

    public List<TestCaseImportData> getContents() {
        return contents;
    }

    public TestCaseFolder convertToSquashFolder() {
        TestCaseFolder testCaseFolder = new TestCaseFolder();
        testCaseFolder.setName(getName());
        testCaseFolder.setDescription("");

        addContentsInSquashFolder(testCaseFolder);

        for (FolderNode child : getChildren().values()) {
            TestCaseFolder childTestCaseFolder = child.convertToSquashFolder();
            testCaseFolder.addContent(childTestCaseFolder);
        }

        return testCaseFolder;
    }

    private void addContentsInSquashFolder(TestCaseFolder testCaseFolder) {
        if (contents.isEmpty()) {
            return;
        }

        Set<String> existingNames = new HashSet<>();

        contents.forEach(
                content -> {
                    TestCase testCase = content.getTestCase();
                    resolveNameConflict(existingNames, testCase);
                    testCaseFolder.addContent(testCase);
                    existingNames.add(testCase.getName());
                });
    }

    private void resolveNameConflict(Set<String> existingNames, TestCase testCase) {
        String currentName = testCase.getName();
        String uniqueName =
                LibraryUtils.generateNonClashingName(currentName, existingNames, Sizes.NAME_MAX);

        if (!uniqueName.equals(currentName)) {
            testCase.setName(uniqueName);
        }
    }

    public List<TestCaseImportData> collectAllTestCaseImportData() {
        List<TestCaseImportData> result =
                contents.isEmpty() ? new ArrayList<>() : new ArrayList<>(contents);

        for (FolderNode child : getChildren().values()) {
            result.addAll(child.collectAllTestCaseImportData());
        }

        return result;
    }

    public FolderNode getChildOrCreate(String part, String path) {
        FolderNode child = getChild(part);
        if (child == null) {
            child = new FolderNode(part, path);
            addChild(child);
        }

        return child;
    }
}
