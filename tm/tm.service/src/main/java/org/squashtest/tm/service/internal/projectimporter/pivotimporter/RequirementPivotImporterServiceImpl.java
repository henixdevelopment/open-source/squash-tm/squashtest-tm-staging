/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.ProjectIdsReferences;
import org.squashtest.tm.service.internal.dto.projectimporter.RequirementToImport;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementFolderDao;
import org.squashtest.tm.service.internal.repository.RequirementLibraryDao;
import org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig;
import org.squashtest.tm.service.internal.requirement.CategoryChainFixer;
import org.squashtest.tm.service.internal.requirement.RequirementFactory;
import org.squashtest.tm.service.projectimporter.pivotimporter.AttachmentPivotImportService;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.RequirementPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.RequirementWorkspaceParser;

@Service("RequirementPivotImporterService")
public class RequirementPivotImporterServiceImpl implements RequirementPivotImporterService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(RequirementPivotImporterServiceImpl.class);
    private final RequirementLibraryDao requirementLibraryDao;
    private final RequirementFolderDao requirementFolderDao;
    private final RequirementDao requirementDao;
    private final RequirementFactory requirementFactory;
    private final PrivateCustomFieldValueService customFieldValueService;
    private final RequirementWorkspaceParser requirementWorkspaceParser;
    private final AttachmentPivotImportService attachmentPivotImportService;
    @PersistenceContext private EntityManager entityManager;

    public RequirementPivotImporterServiceImpl(
            RequirementLibraryDao requirementLibraryDao,
            RequirementFolderDao requirementFolderDao,
            RequirementDao requirementDao,
            RequirementFactory requirementFactory,
            PrivateCustomFieldValueService customFieldValueService,
            RequirementWorkspaceParser requirementWorkspaceParser,
            AttachmentPivotImportService attachmentPivotImportService) {
        this.requirementLibraryDao = requirementLibraryDao;
        this.requirementFolderDao = requirementFolderDao;
        this.requirementDao = requirementDao;
        this.requirementFactory = requirementFactory;
        this.customFieldValueService = customFieldValueService;
        this.requirementWorkspaceParser = requirementWorkspaceParser;
        this.attachmentPivotImportService = attachmentPivotImportService;
    }

    @Override
    public void importRequirementsFromZipArchive(
            ZipFile zipFile,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.REQUIREMENTS, pivotFormatImport);
        ZipEntry entry = zipFile.getEntry(JsonImportFile.REQUIREMENTS.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleRequirementsInJsonFile(
                        jsonInputStream, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.REQUIREMENTS, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.REQUIREMENTS, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleRequirementsInJsonFile(
            InputStream jsonInputStream,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseRequirementsArray(
                        projectIdsReferences, pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
    }

    private void parseRequirementsArray(
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<RequirementToImport> requirementsToImport = new ArrayList<>();
        HibernateConfig.enableBatch(entityManager, HibernateConfig.BATCH_50);

        if (!JsonImportField.REQUIREMENTS.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                RequirementToImport requirementToImport =
                        requirementWorkspaceParser.parseRequirement(
                                jsonParser, pivotImportMetadata, pivotFormatImport);
                requirementsToImport.add(requirementToImport);
            }
            if (requirementsToImport.size() == HibernateConfig.BATCH_50) {
                createRequirements(
                        requirementsToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
            }
        }

        if (!requirementsToImport.isEmpty()) {
            createRequirements(
                    requirementsToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
        }
        HibernateConfig.disabledBatch(entityManager);
    }

    private void createRequirements(
            List<RequirementToImport> requirementsToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        Map<Requirement, RequirementToImport> createdRequirements = new HashMap<>();
        batchCreateHibernateRequirements(
                requirementsToImport,
                projectIdsReferences,
                pivotImportMetadata,
                pivotFormatImport,
                createdRequirements);

        createdRequirements.forEach(
                (requirement, requirementToImport) -> {
                    try {
                        postProcessCreatedRequirements(
                                pivotImportMetadata, pivotFormatImport, requirement, requirementToImport);
                    } catch (Exception e) {
                        PivotFormatLoggerHelper.handleEntityCreationFailed(
                                LOGGER,
                                PivotFormatLoggerHelper.REQUIREMENT,
                                requirementToImport.getRequirement().getName(),
                                requirementToImport.getInternalId(),
                                pivotFormatImport,
                                e);
                    }

                    PivotFormatLoggerHelper.logEntityCreatedSuccessfully(
                            LOGGER,
                            PivotFormatLoggerHelper.REQUIREMENT,
                            requirementToImport.getRequirement().getName(),
                            requirementToImport.getInternalId(),
                            pivotFormatImport);
                });

        requirementsToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void batchCreateHibernateRequirements(
            List<RequirementToImport> requirementsToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport,
            Map<Requirement, RequirementToImport> createdRequirements) {
        List<RequirementToImport> requirementsToImportInLibrary = new ArrayList<>();
        Map<String, List<RequirementToImport>> requirementToImportByFolderIdMap = new HashMap<>();
        Map<String, List<RequirementToImport>> requirementToImportByRequirementIdMap = new HashMap<>();
        Map<RequirementVersion, Map<Long, RawValue>> requirementCustomFields = new HashMap<>();

        for (RequirementToImport requirementToImport : requirementsToImport) {
            if (requirementToImport.getParentId() == null) {
                requirementsToImportInLibrary.add(requirementToImport);
            } else if (EntityType.REQUIREMENT_FOLDER.equals(requirementToImport.getParentType())) {
                requirementToImportByFolderIdMap
                        .computeIfAbsent(requirementToImport.getParentId(), k -> new ArrayList<>())
                        .add(requirementToImport);
            } else {
                requirementToImportByRequirementIdMap
                        .computeIfAbsent(requirementToImport.getParentId(), k -> new ArrayList<>())
                        .add(requirementToImport);
            }
        }

        addRequirementsToLibrary(
                pivotImportMetadata,
                projectIdsReferences,
                requirementsToImportInLibrary,
                createdRequirements,
                requirementCustomFields,
                pivotFormatImport);

        addRequirementsToFolders(
                pivotImportMetadata,
                requirementToImportByFolderIdMap,
                createdRequirements,
                requirementCustomFields,
                pivotFormatImport);

        addRequirementsToRequirements(
                pivotImportMetadata,
                requirementToImportByRequirementIdMap,
                createdRequirements,
                requirementCustomFields,
                pivotFormatImport);
        initializeCustomFieldValues(pivotFormatImport, createdRequirements, requirementCustomFields);
    }

    private void addRequirementsToLibrary(
            PivotImportMetadata pivotImportMetadata,
            ProjectIdsReferences projectIdsReferences,
            List<RequirementToImport> requirementsToImportInLibrary,
            Map<Requirement, RequirementToImport> createdRequirements,
            Map<RequirementVersion, Map<Long, RawValue>> requirementCustomFields,
            PivotFormatImport pivotFormatImport) {
        RequirementLibrary library =
                requirementLibraryDao.loadForNodeAddition(projectIdsReferences.getRequirementLibraryId());

        for (RequirementToImport requirementToImport : requirementsToImportInLibrary) {
            createAndAddRequirementToEntity(
                    createdRequirements,
                    requirementCustomFields,
                    pivotImportMetadata,
                    pivotFormatImport,
                    requirementToImport,
                    library);
        }
    }

    private void addRequirementsToFolders(
            PivotImportMetadata pivotImportMetadata,
            Map<String, List<RequirementToImport>> requirementToImportByFolderIdMap,
            Map<Requirement, RequirementToImport> createdRequirements,
            Map<RequirementVersion, Map<Long, RawValue>> requirementCustomFields,
            PivotFormatImport pivotFormatImport) {
        List<Long> squashFolderIds =
                requirementToImportByFolderIdMap.keySet().stream()
                        .map(parentId -> pivotImportMetadata.getRequirementFoldersIdsMap().get(parentId))
                        .toList();

        Map<Long, RequirementFolder> requirementFolderById =
                requirementFolderDao.loadForNodeAddition(squashFolderIds).stream()
                        .collect(Collectors.toMap(RequirementFolder::getId, Function.identity()));

        requirementToImportByFolderIdMap.forEach(
                (parentId, requirementsToImportInFolder) -> {
                    Long squashFolderId = pivotImportMetadata.getRequirementFoldersIdsMap().get(parentId);
                    RequirementFolder folder = requirementFolderById.get(squashFolderId);

                    for (RequirementToImport requirementToImport : requirementsToImportInFolder) {
                        createAndAddRequirementToEntity(
                                createdRequirements,
                                requirementCustomFields,
                                pivotImportMetadata,
                                pivotFormatImport,
                                requirementToImport,
                                folder);
                    }
                });
    }

    private void addRequirementsToRequirements(
            PivotImportMetadata pivotImportMetadata,
            Map<String, List<RequirementToImport>> requirementToImportByRequirementIdMap,
            Map<Requirement, RequirementToImport> createdRequirements,
            Map<RequirementVersion, Map<Long, RawValue>> requirementCustomFields,
            PivotFormatImport pivotFormatImport) {
        List<Long> requirementIds =
                requirementToImportByRequirementIdMap.keySet().stream()
                        .map(parentId -> pivotImportMetadata.getRequirementIdsMap().get(parentId))
                        .toList();

        Map<Long, Requirement> requirementById =
                requirementDao.loadForNodeAddition(requirementIds).stream()
                        .collect(Collectors.toMap(Requirement::getId, Function.identity()));

        requirementToImportByRequirementIdMap.forEach(
                (parentId, requirementsToImportInRequirement) -> {
                    Long requirementId = pivotImportMetadata.getRequirementIdsMap().get(parentId);
                    Requirement requirement = requirementById.get(requirementId);

                    for (RequirementToImport requirementToImport : requirementsToImportInRequirement) {
                        createAndAddRequirementToEntity(
                                createdRequirements,
                                requirementCustomFields,
                                pivotImportMetadata,
                                pivotFormatImport,
                                requirementToImport,
                                requirement);
                    }
                });
    }

    private void initializeCustomFieldValues(
            PivotFormatImport pivotFormatImport,
            Map<Requirement, RequirementToImport> createdRequirements,
            Map<RequirementVersion, Map<Long, RawValue>> requirementCustomFields) {

        Set<RequirementVersion> requirementVersions =
                createdRequirements.keySet().stream()
                        .map(Requirement::getCurrentVersion)
                        .collect(Collectors.toSet());

        customFieldValueService.createAllCustomFieldValues(
                requirementVersions, pivotFormatImport.getProject());
        customFieldValueService.initBatchCustomFieldValues(requirementCustomFields);
    }

    private <T> void createAndAddRequirementToEntity(
            Map<Requirement, RequirementToImport> createdRequirements,
            Map<RequirementVersion, Map<Long, RawValue>> requirementVersionCustomFields,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport,
            RequirementToImport requirementToImport,
            T entity) {

        try {
            Requirement squashRequirement =
                    requirementFactory.createRequirement(requirementToImport.getRequirement());

            if (entity instanceof RequirementLibrary library) {
                library.addContent(squashRequirement);
            } else if (entity instanceof RequirementFolder folder) {
                folder.addContent(squashRequirement);
            } else if (entity instanceof Requirement requirement) {
                requirement.addContent(squashRequirement);
            }

            replaceInfoListReferences(squashRequirement);
            entityManager.persist(squashRequirement);
            createdRequirements.put(squashRequirement, requirementToImport);
            requirementVersionCustomFields.put(
                    squashRequirement.getCurrentVersion(),
                    requirementToImport.getRequirement().getCustomFields());

            pivotImportMetadata
                    .getRequirementIdsMap()
                    .put(requirementToImport.getInternalId(), squashRequirement.getId());
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleEntityCreationFailed(
                    LOGGER,
                    PivotFormatLoggerHelper.TEST_CASE,
                    requirementToImport.getRequirement().getName(),
                    requirementToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }
    }

    private void replaceInfoListReferences(Requirement requirement) {
        CategoryChainFixer.fix(requirement);
    }

    private void postProcessCreatedRequirements(
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport,
            Requirement squashRequirement,
            RequirementToImport requirementToImport)
            throws IOException {
        squashRequirement.getCurrentVersion().updateStatusWithoutCheck(requirementToImport.getStatus());

        attachmentPivotImportService.addAttachmentsToEntity(
                requirementToImport.getAttachments(),
                new AttachmentHolderInfo(
                        squashRequirement.getId(),
                        squashRequirement.getAttachmentList().getId(),
                        EntityType.REQUIREMENT,
                        requirementToImport.getInternalId()),
                pivotFormatImport,
                pivotImportMetadata);
    }
}
