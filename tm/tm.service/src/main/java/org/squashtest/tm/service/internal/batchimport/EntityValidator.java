/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.squashtest.tm.service.internal.batchimport.Existence.NOT_EXISTS;
import static org.squashtest.tm.service.internal.batchimport.Existence.TO_BE_DELETED;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementSheetColumn.REQ_PATH;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementSheetColumn.REQ_VERSION_NAME;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementSheetColumn.REQ_VERSION_REFERENCE;
import static org.squashtest.tm.service.internal.batchimport.column.testcase.DatasetSheetColumn.TC_DATASET_NAME;
import static org.squashtest.tm.service.internal.batchimport.column.testcase.ParameterSheetColumn.TC_OWNER_PATH;
import static org.squashtest.tm.service.internal.batchimport.column.testcase.ParameterSheetColumn.TC_PARAM_NAME;
import static org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn.TC_NAME;
import static org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn.TC_REFERENCE;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.importer.ImportMode;
import org.squashtest.tm.service.importer.ImportStatus;
import org.squashtest.tm.service.importer.LogEntry;
import org.squashtest.tm.service.importer.Target;
import org.squashtest.tm.service.importer.WithPath;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageTarget;
import org.squashtest.tm.service.internal.batchimport.column.testcase.StepSheetColumn;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementLinkInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementLinkTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementImportValidationBag;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseImportValidationBag;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.requirement.HighLevelRequirementService;
import org.squashtest.tm.service.requirement.RequirementLibraryFinderService;

@Component
@Scope(SCOPE_PROTOTYPE)
public class EntityValidator {

    private final PluginFinderService pluginFinderService;
    private final HighLevelRequirementService highLevelRequirementService;
    private final RequirementLibraryFinderService reqFinderService;
    private Model model;

    public EntityValidator(
            PluginFinderService pluginFinderService,
            HighLevelRequirementService highLevelRequirementService,
            RequirementLibraryFinderService reqFinderService) {
        this.pluginFinderService = pluginFinderService;
        this.highLevelRequirementService = highLevelRequirementService;
        this.reqFinderService = reqFinderService;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * Prerforms Test Case entity check before modifying a test case.
     *
     * <p>It checks : - the path is well formed (failure) - the test case has a name (failure) - the
     * test case name has length between 0 and 255 - the project exists (failure) - the size of fields
     * that are restricted in size (warning)
     *
     * @param target
     * @param testCase
     * @return
     */
    public LogTrain updateTestCaseChecks(
            TestCaseTarget target, TestCase testCase, TestCaseImportValidationBag validationBag) {

        LogTrain logs = createTestCaseChecks(target, testCase, validationBag);

        // 1 - name must be supplied
        String name = testCase.getName();
        if (StringUtils.isBlank(name)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_FIELD_MANDATORY, TC_NAME.header)
                            .build());
        }

        // 2 - natures and types now
        logs.append(checkNatureAndTypeAndFixIfNeeded(target, testCase, validationBag));

        return logs;
    }

    /**
     * Performs Test Case entity check before creating a test case.
     *
     * @param target
     * @param testCase
     * @return
     */
    public LogTrain createTestCaseChecks(
            TestCaseTarget target, TestCase testCase, TestCaseImportValidationBag validationBag) {
        String name = testCase.getName();
        LogTrain logs = new LogTrain();

        // 1 - path must be supplied and and well formed
        if (!target.isWellFormed()) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MALFORMED_PATH, target.getPath())
                            .build());
        }

        // 3 - the project actually exists
        if (target.isWellFormed()) {
            TargetStatus projectStatus = model.getProjectStatus(target.getProject());
            if (projectStatus.getStatus() != Existence.EXISTS) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_PROJECT_NOT_EXIST)
                                .build());
            }
        }

        // 4 - name has length between 0 and 255
        if (name != null && name.length() > Sizes.NAME_MAX) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MAX_SIZE, TC_NAME.header)
                            .withImpact(Messages.IMPACT_MAX_SIZE)
                            .build());
        }

        // 5 - reference, if exists, has length between 0 and 50
        String reference = testCase.getReference();
        if (!StringUtils.isBlank(reference) && reference.length() > Sizes.REFERENCE_MAX) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MAX_SIZE, TC_REFERENCE.header)
                            .withImpact(Messages.IMPACT_MAX_SIZE)
                            .build());
        }

        // 6 - natures and types now
        logs.append(checkNatureAndTypeAndFixIfNeeded(target, testCase, validationBag));

        return logs;
    }

    /**
     * those checks are run for a test step for any type of operations.
     *
     * <p>It checks : - the path of the test case is well formed (failure) - the project exists
     * (failure) - the format of the custom fields (lists, dates and checkbox) (warning)
     *
     * @param target
     * @return
     */
    LogTrain basicTestStepChecks(TestStepTarget target) {

        LogTrain logs = new LogTrain();

        TestCaseTarget testCase = target.getTestCase();

        // 1 - test case owner path must be supplied and and well formed
        if (!testCase.isWellFormed()) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MALFORMED_PATH, testCase.getPath())
                            .build());
        }

        // 2 - the test case must exist
        TargetStatus tcStatus = model.getStatus(testCase);
        if (tcStatus.status == TO_BE_DELETED || tcStatus.status == NOT_EXISTS) {
            logs.addEntry(
                    LogEntry.failure().forTarget(target).withMessage(Messages.ERROR_TC_NOT_FOUND).build());
        }

        // 3 - the project actually exists
        if (target.isWellFormed()) {
            TargetStatus projectStatus = model.getProjectStatus(target.getProject());
            if (projectStatus.getStatus() != Existence.EXISTS) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_PROJECT_NOT_EXIST)
                                .build());
            }
        }

        return logs;
    }

    LogTrain validateCallStep(
            TestStepTarget target,
            TestCaseTarget calledTestCase,
            CallStepParamsInfo paramInfos,
            ImportMode mode) {

        LogTrain logs = new LogTrain();

        // 1 - the target must exist and be valid
        String errorMessage = checkTestCaseExists(calledTestCase);

        if (errorMessage != null) {
            logMustExistAndBeValidCalledTest(target, mode, logs, errorMessage);
        } else {
            // 2 - there must be no cyclic calls
            if (model.wouldCreateCycle(target, calledTestCase)) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(
                                        Messages.ERROR_CYCLIC_STEP_CALLS,
                                        target.getTestCase().getPath(),
                                        calledTestCase.getPath())
                                .build());
            }

            // 3 - check a called dataset
            if (paramInfos.getParamMode() == ParameterAssignationMode.CALLED_DATASET) {
                String dsname = paramInfos.getCalledDatasetName();

                // 3.1 - if a dataset is specified, the name must not exceed the max limit
                if (dsname.length() > FacilityImplHelper.STD_TRUNCATE_SIZE) {
                    LogEntry entry =
                            LogEntry.warning()
                                    .forTarget(target)
                                    .withMessage(Messages.ERROR_MAX_SIZE, StepSheetColumn.TC_STEP_CALL_DATASET.name())
                                    .withImpact(Messages.IMPACT_MAX_SIZE)
                                    .build();

                    logs.addEntry(entry);
                }

                // 3.2 - if a dataset is specified, it must be owned by the called test case
                DatasetTarget dsTarget = new DatasetTarget(calledTestCase, dsname);
                if (model.datasetNotExist(dsTarget)) {
                    logs.addEntry(
                            LogEntry.warning()
                                    .forTarget(target)
                                    .withMessage(Messages.ERROR_DATASET_NOT_FOUND_ST)
                                    .withImpact(Messages.IMPACT_NO_CALL_DATASET)
                                    .build());
                }
            }
        }

        return logs;
    }

    private String checkTestCaseExists(TestCaseTarget calledTestCase) {

        TargetStatus calledStatus = model.getStatus(calledTestCase);

        String mustExistAndBeValidMessage = null;
        if (calledStatus.status == NOT_EXISTS || calledStatus.status == TO_BE_DELETED) {
            mustExistAndBeValidMessage = Messages.ERROR_CALLED_TC_NOT_FOUND;
        } else if (!calledTestCase.isWellFormed()) {
            mustExistAndBeValidMessage = Messages.ERROR_CALLED_STEP_WRONG_FORMAT;
        }

        return mustExistAndBeValidMessage;
    }

    // ********************* REQUIREMENTS CHECKS **********************//

    public void preVersionValidation(RequirementVersionInstruction instruction) {
        checkVersionPath(instruction);
        checkVersionName(instruction);
    }

    /** Check path to ensure that all element in path are not too long. Truncate if needed... */
    private void checkVersionPath(RequirementVersionInstruction instruction) {
        RequirementVersionTarget target = instruction.getTarget();
        if (!target.isWellFormed()) {
            return;
        }

        String path = target.getRequirement().getPath();
        String[] names = PathUtils.splitPath(path);
        for (int i = 1; i < names.length; i++) { // begin to 1 as first split is project name
            String name = names[i];
            if (name.length() > Sizes.NAME_MAX) {
                names[i] = StringUtils.abbreviate(name, Sizes.NAME_MAX);

                instruction.addLogEntry(
                        ImportStatus.WARNING, Messages.ERROR_MAX_SIZE, null, REQ_PATH.header);

                target.getRequirement().setPath(PathUtils.buildPathFromParts(names));
            }
        }
    }

    private void checkVersionName(RequirementVersionInstruction instruction) {
        RequirementVersion reqVersion = instruction.getRequirementVersion();

        String name = reqVersion.getName();
        if (name != null && name.length() > Sizes.NAME_MAX) {
            reqVersion.setName(StringUtils.abbreviate(name, Sizes.NAME_MAX));

            instruction.addLogEntry(
                    ImportStatus.WARNING,
                    Messages.ERROR_MAX_SIZE,
                    Messages.IMPACT_MAX_SIZE,
                    REQ_VERSION_NAME.header);
        }
    }

    public LogTrain createRequirementVersionChecks(
            RequirementVersionTarget target,
            RequirementVersion reqVersion,
            RequirementImportValidationBag validationBag) {
        LogTrain logs = new LogTrain();
        return basicReqVersionTests(target, reqVersion, logs, validationBag);
    }

    public void createHighLevelRequirementChecks(RequirementVersionTarget target, LogTrain logs) {
        highLevelRequirementCreateTests(target, logs);
    }

    public LogTrain updateRequirementChecks(
            RequirementVersionTarget target,
            RequirementVersion reqVersion,
            RequirementImportValidationBag validationBag) {
        LogTrain logs = new LogTrain();
        // 1 - For update checking if requirement version number value isn't empty and > 0
        checkRequirementVersionNumber(target, logs);
        if (logs.hasCriticalErrors()) {
            return logs;
        }

        basicReqVersionTests(target, reqVersion, logs, validationBag);
        return logs;
    }

    private void checkRequirementVersionNumber(RequirementVersionTarget target, LogTrain logs) {
        if (target.getVersion() == null || target.getVersion() < 1) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_REQUIREMENT_VERSION_INVALID)
                            .build());
        }
    }

    /**
     * Basics check commons to create and update requirements
     *
     * @param target
     * @param reqVersion
     * @param logs
     * @return
     */
    private LogTrain basicReqVersionTests(
            RequirementVersionTarget target,
            RequirementVersion reqVersion,
            LogTrain logs,
            RequirementImportValidationBag validationBag) {
        checkMalformedPath(target, logs);
        if (logs.hasCriticalErrors()) {
            return logs;
        }
        checkProjectExists(target, logs);
        checkVersionReference(target, reqVersion, logs);
        logs.append(checkCategoryAndFixIfNeeded(target, reqVersion, validationBag));
        return logs;
    }

    private void highLevelRequirementCreateTests(RequirementVersionTarget target, LogTrain logs) {
        RequirementTarget reqTarget = target.getRequirement();

        if (reqTarget.isHighLevel()) {
            checkPremiumPluginIsInstalled(target, logs, true);
            // stop checks here if premium plugin is not installed or path is not well formed (from
            // previous check)
            if (logs.hasCriticalErrors()) {
                return;
            }
            if (model.isRequirementChild(reqTarget)) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_HIGH_LEVEL_REQ_UNDER_ANOTHER_REQ, REQ_PATH.header)
                                .build());
            }
        }
    }

    public void highLevelRequirementUpdateTests(RequirementVersionTarget target, LogTrain logs) {
        if (logs.hasCriticalErrors()) {
            return;
        }
        RequirementTarget reqTarget = target.getRequirement();
        Requirement origRequirement =
                reqTarget.getId() != null ? reqFinderService.findRequirement(reqTarget.getId()) : null;
        if (origRequirement != null) {
            boolean needTestForUpdate = reqTarget.isHighLevel() != origRequirement.isHighLevel();

            basicHighLevelRequirementTests(target, logs, needTestForUpdate, false);
            requirementConvertTests(origRequirement, logs, target);
        }
    }

    private void requirementConvertTests(
            Requirement origRequirement, LogTrain logs, RequirementVersionTarget target) {
        if (!origRequirement.isHighLevel() && target.getRequirement().isHighLevel()) {
            checkReqAlreadyLinkedToHighLvlReq(origRequirement, logs, target);
        } else if (origRequirement.isHighLevel() && !target.getRequirement().isHighLevel()) {
            checkHighLvlReqHasLinkedLowLvlReqs(origRequirement, logs, target);
        }
    }

    private void checkReqAlreadyLinkedToHighLvlReq(
            Requirement origRequirement, LogTrain logs, RequirementVersionTarget target) {
        if (origRequirement.getHighLevelRequirement() != null) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.WARN_REQ_ALREADY_LINKED_TO_HIGH_LVL_REQ)
                            .withImpact(Messages.IMPACT_HIGH_LVL_REQ_REFERENCE_LOSS)
                            .build());
        }
    }

    private void checkHighLvlReqHasLinkedLowLvlReqs(
            Requirement origRequirement, LogTrain logs, RequirementVersionTarget target) {
        List<Requirement> linkedLowLevelReqs =
                highLevelRequirementService.findStandardRequirementsByHighLvlReqId(origRequirement.getId());
        if (linkedLowLevelReqs != null && !linkedLowLevelReqs.isEmpty()) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.WARN_HIGH_LVL_REQ_HAS_LINKED_LOW_LVL_REQS)
                            .withImpact(Messages.IMPACT_LINKED_LOW_LVL_REQS_LOSS)
                            .build());
        }
    }

    private void basicHighLevelRequirementTests(
            RequirementVersionTarget target, LogTrain logs, boolean needTest, boolean isCreateReq) {
        RequirementTarget reqTarget = target.getRequirement();
        if (reqTarget != null && needTest) {
            checkPremiumPluginIsInstalled(target, logs, isCreateReq);
            // stop checks here if premium plugin is not installed or path is not well formed (from
            // previous check)
            if (logs.hasCriticalErrors()) {
                return;
            }
            if (isChildRequirementByPath(target.getPath())) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_HIGH_LEVEL_REQ_UNDER_ANOTHER_REQ, REQ_PATH.header)
                                .build());
            }
        }
    }

    private void checkPremiumPluginIsInstalled(
            RequirementVersionTarget target, LogTrain logs, boolean isCreateReq) {
        if (!pluginFinderService.isPremiumPluginInstalled()) {
            if (isCreateReq) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_HIGH_LEVEL_REQ_NEEDS_PREMIUM_PLUGIN_FOR_CREATE)
                                .build());
            } else {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(
                                        Messages.ERROR_HIGH_LEVEL_REQ_NEEDS_PREMIUM_PLUGIN_FOR_UPDATE, REQ_PATH.header)
                                .build());
            }
        }
    }

    public boolean isChildRequirementByPath(String targetPath) {
        List<String> paths = PathUtils.scanPath(targetPath);
        if (paths.size() > 2) {
            String parentPath = paths.get(paths.size() - 2);
            RequirementTarget parentTarget = new RequirementTarget(parentPath);
            Long parentId = reqFinderService.findNodeIdByPath(parentPath);

            return isReqParentARequirement(parentId, parentTarget);
        }
        return false;
    }

    // Either parent already exists and it is a requirement OR parent does not exist, then it is
    // created in same import, and it is a requirement
    private boolean isReqParentARequirement(Long parentId, RequirementTarget parentTarget) {
        return (parentId != null && reqFinderService.findRequirement(parentId) != null)
                || (parentId == null && !model.isRequirementFolder(parentTarget));
    }

    private void checkVersionReference(
            RequirementVersionTarget target, RequirementVersion reqVersion, LogTrain logs) {
        String reference = reqVersion.getReference();
        if (!StringUtils.isBlank(reference) && reference.length() > Sizes.REFERENCE_MAX) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MAX_SIZE, REQ_VERSION_REFERENCE.header)
                            .withImpact(Messages.IMPACT_MAX_SIZE)
                            .build());
        }
    }

    private void checkProjectExists(RequirementVersionTarget target, LogTrain logs) {
        if (target.isWellFormed()) {
            TargetStatus projectStatus = model.getProjectStatus(target.getProject());
            if (projectStatus.getStatus() != Existence.EXISTS) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_PROJECT_NOT_EXIST)
                                .build());
            }
        }
    }

    private void checkMalformedPath(RequirementVersionTarget target, LogTrain logs) {
        if (!target.isWellFormed() || pathHasEmptyParts(target.getPath())) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MALFORMED_PATH, target.getPath())
                            .build());
        }
    }

    private boolean pathHasEmptyParts(String path) {
        String[] splits = PathUtils.splitPath(path);
        for (String split : splits) {
            if (split.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private void logMustExistAndBeValidCalledTest(
            TestStepTarget target, ImportMode mode, LogTrain logs, String message) {
        switch (mode) {
            case CREATE:
                logs.addEntry(
                        LogEntry.warning()
                                .forTarget(target)
                                .withMessage(message)
                                .withImpact(Messages.IMPACT_CALL_AS_ACTION_STEP)
                                .build());
                break;
            case UPDATE: // do default
            default:
                logs.addEntry(LogEntry.failure().forTarget(target).withMessage(message).build());
                break;
        }
    }

    LogTrain basicParameterChecks(ParameterTarget target) {
        return basicParameterChecks(
                target, TC_OWNER_PATH.name(), Messages.ERROR_PARAMETER_OWNER_NOT_FOUND);
    }

    public LogTrain basicParameterValueChecks(ParameterTarget target) {
        return basicParameterChecks(
                target, "TC_PARAMETER_OWNER_PATH", Messages.ERROR_DATASET_PARAM_OWNER_NOT_FOUND);
    }

    private LogTrain basicParameterChecks(
            ParameterTarget target, String fieldPathErrorArg, String ownerNotFoundMessage) {
        LogTrain logs = new LogTrain();

        // logging
        TestCaseTarget testCase = target.getOwner();

        basicTestCaseTargetCheck(testCase, logs, fieldPathErrorArg, ownerNotFoundMessage, target);

        basicParameterChecksValidateName(target, logs);

        return logs;
    }

    LogTrain basicDatasetCheck(DatasetTarget target) {

        LogTrain logs = new LogTrain();
        String fieldNameErrorArg = TC_DATASET_NAME.name();

        TestCaseTarget testCase = target.getTestCase();

        basicTestCaseTargetCheck(
                testCase, logs, testCase.getPath(), Messages.ERROR_TC_NOT_FOUND, target);

        // 4 - name has length between 1 and 255
        String name = target.getName();
        if (name != null && name.length() > 255) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MAX_SIZE, fieldNameErrorArg)
                            .withImpact(Messages.IMPACT_MAX_SIZE)
                            .build());
        }
        if (StringUtils.isBlank(name)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_FIELD_MANDATORY, fieldNameErrorArg)
                            .build());
        }

        return logs;
    }

    // ************************* private stuffs ****************************************

    private <T extends Target & WithPath> void basicTestCaseTargetCheck(
            TestCaseTarget testCase,
            LogTrain logs,
            String fieldPathErrorArg,
            String tcNotFoundMessage,
            T target) {
        // 1 - test case owner path must be supplied and and well formed
        if (!testCase.isWellFormed()) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MALFORMED_PATH, fieldPathErrorArg)
                            .build());
        }

        // 2 - the test case must exist
        TargetStatus tcStatus = model.getStatus(testCase);
        if (tcStatus.status == TO_BE_DELETED || tcStatus.status == NOT_EXISTS) {
            logs.addEntry(LogEntry.failure().forTarget(target).withMessage(tcNotFoundMessage).build());
        }

        // 3 - the project actually exists
        if (testCase.isWellFormed()) {
            TargetStatus projectStatus = model.getProjectStatus(target.getProject());
            if (projectStatus.getStatus() != Existence.EXISTS) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_PROJECT_NOT_EXIST)
                                .build());
            }
        }
    }

    private void basicParameterChecksValidateName(ParameterTarget target, LogTrain logs) {
        final String fieldNameErrorArg = TC_PARAM_NAME.name();

        String name = target.getName();

        if (StringUtils.isBlank(name)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_FIELD_MANDATORY, fieldNameErrorArg)
                            .build());
        } else {

            // 4 - name has length between 1 and 255
            if (name.length() > 255) {
                logs.addEntry(
                        LogEntry.warning()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_MAX_SIZE, fieldNameErrorArg)
                                .withImpact(Messages.IMPACT_MAX_SIZE)
                                .build());
            }

            // 5 - name does not contain forbidden characters
            String regex = Parameter.NAME_REGEXP;
            name = name.trim();
            target.setName(name);
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(name);
            if (!StringUtils.isBlank(name) && !m.matches() && name.length() < 256) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(
                                        Messages.ERROR_PARAMETER_CONTAINS_FORBIDDEN_CHARACTERS, fieldNameErrorArg)
                                .build());
            }
        }
    }

    /*
     * This method will check that, in case a nature and/or a type were supplied,
     * this element is consistent with the set of natures/types available in the
     * given project.
     *
     */
    private LogTrain checkNatureAndTypeAndFixIfNeeded(
            TestCaseTarget target, TestCase testCase, TestCaseImportValidationBag validationBag) {

        LogTrain logs = new LogTrain();

        if (target.isWellFormed()) {

            TargetStatus projectStatus = model.getProjectStatus(target.getProject());
            if (projectStatus.getStatus() == Existence.EXISTS) {

                // 2-1 nature, if specified, must be consistent with the natures of the target project
                if (!natureDefinedAndConsistent(testCase, validationBag.consistentNatures())) {
                    testCase.setNature(validationBag.defaultNature());
                    logs.addEntry(
                            LogEntry.warning()
                                    .forTarget(target)
                                    .withMessage(Messages.ERROR_INVALID_NATURE, target.getPath())
                                    .withImpact(Messages.IMPACT_DEFAULT_VALUE)
                                    .build());
                }

                if (!typeDefinedAndConsistent(testCase, validationBag.consistentTypes())) {
                    testCase.setType(validationBag.defaultType());
                    logs.addEntry(
                            LogEntry.warning()
                                    .forTarget(target)
                                    .withMessage(Messages.ERROR_INVALID_TYPE, target.getPath())
                                    .withImpact(Messages.IMPACT_DEFAULT_VALUE)
                                    .build());
                }
            }
        }

        return logs;
    }

    /*
     * This method will check that, in case a nature and/or a type were supplied,
     * this element is consistent with the set of natures/types available in the
     * given project.
     */
    private LogTrain checkCategoryAndFixIfNeeded(
            RequirementVersionTarget target,
            RequirementVersion reqVersion,
            RequirementImportValidationBag validationBag) {

        LogTrain logs = new LogTrain();

        if (target.isWellFormed()
                && !categoryDefinedAndConsistent(reqVersion, validationBag.consistentCategories())) {
            // category, if specified, must be consistent with the categories of the target project
            reqVersion.updateCategoryWhithoutCheck(validationBag.defaultCategory());
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_INVALID_CATEGORY, target.getPath())
                            .withImpact(Messages.IMPACT_DEFAULT_VALUE)
                            .build());
        }

        return logs;
    }

    /**
     * Check if a category defined in an imported {@link RequirementVersion} is defined and exist in
     * database.
     *
     * @param projectStatus the target project represented by a {@link ProjectTargetStatus}
     * @param reqVersion the {@link RequirementVersion} being imported
     * @return
     */
    private boolean categoryDefinedAndConsistent(
            RequirementVersion reqVersion, Set<String> consistentCategories) {
        InfoListItem category = reqVersion.getCategory();

        if (category == null) {
            return false;
        } else {
            return consistentCategories.contains(category.getCode());
        }
    }

    private boolean natureDefinedAndConsistent(TestCase testCase, Set<String> consistentNatures) {
        InfoListItem nature = testCase.getNature();

        if (nature == null) {
            return true;
        } else {
            return consistentNatures.contains(nature.getCode());
        }
    }

    private boolean typeDefinedAndConsistent(TestCase testCase, Set<String> consistentTypes) {
        InfoListItem type = testCase.getType();

        if (type == null) {
            return true;
        } else {
            return consistentTypes.contains(type.getCode());
        }
    }

    public void preCoverageValidation(CoverageInstruction instruction) {
        CoverageTarget target = instruction.getTarget();

        if (!target.isReqPathWellFormed()) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_MALFORMED_PATH, null, target.getReqPath());
        }

        if (target.getReqVersion() <= 0) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_REQUIREMENT_VERSION_INVALID, null);
        }

        if (!target.isTcPathWellFormed()) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_MALFORMED_PATH, null, target.getTcPath());
        }
    }

    public void preRequirementLinkValidation(RequirementLinkInstruction instruction) {
        RequirementLinkTarget target = instruction.getTarget();

        RequirementVersionTarget source = target.getSourceVersion();
        if (!source.isWellFormed()) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE,
                    Messages.ERROR_SOURCE_REQUIREMENT_PATH_MALFORMED,
                    null,
                    source.getPath());
        }

        RequirementVersionTarget dest = target.getDestVersion();
        if (!dest.isWellFormed()) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE,
                    Messages.ERROR_DEST_REQUIREMENT_PATH_MALFORMED,
                    null,
                    dest.getPath());
        }
    }
}
