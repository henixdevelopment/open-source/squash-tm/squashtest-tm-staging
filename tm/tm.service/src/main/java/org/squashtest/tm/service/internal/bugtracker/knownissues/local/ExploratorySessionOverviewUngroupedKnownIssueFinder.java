/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import org.jooq.DSLContext;
import org.jooq.Record6;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class ExploratorySessionOverviewUngroupedKnownIssueFinder
        extends ExploratorySessionOverviewKnownIssueFinder {

    public ExploratorySessionOverviewUngroupedKnownIssueFinder(DSLContext dsl) {
        super(dsl);
    }

    @Override
    protected SelectHavingStep<Record6<Long, Long, String, String, String, String>> applyGrouping(
            SelectConditionStep<Record6<Long, Long, String, String, String, String>> select) {
        return select.groupBy(
                PROJECT.PROJECT_ID, ISSUE.BUGTRACKER_ID, ISSUE.REMOTE_ISSUE_ID, ISSUE.ISSUE_ID);
    }
}
