/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.AI_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PAYLOAD_TEMPLATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.URL;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.AiServerDto;
import org.squashtest.tm.service.internal.repository.AiTestCaseGenerationDao;

@Repository
public class AiTestCaseGenerationDaoImpl implements AiTestCaseGenerationDao {
    private final DSLContext dslContext;

    public AiTestCaseGenerationDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public Long findTargetTestCaseLibraryId(Long projectId) {
        return dslContext
                .select(TEST_CASE_LIBRARY.TCL_ID)
                .from(TEST_CASE_LIBRARY)
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY.TCL_ID.eq(PROJECT.TCL_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchOneInto(Long.class);
    }

    @Override
    public AiServerDto findAiServerDtoByAiServerId(Long aiServerId) {
        return dslContext
                .select(
                        THIRD_PARTY_SERVER.URL.as(URL),
                        AI_SERVER.PAYLOAD_TEMPLATE.as(PAYLOAD_TEMPLATE),
                        AI_SERVER.SERVER_ID.as(ID),
                        AI_SERVER.JSON_PATH)
                .from(THIRD_PARTY_SERVER)
                .innerJoin(AI_SERVER)
                .on(THIRD_PARTY_SERVER.SERVER_ID.eq(AI_SERVER.SERVER_ID))
                .where(THIRD_PARTY_SERVER.SERVER_ID.eq(aiServerId))
                .fetchOneInto(AiServerDto.class);
    }

    @Override
    public AiServerDto findAiServerDtoByProjectId(long projectId) {
        return dslContext
                .select(
                        THIRD_PARTY_SERVER.URL.as(URL),
                        AI_SERVER.PAYLOAD_TEMPLATE.as(PAYLOAD_TEMPLATE),
                        AI_SERVER.SERVER_ID.as(ID),
                        AI_SERVER.JSON_PATH)
                .from(THIRD_PARTY_SERVER)
                .innerJoin(AI_SERVER)
                .on(AI_SERVER.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.AI_SERVER_ID.eq(AI_SERVER.SERVER_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchOneInto(AiServerDto.class);
    }
}
