/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE_OPTION;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.jooq.Record5;
import org.jooq.SelectUnionStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomFieldValueType;
import org.squashtest.tm.domain.customfield.MultiSelectField;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

@Repository
public class CustomFieldValueDisplayDaoImpl implements CustomFieldValueDisplayDao {

    @Inject private DSLContext dsl;

    @Override
    public List<CustomFieldValueDto> findCustomFieldValues(
            BindableEntity bindableEntity, Long entityId) {
        ListMultimap<Long, CustomFieldValueDto> customFieldValues =
                this.findCustomFieldValues(bindableEntity, Collections.singletonList(entityId));
        return customFieldValues.get(entityId);
    }

    @Override
    public ListMultimap<Long, CustomFieldValueDto> findCustomFieldValues(
            BindableEntity bindableEntity, List<Long> entityIds) {
        if (Objects.nonNull(bindableEntity) && Objects.nonNull(entityIds) && !entityIds.isEmpty()) {
            return fetchCustomFieldValues(bindableEntity, entityIds);
        } else {
            return ArrayListMultimap.create();
        }
    }

    private ListMultimap<Long, CustomFieldValueDto> fetchCustomFieldValues(
            BindableEntity bindableEntity, List<Long> entityIds) {
        ListMultimap<Long, CustomFieldValueDto> values = ArrayListMultimap.create();
        SelectUnionStep<Record5<Long, Long, String, Long, String>> selectRequest =
                craftSelectRequest(bindableEntity, entityIds);
        selectRequest
                .fetch()
                .into(CustomFieldValueDto.class)
                .forEach(
                        customFieldValueDto -> {
                            // Sanitize rich text fields
                            if (customFieldValueDto.getFieldType().equals(CustomFieldValueType.RTF.name())) {
                                final String sanitized = HTMLCleanupUtils.cleanHtml(customFieldValueDto.getValue());
                                customFieldValueDto.setValue(sanitized);
                            }

                            values.put(customFieldValueDto.getBoundEntityId(), customFieldValueDto);
                        });
        return values;
    }

    private SelectUnionStep<Record5<Long, Long, String, Long, String>> craftSelectRequest(
            BindableEntity bindableEntity, List<Long> entityIds) {
        EnumSet<CustomFieldValueType> customFieldValueTypes = EnumSet.allOf(CustomFieldValueType.class);

        Deque<SelectUnionStep<Record5<Long, Long, String, Long, String>>> clauses =
                customFieldValueTypes.stream()
                        .map(type -> this.getSelectClause(bindableEntity, type, entityIds))
                        .collect(Collectors.toCollection(ArrayDeque::new));

        SelectUnionStep<Record5<Long, Long, String, Long, String>> selectRequest = clauses.pop();
        while (clauses.peek() != null) {
            selectRequest.unionAll(clauses.pop());
        }
        return selectRequest;
    }

    private SelectUnionStep<Record5<Long, Long, String, Long, String>> getSelectClause(
            BindableEntity bindableEntity, CustomFieldValueType type, List<Long> entityIds) {
        if (type.isMultiValue()) {
            return getMultiValueClause(bindableEntity, type, entityIds);
        } else {
            return getSingleValueClause(bindableEntity, type, entityIds);
        }
    }

    private SelectUnionStep<Record5<Long, Long, String, Long, String>> getMultiValueClause(
            BindableEntity bindableEntity, CustomFieldValueType type, List<Long> entityIds) {
        Table customFieldValueOption =
                dsl.select(
                                CUSTOM_FIELD_VALUE_OPTION.CFV_ID,
                                CUSTOM_FIELD_VALUE_OPTION.LABEL,
                                CUSTOM_FIELD_VALUE_OPTION.POSITION)
                        .from(CUSTOM_FIELD_VALUE_OPTION)
                        .orderBy(CUSTOM_FIELD_VALUE_OPTION.CFV_ID, CUSTOM_FIELD_VALUE_OPTION.POSITION)
                        .asTable("CUSTOM_FIELD_VALUE_OPTION");

        return dsl.select(
                        CUSTOM_FIELD_VALUE.CF_ID.as(RequestAliasesConstants.CUF_ID),
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID,
                        DSL.listAgg(type.getValueColumn(), MultiSelectField.SEPARATOR)
                                .withinGroupOrderBy(CUSTOM_FIELD_VALUE_OPTION.POSITION)
                                .as(RequestAliasesConstants.VALUE),
                        CUSTOM_FIELD_VALUE.CFV_ID.as(RequestAliasesConstants.ID),
                        CUSTOM_FIELD_VALUE.FIELD_TYPE)
                .from(CUSTOM_FIELD_VALUE)
                .leftJoin(customFieldValueOption)
                .on(CUSTOM_FIELD_VALUE.CFV_ID.eq(CUSTOM_FIELD_VALUE_OPTION.CFV_ID))
                .where(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(bindableEntity.name()))
                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.in(entityIds))
                .and(CUSTOM_FIELD_VALUE.FIELD_TYPE.eq(type.getDiscriminatorValue()))
                .groupBy(CUSTOM_FIELD_VALUE.CFV_ID);
    }

    private SelectUnionStep<Record5<Long, Long, String, Long, String>> getSingleValueClause(
            BindableEntity bindableEntity, CustomFieldValueType type, List<Long> entityIds) {
        return dsl.select(
                        CUSTOM_FIELD_VALUE.CF_ID.as(RequestAliasesConstants.CUF_ID),
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID,
                        type.getValueColumn().as(RequestAliasesConstants.VALUE),
                        CUSTOM_FIELD_VALUE.CFV_ID.as(RequestAliasesConstants.ID),
                        CUSTOM_FIELD_VALUE.FIELD_TYPE)
                .from(CUSTOM_FIELD_VALUE)
                .where(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(bindableEntity.name()))
                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.in(entityIds))
                .and(CUSTOM_FIELD_VALUE.FIELD_TYPE.eq(type.getDiscriminatorValue()));
    }
}
