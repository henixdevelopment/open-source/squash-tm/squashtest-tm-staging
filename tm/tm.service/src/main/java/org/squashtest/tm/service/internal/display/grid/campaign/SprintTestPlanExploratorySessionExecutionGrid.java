/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION_EVENT;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_FULL_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ORDER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ISSUE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NOTE_TYPES;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REVIEWED;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.RUNNING_STATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TASK_DIVISION;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.execution.ExploratoryExecutionEventType;
import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

public class SprintTestPlanExploratorySessionExecutionGrid extends AbstractGrid {

    private static final String EXEC_ID = "EXEC_ID";

    private final Long overviewId;

    public SprintTestPlanExploratorySessionExecutionGrid(Long overviewId) {
        this.overviewId = overviewId;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(EXEC_ID).as(EXECUTION_ID)),
                new GridColumn(DSL.field(LAST_EXECUTED_ON, Timestamp.class)),
                new GridColumn(DSL.field(ASSIGNEE_ID)),
                new GridColumn(DSL.field(ASSIGNEE_LOGIN)),
                new GridColumn(DSL.field(ASSIGNEE_FULL_NAME)),
                new GridColumn(DSL.field(TASK_DIVISION)),
                new GridColumn(DSL.field(REVIEWED)),
                new GridColumn(DSL.field(NOTE_TYPES)),
                new GridColumn(DSL.field(EXECUTION_ORDER)),
                new GridColumn(DSL.field(PROJECT_ID)),
                new GridColumn(countIssue().as(ISSUE_COUNT)),
                new GridColumn(DSL.field(RUNNING_STATE)));
    }

    private Field<String> findSessionStatus() {
        return DSL.select(
                        when(
                                        EXPLORATORY_EXECUTION_EVENT.EVENT_TYPE.eq(
                                                ExploratoryExecutionEventType.STOP.name()),
                                        ExploratoryExecutionRunningState.STOPPED.name())
                                .otherwise(
                                        when(
                                                        EXPLORATORY_EXECUTION_EVENT.EVENT_TYPE.in(
                                                                ExploratoryExecutionEventType.RESUME.name(),
                                                                ExploratoryExecutionEventType.START.name(),
                                                                ExploratoryExecutionEventType.PAUSE.name()),
                                                        ExploratoryExecutionRunningState.RUNNING.name())
                                                .otherwise(ExploratoryExecutionRunningState.NEVER_STARTED.name()))
                                .as(RUNNING_STATE))
                .from(EXPLORATORY_EXECUTION_EVENT)
                .where(EXPLORATORY_EXECUTION_EVENT.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .orderBy(EXPLORATORY_EXECUTION_EVENT.EVENT_DATE.desc())
                .limit(1)
                .asField();
    }

    private Field<Integer> countIssue() {
        return DSL.select(DSL.countDistinct(ISSUE.REMOTE_ISSUE_ID))
                .from(ISSUE)
                .innerJoin(EXECUTION_ISSUES_CLOSURE)
                .on(EXECUTION_ISSUES_CLOSURE.ISSUE_ID.eq(ISSUE.ISSUE_ID))
                .where(DSL.field(EXEC_ID).eq(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID))
                .asField(ISSUE_COUNT);
    }

    @Override
    protected Table<?> getTable() {
        return DSL.select(
                        EXECUTION.EXECUTION_ID.as(EXEC_ID), // Alias is used to avoid ambiguity in countIssue()
                        EXECUTION.LAST_EXECUTED_ON,
                        EXPLORATORY_EXECUTION.ASSIGNEE_ID,
                        EXPLORATORY_EXECUTION.TASK_DIVISION,
                        CORE_USER.LOGIN.as(ASSIGNEE_LOGIN),
                        EXPLORATORY_EXECUTION.REVIEWED,
                        EXECUTION.EXECUTION_ORDER,
                        PROJECT.PROJECT_ID,
                        DSL.groupConcat(SESSION_NOTE.KIND).as(NOTE_TYPES),
                        TestPlanGridHelpers.formatAssigneeFullName(),
                        DSL.coalesce(findSessionStatus(), ExploratoryExecutionRunningState.NEVER_STARTED.name())
                                .as(RUNNING_STATE))
                .from(EXECUTION)
                .innerJoin(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXECUTION.TEST_PLAN_ITEM_ID))
                .innerJoin(TEST_PLAN)
                .on(TEST_PLAN.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.CL_ID.eq(TEST_PLAN.CL_ID))
                .innerJoin(EXPLORATORY_EXECUTION)
                .on(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .leftJoin(SESSION_NOTE)
                .on(SESSION_NOTE.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                .leftJoin(CORE_USER)
                .on(CORE_USER.PARTY_ID.eq(EXPLORATORY_EXECUTION.ASSIGNEE_ID))
                .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                .on(
                        EXPLORATORY_SESSION_OVERVIEW
                                .TEST_PLAN_ITEM_ID
                                .eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID)
                                .and(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId)))
                .groupBy(
                        EXECUTION.EXECUTION_ID,
                        EXPLORATORY_EXECUTION.ASSIGNEE_ID,
                        EXPLORATORY_EXECUTION.TASK_DIVISION,
                        EXPLORATORY_EXECUTION.REVIEWED,
                        EXECUTION.EXECUTION_ORDER,
                        PROJECT.PROJECT_ID,
                        CORE_USER.PARTY_ID)
                .asTable();
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(EXECUTION_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return DSL.field(PROJECT_ID);
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return DSL.field(EXECUTION_ORDER).asc();
    }
}
