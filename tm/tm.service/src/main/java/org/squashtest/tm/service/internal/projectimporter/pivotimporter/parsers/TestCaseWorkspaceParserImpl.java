/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter.parsers;

import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.bdd.Keyword;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.SquashCustomFieldInfo;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.ActionTestStepToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.CalledTestCaseStepToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.DatasetParamToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.DatasetParamValueToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.DatasetToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.KeywordTestStepToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.TestCaseToImport;
import org.squashtest.tm.service.internal.projectimporter.pivotimporter.PivotFormatLoggerHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.TestCaseWorkspaceParser;

@Service("TestCaseWorkspaceParser")
public class TestCaseWorkspaceParserImpl implements TestCaseWorkspaceParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestCaseWorkspaceParser.class);

    @Override
    public TestCaseToImport parseTestCase(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        TestCaseToImport testCaseToImport = new TestCaseToImport();

        try {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> testCaseToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.TEST_CASE_KIND ->
                            testCaseToImport.setTestCaseKind(jsonParser.getText());
                    case JsonImportField.NAME -> testCaseToImport.setName(jsonParser.getText());
                    case JsonImportField.DESCRIPTION -> testCaseToImport.setDescription(jsonParser.getText());
                    case JsonImportField.REFERENCE -> testCaseToImport.setReference(jsonParser.getText());
                    case JsonImportField.IMPORTANCE ->
                            testCaseToImport.setImportance(TestCaseImportance.valueOf(jsonParser.getText()));
                    case JsonImportField.STATUS ->
                            testCaseToImport.setStatus(TestCaseStatus.valueOf(jsonParser.getText()));
                    case JsonImportField.NATURE -> testCaseToImport.setNature(jsonParser.getText());
                    case JsonImportField.TYPE -> testCaseToImport.setType(jsonParser.getText());
                    case JsonImportField.CUSTOM_FIELDS ->
                            testCaseToImport.setCustomFields(
                                    PivotJsonParsingHelper.getCustomFieldValues(
                                            jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                    case JsonImportField.DATASET_PARAMS -> handleDatasetParams(jsonParser, testCaseToImport);
                    case JsonImportField.DATASETS -> handleDataSets(jsonParser, testCaseToImport);
                    case JsonImportField.VERIFIED_REQUIREMENTS ->
                            testCaseToImport.setVerifiedRequirementIds(
                                    PivotJsonParsingHelper.getArrayStringValues(jsonParser));
                    case JsonImportField.PREREQUISITE ->
                            testCaseToImport.setPrerequisite(jsonParser.getText());
                    case JsonImportField.SCRIPT -> testCaseToImport.setScript(jsonParser.getText());
                    case JsonImportField.CHARTER -> testCaseToImport.setCharter(jsonParser.getText());
                    case JsonImportField.SESSION_DURATION ->
                            testCaseToImport.setSessionDuration(jsonParser.getIntValue());
                    case JsonImportField.ACTION_STEPS ->
                            handleActionTestSteps(
                                    jsonParser, testCaseToImport, pivotImportMetadata.getCustomFieldIdsMap());
                    case JsonImportField.KEYWORD_STEPS -> handleKeyWordSteps(jsonParser, testCaseToImport);
                    case JsonImportField.PARENT_TYPE ->
                            testCaseToImport.setParentType(EntityType.valueOf(jsonParser.getText()));
                    case JsonImportField.PARENT_ID -> testCaseToImport.setParentId(jsonParser.getText());
                    case JsonImportField.ATTACHMENTS ->
                            testCaseToImport.setAttachments(AttachmentParserHelper.parseAttachments(jsonParser));
                    default -> {
                        // continue parsing
                    }
                }
            }

            PivotFormatLoggerHelper.logParsingSuccessForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.TEST_CASE,
                    testCaseToImport.getName(),
                    testCaseToImport.getInternalId(),
                    pivotFormatImport);

        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.TEST_CASE,
                    testCaseToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }
        return testCaseToImport;
    }

    private void handleDatasetParams(JsonParser jsonParser, TestCaseToImport testCase)
            throws IOException {
        List<DatasetParamToImport> datasetParams = new ArrayList<>();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                parseDatasetParam(jsonParser, datasetParams);
            }
        }
        testCase.setDatasetParams(datasetParams);
    }

    private void parseDatasetParam(JsonParser jsonParser, List<DatasetParamToImport> datasetParams)
            throws IOException {
        DatasetParamToImport datasetParam = new DatasetParamToImport();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            switch (fieldName) {
                case JsonImportField.ID -> datasetParam.setInternalId(jsonParser.getText());
                case JsonImportField.NAME -> datasetParam.setName(jsonParser.getText());
                case JsonImportField.DESCRIPTION -> datasetParam.setDescription(jsonParser.getText());
                default -> {
                    // continue parsing
                }
            }
        }
        datasetParams.add(datasetParam);
    }

    private void handleDataSets(JsonParser jsonParser, TestCaseToImport testCase) throws IOException {
        List<DatasetToImport> dataSets = new ArrayList<>();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                parseDataSet(jsonParser, dataSets);
            }
        }
        testCase.setDataSets(dataSets);
    }

    private void parseDataSet(JsonParser jsonParser, List<DatasetToImport> dataSets)
            throws IOException {
        DatasetToImport dataset = new DatasetToImport();
        List<DatasetParamValueToImport> datasetParamValues = new ArrayList<>();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            switch (fieldName) {
                case JsonImportField.ID -> dataset.setInternalId(jsonParser.getText());
                case JsonImportField.NAME -> dataset.setName(jsonParser.getText());
                case JsonImportField.PARAM_VALUES ->
                        handleDatasetParamValues(jsonParser, datasetParamValues);
                default -> {
                    // continue parsing
                }
            }
        }
        dataset.setParamValues(datasetParamValues);
        dataSets.add(dataset);
    }

    private void handleDatasetParamValues(
            JsonParser jsonParser, List<DatasetParamValueToImport> datasetParamValues)
            throws IOException {
        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                parseDatasetParamValue(jsonParser, datasetParamValues);
            }
        }
    }

    private void parseDatasetParamValue(
            JsonParser jsonParser, List<DatasetParamValueToImport> datasetParamValues)
            throws IOException {

        DatasetParamValueToImport datasetParamValue = new DatasetParamValueToImport();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            if (JsonImportField.PARAM_ID.equals(fieldName)) {
                datasetParamValue.setInternalParamId(jsonParser.getText());
            } else if (JsonImportField.VALUE.equals(fieldName)) {
                datasetParamValue.setValue(jsonParser.getText());
            }
        }
        datasetParamValues.add(datasetParamValue);
    }

    private void handleKeyWordSteps(JsonParser jsonParser, TestCaseToImport testCase)
            throws IOException {
        List<KeywordTestStepToImport> keywordSteps = new ArrayList<>();

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                parseKeywordStep(jsonParser, keywordSteps);
            }
        }
        testCase.setKeywordTestSteps(keywordSteps);
    }

    private void parseKeywordStep(JsonParser jsonParser, List<KeywordTestStepToImport> keywordSteps)
            throws IOException {
        KeywordTestStepToImport keywordTestStepToImport = new KeywordTestStepToImport();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            switch (fieldName) {
                case JsonImportField.ID -> keywordTestStepToImport.setInternalId(jsonParser.getText());
                case JsonImportField.KEYWORD ->
                        keywordTestStepToImport.setKeyword(Keyword.valueOf(jsonParser.getText()));
                case JsonImportField.ACTION_WORD ->
                        keywordTestStepToImport.setActionWord(jsonParser.getText());
                case JsonImportField.DOC_STRING ->
                        keywordTestStepToImport.setDocString(jsonParser.getText());
                case JsonImportField.COMMENT -> keywordTestStepToImport.setComment(jsonParser.getText());
                case JsonImportField.DATA_TABLE ->
                        keywordTestStepToImport.setDataTable(jsonParser.getText());
                default -> {
                    // continue parsing
                }
            }
        }
        keywordSteps.add(keywordTestStepToImport);
    }

    private void handleActionTestSteps(
            JsonParser jsonParser,
            TestCaseToImport testCaseToImport,
            Map<String, SquashCustomFieldInfo> customFieldIdsMap)
            throws IOException {
        List<ActionTestStepToImport> actionTestStepToImports = new ArrayList<>();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                parseActionTestStep(jsonParser, actionTestStepToImports, customFieldIdsMap);
            }
        }
        testCaseToImport.setActionTestSteps(actionTestStepToImports);
    }

    private void parseActionTestStep(
            JsonParser jsonParser,
            List<ActionTestStepToImport> actionTestStepToImports,
            Map<String, SquashCustomFieldInfo> customFieldIdsMap)
            throws IOException {
        ActionTestStepToImport actionTestStepToImport = new ActionTestStepToImport();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            switch (fieldName) {
                case JsonImportField.ID -> actionTestStepToImport.setInternalId(jsonParser.getText());
                case JsonImportField.ACTION -> actionTestStepToImport.setAction(jsonParser.getText());
                case JsonImportField.EXPECTED_RESULT ->
                        actionTestStepToImport.setExpectedResult(jsonParser.getText());
                case JsonImportField.CUSTOM_FIELDS ->
                        actionTestStepToImport.setCustomFields(
                                PivotJsonParsingHelper.getCustomFieldValues(jsonParser, customFieldIdsMap));
                case JsonImportField.VERIFIED_REQUIREMENTS ->
                        actionTestStepToImport.setVerifiedRequirementIds(
                                PivotJsonParsingHelper.getArrayStringValues(jsonParser));
                case JsonImportField.ATTACHMENTS ->
                        actionTestStepToImport.setAttachments(
                                AttachmentParserHelper.parseAttachments(jsonParser));
                default -> {
                    // continue parsing
                }
            }
        }
        actionTestStepToImports.add(actionTestStepToImport);
    }

    @Override
    public CalledTestCaseStepToImport parseCalledTestCase(
            JsonParser jsonParser, PivotFormatImport pivotFormatImport) {
        CalledTestCaseStepToImport calledTestCaseStepToImport = new CalledTestCaseStepToImport();
        try {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();
                switch (fieldName) {
                    case JsonImportField.ID -> calledTestCaseStepToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.CALLER_ID ->
                            calledTestCaseStepToImport.setCallerId(jsonParser.getText());
                    case JsonImportField.CALLED_ID ->
                            calledTestCaseStepToImport.setCalledTestCaseInternalId(jsonParser.getText());
                    case JsonImportField.INDEX ->
                            calledTestCaseStepToImport.setIndex(jsonParser.getIntValue());
                    case JsonImportField.PARAMETER_ASSIGNATION_MODE ->
                            calledTestCaseStepToImport.setParameterAssignationMode(
                                    ParameterAssignationMode.valueOf(jsonParser.getText()));
                    case JsonImportField.DATASET_ID ->
                            calledTestCaseStepToImport.setInternalDatasetId(jsonParser.getText());
                    default -> {
                        // continue parsing
                    }
                }
            }
            PivotFormatLoggerHelper.logParsingSuccessForUnnamedEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.CALLED_TEST_CASE,
                    calledTestCaseStepToImport.getInternalId(),
                    pivotFormatImport);

        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.CALLED_TEST_CASE,
                    calledTestCaseStepToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }

        return calledTestCaseStepToImport;
    }
}
