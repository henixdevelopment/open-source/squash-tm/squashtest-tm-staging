/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.model;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.AbstractGenericItem;

public class XrayImportModel {
    private String pivotFileName;
    private String pivotLogFileName;
    private final List<EntityCount> entityCounts = new ArrayList<>();

    // GETTER - SETTER
    public String getPivotFileName() {
        return pivotFileName;
    }

    public void setPivotFileName(String pivotFileName) {
        this.pivotFileName = pivotFileName;
    }

    public String getPivotLogFileName() {
        return pivotLogFileName;
    }

    public void setPivotLogFileName(String pivotLogFileName) {
        this.pivotLogFileName = pivotLogFileName;
    }

    public List<EntityCount> getEntityCounts() {
        return entityCounts;
    }

    public static class EntityCount {
        private String i18nKey;
        private EntityType entityType;
        private int entitySuccesses = 0;
        private int entityWarnings = 0;
        private int entityFailures = 0;

        public EntityCount(EntityType entityType) {
            this.entityType = entityType;
            this.i18nKey = entityType.getI18nKey();
        }

        public String getI18nKey() {
            return i18nKey;
        }

        public void setI18nKey(String i18nKey) {
            this.i18nKey = i18nKey;
        }

        public EntityType getEntityType() {
            return entityType;
        }

        public void setEntityType(EntityType entityType) {
            this.entityType = entityType;
        }

        public int getEntitySuccesses() {
            return entitySuccesses;
        }

        public int getEntityWarnings() {
            return entityWarnings;
        }

        public int getEntityFailures() {
            return entityFailures;
        }

        public <T extends AbstractGenericItem> void countEntity(T item) {
            switch (item.getItemStatus()) {
                case SUCCESS -> entitySuccesses++;
                case WARNING -> entityWarnings++;
                default -> entityFailures++;
            }
        }
    }

    public enum EntityType {
        TEST_CASE(
                "sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.generate-pivot.test-case"),
        CALLED_TEST_CASE(
                "sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.generate-pivot.call-test-case"),
        CAMPAIGN(
                "sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.generate-pivot.campaign"),
        ITERATION(
                "sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.generate-pivot.iteration");

        private final String i18nKey;

        EntityType(String i18nKey) {
            this.i18nKey = i18nKey;
        }

        public String getI18nKey() {
            return i18nKey;
        }
    }
}
