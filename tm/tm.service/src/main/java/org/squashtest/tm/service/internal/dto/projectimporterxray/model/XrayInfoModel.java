/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception.XrayParsingException;

public class XrayInfoModel {
    private final Map<String, Entity> entities = new HashMap<>();
    private final Set<String> keys = new HashSet<>();

    public Map<String, Entity> getEntities() {
        return entities;
    }

    public void addEntity(Entity entity, String filename) {
        if (Objects.nonNull(entity)) {
            String type = entity.getType();
            if (!entities.containsKey(type)) {
                entities.put(type, entity);
            } else {
                entities.get(type).mergingEntity(entity);
            }
            if (keys.contains(entity.getKey())) {
                throw new XrayParsingException(filename, "Duplicate jira keys found in the import.");
            } else {
                keys.add(entity.getKey());
            }
        }
    }

    public static class Entity {
        private String key;
        private String type;
        private int entityCount = 1;
        private final Set<String> status = new HashSet<>();
        private final Set<String> priorities = new HashSet<>();

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public int getEntityCount() {
            return entityCount;
        }

        public Set<String> getStatus() {
            return status;
        }

        public Set<String> getPriorities() {
            return priorities;
        }

        public void mergingEntity(Entity entity) {
            entityCount += entity.getEntityCount();
            status.addAll(entity.getStatus());
            priorities.addAll(entity.getPriorities());
        }
    }
}
