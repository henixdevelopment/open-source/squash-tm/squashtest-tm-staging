/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.remotesynchronisation;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.exception.sync.PathAlreadyInUseException;
import org.squashtest.tm.exception.sync.PathContainsASprintGroupException;
import org.squashtest.tm.service.internal.repository.CustomRemoteSynchronisationDao;
import org.squashtest.tm.service.internal.utils.JSONUtils;
import org.squashtest.tm.service.remotesynchronisation.RemoteSynchronisationService;

@Service("RemoteSynchronisationService")
public class RemoteSynchronisationServiceImpl implements RemoteSynchronisationService {

    public static final String SYNCHRONISATION_PATH = "synchronisationPath";
    public static final String SPRINT_SYNCHRONISATION_PATH = "sprintSynchronisationPath";

    @Inject private CustomRemoteSynchronisationDao customRemoteSynchronisationDao;

    @Override
    public void checkPathAvailability(String projectName, String synchronisationPath) {
        Set<String> existingPaths = getExistingPaths(projectName, SYNCHRONISATION_PATH);
        synchronisationPath = PathUtils.removeAllUnnecessarySpaces(synchronisationPath);
        for (String existingPath : existingPaths) {
            if (synchronisationPath.equals(existingPath)) {
                throw new PathAlreadyInUseException(SYNCHRONISATION_PATH);
            }
        }
    }

    @Override
    public void checkSprintPathAvailability(String projectName, String sprintSynchronisationPath) {
        Set<String> existingPaths = getExistingPaths(projectName, SPRINT_SYNCHRONISATION_PATH);
        sprintSynchronisationPath = PathUtils.removeAllUnnecessarySpaces(sprintSynchronisationPath);
        for (String existingPath : existingPaths) {
            if (sprintSynchronisationPath.equals(existingPath)) {
                throw new PathAlreadyInUseException(SPRINT_SYNCHRONISATION_PATH);
            }
            if ((sprintSynchronisationPath + "/").startsWith(existingPath + "/")) {
                throw new PathContainsASprintGroupException(SPRINT_SYNCHRONISATION_PATH);
            }
        }
    }

    private Set<String> getExistingPaths(String projectName, String key) {
        List<String> allOptions =
                customRemoteSynchronisationDao.findAllRemoteSynchronisationOptionsFromProjectName(
                        projectName);
        return allOptions.stream().map(json -> getCleanPath(json, key)).collect(Collectors.toSet());
    }

    private String getCleanPath(String json, String key) {
        String valueFromKey = JSONUtils.getValueFromKey(json, key);
        return PathUtils.removeAllUnnecessarySpaces(valueFromKey);
    }
}
