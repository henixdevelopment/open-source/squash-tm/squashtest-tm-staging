/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customreport;

import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.customreport.CustomReportFolder;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportNodeType;
import org.squashtest.tm.domain.customreport.CustomReportTreeEntityVisitor;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.service.customreport.CustomReportCustomExportModificationService;
import org.squashtest.tm.service.customreport.CustomReportCustomExportService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomExportWorkbenchData;

@Service
@Transactional
public class CustomReportCustomExportModificationServiceImpl
        implements CustomReportCustomExportModificationService {

    @Inject private CustomReportLibraryNodeService reportLibraryNodeService;
    @Inject private CustomReportCustomExportService customExportService;

    @Override
    @Transactional(readOnly = true)
    public CustomExportWorkbenchData getWorkbenchData(Long customReportLibraryNodeId) {
        CustomExportWorkbenchData data = new CustomExportWorkbenchData();

        CustomReportLibraryNode node =
                reportLibraryNodeService.findCustomReportLibraryNodeById(customReportLibraryNodeId);
        data.setProjectId(node.getLibrary().getProject().getId());
        data.setContainerId(findContainerId(node));

        if (node.getEntityType().getTypeName().equals(CustomReportNodeType.CUSTOM_EXPORT_NAME)) {
            data.setCustomExport(
                    customExportService.findCustomExportDtoByNodeId(customReportLibraryNodeId));
        }

        return data;
    }

    // TODO PCK COPYPASTA
    private Long findContainerId(CustomReportLibraryNode node) {
        final Long[] customReportLibraryNodeIds = new Long[1];
        CustomReportTreeEntityVisitor treeEntityVisitor =
                new CustomReportTreeEntityVisitor() {
                    @Override
                    public void visit(CustomReportFolder crf) {
                        customReportLibraryNodeIds[0] = node.getId();
                    }

                    @Override
                    public void visit(CustomReportLibrary crl) {
                        customReportLibraryNodeIds[0] = node.getId();
                    }

                    @Override
                    public void visit(CustomReportDashboard crf) {
                        customReportLibraryNodeIds[0] = node.getParent().getId();
                    }

                    @Override
                    public void visit(ChartDefinition chartDefinition) {
                        customReportLibraryNodeIds[0] = node.getParent().getId();
                    }

                    @Override
                    public void visit(ReportDefinition reportDefinition) {
                        customReportLibraryNodeIds[0] = node.getParent().getId();
                    }

                    @Override
                    public void visit(CustomReportCustomExport crce) {
                        customReportLibraryNodeIds[0] = node.getParent().getId();
                    }
                };
        node.getEntity().accept(treeEntityVisitor);

        // Return the container id
        return customReportLibraryNodeIds[0];
    }

    @Override
    public void updateCustomExport(
            Long customExportId, CustomReportCustomExport updatedCustomExport) {
        CustomReportCustomExport customExport =
                reportLibraryNodeService.findCustomExportByNodeId(customExportId);
        if (!customExport.getName().equals(updatedCustomExport.getName())) {
            reportLibraryNodeService.renameNode(customExportId, updatedCustomExport.getName());
        }
        customExport.setScope(updatedCustomExport.getScope());
        customExport.setColumns(updatedCustomExport.getColumns());
    }
}
