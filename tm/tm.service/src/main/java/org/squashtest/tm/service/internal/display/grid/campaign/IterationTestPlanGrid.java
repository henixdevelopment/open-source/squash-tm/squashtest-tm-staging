/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.coalesce;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_FULL_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_TO_BLOCKING_MILESTONE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_LATEST_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LATEST_EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_LABELS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MAX_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MIN_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TC_KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_DELETED;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_SUITES;

import java.util.Arrays;
import java.util.List;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.jooq.domain.tables.records.IterationTestPlanItemRecord;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;

public class IterationTestPlanGrid extends AbstractTestPlanGrid<IterationTestPlanItemRecord> {

    private final Long iterationId;
    private final IterationDisplayDao iterationDisplayDao;
    private final PermissionEvaluationService permissionEvaluationService;

    public IterationTestPlanGrid(
            Long iterationId,
            String userLoginToRestrictTo,
            IterationDisplayDao iterationDisplayDao,
            PermissionEvaluationService permissionEvaluationService) {
        super(userLoginToRestrictTo);
        this.iterationId = iterationId;
        this.iterationDisplayDao = iterationDisplayDao;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    @Override
    protected TableField<IterationTestPlanItemRecord, Long> getItemIdColumn() {
        return ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID;
    }

    @Override
    protected TableField<IterationTestPlanItemRecord, Long> getAssigneeIdColumn() {
        return ITERATION_TEST_PLAN_ITEM.USER_ID;
    }

    @Override
    protected TableField<IterationTestPlanItemRecord, Long> getTestCaseIdColumn() {
        return ITERATION_TEST_PLAN_ITEM.TCLN_ID;
    }

    @Override
    protected Table<IterationTestPlanItemRecord> getItemTable() {
        return ITERATION_TEST_PLAN_ITEM;
    }

/*
         If no filters are applied and the user can read unassigned items, we directly retrieve the count
         using a simpler, faster method. When filters are present, we must use getRows() to apply them,
         but this approach is significantly slower due to the complex query and filtering logic.
 */
    @Override
    protected Long countRows(DSLContext dslContext, GridRequest request) {

        if (request.getFilterValues().isEmpty() && currentUserCanReadUnassigned(iterationId)) {
            return Long.valueOf(
                    iterationDisplayDao.getNbTestPlanItem(iterationId, userLoginToRestrictTo));
        } else {
            return super.countRows(dslContext, request);
        }
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID),
                new GridColumn(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON),
                new GridColumn(findItpiWithLatestExecution().field(LATEST_EXECUTION_ID)),
                new GridColumn(findItpiWithLatestExecution().field(LAST_EXECUTION_MODE)),
                new GridColumn(findItpiWithLatestExecution().field(EXECUTION_LATEST_STATUS)),
                new GridColumn(findItpiWithLatestExecution().field(EXECUTION_LAST_EXECUTED_ON)),
                new GridColumn(ITERATION_TEST_PLAN_ITEM.USER_ID.as(ASSIGNEE_ID)),
                new GridColumn(ITEM_TEST_PLAN_LIST.ITERATION_ID),
                new GridColumn(TEST_CASE_LIBRARY_NODE.TCLN_ID.as(TEST_CASE_ID)),
                new GridColumn(getTestCaseKind().field(TC_KIND)),
                new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as(TEST_CASE_NAME), TEST_CASE_LIBRARY_NODE.NAME),
                new GridColumn(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID)),
                new GridColumn(PROJECT.NAME.as(PROJECT_NAME), PROJECT.NAME),
                new GridColumn(TEST_CASE.REFERENCE.as(TEST_CASE_REFERENCE), TEST_CASE.REFERENCE),
                new LevelEnumColumn(TestCaseImportance.class, TEST_CASE.IMPORTANCE),
                new GridColumn(getUser().field(ASSIGNEE_FULL_NAME)),
                new GridColumn(getUser().field(ASSIGNEE_LOGIN)), // this field is used only for filters
                new GridColumn(DATASET.NAME.as(DATASET_NAME), DATASET.NAME),
                new GridColumn(
                        coalesce(getAutomationFields().field(INFERRED_EXECUTION_MODE), EXECUTION.EXECUTION_MODE)
                                .as(INFERRED_EXECUTION_MODE)),
                new GridColumn(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID),
                new GridColumn(getTestSuites().field(TEST_SUITES)),
                new LevelEnumColumn(ExecutionStatus.class, ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS),
                new GridColumn(DSL.field(MILESTONE_MIN_DATE)),
                new GridColumn(DSL.field(MILESTONE_MAX_DATE)),
                new GridColumn(DSL.field(MILESTONE_LABELS)),
                new GridColumn(DSL.field(BOUND_TO_BLOCKING_MILESTONE)),
                new GridColumn(TEST_CASE_LIBRARY_NODE.TCLN_ID.isNull().as(TEST_CASE_DELETED)));
    }

    @Override
    protected Table<?> getTable() {
        final SelectHavingStep<?> getAutomationFields = getAutomationFields();
        final SelectHavingStep<?> getUser = getUser();
        final SelectHavingStep<?> findItpiWithLatestExecution = findItpiWithLatestExecution();
        final SelectHavingStep<?> testSuites = getTestSuites();
        final SelectHavingStep<?> milestoneDates = getMilestoneDates();
        final SelectHavingStep<?> boundToLockedMilestone = getBoundToBlockingMilestone();
        final SelectHavingStep<?> testCaseKind = getTestCaseKind();

        return getFilteredTableOnAssigneeIfNotExploratory(
                ITERATION_TEST_PLAN_ITEM
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                        .leftJoin(TEST_CASE_LIBRARY_NODE)
                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .leftJoin(ITERATION)
                        .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                        .leftJoin(CAMPAIGN_ITERATION)
                        .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                        .leftJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .leftJoin(PROJECT)
                        .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                        .leftJoin(TEST_CASE)
                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                        .leftJoin(getAutomationFields)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        getAutomationFields.field(ITEM_ID, Long.class)))
                        .leftJoin(DATASET)
                        .on(ITERATION_TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID))
                        .leftJoin(getUser)
                        .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(getUser.field(ITEM_ID, Long.class)))
                        .leftJoin(findItpiWithLatestExecution)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        findItpiWithLatestExecution.field(ITEM_ID, Long.class)))
                        .leftJoin(EXECUTION)
                        .on(
                                EXECUTION.EXECUTION_ID.eq(
                                        findItpiWithLatestExecution.field(LATEST_EXECUTION_ID, Long.class)))
                        .leftJoin(testSuites)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        testSuites.field(ITEM_ID, Long.class)))
                        .leftJoin(milestoneDates)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        milestoneDates.field(ITEM_ID, Long.class)))
                        .leftJoin(boundToLockedMilestone)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        boundToLockedMilestone.field(ITEM_ID, Long.class)))
                        .leftJoin(EXPLORATORY_SESSION_OVERVIEW)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID))
                    .leftJoin(testCaseKind)
                    .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(testCaseKind.field(ITEM_ID, Long.class)))
        );
    }

    @Override
    protected Condition craftInvariantFilter() {
        return ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(this.iterationId);
    }

    @Override
    protected Field<?> getIdentifier() {
        return ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID;
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return CAMPAIGN_LIBRARY_NODE.PROJECT_ID;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ORDER.asc();
    }

    @Override
    protected SelectHavingStep<Record2<Long, Integer>> findLastExecutionByOrder() {
        return DSL.select(
                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.as(ITEM_ID),
                        DSL.max(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER).as(MAX_EXECUTION_ORDER))
                .from(ITEM_TEST_PLAN_EXECUTION)
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .where(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(iterationId))
                .groupBy(DSL.field(ITEM_ID));
    }

    /* Concatenates test suite names e.g. "test suite 1, test suite 2" */
    private SelectHavingStep<?> getTestSuites() {
        // ITEM_ID | TEST_SUITES
        return DSL.select(
                        TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.as(ITEM_ID),
                        DSL.listAgg(TEST_SUITE.NAME, ", ")
                                .withinGroupOrderBy(TEST_SUITE_TEST_PLAN_ITEM.TEST_PLAN_ORDER)
                                .as(TEST_SUITES))
                .from(TEST_SUITE_TEST_PLAN_ITEM)
                .innerJoin(TEST_SUITE)
                .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
                .innerJoin(ITERATION_TEST_SUITE)
                .on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                .where(ITERATION_TEST_SUITE.ITERATION_ID.eq(iterationId))
                .groupBy(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID);
    }

    private boolean currentUserCanReadUnassigned(Long iterationId) {
        return this.permissionEvaluationService.hasRoleOrPermissionOnObject(
                Roles.ROLE_ADMIN,
                Permissions.READ_UNASSIGNED.name(),
                iterationId,
                Iteration.SIMPLE_CLASS_NAME);
    }
}
