/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import static org.squashtest.tm.domain.testcase.TestCaseKind.GHERKIN;

import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;

public final class TestCaseModel {
    public static final Comparator<TestCaseModel> COMPARATOR =
            new Comparator<TestCaseModel>() {
                @Override
                public int compare(TestCaseModel o1, TestCaseModel o2) {
                    return o1.getPath().compareTo(o2.getPath());
                }
            };

    private Long projectId;
    private String projectName;
    private String path;
    private Integer order;
    private Long id;
    private String reference;
    private String name;
    private String milestone;
    private int weightAuto;
    private TestCaseImportance weight;
    private InfoListItem nature;
    private InfoListItem type;
    private TestCaseStatus status;
    private String description;
    private String prerequisite;
    private Long nbReq;
    private Long nbCaller;
    private Long nbAttachments;
    private Long nbIterations;
    private Date createdOn;
    private String createdBy;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private List<CustomField> cufs = new LinkedList<>();
    private TestCaseKind testCaseKind;
    // scripted tc optional attributes
    private String tcScript;
    private TestCaseAutomatable automatable;
    private String uuid;
    private boolean allowAutomationWorkflow;
    private Long datasetCount;
    private Long milestonesCount;
    private Long testStepCount;
    private Long parameter;
    private Long executionCount;
    private Integer priority;
    private Date transmissionDate;
    private String automationRequestStatus;
    private Long hasAutoScript;
    private String automatedTestTechno;
    private Long hasBoundScmRepository;
    private String hasBoundAutomatedTestReference;
    private String milestoneStatus;
    private String milestoneEndDate;
    private Long calledStepCount;
    private Long issueCount;
    private boolean draftedByAi;

    // that monster constructor will be used by Hibernate in a hql query
    public TestCaseModel(
            Long projectId,
            Boolean allowAutomationWorkflow,
            String projectName,
            Integer order,
            Long id,
            String uuid,
            String reference,
            String name,
            String milestone,
            Boolean weightAuto,
            TestCaseImportance weight,
            InfoListItem nature,
            InfoListItem type,
            TestCaseStatus status,
            TestCaseAutomatable automatable,
            String description,
            String prerequisite,
            Long nbReq,
            Long nbCaller,
            Long nbAttachments,
            Long nbIterations,
            Date createdOn,
            String createdBy,
            Date lastModifiedOn,
            String lastModifiedBy,
            TestCaseKind testCaseKind,
            String tcScript,
            Long datasetCount,
            Long milestonesCount,
            Long testStepCount,
            Long parameter,
            Long executionCount,
            Integer priority,
            Date transmissionDate,
            String automationRequestStatus,
            Long hasAutoScript,
            String automatedTestTechno,
            Long hasBoundScmRepository,
            String hasBoundAutomatedTestReference,
            String milestoneStatus,
            String milestoneEndDate,
            Long calledStepCount,
            Long issueCount,
            Boolean draftedByAi) {

        super();
        this.projectId = projectId;
        this.allowAutomationWorkflow = allowAutomationWorkflow;
        this.projectName = projectName;
        this.order = order;
        this.id = id;
        this.uuid = uuid;
        this.reference = reference;
        this.name = name;
        this.milestone = milestone;
        this.weightAuto = Boolean.TRUE.equals(weightAuto) ? 1 : 0;
        this.weight = weight;
        this.nature = nature;
        this.type = type;
        this.status = status;
        this.automatable = automatable;
        this.description = description;
        this.prerequisite = prerequisite;
        this.nbReq = nbReq;
        this.nbCaller = nbCaller;
        this.nbAttachments = nbAttachments;
        this.nbIterations = nbIterations;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.lastModifiedOn = lastModifiedOn;
        this.lastModifiedBy = lastModifiedBy;
        this.testCaseKind = testCaseKind;
        this.datasetCount = datasetCount;
        this.milestonesCount = milestonesCount;
        this.testStepCount = testStepCount;
        this.parameter = parameter;
        this.executionCount = executionCount;
        this.priority = priority;
        this.transmissionDate = transmissionDate;
        this.automationRequestStatus = automationRequestStatus;
        this.hasAutoScript = hasAutoScript;
        this.automatedTestTechno = automatedTestTechno;
        this.hasBoundScmRepository = hasBoundScmRepository;
        this.hasBoundAutomatedTestReference = hasBoundAutomatedTestReference;
        this.milestoneStatus = milestoneStatus;
        this.milestoneEndDate = milestoneEndDate;
        this.calledStepCount = calledStepCount;
        this.issueCount = issueCount;
        this.draftedByAi = draftedByAi;

        if (GHERKIN.equals(testCaseKind)) {
            this.tcScript = tcScript;
        }
    }

    public String getMilestone() {
        return milestone;
    }

    public void setMilestone(String milestone) {
        this.milestone = milestone;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeightAuto() {
        return weightAuto;
    }

    public void setWeightAuto(int weightAuto) {
        this.weightAuto = weightAuto;
    }

    public TestCaseImportance getWeight() {
        return weight;
    }

    public void setWeight(TestCaseImportance weight) {
        this.weight = weight;
    }

    public InfoListItem getNature() {
        return nature;
    }

    public void setNature(InfoListItem nature) {
        this.nature = nature;
    }

    public InfoListItem getType() {
        return type;
    }

    public void setType(InfoListItem type) {
        this.type = type;
    }

    public TestCaseStatus getStatus() {
        return status;
    }

    public void setStatus(TestCaseStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public Long getNbReq() {
        return nbReq;
    }

    public void setNbReq(Long nbReq) {
        this.nbReq = nbReq;
    }

    public Long getNbCaller() {
        return nbCaller;
    }

    public void setNbCaller(Long nbCaller) {
        this.nbCaller = nbCaller;
    }

    public Long getNbAttachments() {
        return nbAttachments;
    }

    public void setNbAttachments(Long nbAttachments) {
        this.nbAttachments = nbAttachments;
    }

    public Long getNbIterations() {
        return nbIterations;
    }

    public void setNbIterations(Long nbIterations) {
        this.nbIterations = nbIterations;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public void addCuf(CustomField cuf) {
        cufs.add(cuf);
    }

    public List<CustomField> getCufs() {
        return cufs;
    }

    public TestCaseKind getTestCaseKind() {
        return testCaseKind;
    }

    public void setTestCaseKind(TestCaseKind testCaseKind) {
        this.testCaseKind = testCaseKind;
    }

    public String getTcScript() {
        return tcScript;
    }

    public void setTcScript(String tcScript) {
        this.tcScript = tcScript;
    }

    public TestCaseAutomatable getAutomatable() {
        return automatable;
    }

    public void setAutomatable(TestCaseAutomatable automatable) {
        this.automatable = automatable;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean getAllowAutomationWorkflow() {
        return allowAutomationWorkflow;
    }

    public Long getDatasetCount() {
        return datasetCount;
    }

    public Long getMilestonesCount() {
        return milestonesCount;
    }

    public Long getTestStepCount() {
        return testStepCount;
    }

    public Long getParameter() {
        return parameter;
    }

    public Long getExecutionCount() {
        return executionCount;
    }

    public Date getTransmissionDate() {
        return transmissionDate;
    }

    public Integer getPriority() {
        return priority;
    }

    public String getAutomationRequestStatus() {
        return automationRequestStatus;
    }

    public Long getHasAutoScript() {
        return hasAutoScript;
    }

    public String getAutomatedTestTechno() {
        return automatedTestTechno;
    }

    public Long getHasBoundScmRepository() {
        return hasBoundScmRepository;
    }

    public String getHasBoundAutomatedTestReference() {
        return hasBoundAutomatedTestReference;
    }

    public String getMilestoneStatus() {
        return milestoneStatus;
    }

    public String getMilestoneEndDate() {
        return milestoneEndDate;
    }

    public Long getCalledStepCount() {
        return calledStepCount;
    }

    public Long getIssueCount() {
        return issueCount;
    }

    public boolean getDraftedByAi() {
        return draftedByAi;
    }
}
