/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.milestone;

import static org.squashtest.tm.service.security.Authorizations.MANAGE_MILESTONE_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneRange;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.audit.AuditModificationService;
import org.squashtest.tm.service.internal.display.dto.BindableProjectToMilestoneDto;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.milestone.MilestoneManagerService;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.user.UserAccountService;

@Transactional
@Service("squashtest.tm.service.MilestoneBindingManagerService")
public class CustomMilestoneBindingServiceImpl implements MilestoneBindingManagerService {

    private static final String MILESTONE_UNBINDING_UPDATING_AUDITABLE_MESSAGE =
            "Milestone unbinding: updating auditable milestone {}";

    private static final Logger LOGGER =
            LoggerFactory.getLogger(CustomMilestoneBindingServiceImpl.class);

    @Inject private MilestoneDao milestoneDao;
    @Inject private MilestoneDisplayDao milestoneDisplayDao;
    @Inject private GenericProjectDao projectDao;
    @Inject private UserAccountService userAccountService;
    @Inject private PermissionEvaluationService permissionEvaluationService;
    @Inject private AuditModificationService auditModificationService;
    @Inject private CustomProjectFinder customProjectFinder;
    @Inject private MilestoneManagerService milestoneManagerService;

    @Override
    public List<Milestone> getAllBindableMilestoneForProject(long projectId) {
        List<Milestone> milestoneBoundToProject = getAllBindedMilestoneForProject(projectId);
        List<Milestone> allMilestones = milestoneDao.findAll();
        allMilestones.removeAll(milestoneBoundToProject);
        GenericProject project = projectDao.getReferenceById(projectId);
        return getMilestoneYouCanSee(allMilestones, project);
    }

    private List<Milestone> getMilestoneYouCanSee(
            List<Milestone> allMilestones, GenericProject project) {

        List<Milestone> filtered = new ArrayList<>();
        if (permissionEvaluationService.hasRole("ROLE_ADMIN")) {
            // admin can see all
            filtered = allMilestones;
        } else {

            for (Milestone milestone : allMilestones) {
                // project manager can see global, owned and milestone within the project perimeter
                if (!isRestricted(milestone)
                        || isCreatedBySelf(milestone)
                        || milestone.isInPerimeter(project)) {
                    filtered.add(milestone);
                }
            }
        }
        return filtered;
    }

    @Override
    @PreAuthorize(MANAGE_MILESTONE_OR_ROLE_ADMIN)
    public void bindMilestonesToProject(List<Long> milestoneIds, long projectId) {
        milestoneManagerService.checkIfCanManageMilestonesOrAdmin(milestoneIds);

        doBindMilestonesToProject(milestoneIds, projectId);
    }

    @Override
    @PreAuthorize(MANAGE_MILESTONE_OR_ROLE_ADMIN)
    public void bindNewMilestonesToProject(List<Long> milestoneIds, long projectId) {
        doBindMilestonesToProject(milestoneIds, projectId);
    }

    private void doBindMilestonesToProject(List<Long> milestoneIds, long projectId) {
        GenericProject project = projectDao.getReferenceById(projectId);
        List<Milestone> milestones = milestoneDao.findAllByIdAndStatusNotLocked(milestoneIds);
        project.bindMilestones(milestones);
        for (Milestone milestone : milestones) {
            milestone.addProjectToPerimeter(project);
            LOGGER.debug("Milestone binding: updating auditable milestone {}", milestone.getId());
            auditModificationService.updateAuditable((AuditableMixin) milestone);
        }

        LOGGER.debug("Milestone binding: updating auditable project {}", projectId);
        auditModificationService.updateAuditable((AuditableMixin) project);
    }

    @Override
    public void bindProjectsToMilestone(List<Long> projectIds, long milestoneId) {
        milestoneManagerService.verifyCanEditMilestone(milestoneId);

        List<GenericProject> projects = projectDao.findAllById(projectIds);
        Milestone milestone = milestoneDao.getReferenceById(milestoneId);
        milestone.bindProjects(projects);
        milestone.addProjectsToPerimeter(projects);

        LOGGER.debug("Milestone binding: updating auditable milestone {}", milestoneId);
        auditModificationService.updateAuditable((AuditableMixin) milestone);

        LOGGER.debug("Milestone binding: updating multiple auditable projects");
        projects.forEach(project -> auditModificationService.updateAuditable((AuditableMixin) project));
    }

    @Override
    public List<Milestone> getAllBindedMilestoneForProject(long projectId) {
        GenericProject project = projectDao.getReferenceById(projectId);
        return project.getMilestones();
    }

    @Override
    public List<BindableProjectToMilestoneDto> getAllBindableProjectForMilestone(long milestoneId) {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        List<Long> allMilestoneManageableProjectIds =
                customProjectFinder.findAllMilestoneManageableIds(currentUser);

        if (!currentUser.isAdmin()) {
            if (allMilestoneManageableProjectIds.isEmpty()) {
                throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
            }
            milestoneDisplayDao.checkIfAllMilestoneAreGlobalOrInOneManageableProjectPerimeterOrOwner(
                    Collections.singletonList(milestoneId),
                    allMilestoneManageableProjectIds,
                    currentUser.getUserId());
        }

        return milestoneDao.findBindableProjectByMilestoneId(
                milestoneId, allMilestoneManageableProjectIds);
    }

    @Override
    @PreAuthorize(MANAGE_MILESTONE_OR_ROLE_ADMIN)
    public void unbindMilestonesFromProject(List<Long> milestoneIds, long projectId) {
        milestoneManagerService.checkIfCanManageMilestonesOrAdmin(milestoneIds);

        GenericProject project = projectDao.getReferenceById(projectId);
        List<Milestone> milestones = milestoneDao.findAllById(milestoneIds);
        unbindMilestonesFromProject(project, milestones);
    }

    private void unbindMilestonesFromProject(GenericProject project, List<Milestone> milestones) {

        project.unbindMilestones(milestones);

        LOGGER.debug("Milestone unbinding: updating auditable project {}", project.getId());
        auditModificationService.updateAuditable((AuditableMixin) project);

        // Remove the project in different for loop because milestoneDao.unbindAllObjectsForProject may
        // clear the
        // session
        for (Milestone milestone : milestones) {
            milestone.removeProjectFromPerimeter(project);
            LOGGER.debug(MILESTONE_UNBINDING_UPDATING_AUDITABLE_MESSAGE, milestone.getId());
            auditModificationService.updateAuditable((AuditableMixin) milestone);
        }

        for (Milestone milestone : milestones) {
            // that thing will probably clear the session, be careful
            milestoneDao.unbindAllObjectsForProject(milestone.getId(), project.getId());
        }
        // [SQUASH-4797] remove project from all milestone perimeters,
        // it can be in a milestone perimeter without being bound to this milestone
        milestoneDao.removeProjectFromAllPerimeters(project.getId());
    }

    @Override
    public void unbindAllMilestonesFromProject(@NotNull GenericProject project) {
        unbindMilestonesFromProject(project, project.getMilestones());
    }

    @Override
    public void unbindProjectsFromMilestone(List<Long> projectIds, long milestoneId) {
        milestoneManagerService.verifyCanEditMilestone(milestoneId);

        Milestone milestone = milestoneDao.getReferenceById(milestoneId);
        List<GenericProject> projects = projectDao.findAllById(projectIds);
        milestone.unbindProjects(projects);

        LOGGER.debug("Milestone unbinding: updating multiple auditable projects");
        projects.forEach(project -> auditModificationService.updateAuditable((AuditableMixin) project));

        milestone.removeProjectsFromPerimeter(projects);

        LOGGER.debug(MILESTONE_UNBINDING_UPDATING_AUDITABLE_MESSAGE, milestone.getId());
        auditModificationService.updateAuditable((AuditableMixin) milestone);

        milestoneDao.unbindAllObjectsForProjects(milestoneId, projectIds);
    }

    @Override
    public List<Milestone> getAllBindableMilestoneForProject(long projectId, String type) {
        List<Milestone> milestones = getAllBindableMilestoneForProject(projectId);

        return removeNonBindableStatus(filterByType(milestones, type));
    }

    private List<Milestone> removeNonBindableStatus(List<Milestone> milestones) {

        List<Milestone> filtered = new ArrayList<>();

        for (Milestone milestone : milestones) {
            if (milestone.getStatus().isBindableToProject()) {
                filtered.add(milestone);
            }
        }
        return filtered;
    }

    private List<Milestone> filterByType(List<Milestone> milestones, String type) {

        List<Milestone> filtered;
        if ("global".equals(type)) {
            // global milestone
            filtered = getGlobalMilestones(milestones);

        } else if ("personal".equals(type)) {
            // milestone created by the user
            filtered = getMilestoneCreatedBySelf(milestones);
        } else {
            // other milestone
            filtered = getOtherMilestones(milestones);
        }
        return filtered;
    }

    private List<Milestone> getOtherMilestones(List<Milestone> milestones) {
        List<Milestone> filtered = new ArrayList<>();

        for (Milestone milestone : milestones) {
            if (isRestricted(milestone) && !isCreatedBySelf(milestone)) {
                filtered.add(milestone);
            }
        }
        return filtered;
    }

    private List<Milestone> getMilestoneCreatedBySelf(List<Milestone> milestones) {
        List<Milestone> filtered = new ArrayList<>();
        for (Milestone milestone : milestones) {
            if (isRestricted(milestone) && isCreatedBySelf(milestone)) {
                filtered.add(milestone);
            }
        }
        return filtered;
    }

    private boolean isRestricted(Milestone milestone) {
        boolean isRestricted = false;
        if (milestone.getRange() == MilestoneRange.RESTRICTED) {
            isRestricted = true;
        }
        return isRestricted;
    }

    private boolean isCreatedBySelf(Milestone milestone) {
        boolean isCreatedBySelf = false;
        String myName = UserContextHolder.getUsername();
        if (myName.equals(milestone.getOwner().getLogin())) {
            isCreatedBySelf = true;
        }
        return isCreatedBySelf;
    }

    private List<Milestone> getGlobalMilestones(List<Milestone> milestones) {
        List<Milestone> filtered = new ArrayList<>();
        for (Milestone milestone : milestones) {
            if (milestone.getRange() == MilestoneRange.GLOBAL) {
                filtered.add(milestone);
            }
        }
        return filtered;
    }

    @Override
    public void unbindProjectsFromMilestoneKeepInPerimeter(List<Long> projectIds, long milestoneId) {
        milestoneManagerService.verifyCanEditMilestone(milestoneId);

        Milestone milestone = milestoneDao.getReferenceById(milestoneId);
        List<GenericProject> projects = projectDao.findAllById(projectIds);
        milestone.unbindProjects(projects);

        LOGGER.debug(MILESTONE_UNBINDING_UPDATING_AUDITABLE_MESSAGE, milestone.getId());
        auditModificationService.updateAuditable((AuditableMixin) milestone);

        LOGGER.debug("Milestone unbinding: updating multiple auditable project");
        projects.forEach(project -> auditModificationService.updateAuditable((AuditableMixin) project));

        milestoneDao.unbindAllObjectsForProjects(milestoneId, projectIds);
    }

    @Override
    public void unbindTemplateFrom(long milestoneId) {
        Milestone milestone = milestoneDao.getReferenceById(milestoneId);
        milestone.removeTemplates();
    }

    @Override
    @PreAuthorize(MANAGE_MILESTONE_OR_ROLE_ADMIN)
    public void bindMilestonesToProjectAndBindObject(long projectId, List<Long> milestoneIds) {
        bindMilestonesToProject(milestoneIds, projectId);
        doBindMilestonesToProjectAndBindObject(projectId, milestoneIds);
    }

    @Override
    @PreAuthorize(MANAGE_MILESTONE_OR_ROLE_ADMIN)
    public void bindNewMilestonesToProjectAndBindObject(long projectId, List<Long> milestoneIds) {
        bindNewMilestonesToProject(milestoneIds, projectId);
        doBindMilestonesToProjectAndBindObject(projectId, milestoneIds);
    }

    private void doBindMilestonesToProjectAndBindObject(long projectId, List<Long> milestoneIds) {
        for (Long milestoneId : milestoneIds) {
            milestoneDao.bindMilestoneToProjectTestCases(projectId, milestoneId);
            milestoneDao.bindMilestoneToProjectRequirementVersions(projectId, milestoneId);
        }
    }
}
