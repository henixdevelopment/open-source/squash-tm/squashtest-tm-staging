/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportType;

public interface GlobalProjectPivotImporterService {
    String ENTITY_TYPE_NOT_HANDLED = "This entity type is not handled ";
    int BATCH_SIZE = 50;

    void createImportRequest(
            long projectId,
            MultipartFile multipartFile,
            String importName,
            PivotFormatImportType importType);

    void deleteImportRequests(long projectId, List<Long> idImportRequests);

    void importProject(PivotFormatImport pivotFormatImport);

    void importInExistingProject(PivotFormatImport pivotFormatImport);

    String getImportErrorLogFilePath(Long importId, PivotFormatImportType importType);

    String getImportWarningLogFilePath(Long importId, PivotFormatImportType importType);
}
