/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import org.squashtest.tm.domain.EntityType;

/**
 * @param ownerId The ID of the owner of the permission (For example for a test step, it will be the
 *     test case ID)
 * @param attachmentListId The ID of the attachment list of the holder of the attachment (For a test
 *     step, it will be the ID of its own attachment list, not the test case one)
 * @param holderType The type of the holder of the attachment
 * @param internalHolderId The ID inside the json import file of the holder of the attachment
 */
public record AttachmentHolderInfo(
        Long ownerId, Long attachmentListId, EntityType holderType, String internalHolderId) {}
