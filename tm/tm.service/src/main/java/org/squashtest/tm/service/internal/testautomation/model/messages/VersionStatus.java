/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model.messages;

import java.util.Map;
import java.util.Objects;
import org.opentestfactory.messages.Status;
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions;

public class VersionStatus extends Status<OrchestratorConfVersions> {

    private static final String ITEMS_KEY = "items";

    public VersionStatus(String apiVersion) {
        super(apiVersion);
    }

    public OrchestratorConfVersions getOrchestratorConfigurations() {
        Map<String, OrchestratorConfVersions> details = getDetails();
        return Objects.isNull(details) ? null : details.get(ITEMS_KEY);
    }
}
