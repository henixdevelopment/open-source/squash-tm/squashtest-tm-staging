/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;

public class CalledTestCasesToPivot {
    @JsonProperty(JsonImportField.ID)
    protected String pivotId;

    @JsonProperty(JsonImportField.CALLED_ID)
    private String calledId;

    @JsonProperty(JsonImportField.CALLER_ID)
    private String callerId;

    @JsonProperty(JsonImportField.INDEX)
    private int index;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.PARAMETER_ASSIGNATION_MODE)
    private ParameterAssignationMode parameterAssignationMode;

    @JsonProperty(JsonImportField.DATASET_ID)
    private String datasetId;

    public String getPivotId() {
        return pivotId;
    }

    public void setPivotId(Integer id) {
        this.pivotId = String.format("%s%s", XrayField.BASE_PIVOT_ID_CALLED_TC, id);
    }

    public String getCalledId() {
        return calledId;
    }

    public void setCalledId(String calledId) {
        this.calledId = calledId;
    }

    public String getCallerId() {
        return callerId;
    }

    public void setCallerId(String callerId) {
        this.callerId = callerId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public ParameterAssignationMode getParameterAssignationMode() {
        return parameterAssignationMode;
    }

    public void setParameterAssignationMode(ParameterAssignationMode parameterAssignationMode) {
        this.parameterAssignationMode = parameterAssignationMode;
    }

    public String getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(String datasetId) {
        this.datasetId = datasetId;
    }
}
