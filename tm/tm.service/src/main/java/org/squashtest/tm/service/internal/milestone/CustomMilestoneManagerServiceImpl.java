/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.milestone;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;
import static org.squashtest.tm.service.internal.display.dto.milestone.MilestoneDuplicationModel.toMilestone;
import static org.squashtest.tm.service.internal.dto.UserDto.fromUser;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MILESTONE_FEAT_ENABLED;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneHolder;
import org.squashtest.tm.domain.milestone.MilestoneRange;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.milestone.MilestoneLabelAlreadyExistsException;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.internal.display.dto.milestone.MilestoneDuplicationModel;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.CustomMilestoneDao.HolderConsumer;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.milestone.CustomMilestoneManager;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.user.UserAccountService;

@Transactional
@Service("CustomMilestoneManager")
public class CustomMilestoneManagerServiceImpl implements CustomMilestoneManager {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(CustomMilestoneManagerServiceImpl.class);

    @Inject private ProjectFinder projectFinder;
    @Inject private MilestoneDao milestoneDao;
    @Inject private MilestoneDisplayDao milestoneDisplayDao;
    @Inject private UserAccountService userAccountService;
    @Inject private PermissionEvaluationService permissionEvaluationService;

    @PersistenceContext private EntityManager em;

    @Override
    @PreAuthorize(MILESTONE_FEAT_ENABLED)
    public void addMilestone(Milestone milestone) {
        User owner = userAccountService.findCurrentUser();
        UserDto ownerDto = fromUser(owner);
        permissionEvaluationService.checkAtLeastOneMilestoneManagementPermissionOrAdmin(
                ownerDto.getPartyIds());

        checkLabelAvailability(milestone.getLabel());
        milestone.setOwner(owner);
        milestoneDao.save(milestone);
    }

    @Override
    public void changeLabel(long milestoneId, String newLabel) {
        permissionEvaluationService.checkAtLeastOneMilestoneManagementPermissionOrAdmin();
        Milestone milestone = milestoneDao.getReferenceById(milestoneId);
        if (StringUtils.equals(milestone.getLabel(), newLabel)) {
            return;
        }
        checkLabelAvailability(newLabel);
        milestone.setLabel(newLabel);
    }

    private void checkLabelAvailability(String label) {
        if (milestoneDao.findByLabel(label) != null) {
            throw new MilestoneLabelAlreadyExistsException(label);
        }
    }

    @Override
    public List<Milestone> findAll() {
        return milestoneDao.findAll();
    }

    @Override
    public void removeMilestones(Collection<Long> ids) {
        permissionEvaluationService.checkAtLeastOneMilestoneManagementPermissionOrAdmin();
        List<Milestone> milestones = milestoneDao.findAllById(ids);
        for (Milestone milestone : milestones) {
            if (canEditMilestone(milestone)) {
                deleteMilestoneBinding(milestone);
                deleteMilestone(milestone);
            } else {
                LOGGER.debug(
                        "Milestone with ID {} was filtered out because the user cannot delete it.",
                        milestone.getId());
            }
        }
    }

    private void deleteMilestoneBinding(final Milestone milestone) {
        List<GenericProject> projects = milestone.getProjects();
        for (GenericProject project : projects) {
            project.unbindMilestone(milestone);
        }
    }

    private void deleteMilestone(final Milestone milestone) {

        milestoneDao.delete(milestone);
    }

    @Override
    @Transactional(readOnly = true)
    public Milestone findById(long milestoneId) {
        return milestoneDao.getReferenceById(milestoneId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Milestone> findAllByIds(List<Long> milestoneIds) {
        return milestoneDao.findAllById(milestoneIds);
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> findMilestoneLabelByIds(List<Long> milestoneIds, List<Long> projectIds) {
        return milestoneDao.findMilestoneLabelByIds(milestoneIds, projectIds);
    }

    @Override
    public void verifyCanEditMilestone(long milestoneId) {
        if (!canEditMilestone(milestoneId)) {
            throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
        }
    }

    private boolean isGlobal(Milestone milestone) {
        return MilestoneRange.GLOBAL == milestone.getRange();
    }

    private boolean isCreatedBySelf(Milestone milestone) {
        String myName = UserContextHolder.getUsername();
        return myName.equals(milestone.getOwner().getLogin());
    }

    @Override
    public void verifyCanEditMilestoneRange() {
        if (!permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
        }
    }

    @Override
    public boolean canEditMilestone(long milestoneId) {
        Milestone milestone = milestoneDao.getReferenceById(milestoneId);

        return canEditMilestone(milestone);
    }

    private boolean canEditMilestone(Milestone milestone) {
        if (!permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            // project manager can't edit global milestone or milestone they don't own
            return !(isGlobal(milestone) || !isCreatedBySelf(milestone));
        }
        return true;
    }

    @Override
    public List<Long> findAllIdsOfEditableMilestone() {
        List<Milestone> milestones = findAll();
        List<Long> ids = new ArrayList<>();
        for (Milestone milestone : milestones) {
            if (canEditMilestone(milestone.getId())) {
                ids.add(milestone.getId());
            }
        }
        return ids;
    }

    @Override
    public List<Milestone> findAllVisibleToCurrentManager() {

        List<Milestone> allMilestones = findAll();
        List<Milestone> milestones = new ArrayList<>();

        if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            milestones.addAll(allMilestones);
        } else {
            for (Milestone milestone : allMilestones) {
                if (isGlobal(milestone) || isCreatedBySelf(milestone) || isInAProjetICanManage(milestone)) {
                    milestones.add(milestone);
                }
            }
        }
        return milestones;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Milestone> findAllVisibleToCurrentUser() {

        List<Long> milestoneIds = findAllIdsVisibleToCurrentUser();
        return milestoneDao.findAllById(milestoneIds);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Long> findAllIdsVisibleToCurrentUser() {
        UserDto user = userAccountService.findCurrentUserDto();
        if (user.isAdmin()) {
            return milestoneDao.findAllMilestoneIds();
        } else {
            return milestoneDao.findMilestoneIdsForUsers(user.getPartyIds());
        }
    }

    private boolean isInAProjetICanManage(Milestone milestone) {
        boolean isInAProjetICanManage = false;
        List<GenericProject> perimeter = milestone.getPerimeter();

        for (GenericProject project : perimeter) {
            if (canIManageThisProject(project)) {
                isInAProjetICanManage = true;
                break;
            }
        }
        return isInAProjetICanManage;
    }

    private boolean canIManageThisProject(GenericProject project) {
        return permissionEvaluationService.hasRoleOrPermissionOnObject(
                ROLE_ADMIN, Permissions.MANAGE_PROJECT.name(), project);
    }

    private List<GenericProject> getProjectICanManage(Collection<GenericProject> projects) {

        List<GenericProject> manageableProjects = new ArrayList<>();

        for (GenericProject project : projects) {
            if (canIManageThisProject(project)) {
                manageableProjects.add(project);
            }
        }
        return manageableProjects;
    }

    @Override
    public boolean isBoundToATemplate(long milestoneId) {
        Milestone milestone = findById(milestoneId);
        return milestone.isBoundToATemplate();
    }

    @Override
    @PreAuthorize(MILESTONE_FEAT_ENABLED)
    public long cloneMilestone(long motherId, MilestoneDuplicationModel model) {
        UserDto user = userAccountService.findCurrentUserDto();

        Milestone mother = findById(motherId);

        if (mother.getStatus().isAllowObjectDuplication()) {
            Milestone milestone = toMilestone(model);

            if (user.isAdmin()) {
                milestone.setRange(mother.getRange());
            } else {
                milestone.setRange(MilestoneRange.RESTRICTED);
            }

            boolean copyAllPerimeter = user.isAdmin() || !isGlobal(mother) && isCreatedBySelf(mother);

            bindProjectsAndPerimeter(mother, milestone, copyAllPerimeter);
            bindRequirements(mother, milestone, model.bindToRequirements(), copyAllPerimeter);
            bindTestCases(mother, milestone, model.bindToTestCases(), copyAllPerimeter);
            addMilestone(milestone);

            return milestone.getId();
        }

        throw new IllegalArgumentException(
                String.format(
                        "%s %s %s",
                        "The milestone status: ",
                        mother.getStatus().name(),
                        " does not allow milestone duplication operation."));
    }

    @Override
    public void migrateMilestones(MilestoneHolder member) {

        Collection<Milestone> projectMilestones = member.getProject().getMilestones();
        Collection<Milestone> memberMilestones = member.getMilestones();

        Iterator<Milestone> memberIterator = memberMilestones.iterator();
        while (memberIterator.hasNext()) {
            Milestone m = memberIterator.next();
            if (!projectMilestones.contains(m)) {
                memberIterator.remove();
            }
        }
    }

    private void bindProjectsAndPerimeter(
            Milestone mother, Milestone milestone, boolean copyAllPerimeter) {

        if (copyAllPerimeter) {
            milestone.bindProjects(mother.getProjects());
            milestone.addProjectsToPerimeter(mother.getPerimeter());
        } else {

            List<GenericProject> projects = new ArrayList<>(mother.getProjects());
            projects.retainAll(projectFinder.findAllICanManage());

            List<GenericProject> perim = new ArrayList<>(mother.getPerimeter());
            perim.retainAll(projectFinder.findAllICanManage());

            milestone.bindProjects(projects);
            milestone.addProjectsToPerimeter(perim);
        }
    }

    private void bindTestCases(
            Milestone mother, Milestone milestone, boolean bindToTestCases, boolean copyAllPerimeter) {
        if (bindToTestCases) {
            for (TestCase tc : mother.getTestCases()) {
                if (copyAllPerimeter || canIManageThisProject(tc.getProject())) {
                    milestone.bindTestCase(tc);
                }
            }
        }
    }

    private void bindRequirements(
            Milestone mother, Milestone milestone, boolean bindToRequirements, boolean copyAllPerimeter) {
        if (bindToRequirements) {
            for (RequirementVersion req : mother.getRequirementVersions()) {
                if (copyAllPerimeter || canIManageThisProject(req.getProject())) {
                    milestone.bindRequirementVersion(req);
                }
            }
        }
    }

    @Override
    public void synchronize(long sourceId, long targetId, boolean extendPerimeter, boolean isUnion) {
        permissionEvaluationService.checkAtLeastOneMilestoneManagementPermissionOrAdmin();
        Milestone source = findById(sourceId);
        Milestone target = findById(targetId);
        verifyCanSynchronize(source, target, isUnion);
        synchronizePerimeterAndProjects(source, target, extendPerimeter, isUnion);
        synchronizeTestCases(source, target, isUnion, extendPerimeter);
        synchronizeRequirementVersions(source, target, isUnion, extendPerimeter);
    }

    private void verifyCanSynchronize(Milestone source, Milestone target, boolean isUnion) {
        boolean isNotAdmin = !permissionEvaluationService.hasRole(ROLE_ADMIN);
        if (isUnion && (!source.getStatus().isBindableToObject() || isNotAdmin && isGlobal(source))) {
            throw new IllegalArgumentException(
                    "milestone can't be synchronized because it's status or range don't allow it");
        }

        if (!target.getStatus().isBindableToObject() || isNotAdmin && isGlobal(target)) {
            throw new IllegalArgumentException(
                    "milestone can't be synchronized because it's status or range don't allow it");
        }
    }

    private void synchronizeRequirementVersions(
            Milestone source, Milestone target, boolean isUnion, boolean extendPerimeter) {
        milestoneDao.synchronizeRequirementVersions(
                source.getId(),
                target.getId(),
                getProjectsToSynchronize(source, target, extendPerimeter, isUnion));
        if (isUnion) {
            milestoneDao.synchronizeRequirementVersions(
                    target.getId(),
                    source.getId(),
                    getProjectsToSynchronize(target, source, extendPerimeter, isUnion));
        }
    }

    private void synchronizeTestCases(
            Milestone source, Milestone target, boolean isUnion, boolean extendPerimeter) {
        milestoneDao.synchronizeTestCases(
                source.getId(),
                target.getId(),
                getProjectsToSynchronize(source, target, extendPerimeter, isUnion));
        if (isUnion) {
            milestoneDao.synchronizeTestCases(
                    target.getId(),
                    source.getId(),
                    getProjectsToSynchronize(target, source, extendPerimeter, isUnion));
        }
    }

    private Set<GenericProject> getProjectsToSynchronizeForProjectManager(
            Set<GenericProject> result, Milestone target, boolean extendPerimeter) {
        if (extendPerimeter && isCreatedBySelf(target)) {
            result.addAll(target.getPerimeter());
        } else {
            result.retainAll(target.getPerimeter());

            if (!isCreatedBySelf(target)) {
                result.retainAll(getProjectICanManage(result));
            }
        }
        return result;
    }

    private List<Long> getProjectsToSynchronize(
            Milestone source, Milestone target, boolean extendPerimeter, boolean isUnion) {

        Set<GenericProject> result = new HashSet<>(source.getPerimeter());

        if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {

            getProjectsToSynchronizeForProjectForAdmin(result, source, target, isUnion);

        } else {
            getProjectsToSynchronizeForProjectManager(result, target, extendPerimeter);
        }

        List<Long> ids = new ArrayList<>();
        for (GenericProject p : result) {
            ids.add(p.getId());
        }
        return ids;
    }

    private Set<GenericProject> getProjectsToSynchronizeForProjectForAdmin(
            Set<GenericProject> result, Milestone source, Milestone target, boolean isUnion) {

        if (isUnion && isGlobal(source) && isGlobal(target) || !isUnion && isGlobal(target)) {
            result.addAll(target.getPerimeter());
        } else {
            result.retainAll(target.getPerimeter());
        }

        return result;
    }

    private void adminSynchronizePerimeterAndProjects(
            Milestone source, Milestone target, boolean isUnion) {

        if (isUnion) {
            adminSynchronizePerimeterAndProjectsForUnion(source, target);
        } else {
            adminSynchronizePerimeterAndProjects(source, target);
        }
    }

    private void adminSynchronizePerimeterAndProjectsForUnion(Milestone source, Milestone target) {
        if (isGlobal(source) && isGlobal(target)) {
            adminSynchronizePerimeterAndProjects(source, target);
            adminSynchronizePerimeterAndProjects(target, source);
        }
    }

    private void adminSynchronizePerimeterAndProjects(Milestone source, Milestone target) {
        if (isGlobal(target)) {
            target.bindProjects(source.getProjects());
            target.addProjectsToPerimeter(source.getPerimeter());
        }
    }

    private void projectManagerSynchronizePerimeterAndProjects(
            Milestone source, Milestone target, boolean extendPerimeter) {

        if (isCreatedBySelf(target) && extendPerimeter) {
            // can extend perimeter only if own milestone
            target.bindProjects(source.getProjects());
            target.addProjectsToPerimeter(source.getPerimeter());
        }
    }

    private void synchronizePerimeterAndProjects(
            Milestone source, Milestone target, boolean extendPerimeter, boolean isUnion) {

        if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            adminSynchronizePerimeterAndProjects(source, target, isUnion);
        } else {
            projectManagerSynchronizePerimeterAndProjects(source, target, extendPerimeter);
        }
    }

    /**
     * @see org.squashtest.tm.service.milestone.CustomMilestoneManager#enableFeature()
     */
    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void enableFeature() {
        // NOOP (AFAIK)

    }

    /**
     * @see org.squashtest.tm.service.milestone.CustomMilestoneManager#disableFeature()
     */
    @SuppressWarnings("unchecked")
    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void disableFeature() {
        LOGGER.info(
                "Disabling the Milestones feature: all milestones are going to be deleted from database");

        milestoneDao.performBatchUpdate(
                new HolderConsumer() {
                    @Override
                    public void consume(MilestoneHolder holder) {
                        holder.unbindAllMilestones();
                    }
                });

        Session session = em.unwrap(Session.class);
        List<Milestone> milestones = session.createQuery("from Milestone").list();

        for (Milestone milestone : milestones) {
            milestone.unbindAllProjects();
            milestone.clearPerimeter();
            session.delete(milestone);
        }
    }

    @Override
    public boolean isBoundToAtleastOneObject(long milestoneId) {
        return milestoneDao.isBoundToAtleastOneObject(milestoneId);
    }

    @Override
    public void unbindAllObjects(long milestoneId) {

        milestoneDao.unbindAllObjects(milestoneId);
        Milestone milestone = findById(milestoneId);
        milestone.clearObjects();
    }

    @Override
    @Transactional(readOnly = true)
    public Milestone findByName(String name) {
        return milestoneDao.findByName(name);
    }

    @Override
    public boolean isMilestoneBoundToOneObjectOfProject(Milestone milestone, GenericProject project) {

        return milestoneDao.isMilestoneBoundToOneObjectOfProject(milestone.getId(), project.getId());
    }

    @Override
    public boolean hasMilestone(List<Long> userdIds) {
        long result = milestoneDao.countMilestonesForUsers(userdIds);
        return result > 0;
    }

    @Override
    public boolean canManageMilestonesOrAdmin(Collection<Long> milestoneIds) {
        UserDto currentUser = userAccountService.findCurrentUserDto();

        return canManageMilestonesOrAdminWithUserDto(milestoneIds, currentUser);
    }

    private boolean canManageMilestonesOrAdminWithUserDto(
            Collection<Long> milestoneIds, UserDto userDto) {
        if (userDto.isAdmin()) {
            return true;
        } else {
            List<Long> manageableProjectIds = projectFinder.findAllMilestoneManageableIds(userDto);
            if (manageableProjectIds.isEmpty()) {
                return false;
            }
            return milestoneDisplayDao.areAllMilestoneGlobalOrInOneManageableProjectPerimeterOrOwner(
                    milestoneIds, manageableProjectIds, userDto.getUserId());
        }
    }

    @Override
    public void checkIfCanManageMilestonesOrAdmin(Collection<Long> milestoneIds) {
        if (!canManageMilestonesOrAdmin(milestoneIds)) {
            throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
        }
    }
}
