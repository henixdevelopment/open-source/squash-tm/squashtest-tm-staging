/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.domain.project.AutomationWorkflowType.NONE;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.jenkins;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.squashOrchestrator;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;
import static org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus.AUTOMATED;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.CampaignIteration.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.tables.CampaignLibraryNode.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.Iteration.ITERATION;
import static org.squashtest.tm.jooq.domain.tables.IterationTestPlanItem.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.IterationTestSuite.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.tables.Project.PROJECT;
import static org.squashtest.tm.jooq.domain.tables.TestCase.TEST_CASE;
import static org.squashtest.tm.jooq.domain.tables.TestSuite.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.tables.TestSuiteTestPlanItem.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.HAS_AUTOMATED_TESTS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.HAS_DATASETS;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.jooq.domain.tables.TestSuiteTestPlanItem;
import org.squashtest.tm.service.internal.display.dto.campaign.TestSuiteDto;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;

@Repository
public class TestSuiteDisplayDaoImpl implements TestSuiteDisplayDao {

    private DSLContext dsl;

    public TestSuiteDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public TestSuiteDto findById(long testSuiteId) {

        return dsl.select(
                        TEST_SUITE.ID,
                        TEST_SUITE.UUID,
                        TEST_SUITE.NAME,
                        TEST_SUITE.DESCRIPTION,
                        TEST_SUITE.EXECUTION_STATUS,
                        TEST_SUITE.ATTACHMENT_LIST_ID,
                        TEST_SUITE.CREATED_BY,
                        TEST_SUITE.CREATED_ON,
                        TEST_SUITE.LAST_MODIFIED_BY,
                        TEST_SUITE.LAST_MODIFIED_ON,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
                        DSL.count(TEST_AUTOMATION_SERVER.SERVER_ID).gt(0).as(HAS_AUTOMATED_TESTS),
                        DSL.count(ITERATION_TEST_PLAN_ITEM.DATASET_ID).gt(0).as(HAS_DATASETS),
                        ITERATION_TEST_SUITE.ITERATION_ID)
                .from(TEST_SUITE)
                .join(ITERATION_TEST_SUITE)
                .on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                .join(CAMPAIGN_ITERATION)
                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .join(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                .on(TEST_SUITE.ID.eq(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID))
                .leftJoin(ITERATION_TEST_PLAN_ITEM)
                .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .leftJoin(TEST_CASE)
                .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .leftJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftJoin(TEST_AUTOMATION_SERVER)
                .on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
                .and(
                        PROJECT
                                .AUTOMATION_WORKFLOW_TYPE
                                .ne(NONE.name())
                                .and(TEST_CASE.AUTOMATABLE.eq(Y.name()))
                                .and(AUTOMATION_REQUEST.REQUEST_STATUS.eq(AUTOMATED.name()))
                                .or(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(NONE.name())))
                // if Jenkins server, TestCase only has to be linked to an automation test
                .and(
                        TEST_AUTOMATION_SERVER
                                .KIND
                                .eq(jenkins.name())
                                .and(TEST_CASE.TA_TEST.isNotNull())
                                // if Squash Autom server, then the 3 automation attributes must exist
                                .or(
                                        TEST_AUTOMATION_SERVER
                                                .KIND
                                                .eq(squashOrchestrator.name())
                                                .and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
                                                .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
                                                .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull())))
                .where(TEST_SUITE.ID.eq(testSuiteId))
                .groupBy(TEST_SUITE.ID, CAMPAIGN_LIBRARY_NODE.PROJECT_ID, ITERATION_TEST_SUITE.ITERATION_ID)
                .fetchOneInto(TestSuiteDto.class);
    }

    @Override
    public List<NamedReference> findForIteration(Long iterationId) {
        return dsl.select(TEST_SUITE.ID, TEST_SUITE.NAME)
                .from(TEST_SUITE)
                .innerJoin(ITERATION_TEST_SUITE)
                .on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                .where(ITERATION_TEST_SUITE.ITERATION_ID.eq(iterationId))
                .fetch()
                .into(NamedReference.class);
    }

    @Override
    public Long findIterationIdByTestsuiteId(Long testSuiteId) {
        return dsl.select(ITERATION_TEST_SUITE.ITERATION_ID)
                .from(ITERATION_TEST_SUITE)
                .where(ITERATION_TEST_SUITE.TEST_SUITE_ID.eq(testSuiteId))
                .fetchOne(0, long.class);
    }

    @Override
    public HashMap<Long, String> getExecutionStatusMap(Long testSuiteId) {
        return (HashMap<Long, String>)
                dsl.select(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS)
                        .from(TEST_SUITE)
                        .innerJoin(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                        .where(TEST_SUITE.ID.eq(testSuiteId))
                        .fetch()
                        .intoMap(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS);
    }

    @Override
    public int getNbTestPlanItem(Long testSuiteId, String login) {
        SelectConditionStep<Record1<Integer>> allTestPlanItemsFromCampaign =
                dsl.selectCount()
                        .from(TEST_SUITE)
                        .innerJoin(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                        .leftJoin(CORE_USER)
                        .on(CORE_USER.PARTY_ID.eq(ITERATION_TEST_PLAN_ITEM.USER_ID))
                        .where(TEST_SUITE.ID.eq(testSuiteId));
        if (login != null) {
            allTestPlanItemsFromCampaign = allTestPlanItemsFromCampaign.and(CORE_USER.LOGIN.eq(login));
        }

        Integer count = allTestPlanItemsFromCampaign.groupBy(TEST_SUITE.ID).fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public ExecutionStatus findTestSuiteStatusById(Long testSuiteId) {
        String executionStatus =
                dsl.select(TEST_SUITE.EXECUTION_STATUS)
                        .from(TEST_SUITE)
                        .where(TEST_SUITE.ID.eq(testSuiteId))
                        .fetchOneInto(String.class);
        return ExecutionStatus.valueOf(executionStatus);
    }

    @Override
    public Set<Long> findCampaignIdsBySuiteIds(Set<Long> longs) {
        return dsl.select(CAMPAIGN_ITERATION.CAMPAIGN_ID)
                .from(CAMPAIGN_ITERATION)
                .innerJoin(ITERATION)
                .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(ITERATION_TEST_SUITE)
                .on(ITERATION.ITERATION_ID.eq(ITERATION_TEST_SUITE.ITERATION_ID))
                .where(ITERATION_TEST_SUITE.TEST_SUITE_ID.in(longs))
                .fetchSet(CAMPAIGN_ITERATION.CAMPAIGN_ID);
    }

    @Override
    public List<NamedReference> findNamedReferences(List<Long> testSuiteIds) {
        return dsl.select(TEST_SUITE.ID, TEST_SUITE.NAME)
                .from(TEST_SUITE)
                .where(TEST_SUITE.ID.in(testSuiteIds))
                .fetch()
                .into(NamedReference.class);
    }

    @Override
    public Map<Long, List<Long>> findExecutionIdsBySuiteIds(List<Long> targetIds) {
        return dsl.select(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID, ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID)
                .from(TEST_SUITE_TEST_PLAN_ITEM)
                .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.in(targetIds))
                .fetchGroups(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID, ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID);
    }

    @Override
    public Map<Long, Set<Long>> findLinkedSuites(Set<Long> testSuiteIds) {
        final TestSuiteTestPlanItem linkedSuite = TEST_SUITE_TEST_PLAN_ITEM.as("linkedSuite");

        final Map<Long, List<Long>> resultWithDuplicates =
                dsl.select(
                                TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID,
                                linkedSuite.field(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID))
                        .from(TEST_SUITE_TEST_PLAN_ITEM)
                        .innerJoin(linkedSuite)
                        .on(
                                TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(
                                        linkedSuite.field(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID)))
                        .where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.in(testSuiteIds))
                        .fetchGroups(
                                TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID,
                                linkedSuite.field(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID));

        final Map<Long, Set<Long>> result = new HashMap<>();

        for (Map.Entry<Long, List<Long>> entry : resultWithDuplicates.entrySet()) {
            result.put(entry.getKey(), new HashSet<>(entry.getValue()));
        }

        return result;
    }
}
