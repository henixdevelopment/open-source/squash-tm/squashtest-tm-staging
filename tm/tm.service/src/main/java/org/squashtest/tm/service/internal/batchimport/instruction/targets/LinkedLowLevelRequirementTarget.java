/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.instruction.targets;

import static org.squashtest.tm.service.importer.EntityType.LINKED_LOW_LEVEL_REQ;

import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.service.importer.EntityType;
import org.squashtest.tm.service.importer.Target;

public class LinkedLowLevelRequirementTarget implements Target {

    private String highLevelReqPath;
    private String standardReqPath;

    public String getHighLevelReqPath() {
        return highLevelReqPath;
    }

    public void setHighLevelReqPath(String highLevelReqPath) {
        String sanitizedPath = highLevelReqPath.trim();
        this.highLevelReqPath = PathUtils.cleanMultipleSlashes(sanitizedPath);
    }

    public String getStandardReqPath() {
        return standardReqPath;
    }

    public void setStandardReqPath(String standardReqPath) {
        String sanitizedPath = standardReqPath.trim();
        this.standardReqPath = PathUtils.cleanMultipleSlashes(sanitizedPath);
    }

    public boolean isHighLevelReqPathWellFormed() {
        return PathUtils.isPathWellFormed(highLevelReqPath);
    }

    public boolean isStandardReqPathWellFormed() {
        return PathUtils.isPathWellFormed(standardReqPath);
    }

    @Override
    public EntityType getType() {
        return LINKED_LOW_LEVEL_REQ;
    }

    @Override
    public boolean isWellFormed() {
        return isHighLevelReqPathWellFormed() && isStandardReqPathWellFormed();
    }

    @Override
    public String getSourcePath() {
        return standardReqPath;
    }
}
