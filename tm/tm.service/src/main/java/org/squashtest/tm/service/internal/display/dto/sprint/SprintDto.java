/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.sprint;

import java.util.Date;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.UserView;

public class SprintDto {

    private Long id;
    private Long projectId;
    private String name;
    private String reference;
    private Date createdOn;
    private String createdBy;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private Date startDate;
    private Date endDate;
    private String description;
    private Long attachmentListId;
    private AttachmentListDto attachmentList;
    private String path;
    private List<SprintReqVersionDto> sprintReqVersions;
    private SprintStatus status;
    private SprintStatus remoteState;
    private String synchronisationKind;
    private int nbIssues;
    private int nbTestPlanItems;
    private List<UserView> assignableUsers;
    private Long campaignLibraryId;

    public SprintDto() {}

    public SprintDto(
            Long id,
            String name,
            String reference,
            Date createdOn,
            String createdBy,
            Date lastModifiedOn,
            String lastModifiedBy) {
        this.id = id;
        this.name = name;
        this.reference = reference;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.lastModifiedOn = lastModifiedOn;
        this.lastModifiedBy = lastModifiedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getAttachmentListId() {
        return attachmentListId;
    }

    public void setAttachmentListId(Long attachmentListId) {
        this.attachmentListId = attachmentListId;
    }

    public AttachmentListDto getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(AttachmentListDto attachmentList) {
        this.attachmentList = attachmentList;
    }

    public List<SprintReqVersionDto> getSprintReqVersions() {
        return sprintReqVersions;
    }

    public void setSprintReqVersions(List<SprintReqVersionDto> sprintReqVersions) {
        this.sprintReqVersions = sprintReqVersions;
    }

    public SprintStatus getStatus() {
        return status;
    }

    public void setStatus(SprintStatus status) {
        this.status = status;
    }

    public SprintStatus getRemoteState() {
        return remoteState;
    }

    public void setRemoteState(SprintStatus remoteState) {
        this.remoteState = remoteState;
    }

    public String getSynchronisationKind() {
        return synchronisationKind;
    }

    public void setSynchronisationKind(String synchronisationKind) {
        this.synchronisationKind = synchronisationKind;
    }

    public int getNbIssues() {
        return nbIssues;
    }

    public void setNbIssues(int nbIssues) {
        this.nbIssues = nbIssues;
    }

    public int getNbTestPlanItems() {
        return nbTestPlanItems;
    }

    public void setNbTestPlanItems(int nbTestPlanItems) {
        this.nbTestPlanItems = nbTestPlanItems;
    }

    public List<UserView> getAssignableUsers() {
        return assignableUsers;
    }

    public void setAssignableUsers(List<UserView> assignableUsers) {
        this.assignableUsers = assignableUsers;
    }

    public Long getCampaignLibraryId() {
        return campaignLibraryId;
    }

    public void setCampaignLibraryId(Long campaignLibraryId) {
        this.campaignLibraryId = campaignLibraryId;
    }
}
