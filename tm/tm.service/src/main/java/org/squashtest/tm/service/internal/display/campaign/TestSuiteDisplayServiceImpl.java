/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import static org.squashtest.tm.service.internal.repository.ParameterNames.TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_TEST_PLAN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTION_STATUSES;
import static org.squashtest.tm.service.security.Authorizations.READ_TS_OR_ROLE_ADMIN;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.display.campaign.TestSuiteDisplayService;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.TestSuiteKnownIssueFinder;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.campaign.TestSuiteDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.TestPlanGridHelpers;
import org.squashtest.tm.service.internal.display.grid.campaign.TestSuiteTestPlanGrid;
import org.squashtest.tm.service.internal.dto.ExecutionSummaryDto;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;

@Service
@Transactional(readOnly = true)
public class TestSuiteDisplayServiceImpl implements TestSuiteDisplayService {

    private final TestSuiteDisplayDao testSuiteDisplayDao;
    private final TestSuiteDao testSuiteDao;
    private final AttachmentDisplayDao attachmentDisplayDao;
    private final CustomFieldValueDisplayDao customFieldValueDisplayDao;
    private final TestSuiteKnownIssueFinder testSuiteKnownIssueFinder;
    private final MilestoneDisplayDao milestoneDisplayDao;
    private final DSLContext dslContext;
    private final TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;
    private final IterationTestPlanItemSuccessRateCalculator successRateCalculator;
    private final AutomatedSuiteDisplayDao automatedSuiteDisplayDao;
    private final ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper;
    private final AvailableDatasetAppender availableDatasetAppender;
    private final UserContextService userContextService;
    private final PermissionEvaluationService permissionEvaluationService;
    private final EntityPathHeaderDao entityPathHeaderDao;
    private final GridConfigurationService gridConfigurationService;
    private final EntityManager entityManager;
    private final MessageSource translateService;

    public TestSuiteDisplayServiceImpl(
            TestSuiteDisplayDao testSuiteDisplayDao,
            TestSuiteDao testSuiteDao,
            AttachmentDisplayDao attachmentDisplayDao,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            TestSuiteKnownIssueFinder testSuiteKnownIssueFinder,
            MilestoneDisplayDao milestoneDisplayDao,
            DSLContext dslContext,
            TestSuiteTestPlanManagerService testSuiteTestPlanManagerService,
            IterationTestPlanItemSuccessRateCalculator successRateCalculator,
            AutomatedSuiteDisplayDao automatedSuiteDisplayDao,
            ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper,
            AvailableDatasetAppender availableDatasetAppender,
            UserContextService userContextService,
            PermissionEvaluationService permissionEvaluationService,
            EntityPathHeaderDao entityPathHeaderDao,
            GridConfigurationService gridConfigurationService,
            EntityManager entityManager,
            MessageSource translateService) {
        this.testSuiteDisplayDao = testSuiteDisplayDao;
        this.testSuiteDao = testSuiteDao;
        this.attachmentDisplayDao = attachmentDisplayDao;
        this.customFieldValueDisplayDao = customFieldValueDisplayDao;
        this.testSuiteKnownIssueFinder = testSuiteKnownIssueFinder;
        this.milestoneDisplayDao = milestoneDisplayDao;
        this.dslContext = dslContext;
        this.testSuiteTestPlanManagerService = testSuiteTestPlanManagerService;
        this.successRateCalculator = successRateCalculator;
        this.automatedSuiteDisplayDao = automatedSuiteDisplayDao;
        this.readUnassignedTestPlanHelper = readUnassignedTestPlanHelper;
        this.availableDatasetAppender = availableDatasetAppender;
        this.userContextService = userContextService;
        this.permissionEvaluationService = permissionEvaluationService;
        this.entityPathHeaderDao = entityPathHeaderDao;
        this.gridConfigurationService = gridConfigurationService;
        this.entityManager = entityManager;
        this.translateService = translateService;
    }

    @PreAuthorize(READ_TS_OR_ROLE_ADMIN)
    @Override
    public TestSuiteDto findTestSuiteView(Long testSuiteId) {
        TestSuiteDto testSuite = testSuiteDisplayDao.findById(testSuiteId);
        testSuite.setAttachmentList(
                attachmentDisplayDao.findAttachmentListById(testSuite.getAttachmentListId()));
        testSuite
                .getCustomFieldValues()
                .addAll(
                        customFieldValueDisplayDao.findCustomFieldValues(
                                BindableEntity.TEST_SUITE, testSuiteId));
        testSuite.setTestPlanStatistics(testSuiteDao.getTestSuiteStatistics(testSuiteId));
        testSuite.setNbIssues(testSuiteKnownIssueFinder.countKnownIssues(testSuiteId));
        testSuite.setMilestones(milestoneDisplayDao.getMilestonesBySuiteId(testSuiteId));
        appendUsers(testSuite);
        testSuite.setExecutionStatusMap(testSuiteDisplayDao.getExecutionStatusMap(testSuiteId));
        testSuite.setNbAutomatedSuites(
                automatedSuiteDisplayDao.countAutomatedSuiteByTestSuiteId(testSuiteId));
        appendNbTestPlanItems(testSuite);
        testSuite.setPath(entityPathHeaderDao.buildTestSuitePathHeader(testSuiteId));
        return testSuite;
    }

    private void appendUsers(TestSuiteDto testSuite) {
        List<User> users =
                this.testSuiteTestPlanManagerService.findAssignableUserForTestPlan(testSuite.getId());
        testSuite.setUsers(UserView.fromEntities(users));
    }

    private void appendNbTestPlanItems(TestSuiteDto testSuite) {
        String login = this.userContextService.getUsername();
        int count;

        if (currentUserCanReadUnassigned(testSuite.getId())) {
            count = this.testSuiteDisplayDao.getNbTestPlanItem(testSuite.getId(), null);
        } else {
            count = this.testSuiteDisplayDao.getNbTestPlanItem(testSuite.getId(), login);
        }
        testSuite.setNbTestPlanItems(count);
    }

    private boolean currentUserCanReadUnassigned(Long testSuiteId) {
        return this.permissionEvaluationService.hasRoleOrPermissionOnObject(
                Roles.ROLE_ADMIN,
                Permissions.READ_UNASSIGNED.name(),
                testSuiteId,
                TestSuite.SIMPLE_CLASS_NAME);
    }

    @PreAuthorize(READ_TS_OR_ROLE_ADMIN)
    @Override
    public GridResponse findTestPlan(Long testSuiteId, GridRequest gridRequest) {
        String userToRestrictTo =
                this.readUnassignedTestPlanHelper.getUserToRestrictTo(
                        testSuiteId, TestSuite.SIMPLE_CLASS_NAME);
        TestSuiteTestPlanGrid testPlanGrid =
                new TestSuiteTestPlanGrid(testSuiteId, userToRestrictTo, testSuiteDisplayDao);
        TestSuite testSuite = entityManager.find(TestSuite.class, testSuiteId);
        Long projectId = testSuite.getProject().getId();
        GridResponse gridResponse = testPlanGrid.getRows(gridRequest, this.dslContext);

        appendDataRows(gridResponse);

        successRateCalculator.appendSuccessRate(gridResponse);
        availableDatasetAppender.appendAvailableDatasets(gridResponse);
        gridResponse.setActiveColumnIds(
                gridConfigurationService.findActiveColumnIdsForUserWithProjectId(
                        gridRequest.getGridId(), projectId));

        return gridResponse;
    }

    private void appendDataRows(GridResponse gridResponse) {
        Set<Long> testPlanIds =
                gridResponse.getDataRows().stream()
                        .map(
                                row ->
                                        (Long)
                                                row.getData().get(RequestAliasesConstants.toCamelCase(ITEM_TEST_PLAN_ID)))
                        .collect(Collectors.toSet());

        Map<Long, List<ExecutionSummaryDto>> lastExecutionStatuses =
                testSuiteTestPlanManagerService.getLastExecutionStatuses(testPlanIds);

        Set<Long> testCaseIds =
                gridResponse.getDataRows().stream()
                        .map(row -> (Long) row.getData().get(RequestAliasesConstants.toCamelCase(TEST_CASE_ID)))
                        .collect(Collectors.toSet());

        Map<Long, String> testCasePathById =
                entityPathHeaderDao.buildTestCasePathByIds(testCaseIds, " / ");

        for (DataRow row : gridResponse.getDataRows()) {
            TestPlanGridHelpers.addTestCasePath(row, testCasePathById);
            TestPlanGridHelpers.formatDeactivatedUserNameInRowData(row, translateService);
            addLastExecutionStatuses(row, lastExecutionStatuses);
        }
    }

    private void addLastExecutionStatuses(
            DataRow row, Map<Long, List<ExecutionSummaryDto>> lastExecutionStatuses) {
        Long rowItemTestPlanId =
                (Long) row.getData().get(RequestAliasesConstants.toCamelCase(ITEM_TEST_PLAN_ID));
        row.addData(
                RequestAliasesConstants.toCamelCase(LAST_EXECUTION_STATUSES),
                lastExecutionStatuses.getOrDefault(rowItemTestPlanId, Collections.emptyList()));
    }

    @PreAuthorize(READ_TS_OR_ROLE_ADMIN)
    @Override
    public Long findIterationIdByTestsuiteId(Long testSuiteId) {
        return testSuiteDisplayDao.findIterationIdByTestsuiteId(testSuiteId);
    }

    @Override
    public ExecutionStatus findTestSuiteStatusById(Long testSuiteId) {
        return testSuiteDisplayDao.findTestSuiteStatusById(testSuiteId);
    }
}
