/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AVAILABLE_DATASETS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.internal.display.dto.testcase.DataSetDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.DatasetDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AvailableDatasetAppender {

    private static final String TEST_CASE_ID_KEY = "testCaseId";

    private final DatasetDisplayDao datasetDisplayDao;

    public AvailableDatasetAppender(DatasetDisplayDao datasetDisplayDao) {
        this.datasetDisplayDao = datasetDisplayDao;
    }

    public void appendAvailableDatasets(GridResponse response) {
        appendAvailableDatasets(response.getDataRows());
    }

    private void appendAvailableDatasets(List<DataRow> dataRows) {
        final List<Long> testCaseIds = extractTestCaseIds(dataRows);
        final Map<Long, List<DataSetDto>> map = datasetDisplayDao.findAllByTestCaseIds(testCaseIds);
        final String key = RequestAliasesConstants.toCamelCase(AVAILABLE_DATASETS);

        dataRows.forEach(
                dataRow -> {
                    final Long testCaseId = (Long) dataRow.getData().get(TEST_CASE_ID_KEY);
                    dataRow.getData().put(key, map.getOrDefault(testCaseId, new ArrayList<>()));
                });
    }

    private List<Long> extractTestCaseIds(List<DataRow> dataRows) {
        return dataRows.stream()
                .map(DataRow::getData)
                .map(data -> (Long) data.get(TEST_CASE_ID_KEY))
                .toList();
    }
}
