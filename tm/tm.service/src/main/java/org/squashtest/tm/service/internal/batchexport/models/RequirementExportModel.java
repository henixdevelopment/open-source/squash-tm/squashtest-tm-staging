/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.LinkedList;
import java.util.List;

public class RequirementExportModel {

    private List<RequirementModel> requirementsModels = new LinkedList<>();

    private List<CoverageModel> coverages = new LinkedList<>();

    private List<RequirementLinkModel> reqLinks = new LinkedList<>();

    private List<LinkedLowLevelRequirementModel> linkedLowLevelReqs = new LinkedList<>();
    private List<RequirementModelFromJooq> requirementModelFromJooqs = new LinkedList<>();

    public RequirementExportModel() {
        super();
    }

    public List<CoverageModel> getCoverages() {
        return coverages;
    }

    public void setCoverages(List<CoverageModel> coverages) {
        this.coverages = coverages;
    }

    public List<RequirementModel> getRequirementsModels() {
        return requirementsModels;
    }

    public void setRequirementsModels(List<RequirementModel> requirementsModels) {
        this.requirementsModels = requirementsModels;
    }

    public void setRequirementsModelsFromJooq(
            List<RequirementModelFromJooq> requirementModelFromJooqs) {
        this.requirementModelFromJooqs = requirementModelFromJooqs;
    }

    public List<RequirementLinkModel> getReqLinks() {
        return reqLinks;
    }

    public void setReqLinks(List<RequirementLinkModel> reqLinks) {
        this.reqLinks = reqLinks;
    }

    public List<LinkedLowLevelRequirementModel> getLinkedLowLevelReqs() {
        return linkedLowLevelReqs;
    }

    public List<RequirementModelFromJooq> getRequirementModelFromJooqs() {
        return requirementModelFromJooqs;
    }

    public void setLinkedLowLevelReqs(List<LinkedLowLevelRequirementModel> linkedLowLevelReqs) {
        this.linkedLowLevelReqs = linkedLowLevelReqs;
    }

    public interface RequirementPathSortable {
        String getProjectName();

        String getPath();

        int getRequirementVersionNumber();
    }
}
