/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import static org.jooq.impl.DSL.coalesce;
import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.max;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.LEGACY_REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.LEGACY_REQUIREMENT_SQL_VALIDATION_STATISTICS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.LEGACY_REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_IDS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_BOUND_DESC_STATISTICS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_BOUND_DESC_STATISTICS_BY_VERSION_IDS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_BOUND_TCS_STATISTICS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_BOUND_TCS_STATISTICS_BY_VERSION_IDS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_COVERAGE_STATISTICS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_COVERAGE_STATISTICS_BY_VERSION_IDS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION_BY_VERSION_IDS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_VALIDATION_STATISTICS;
import static org.squashtest.tm.service.internal.repository.hibernate.NativeQueries.REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_IDS;
import static org.squashtest.tm.service.statistics.helpers.WorkspaceStatisticsHelper.SQL_IN_CLAUSE_ITEMS_LIMIT;
import static org.squashtest.tm.service.statistics.helpers.WorkspaceStatisticsHelper.retrieveAllStatisticsResultPartitions;
import static org.squashtest.tm.service.statistics.helpers.WorkspaceStatisticsHelper.retrieveAllStatisticsResultPartitionsIntoMap;
import static org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat.SimpleRequirementStats.REDACTION_RATE_KEY;
import static org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat.SimpleRequirementStats.VALIDATION_RATE_KEY;
import static org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat.SimpleRequirementStats.VERIFICATION_RATE_KEY;

import com.google.common.collect.Lists;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.SelectOnConditionStep;
import org.jooq.Table;
import org.jooq.TableOnConditionStep;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionBundleStatsDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionStatsDto;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.requirement.RequirementStatisticsService;
import org.squashtest.tm.service.statistics.requirement.RequirementBoundDescriptionStatistics;
import org.squashtest.tm.service.statistics.requirement.RequirementBoundTestCasesStatistics;
import org.squashtest.tm.service.statistics.requirement.RequirementCoverageStatistics;
import org.squashtest.tm.service.statistics.requirement.RequirementCriticalityStatistics;
import org.squashtest.tm.service.statistics.requirement.RequirementStatisticsBundle;
import org.squashtest.tm.service.statistics.requirement.RequirementStatusesStatistics;
import org.squashtest.tm.service.statistics.requirement.RequirementValidationStatistics;
import org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat;

@Service("RequirementStatisticsService")
@Transactional(readOnly = true)

// that SuppressWarning is intended for SONAR so it ignores the rule squid:S1192 (that is, define
// constants for long Strings)
@SuppressWarnings("squid:S1192")
public class RequirementStatisticsServiceImpl implements RequirementStatisticsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequirementStatisticsService.class);

    private static final String CRITICALITY_STATISTICS_NAMED_QUERY =
            "RequirementStatistics.criticalityStatistics";
    private static final String CRITICALITY_STATISTICS_BY_VERSION_IDS_NAMED_QUERY =
            "RequirementStatistics.criticalityStatisticsByVersionIds";
    private static final String STATUSES_STATISTICS_NAMED_QUERY =
            "RequirementStatistics.statusesStatistics";
    private static final String STATUSES_STATISTICS_BY_VERSION_IDS_NAMED_QUERY =
            "RequirementStatistics.statusesStatisticsByVersionIds";

    private static final String UNDEFINED = "UNDEFINED";
    private static final String MINOR = "MINOR";
    private static final String MAJOR = "MAJOR";
    private static final String CRITICAL = "CRITICAL";

    private static final String CRITICALITY_ERROR_MESSAGE =
            "RequirementStatisticsService cannot handle the following RequirementCriticality value : ";

    private static final String REQUIREMENT_IDS = "requirementIds";
    private static final String REQUIREMENT_VERSION_IDS = "requirementVersionIds";
    private static final String CRITICALITY = "criticality";
    private static final String VALIDATION_STATUS = "validationStatus";

    @Value("${squash.legacy.requirement.validation.chart:false}")
    private boolean withLegacyValidationStatistics;

    @PersistenceContext private EntityManager entityManager;

    @Inject private DSLContext dsl;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private RequirementDao requirementDao;

    @Override
    public RequirementStatisticsBundle gatherRequirementStatisticsBundle(
            Collection<Long> requirementIds) {

        RequirementBoundTestCasesStatistics tcs = gatherBoundTestCaseStatistics(requirementIds);
        RequirementStatusesStatistics status = gatherRequirementStatusesStatistics(requirementIds);
        RequirementCriticalityStatistics criticality =
                gatherRequirementCriticalityStatistics(requirementIds);
        RequirementBoundDescriptionStatistics description =
                gatherRequirementBoundDescriptionStatistics(requirementIds);
        RequirementCoverageStatistics coverage = gatherRequirementCoverageStatistics(requirementIds);
        RequirementValidationStatistics validation =
                gatherRequirementValidationStatistics(requirementIds);

        return new RequirementStatisticsBundle(
                tcs, status, criticality, description, coverage, validation, requirementIds);
    }

    @Override
    public RequirementStatisticsBundle gatherRequirementStatisticsBundleForActiveMilestone(
            Collection<Long> requirementIds, Milestone activeMilestone) {
        List<Long> reqVersionIdsInMilestoneAndSelection =
                findAllReqVersionIdsInMilestoneAndSelectedReqIds(activeMilestone, requirementIds);

        RequirementBoundTestCasesStatistics tcs =
                gatherBoundTestCaseStatisticsByVersionIds(reqVersionIdsInMilestoneAndSelection);
        RequirementStatusesStatistics status =
                gatherRequirementStatusesStatisticsByVersionIds(reqVersionIdsInMilestoneAndSelection);
        RequirementCriticalityStatistics criticality =
                gatherRequirementCriticalityStatisticsByVersionIds(reqVersionIdsInMilestoneAndSelection);
        RequirementBoundDescriptionStatistics description =
                gatherRequirementBoundDescriptionStatisticsByVersionIds(
                        reqVersionIdsInMilestoneAndSelection);
        RequirementCoverageStatistics coverage =
                gatherRequirementCoverageStatisticsByVersionIds(reqVersionIdsInMilestoneAndSelection);
        RequirementValidationStatistics validation =
                gatherRequirementValidationStatisticsByVersionIds(reqVersionIdsInMilestoneAndSelection);

        return new RequirementStatisticsBundle(
                tcs,
                status,
                criticality,
                description,
                coverage,
                validation,
                reqVersionIdsInMilestoneAndSelection);
    }

    @Override
    public RequirementBoundTestCasesStatistics gatherBoundTestCaseStatistics(
            Collection<Long> requirementIds) {
        return doGatherBoundTestCaseStatistics(
                requirementIds, REQUIREMENT_SQL_BOUND_TCS_STATISTICS, REQUIREMENT_IDS);
    }

    @Override
    public RequirementBoundTestCasesStatistics gatherBoundTestCaseStatisticsByVersionIds(
            Collection<Long> requirementVersionIds) {
        return doGatherBoundTestCaseStatistics(
                requirementVersionIds,
                REQUIREMENT_SQL_BOUND_TCS_STATISTICS_BY_VERSION_IDS,
                REQUIREMENT_VERSION_IDS);
    }

    private RequirementBoundTestCasesStatistics doGatherBoundTestCaseStatistics(
            Collection<Long> ids, String queryString, String parameterName) {
        if (ids.isEmpty()) {
            return new RequirementBoundTestCasesStatistics();
        }

        Map<Object, Integer> tuples =
                retrieveAllStatisticsResultPartitionsIntoMap(
                        queryString, true, parameterName, ids, entityManager);

        return createRequirementBoundTestCasesStatistics(tuples);
    }

    private RequirementBoundTestCasesStatistics createRequirementBoundTestCasesStatistics(
            Map<Object, Integer> tuples) {
        RequirementBoundTestCasesStatistics stats = new RequirementBoundTestCasesStatistics();

        Integer sizeClass;
        Integer count;
        for (Entry<Object, Integer> tuple : tuples.entrySet()) {

            sizeClass = (Integer) tuple.getKey();
            count = tuple.getValue();

            switch (sizeClass) {
                case 0:
                    stats.setZeroTestCases(count);
                    break;
                case 1:
                    stats.setOneTestCase(count);
                    break;
                case 2:
                    stats.setManyTestCases(count);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "RequirementStatisticsServiceImpl#gatherBoundTestCaseStatistics : "
                                    + "there should not be a sizeclass <0 or >2. It's a bug.");
            }
        }
        return stats;
    }

    @Override
    public RequirementCriticalityStatistics gatherRequirementCriticalityStatistics(
            Collection<Long> requirementIds) {
        if (requirementIds.isEmpty()) {
            return new RequirementCriticalityStatistics();
        }

        Map<Object, Integer> tuples =
                retrieveAllStatisticsResultPartitionsIntoMap(
                        CRITICALITY_STATISTICS_NAMED_QUERY,
                        false,
                        REQUIREMENT_IDS,
                        requirementIds,
                        entityManager);

        return createRequirementCriticalityStats(tuples);
    }

    @Override
    public RequirementCriticalityStatistics gatherRequirementCriticalityStatisticsByVersionIds(
            Collection<Long> requirementVersionIds) {
        if (requirementVersionIds.isEmpty()) {
            return new RequirementCriticalityStatistics();
        }

        Map<Object, Integer> tuples =
                retrieveAllStatisticsResultPartitionsIntoMap(
                        CRITICALITY_STATISTICS_BY_VERSION_IDS_NAMED_QUERY,
                        false,
                        REQUIREMENT_VERSION_IDS,
                        requirementVersionIds,
                        entityManager);

        return createRequirementCriticalityStats(tuples);
    }

    private RequirementCriticalityStatistics createRequirementCriticalityStats(
            Map<Object, Integer> tuples) {
        RequirementCriticalityStatistics stats = new RequirementCriticalityStatistics();

        RequirementCriticality criticality;
        Integer cardinality;
        for (Entry<Object, Integer> tuple : tuples.entrySet()) {
            criticality = (RequirementCriticality) tuple.getKey();
            cardinality = tuple.getValue();
            switch (criticality) {
                case UNDEFINED:
                    stats.setUndefined(cardinality);
                    break;
                case MINOR:
                    stats.setMinor(cardinality);
                    break;
                case MAJOR:
                    stats.setMajor(cardinality);
                    break;
                case CRITICAL:
                    stats.setCritical(cardinality);
                    break;
                default:
                    throw new IllegalArgumentException(CRITICALITY_ERROR_MESSAGE + tuple.getKey());
            }
        }

        return stats;
    }

    @Override
    public RequirementStatusesStatistics gatherRequirementStatusesStatistics(
            Collection<Long> requirementIds) {
        if (requirementIds.isEmpty()) {
            return new RequirementStatusesStatistics();
        }

        Map<Object, Integer> tuples =
                retrieveAllStatisticsResultPartitionsIntoMap(
                        STATUSES_STATISTICS_NAMED_QUERY, false, REQUIREMENT_IDS, requirementIds, entityManager);

        return createRequirementStatusesStats(tuples);
    }

    @Override
    public RequirementStatusesStatistics gatherRequirementStatusesStatisticsByVersionIds(
            Collection<Long> requirementVersionIds) {
        if (requirementVersionIds.isEmpty()) {
            return new RequirementStatusesStatistics();
        }

        Map<Object, Integer> tuples =
                retrieveAllStatisticsResultPartitionsIntoMap(
                        STATUSES_STATISTICS_BY_VERSION_IDS_NAMED_QUERY,
                        false,
                        REQUIREMENT_VERSION_IDS,
                        requirementVersionIds,
                        entityManager);

        return createRequirementStatusesStats(tuples);
    }

    private RequirementStatusesStatistics createRequirementStatusesStats(
            Map<Object, Integer> tuples) {
        RequirementStatusesStatistics stats = new RequirementStatusesStatistics();

        RequirementStatus status;
        Integer cardinality;
        for (Entry<Object, Integer> tuple : tuples.entrySet()) {
            status = (RequirementStatus) tuple.getKey();
            cardinality = tuple.getValue();
            switch (status) {
                case WORK_IN_PROGRESS:
                    stats.setWorkInProgress(cardinality);
                    break;
                case UNDER_REVIEW:
                    stats.setUnderReview(cardinality);
                    break;
                case APPROVED:
                    stats.setApproved(cardinality);
                    break;
                case OBSOLETE:
                    stats.setObsolete(cardinality);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "RequirmentStatisticsService cannot handle the following RequirementStatus value : '"
                                    + tuple.getKey()
                                    + "'");
            }
        }
        return stats;
    }

    @Override
    public RequirementBoundDescriptionStatistics gatherRequirementBoundDescriptionStatistics(
            Collection<Long> requirementIds) {
        if (requirementIds.isEmpty()) {
            return new RequirementBoundDescriptionStatistics();
        }

        Map<Object, Integer> tuples =
                retrieveAllStatisticsResultPartitionsIntoMap(
                        REQUIREMENT_SQL_BOUND_DESC_STATISTICS,
                        true,
                        REQUIREMENT_IDS,
                        requirementIds,
                        entityManager);

        return createRequirementBoundDescriptionStats(tuples);
    }

    @Override
    public RequirementBoundDescriptionStatistics
            gatherRequirementBoundDescriptionStatisticsByVersionIds(
                    Collection<Long> requirementVersionIds) {
        if (requirementVersionIds.isEmpty()) {
            return new RequirementBoundDescriptionStatistics();
        }

        Map<Object, Integer> tuples =
                retrieveAllStatisticsResultPartitionsIntoMap(
                        REQUIREMENT_SQL_BOUND_DESC_STATISTICS_BY_VERSION_IDS,
                        true,
                        REQUIREMENT_VERSION_IDS,
                        requirementVersionIds,
                        entityManager);

        return createRequirementBoundDescriptionStats(tuples);
    }

    private RequirementBoundDescriptionStatistics createRequirementBoundDescriptionStats(
            Map<Object, Integer> tuples) {
        RequirementBoundDescriptionStatistics stats = new RequirementBoundDescriptionStatistics();

        boolean hasDescription;
        Integer count;
        for (Entry<Object, Integer> tuple : tuples.entrySet()) {

            /* If only one requirement is present,
             * request return tuple[0] as a BigInteger, it returns an Integer in other cases. */
            try {
                hasDescription = (Integer) tuple.getKey() != 0;
            } catch (ClassCastException exception) {
                hasDescription = ((BigInteger) tuple.getKey()).intValue() != 0;
                LOGGER.debug("BigInteger handled.", exception);
            }
            count = tuple.getValue();

            if (hasDescription) {
                stats.setHasDescription(count);
            } else {
                stats.setHasNoDescription(count);
            }
        }

        return stats;
    }

    @Override
    public RequirementCoverageStatistics gatherRequirementCoverageStatistics(
            Collection<Long> requirementIds) {
        if (requirementIds.isEmpty()) {
            return new RequirementCoverageStatistics();
        }

        Map<String, Object[]> tuples =
                retrieveAllRequirementCoverageStatisticsIntoMap(
                        REQUIREMENT_SQL_COVERAGE_STATISTICS,
                        true,
                        REQUIREMENT_IDS,
                        requirementIds,
                        entityManager);

        return createRequirementCoverageStatistics(tuples);
    }

    @Override
    public RequirementCoverageStatistics gatherRequirementCoverageStatisticsByVersionIds(
            Collection<Long> requirementVersionIds) {
        if (requirementVersionIds.isEmpty()) {
            return new RequirementCoverageStatistics();
        }

        Map<String, Object[]> tuples =
                retrieveAllRequirementCoverageStatisticsIntoMap(
                        REQUIREMENT_SQL_COVERAGE_STATISTICS_BY_VERSION_IDS,
                        true,
                        REQUIREMENT_VERSION_IDS,
                        requirementVersionIds,
                        entityManager);

        return createRequirementCoverageStatistics(tuples);
    }

    private RequirementCoverageStatistics createRequirementCoverageStatistics(
            Map<String, Object[]> tuples) {
        RequirementCoverageStatistics stats = new RequirementCoverageStatistics();

        String criticality;
        int count;
        int total;

        for (Entry<String, Object[]> tuple : tuples.entrySet()) {

            criticality = tuple.getKey();
            count = ((BigInteger) tuple.getValue()[0]).intValue();
            total = ((BigInteger) tuple.getValue()[1]).intValue();

            switch (criticality) {
                case UNDEFINED:
                    stats.setUndefined(count);
                    stats.setTotalUndefined(total);
                    break;
                case MINOR:
                    stats.setMinor(count);
                    stats.setTotalMinor(total);
                    break;
                case MAJOR:
                    stats.setMajor(count);
                    stats.setTotalMajor(total);
                    break;
                case CRITICAL:
                    stats.setCritical(count);
                    stats.setTotalCritical(total);
                    break;
                default:
                    throw new IllegalArgumentException(CRITICALITY_ERROR_MESSAGE + tuple.getKey());
            }
        }

        return stats;
    }

    @Override
    public RequirementValidationStatistics gatherRequirementValidationStatistics(
            Collection<Long> requirementIds) {
        return doGatherRequirementValidationStatistics(
                requirementIds,
                LEGACY_REQUIREMENT_SQL_VALIDATION_STATISTICS,
                REQUIREMENT_SQL_VALIDATION_STATISTICS,
                REQUIREMENT_IDS);
    }

    private RequirementValidationStatistics doGatherRequirementValidationStatistics(
            Collection<Long> requirementIds,
            String legacyRequirementSqlValidationStatistics,
            String requirementSqlValidationStatistics,
            String parameterName) {
        if (requirementIds.isEmpty()) {
            return new RequirementValidationStatistics();
        }

        String query =
                withLegacyValidationStatistics
                        ? legacyRequirementSqlValidationStatistics
                        : requirementSqlValidationStatistics;

        List<Object[]> tuples =
                retrieveAllRequirementValidationStatistics(
                        query, true, parameterName, requirementIds, entityManager);

        if (withLegacyValidationStatistics) {
            RequirementValidationStatistics legacyStatistics = createRequirementValidationStats(tuples);
            legacyStatistics.setLegacy(true);
            return legacyStatistics;
        } else {
            return createRequirementValidationStats(tuples);
        }
    }

    @Override
    public RequirementValidationStatistics gatherRequirementValidationStatisticsByVersionIds(
            Collection<Long> requirementVersionIds) {
        return doGatherRequirementValidationStatistics(
                requirementVersionIds,
                LEGACY_REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_IDS,
                REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_IDS,
                REQUIREMENT_VERSION_IDS);
    }

    private RequirementValidationStatistics createRequirementValidationStats(List<Object[]> tuples) {
        RequirementValidationStatistics stats = new RequirementValidationStatistics();

        String requirementCriticality;
        String executionStatus;
        int count;

        for (Object[] tuple : tuples) {
            requirementCriticality = (String) tuple[0];
            executionStatus = (String) tuple[1];
            count = ((BigInteger) tuple[2]).intValue();

            // If the TestCase has no executions, it counts as an Undefined Test
            if (executionStatus == null) executionStatus = "NOT_RUN";

            determineValidationStatisticsCount(stats, requirementCriticality, executionStatus, count);
        }

        return stats;
    }

    private void determineValidationStatisticsCount(
            RequirementValidationStatistics stats,
            String requirementCriticality,
            String executionStatus,
            Integer count) {
        switch (executionStatus) {
            case "SUCCESS":
            case "SETTLED":
                determineConclusiveValidationCount(stats, requirementCriticality, count);
                break;
            case "FAILURE":
                determineInconclusiveValidationCount(stats, requirementCriticality, count);
                break;
            case "BLOCKED":
            case "ERROR":
            case "NOT_FOUND":
            case "NOT_RUN":
            case "READY":
            case "RUNNING":
            case "UNTESTABLE":
            case "WARNING":
                determineUndefinedValidationCount(stats, requirementCriticality, count);
                break;
            default:
                throw new IllegalArgumentException(
                        "RequirementStatisticsService cannot handle the following ExecutionStatus value : '"
                                + executionStatus
                                + "'");
        }
    }

    private void determineUndefinedValidationCount(
            RequirementValidationStatistics stats, String requirementCriticality, Integer count) {
        switch (requirementCriticality) {
            case UNDEFINED:
                stats.setUndefinedUndefined(stats.getUndefinedUndefined() + count);
                break;
            case MINOR:
                stats.setUndefinedMinor(stats.getUndefinedMinor() + count);
                break;
            case MAJOR:
                stats.setUndefinedMajor(stats.getUndefinedMajor() + count);
                break;
            case CRITICAL:
                stats.setUndefinedCritical(stats.getUndefinedCritical() + count);
                break;
            default:
                throw new IllegalArgumentException(CRITICALITY_ERROR_MESSAGE + requirementCriticality);
        }
    }

    private void determineInconclusiveValidationCount(
            RequirementValidationStatistics stats, String requirementCriticality, Integer count) {
        switch (requirementCriticality) {
            case UNDEFINED:
                stats.setInconclusiveUndefined(count);
                break;
            case MINOR:
                stats.setInconclusiveMinor(count);
                break;
            case MAJOR:
                stats.setInconclusiveMajor(count);
                break;
            case CRITICAL:
                stats.setInconclusiveCritical(count);
                break;
            default:
                throw new IllegalArgumentException(CRITICALITY_ERROR_MESSAGE + requirementCriticality);
        }
    }

    private void determineConclusiveValidationCount(
            RequirementValidationStatistics stats, String requirementCriticality, Integer count) {
        switch (requirementCriticality) {
            case UNDEFINED:
                stats.setConclusiveUndefined(stats.getConclusiveUndefined() + count);
                break;
            case MINOR:
                stats.setConclusiveMinor(stats.getConclusiveMinor() + count);
                break;
            case MAJOR:
                stats.setConclusiveMajor(stats.getConclusiveMajor() + count);
                break;
            case CRITICAL:
                stats.setConclusiveCritical(stats.getConclusiveCritical() + count);
                break;
            default:
                throw new IllegalArgumentException(CRITICALITY_ERROR_MESSAGE + requirementCriticality);
        }
    }

    @Override
    public List<Long> findAllReqVersionIdsInMilestoneAndSelectedReqIds(
            Milestone activeMilestone, Collection<Long> requirementIds) {
        if (activeMilestone != null) {
            List<Long> milestoneIds = new ArrayList<>();
            milestoneIds.add(activeMilestone.getId());
            return requirementDao.findAllRequirementIdsFromMilestonesAndSelectedReqIds(
                    milestoneIds, requirementIds);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Collection<Long> gatherRequirementIdsFromValidation(
            Collection<Long> requirementIds,
            RequirementCriticality criticality,
            Collection<String> validationStatus) {
        final String query =
                withLegacyValidationStatistics
                        ? LEGACY_REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION
                        : REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION;
        return doGatherRequirementIdsFromValidation(
                requirementIds, criticality, validationStatus, query, REQUIREMENT_IDS);
    }

    @Override
    public Collection<Long> gatherRequirementIdsFromValidationByVersionIds(
            Collection<Long> requirementVersionIds,
            RequirementCriticality criticality,
            Collection<String> validationStatus) {
        return doGatherRequirementIdsFromValidation(
                requirementVersionIds,
                criticality,
                validationStatus,
                REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION_BY_VERSION_IDS,
                REQUIREMENT_VERSION_IDS);
    }

    private Collection<Long> doGatherRequirementIdsFromValidation(
            Collection<Long> reqIds,
            RequirementCriticality criticality,
            Collection<String> validationStatus,
            String queryString,
            String mainQueryParameter) {
        if (reqIds.isEmpty()) {
            return new ArrayList<>();
        }

        List<List<Long>> partitionsIds =
                Lists.partition(new ArrayList<>(reqIds), SQL_IN_CLAUSE_ITEMS_LIMIT);
        List<List<BigInteger>> partitionResults = new ArrayList<>();
        for (List<Long> partitionIds : partitionsIds) {
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(mainQueryParameter, partitionIds);
            query.setParameter(CRITICALITY, criticality.toString());
            query.setParameter(VALIDATION_STATUS, validationStatus);

            partitionResults.add(query.getResultList());
        }
        Set<Long> reqIdsList = new HashSet<>();
        for (List<BigInteger> ids : partitionResults) {
            reqIdsList.addAll(ids.stream().map(BigInteger::longValue).collect(Collectors.toSet()));
        }
        return new ArrayList<>(reqIdsList);
    }

    @Override
    public RequirementVersionBundleStat findSimplifiedCoverageStats(
            String remoteSynchronisationKind) {
        RequirementVersionBundleStat bundle = new RequirementVersionBundleStat();
        computeRedactionRate(remoteSynchronisationKind, bundle);
        // compute verification rates
        computeItpiByStatusRate(
                remoteSynchronisationKind,
                bundle,
                ExecutionStatus.getTerminatedStatusSet(),
                EnumSet.allOf(ExecutionStatus.class),
                VERIFICATION_RATE_KEY);
        // compute validation rates
        computeItpiByStatusRate(
                remoteSynchronisationKind,
                bundle,
                ExecutionStatus.getSuccessStatusSet(),
                ExecutionStatus.getTerminatedStatusSet(),
                VALIDATION_RATE_KEY);
        return bundle;
    }

    @Override
    public RequirementVersionBundleStatsDto findCoveragesStatsByRequirementVersionId(
            Long requirementVersionId) {
        List<Long> descendantResId = getNonObsoleteDescendantIds(requirementVersionId);
        return findCoveragesStats(requirementVersionId, descendantResId);
    }

    @Override
    public RequirementVersionBundleStatsDto findCoveragesStatsByHighLvlReqVersionId(
            Long requirementVersionId) {
        List<Long> descendantResId = getHighLvlReqNonObsoleteDescendantIds(requirementVersionId);
        return findCoveragesStats(requirementVersionId, descendantResId);
    }

    private RequirementVersionBundleStatsDto findCoveragesStats(
            Long requirementVersionId, List<Long> descendantResId) {
        RequirementVersionBundleStatsDto bundleStatsDto = new RequirementVersionBundleStatsDto();

        appendCoverageStats(bundleStatsDto, descendantResId);

        bundleStatsDto.setHaveChildren(!descendantResId.isEmpty());
        descendantResId.add(requirementVersionId);

        RequirementVersionStatsDto currentVersionStats = new RequirementVersionStatsDto();
        RequirementVersionStatsDto childrenStats = new RequirementVersionStatsDto();

        appendRedactionStats(requirementVersionId, descendantResId, currentVersionStats, childrenStats);

        appendItpiByStatusStats(
                requirementVersionId,
                descendantResId,
                ExecutionStatus.getTerminatedStatusSet(),
                EnumSet.allOf(ExecutionStatus.class),
                currentVersionStats,
                childrenStats,
                VERIFICATION_RATE_KEY);

        appendItpiByStatusStats(
                requirementVersionId,
                descendantResId,
                ExecutionStatus.getSuccessStatusSet(),
                ExecutionStatus.getTerminatedStatusSet(),
                currentVersionStats,
                childrenStats,
                VALIDATION_RATE_KEY);

        bundleStatsDto.setChildren(childrenStats);
        bundleStatsDto.setCurrentVersion(currentVersionStats);
        bundleStatsDto.setTotal(createTotalStats(currentVersionStats, childrenStats));

        return bundleStatsDto;
    }

    private void appendCoverageStats(
            RequirementVersionBundleStatsDto bundle, List<Long> descendantResId) {
        final int[] numCovered = new int[] {0};

        dsl.select(count(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
                .from(REQUIREMENT_VERSION_COVERAGE)
                .where(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.in(descendantResId))
                .groupBy(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID)
                .fetch()
                .forEach(
                        r -> {
                            int testCaseCount = r.component1();
                            if (testCaseCount > 0) {
                                numCovered[0] += 1;
                            }
                        });

        bundle.setCoveredDescendantsCount(numCovered[0]);
        bundle.setNonObsoleteDescendantsCount(descendantResId.size());
    }

    @Override
    public RequirementVersionBundleStatsDto findCoveragesStatsByHighLvlReqId(Long requirementId) {
        org.squashtest.tm.domain.requirement.Requirement req =
                entityManager.find(org.squashtest.tm.domain.requirement.Requirement.class, requirementId);
        return req != null
                ? findCoveragesStatsByHighLvlReqVersionId(req.getCurrentVersion().getId())
                : new RequirementVersionBundleStatsDto();
    }

    private RequirementVersionStatsDto createTotalStats(
            RequirementVersionStatsDto currentVersionStats, RequirementVersionStatsDto childrenStats) {
        RequirementVersionStatsDto total = new RequirementVersionStatsDto();
        total.setPlannedTestCase(
                currentVersionStats.getPlannedTestCase() + childrenStats.getPlannedTestCase());
        total.setVerifiedTestCase(
                currentVersionStats.getVerifiedTestCase() + childrenStats.getVerifiedTestCase());
        total.setValidatedTestCases(
                currentVersionStats.getValidatedTestCases() + childrenStats.getValidatedTestCases());
        total.setExecutedTestCase(
                currentVersionStats.getExecutedTestCase() + childrenStats.getExecutedTestCase());
        total.setAllTestCaseCount(
                currentVersionStats.getAllTestCaseCount() + childrenStats.getAllTestCaseCount());
        total.setRedactedTestCase(
                currentVersionStats.getRedactedTestCase() + childrenStats.getRedactedTestCase());
        return total;
    }

    private void appendRedactionStats(
            Long requirementVersionId,
            List<Long> descendantResId,
            RequirementVersionStatsDto currentVersionStats,
            RequirementVersionStatsDto childrenStats) {

        TableOnConditionStep<Record> joinFromRequirementVersionToTestCase =
                REQUIREMENT_VERSION
                        .innerJoin(REQUIREMENT_VERSION_COVERAGE)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                        .innerJoin(TEST_CASE)
                        .on(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID));

        // creating a "virtual table" like ReqVersionId | CountCoverageTC to avoid correlated sub
        // queries
        Field<Long> reqIdAllTC = REQUIREMENT_VERSION.RES_ID.as("reqIdAllTC");
        Field<Integer> countAllTC = count().as("countAllTC");
        Table<Record2<Long, Integer>> allTestCase =
                dsl.select(reqIdAllTC, countAllTC)
                        .from(joinFromRequirementVersionToTestCase)
                        .where(REQUIREMENT_VERSION.RES_ID.in(descendantResId))
                        .groupBy(REQUIREMENT_VERSION.RES_ID)
                        .asTable("allTestCase");

        // creating a "virtual table" like ReqVersionId | CountCoverageWithGoodStatus TC to avoid
        // correlated sub queries
        Field<Long> reqIdValidatedTC = REQUIREMENT_VERSION.RES_ID.as("reqIdValidatedTC");
        Field<Integer> countValidatedTC = count().as("countValidatedTC");
        Table<Record2<Long, Integer>> validatedTestCase =
                dsl.select(reqIdValidatedTC, countValidatedTC)
                        .from(joinFromRequirementVersionToTestCase)
                        .where(REQUIREMENT_VERSION.RES_ID.in(descendantResId))
                        .and(
                                TEST_CASE.TC_STATUS.in(
                                        TestCaseStatus.UNDER_REVIEW.name(), TestCaseStatus.APPROVED.name()))
                        .groupBy(REQUIREMENT_VERSION.RES_ID)
                        .asTable("verifiedTestCase");

        // making our main request with joins versus our "virtual tables" and fetch into stat bundle
        dsl.select(
                        REQUIREMENT_VERSION.RES_ID,
                        allTestCase.field(countAllTC),
                        validatedTestCase.field(countValidatedTC))
                .from(REQUIREMENT_VERSION)
                .leftJoin(allTestCase)
                .on(
                        REQUIREMENT_VERSION.RES_ID.eq(
                                allTestCase.field(
                                        reqIdAllTC))) // performing left join to have tuple even if no coverages for
                // requirement
                .leftJoin(validatedTestCase)
                .on(REQUIREMENT_VERSION.RES_ID.eq(validatedTestCase.field(reqIdValidatedTC)))
                .where(REQUIREMENT_VERSION.RES_ID.in(descendantResId))
                .fetch()
                .forEach(
                        r -> {
                            Long reqId = r.get(REQUIREMENT_VERSION.RES_ID);
                            Integer countAllTestCase =
                                    Optional.ofNullable(r.get(allTestCase.field(countAllTC))).orElse(0);
                            Integer countValidatedTestCase =
                                    Optional.ofNullable(r.get(validatedTestCase.field(countValidatedTC))).orElse(0);

                            if (reqId.equals(requirementVersionId)) {
                                currentVersionStats.setAllTestCaseCount(countAllTestCase);
                                currentVersionStats.setRedactedTestCase(countValidatedTestCase);
                            } else {
                                childrenStats.setAllTestCaseCount(
                                        childrenStats.getAllTestCaseCount() + countAllTestCase);
                                childrenStats.setRedactedTestCase(
                                        childrenStats.getRedactedTestCase() + countValidatedTestCase);
                            }
                        });
    }

    private void appendItpiByStatusStats(
            Long requirementVersionId,
            List<Long> requirementVersionIds,
            Set<ExecutionStatus> matchingStatusSet,
            Set<ExecutionStatus> allStatusSet,
            RequirementVersionStatsDto currentVersionStats,
            RequirementVersionStatsDto childrenStats,
            String key) {

        // preparing our join from REQUIREMENT_VERSION to ITPI
        TableOnConditionStep<Record> joinFromRequirementVersionToITPI =
                REQUIREMENT_VERSION
                        .innerJoin(REQUIREMENT_VERSION_COVERAGE)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                        .innerJoin(TEST_CASE)
                        .on(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(TEST_CASE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID));

        Field<Long> lastExecutionTC = ITERATION_TEST_PLAN_ITEM.TCLN_ID.as("lastExecutionTC");
        Field<Long> lastExecutionDS =
                coalesce(ITERATION_TEST_PLAN_ITEM.DATASET_ID, 0L)
                        .as("lastExecutionDS"); // coalesce null dataset to 0, so we can make a join
        Field<Timestamp> lastExecutionDate =
                coalesce(max(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON), new Timestamp(0L))
                        .as("lastExecutionDate"); // coalesce null execution date to now, so we can make a join

        // Here we perform a subrequest that will create a result like TCLN_ID, DATASET_ID,
        // MAX(LAST_EXECUTED_ON)
        // So we can use this to perform a join with ours other sub queries :
        // A join between TCLN_ID, DATASET_ID, LAST_EXECUTED_ON  -> TCLN_ID, DATASET_ID,
        // MAX(LAST_EXECUTED_ON)
        // So we have the last executed ITPI for every combination of TCLN_ID - DATASET_ID
        Table<Record3<Long, Long, Timestamp>> selectLastExecution =
                dsl.select(lastExecutionTC, lastExecutionDS, lastExecutionDate)
                        .from(joinFromRequirementVersionToITPI)
                        .where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
                        .groupBy(ITERATION_TEST_PLAN_ITEM.TCLN_ID, ITERATION_TEST_PLAN_ITEM.DATASET_ID)
                        .asTable("selectLastExecution");

        Field<Long> reqIdAll = REQUIREMENT_VERSION.RES_ID.as("reqIdAll");
        Field<Integer> countAll = count().as("countAll");

        Table<Record2<Long, Integer>> allITPI =
                dsl.select(reqIdAll, countAll)
                        .from(joinFromRequirementVersionToITPI)
                        .innerJoin(selectLastExecution) // inner join to avoid correlated sub query like
                        // LAST_EXECUTED_ON = SELECT(MAX(LAST_EXECUTED_ON))... which
                        // can be performance killer
                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(selectLastExecution.field(lastExecutionTC)))
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.DATASET_ID, 0L)
                                        .eq(
                                                selectLastExecution.field(
                                                        lastExecutionDS))) // coalesce to allow join if no dataset
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON, new Timestamp(0L))
                                        .eq(
                                                selectLastExecution.field(
                                                        lastExecutionDate))) // coalesce to allow join if no execution date
                        .where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
                        .and(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.in(allStatusSet))
                        .groupBy(REQUIREMENT_VERSION.RES_ID)
                        .asTable("allITPI");

        Field<Long> reqIdMatch = REQUIREMENT_VERSION.RES_ID.as("reqIdMatch");
        Field<Integer> countMatch = count().as("countMatch");

        Table<Record2<Long, Integer>> matchITPI =
                dsl.select(reqIdMatch, countMatch)
                        .from(joinFromRequirementVersionToITPI)
                        .innerJoin(selectLastExecution)
                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(selectLastExecution.field(lastExecutionTC)))
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.DATASET_ID, 0L)
                                        .eq(selectLastExecution.field(lastExecutionDS)))
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON, new Timestamp(0L))
                                        .eq(selectLastExecution.field(lastExecutionDate)))
                        .where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
                        .and(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.in(matchingStatusSet))
                        .groupBy(REQUIREMENT_VERSION.RES_ID)
                        .asTable("matchITPI");

        // making our joins versus our "virtual tables" and fetch into stat bundle
        dsl.select(REQUIREMENT_VERSION.RES_ID, allITPI.field(countAll), matchITPI.field(countMatch))
                .from(REQUIREMENT_VERSION)
                .leftJoin(allITPI)
                .on(REQUIREMENT_VERSION.RES_ID.eq(allITPI.field(reqIdAll)))
                .leftJoin(matchITPI)
                .on(REQUIREMENT_VERSION.RES_ID.eq(matchITPI.field(reqIdMatch)))
                .where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
                .fetch()
                .forEach(
                        r -> {
                            Long reqId = r.get(REQUIREMENT_VERSION.RES_ID);
                            Integer countAllITPI = Optional.ofNullable(r.get(allITPI.field(countAll))).orElse(0);
                            Integer countValidatedITPI =
                                    Optional.ofNullable(r.get(matchITPI.field(countMatch))).orElse(0);

                            if (key.equals(VALIDATION_RATE_KEY)) {
                                if (reqId.equals(requirementVersionId)) {
                                    currentVersionStats.setValidatedTestCases(countValidatedITPI);
                                    currentVersionStats.setExecutedTestCase(countAllITPI);
                                } else {
                                    childrenStats.setValidatedTestCases(
                                            childrenStats.getValidatedTestCases() + countValidatedITPI);
                                    childrenStats.setExecutedTestCase(
                                            childrenStats.getExecutedTestCase() + countAllITPI);
                                }
                            } else {
                                if (reqId.equals(requirementVersionId)) {
                                    currentVersionStats.setVerifiedTestCase(countValidatedITPI);
                                    currentVersionStats.setPlannedTestCase(countAllITPI);
                                } else {
                                    childrenStats.setVerifiedTestCase(
                                            childrenStats.getVerifiedTestCase() + countValidatedITPI);
                                    childrenStats.setPlannedTestCase(
                                            childrenStats.getPlannedTestCase() + countAllITPI);
                                }
                            }
                        });
    }

    /*
     * Compute a ratio of ITPI status from a list of requirements.
     * The ratio is like :
     * nb of ITPI in matchingStatusSet / nb of ITPI in allStatusSet.
     * The chosen ITPI are the most recently executed if at least one ITPI has execution or all of them is no execution date...
     */
    private void computeItpiByStatusRate(
            String remoteSynchronisationKind,
            RequirementVersionBundleStat bundle,
            Set<ExecutionStatus> matchingStatusSet,
            Set<ExecutionStatus> allStatusSet,
            String key) {

        // preparing our join from RLN_RELATIONSHIP_CLOSURE to ITPI
        TableOnConditionStep<Record> joinFromAncestorToITPI =
                RLN_RELATIONSHIP_CLOSURE
                        .innerJoin(REQUIREMENT)
                        .on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                        .innerJoin(REQUIREMENT_SYNC_EXTENDER)
                        .on(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID.eq(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
                        .innerJoin(REMOTE_SYNCHRONISATION)
                        .on(
                                REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID.eq(
                                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID))
                        .innerJoin(REQUIREMENT_VERSION)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT.CURRENT_VERSION_ID))
                        .innerJoin(REQUIREMENT_VERSION_COVERAGE)
                        .on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                        .innerJoin(TEST_CASE)
                        .on(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(TEST_CASE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID));

        Field<Long> lastExecutionTC = ITERATION_TEST_PLAN_ITEM.TCLN_ID.as("lastExecutionTC");
        Field<Long> lastExecutionDS =
                coalesce(ITERATION_TEST_PLAN_ITEM.DATASET_ID, 0L)
                        .as("lastExecutionDS"); // coalesce null dataset to 0, so we can make a join
        Field<Timestamp> lastExecutionDate =
                coalesce(max(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON), new Timestamp(0L))
                        .as("lastExecutionDate"); // coalesce null execution date to now, so we can make a join

        // Here we perform a subrequest that will create a result like TCLN_ID, DATASET_ID,
        // MAX(LAST_EXECUTED_ON)
        // So we can use this to perform a join with ours other sub queries :
        // A join between TCLN_ID, DATASET_ID, LAST_EXECUTED_ON  -> TCLN_ID, DATASET_ID,
        // MAX(LAST_EXECUTED_ON)
        // So we have the last executed ITPI for every combination of TCLN_ID - DATASET_ID
        Table<Record3<Long, Long, Timestamp>> selectLastExecution =
                dsl.select(lastExecutionTC, lastExecutionDS, lastExecutionDate)
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .groupBy(ITERATION_TEST_PLAN_ITEM.TCLN_ID, ITERATION_TEST_PLAN_ITEM.DATASET_ID)
                        .asTable("selectLastExecution");

        Field<Long> reqIdAll = RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.as("reqIdAll");
        Field<Integer> countAll = count().as("countAll");

        Table<Record2<Long, Integer>> allITPI =
                dsl.select(reqIdAll, countAll)
                        .from(joinFromAncestorToITPI)
                        .innerJoin(selectLastExecution) // inner join to avoid correlated sub query like
                        // LAST_EXECUTED_ON = SELECT(MAX(LAST_EXECUTED_ON))... which
                        // can be performance killer
                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(selectLastExecution.field(lastExecutionTC)))
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.DATASET_ID, 0L)
                                        .eq(
                                                selectLastExecution.field(
                                                        lastExecutionDS))) // coalesce to allow join if no dataset
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON, new Timestamp(0L))
                                        .eq(
                                                selectLastExecution.field(
                                                        lastExecutionDate))) // coalesce to allow join if no execution date
                        .where(REMOTE_SYNCHRONISATION.KIND.eq(remoteSynchronisationKind))
                        .and(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.in(allStatusSet))
                        .groupBy(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID)
                        .asTable("allITPI");

        Field<Long> reqIdMatch = RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.as("reqIdMatch");
        Field<Integer> countMatch = count().as("countMatch");

        Table<Record2<Long, Integer>> matchITPI =
                dsl.select(reqIdMatch, countMatch)
                        .from(joinFromAncestorToITPI)
                        .innerJoin(selectLastExecution)
                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(selectLastExecution.field(lastExecutionTC)))
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.DATASET_ID, 0L)
                                        .eq(selectLastExecution.field(lastExecutionDS)))
                        .and(
                                coalesce(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON, new Timestamp(0L))
                                        .eq(selectLastExecution.field(lastExecutionDate)))
                        .where(REMOTE_SYNCHRONISATION.KIND.eq(remoteSynchronisationKind))
                        .and(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.in(matchingStatusSet))
                        .groupBy(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID)
                        .asTable("matchITPI");

        // making our joins versus our "virtual tables" and fetch into stat bundle
        dsl.select(REQUIREMENT.RLN_ID, allITPI.field(countAll), matchITPI.field(countMatch))
                .from(REQUIREMENT)
                .innerJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(REMOTE_SYNCHRONISATION)
                .on(
                        REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID.eq(
                                REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID))
                .leftJoin(allITPI)
                .on(REQUIREMENT.RLN_ID.eq(allITPI.field(reqIdAll)))
                .leftJoin(matchITPI)
                .on(REQUIREMENT.RLN_ID.eq(matchITPI.field(reqIdMatch)))
                .where(REMOTE_SYNCHRONISATION.KIND.eq(remoteSynchronisationKind))
                .fetch()
                .forEach(
                        r -> {
                            Long reqId = r.get(REQUIREMENT.RLN_ID);
                            Integer countAllITPI = r.get(allITPI.field(countAll));
                            Integer countValidatedITPI = r.get(matchITPI.field(countMatch));
                            bundle.computeRate(reqId, key, countAllITPI, countValidatedITPI);
                        });
    }

    /*
     * Compute the redaction rate for a list of requirements.
     * The redaction rate is defined by the ratio Covering Test Case / Covering Test Case with status UNDER_REVIEW or APPROVED, for all the hierarchy of the requirement.
     */
    private void computeRedactionRate(String kind, RequirementVersionBundleStat bundle) {

        // preparing our join from RLN_CLOSURE to TEST_CASE
        TableOnConditionStep<Record> joinFromAncestorToTestCase =
                RLN_RELATIONSHIP_CLOSURE
                        .innerJoin(REQUIREMENT)
                        .on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                        .innerJoin(REQUIREMENT_SYNC_EXTENDER)
                        .on(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID.eq(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
                        .innerJoin(REMOTE_SYNCHRONISATION)
                        .on(
                                REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID.eq(
                                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID))
                        .innerJoin(REQUIREMENT_VERSION)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT.CURRENT_VERSION_ID))
                        .innerJoin(REQUIREMENT_VERSION_COVERAGE)
                        .on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                        .innerJoin(TEST_CASE)
                        .on(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID));

        // creating a "virtual table" like ReqVersionId | CountCoverageTC to avoid correlated sub
        // queries
        Field<Long> reqIdAllTC = RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.as("reqIdAllTC");
        Field<Integer> countAllTC = count().as("countAllTC");
        Table<Record2<Long, Integer>> allTestCase =
                dsl.select(reqIdAllTC, countAllTC)
                        .from(joinFromAncestorToTestCase)
                        .where(REMOTE_SYNCHRONISATION.KIND.eq(kind))
                        .groupBy(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID)
                        .asTable("allTestCase");

        // creating a "virtual table" like ReqVersionId | CountCoverageWithGoodStatus TC to avoid
        // correlated sub queries
        Field<Long> reqIdValidatedTC = RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.as("reqIdValidatedTC");
        Field<Integer> countValidatedTC = count().as("countValidatedTC");
        Table<Record2<Long, Integer>> validatedTestCase =
                dsl.select(reqIdValidatedTC, countValidatedTC)
                        .from(joinFromAncestorToTestCase)
                        .where(REMOTE_SYNCHRONISATION.KIND.eq(kind))
                        .and(
                                TEST_CASE.TC_STATUS.in(
                                        TestCaseStatus.UNDER_REVIEW.name(), TestCaseStatus.APPROVED.name()))
                        .groupBy(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID)
                        .asTable("verifiedTestCase");

        // making our main request with joins versus our "virtual tables" and fetch into stat bundle
        dsl.select(
                        REQUIREMENT.RLN_ID,
                        allTestCase.field(countAllTC),
                        validatedTestCase.field(countValidatedTC))
                .from(REQUIREMENT)
                .innerJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(REMOTE_SYNCHRONISATION)
                .on(
                        REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID.eq(
                                REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID))
                .leftJoin(allTestCase)
                .on(
                        REQUIREMENT.RLN_ID.eq(
                                allTestCase.field(
                                        reqIdAllTC))) // performing left join to have tuple even if no coverages for
                // requirement
                .leftJoin(validatedTestCase)
                .on(REQUIREMENT.RLN_ID.eq(validatedTestCase.field(reqIdValidatedTC)))
                .where(REMOTE_SYNCHRONISATION.KIND.eq(kind))
                .fetch()
                .forEach(
                        r -> {
                            Long reqId = r.get(REQUIREMENT.RLN_ID);
                            Integer countAllTestCase = r.get(allTestCase.field(countAllTC));
                            Integer countValidatedTestCase = r.get(validatedTestCase.field(countValidatedTC));
                            bundle.computeRate(
                                    reqId, REDACTION_RATE_KEY, countAllTestCase, countValidatedTestCase);
                        });
    }

    // here, value is Object[] => (count, total)
    public static Map<String, Object[]> retrieveAllRequirementCoverageStatisticsIntoMap(
            String queryString,
            boolean isNativeQuery,
            String parameterName,
            Collection<Long> entityIds,
            EntityManager entityManager) {
        List<List<Object[]>> partitionsResults =
                retrieveAllStatisticsResultPartitions(
                        entityManager, entityIds, queryString, isNativeQuery, parameterName);

        Map<String, Object[]> statisticsMap = new HashMap<>();
        for (List<Object[]> partitionResults : partitionsResults) {
            Map<String, Object[]> partitionResultMap =
                    partitionResults.stream()
                            .collect(
                                    Collectors.toMap(
                                            tuple -> (String) tuple[0], tuple -> new Object[] {tuple[1], tuple[2]}));
            statisticsMap =
                    statisticsMap.isEmpty()
                            ? partitionResultMap
                            : mergeObjectValueMaps(statisticsMap, partitionResultMap);
        }
        return statisticsMap;
    }

    private static Map<String, Object[]> mergeObjectValueMaps(
            Map<String, Object[]> tuples, Map<String, Object[]> partitionResultMap) {
        for (Entry<String, Object[]> partitionResult : partitionResultMap.entrySet()) {
            if (tuples.containsKey(partitionResult.getKey())) {
                tuples.put(
                        partitionResult.getKey(),
                        new Object[] {
                            ((BigInteger) tuples.get(partitionResult.getKey())[0])
                                    .add((BigInteger) partitionResult.getValue()[0]),
                            ((BigInteger) tuples.get(partitionResult.getKey())[1])
                                    .add((BigInteger) partitionResult.getValue()[1])
                        });
            } else {
                tuples.put(partitionResult.getKey(), partitionResult.getValue());
            }
        }
        return tuples;
    }

    // here, key is an Object[] => (requirementCriticality, executionStatus)
    public static List<Object[]> retrieveAllRequirementValidationStatistics(
            String queryString,
            boolean isNativeQuery,
            String parameterName,
            Collection<Long> entityIds,
            EntityManager entityManager) {
        final List<List<Object[]>> partitionsResults =
                retrieveAllStatisticsResultPartitions(
                        entityManager, entityIds, queryString, isNativeQuery, parameterName);
        final List<Object[]> statistics = new ArrayList<>();

        for (List<Object[]> partitionResults : partitionsResults) {
            for (Object[] statToAdd : partitionResults) {
                final boolean isAlreadyInList = checkIfAlreadyInList(statistics, statToAdd);

                if (!isAlreadyInList) {
                    statistics.add(statToAdd);
                }
            }
        }

        return statistics;
    }

    private static boolean checkIfAlreadyInList(List<Object[]> statistics, Object[] statToAdd) {
        for (Object[] statistic : statistics) {
            final String requirementCriticality = (String) statistic[0];
            final String executionStatus = (String) statistic[1];

            final boolean criticalityIsEqual = requirementCriticality.equals(statToAdd[0]);
            final boolean bothStatusesAreNull = executionStatus == null && statToAdd[1] == null;
            final boolean statusIsEqual = executionStatus != null && executionStatus.equals(statToAdd[1]);

            if (criticalityIsEqual && (bothStatusesAreNull || statusIsEqual)) {
                statistic[2] = ((BigInteger) statistic[2]).add((BigInteger) statToAdd[2]);
                return true;
            }
        }

        return false;
    }

    private List<Long> getNonObsoleteDescendantIds(Long requirementVersionId) {
        Requirement requirement2 = REQUIREMENT.as("R2");
        RequirementVersion rv2 = REQUIREMENT_VERSION.as("RV2");

        SelectOnConditionStep<Record1<Long>> baseRequest =
                dsl.select(rv2.RES_ID)
                        .from(REQUIREMENT_VERSION)
                        .innerJoin(REQUIREMENT)
                        .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
                        .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                        .on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
                        .innerJoin(requirement2)
                        .on(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(requirement2.RLN_ID))
                        .innerJoin(rv2)
                        .on(requirement2.CURRENT_VERSION_ID.eq(rv2.RES_ID));
        List<Long> descendantResId;

        if (activeMilestoneHolder.getActiveMilestone().isPresent()) {
            Long activeMilestoneId = activeMilestoneHolder.getActiveMilestone().get().getId();
            descendantResId =
                    baseRequest
                            .innerJoin(MILESTONE_REQ_VERSION)
                            .on(rv2.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                            .where(
                                    REQUIREMENT_VERSION
                                            .RES_ID
                                            .eq(requirementVersionId)
                                            .and(RLN_RELATIONSHIP_CLOSURE.DEPTH.ne((short) 0))
                                            .and(rv2.REQUIREMENT_STATUS.ne("OBSOLETE"))
                                            .and(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(activeMilestoneId)))
                            .fetch(rv2.RES_ID);
        } else {
            descendantResId =
                    baseRequest
                            .where(
                                    REQUIREMENT_VERSION
                                            .RES_ID
                                            .eq(requirementVersionId)
                                            .and(RLN_RELATIONSHIP_CLOSURE.DEPTH.ne((short) 0))
                                            .and(rv2.REQUIREMENT_STATUS.ne("OBSOLETE")))
                            .fetch(rv2.RES_ID);
        }

        return descendantResId;
    }

    private List<Long> getHighLvlReqNonObsoleteDescendantIds(Long requirementVersionId) {
        RequirementVersion descendantRequirementVersion = REQUIREMENT_VERSION.as("DESC_RV");

        SelectOnConditionStep<Record1<Long>> baseRequest =
                dsl.selectDistinct(REQUIREMENT.CURRENT_VERSION_ID)
                        .from(HIGH_LEVEL_REQUIREMENT)
                        .innerJoin(REQUIREMENT)
                        .on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
                        .innerJoin(REQUIREMENT_VERSION)
                        .on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                        .innerJoin(descendantRequirementVersion)
                        .on(REQUIREMENT.CURRENT_VERSION_ID.eq(descendantRequirementVersion.RES_ID));

        List<Long> descendantResId;

        if (activeMilestoneHolder.getActiveMilestone().isPresent()) {
            Long activeMilestoneId = activeMilestoneHolder.getActiveMilestone().get().getId();
            descendantResId =
                    baseRequest
                            .innerJoin(MILESTONE_REQ_VERSION)
                            .on(descendantRequirementVersion.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                            .where(
                                    REQUIREMENT_VERSION
                                            .RES_ID
                                            .eq(requirementVersionId)
                                            .and(descendantRequirementVersion.REQUIREMENT_STATUS.ne("OBSOLETE"))
                                            .and(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(activeMilestoneId)))
                            .fetch(REQUIREMENT.CURRENT_VERSION_ID);
        } else {
            descendantResId =
                    baseRequest
                            .where(
                                    REQUIREMENT_VERSION
                                            .RES_ID
                                            .eq(requirementVersionId)
                                            .and(descendantRequirementVersion.REQUIREMENT_STATUS.ne("OBSOLETE")))
                            .fetch(REQUIREMENT.CURRENT_VERSION_ID);
        }

        return descendantResId;
    }
}
