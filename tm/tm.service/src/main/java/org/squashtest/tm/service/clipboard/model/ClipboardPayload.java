/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.clipboard.model;

import java.util.List;
import org.squashtest.tm.domain.NodeReference;

/**
 * When trying to paste the same element (and its children) several times in a row, we want to keep
 * the state of the element when it was copied. The white list refers to the content of this element
 * at the time it was copied.
 */
public class ClipboardPayload {
    private List<NodeReference> selectedNode;

    private List<Long> selectedNodeIds;

    private List<Long> whiteListNodeIds;

    private boolean canWhiteListBeIgnored;

    public static ClipboardPayload withWhiteListIgnored(List<Long> selectedNodeIds) {
        ClipboardPayload clipboardPayload = new ClipboardPayload();
        clipboardPayload.setWhiteListShouldBeIgnored(true);
        clipboardPayload.setSelectedNodeIds(selectedNodeIds);
        return clipboardPayload;
    }

    public List<NodeReference> getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(List<NodeReference> selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Long> getSelectedNodeIds() {
        return selectedNodeIds;
    }

    public void setSelectedNodeIds(List<Long> selectedNodeIds) {
        this.selectedNodeIds = selectedNodeIds;
    }

    public List<Long> getWhiteListNodeIds() {
        return whiteListNodeIds;
    }

    public void setWhiteListNodeIds(List<Long> whiteListNodeIds) {
        this.whiteListNodeIds = whiteListNodeIds;
    }

    public boolean shouldWhiteListBeIgnored() {
        return canWhiteListBeIgnored;
    }

    public void setWhiteListShouldBeIgnored(boolean canWhiteListBeIgnored) {
        this.canWhiteListBeIgnored = canWhiteListBeIgnored;
    }
}
