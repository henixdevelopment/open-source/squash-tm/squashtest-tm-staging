/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.copier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import javax.persistence.EntityManager;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.library.Copiable;
import org.squashtest.tm.domain.library.Folder;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.domain.library.NodeVisitor;
import org.squashtest.tm.domain.library.TreeNode;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneHolder;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.internal.library.LibraryUtils;

public class NodeCopierVisitor implements NodeVisitor {
    private static final String UNCHECKED = "unchecked";

    private final EntityManager entityManager;
    private final AttachmentManagerService attachmentManagerService;

    private NodeContainer<? extends TreeNode> destination;
    private final String copySuffix;
    private final Map<String, Map<Long, Long>> pairingIdsSourceCopyNode;

    private final List<BoundEntityCopy> boundEntityCopies = new ArrayList<>();
    private final List<BoundEntityCopy> boundEntityCopiesProjectChanged = new ArrayList<>();
    private final List<AttachmentHolder> attachmentHolders = new ArrayList<>();

    private boolean projectChanged = false;

    public NodeCopierVisitor(
            EntityManager entityManager,
            AttachmentManagerService attachmentManagerService,
            NodeContainer<? extends TreeNode> destination,
            String copySuffix,
            Map<String, Map<Long, Long>> pairingIdsSourceCopyNode) {
        this.entityManager = entityManager;
        this.attachmentManagerService = attachmentManagerService;
        this.destination = destination;
        this.copySuffix = copySuffix;
        this.pairingIdsSourceCopyNode = pairingIdsSourceCopyNode;
    }

    public void setDestination(NodeContainer<? extends TreeNode> destination) {
        this.destination = destination;
    }

    public void setProjectChanged(boolean projectChanged) {
        this.projectChanged = projectChanged;
    }

    public List<BoundEntityCopy> getBoundEntityCopies() {
        return boundEntityCopies;
    }

    public List<BoundEntityCopy> getBoundEntityCopiesProjectChanged() {
        return boundEntityCopiesProjectChanged;
    }

    public List<AttachmentHolder> getAttachmentHolders() {
        return attachmentHolders;
    }

    public void performCopy(TreeNode node) {
        node.accept(this);
    }

    @Override
    public void visit(Campaign source) {
        Campaign copy = copyNode(source, destination);
        addCustomField(source, copy);
        attachmentHolders.add(copy);
        updateRichTextAttachmentUrls(
                source.getAttachmentList(),
                copy.getAttachmentList(),
                source.getDescription(),
                copy::setDescription);
        migrateMilestonesIfProjectChanged(copy);
        entityManager.persist(copy);
        this.pairingIdsSourceCopyNode
                .computeIfAbsent(source.getClass().getSimpleName(), k -> new HashMap<>())
                .put(source.getId(), copy.getId());
    }

    @Override
    public void visit(Iteration source) {
        Iteration copy = copyNode(source, destination);
        addCustomFieldIteration(source, copy);
        attachmentHolders.add(copy);
        updateRichTextAttachmentUrls(
                source.getAttachmentList(),
                copy.getAttachmentList(),
                source.getDescription(),
                copy::setDescription);
        entityManager.persist(copy);
    }

    @Override
    public void visit(TestSuite source) {
        TestSuite copy = copyNode(source, destination);
        addCustomField(source, copy);
        attachmentHolders.add(copy);
        updateRichTextAttachmentUrls(
                source.getAttachmentList(),
                copy.getAttachmentList(),
                source.getDescription(),
                copy::setDescription);
        addTestPlanToTestSuiteCopy((Iteration) destination, source, copy);
        entityManager.persist(copy);
    }

    private void addTestPlanToTestSuiteCopy(Iteration iteration, TestSuite source, TestSuite copy) {
        List<IterationTestPlanItem> copyOfTestPlan = source.createPastableCopyOfTestPlan();
        copyOfTestPlan.forEach(iteration::addTestPlan);
        copy.bindTestPlanItems(copyOfTestPlan, iteration);
    }

    @Override
    public void visit(Sprint source) {
        Sprint copy = copyNode(source, destination);
        addCustomField(source, copy);
        attachmentHolders.add(copy);
        updateRichTextAttachmentUrls(
                source.getAttachmentList(),
                copy.getAttachmentList(),
                source.getDescription(),
                copy::setDescription);
        if (projectChanged) {
            copy.migrateTestPlans();
        }
        entityManager.persist(copy);
    }

    @Override
    public void visit(SprintGroup source) {
        SprintGroup copy = copyNode(source, destination);
        addCustomField(source, copy);
        attachmentHolders.add(copy);
        updateRichTextAttachmentUrls(
                source.getAttachmentList(),
                copy.getAttachmentList(),
                source.getDescription(),
                copy::setDescription);
        entityManager.persist(copy);
        this.pairingIdsSourceCopyNode
                .computeIfAbsent(source.getClass().getSimpleName(), k -> new HashMap<>())
                .put(source.getId(), copy.getId());
    }

    @Override
    public void visit(Requirement source) {
        throw new UnsupportedOperationException("Copy of Requirement is not supported");
    }

    @Override
    public void visit(HighLevelRequirement source) {
        throw new UnsupportedOperationException("Copy of HighLevelRequirement is not supported");
    }

    @Override
    public void visit(TestCase source) {
        throw new UnsupportedOperationException("Copy of TestCase is not supported");
    }

    public <ENTITY extends LibraryNode> void visitFolder(Folder<ENTITY> source) {
        Folder<ENTITY> copy = copyNode(source, destination);
        addCustomField(source, copy);
        attachmentHolders.add(copy);
        updateRichTextAttachmentUrls(
                source.getAttachmentList(),
                copy.getAttachmentList(),
                source.getDescription(),
                copy::setDescription);
        entityManager.persist(copy);
        this.pairingIdsSourceCopyNode
                .computeIfAbsent(source.getClass().getSimpleName(), k -> new HashMap<>())
                .put(source.getId(), copy.getId());
    }

    @Override
    public void visit(CampaignFolder source) {
        visitFolder(source);
    }

    @Override
    public void visit(RequirementFolder source) {
        visitFolder(source);
    }

    @Override
    public void visit(TestCaseFolder source) {
        visitFolder(source);
    }

    @SuppressWarnings(UNCHECKED)
    private <ENTITY extends TreeNode & Copiable> ENTITY copyNode(
            ENTITY source, NodeContainer<? extends TreeNode> destination) {
        NodeContainer<ENTITY> container = (NodeContainer<ENTITY>) destination;
        ENTITY copyNode = (ENTITY) source.createCopy();
        renameIfNeeded(copyNode, container);
        container.addContentWithoutSet(copyNode);
        return copyNode;
    }

    protected <ENTITY extends TreeNode & Copiable> void renameIfNeeded(
            ENTITY copy, NodeContainer<ENTITY> destination) {
        String name = copy.getName();
        if (destination.isContentNameAvailable(name)) {
            return;
        }
        String newName =
                LibraryUtils.generateUniqueCopyName(
                        destination.getContentNames(), name, Sizes.NAME_MAX, copySuffix);

        copy.setName(newName);
    }

    private void addCustomField(BoundEntity source, BoundEntity copy) {
        if (projectChanged) {
            boundEntityCopiesProjectChanged.add(new BoundEntityCopy(source.getId(), copy));
        } else {
            boundEntityCopies.add(new BoundEntityCopy(source.getId(), copy));
        }
    }

    private void addCustomFieldIteration(Iteration source, Iteration copy) {
        addCustomField(source, copy);
        copy.getTestSuites()
                .forEach(
                        testSuiteCopy -> {
                            TestSuite testSuiteSource = source.getTestSuiteByName(testSuiteCopy.getName());
                            addCustomField(testSuiteSource, testSuiteCopy);
                        });
    }

    protected void updateRichTextAttachmentUrls(
            AttachmentList sourceList,
            AttachmentList copyList,
            String richText,
            Consumer<String> consumer) {
        if (richText == null || richText.isBlank()) {
            return;
        }

        String updatedDescription =
                attachmentManagerService.updateRichTextUrlsOnEntityCopy(sourceList, copyList, richText);

        if (!richText.equals(updatedDescription)) {
            consumer.accept(updatedDescription);
        }
    }

    private void migrateMilestonesIfProjectChanged(MilestoneHolder member) {
        if (projectChanged) {
            List<Milestone> milestonesProject = this.destination.getProject().getMilestones();
            member.getMilestones().removeIf(m -> !milestonesProject.contains(m));
        }
    }
}
