/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.crypto.SecretKey;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.users.ApiToken;
import org.squashtest.tm.domain.users.ApiTokenPermission;
import org.squashtest.tm.service.RestApiJwtSecretKeyManager;
import org.squashtest.tm.service.internal.repository.ApiTokenDao;
import org.squashtest.tm.service.jwt.JwtTokenService;

@Service
public class JwtTokenServiceImpl implements JwtTokenService {

    public static final String INVALID_TOKEN = "Invalid token";

    @Inject private ApiTokenDao apiTokenDao;

    @Override
    public String generateJwt(
            String userId,
            String uuid,
            Date expiryDate,
            Date createdOn,
            String permission,
            String jwtSecret) {

        SecretKey key = RestApiJwtSecretKeyManager.getRestApiJwtSecretKey(jwtSecret);

        return Jwts.builder()
                .subject(userId)
                .claim("uuid", uuid)
                .claim("permissions", permission)
                .issuedAt(createdOn)
                .expiration(expiryDate)
                .signWith(key, Jwts.SIG.HS512)
                .compact();
    }

    @Override
    public String getUserIdFromToken(String token, String jwtSecret) {
        SecretKey key = RestApiJwtSecretKeyManager.getRestApiJwtSecretKey(jwtSecret);
        return Jwts.parser().verifyWith(key).build().parseSignedClaims(token).getPayload().getSubject();
    }

    @Override
    public boolean validateApiToken(
            List<String> permissionsExemptions,
            String token,
            String jwtSecret,
            HttpServletRequest request) {

        SecretKey key = RestApiJwtSecretKeyManager.getRestApiJwtSecretKey(jwtSecret);

        try {
            Claims payload = Jwts.parser().verifyWith(key).build().parseSignedClaims(token).getPayload();

            String jwtPermissions = payload.get("permissions", String.class);
            String jwtUuid = payload.get("uuid", String.class);
            String jwtUserId = payload.getSubject();

            if (jwtPermissions == null || jwtUuid == null || jwtUserId == null) {
                throw new BadCredentialsException(INVALID_TOKEN);
            }

            Optional<ApiToken> potentialApiToken = apiTokenDao.findByUuid(jwtUuid);
            ApiToken squashApiToken = throwNotExistOrSetLastUsage(potentialApiToken);

            if (!isTokenValid(squashApiToken, jwtPermissions, payload.getExpiration(), jwtUserId)) {
                throw new BadCredentialsException(INVALID_TOKEN);
            }

            boolean shouldByPassPermission =
                    permissionsExemptions.stream()
                            .anyMatch(endpoint -> request.getServletPath().endsWith(endpoint));
            boolean isMethodAllowed =
                    shouldByPassPermission
                            || ApiTokenPermission.valueOf(jwtPermissions)
                                    .getAllowedMethods()
                                    .contains(request.getMethod());

            if (!isMethodAllowed) {
                throw new BadCredentialsException(INVALID_TOKEN);
            }

            return true;
        } catch (JwtException | IllegalArgumentException ex) {
            throw new BadCredentialsException(INVALID_TOKEN, ex);
        }
    }

    private ApiToken throwNotExistOrSetLastUsage(Optional<ApiToken> potentialApiToken) {
        if (potentialApiToken.isEmpty()) {
            throw new BadCredentialsException(INVALID_TOKEN);
        } else {
            ApiToken squashApiToken = potentialApiToken.get();
            squashApiToken.setLastUsage(new Date());
            apiTokenDao.saveAndFlush(squashApiToken);
            return squashApiToken;
        }
    }

    private static boolean isTokenValid(
            ApiToken apiToken, String jwtPermissions, Date jwtExpiryDate, String jwtUserId) {

        Instant instant = jwtExpiryDate.toInstant();
        LocalDateTime jwtExpiryDateUtc = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        String jwtExpiryDateUtcString = jwtExpiryDateUtc.format(DateTimeFormatter.ISO_DATE);

        return Objects.equals(apiToken.getPermissions(), jwtPermissions)
                && Objects.equals(apiToken.getExpiryDate(), jwtExpiryDateUtcString)
                && Objects.equals(apiToken.getUser().getId().toString(), jwtUserId)
                && new Date().before(jwtExpiryDate);
    }
}
