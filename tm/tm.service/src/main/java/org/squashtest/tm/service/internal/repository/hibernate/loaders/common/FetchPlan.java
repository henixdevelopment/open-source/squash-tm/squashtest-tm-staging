/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders.common;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.TypedQuery;
import org.squashtest.tm.service.internal.repository.ParameterNames;

public class FetchPlan<KEY, TYPE> extends AbstractHqlLoader<KEY, TYPE> {
    private final Set<JoinOption<KEY, TYPE>> joinOptions;

    protected final Set<KEY> ids;

    public FetchPlan(
            HintOptions hintOptions,
            Set<JoinOption<KEY, TYPE>> joinOptions,
            TypedQuery<TYPE> mainQuery,
            Set<KEY> ids) {
        super(hintOptions, mainQuery);
        this.joinOptions = joinOptions;
        this.ids = Objects.requireNonNull(ids);
    }

    public List<TYPE> fetch() {
        if (ids.isEmpty()) {
            return new ArrayList<>();
        }
        List<TYPE> mainAggregates = fetchMainEntity();
        joinOptions.forEach(joinOption -> joinOption.fetch(new HashSet<>(ids)));
        return mainAggregates;
    }

    private List<TYPE> fetchMainEntity() {
        TypedQuery<TYPE> mainQuery = build();
        mainQuery.setParameter(ParameterNames.IDS, ids);
        return mainQuery.getResultList();
    }
}
