/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.service.internal.repository.EntityDao;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.SprintGroupDao;

@Repository("squashtest.tm.repository.CampaignLibraryNodeDao")
public class HibernateCampaignLibraryNodeDao extends HibernateEntityDao<CampaignLibraryNode>
        implements LibraryNodeDao<CampaignLibraryNode>, EntityDao<CampaignLibraryNode> {

    private static final String ID = "ID";
    private static final String PATH = "PATH";
    private static final String PATH_CTE = "PATH_CTE";

    @Inject private DSLContext dslContext;

    @Inject private SprintGroupDao sprintGroupDao;

    @Inject private ProjectDao projectDao;

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getParentsName(long entityId) {
        NativeQuery query =
                currentSession().createNativeQuery(NativeQueries.CLN_FIND_SORTED_PARENT_NAMES);
        query.setParameter(ParameterNames.NODE_ID, entityId, LongType.INSTANCE);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getParentsIds(long entityId) {
        NativeQuery query =
                currentSession().createNativeQuery(NativeQueries.CLN_FIND_SORTED_PARENT_IDS);
        query.setResultTransformer(new SqLIdResultTransformer());
        query.setParameter(ParameterNames.NODE_ID, entityId, LongType.INSTANCE);
        return query.list();
    }

    @Override
    public List<Long> findNodeIdsByPath(List<String> paths) {
        String fullPath = paths.get(paths.size() - 1);
        List<String> splits = Arrays.asList(PathUtils.splitPath(fullPath));
        List<String> effectiveSplits = PathUtils.unescapeSlashes(splits);

        if (effectiveSplits.size() < 2) {
            return new ArrayList<>();
        }

        Long projectId = projectDao.findIdByName(effectiveSplits.get(0));
        String rootFolderName = effectiveSplits.get(1);

        if (effectiveSplits.size() == 2) {
            Long existingId = existingNodeIdOrNull(projectId, rootFolderName);
            return Objects.isNull(existingId) ? new ArrayList<>() : Collections.singletonList(existingId);
        }

        String fullPathWithoutProjectName = PathUtils.getPathWithoutProject(effectiveSplits);

        return getChildren(projectId, rootFolderName, fullPathWithoutProjectName);
    }

    @Override
    public Long findNodeIdByPath(String path) {
        List<String> effectiveSplits = PathUtils.getSplitPathWithoutProjectNameFromAPath(path);

        if (effectiveSplits.size() < 2) {
            return null;
        }

        if (effectiveSplits.size() == 2) {
            Long projectId = projectDao.findIdByName(effectiveSplits.get(0));
            return existingNodeIdOrNull(projectId, effectiveSplits.get(1));
        }

        List<Long> children = findNodeIdsByPath(Collections.singletonList(path));
        return effectiveSplits.size() == children.size() ? children.get(children.size() - 1) : null;
    }

    private Long existingNodeIdOrNull(Long projectId, String rootFolderName) {
        return sprintGroupDao.findRootNodeIdByProjectIdAndName(projectId, rootFolderName);
    }

    private List<Long> getChildren(
            Long projectId, String rootFolderName, String fullPathWithoutProjectName) {
        Field<String> exactPathToCheckInCte =
                concat(field(name(PATH_CTE, PATH)), val("/"), CAMPAIGN_LIBRARY_NODE.NAME);
        Field<String> longerPathToCheckInCte = concat(exactPathToCheckInCte, val("/%"));

        CommonTableExpression<Record2<Long, String>> cte =
                name(PATH_CTE)
                        .fields(ID, PATH)
                        .as(
                                dslContext
                                        .select(
                                                CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN_LIBRARY_NODE.NAME.cast(String.class))
                                        .from(PROJECT)
                                        .join(CAMPAIGN_LIBRARY_NODE)
                                        .on(PROJECT.PROJECT_ID.eq(CAMPAIGN_LIBRARY_NODE.PROJECT_ID))
                                        .join(CAMPAIGN_LIBRARY_CONTENT)
                                        .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID))
                                        .where(PROJECT.PROJECT_ID.eq(projectId))
                                        .and(CAMPAIGN_LIBRARY_NODE.NAME.eq(rootFolderName))
                                        .union(
                                                select(
                                                                CLN_RELATIONSHIP.DESCENDANT_ID,
                                                                field(name(PATH_CTE, PATH))
                                                                        .concat("/")
                                                                        .concat(CAMPAIGN_LIBRARY_NODE.NAME)
                                                                        .cast(String.class))
                                                        .from(name(PATH_CTE))
                                                        .join(CLN_RELATIONSHIP)
                                                        .on(field(name(PATH_CTE, ID)).eq(CLN_RELATIONSHIP.ANCESTOR_ID))
                                                        .join(CAMPAIGN_LIBRARY_NODE)
                                                        .on(CLN_RELATIONSHIP.DESCENDANT_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                                        .where(val(fullPathWithoutProjectName).like(exactPathToCheckInCte))
                                                        .or(val(fullPathWithoutProjectName).like(longerPathToCheckInCte))));
        return dslContext.withRecursive(cte).select(cte.field(ID)).from(cte).fetchInto(Long.class);
    }
}
