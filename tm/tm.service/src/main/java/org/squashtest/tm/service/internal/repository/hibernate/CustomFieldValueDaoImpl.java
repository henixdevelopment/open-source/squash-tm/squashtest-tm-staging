/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.groupConcat;
import static org.squashtest.tm.domain.customreport.CustomExportColumnLabel.getSelectableEntityTypeToIdTableFieldMap;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.BINDING;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_CUF_VALUES_BY_ENTITY_AND_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ENTITY;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ENTITY_IDS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.SelectJoinStep;
import org.jooq.SelectOrderByStep;
import org.jooq.SelectSelectStep;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customreport.CustomExportColumnLabel;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomFieldCustomExportDto;
import org.squashtest.tm.service.internal.dto.NumericCufHelper;
import org.squashtest.tm.service.internal.repository.CustomCustomFieldValueDao;
import org.squashtest.tm.service.internal.repository.EntityGraphName;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;

public class CustomFieldValueDaoImpl implements CustomCustomFieldValueDao {

    private static final String CUF_ID = "cufId";
    private static final String BINDABLE_ENTITY = "bindableEntity";

    private static final Field CUSTOM_FIELD_VALUE_TAG_VALUE =
            groupConcat(CUSTOM_FIELD_VALUE_OPTION.LABEL).separator(", ");

    private static final Field DENORMALIZED_FIELD_VALUE_TAG_VALUE =
            groupConcat(DENORMALIZED_FIELD_VALUE_OPTION.LABEL).separator(", ");

    private static final Field CUSTOM_FIELD_VALUE_COMPUTED =
            DSL.when(CUSTOM_FIELD_VALUE.FIELD_TYPE.eq("CF"), CUSTOM_FIELD_VALUE.VALUE)
                    .when(CUSTOM_FIELD_VALUE.FIELD_TYPE.eq("RTF"), CUSTOM_FIELD_VALUE.LARGE_VALUE)
                    .when(CUSTOM_FIELD_VALUE.FIELD_TYPE.eq("NUM"), CUSTOM_FIELD_VALUE.VALUE)
                    .when(CUSTOM_FIELD_VALUE.FIELD_TYPE.eq("TAG"), CUSTOM_FIELD_VALUE_TAG_VALUE)
                    .as("COMPUTED_VALUE");

    private static final Field DENORMALIZED_FIELD_VALUE_COMPUTED =
            DSL.when(DENORMALIZED_FIELD_VALUE.FIELD_TYPE.eq("CF"), DENORMALIZED_FIELD_VALUE.VALUE)
                    .when(DENORMALIZED_FIELD_VALUE.FIELD_TYPE.eq("SSF"), DENORMALIZED_FIELD_VALUE.VALUE)
                    .when(DENORMALIZED_FIELD_VALUE.FIELD_TYPE.eq("RTF"), DENORMALIZED_FIELD_VALUE.LARGE_VALUE)
                    .when(DENORMALIZED_FIELD_VALUE.FIELD_TYPE.eq("NUM"), DENORMALIZED_FIELD_VALUE.VALUE)
                    .when(DENORMALIZED_FIELD_VALUE.FIELD_TYPE.eq("MFV"), DENORMALIZED_FIELD_VALUE_TAG_VALUE)
                    .as("COMPUTED_VALUE");

    @Inject private DSLContext dsl;

    @PersistenceContext private EntityManager entityManager;

    @Override
    public Map<EntityReference, Map<Long, Object>> getCufValuesMapByEntityReference(
            List<EntityReference> scopeEntities, Map<EntityType, List<Long>> entityTypeToCufIdsListMap) {

        // Fetch all the Entities involved in the Campaign which cuf were requested
        Set<EntityReference> allRequestedEntitiesInCampaign =
                getEntityReferencesFromCampaign(scopeEntities, entityTypeToCufIdsListMap.keySet());

        Iterator<Record> standardCufQueryResult;

        if (allRequestedEntitiesInCampaign.isEmpty()) {
            return null;
        }

        if (entityTypeToCufIdsListMap.keySet().size() == 1
                && entityTypeToCufIdsListMap.containsKey(EntityType.TEST_STEP)) {
            // If only TEST_STEP cufs were requested, no need to query the Standard Cufs
            standardCufQueryResult = Collections.emptyIterator();
        } else {
            // Build Standard Cuf Query
            SelectOrderByStep query =
                    buildStandardCufQuery(entityTypeToCufIdsListMap, allRequestedEntitiesInCampaign);
            // Execute the Standard Cuf Query
            standardCufQueryResult = query.fetch().iterator();
        }

        Iterator<Record> denormalizedCufQueryResult;

        Set<Long> testStepIdsInScope =
                allRequestedEntitiesInCampaign.stream()
                        .filter(ref -> EntityType.TEST_STEP.equals(ref.getType()))
                        .map(EntityReference::getId)
                        .collect(Collectors.toSet());

        if (entityTypeToCufIdsListMap.containsKey(EntityType.TEST_STEP)
                && !testStepIdsInScope.isEmpty()) {
            // Build Denormalized Cuf Query
            SelectOrderByStep denormQuery =
                    buildDenormalizedCufQuery(entityTypeToCufIdsListMap, testStepIdsInScope);
            // Execute the Denormalized Cuf Query
            denormalizedCufQueryResult = denormQuery.fetch().iterator();
        } else {
            // Just create an empty iterator
            denormalizedCufQueryResult = Collections.emptyIterator();
        }

        // Build the resulting map
        return buildResultMapFromQueryResult(standardCufQueryResult, denormalizedCufQueryResult);
    }

    /**
     * Retrieves custom fields present in projects within the scope as well as test cases (and test
     * steps) present in test plans linked to projects outside the scope. First search for custom
     * fields linked to projects in the scope. The first union searches for custom fields present in
     * test cases and test steps linked to the itpi of projects in the scope. The second union
     * searches for custom fields in test cases and test steps linked to the itpi of projects in the
     * scope.
     *
     * @param projectIds project ids present in the scope
     * @return list of custom fields export dto
     */
    @Override
    public List<CustomFieldCustomExportDto> findAllAvailableForCustomExportByProjectIds(
            List<Long> projectIds) {
        return dsl.selectDistinct(
                        CUSTOM_FIELD.LABEL,
                        CUSTOM_FIELD.CF_ID.as(CUF_ID),
                        CUSTOM_FIELD_BINDING.BOUND_ENTITY.as(BINDABLE_ENTITY))
                .from(CUSTOM_FIELD)
                .join(CUSTOM_FIELD_BINDING)
                .on(CUSTOM_FIELD.CF_ID.eq(CUSTOM_FIELD_BINDING.CF_ID))
                .where(CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.in(projectIds))
                .or(
                        CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.in(
                                dsl.select(TEST_CASE_LIBRARY_NODE.PROJECT_ID)
                                        .from(CAMPAIGN_LIBRARY_NODE)
                                        .join(CAMPAIGN_ITERATION)
                                        .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
                                        .join(ITEM_TEST_PLAN_LIST)
                                        .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                                        .join(ITERATION_TEST_PLAN_ITEM)
                                        .on(
                                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                        .join(TEST_CASE_LIBRARY_NODE)
                                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                                        .where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))))
                .or(
                        CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.in(
                                dsl.select(TEST_CASE_LIBRARY_NODE.PROJECT_ID)
                                        .from(CAMPAIGN_LIBRARY_NODE)
                                        .join(CAMPAIGN_TEST_PLAN_ITEM)
                                        .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID))
                                        .join(TEST_CASE_LIBRARY_NODE)
                                        .on(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                                        .where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))))
                .fetchInto(CustomFieldCustomExportDto.class);
    }

    @Override
    public Map<Long, List<CustomFieldValue>> getCufValuesMapByBoundEntity(
            BindableEntity entity, List<Long> entityIds) {
        try (Stream<CustomFieldValue> resultStream =
                getCufValuesByEntityAndIdsStream(entity, entityIds)) {
            return resultStream.collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
        }
    }

    private Stream<CustomFieldValue> getCufValuesByEntityAndIdsStream(
            BindableEntity entity, List<Long> entityIds) {
        return new EntityGraphQueryBuilder<>(
                        entityManager, CustomFieldValue.class, FIND_CUF_VALUES_BY_ENTITY_AND_IDS)
                .addAttributeNodes(BINDING)
                .addSubGraph(BINDING, EntityGraphName.CUSTOM_FIELD)
                .executeStream(Map.of(ENTITY, entity, ENTITY_IDS, entityIds));
    }

    /**
     * Exploit the Cuf Queries Results to build the Map of all CustomFieldValues by EntityReference.
     *
     * @param standardCufQueryResult The Results of the Standard Cuf Query
     * @param denormalizedCufQueryResult The Results of the Denormalized Cuf Query
     * @return The Map of all CustomFieldValues by EntityReferences
     */
    private Map<EntityReference, Map<Long, Object>> buildResultMapFromQueryResult(
            Iterator<Record> standardCufQueryResult, Iterator<Record> denormalizedCufQueryResult) {
        Map<EntityReference, Map<Long, Object>> resultMap = new HashMap<>();
        standardCufQueryResult.forEachRemaining(
                record -> {
                    // Create the EntityReference
                    EntityReference entity =
                            new EntityReference(
                                    EntityType.valueOf(record.get(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE)),
                                    record.get(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID));
                    // Get the value
                    Object cufValue = record.get(CUSTOM_FIELD_VALUE_COMPUTED);
                    if ("NUM".equals(record.get(CUSTOM_FIELD_VALUE.FIELD_TYPE))) {
                        cufValue = NumericCufHelper.formatOutputNumericCufValue(String.valueOf(cufValue));
                    }
                    // Get the Cuf id
                    Long cufId = record.get(CUSTOM_FIELD_VALUE.CF_ID);
                    // populate the Map
                    populateCustomFieldValuesMap(entity, cufId, cufValue, resultMap);
                });
        denormalizedCufQueryResult.forEachRemaining(
                record -> {
                    // Create the EntityReference with denormalized Cufs
                    EntityReference entity =
                            new EntityReference(
                                    EntityType.TEST_STEP,
                                    record.get(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID));
                    // Get the value
                    Object cufValue = record.get(DENORMALIZED_FIELD_VALUE_COMPUTED);
                    if ("NUM".equals(record.get(DENORMALIZED_FIELD_VALUE.FIELD_TYPE))) {
                        cufValue = NumericCufHelper.formatOutputNumericCufValue(String.valueOf(cufValue));
                    }
                    // Get the Cuf id
                    Long cufId = record.get(CUSTOM_FIELD_VALUE.CF_ID);
                    // populate the Map
                    populateCustomFieldValuesMap(entity, cufId, cufValue, resultMap);
                });
        return resultMap;
    }

    /**
     * Given an EntityReference, the Id of the Cuf and the Value of the Cuf, populate the result Map
     * of the CustomFields.
     *
     * @param entityReference The EntityReference
     * @param cufValue The Cuf Value
     * @param resultMap The Result Map to populate
     */
    private void populateCustomFieldValuesMap(
            EntityReference entityReference,
            Long cufId,
            Object cufValue,
            Map<EntityReference, Map<Long, Object>> resultMap) {

        Map<Long, Object> currentEntityMap = resultMap.get(entityReference);
        if (currentEntityMap == null) {
            resultMap.put(entityReference, new HashMap<>());
            currentEntityMap = resultMap.get(entityReference);
        }
        currentEntityMap.put(cufId, cufValue);
    }

    /**
     * Build the Query to retrieve all requested Standard CustomFieldValues of all requested Entities.
     *
     * @param entityTypeToCufIdsListMap The Map giving the List of CustomField ids to retrieve by
     *     EntityType
     * @param allRequestedEntitiesInCampaign The Set of all the EntityReferences which
     *     CustomFieldValues are to retrieve
     * @return The Query to retrieve all requested Standard CustomFieldValues of all requested
     *     Entities
     */
    private SelectSelectStep buildStandardCufQuery(
            Map<EntityType, List<Long>> entityTypeToCufIdsListMap,
            Set<EntityReference> allRequestedEntitiesInCampaign) {

        SelectSelectStep query1 =
                dsl.select(
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE,
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID,
                        CUSTOM_FIELD_VALUE.CF_ID,
                        CUSTOM_FIELD_VALUE.FIELD_TYPE,
                        CUSTOM_FIELD_VALUE_COMPUTED);

        query1
                .from(CUSTOM_FIELD_VALUE)
                .leftJoin(CUSTOM_FIELD_VALUE_OPTION)
                .on(CUSTOM_FIELD_VALUE.CFV_ID.eq(CUSTOM_FIELD_VALUE_OPTION.CFV_ID));

        query1.where(
                buildWhereConditionOfCufQuery(entityTypeToCufIdsListMap, allRequestedEntitiesInCampaign));

        query1.groupBy(CUSTOM_FIELD_VALUE.CFV_ID);

        return query1;
    }

    /**
     * Build the Query to retrieve all requested Denormalized CustomFieldValues of all requested
     * Entities (but only the TestSteps will be relevant here).
     *
     * @param entityTypeToCufIdsListMap The Map giving the List of CustomField ids to retrieve by
     *     EntityType
     * @param testStepIdsInScope The Set of TestStep ids in scope
     * @return The Query to retrieve all requested Denormalized CustomFieldValues of all requested
     *     Entities.
     */
    private SelectSelectStep buildDenormalizedCufQuery(
            Map<EntityType, List<Long>> entityTypeToCufIdsListMap, Set<Long> testStepIdsInScope) {

        SelectSelectStep query2 =
                dsl.select(
                        DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE,
                        DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID,
                        DENORMALIZED_FIELD_VALUE.FIELD_TYPE,
                        CUSTOM_FIELD_VALUE.CF_ID,
                        DENORMALIZED_FIELD_VALUE_COMPUTED);
        query2
                .from(DENORMALIZED_FIELD_VALUE)
                .leftJoin(DENORMALIZED_FIELD_VALUE_OPTION)
                .on(DENORMALIZED_FIELD_VALUE_OPTION.DFV_ID.eq(DENORMALIZED_FIELD_VALUE.DFV_ID))
                .innerJoin(CUSTOM_FIELD_VALUE)
                .on(DENORMALIZED_FIELD_VALUE.CFV_ID.eq(CUSTOM_FIELD_VALUE.CFV_ID))
                .innerJoin(EXECUTION_STEP)
                .on(
                        DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID.eq(
                                EXECUTION_STEP.EXECUTION_STEP_ID));

        Condition condition =
                DENORMALIZED_FIELD_VALUE
                        .DENORMALIZED_FIELD_HOLDER_TYPE
                        .eq("EXECUTION_STEP")
                        .and(EXECUTION_STEP.TEST_STEP_ID.in(testStepIdsInScope))
                        .and(CUSTOM_FIELD_VALUE.CF_ID.in(entityTypeToCufIdsListMap.get(EntityType.TEST_STEP)));

        query2.where(condition);

        query2.groupBy(DENORMALIZED_FIELD_VALUE.DFV_ID, CUSTOM_FIELD_VALUE.CFV_ID);

        return query2;
    }

    /**
     * Build the Where condition of the CustomFieldValues Query.
     *
     * @param entityTypeToCufIdsListMap The Map of Cuf ids List by EntityType
     * @param allRequestedEntitiesInCampaign All the EntityReferences which CustomFieldValues are
     *     requested
     * @return The Where condition of the CustomFieldValues Query
     */
    private Condition buildWhereConditionOfCufQuery(
            Map<EntityType, List<Long>> entityTypeToCufIdsListMap,
            Set<EntityReference> allRequestedEntitiesInCampaign) {

        Condition whereCondition = null;
        for (EntityReference entityReference : allRequestedEntitiesInCampaign) {
            List<Long> cufIdList = entityTypeToCufIdsListMap.get(entityReference.getType());
            Condition currentCondition = null;
            if (cufIdList != null) {
                currentCondition =
                        CUSTOM_FIELD_VALUE
                                .BOUND_ENTITY_TYPE
                                .eq(entityReference.getType().toString())
                                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.eq(entityReference.getId()))
                                .and(CUSTOM_FIELD_VALUE.CF_ID.in(cufIdList));
            }
            if (whereCondition == null) {
                whereCondition = currentCondition;
            } else {
                whereCondition = whereCondition.or(currentCondition);
            }
        }
        return whereCondition;
    }

    /**
     * Extract a Set of all the EntityReferences contained in the given Campaign, limited to the
     * EntityTypes contained in the Set given as parameter.
     *
     * @param entities The id of the Campaign concerned
     * @param entityTypes The EntityTypes of the EntityReferences that must be returned
     * @return A Set of all the EntityReferences contained in the Campaign which EntityType is
     *     contained in the given Set.
     */
    private Set<EntityReference> getEntityReferencesFromCampaign(
            List<EntityReference> entities, Set<EntityType> entityTypes) {
        SelectJoinStep query = buildEntityReferencesQuery(entities, entityTypes);
        Iterator<Record> queryResult = query.fetch().iterator();
        return buildEntityReferencesSetFromQueryResult(entityTypes, queryResult);
    }

    /**
     * Build the complete Query to retrieve all the requested EntityReferences from a Campaign.
     *
     * @param scopeEntities The EntityReference list representing the scope
     * @param requestedEntityTypes The EntityType of the requested EntityReferences
     * @return The Query to retrieve all the requested EntityReferences from the Campaign
     */
    private SelectJoinStep<Record> buildEntityReferencesQuery(
            List<EntityReference> scopeEntities, Set<EntityType> requestedEntityTypes) {

        int cufQueryDepth = getDepthOfEntitiesQuery(requestedEntityTypes);
        boolean isTestSuiteRequested = requestedEntityTypes.contains(EntityType.TEST_SUITE);

        List<Field<?>> fieldList =
                buildFieldsListOfEntitiesQuery(cufQueryDepth, isTestSuiteRequested, scopeEntities);

        SelectSelectStep<Record> selectQuery = dsl.select(fieldList);
        SelectJoinStep<Record> fromQuery =
                buildFromClauseOfEntitiesQuery(
                        selectQuery, cufQueryDepth, isTestSuiteRequested, scopeEntities);

        Condition condition = buildCondition(scopeEntities);

        fromQuery.where(condition);
        return fromQuery;
    }

    private Condition buildCondition(List<EntityReference> scopeEntities) {
        Map<EntityType, TableField<?, Long>> tableFieldByEntityTypeMap =
                getSelectableEntityTypeToIdTableFieldMap();

        Condition condition = null;

        for (EntityReference entity : scopeEntities) {
            TableField<?, Long> field = tableFieldByEntityTypeMap.get(entity.getType());
            if (condition == null) {
                condition = field.eq(entity.getId());
            } else {
                condition = condition.or(field.eq(entity.getId()));
            }
        }
        return condition;
    }

    /**
     * Exploit the EntityReferences Query Results to build a Set of all the EntityReferences in the
     * given Campaign.
     *
     * @param entityTypes The EntityTypes requested in the result
     * @param queryResult The Results of the Query
     * @return The Set of all EntityReferences in the Campaign based on the Query Results
     */
    private Set<EntityReference> buildEntityReferencesSetFromQueryResult(
            Set<EntityType> entityTypes, Iterator<Record> queryResult) {

        Set<EntityReference> entityReferenceSet = new HashSet<>();

        queryResult.forEachRemaining(
                record -> {
                    for (EntityType entityType : entityTypes) {
                        Long entityId =
                                record.get(
                                        CustomExportColumnLabel.getEntityTypeToIdTableFieldMap().get(entityType));
                        // The id could be null if left joined with a non existent entity
                        if (entityId != null) {
                            entityReferenceSet.add(new EntityReference(entityType, entityId));
                        }
                    }
                });
        return entityReferenceSet;
    }

    /**
     * Build the From clause of the Query
     *
     * @param selectQuery The previously built Select clause of the Query
     * @param cufQueryDepth The depth of the Query
     * @param isTestSuiteRequested If at least one TestSuite CustomField is requested
     * @return The From clause of the Query
     */
    private SelectJoinStep<Record> buildFromClauseOfEntitiesQuery(
            SelectSelectStep<Record> selectQuery,
            int cufQueryDepth,
            boolean isTestSuiteRequested,
            List<EntityReference> scopeEntity) {

        List<EntityType> types = scopeEntity.stream().map(EntityReference::getType).toList();
        boolean isIterationSelected = types.contains(EntityType.ITERATION);
        boolean isTestSuiteSelected = types.contains(EntityType.TEST_SUITE);
        boolean isFolderSelected = types.contains(EntityType.CAMPAIGN_FOLDER);

        SelectJoinStep<Record> fromQuery = selectQuery.from(CAMPAIGN);

        if (isFolderSelected) {
            fromQuery
                    .innerJoin(CLN_RELATIONSHIP_CLOSURE)
                    .on(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(CAMPAIGN.CLN_ID))
                    .innerJoin(CAMPAIGN_FOLDER)
                    .on(CAMPAIGN_FOLDER.CLN_ID.eq(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID));
        }

        if (cufQueryDepth > 1 || isIterationSelected || isTestSuiteSelected) {
            fromQuery
                    .leftJoin(CAMPAIGN_ITERATION)
                    .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                    .leftJoin(ITERATION)
                    .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID));
        }

        if (cufQueryDepth > 2 || isTestSuiteSelected) {
            fromQuery
                    .leftJoin(ITEM_TEST_PLAN_LIST)
                    .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                    .leftJoin(ITERATION_TEST_PLAN_ITEM)
                    .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                    .leftJoin(TEST_CASE)
                    .on(TEST_CASE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID));
            if (isTestSuiteRequested || isTestSuiteSelected) {
                fromQuery
                        .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .leftJoin(TEST_SUITE)
                        .on(TEST_SUITE.ID.eq(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID));
            }
        }

        if (cufQueryDepth > 3) {
            fromQuery
                    .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                    .on(
                            ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                    ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                    .leftJoin(EXECUTION)
                    .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID));
        }

        if (cufQueryDepth > 4) {
            fromQuery
                    .leftJoin(EXECUTION_EXECUTION_STEPS)
                    .on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                    .leftJoin(EXECUTION_STEP)
                    .on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                    .leftJoin(TEST_STEP)
                    .on(TEST_STEP.TEST_STEP_ID.eq(EXECUTION_STEP.TEST_STEP_ID));
        }
        return fromQuery;
    }

    /**
     * Build the List of Id Fields that the Cuf Query will select.
     *
     * @param cufQueryDepth The depth of the Cuf Query
     * @param isTestSuiteRequested If at least one TestSuite Cuf is requested
     * @return The List of Fields the Cuf Query have to select
     */
    private List<Field<?>> buildFieldsListOfEntitiesQuery(
            int cufQueryDepth, boolean isTestSuiteRequested, List<EntityReference> scopeEntity) {

        List<Field<?>> fieldList = new ArrayList<>();
        List<EntityType> types = scopeEntity.stream().map(EntityReference::getType).toList();

        fieldList.add(CAMPAIGN.CLN_ID);

        boolean isIterationSelected = types.contains(EntityType.ITERATION);
        boolean isTestSuiteSelected = types.contains(EntityType.TEST_SUITE);
        boolean isFolderSelected = types.contains(EntityType.CAMPAIGN_FOLDER);

        if (isFolderSelected) {
            fieldList.add(CAMPAIGN_FOLDER.CLN_ID);
        }

        if (cufQueryDepth > 1 || isIterationSelected) {
            fieldList.add(ITERATION.ITERATION_ID);
        }
        if (cufQueryDepth > 2 || isTestSuiteSelected) {
            fieldList.add(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);
            fieldList.add(TEST_CASE.TCLN_ID);
            if (isTestSuiteRequested || isTestSuiteSelected) {
                fieldList.add(TEST_SUITE.ID);
            }
        }
        if (cufQueryDepth > 3) {
            fieldList.add(EXECUTION.EXECUTION_ID);
        }
        if (cufQueryDepth > 4) {
            fieldList.add(EXECUTION_STEP.EXECUTION_STEP_ID);
            fieldList.add(EXECUTION_STEP.TEST_STEP_ID);
            fieldList.add(TEST_STEP.TEST_STEP_ID);
        }
        return fieldList;
    }

    /**
     * Return the depth of the Query.
     *
     * @param entityTypeList The list of the EntityTypes involved in the Query
     * @return The depth of the Query
     */
    private int getDepthOfEntitiesQuery(Set<EntityType> entityTypeList) {
        if (entityTypeList.contains(EntityType.EXECUTION_STEP)
                || entityTypeList.contains(EntityType.TEST_STEP)) return 5;
        if (entityTypeList.contains(EntityType.EXECUTION)) return 4;
        if (entityTypeList.contains(EntityType.TEST_CASE)
                || entityTypeList.contains(EntityType.TEST_SUITE)) return 3;
        if (entityTypeList.contains(EntityType.ITERATION)) return 2;
        // default
        return 1;
    }
}
