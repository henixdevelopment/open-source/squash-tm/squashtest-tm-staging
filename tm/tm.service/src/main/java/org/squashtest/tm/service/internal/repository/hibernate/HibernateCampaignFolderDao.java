/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_CAMPAIGN_FOLDER_CONTENT_BY_ID;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_CHILD_ENTITY_DTO_CAMPAIGN_FOLDERS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_CAMPAIGN_FOLDERS_ATTACHMENT_CONTENT_BY_ID;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_CAMPAIGN_FOLDERS_CONTENT_BY_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.inject.Provider;
import org.hibernate.annotations.QueryHints;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.internal.copier.ChildEntityDtoResult;
import org.squashtest.tm.service.internal.repository.CampaignFolderDao;

@Repository
public class HibernateCampaignFolderDao extends HibernateEntityDao<CampaignFolder>
        implements CampaignFolderDao {

    private final Provider<HibernateSprintGroupDao> sprintGroupDaoProvider;
    private final Provider<HibernateSprintDao> sprintDaoProvider;
    private final Provider<HibernateCampaignDao> campaignDaoProvider;

    @Autowired
    public HibernateCampaignFolderDao(
            Provider<HibernateSprintGroupDao> sprintGroupDaoProvider,
            Provider<HibernateSprintDao> sprintDaoProvider,
            Provider<HibernateCampaignDao> campaignDaoProvider) {
        this.sprintGroupDaoProvider = sprintGroupDaoProvider;
        this.sprintDaoProvider = sprintDaoProvider;
        this.campaignDaoProvider = campaignDaoProvider;
    }

    @Override
    public CampaignFolder findByContent(final CampaignLibraryNode node) {
        SetQueryParametersCallback callback = new SetNodeContentParameter(node);
        return executeEntityNamedQuery("campaignFolder.findByContent", callback);
    }

    @Override
    public List<String> findNamesInLibraryStartingWith(final long libraryId, final String nameStart) {
        SetQueryParametersCallback newCallBack1 =
                new ContainerIdNameStartParameterCallback(libraryId, nameStart);
        return executeListNamedQuery("campaignFolder.findNamesInLibraryStartingWith", newCallBack1);
    }

    /* ******************** //FIXME ******************* */

    @Override
    @SuppressWarnings("unchecked")
    public List<Long[]> findPairedContentForList(final List<Long> ids) {

        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        NativeQuery query =
                currentSession()
                        .createNativeQuery(NativeQueries.CAMPAIGN_FOLDER_SQL_FIND_PAIRED_CONTENT_FOR_FOLDERS);
        query.setParameterList("folderIds", ids, LongType.INSTANCE);
        query.addScalar("ancestor_id", LongType.INSTANCE);
        query.addScalar("descendant_id", LongType.INSTANCE);

        List<Object[]> result = query.list();

        return toArrayOfLong(result);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> findContentForList(List<Long> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        NativeQuery query =
                currentSession()
                        .createNativeQuery(NativeQueries.CAMPAIGN_FOLDER_SQL_FIND_CONTENT_FOR_FOLDER);
        query.setParameterList("folderIds", ids, LongType.INSTANCE);
        query.addScalar("descendant_id", LongType.INSTANCE);

        return query.list();
    }

    private List<Long[]> toArrayOfLong(List<Object[]> input) {
        List<Long[]> result = new ArrayList<>();

        for (Object[] pair : input) {
            Long[] newPair = new Long[] {(Long) pair[0], (Long) pair[1]};
            result.add(newPair);
        }

        return result;
    }

    @Override
    public CampaignFolder findParentOf(final Long id) {
        SetQueryParametersCallback newCallBack =
                new SetQueryParametersCallback() {

                    @Override
                    public void setQueryParameters(Query query) {
                        query.setParameter("contentId", id, LongType.INSTANCE);
                    }
                };
        return executeEntityNamedQuery("campaignFolder.findParentOf", newCallBack);
    }

    @Override
    public CampaignFolder loadContainerForPaste(long id) {
        return entityManager
                .createQuery(FIND_CAMPAIGN_FOLDER_CONTENT_BY_ID, CampaignFolder.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<CampaignFolder> loadContainersForPaste(Collection<Long> ids) {
        return entityManager
                .createQuery(FIND_DISTINCT_CAMPAIGN_FOLDERS_CONTENT_BY_ID, CampaignFolder.class)
                .setParameter("id", ids)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public List<CampaignFolder> loadNodeForPaste(Collection<Long> ids) {
        return entityManager
                .createQuery(FIND_DISTINCT_CAMPAIGN_FOLDERS_ATTACHMENT_CONTENT_BY_ID, CampaignFolder.class)
                .setParameter(IDS, ids)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public ChildEntityDtoResult loadChildForPaste(
            Collection<Long> ids, int maxResult, int offset, ClipboardPayload clipboardPayload) {
        return getChildEntityDtoForPaste(
                FIND_CHILD_ENTITY_DTO_CAMPAIGN_FOLDERS_BY_IDS,
                ids,
                maxResult,
                offset,
                clipboardPayload,
                (k, v) -> {
                    switch (k) {
                        case CAMPAIGN_FOLDER -> this.loadNodeForPaste(v);
                        case CAMPAIGN -> campaignDaoProvider.get().loadNodeForPaste(v);
                        case SPRINT_GROUP -> sprintGroupDaoProvider.get().loadNodeForPaste(v);
                        case SPRINT -> sprintDaoProvider.get().loadNodeForPaste(v);
                        default -> throw new UnsupportedOperationException("Unsupported entity type: " + k);
                    }
                });
    }

    @Override
    public CampaignFolder loadForNodeAddition(Long folderId) {
        return entityManager.createQuery("""
                select folder from CampaignFolder folder
                join fetch folder.project project
                left join fetch folder.content content
                where folder.id in :id""",
                CampaignFolder.class)
            .setParameter(ID, folderId)
            .getSingleResult();
    }

    @Override
    public List<CampaignFolder> loadForNodeAddition(Collection<Long> folderIds) {
        return entityManager.createQuery("""
                select distinct folder from CampaignFolder folder
                join fetch folder.project project
                left join fetch folder.content content
                where folder.id in :ids""",
                CampaignFolder.class)
            .setParameter(IDS, folderIds)
            .setHint(org.hibernate.jpa.QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
