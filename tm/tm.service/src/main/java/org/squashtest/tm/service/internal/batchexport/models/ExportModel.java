/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.InputType;

public class ExportModel {
    private List<TestCaseModel> testCases = new LinkedList<>();
    private List<TestStepModel> testSteps = new LinkedList<>();
    private List<ParameterModel> parameters = new LinkedList<>();
    private List<DatasetModel> datasets = new LinkedList<>();
    private List<CoverageModel> coverages = new LinkedList<>();

    public ExportModel() {
        super();
    }

    public void setTestCases(List<TestCaseModel> models) {
        this.testCases = models;
    }

    public void setTestSteps(List<TestStepModel> models) {
        this.testSteps = models;
    }

    public void setParameters(List<ParameterModel> models) {
        this.parameters = models;
    }

    public void setDatasets(List<DatasetModel> models) {
        this.datasets = models;
    }

    public void addTestCaseModel(TestCaseModel model) {
        testCases.add(model);
    }

    public void addTestStepModel(TestStepModel model) {
        testSteps.add(model);
    }

    public void addParameterModel(ParameterModel model) {
        parameters.add(model);
    }

    public void addDatasetModel(DatasetModel model) {
        datasets.add(model);
    }

    public void addCoverageModel(CoverageModel model) {
        coverages.add(model);
    }

    public List<TestCaseModel> getTestCases() {
        return testCases;
    }

    public List<TestStepModel> getTestSteps() {
        return testSteps;
    }

    public List<ParameterModel> getParameters() {
        return parameters;
    }

    public List<DatasetModel> getDatasets() {
        return datasets;
    }

    public List<CoverageModel> getCoverages() {
        return coverages;
    }

    public void setCoverages(List<CoverageModel> coverages) {
        this.coverages = coverages;
    }

    public static final class CustomField {
        private Long ownerId;
        private BindableEntity ownerType;
        private String code;
        private String value;
        private InputType type;
        private String selectedOptions;
        private String label;
        private Long cufId;

        public CustomField(
                Long ownerId,
                BindableEntity ownerType,
                String code,
                String value,
                String largeValue,
                InputType type,
                String label,
                Long cufId,
                String selectedOptions) {
            super();
            this.ownerId = ownerId;
            this.ownerType = ownerType;
            this.code = code;
            this.value = !StringUtils.isBlank(largeValue) ? largeValue : value;
            this.type = type;
            this.selectedOptions = selectedOptions;
            this.label = label;
            this.cufId = cufId;
        }

        public Long getOwnerId() {
            return this.ownerId;
        }

        public BindableEntity getOwnerType() {
            return ownerType;
        }

        public String getCode() {
            return code;
        }

        public String getValue() {
            return !StringUtils.isBlank(selectedOptions) ? selectedOptions : value;
        }

        public InputType getType() {
            return type;
        }

        public String getLabel() {
            return label;
        }

        public Long getCufId() {
            return cufId;
        }

        public void setValue(String newValue) {
            this.value = newValue;
        }
    }
}
