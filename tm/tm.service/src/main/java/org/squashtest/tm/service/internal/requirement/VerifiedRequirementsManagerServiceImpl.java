/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import static org.squashtest.tm.service.security.Authorizations.LINK_TESTSTEP_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQVERSION_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import org.apache.commons.collections.MultiMap;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.core.foundation.collection.PagingBackedPagedCollectionHolder;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementCoverageStat;
import org.squashtest.tm.domain.requirement.RequirementCoverageStat.Rate;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseExecutionStatus;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.exception.requirement.AbstractVerifiedRequirementException;
import org.squashtest.tm.exception.requirement.RequirementAlreadyVerifiedException;
import org.squashtest.tm.exception.requirement.RequirementVersionNotLinkableException;
import org.squashtest.tm.service.audit.AuditModificationService;
import org.squashtest.tm.service.internal.repository.ExecutionStepDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionCoverageDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.TestStepDao;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.requirement.VerifiedRequirement;
import org.squashtest.tm.service.requirement.VerifiedRequirementsManagerService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testcase.TestCaseImportanceManagerService;

@Service("squashtest.tm.service.VerifiedRequirementsManagerService")
@Transactional
public class VerifiedRequirementsManagerServiceImpl implements VerifiedRequirementsManagerService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(VerifiedRequirementsManagerServiceImpl.class);
    private static final String LINK_TC_OR_ROLE_ADMIN =
            "hasPermission(#testCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'LINK')"
                    + OR_HAS_ROLE_ADMIN;

    @Inject private TestCaseDao testCaseDao;

    @Inject private TestStepDao testStepDao;

    @Inject private RequirementVersionDao requirementVersionDao;

    @Inject private TestCaseCallTreeFinder callTreeFinder;

    @Inject private RequirementVersionCoverageDao requirementVersionCoverageDao;

    @Inject private TestCaseImportanceManagerService testCaseImportanceManagerService;

    @Inject private RequirementDao requirementDao;

    @Inject private IterationDao iterationDao;

    @Inject private ExecutionStepDao executionStepDao;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private AuditModificationService auditModificationService;

    @Inject private TestCaseLoader testCaseLoader;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @SuppressWarnings("rawtypes")
    @Inject
    @Qualifier("squashtest.tm.repository.RequirementLibraryNodeDao")
    private LibraryNodeDao<RequirementLibraryNode> requirementLibraryNodeDao;

    @Override
    @PreAuthorize(LINK_TC_OR_ROLE_ADMIN)
    public Collection<AbstractVerifiedRequirementException> addVerifiedRequirementsToTestCase(
            List<Long> requirementsIds, long testCaseId) {

        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                requirementsIds,
                Permissions.READ.name(),
                Requirement.class.getName());

        List<RequirementVersion> requirementVersions = findCurrentRequirementVersions(requirementsIds);
        return findVerifiedRequirementExceptionsForTestCase(testCaseId, requirementVersions);
    }

    @Override
    public Collection<AbstractVerifiedRequirementException>
            addVerifiedRequirementsToTestCaseUnsecured(List<Long> requirementsIds, TestCase testCase) {
        List<RequirementVersion> requirementVersions = findCurrentRequirementVersions(requirementsIds);
        return findVerifiedRequirementExceptionsForTestCase(testCase, requirementVersions);
    }

    @Override
    @PreAuthorize(LINK_TC_OR_ROLE_ADMIN)
    public Collection<AbstractVerifiedRequirementException> addRequirementVersionsToTestCase(
            List<Long> requirementVersionIds, long testCaseId) {
        List<RequirementVersion> requirementVersions =
                getRequirementVersionsInCorrectOrder(requirementVersionIds);
        return findVerifiedRequirementExceptionsForTestCase(testCaseId, requirementVersions);
    }

    private Collection<AbstractVerifiedRequirementException>
            findVerifiedRequirementExceptionsForTestCase(
                    long testCaseId, List<RequirementVersion> requirementVersions) {

        if (!requirementVersions.isEmpty()) {
            TestCase testCase =
                    testCaseLoader.load(
                            testCaseId, EnumSet.of(TestCaseLoader.Options.FETCH_REQUIREMENT_VERSION_COVERAGES));

            return doAddVerifyingRequirementVersionsToTestCase(requirementVersions, testCase);
        }

        return Collections.emptyList();
    }

    private Collection<AbstractVerifiedRequirementException>
            findVerifiedRequirementExceptionsForTestCase(
                    TestCase testCase, List<RequirementVersion> requirementVersions) {
        if (!requirementVersions.isEmpty()) {
            return doAddVerifyingRequirementVersionsToTestCase(requirementVersions, testCase);
        }
        return Collections.emptyList();
    }

    private List<RequirementVersion> extractVersions(List<Requirement> requirements) {

        List<RequirementVersion> rvs = new ArrayList<>(requirements.size());

        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();

        for (Requirement requirement : requirements) {

            // normal mode
            if (activeMilestone.isEmpty()) {
                rvs.add(requirement.getResource());
            }
            // milestone mode
            else {
                rvs.add(requirement.findByMilestone(activeMilestone.get()));
            }
        }
        return rvs;
    }

    @Override
    @PreAuthorize(LINK_TC_OR_ROLE_ADMIN)
    public void removeVerifiedRequirementFromTestCase(List<Long> requirementIds, long testCaseId) {
        List<RequirementVersion> requirementVersions = findCurrentRequirementVersions(requirementIds);
        List<Long> requirementVersionsIds =
                requirementVersions.stream().map(RequirementVersion::getId).toList();
        removeVerifiedRequirementVersionsFromTestCase(requirementVersionsIds, testCaseId);
    }

    @Override
    @PreAuthorize(LINK_TC_OR_ROLE_ADMIN)
    public void removeVerifiedRequirementVersionsFromTestCase(
            List<Long> requirementVersionsIds, long testCaseId) {

        if (!requirementVersionsIds.isEmpty()) {

            List<Long> requirementVersionCoverageIds =
                    requirementVersionCoverageDao.byTestCaseAndRequirementVersions(
                            requirementVersionsIds, testCaseId);

            requirementVersionCoverageDao.delete(requirementVersionCoverageIds);

            testCaseImportanceManagerService.changeImportanceIfRelationsRemovedFromTestCase(
                    requirementVersionsIds, testCaseId);
        }
    }

    @Override
    @PreAuthorize(LINK_TC_OR_ROLE_ADMIN)
    public void removeVerifiedRequirementVersionFromTestCase(
            long requirementVersionId, long testCaseId) {
        RequirementVersionCoverage coverage =
                requirementVersionCoverageDao.byRequirementVersionAndTestCase(
                        requirementVersionId, testCaseId);

        requirementVersionCoverageDao.delete(List.of(coverage.getId()));
        testCaseImportanceManagerService.changeImportanceIfRelationsRemovedFromTestCase(
                Arrays.asList(requirementVersionId), testCaseId);
    }

    @Override
    public int changeVerifiedRequirementVersionOnTestCase(
            long oldVerifiedRequirementVersionId, long newVerifiedRequirementVersionId, long testCaseId) {
        RequirementVersion newReq =
                requirementVersionDao.getReferenceById(newVerifiedRequirementVersionId);
        RequirementVersionCoverage coverage =
                requirementVersionCoverageDao.byRequirementVersionAndTestCase(
                        oldVerifiedRequirementVersionId, testCaseId);
        coverage.setVerifiedRequirementVersion(newReq);
        testCaseImportanceManagerService.changeImportanceIfRelationsRemovedFromTestCase(
                Arrays.asList(newVerifiedRequirementVersionId), testCaseId);

        return newReq.getVersionNumber();
    }

    private Collection<AbstractVerifiedRequirementException>
            doAddVerifyingRequirementVersionsToTestCase(
                    List<RequirementVersion> requirementVersions, TestCase testCase) {
        Collection<AbstractVerifiedRequirementException> rejections = new ArrayList<>();
        Iterator<RequirementVersion> iterator = requirementVersions.iterator();
        while (iterator.hasNext()) {
            RequirementVersion requirementVersion = iterator.next();
            try {
                RequirementVersionCoverage coverage =
                        new RequirementVersionCoverage(requirementVersion, testCase);
                requirementVersionCoverageDao.persist(coverage);
            } catch (RequirementAlreadyVerifiedException | RequirementVersionNotLinkableException ex) {
                LOGGER.warn(ex.getMessage());
                rejections.add(ex);
                iterator.remove();
            }
        }
        testCaseImportanceManagerService.changeImportanceIfRelationsAddedToTestCase(
                requirementVersions, testCase);
        return rejections;
    }

    @Override
    public void addVerifiedRequirementVersionsToTestCaseFromReq(
            RequirementVersion requirementVersion, TestCase testCase) {

        RequirementVersionCoverage coverage =
                new RequirementVersionCoverage(requirementVersion, testCase, false);
        requirementVersionCoverageDao.persist(coverage);
    }

    @Override
    @PreAuthorize(LINK_TESTSTEP_OR_ROLE_ADMIN)
    public Collection<AbstractVerifiedRequirementException> addVerifiedRequirementsToTestStep(
            List<Long> requirementsIds, long testStepId) {

        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                requirementsIds,
                Permissions.READ.name(),
                Requirement.class.getName());

        List<RequirementVersion> requirementVersions = findCurrentRequirementVersions(requirementsIds);
        return findVerifiedRequirementExceptionsForStep(testStepId, requirementVersions);
    }

    @Override
    public Collection<AbstractVerifiedRequirementException>
            addVerifiedRequirementsToTestStepUnsecured(
                    List<Long> requirementsIds, ActionTestStep actionTestStep, TestCase testCase) {
        List<RequirementVersion> requirementVersions = findCurrentRequirementVersions(requirementsIds);
        return findVerifiedRequirementExceptionsForStep(actionTestStep, testCase, requirementVersions);
    }

    private Collection<AbstractVerifiedRequirementException> findVerifiedRequirementExceptionsForStep(
            ActionTestStep actionTestStep,
            TestCase testCase,
            List<RequirementVersion> requirementVersions) {
        Collection<AbstractVerifiedRequirementException> rejections = new ArrayList<>();

        if (!requirementVersions.isEmpty()) {
            testAllRequirement(requirementVersions, actionTestStep, testCase, rejections);
            testCaseImportanceManagerService.changeImportanceIfRelationsAddedToTestCase(
                    requirementVersions, testCase);
        }
        return rejections;
    }

    @Override
    @PreAuthorize(LINK_TESTSTEP_OR_ROLE_ADMIN)
    public Collection<AbstractVerifiedRequirementException> addRequirementVersionsToTestStep(
            List<Long> requirementVersionIds, long testStepId) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                requirementVersionIds,
                Permissions.READ.name(),
                RequirementVersion.class.getName());
        List<RequirementVersion> requirementVersions =
                getRequirementVersionsInCorrectOrder(requirementVersionIds);
        return findVerifiedRequirementExceptionsForStep(testStepId, requirementVersions);
    }

    /**
     * Fetch requirement versions, then group them by requirement and order them from latest to
     * oldest.
     *
     * <p>This ensures that if multiple versions of the same requirement are selected, the latest one
     * will be treated first.
     *
     * @param requirementVersionIds
     * @return a {@link List} of {@link RequirementVersion}
     */
    private List<RequirementVersion> getRequirementVersionsInCorrectOrder(
            List<Long> requirementVersionIds) {
        List<RequirementVersion> requirementVersions =
                requirementVersionDao.findAllById(requirementVersionIds);
        requirementVersions.sort(
                Comparator.comparing(RequirementVersion::fetchRequirementId)
                        .thenComparing(RequirementVersion::getVersionNumber)
                        .reversed());
        return requirementVersions;
    }

    private Collection<AbstractVerifiedRequirementException> findVerifiedRequirementExceptionsForStep(
            long testStepId, List<RequirementVersion> requirementVersions) {
        // init rejections
        Collection<AbstractVerifiedRequirementException> rejections = new ArrayList<>();
        // check if list not empty
        if (!requirementVersions.isEmpty()) {
            // collect concerned entities
            ActionTestStep step = testStepDao.findActionTestStepById(testStepId);
            TestCase testCase = step.getTestCase();
            // iterate on requirement versions
            testAllRequirement(requirementVersions, step, testCase, rejections);
            testCaseImportanceManagerService.changeImportanceIfRelationsAddedToTestCase(
                    requirementVersions, testCase);
        }
        return rejections;
    }

    public void testAllRequirement(
            List<RequirementVersion> requirementVersions,
            ActionTestStep step,
            TestCase testCase,
            Collection<AbstractVerifiedRequirementException> rejections) {
        Iterator<RequirementVersion> iterator = requirementVersions.iterator();
        while (iterator.hasNext()) {
            try {
                RequirementVersion requirementVersion = iterator.next();
                boolean newReqCoverage =
                        addVerifiedRequirementVersionToTestStep(requirementVersion, step, testCase);
                if (!newReqCoverage) {
                    iterator.remove();
                }
            } catch (RequirementAlreadyVerifiedException | RequirementVersionNotLinkableException ex) {
                LOGGER.warn(ex.getMessage());
                iterator.remove();
                rejections.add(ex);
            }
        }
    }

    /**
     * Will find the RequirementVersionCoverage for the given requirement version and test case to add
     * the step to it. If not found, will create a new RequirementVersionCoverage for the test case
     * and add the step to it.<br>
     *
     * @param step
     * @param testCase
     * @return true if a new RequirementVersionCoverage has been created.
     */
    private boolean addVerifiedRequirementVersionToTestStep(
            RequirementVersion requirementVersion, ActionTestStep step, TestCase testCase) {

        RequirementVersionCoverage coverage =
                requirementVersionCoverageDao.byRequirementVersionAndTestCase(
                        requirementVersion.getId(), testCase.getId());

        if (coverage == null) {
            RequirementVersionCoverage newCoverage =
                    new RequirementVersionCoverage(requirementVersion, testCase);
            newCoverage.addAllVerifyingSteps(Arrays.asList(step));
            requirementVersionCoverageDao.persist(newCoverage);
            return true;
        } else {
            if (coverage.hasStepAsVerifying(step.getId())) {
                throw new RequirementAlreadyVerifiedException(requirementVersion, testCase);
            }
            coverage.addAllVerifyingSteps(Arrays.asList(step));
            return false;
        }
    }

    private List<RequirementVersion> findCurrentRequirementVersions(List<Long> requirementsIds) {
        List<Requirement> requirements =
                requirementDao.findRequirementsNodeRootAndInFolderByNodeIds(requirementsIds);

        if (!requirements.isEmpty()) {
            return extractVersions(requirements);
        }

        return Collections.emptyList();
    }

    @Override
    @Transactional(readOnly = true)
    public PagedCollectionHolder<List<VerifiedRequirement>> findAllVerifiedRequirementsByTestCaseId(
            long testCaseId, PagingAndSorting pas) {

        LOGGER.debug("Looking for verified requirements of TestCase[id:{}]", testCaseId);

        Set<Long> calleesIds = callTreeFinder.getTestCaseCallTree(testCaseId);

        calleesIds.add(testCaseId);

        LOGGER.debug("Fetching Requirements verified by TestCases {}", calleesIds.toString());

        List<RequirementVersion> pagedVersionVerifiedByCalles =
                requirementVersionCoverageDao.findDistinctRequirementVersionsByTestCases(calleesIds, pas);

        TestCase mainTestCase =
                testCaseLoader.load(
                        testCaseId, EnumSet.of(TestCaseLoader.Options.FETCH_REQUIREMENT_VERSION_COVERAGES));

        List<VerifiedRequirement> pagedVerifiedReqs =
                buildVerifiedRequirementList(mainTestCase, pagedVersionVerifiedByCalles);

        long totalVerified =
                requirementVersionCoverageDao.numberDistinctVerifiedByTestCases(calleesIds);

        LOGGER.debug("Total count of verified requirements : {}", totalVerified);

        return new PagingBackedPagedCollectionHolder<>(pas, totalVerified, pagedVerifiedReqs);
    }

    @Override
    public List<VerifiedRequirement> findAllVerifiedRequirementsByTestCaseId(long testCaseId) {
        LOGGER.debug("Looking for verified requirements of TestCase[id:{}]", testCaseId);

        Set<Long> calleesIds = callTreeFinder.getTestCaseCallTree(testCaseId);

        calleesIds.add(testCaseId);

        LOGGER.debug("Fetching Requirements verified by TestCases {}", calleesIds.toString());

        List<RequirementVersion> pagedVersionVerifiedByCalles =
                requirementVersionCoverageDao.findDistinctRequirementVersionsByTestCases(calleesIds);

        TestCase mainTestCase =
                testCaseLoader.load(
                        testCaseId, EnumSet.of(TestCaseLoader.Options.FETCH_REQUIREMENT_VERSION_COVERAGES));

        return buildVerifiedRequirementList(mainTestCase, pagedVersionVerifiedByCalles);
    }

    @Override
    public Map<Long, Boolean> findisReqCoveredOfCallingTCWhenisReqCoveredChanged(
            long updatedTestCaseId, Collection<Long> toUpdateIds) {
        Map<Long, Boolean> result;
        result = new HashMap<>(toUpdateIds.size());

        if (testCaseHasDirectCoverage(updatedTestCaseId)
                || testCaseHasUndirectRequirementCoverage(updatedTestCaseId)) {
            // set isReqCovered = true for all calling test cases
            for (Long id : toUpdateIds) {
                result.put(id, Boolean.TRUE);
            }
        } else {
            // check each calling testCase to see if their status changed
            for (Long id : toUpdateIds) {
                Boolean value = testCaseHasDirectCoverage(id) || testCaseHasUndirectRequirementCoverage(id);
                result.put(id, value);
            }
        }

        return result;
    }

    @Override
    public boolean testCaseHasUndirectRequirementCoverage(long updatedTestCaseId) {
        List<Long> calledTestCaseIds =
                testCaseDao.findAllDistinctTestCasesIdsCalledByTestCase(updatedTestCaseId);
        if (!calledTestCaseIds.isEmpty()) {
            for (Long id : calledTestCaseIds) {
                if (testCaseHasDirectCoverage(id) || testCaseHasUndirectRequirementCoverage(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean testCaseHasDirectCoverage(long updatedTestCaseId) {
        return requirementVersionDao.countVerifiedByTestCase(updatedTestCaseId) > 0;
    }

    private List<VerifiedRequirement> buildVerifiedRequirementList(
            final TestCase main, List<RequirementVersion> pagedVersionVerifiedByCalles) {

        List<VerifiedRequirement> toReturn = new ArrayList<>(pagedVersionVerifiedByCalles.size());

        for (RequirementVersion rVersion : pagedVersionVerifiedByCalles) {
            boolean isDirect = main.verifies(rVersion);
            toReturn.add(new VerifiedRequirement(rVersion, isDirect).withVerifyingStepsFrom(main));
        }

        return toReturn;
    }

    @Override
    @PreAuthorize(LINK_TESTSTEP_OR_ROLE_ADMIN)
    public void removeVerifiedRequirementVersionsFromTestStep(
            List<Long> requirementVersionsIds, long testStepId) {

        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                requirementVersionsIds,
                Permissions.READ.name(),
                RequirementVersion.class.getName());

        List<Long> coverageIds =
                requirementVersionCoverageDao.byRequirementVersionsAndTestStep(
                        requirementVersionsIds, testStepId);
        requirementVersionCoverageDao.removeTestSTepByCoverageIds(coverageIds);
        TestCase tc = testStepDao.findTestCaseFromActionTestStep(testStepId);

        auditModificationService.updateAuditable((AuditableMixin) tc);
    }

    @Override
    @PreAuthorize(READ_REQVERSION_OR_ROLE_ADMIN)
    public void findCoverageStat(
            Long requirementVersionId, List<Long> iterationsIds, RequirementCoverageStat stats) {

        RequirementVersion mainVersion = requirementVersionDao.getReferenceById(requirementVersionId);
        Requirement mainRequirement = mainVersion.getRequirement();
        List<RequirementVersion> descendants = findValidDescendants(mainRequirement);
        findCoverageRate(mainVersion, descendants, stats);

        // if we have a valid perimeter (ie iteration(s)), we'll have to calculate verification and
        // validation rates
        if (!iterationsIds.isEmpty()) {
            checkPerimeter(iterationsIds, stats);
            if (!stats.isCorruptedPerimeter()) {
                findExecutionRate(mainVersion, descendants, stats, iterationsIds);
            }
        }
        stats.convertRatesToPercent();
    }

    private void checkPerimeter(List<Long> iterationsIds, RequirementCoverageStat stats) {
        List<Iteration> iterations = iterationDao.findAllByIds(iterationsIds);
        if (iterations.size() != iterationsIds.size()) {
            stats.setCorruptedPerimeter(true);
        }
    }

    /**
     * Extract a {@link Map}, key : {@link ExecutionStatus} value : {@link Long}. The goal is to
     * perform arithmetic operation with this map to calculate several rates on {@link
     * RequirementVersion} Constraints from specification Feat 4434 : <code>
     * <ul>
     * <li>Requirement without linked {@link TestStep} must be treated at {@link Execution} level, for last execution.
     * We must also include fast pass so we take the {@link IterationTestPlanItem} status</li>
     * <li>Requirement with linked {@link TestStep} must be treated at {@link ExecutionStep} level</li>
     * <li>Only last execution must be considered for a given {@link IterationTestPlanItem}</li>
     * <li>FastPass must be considered for all cases (ie even if the {@link RequirementVersion} is linked to {@link TestStep})</li>
     * <li>Rate must be calculate on the designed {@link Requirement} and it's descendants</li>
     * <li>The descendant list must be filtered by {@link Milestone} and exclude {@link RequirementVersion} with {@link RequirementStatus#OBSOLETE}</li>
     * </ul>
     * </code>
     *
     * @param mainVersion
     * @param descendants
     * @param stats pojo containing the computed stats
     * @param iterationsIds
     */
    private void findExecutionRate(
            RequirementVersion mainVersion,
            List<RequirementVersion> descendants,
            RequirementCoverageStat stats,
            List<Long> iterationsIds) {
        boolean hasDescendant = !descendants.isEmpty();
        Rate verificationRate = new Rate();
        Rate validationRate = new Rate();

        // see http://javadude.com/articles/passbyvalue.htm to understand why an array (or any object)
        // is needed here
        Long[] mainUntestedElementsCount = new Long[] {0L};
        Map<ExecutionStatus, Long> mainStatusMap = new EnumMap<>(ExecutionStatus.class);
        makeStatusMap(
                mainVersion.getRequirementVersionCoverages(),
                mainUntestedElementsCount,
                mainStatusMap,
                iterationsIds);
        verificationRate.setRequirementVersionRate(
                doRateVerifiedCalculation(mainStatusMap, mainUntestedElementsCount[0]));
        validationRate.setRequirementVersionRate(doRateValidatedCalculation(mainStatusMap));

        if (hasDescendant) {
            verificationRate.setAncestor(true);
            validationRate.setAncestor(true);
            Set<RequirementVersionCoverage> descendantCoverages = getDescendantCoverages(descendants);
            Long[] descendantUntestedElementsCount = new Long[] {0L};
            Map<ExecutionStatus, Long> descendantStatusMap = new EnumMap<>(ExecutionStatus.class);
            makeStatusMap(
                    descendantCoverages, descendantUntestedElementsCount, descendantStatusMap, iterationsIds);
            verificationRate.setRequirementVersionChildrenRate(
                    doRateVerifiedCalculation(descendantStatusMap, descendantUntestedElementsCount[0]));
            validationRate.setRequirementVersionChildrenRate(
                    doRateValidatedCalculation(descendantStatusMap));

            Long[] allUntestedElementsCount = new Long[] {0L};
            allUntestedElementsCount[0] =
                    mainUntestedElementsCount[0] + descendantUntestedElementsCount[0];
            Map<ExecutionStatus, Long> allStatusMap = mergeMapResult(mainStatusMap, descendantStatusMap);
            verificationRate.setRequirementVersionGlobalRate(
                    doRateVerifiedCalculation(allStatusMap, allUntestedElementsCount[0]));
            validationRate.setRequirementVersionGlobalRate(doRateValidatedCalculation(allStatusMap));
        }

        stats.addRate("verification", verificationRate);
        stats.addRate("validation", validationRate);
    }

    /**
     * Return a merged map. For each {@link ExecutionStatus}, the returned value is the value in map1
     * + value in map 2. The state of the two arguments maps is preserved
     *
     * @param mainStatusMap
     * @param descendantStatusMap
     * @return
     */
    private Map<ExecutionStatus, Long> mergeMapResult(
            Map<ExecutionStatus, Long> mainStatusMap, Map<ExecutionStatus, Long> descendantStatusMap) {
        Map<ExecutionStatus, Long> mergedStatusMap = new EnumMap<>(ExecutionStatus.class);
        EnumSet<ExecutionStatus> allStatus = EnumSet.allOf(ExecutionStatus.class);
        for (ExecutionStatus executionStatus : allStatus) {

            Long mainCount =
                    mainStatusMap.get(executionStatus) == null
                            ? Long.valueOf(0L)
                            : mainStatusMap.get(executionStatus);
            Long descendantCount =
                    descendantStatusMap.get(executionStatus) == null
                            ? Long.valueOf(0L)
                            : descendantStatusMap.get(executionStatus);
            Long totalCount = mainCount + descendantCount;
            mergedStatusMap.put(executionStatus, totalCount);
        }
        return mergedStatusMap;
    }

    /**
     * As above but with no return. The second map is merged into the first map, which orginal state
     * is lost
     *
     * @param statusMap
     * @param statusMapToMerge
     */
    private void fusionMapResult(
            Map<ExecutionStatus, Long> statusMap, Map<ExecutionStatus, Long> statusMapToMerge) {
        for (Entry<ExecutionStatus, Long> mergeEntry : statusMapToMerge.entrySet()) {
            ExecutionStatus executionStatus = mergeEntry.getKey();
            Long originalValue = statusMap.get(executionStatus);
            Long mergedValue = mergeEntry.getValue();
            if (mergedValue != null && originalValue == null) {
                statusMap.put(executionStatus, mergedValue);
            }
            if (mergedValue != null && originalValue != null) {
                statusMap.put(executionStatus, mergedValue + originalValue);
            }
        }
    }

    private Set<RequirementVersionCoverage> getDescendantCoverages(
            List<RequirementVersion> descendants) {
        Set<RequirementVersionCoverage> covs = new HashSet<>();
        for (RequirementVersion requirementVersion : descendants) {
            Set<RequirementVersionCoverage> coverages =
                    requirementVersion.getRequirementVersionCoverages();
            if (!coverages.isEmpty()) {
                covs.addAll(coverages);
            }
        }
        return covs;
    }

    private List<Long> filterTCIds(List<Long> tcIds, List<Long> tCWithItpiIds) {
        List<Long> filtered = new ArrayList<>();
        filtered.addAll(tcIds);
        filtered.removeAll(tCWithItpiIds);
        return filtered;
    }

    private List<Long> convertSetToList(Map<Long, Long> nbSimpleCoverageByTestCase) {
        List<Long> testCaseIds = new ArrayList<>();
        testCaseIds.addAll(nbSimpleCoverageByTestCase.keySet());
        return testCaseIds;
    }

    private List<Long> findTCWithItpi(List<Long> tcIds, List<Long> iterationsIds) {
        return iterationDao.findVerifiedTcIdsInIterations(tcIds, iterationsIds);
    }

    private void makeStatusMap(
            Set<RequirementVersionCoverage> covs,
            Long[] untestedElementsCount,
            Map<ExecutionStatus, Long> statusMap,
            List<Long> iterationsIds) {

        List<RequirementVersionCoverage> simpleCoverage = new ArrayList<>();
        List<RequirementVersionCoverage> stepedCoverage = new ArrayList<>();
        Map<Long, Long> nbSimpleCoverageByTestCase = new HashMap<>();
        Map<Long, Long> nbSteppedCoverageByTestCase = new HashMap<>();
        partRequirementVersionCoverage(
                covs,
                simpleCoverage,
                stepedCoverage,
                nbSimpleCoverageByTestCase,
                nbSteppedCoverageByTestCase);
        // Find the test case with at least one itpi
        List<Long> simpleCoverageTCIds = convertSetToList(nbSimpleCoverageByTestCase);
        List<Long> simpleTCWithItpiIds = findTCWithItpi(simpleCoverageTCIds, iterationsIds);
        // Filter to have the test case without itpi
        List<Long> mainVersionTCWithoutItpiIds = filterTCIds(simpleCoverageTCIds, simpleTCWithItpiIds);
        Map<ExecutionStatus, Long> statusMapForSimple =
                findResultsForSimpleCoverage(
                        simpleTCWithItpiIds, iterationsIds, nbSimpleCoverageByTestCase);

        // STEPPED
        // we need : TC without ITPI -> untested, TC With ITPI but no execution -> treated like fastpass
        // (ie the status of ITPI is applied to each stepped coverage),
        // TC with execution -> treated at test step level
        List<Long> steppedCoverageTCIds = convertSetToList(nbSteppedCoverageByTestCase);
        List<Long> steppedCoverageTCIdsWithITPI =
                iterationDao.findVerifiedTcIdsInIterations(steppedCoverageTCIds, iterationsIds);
        List<Long> steppedCoverageTCIdsWithExecution =
                iterationDao.findVerifiedTcIdsInIterationsWithExecution(
                        steppedCoverageTCIds, iterationsIds);
        List<Long> steppedCoverageTCIdsWithoutITPI =
                filterTCIds(steppedCoverageTCIds, steppedCoverageTCIdsWithITPI);
        List<Long> steppedCoverageTCIdsWithoutExecution =
                filterTCIds(steppedCoverageTCIdsWithITPI, steppedCoverageTCIdsWithExecution);

        // TC With ITPI but no execution are treated like simple testcase
        Map<ExecutionStatus, Long> statusMapForSteppedNoExecution =
                findResultsForSteppedCoverageWithoutExecution(
                        stepedCoverage, steppedCoverageTCIdsWithoutExecution, iterationsIds);
        untestedElementsCount[0] =
                calculateUntestedElementCount(
                        mainVersionTCWithoutItpiIds,
                        nbSimpleCoverageByTestCase,
                        stepedCoverage,
                        steppedCoverageTCIdsWithoutITPI);
        Map<ExecutionStatus, Long> statusMapForSteppedWithExecution =
                findResultsForSteppedCoverageWithExecution(
                        stepedCoverage, steppedCoverageTCIdsWithExecution);

        // merging the three map of results
        fusionMapResult(statusMap, statusMapForSimple);
        fusionMapResult(statusMap, statusMapForSteppedNoExecution);
        fusionMapResult(statusMap, statusMapForSteppedWithExecution);
    }

    @SuppressWarnings("unchecked")
    private Map<ExecutionStatus, Long> findResultsForSteppedCoverageWithoutExecution(
            List<RequirementVersionCoverage> stepedCoverage,
            List<Long> testCaseIds,
            List<Long> iterationsIds) {

        MultiMap testCaseExecutionStatus = iterationDao.findVerifiedITPI(testCaseIds, iterationsIds);
        Map<ExecutionStatus, Long> result = new EnumMap<>(ExecutionStatus.class);
        for (RequirementVersionCoverage cov : stepedCoverage) {
            Long tcId = cov.getVerifyingTestCase().getId();
            List<TestCaseExecutionStatus> tcsStatus =
                    (List<TestCaseExecutionStatus>) testCaseExecutionStatus.get(tcId);
            if (tcsStatus != null) {
                for (TestCaseExecutionStatus tcStatus : tcsStatus) {
                    // For each cov we must count one status per steps. So fast pass status is forwarded to
                    // steps...
                    result.put(tcStatus.getStatus(), (long) cov.getVerifyingSteps().size());
                }
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private Map<ExecutionStatus, Long> findResultsForSteppedCoverageWithExecution(
            List<RequirementVersionCoverage> stepedCoverage, List<Long> mainVersionTCWithItpiIds) {
        List<Long> testStepsIds = new ArrayList<>();
        Map<ExecutionStatus, Long> result = new EnumMap<>(ExecutionStatus.class);
        // First we compute all testStep id in a list, to allow multiple occurrence of the same step.
        // Which is not a good practice but is allowed by the app so we must take this possibility in
        // account for calculations.
        for (RequirementVersionCoverage cov : stepedCoverage) {
            Long tcId = cov.getVerifyingTestCase().getId();
            if (mainVersionTCWithItpiIds.contains(tcId)) {
                for (ActionTestStep step : cov.getVerifyingSteps()) {
                    testStepsIds.add(step.getId());
                }
            }
        }
        // now retrieve a list of exec steps
        MultiMap executionsStatus =
                executionStepDao.findStepExecutionsStatus(mainVersionTCWithItpiIds, testStepsIds);
        for (Long testStepsId : testStepsIds) {
            // [Issue 6943] If the testStep has never been executed, then it will not be take into account
            // for the calculation.
            if (executionsStatus.containsKey(testStepsId)) {
                List<ExecutionStep> executionSteps =
                        (List<ExecutionStep>) executionsStatus.get(testStepsId);
                fillingResult(executionSteps, result);
            }
        }
        return result;
    }

    private void fillingResult(
            List<ExecutionStep> executionSteps, Map<ExecutionStatus, Long> result) {
        for (ExecutionStep executionStep : executionSteps) {
            // Here come horrible code to detect if ITPI was fast passed AFTER execution.
            // We have no attribute in model to help us, and no time to develop a proper solution.
            // So we'll use execution date on itpi and exec. If the delta between two date is superior to
            // 2 seconds,
            // we consider it's a fast pass
            Execution execution = executionStep.getExecution();
            IterationTestPlanItem itpi = execution.getTestPlan();
            Date itpiDateLastExecutedOn = itpi.getLastExecutedOn();
            Date execDateLastExecutedOn = execution.getLastExecutedOn();
            ExecutionStatus status = ExecutionStatus.READY;
            // if execution dates are null, the execution was only READY, so we don't compare dates to
            // avoid npe
            if (itpiDateLastExecutedOn != null && execDateLastExecutedOn != null) {
                DateTime itpiLastExecutedOn = new DateTime(itpi.getLastExecutedOn().getTime());
                DateTime execLastExecutedOn = new DateTime(execution.getLastExecutedOn().getTime());
                Interval interval = new Interval(execLastExecutedOn, itpiLastExecutedOn);
                boolean fastPass = interval.toDuration().isLongerThan(new Duration(2000L));
                // If we have a fast path use it for step status
                status = fastPass ? itpi.getExecutionStatus() : executionStep.getExecutionStatus();
            }
            Long memo = result.get(status);
            if (memo == null) {
                result.put(status, 1L);
            } else {
                result.put(status, memo + 1);
            }
        }
    }

    private Long calculateUntestedElementCount(
            List<Long> mainVersionTCWithoutItpiIds,
            Map<Long, Long> nbSimpleCoverageByTestCase,
            List<RequirementVersionCoverage> stepedCoverage,
            List<Long> steppedCoverageTCIdsWithoutITPI) {
        Long total = 0L;
        for (Long tcId : mainVersionTCWithoutItpiIds) {
            Long nbCovegrage = nbSimpleCoverageByTestCase.get(tcId);
            if (nbCovegrage != null && nbCovegrage != 0L) {
                total += nbCovegrage;
            }
        }
        for (Long tcId : steppedCoverageTCIdsWithoutITPI) {
            for (RequirementVersionCoverage cov : stepedCoverage) {
                if (cov.getVerifyingTestCase().getId().equals(tcId)) {
                    total += cov.getVerifyingSteps().size();
                }
            }
        }
        return total;
    }

    private double doRateVerifiedCalculation(
            Map<ExecutionStatus, Long> fullCoverageResult, Long untestedElementsCount) {
        Set<ExecutionStatus> statusSet = getVerifiedStatus();
        return doRateCalculation(statusSet, fullCoverageResult, untestedElementsCount);
    }

    private double doRateValidatedCalculation(Map<ExecutionStatus, Long> fullCoverageResult) {
        Set<ExecutionStatus> validStatusSet = getValidatedStatus();
        Set<ExecutionStatus> verifiedStatusSet = getVerifiedStatus();
        return doRateCalculation(validStatusSet, verifiedStatusSet, fullCoverageResult);
    }

    /**
     * Rate calculation for two status set. The count on the first one will be the numerator, the
     * count one second set will be the denominator
     *
     * @param numeratorStatus
     * @param fullCoverageResult
     * @return
     */
    private double doRateCalculation(
            Set<ExecutionStatus> numeratorStatus,
            Set<ExecutionStatus> denominatorStatus,
            Map<ExecutionStatus, Long> fullCoverageResult) {
        double numerator = countforStatus(fullCoverageResult, numeratorStatus);
        double denominator = countforStatus(fullCoverageResult, denominatorStatus);
        return numerator / denominator;
    }

    /**
     * Rate calculation with some untested elements
     *
     * @param statusSet
     * @param fullCoverageResult
     * @param untestedElementsCount
     * @return
     */
    private double doRateCalculation(
            Set<ExecutionStatus> statusSet,
            Map<ExecutionStatus, Long> fullCoverageResult,
            Long untestedElementsCount) {
        // Implicit conversion of all Long and Integer in floating point number to allow proper rate
        // operation
        double execWithRequiredStatus = countforStatus(fullCoverageResult, statusSet);
        double allExecutionCount = getCandidateExecCount(fullCoverageResult);
        double nbTCWithoutItpi = untestedElementsCount;
        return execWithRequiredStatus / (allExecutionCount + nbTCWithoutItpi);
    }

    private Long getCandidateExecCount(Map<ExecutionStatus, Long> fullCoverageResult) {
        Long nbStatus = 0L;
        for (Long countForOneStatus : fullCoverageResult.values()) {
            nbStatus += countForOneStatus;
        }
        return nbStatus;
    }

    private Long countforStatus(
            Map<ExecutionStatus, Long> fullCoverageResult, Set<ExecutionStatus> statusSet) {
        Long count = 0L;
        for (Entry<ExecutionStatus, Long> executionStatus : fullCoverageResult.entrySet()) {
            if (statusSet.contains(executionStatus.getKey())) {

                count += executionStatus.getValue();
            }
        }
        return count;
    }

    private Set<ExecutionStatus> getVerifiedStatus() {
        Set<ExecutionStatus> verifiedStatus = new HashSet<>();
        verifiedStatus.add(ExecutionStatus.SUCCESS);
        verifiedStatus.add(ExecutionStatus.SETTLED);
        verifiedStatus.add(ExecutionStatus.FAILURE);
        verifiedStatus.add(ExecutionStatus.BLOCKED);
        verifiedStatus.add(ExecutionStatus.UNTESTABLE);
        verifiedStatus.add(ExecutionStatus.SKIPPED);
        verifiedStatus.add(ExecutionStatus.CANCELLED);
        return verifiedStatus;
    }

    private Set<ExecutionStatus> getValidatedStatus() {
        Set<ExecutionStatus> verifiedStatus = new HashSet<>();
        verifiedStatus.add(ExecutionStatus.SUCCESS);
        verifiedStatus.add(ExecutionStatus.SETTLED);
        return verifiedStatus;
    }

    private Map<ExecutionStatus, Long> findResultsForSimpleCoverage(
            List<Long> testCaseIds, List<Long> iterationIds, Map<Long, Long> nbSimpleCoverageByTestCase) {
        List<TestCaseExecutionStatus> testCaseExecutionStatus =
                iterationDao.findExecStatusForIterationsAndTestCases(testCaseIds, iterationIds);
        Map<ExecutionStatus, Long> computedResults = new EnumMap<>(ExecutionStatus.class);
        for (TestCaseExecutionStatus oneTCES : testCaseExecutionStatus) {
            ExecutionStatus status = oneTCES.getStatus();
            Long nbCoverage = nbSimpleCoverageByTestCase.get(oneTCES.getTestCaseId());
            if (computedResults.containsKey(status)) {
                computedResults.put(status, computedResults.get(status) + nbCoverage);
            } else {
                computedResults.put(status, nbCoverage);
            }
        }
        return computedResults;
    }

    /**
     * Part the {@link RequirementVersionCoverage} list in two list : One with {@link
     * RequirementVersionCoverage} with linked test steps. One with {@link RequirementVersionCoverage}
     * without linked test steps. This is necessary as {@link RequirementVersionCoverage} with {@link
     * TestStep} linked must be treated at step level
     *
     * @param requirementVersionCoverages
     * @param simpleCoverage
     * @param stepedCoverage
     * @param nbSimpleCoverageByTestCase
     */
    private void partRequirementVersionCoverage(
            Set<RequirementVersionCoverage> requirementVersionCoverages,
            List<RequirementVersionCoverage> simpleCoverage,
            List<RequirementVersionCoverage> stepedCoverage,
            Map<Long, Long> nbSimpleCoverageByTestCase,
            Map<Long, Long> nbSteppedCoverageByTestCase) {
        for (RequirementVersionCoverage requirementVersionCoverage : requirementVersionCoverages) {
            Long tcId = requirementVersionCoverage.getVerifyingTestCase().getId();
            if (requirementVersionCoverage.hasSteps()) {
                stepedCoverage.add(requirementVersionCoverage);
                if (nbSteppedCoverageByTestCase.containsKey(tcId)) {
                    nbSteppedCoverageByTestCase.put(tcId, nbSteppedCoverageByTestCase.get(tcId) + 1);
                } else {
                    nbSteppedCoverageByTestCase.put(tcId, 1L);
                }
            } else {
                simpleCoverage.add(requirementVersionCoverage);
                if (nbSimpleCoverageByTestCase.containsKey(tcId)) {
                    nbSimpleCoverageByTestCase.put(tcId, nbSimpleCoverageByTestCase.get(tcId) + 1);
                } else {
                    nbSimpleCoverageByTestCase.put(tcId, 1L);
                }
            }
        }
    }

    private void findCoverageRate(
            RequirementVersion mainVersion,
            List<RequirementVersion> descendants,
            RequirementCoverageStat stats) {

        Rate coverageRate = new Rate();
        boolean hasValidDescendant = !descendants.isEmpty();
        coverageRate.setRequirementVersionRate(calculateCoverageRate(mainVersion));

        if (hasValidDescendant) {
            coverageRate.setRequirementVersionChildrenRate(calculateCoverageRate(descendants));
            List<RequirementVersion> all = getAllRequirementVersion(mainVersion, descendants);
            coverageRate.setRequirementVersionGlobalRate(calculateCoverageRate(all));
            coverageRate.setAncestor(true);
        }
        stats.addRate("coverage", coverageRate);
        stats.setAncestor(hasValidDescendant);
    }

    private List<RequirementVersion> getAllRequirementVersion(
            RequirementVersion mainVersion, List<RequirementVersion> descendants) {
        List<RequirementVersion> all = new ArrayList<>();
        all.add(mainVersion);
        all.addAll(descendants);
        return all;
    }

    private double calculateCoverageRate(List<RequirementVersion> rvs) {
        double total = 0;
        double size = rvs.size();
        for (RequirementVersion rv : rvs) {
            total += calculateCoverageRate(rv);
        }
        return total / size;
    }

    /**
     * Coverage Rate is 100% for 1+ {@link TestCase} linked to this {@link RequirementVersion}. 0% if
     * no link
     *
     * @param mainVersion
     * @return
     */
    private Long calculateCoverageRate(RequirementVersion mainVersion) {
        if (!mainVersion.getRequirementVersionCoverages().isEmpty()) {
            return 1L;
        }
        return 0L;
    }

    private List<RequirementVersion> findValidDescendants(Requirement requirement) {
        List<Long> candidatesIds =
                requirementDao.findDescendantRequirementIds(Arrays.asList(requirement.getId()));
        List<Requirement> candidates = requirementDao.findAllByIds(candidatesIds);
        return extractCurrentVersions(candidates);
    }

    private List<RequirementVersion> extractCurrentVersions(List<Requirement> requirements) {

        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();

        List<RequirementVersion> rvs = new ArrayList<>(requirements.size());
        for (Requirement requirement : requirements) {
            RequirementVersion rv = requirement.getResource();

            if (rv.isNotObsolete()
                    && (!activeMilestone.isPresent() || rv.getMilestones().contains(activeMilestone.get()))) {
                rvs.add(rv);
            }
        }
        return rvs;
    }
}
