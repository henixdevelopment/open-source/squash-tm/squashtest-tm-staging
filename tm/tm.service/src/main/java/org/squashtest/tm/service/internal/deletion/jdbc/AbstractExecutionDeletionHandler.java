/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_TAG;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_RENDERING_LOCATION;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION_EVENT;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE_LIST;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;

import javax.persistence.EntityManager;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record3;
import org.jooq.SelectOrderByStep;
import org.jooq.Table;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

public abstract class AbstractExecutionDeletionHandler extends AbstractJdbcDeletionHandler {
    protected AbstractExecutionDeletionHandler(
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper,
            String operationId) {
        super(dslContext, attachmentRepository, reorderHelper, operationId, entityManager);
    }

    protected abstract Condition getPredicate();

    protected abstract Table<Record> joinToExecution();

    private Table<Record> joinToAutomatedExecutionExtender() {
        return joinToExecution()
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID));
    }

    private Table<Record> joinToAutomatedExecutionExtenderFailureDetail() {
        return joinToExecution()
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .innerJoin(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                .on(
                        AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID.eq(
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID));
    }

    private Table<Record> joinToExecutionDfv() {
        return joinToExecution()
                .innerJoin(DENORMALIZED_FIELD_VALUE)
                .on(
                        DENORMALIZED_FIELD_VALUE
                                .DENORMALIZED_FIELD_HOLDER_ID
                                .eq(Tables.EXECUTION.EXECUTION_ID)
                                .and(
                                        DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(
                                                DenormalizedFieldHolderType.EXECUTION.name())));
    }

    private Table<Record> joinToExecutionStep() {
        return joinToExecution()
                .innerJoin(EXECUTION_EXECUTION_STEPS)
                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(Tables.EXECUTION.EXECUTION_ID))
                .innerJoin(EXECUTION_STEP)
                .on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID));
    }

    private Table<Record> joinToExecutionStepDfv() {
        return joinToExecutionStep()
                .innerJoin(DENORMALIZED_FIELD_VALUE)
                .on(
                        DENORMALIZED_FIELD_VALUE
                                .DENORMALIZED_FIELD_HOLDER_ID
                                .eq(EXECUTION_STEP.EXECUTION_STEP_ID)
                                .and(
                                        DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(
                                                DenormalizedFieldHolderType.EXECUTION_STEP.name())));
    }

    private Table<Record> joinToDenormalizedEnvironmentVariable() {
        return joinToAutomatedExecutionExtender()
                .innerJoin(DENORMALIZED_ENVIRONMENT_VARIABLE)
                .on(
                        DENORMALIZED_ENVIRONMENT_VARIABLE.HOLDER_ID.eq(
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID));
    }

    private Table<Record> joinToDenormalizedEnvironmentTag() {
        return joinToAutomatedExecutionExtender()
                .innerJoin(DENORMALIZED_ENVIRONMENT_TAG)
                .on(DENORMALIZED_ENVIRONMENT_TAG.HOLDER_ID.eq(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID));
    }

    protected SelectOrderByStep<Record3<Long, String, String>> selectExecutionCustomFieldValues() {
        return makeSelectCustomFieldValues(
                        EXECUTION_STEP.EXECUTION_STEP_ID, BindableEntity.EXECUTION_STEP)
                .union(makeSelectCustomFieldValues(EXECUTION.EXECUTION_ID, BindableEntity.EXECUTION));
    }

    protected SelectOrderByStep<Record3<Long, String, String>> selectExecutionAttachmentLists() {
        return makeSelectAttachmentList(
                        EXECUTION_STEP.EXECUTION_STEP_ID, EXECUTION_STEP.ATTACHMENT_LIST_ID)
                .union(makeSelectAttachmentList(EXECUTION.EXECUTION_ID, EXECUTION.ATTACHMENT_LIST_ID));
    }

    protected void storeExecutionsToDeleteIntoWorkingTable() {
        addDenormalizedCustomFieldValues();
        addDenormalizedEnvironmentVariables();
        addDenormalizedEnvironmentTags();
        addExecutionSteps();
        addIssueLists();
        addExecutions();
        addAutomatedExecutionExtenderFailureDetails();
    }

    private void addDenormalizedCustomFieldValues() {
        workingTables.addEntity(
                DENORMALIZED_FIELD_VALUE.DFV_ID,
                () ->
                        makeSelectClause(DENORMALIZED_FIELD_VALUE.DFV_ID)
                                .from(joinToExecutionStepDfv())
                                .where(getPredicate())
                                .union(
                                        makeSelectClause(DENORMALIZED_FIELD_VALUE.DFV_ID)
                                                .from(joinToExecutionDfv())
                                                .where(getPredicate())));
    }

    private void addDenormalizedEnvironmentVariables() {
        workingTables.addEntity(
                DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID,
                () ->
                        makeSelectClause(DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID)
                                .from(joinToDenormalizedEnvironmentVariable())
                                .where(getPredicate()));
    }

    private void addDenormalizedEnvironmentTags() {
        workingTables.addEntity(
                DENORMALIZED_ENVIRONMENT_TAG.DET_ID,
                () ->
                        makeSelectClause(DENORMALIZED_ENVIRONMENT_TAG.DET_ID)
                                .from(joinToDenormalizedEnvironmentTag())
                                .where(getPredicate()));
    }

    private void addExecutionSteps() {
        workingTables.addEntity(
                EXECUTION_STEP.EXECUTION_STEP_ID,
                () ->
                        makeSelectClause(EXECUTION_STEP.EXECUTION_STEP_ID)
                                .from(joinToExecutionStep())
                                .where(getPredicate()));
    }

    private void addExecutions() {
        workingTables.addEntity(
                EXECUTION.EXECUTION_ID,
                () ->
                        makeSelectClause(EXECUTION.EXECUTION_ID).from(joinToExecution()).where(getPredicate()));
    }

    private void addAutomatedExecutionExtenderFailureDetails() {
        workingTables.addEntity(
                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID,
                () ->
                        makeSelectClause(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID)
                                .from(joinToAutomatedExecutionExtenderFailureDetail())
                                .where(getPredicate()));
    }

    private void addIssueLists() {
        workingTables.addEntity(
                ISSUE_LIST.ISSUE_LIST_ID,
                () ->
                        makeSelectClause(ISSUE_LIST.ISSUE_LIST_ID)
                                .from(ISSUE_LIST)
                                .innerJoin(joinToExecution())
                                .on(EXECUTION.ISSUE_LIST_ID.eq(ISSUE_LIST.ISSUE_LIST_ID))
                                .where(getPredicate())
                                .union(
                                        makeSelectClause(ISSUE_LIST.ISSUE_LIST_ID)
                                                .from(ISSUE_LIST)
                                                .innerJoin(joinToExecutionStep())
                                                .on(EXECUTION_STEP.ISSUE_LIST_ID.eq(ISSUE_LIST.ISSUE_LIST_ID))
                                                .where(getPredicate())));
    }

    protected void performExecutionDeletions() {
        deleteAutomatedExecutionFailureDetails();
        deleteDenormalizedCustomFieldValues();
        deleteDenormalizedEnvironmentVariables();
        deleteDenormalizedEnvironmentTags();
        deleteExecutionSteps();
        deleteKeywordExecution();
        deleteExecutions();
        deleteIssueLists();
    }

    private void deleteAutomatedExecutionFailureDetails() {
        workingTables.delete(
                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID,
                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID);
    }

    private void deleteDenormalizedCustomFieldValues() {
        workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_VALUE_OPTION.DFV_ID);
        workingTables.delete(
                DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_RENDERING_LOCATION.DFV_ID);
        workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_VALUE.DFV_ID);
        logDelete(DENORMALIZED_FIELD_VALUE);
    }

    private void deleteDenormalizedEnvironmentVariables() {
        workingTables.delete(
                DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID, DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID);
        logDelete(DENORMALIZED_ENVIRONMENT_VARIABLE);
    }

    private void deleteDenormalizedEnvironmentTags() {
        workingTables.delete(DENORMALIZED_ENVIRONMENT_TAG.DET_ID, DENORMALIZED_ENVIRONMENT_TAG.DET_ID);
        logDelete(DENORMALIZED_ENVIRONMENT_TAG);
    }

    private void deleteExecutionSteps() {
        workingTables.delete(EXECUTION.EXECUTION_ID, EXECUTION_EXECUTION_STEPS.EXECUTION_ID);
        workingTables.delete(EXECUTION_STEP.EXECUTION_STEP_ID, EXECUTION_STEP.EXECUTION_STEP_ID);
        logDelete(EXECUTION_STEP);
    }

    private void deleteKeywordExecution() {
        workingTables.delete(EXECUTION.EXECUTION_ID, KEYWORD_EXECUTION.EXECUTION_ID);
        logDelete(KEYWORD_EXECUTION);
    }

    private void deleteExecutions() {
        workingTables.delete(EXECUTION.EXECUTION_ID, AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, SCRIPTED_EXECUTION.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, SESSION_NOTE.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, EXPLORATORY_EXECUTION_EVENT.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, EXPLORATORY_EXECUTION.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, EXECUTION.EXECUTION_ID);
        logDelete(EXECUTION);
    }

    private void deleteIssueLists() {
        workingTables.delete(ISSUE_LIST.ISSUE_LIST_ID, ISSUE.ISSUE_LIST_ID);
        workingTables.delete(ISSUE_LIST.ISSUE_LIST_ID, ISSUE_LIST.ISSUE_LIST_ID);
        logDelete(ISSUE_LIST);
    }
}
