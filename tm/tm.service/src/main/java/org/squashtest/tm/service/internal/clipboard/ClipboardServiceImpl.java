/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.clipboard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.Referenceable;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.domain.library.TreeNode;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.service.clipboard.ClipboardService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;

@Service
public class ClipboardServiceImpl implements ClipboardService {

    @PersistenceContext private EntityManager entityManager;

    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    public ClipboardServiceImpl(CustomReportLibraryNodeService customReportLibraryNodeService) {
        this.customReportLibraryNodeService = customReportLibraryNodeService;
    }

    public List<NodeReference> copyNodes(@RequestBody() List<NodeReference> sourceNodeList) {
        List<NodeReference> childrenList = new ArrayList<>();
        sourceNodeList.forEach(nodeReference -> extractContent(nodeReference, childrenList));
        return childrenList;
    }

    private void extractContent(NodeReference nodeReference, List<NodeReference> childrenList) {
        switch (nodeReference.getNodeType()) {
            case CAMPAIGN_FOLDER -> {
                CampaignFolder campaignFolder =
                        entityManager.find(CampaignFolder.class, nodeReference.getId());
                processChildren(campaignFolder, childrenList);
            }
            case CAMPAIGN -> {
                Campaign campaign = entityManager.find(Campaign.class, nodeReference.getId());
                processChildren(campaign, childrenList);
            }
            case SPRINT_GROUP -> {
                SprintGroup sprintGroup = entityManager.find(SprintGroup.class, nodeReference.getId());
                processChildren(sprintGroup, childrenList);
            }
            case ITERATION -> {
                Iteration iteration = entityManager.find(Iteration.class, nodeReference.getId());
                processChildren(iteration, childrenList);
            }
            case TEST_CASE_FOLDER -> {
                TestCaseFolder testCaseFolder =
                        entityManager.find(TestCaseFolder.class, nodeReference.getId());
                processChildren(testCaseFolder, childrenList);
            }
            case REQUIREMENT_FOLDER -> {
                RequirementFolder requirementFolder =
                        entityManager.find(RequirementFolder.class, nodeReference.getId());
                processChildren(requirementFolder, childrenList);
            }
            case REQUIREMENT -> {
                Requirement requirement = entityManager.find(Requirement.class, nodeReference.getId());
                processChildren(requirement, childrenList);
            }
            case HIGH_LEVEL_REQUIREMENT -> {
                HighLevelRequirement highLevelRequirement =
                        entityManager.find(HighLevelRequirement.class, nodeReference.getId());
                processChildren(highLevelRequirement, childrenList);
            }
            case CUSTOM_REPORT_FOLDER -> {
                processCustomReportLibraryNode(nodeReference, childrenList);
            }
            case TEST_SUITE,
                    TEST_CASE,
                    CUSTOM_REPORT_CUSTOM_EXPORT,
                    REPORT_DEFINITION,
                    CHART_DEFINITION,
                    CUSTOM_REPORT_DASHBOARD,
                    ACTION_WORD,
                    SPRINT -> {
                // NOOP: this element is a tree leaf
            }
            default ->
                    throw new IllegalArgumentException(
                            String.format("This type '%s' is not handled", nodeReference.getNodeType()));
        }
    }

    private void processCustomReportLibraryNode(
            NodeReference nodeReference, List<NodeReference> childrenList) {
        CustomReportLibraryNode customReportLibraryNode =
                entityManager.find(CustomReportLibraryNode.class, nodeReference.getId());
        List<Long> nodeChildren =
                customReportLibraryNodeService.findAllFirstLevelDescendantIds(
                        Collections.singletonList(customReportLibraryNode.getId()));

        for (Long nodeChild : nodeChildren) {
            // We do not care about the node reference for this workspace,
            // but we have to provide one to stay consistent with the other cases,
            // so we consider every node to be a folder.
            NodeReference childNodeReference =
                    new NodeReference(NodeType.CUSTOM_REPORT_FOLDER, nodeChild);
            childrenList.add(childNodeReference);
            extractContent(childNodeReference, childrenList);
        }
    }

    private <NODE extends TreeNode> void processChildren(
            NodeContainer<NODE> nodeContainer, List<NodeReference> childrenList) {
        Collection<NODE> nodeChildren = nodeContainer.getOrderedContent();
        for (NODE nodeChild : nodeChildren) {
            EntityReference ref = ((Referenceable) nodeChild).toEntityReference();
            NodeReference childNodeReference = ref.toNodeReference();
            childrenList.add(childNodeReference);
            extractContent(childNodeReference, childrenList);
        }
    }
}
