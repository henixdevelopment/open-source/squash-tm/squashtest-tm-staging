/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ActionStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.CallStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetParamValueInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.LinkedLowLevelRequirementInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementLinkInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;

/**
 * @Implementation of batch import method that won't update the database. It just updates an
 * internal model (held by a ValidationFacility) when it sees fit, depending on the sanity of the
 * data and the operation being simulated.
 */
@Component
@Scope("prototype")
public class SimulationFacility implements Facility {

    private final ValidationFacility validator;

    public SimulationFacility(ValidationFacility validator) {
        this.validator = validator;
    }

    // TODO : Use of TestCaseNodeDeletionHandler#simulate ?
    @Override
    public LogTrain deleteTestCase(TestCaseTarget target) {

        LogTrain logs = validator.deleteTestCase(target);

        // if no fatal error, update the model
        if (!logs.hasCriticalErrors()) {
            validator.getModel().setToBeDeleted(target);
        }

        return logs;
    }

    @Override
    public LogTrain updateActionStep(
            TestStepTarget target, ActionTestStep testStep, Map<String, String> cufValues) {

        // no need to update the model

        return validator.updateActionStep(target, testStep, cufValues);
    }

    @Override
    public LogTrain updateCallStep(
            TestStepTarget target,
            CallTestStep testStep,
            TestCaseTarget calledTestCase,
            CallStepParamsInfo paramInfo,
            ActionTestStep actionStepBackup) {

        LogTrain logs =
                validator.updateCallStep(target, testStep, calledTestCase, paramInfo, actionStepBackup);

        // if all is ok, update the target of this call step then return
        if (!logs.hasCriticalErrors()) {
            validator.getModel().updateCallStepTarget(target, calledTestCase, paramInfo);
        }

        return logs;
    }

    @Override
    public LogTrain deleteTestStep(TestStepTarget target) {

        LogTrain logs = validator.deleteTestStep(target);

        // if all went well, we can remove that step from the model
        if (!logs.hasCriticalErrors()) {
            validator.getModel().remove(target);
        }

        return logs;
    }

    @Override
    public LogTrain updateParameter(ParameterTarget target, Parameter param) {

        LogTrain logs = validator.updateParameter(target, param);

        // if the parameter didn't exist, it is created on the fly
        if (!logs.hasCriticalErrors()) {
            validator.getModel().addParameter(target);
        }

        return logs;
    }

    @Override
    public LogTrain deleteParameter(ParameterTarget target) {

        LogTrain logs = validator.deleteParameter(target);

        // if all is ok let's proceed
        if (!logs.hasCriticalErrors()) {
            validator.getModel().removeParameter(target);
        }

        return logs;
    }

    @Override
    public LogTrain failsafeUpdateParameterValue(
            DatasetTarget dataset, ParameterTarget param, String value, boolean isUpdate) {

        LogTrain logs = validator.failsafeUpdateParameterValue(dataset, param, value, isUpdate);

        if (!logs.hasCriticalErrors()) {
            // when a parameter is created or updated, the dataset must be created on the fly
            // note that this operation can be invoked multiple times, a given dataset will be created
            // only once
            validator.getModel().addDataset(dataset);
        }

        return logs;
    }

    @Override
    public LogTrain deleteDataset(DatasetTarget dataset) {

        LogTrain logs = validator.deleteDataset(dataset);

        // 4 - if ok, update the model
        if (!logs.hasCriticalErrors()) {
            validator.getModel().removeDataset(dataset);
        }

        return logs;
    }

    /**
     * @see
     *     org.squashtest.tm.service.internal.batchimport.Facility#updateTestCase(TestCaseInstruction)
     */
    @Override
    public LogTrain updateTestCase(TestCaseInstruction instr) {

        /*
         * In case of an update, we don't need to change the model regardless of the success or failure of the
         * operation.
         */

        return validator.updateTestCase(instr);
    }

    @Override
    public void updateRequirementVersions(
            List<RequirementVersionInstruction> updateInstructions, Project project) {
        validator.updateRequirementVersions(updateInstructions, project);
    }

    @Override
    public void createCoverages(List<CoverageInstruction> coverageInstructions, Project project) {
        validator.createCoverages(coverageInstructions, project);
    }

    @Override
    public void createOrUpdateRequirementLinks(
            List<RequirementLinkInstruction> requirementLinkInstructions, Project project) {
        validator.createOrUpdateRequirementLinks(requirementLinkInstructions, project);
    }

    @Override
    public LogTrain deleteRequirementVersion(RequirementVersionInstruction instr) {
        throw new NotImplementedException(
                "implement me - must return a Failure : Not implemented in the log train instead of throwing this exception");
    }

    @Override
    public LogTrain createLinkedLowLevelRequirement(LinkedLowLevelRequirementInstruction instr) {
        return validator.createLinkedLowLevelRequirement(instr);
    }

    @Override
    public LogTrain deleteRequirementLink(RequirementLinkInstruction instr) {
        return validator.deleteRequirementLink(instr);
    }

    // --------------------- Batch operations ---------------------

    @Override
    public void createTestCases(List<TestCaseInstruction> instructions, Project project) {

        validator.createTestCases(instructions, project);

        for (var instruction : instructions) {
            TestCaseTarget target = instruction.getTarget();

            if (!instruction.hasCriticalErrors()) {
                validator.getModel().setToBeCreated(target);
            } else {
                validator.getModel().setNotExists(target);
            }
        }
    }

    @Override
    public void createParameters(List<ParameterInstruction> instructions, Project project) {
        validator.createParameters(instructions, project);

        instructions.stream()
                .filter(i -> !i.hasCriticalErrors())
                .forEach(i -> validator.getModel().addParameter(i.getTarget()));
    }

    @Override
    public void createDatasets(List<DatasetInstruction> instructions, Project project) {
        validator.createDatasets(instructions, project);

        instructions.stream()
                .filter(i -> !i.hasCriticalErrors())
                .forEach(i -> validator.getModel().addDataset(i.getTarget()));
    }

    @Override
    public void addActionSteps(List<ActionStepInstruction> instructions, Project project) {
        validator.addActionSteps(instructions, project);

        instructions.stream()
                .filter(i -> !i.hasCriticalErrors())
                .forEach(i -> validator.getModel().addActionStep(i.getTarget()));
    }

    @Override
    public void addCallSteps(List<CallStepInstruction> instructions, Project project) {
        validator.addCallSteps(instructions, project);

        instructions.stream()
                .filter(i -> !i.hasCriticalErrors())
                .forEach(
                        i -> {
                            if (FacilityUtils.mustImportCallAsActionStep(i.getLogTrain()) == null) {
                                validator
                                        .getModel()
                                        .addCallStep(i.getTarget(), i.getCalledTC(), i.getDatasetInfo().getParamMode());
                            } else {
                                validator.getModel().addActionStep(i.getTarget());
                            }
                        });
    }

    @Override
    public void addDatasetParametersValues(
            List<DatasetParamValueInstruction> instructions, Project project) {
        validator.addDatasetParametersValues(instructions, project);

        instructions.stream()
                .filter(i -> !i.hasCriticalErrors())
                .forEach(i -> validator.getModel().addDataset(i.getTarget()));
    }

    @Override
    public void createRequirementVersions(
            List<RequirementVersionInstruction> requirementVersionInstructions, Project project) {
        validator.createRequirementVersions(requirementVersionInstructions, project);
    }
}
