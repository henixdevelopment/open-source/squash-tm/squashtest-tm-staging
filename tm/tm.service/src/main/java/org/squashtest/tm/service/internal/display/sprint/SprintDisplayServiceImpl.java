/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.sprint;

import static org.jooq.impl.DSL.selectCount;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CAMPAIGN_LIBRARY_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CATEGORY_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CATEGORY_LABEL;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CRITICALITY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DESCRIPTION;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NB_EXECUTIONS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NB_TESTS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REMOTE_REQ_STATUS_AND_STATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUIREMENT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUIREMENT_VERSION_PROJECT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUIREMENT_VERSION_PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUIREMENT_VERSION_PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SPRINT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SPRINT_REQ_VERSION_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SPRINT_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.VERSION_ID;
import static org.squashtest.tm.service.security.Authorizations.READ_SPRINT_OR_ADMIN;

import java.util.Collections;
import java.util.List;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.requirement.ManagementMode;
import org.squashtest.tm.domain.synchronisation.SynchronisationKind;
import org.squashtest.tm.jooq.domain.tables.SprintReqVersion;
import org.squashtest.tm.service.campaign.SprintTestPlanItemManagerService;
import org.squashtest.tm.service.display.sprint.SprintDisplayService;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.SprintKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.SprintReqVersionKnownIssueFinder;
import org.squashtest.tm.service.internal.display.campaign.ReadUnassignedTestPlanHelper;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.sprint.AvailableTestPlanItemDto;
import org.squashtest.tm.service.internal.display.dto.sprint.AvailableTestPlanItemIdsDto;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintDto;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionDto;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionView;
import org.squashtest.tm.service.internal.library.EntityPathHeaderService;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

@Service
@Transactional(readOnly = true)
public class SprintDisplayServiceImpl implements SprintDisplayService {

    private final SprintDisplayDao sprintDisplayDao;
    private final AttachmentDisplayDao attachmentDisplayDao;
    private final EntityPathHeaderService entityPathHeaderService;
    private final DSLContext dslContext;
    private final PermissionEvaluationService permissionEvaluationService;
    private final SprintReqVersionKnownIssueFinder sprintReqVersionKnownIssueFinder;
    private final SprintKnownIssueFinder sprintKnownIssueFinder;
    private final SprintTestPlanItemManagerService sprintTestPlanItemManagerService;
    private final ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper;

    public SprintDisplayServiceImpl(
            SprintDisplayDao sprintDisplayDao,
            AttachmentDisplayDao attachmentDisplayDao,
            EntityPathHeaderService entityPathHeaderService,
            DSLContext dslContext,
            PermissionEvaluationService permissionEvaluationService,
            SprintReqVersionKnownIssueFinder sprintReqVersionKnownIssueFinder,
            SprintKnownIssueFinder sprintKnownIssueFinder,
            SprintTestPlanItemManagerService sprintTestPlanItemManagerService,
            ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper) {
        this.sprintDisplayDao = sprintDisplayDao;
        this.entityPathHeaderService = entityPathHeaderService;
        this.attachmentDisplayDao = attachmentDisplayDao;
        this.dslContext = dslContext;
        this.permissionEvaluationService = permissionEvaluationService;
        this.sprintReqVersionKnownIssueFinder = sprintReqVersionKnownIssueFinder;
        this.sprintKnownIssueFinder = sprintKnownIssueFinder;
        this.sprintTestPlanItemManagerService = sprintTestPlanItemManagerService;
        this.readUnassignedTestPlanHelper = readUnassignedTestPlanHelper;
    }

    /**
     * Retrieves all the information of a sprint.
     *
     * @param sprintId : the id of the sprint whose information is to be retrieved
     * @return SprintDto
     */
    @Override
    @PreAuthorize(READ_SPRINT_OR_ADMIN)
    public SprintDto getSprintView(long sprintId) {
        SprintDto sprintDto = sprintDisplayDao.getSprintDtoById(sprintId);
        if (SynchronisationKind.XSQUASH4JIRA.getName().equals(sprintDto.getSynchronisationKind())) {
            translateJiraRemoteStateIntoSquashStatus(sprintDto);
        }
        AttachmentListDto attachmentList =
                attachmentDisplayDao.findAttachmentListById(sprintDto.getAttachmentListId());
        sprintDto.setAttachmentList(attachmentList);
        String path = entityPathHeaderService.buildCLNPathHeader(sprintId);
        sprintDto.setPath(path);
        List<SprintReqVersionDto> sprintReqVersions = findSprintReqVersionDtosBySprintId(sprintId);
        sprintDto.setSprintReqVersions(sprintReqVersions);
        int nbIssues = sprintKnownIssueFinder.countKnownIssues(sprintId);
        sprintDto.setNbIssues(nbIssues);
        sprintDto.setNbTestPlanItems(countTestPlanItems(sprintId));
        sprintDto.setAssignableUsers(
                UserView.fromEntities(
                        sprintTestPlanItemManagerService.findAssignableUsersByCampaignLibraryId(
                                sprintDto.getCampaignLibraryId())));
        return sprintDto;
    }

    private void translateJiraRemoteStateIntoSquashStatus(SprintDto sprintDto) {
        SprintStatus remoteState = sprintDto.getRemoteState();
        if (remoteState == SprintStatus.FUTURE) {
            sprintDto.setRemoteState(SprintStatus.UPCOMING);
        } else if (remoteState == SprintStatus.ACTIVE) {
            sprintDto.setRemoteState(SprintStatus.OPEN);
        }
    }

    @Override
    @PreAuthorize(READ_SPRINT_OR_ADMIN)
    public List<SprintReqVersionDto> findSprintReqVersionDtosBySprintId(long sprintId) {
        Table<?> requirementVersionProject = PROJECT.as(REQUIREMENT_VERSION_PROJECT);
        Field reqVersionProjectId = requirementVersionProject.field(PROJECT_ID);
        Field reqVersionProjectName = requirementVersionProject.field(NAME);

        SprintReqVersion sprintReqVersion = SPRINT_REQ_VERSION.as(SPRINT_REQ_VERSION_ALIAS);

        return dslContext
                .select(
                        SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.as(ID),
                        PROJECT.NAME.as(PROJECT_NAME),
                        PROJECT.PROJECT_ID,
                        reqVersionProjectId.as(REQUIREMENT_VERSION_PROJECT_ID),
                        reqVersionProjectName.as(REQUIREMENT_VERSION_PROJECT_NAME),
                        getReferenceBasedOnReqVersionId().as(REFERENCE),
                        getNameBasedOnReqVersionId().as(NAME),
                        getCategoryBasedOnReqVersionId().as(CATEGORY_ID),
                        getCategoryLabelIfSprintNotClosed().as(CATEGORY_LABEL),
                        getDescriptionBasedOnReqVersionId().as(DESCRIPTION),
                        getCriticalityBasedOnReqVersionId().as(CRITICALITY),
                        getStatusBasedOnReqVersionId().as(STATUS),
                        REQUIREMENT_VERSION.RES_ID.as(VERSION_ID),
                        REQUIREMENT_VERSION.REQUIREMENT_ID.as(REQUIREMENT_ID),
                        SPRINT_REQ_VERSION.SPRINT_ID,
                        SPRINT_REQ_VERSION.MODE,
                        SPRINT_REQ_VERSION.VALIDATION_STATUS,
                        SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_PERIMETER_STATUS,
                        SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_URL,
                        getRemoteReqStatusAndState().as(REMOTE_REQ_STATUS_AND_STATE),
                        selectCount()
                                .from(EXECUTION)
                                .join(TEST_PLAN_ITEM)
                                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                                .join(sprintReqVersion)
                                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(sprintReqVersion.TEST_PLAN_ID))
                                .where(
                                        sprintReqVersion.SPRINT_REQ_VERSION_ID.eq(
                                                SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID))
                                .asField(NB_EXECUTIONS),
                        selectCount()
                                .from(TEST_PLAN_ITEM)
                                .join(TEST_PLAN)
                                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(TEST_PLAN.TEST_PLAN_ID))
                                .join(sprintReqVersion)
                                .on(TEST_PLAN.TEST_PLAN_ID.eq(sprintReqVersion.TEST_PLAN_ID))
                                .where(
                                        sprintReqVersion.SPRINT_REQ_VERSION_ID.eq(
                                                SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID))
                                .asField(NB_TESTS))
                .from(SPRINT_REQ_VERSION)
                .leftJoin(SPRINT_REQUIREMENT_SYNC_EXTENDER)
                .on(
                        SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(
                                SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_VERSION_ID))
                .leftJoin(REQUIREMENT_VERSION)
                .on(REQUIREMENT_VERSION.RES_ID.eq(SPRINT_REQ_VERSION.REQ_VERSION_ID))
                .leftJoin(RESOURCE)
                .on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID))
                .leftJoin(CAMPAIGN_LIBRARY_NODE)
                .on(SPRINT_REQ_VERSION.SPRINT_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .leftJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftJoin(REQUIREMENT_LIBRARY_NODE)
                .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                .leftJoin(requirementVersionProject)
                .on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(reqVersionProjectId))
                .leftJoin(SPRINT)
                .on(SPRINT_REQ_VERSION.SPRINT_ID.eq(SPRINT.CLN_ID))
                .where(SPRINT_REQ_VERSION.SPRINT_ID.eq(sprintId))
                .orderBy(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID)
                .fetchInto(SprintReqVersionDto.class);
    }

    private int countTestPlanItems(long sprintId) {
        String userToRestrictTo =
                readUnassignedTestPlanHelper.getUserToRestrictTo(sprintId, Sprint.class.getName());
        return sprintDisplayDao.countTestPlanItems(sprintId, userToRestrictTo);
    }

    @Override
    public List<SprintReqVersionDto> findSprintReqVersionDtosForDenormalization(long sprintId) {
        return dslContext
                .select(
                        SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.as(ID),
                        REQUIREMENT_VERSION.REFERENCE.as(REFERENCE),
                        RESOURCE.NAME.as(NAME),
                        REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS),
                        REQUIREMENT_VERSION.CRITICALITY.as(CRITICALITY),
                        getCategoryLabelforDenormalization().as(CATEGORY_LABEL),
                        RESOURCE.DESCRIPTION.as(DESCRIPTION))
                .from(SPRINT_REQ_VERSION)
                .leftJoin(SPRINT_REQUIREMENT_SYNC_EXTENDER)
                .on(
                        SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(
                                SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_VERSION_ID))
                .leftJoin(REQUIREMENT_VERSION)
                .on(REQUIREMENT_VERSION.RES_ID.eq(SPRINT_REQ_VERSION.REQ_VERSION_ID))
                .leftJoin(RESOURCE)
                .on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID))
                .where(SPRINT_REQ_VERSION.SPRINT_ID.eq(sprintId))
                .and(SPRINT_REQ_VERSION.REQ_VERSION_ID.isNotNull())
                .and(SPRINT_REQ_VERSION.MODE.eq(ManagementMode.NATIVE.toString()))
                .fetchInto(SprintReqVersionDto.class);
    }

    private Field<String> getCategoryLabelforDenormalization() {
        return dslContext
                .select(INFO_LIST_ITEM.LABEL)
                .from(INFO_LIST_ITEM)
                .where(INFO_LIST_ITEM.ITEM_ID.eq(dslContext.select(REQUIREMENT_VERSION.CATEGORY)))
                .asField();
    }

    private Field<String> getCategoryLabelIfSprintNotClosed() {
        return when(shouldNotDisplayReqVersionData(), SPRINT_REQ_VERSION.CATEGORY);
    }

    private SelectField<Long> getCategoryBasedOnReqVersionId() {
        return when(DSL.not(shouldNotDisplayReqVersionData()), REQUIREMENT_VERSION.CATEGORY);
    }

    private Field<String> getReferenceBasedOnReqVersionId() {
        return when(shouldNotDisplayReqVersionData(), SPRINT_REQ_VERSION.REFERENCE)
                .otherwise(REQUIREMENT_VERSION.REFERENCE);
    }

    private Field<String> getNameBasedOnReqVersionId() {
        return when(shouldNotDisplayReqVersionData(), SPRINT_REQ_VERSION.NAME).otherwise(RESOURCE.NAME);
    }

    private Field<String> getCriticalityBasedOnReqVersionId() {
        return when(shouldNotDisplayReqVersionData(), SPRINT_REQ_VERSION.CRITICALITY)
                .otherwise(REQUIREMENT_VERSION.CRITICALITY);
    }

    private Field<String> getStatusBasedOnReqVersionId() {
        return when(shouldNotDisplayReqVersionData(), SPRINT_REQ_VERSION.STATUS)
                .otherwise(REQUIREMENT_VERSION.REQUIREMENT_STATUS);
    }

    private Field<String> getRemoteReqStatusAndState() {
        return DSL.when(
                        SPRINT_REQ_VERSION.STATUS.isNull().or(SPRINT_REQ_VERSION.STATUS.eq("")),
                        SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_STATE)
                .otherwise(
                        DSL.when(
                                        SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_STATE.isNull(),
                                        SPRINT_REQ_VERSION.STATUS)
                                .otherwise(
                                        SPRINT_REQ_VERSION.STATUS.concat(
                                                DSL.inline(", "), SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_STATE)));
    }

    private Field<String> getDescriptionBasedOnReqVersionId() {
        return when(shouldNotDisplayReqVersionData(), SPRINT_REQ_VERSION.DESCRIPTION)
                .otherwise(RESOURCE.DESCRIPTION);
    }

    private Condition shouldNotDisplayReqVersionData() {
        return SPRINT_REQ_VERSION
                .MODE
                .eq(ManagementMode.SYNCHRONIZED.name())
                .or(SPRINT_REQ_VERSION.REQ_VERSION_ID.isNull())
                .or(
                        SPRINT_REQ_VERSION
                                .MODE
                                .eq(ManagementMode.NATIVE.name())
                                .and(SPRINT.STATUS.eq(SprintStatus.CLOSED.name())));
    }

    /**
     * Retrieves a sprint requirement version dto given its id.
     *
     * @param sprintReqVersionId - ID of the SprintReqVersion
     * @return a SprintReqVersionDto
     */
    public SprintReqVersionView findSprintReqVersionViewById(long sprintReqVersionId) {
        SprintReqVersionView sprintReqVersionDto =
                dslContext
                        .select(
                                SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.as(ID),
                                PROJECT.NAME.as(PROJECT_NAME),
                                PROJECT.PROJECT_ID,
                                PROJECT.CL_ID.as(CAMPAIGN_LIBRARY_ID),
                                getReferenceBasedOnReqVersionId().as(REFERENCE),
                                getNameBasedOnReqVersionId().as(NAME),
                                REQUIREMENT_VERSION.CATEGORY.as(CATEGORY_ID),
                                SPRINT_REQ_VERSION.CATEGORY.as(CATEGORY_LABEL),
                                getCriticalityBasedOnReqVersionId().as(CRITICALITY),
                                getStatusBasedOnReqVersionId().as(STATUS),
                                REQUIREMENT_VERSION.RES_ID.as(VERSION_ID),
                                REQUIREMENT_VERSION.REQUIREMENT_ID.as(REQUIREMENT_ID),
                                SPRINT_REQ_VERSION.SPRINT_ID,
                                getDescriptionBasedOnReqVersionId().as(DESCRIPTION),
                                CAMPAIGN_LIBRARY_NODE.NAME.as(SPRINT_NAME),
                                SPRINT_REQ_VERSION.TEST_PLAN_ID,
                                SPRINT_REQ_VERSION.MODE,
                                SPRINT_REQ_VERSION.CREATED_BY,
                                SPRINT_REQ_VERSION.CREATED_ON,
                                SPRINT_REQ_VERSION.LAST_MODIFIED_BY,
                                SPRINT_REQ_VERSION.LAST_MODIFIED_ON,
                                SPRINT_REQ_VERSION.VALIDATION_STATUS,
                                SPRINT.STATUS.as(SPRINT_STATUS),
                                SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_URL,
                                SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_STATE)
                        .from(SPRINT_REQ_VERSION)
                        .leftJoin(REQUIREMENT_VERSION)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(SPRINT_REQ_VERSION.REQ_VERSION_ID))
                        .leftJoin(RESOURCE)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID))
                        .leftJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(SPRINT_REQ_VERSION.SPRINT_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .leftJoin(PROJECT)
                        .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                        .leftJoin(SPRINT)
                        .on(SPRINT_REQ_VERSION.SPRINT_ID.eq(SPRINT.CLN_ID))
                        .leftJoin(SPRINT_REQUIREMENT_SYNC_EXTENDER)
                        .on(
                                SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(
                                        SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_VERSION_ID))
                        .where(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(sprintReqVersionId))
                        .fetchOneInto(SprintReqVersionView.class);
        Long sprintId = sprintReqVersionDto.getSprintId();
        int nbIssues = sprintReqVersionKnownIssueFinder.countKnownIssues(sprintReqVersionId);
        sprintReqVersionDto.setNbIssues(nbIssues);
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                Collections.singletonList(sprintId),
                Permissions.READ.name(),
                Sprint.class.getName());

        String path = entityPathHeaderService.buildCLNPathHeader(sprintId);
        String sprintName = sprintReqVersionDto.getSprintName();
        sprintReqVersionDto.setPath(path + " > " + sprintName);

        sprintReqVersionDto.setAssignableUsers(
                UserView.fromEntities(
                        sprintTestPlanItemManagerService.findAssignableUsersByCampaignLibraryId(
                                sprintReqVersionDto.getCampaignLibraryId())));

        return sprintReqVersionDto;
    }

    @Override
    public List<AvailableTestPlanItemDto> findAvailableTestPlanItems(long sprintReqVersionId) {
        if (!permissionEvaluationService.hasRoleOrPermissionOnObject(
                ROLE_ADMIN,
                Permissions.LINK.name(),
                sprintReqVersionId,
                org.squashtest.tm.domain.campaign.SprintReqVersion.class.getName())) {
            return Collections.emptyList();
        }

        final List<AvailableTestPlanItemDto> alreadyPlannedItems =
                dslContext
                        .select(
                                TEST_PLAN_ITEM.TCLN_ID,
                                TEST_CASE_LIBRARY_NODE.NAME,
                                TEST_CASE.REFERENCE,
                                TEST_PLAN_ITEM.DATASET_ID,
                                DATASET.NAME)
                        .from(TEST_PLAN_ITEM)
                        .leftJoin(TEST_CASE)
                        .on(TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                        .leftJoin(TEST_CASE_LIBRARY_NODE)
                        .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .leftJoin(DATASET)
                        .on(TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID))
                        .innerJoin(SPRINT_REQ_VERSION)
                        .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                        .where(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(sprintReqVersionId))
                        .fetchInto(AvailableTestPlanItemDto.class);

        final List<AvailableTestPlanItemDto> coverageItems =
                dslContext
                        .select(
                                TEST_CASE.TCLN_ID,
                                TEST_CASE_LIBRARY_NODE.NAME,
                                TEST_CASE.REFERENCE,
                                DATASET.DATASET_ID,
                                DATASET.NAME)
                        .from(TEST_CASE)
                        .leftJoin(TEST_CASE_LIBRARY_NODE)
                        .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .leftJoin(DATASET)
                        .on(TEST_CASE.TCLN_ID.eq(DATASET.TEST_CASE_ID))
                        .innerJoin(REQUIREMENT_VERSION_COVERAGE)
                        .on(TEST_CASE.TCLN_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
                        .innerJoin(REQUIREMENT_VERSION)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                        .innerJoin(SPRINT_REQ_VERSION)
                        .on(SPRINT_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                        .where(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(sprintReqVersionId))
                        .fetchInto(AvailableTestPlanItemDto.class);

        // Remove already planned items from coverage items
        coverageItems.removeIf(coverageItem -> isAlreadyPlanned(coverageItem, alreadyPlannedItems));

        return coverageItems;
    }

    @Override
    public List<AvailableTestPlanItemIdsDto> findAvailableTestPlanItemIds(long sprintReqVersionId) {
        return dslContext
                .select(TEST_CASE.TCLN_ID, DATASET.DATASET_ID)
                .from(SPRINT_REQ_VERSION)
                .leftJoin(REQUIREMENT_VERSION_COVERAGE)
                .on(
                        SPRINT_REQ_VERSION.REQ_VERSION_ID.eq(
                                REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                .leftJoin(TEST_CASE)
                .on(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(DATASET)
                .on(TEST_CASE.TCLN_ID.eq(DATASET.TEST_CASE_ID))
                .where(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(sprintReqVersionId))
                .fetchInto(AvailableTestPlanItemIdsDto.class);
    }

    private static boolean isAlreadyPlanned(
            AvailableTestPlanItemDto coverageItem, List<AvailableTestPlanItemDto> alreadyPlannedItems) {
        return alreadyPlannedItems.stream()
                .anyMatch(alreadyPlannedItem -> compareItems(coverageItem, alreadyPlannedItem));
    }

    private static boolean compareItems(
            AvailableTestPlanItemDto coverageItem, AvailableTestPlanItemDto alreadyPlannedItem) {
        if (alreadyPlannedItem.testCaseId() == null) {
            // Test case could have been deleted, in which case we ignore it
            return false;
        }

        if (!alreadyPlannedItem.testCaseId().equals(coverageItem.testCaseId())) {
            // If the test case is different, the items are different
            return false;
        }

        if (alreadyPlannedItem.datasetId() == null && coverageItem.datasetId() == null) {
            // If both datasets are null, the items are the same
            return true;
        }

        if (alreadyPlannedItem.datasetId() == null || coverageItem.datasetId() == null) {
            // If one dataset is null and the other is not, the items are different
            return false;
        }

        // If both datasets are not null, we compare their ids
        return alreadyPlannedItem.datasetId().equals(coverageItem.datasetId());
    }
}
