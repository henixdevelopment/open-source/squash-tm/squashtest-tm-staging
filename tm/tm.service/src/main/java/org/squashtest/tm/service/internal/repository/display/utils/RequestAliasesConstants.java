/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.utils;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;

public final class RequestAliasesConstants {

    public static final String ACTION = "ACTION";
    public static final String ACTION_WORD_ID = "ACTION_WORD_ID";
    public static final String ACTION_WORD_NAME = "ACTION_WORD_NAME";
    public static final String ACTIVE = "ACTIVE";
    public static final String ASSIGNED_ON = "ASSIGNED_ON";
    public static final String ASSIGNED_TO = "ASSIGNED_TO";
    public static final String ASSIGNED_USER = "ASSIGNED_USER";
    public static final String ASSIGNEE = "ASSIGNEE";
    public static final String ASSIGNEE_FULL_NAME = "ASSIGNEE_FULL_NAME";
    public static final String ASSIGNEE_ID = "ASSIGNEE_ID";
    public static final String ASSIGNEE_LOGIN = "ASSIGNEE_LOGIN";
    public static final String ATTACHMENTS_ALIAS = "ATTACHMENTS";
    public static final String ATTACHMENT_COUNT = "ATTACHMENT_COUNT";
    public static final String AT_TECHNOLOGY_ID = "AT_TECHNOLOGY_ID";
    public static final String AUTOMATED_EXECUTION_COUNT = "AUTOMATED_EXECUTION_COUNT";
    public static final String AUTOMATED_SUITE_COUNT = "AUTOMATED_SUITE_COUNT";
    public static final String AUTOMATED_TEST_FULL_NAME = "AUTOMATED_TEST_FULL_NAME";
    public static final String AUTOMATED_TEST_REFERENCE = "AUTOMATED_TEST_REFERENCE";
    public static final String AUTOMATED_TEST_TECHNOLOGY = "AUTOMATED_TEST_TECHNOLOGY";
    public static final String AUTOMATION_PRIORITY = "AUTOMATION_PRIORITY";
    public static final String AUTOMATION_REQUEST_ID = "AUTOMATION_REQUEST_ID";
    public static final String AVAILABLE_DATASETS = "AVAILABLE_DATASETS";
    public static final String AWLN_ID = "AWLN_ID";
    public static final String BASE_URL = "BASE_URL";
    public static final String BINDABLE_ENTITY = "BINDABLE_ENTITY";
    public static final String BINDING_COUNT = "BINDING_COUNT";
    public static final String BOUND_ENTITY = "BOUND_ENTITY";
    public static final String BOUND_ENTITY_ID = "BOUND_ENTITY_ID";
    public static final String BOUND_TO_BLOCKING_MILESTONE = "BOUND_TO_BLOCKING_MILESTONE";
    public static final String BUGTRACKER_NAME = "BUGTRACKER_NAME";
    public static final String CALL_TREE = "CALL_TREE";
    public static final String CALLED = "CALLED";
    public static final String CALLED_NAME = "CALLED_NAME";
    public static final String CALLED_NODE = "CALLED_NODE";
    public static final String CALLER = "CALLER";
    public static final String CALLER_NAME = "CALLER_NAME";
    public static final String CALLER_NODE = "CALLER_NODE";
    public static final String CALLER_COUNT = "CALLER_COUNT";
    public static final String CALL_STEP_COUNT = "CALL_STEP_COUNT";
    public static final String CAMPAIGN_ID = "CAMPAIGN_ID";
    public static final String CAMPAIGN_LIBRARY_ID = "CAMPAIGN_LIBRARY_ID";
    public static final String CAMPAIGN_NAME = "CAMPAIGN_NAME";
    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String CATEGORY_LABEL = "CATEGORY_LABEL";
    public static final String CHILDREN = "CHILDREN";
    public static final String CHILD_OF_REQUIREMENT = "CHILD_OF_REQUIREMENT";
    public static final String CLN_ID = "CLN_ID";
    public static final String CODE = "CODE";
    public static final String COMMENT = "COMMENT";
    public static final String CONFIGURED_REMOTE_FINAL_STATUS = "CONFIGURED_REMOTE_FINAL_STATUS";
    public static final String CONFLICT_ASSOCIATION = "CONFLICT_ASSOCIATION";
    public static final String CONTENT_ORDER = "CONTENT_ORDER";
    public static final String COVERAGE_ID = "COVERAGE_ID";
    public static final String COVERAGES = "COVERAGES";
    public static final String COVERAGES_ALIAS = "COVERAGES";
    public static final String CREATED_BY = "CREATED_BY";
    public static final String CREATED_ON = "CREATED_ON";
    public static final String CRITICALITY = "CRITICALITY";
    public static final String CRLN_ID_ALIAS = "CUSTOM_REPORT_LIBRARY_NODE_ID";
    public static final String CT_ID = "CT_ID";
    public static final String CUF_ID = "CUF_ID";
    public static final String CUSTOM_FIELD_BINDING_ID = "CUSTOM_FIELD_BINDING_ID";
    public static final String CUSTOM_FIELD_ID = "CUSTOM_FIELD_ID";
    public static final String DATASET_COUNT = "DATASET_COUNT";
    public static final String DATASET_ID = "DATASET_ID";
    public static final String DATASET_LABEL = "DATASET_LABEL";
    public static final String DATASET_NAME = "DATASET_NAME";
    public static final String DATATABLE = "DATATABLE";
    public static final String DATE = "DATE";
    public static final String DEFAULT_VALUE = "DEFAULT_VALUE";
    public static final String DEPTH = "DEPTH";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String DIRECTION = "DIRECTION";
    public static final String DOCSTRING = "DOCSTRING";
    public static final String END_DATE = "END_DATE";
    public static final String ENVIRONMENT_TAG = "ENVIRONMENT_TAG";
    public static final String ENVIRONMENT_TAGS = "ENVIRONMENT_TAGS";
    public static final String ENVIRONMENT_VARIABLE = "ENVIRONMENT_VARIABLE";
    public static final String ENVIRONMENT_VARIABLES = "ENVIRONMENT_VARIABLES";
    public static final String EVENT = "EVENT";
    public static final String EV_ID = "EV_ID";

    public static final String EXEC_PLAN_TYPE = "EXEC_PLAN_TYPE";
    public static final String EXECUTED_BY = "EXECUTED_BY";
    public static final String EXECUTION_COUNT = "EXECUTION_COUNT";
    public static final String EXECUTION_ID = "EXECUTION_ID";
    public static final String EXECUTION_LAST_EXECUTED_ON = "EXECUTION_LAST_EXECUTED_ON";
    public static final String EXECUTION_LATEST_STATUS = "EXECUTION_LATEST_STATUS";
    public static final String EXECUTION_MODE = "EXECUTION_MODE";
    public static final String EXECUTION_NAME = "EXECUTION_NAME";
    public static final String EXECUTION_ON = "EXECUTION_ON";
    public static final String EXECUTION_ORDER = "EXECUTION_ORDER";
    public static final String EXECUTION_PATH = "EXECUTION_PATH";
    public static final String EXECUTION_REFERENCE = "EXECUTION_REFERENCE";
    public static final String EXECUTION_SERVER = "EXECUTION_SERVER";
    public static final String EXECUTION_STATUS = "EXECUTION_STATUS";
    public static final String EXPECTED_RESULT = "EXPECTED_RESULT";
    public static final String EXPIRY_DATE = "EXPIRY_DATE";
    public static final String EXPLORATORY_TEST_CASE_ID = "EXPLORATORY_TEST_CASE_ID";
    public static final String FIELD_TYPE = "FIELD_TYPE";
    public static final String HABILITATION_COUNT = "HABILITATION_COUNT";
    public static final String HAS_AUTO_SCRIPT = "HAS_AUTO_SCRIPT";
    public static final String HAS_BLOCKING_MILESTONE = "HAS_BLOCKING_MILESTONE";
    public static final String HAS_BOUND_AUTOMATED_TEST_REFERENCE =
            "HAS_BOUND_AUTOMATED_TEST_REFERENCE";
    public static final String HAS_BOUND_SCM_REPOSITORY = "HAS_BOUND_SCM_REPOSITORY";
    public static final String HAS_AUTOMATED_TESTS = "HAS_AUTOMATED_TESTS";
    public static final String HAS_DATASETS = "HAS_DATASETS";
    public static final String HAS_DELEGATED_PARAMETER = "HAS_DELEGATED_PARAMETER";
    public static final String HAS_LINK_TYPE = "HAS_LINK_TYPE";
    public static final String HAS_PERMISSIONS = "HAS_PERMISSIONS";
    public static final String HIGH_LEVEL_REQUIREMENT_ID = "HIGH_LEVEL_REQUIREMENT_ID";
    public static final String ID = "ID";
    public static final String ID_LIST = "ID_LIST";
    public static final String IMPORTANCE = "IMPORTANCE";
    public static final String INFERRED_EXECUTION_MODE = "INFERRED_EXECUTION_MODE";
    public static final String INFO_LIST_ID = "INFO_LIST_ID";
    public static final String INNER_REQUIREMENT = "INNER_REQUIREMENT";
    public static final String INNER_REQUIREMENT_ID = "INNER_REQUIREMENT_ID";
    public static final String INNER_VERSIONS = "INNER_VERSIONS";
    public static final String INPUT_TYPE = "INPUT_TYPE";
    public static final String ISSUE_COUNT = "ISSUE_COUNT";
    public static final String ISSUE_LIST_ID = "ISSUE_LIST_ID";
    public static final String IS_DEFAULT = "IS_DEFAULT";
    public static final String IS_HIGH_LEVEL_REQUIREMENT = "IS_HIGH_LEVEL_REQUIREMENT";
    public static final String IS_MANUAL = "IS_MANUAL";
    public static final String IS_TEMPLATE = "IS_TEMPLATE";
    public static final String ITEM_ID = "ITEM_ID";
    public static final String ITEM_TEST_PLAN_ID = "ITEM_TEST_PLAN_ID";
    public static final String ITERATION_COUNT = "ITERATION_COUNT";
    public static final String ITERATION_ID = "ITERATION_ID";
    public static final String ITERATION_NAME = "ITERATION_NAME";
    public static final String ITERATION_TEST_PLAN_ITEM = "ITERATION_TEST_PLAN_ITEM";
    public static final String KEYWORD = "KEYWORD";
    public static final String KEYWORD_TEST_CASE_ID = "KEYWORD_TEST_CASE_ID";
    public static final String KEYWORD_TEST_STEP_COUNT = "KEYWORD_TEST_STEP_COUNT";
    public static final String KIND = "KIND";
    public static final String LABEL = "LABEL";
    public static final String LAST_EXEC = "LAST_EXEC";
    public static final String LAST_CREATED = "LAST_CREATED";
    public static final String LAST_EXECUTED = "LAST_EXECUTED";
    public static final String LAST_EXECUTED_BY = "LAST_EXECUTED_BY";
    public static final String LAST_EXECUTED_ON = "LAST_EXECUTED_ON";
    public static final String LAST_EXECUTION_MODE = "LAST_EXECUTION_MODE";
    public static final String LAST_EXECUTION_STATUSES = "LAST_EXECUTION_STATUSES";
    public static final String LAST_MODIFIED_BY = "LAST_MODIFIED_BY";
    public static final String LAST_MODIFIED_ON = "LAST_MODIFIED_ON";
    public static final String LAST_USAGE = "LAST_USAGE";
    public static final String LATEST_EXECUTION_ID = "LATEST_EXECUTION_ID";
    public static final String LATEST_ITPI_IDS_SUBQUERY = "LATEST_ITPI_IDS_SUBQUERY";
    public static final String LAUNCHED_FROM = "LAUNCHED_FROM";
    public static final String LINKED_STANDARD_REQUIREMENT = "LINKED_STANDARD_REQUIREMENT";
    public static final String LINKED_TEST_CASE_ID = "LINKED_TEST_CASE_ID";
    public static final String LINKED_TO_REQUIREMENT = "LINKED_TO_REQUIREMENT";
    public static final String LINKS = "LINKS";
    public static final String LINK_COUNT = "LINK_COUNT";
    public static final String LINK_TYPE_ID = "LINK_TYPE_ID";
    public static final String LINKED_CT_ID = "LINKED_CT_ID";
    public static final String LOGIN = "LOGIN";
    public static final String MAX_LAST_EXECUTED_ON = "MAX_LAST_EXECUTED_ON";
    public static final String MILESTONES_ALIAS = "MILESTONES";
    public static final String MILESTONE_COUNT = "MILESTONE_COUNT";
    public static final String MILESTONE_END_DATE = "MILESTONE_END_DATE";
    public static final String MILESTONE_ID = "MILESTONE_ID";
    public static final String MILESTONE_LABELS = "MILESTONE_LABELS";
    public static final String MILESTONE_LABEL = "MILESTONE_LABEL";
    public static final String MILESTONE_MAX_DATE = "MILESTONE_MAX_DATE";
    public static final String MILESTONE_MIN_DATE = "MILESTONE_MIN_DATE";
    public static final String MILESTONE_STATUS = "MILESTONE_STATUS";
    public static final String MODIFIED_BY = "MODIFIED_BY";
    public static final String MODIFIED_ON = "MODIFIED_ON";
    public static final String NAME = "NAME";
    public static final String NAMED_VALUE = "NAMED_VALUE";
    public static final String NATURE = "NATURE";
    public static final String NB_EXECUTIONS = "NB_EXECUTIONS";
    public static final String NB_ISSUES = "NB_ISSUES";
    public static final String NB_NOTES = "NB_NOTES";
    public static final String NB_STATUS_OTHER = "NB_STATUS_OTHER";
    public static final String NB_STATUS_PREFIX = "NB_STATUS_";
    public static final String NB_STATUS_TOTAL = "NB_STATUS_TOTAL";
    public static final String NB_TESTS = "NB_TESTS";
    public static final String NEW_VALUE = "NEW_VALUE";
    public static final String NOTE_TYPES = "NOTE_TYPES";
    public static final String OLD_AUTOMATED_EXECUTION_COUNT = "OLD_AUTOMATED_EXECUTION_COUNT";
    public static final String OLD_AUTOMATED_SUITE_COUNT = "OLD_AUTOMATED_SUITE_COUNT";
    public static final String OLD_VALUE = "OLD_VALUE";
    public static final String OPTIONAL = "OPTIONAL";
    public static final String OVERVIEW_ID = "OVERVIEW_ID";
    public static final String OWNER_FIRST_NAME = "OWNER_FIRST_NAME";
    public static final String OWNER_ID = "OWNER_ID";
    public static final String OWNER_LAST_NAME = "OWNER_LAST_NAME";
    public static final String OWNER_LOGIN = "OWNER_LOGIN";
    public static final String PARAMETER_COUNT = "PARAMETER_COUNT";
    public static final String PARTY_COUNT = "PARTY_COUNT";
    public static final String PARTY_ID = "PARTY_ID";
    public static final String PARTY_NAME = "PARTY_NAME";
    public static final String PAYLOAD_TEMPLATE = "PAYLOAD_TEMPLATE";
    public static final String PERMISSIONS = "PERMISSIONS";
    public static final String PRIORITY = "PRIORITY";
    public static final String PROJECT_CTE = "PROJECT_CTE";
    public static final String PROJECT_COUNT = "PROJECT_COUNT";
    public static final String PROJECT_ID = "PROJECT_ID";
    public static final String PROJECT_ID_CTE = "PROJECT_ID_CTE";
    public static final String PROJECT_NAME = "PROJECT_NAME";
    public static final String QUALIFIED_NAME = "QUALIFIED_NAME";
    public static final String RANGE = "RANGE";
    public static final String REFERENCE = "REFERENCE";
    public static final String REMOTE_KEY = "REMOTE_KEY";
    public static final String REMOTE_REQ_STATUS_AND_STATE = "REMOTE_REQ_STATUS_AND_STATE";
    public static final String REQUEST_STATUS = "REQUEST_STATUS";
    public static final String REQUIREMENT_CATEGORY_ID = "REQUIREMENT_CATEGORY_ID";
    public static final String REQUIREMENT_HAS_PARENT_REQ = "HAS_PARENT";
    public static final String REQUIREMENT_ID = "REQUIREMENT_ID";
    public static final String REQUIREMENT_LIBRARY_NODE_ID = "REQUIREMENT_LIBRARY_NODE_ID";
    public static final String REQUIREMENT_VERSION_NUMBER = "REQUIREMENT_VERSION_NUMBER";
    public static final String REQUIREMENT_VERSION_PROJECT = "REQUIREMENT_VERSION_PROJECT";
    public static final String REQUIREMENT_VERSION_PROJECT_ID = "REQUIREMENT_VERSION_PROJECT_ID";
    public static final String REQUIREMENT_VERSION_PROJECT_NAME = "REQUIREMENT_VERSION_PROJECT_NAME";
    public static final String REQUIREMENT_VERSION_REFERENCE = "REQUIREMENT_VERSION_REFERENCE";
    public static final String REQ_VERSION_COVERAGE_COUNT = "REQ_VERSION_COVERAGE_COUNT";
    public static final String RESOURCE_NAME = "RESOURCE_NAME";
    public static final String REVIEWED = "REVIEWED";
    public static final String ROAD = "ROAD";
    public static final String ROLE = "ROLE";
    public static final String ROLE1_CODE = "ROLE1_CODE";
    public static final String ROLE2 = "ROLE2";
    public static final String ROLE2_CODE = "ROLE2_CODE";
    public static final String ROLE_1 = "ROLE_1";
    public static final String ROLE_1_CODE = "ROLE_1_CODE";
    public static final String ROLE_2 = "ROLE_2";
    public static final String ROLE_2_CODE = "ROLE_2_CODE";
    public static final String ROW_NUMBER = "ROW_NUMBER";
    public static final String RUNNING_STATE = "RUNNING_STATE";
    public static final String SCM_REPOSITORY_ID = "SCM_REPOSITORY_ID";
    public static final String SCRIPTED_TEST_CASE_ID = "SCRIPTED_TEST_CASE_ID";
    public static final String SERVER_ID = "SERVER_ID";
    public static final String SPRINT_NAME = "SPRINT_NAME";
    public static final String SPRINT_REQ_VERSION_ALIAS = "SPRINT_REQ_VERSION_ALIAS";
    public static final String SPRINT_REQ_VERSION_ID = "SPRINT_REQ_VERSION_ID";
    public static final String SPRINT_REQ_VERSION_NAME_WITH_REF = "SPRINT_REQ_VERSION_NAME_WITH_REF";
    public static final String SPRINT_STATUS = "SPRINT_STATUS";
    public static final String STATUS = "STATUS";
    public static final String STEP_ID = "STEP_ID";
    public static final String STEP_INDEX = "STEP_INDEX";
    public static final String STEP_STATUS = "STEP_STATUS";
    public static final String SUCCESS_RATE = "SUCCESS_RATE";
    public static final String SUITE_ID = "SUITE_ID";
    public static final String SYNCHRONISATION_COUNT = "SYNCHRONISATION_COUNT";
    public static final String SYNCHRONISATION_ID = "SYNCHRONISATION_ID";
    public static final String SYNC_REQ_CREATION_SOURCE = "SYNC_REQ_CREATION_SOURCE";
    public static final String SYNC_REQ_UPDATE_SOURCE = "SYNC_REQ_UPDATE_SOURCE";
    public static final String TASK_DIVISION = "TASK_DIVISION";
    public static final String TA_SERVER_ID = "TA_SERVER_ID";
    public static final String TCLN_ID = "TCLN_ID";
    public static final String TC_KIND = "KIND";
    public static final String TC_MILESTONE_LOCKED = "TC_MILESTONE_LOCKED";
    public static final String TC_NATURE = "TC_NATURE";
    public static final String TC_TYPE = "TC_TYPE";
    public static final String TEAM = "TEAM";
    public static final String TEAM_COUNT = "TEAM_COUNT";
    public static final String TEAM_MEMBERS_COUNT = "TEAM_MEMBERS_COUNT";
    public static final String TEST_AUTOMATION_SERVER_KIND = "TEST_AUTOMATION_SERVER_KIND";
    public static final String TEST_CASE_DELETED = "TEST_CASE_DELETED";
    public static final String TEST_CASE_ID = "TEST_CASE_ID";
    public static final String TEST_CASE_IMPORTANCE = "TEST_CASE_IMPORTANCE";
    public static final String TEST_CASE_NAME = "TEST_CASE_NAME";
    public static final String TEST_CASE_NATURE_ID = "TEST_CASE_NATURE_ID";
    public static final String TEST_CASE_PATH = "TEST_CASE_PATH";
    public static final String TEST_CASE_REFERENCE = "TEST_CASE_REFERENCE";
    public static final String TEST_CASE_TYPE_ID = "TEST_CASE_TYPE_ID";
    public static final String TEST_PLAN_ITEM_ID = "TEST_PLAN_ITEM_ID";
    public static final String TEST_PLAN_ITEM_ORDER = "TEST_PLAN_ITEM_ORDER";
    public static final String TEST_STEP_COUNT = "TEST_STEP_COUNT";
    public static final String TEST_STEP_ID = "TEST_STEP_ID";
    public static final String TEST_SUITES = "TEST_SUITES";
    public static final String TEST_SUITE_NAME = "TEST_SUITE_NAME";
    public static final String TEST_TECHNOLOGY = "TEST_TECHNOLOGY";
    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TRANSMISSION_DATE = "TRANSMISSION_DATE";
    public static final String TRANSMITTED_ON = "TRANSMITTED_ON";
    public static final String TYPE = "TYPE";
    public static final String TYPE_ID = "TYPE_ID";
    public static final String URL = "URL";
    public static final String USER = "USER";
    public static final String USER_GROUP = "USER_GROUP";
    public static final String USER_SORT_COLUMN = "USER_SORT_COLUMN";
    public static final String UUID = "UUID";
    public static final String VALUE = "VALUE";
    public static final String VERIFIED_BY = "VERIFIED_BY";
    public static final String VERIFYING_TEST_CASE_ID = "VERIFYING_TEST_CASE_ID";
    public static final String VERSIONS_COUNT = "VERSIONS_COUNT";
    public static final String VERSION_ID = "VERSION_ID";
    public static final String WORKFLOWS = "WORKFLOWS";
    public static final String WORKFLOW_TYPE = "WORKFLOW_TYPE";

    private static final Converter<String, String> converter =
            CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);

    private RequestAliasesConstants() {}

    public static String toCamelCase(String upperSnakeCaseInput) {
        return converter.convert(upperSnakeCaseInput);
    }
}
