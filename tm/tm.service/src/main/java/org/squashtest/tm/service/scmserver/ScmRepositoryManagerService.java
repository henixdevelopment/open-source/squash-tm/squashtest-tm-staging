/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.scmserver;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.service.internal.dto.ScmRepositoryDto;

public interface ScmRepositoryManagerService {

    /**
     * Find the ScmRepositories contained in the ScmServer with the given Id ordered by path.
     *
     * @param scmServerId The Id of the ScmServer containing the wanted ScmRepositories.
     * @return The List of the ScmRepositories contained in the given ScmServer ordered by path.
     */
    List<ScmRepository> findByScmServerOrderByPath(Long scmServerId);

    /**
     * Find the <u>cloned</u> ScmRepositories contained in the ScmServer with the given id ordered by
     * name.
     *
     * @param scmServerId id of the ScmServer containing the wanted ScmRepositories
     * @return List of the ScmRepositories contained in the given ScmServer ordered by name.
     */
    List<ScmRepository> findClonedByScmServerOrderByName(long scmServerId);

    /**
     * Find the ScmRepositories contained in the ScmServer with the given Id formatted as a Page to
     * comply the given Pageable.
     *
     * @param scmServerId The Id of the ScmServer containing the wanted ScmRepositories.
     * @param pageable The Pageable against which the Page will be built.
     * @return The Page of ScmRepositories built according to the given Pageable.
     */
    Page<ScmRepository> findPagedScmRepositoriesByScmServer(Long scmServerId, Pageable pageable);

    /**
     * Create a new ScmRepository with its attributes and bind it to the given ScmServer. If parameter
     * cloneRepository is true, this implies the creation of the local repository in the file system.
     *
     * @param scmServerId id of the ScmServer which contains the new ScmRepository.
     * @param name name of the new ScmRepository.
     * @param branch branch of the new ScmRepository.
     * @param workingFolderPath path of the working folder of the new ScmRepository.
     * @param cloneRepository whether to clone the repository in the file system
     * @throws IOException If an error occurs when creating the local repository.
     */
    ScmRepository createNewScmRepository(
            long scmServerId,
            @NotNull String name,
            @NotNull String branch,
            String workingFolderPath,
            boolean cloneRepository)
            throws IOException;

    /**
     * Update the working branch of the ScmRepository with the given Id to the new given branch. This
     * implies the modification of the local repository state.
     *
     * @param scmRepositoryId The Id of the ScmRepository which branch is to update.
     * @param newBranch The new branch of the ScmRepository.
     * @return The new branch of the ScmRepository.
     * @throws IOException If an error occurs when modifying the local repository state.
     */
    String updateBranch(long scmRepositoryId, String newBranch) throws IOException;

    /**
     * Delete the ScmRepositories with the given Ids.
     *
     * @param scmRepositoriesIds The Ids of the ScmRepositories to delete.
     */
    void deleteScmRepositories(Collection<Long> scmRepositoriesIds);

    /**
     * Check if at least one of the ScmRepositories with the given Ids is bound to a Project or a Test
     * Case.
     *
     * @param scmRepositoryIds The Ids of the ScmRepositories
     * @return True if at least one of the given ScmRepository is bound to a Project. False otherwise.
     */
    boolean isOneRepositoryBoundToProjectOrTestCase(Collection<Long> scmRepositoryIds);

    /**
     * Get all declared ScmRepositories in the application and return them as a List of {@linkplain
     * ScmRepositoryDto}. A translated 'None' option is added to allow unbinding. A ScmRepositoryDto
     * is represented by its id and its friendlyUrl which value is the full Url of the ScmRepository
     * concatenated with the working branch name. An example of friendlyUrl could be
     * "http://myserver/myuser/myrepo - master".
     *
     * @param locale to translate the 'None' option
     * @return the List of all {@linkplain ScmRepositoryDto} declared in the application
     */
    List<ScmRepositoryDto> getAllDeclaredScmRepositories(Locale locale);

    void recloneScmRepository(long scmRepositoryId);
}
