/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.referential;

import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.ACTIVATED_USER_EXCESS;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.PLUGIN_LICENSE_EXPIRATION;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.SQUASH_CALLBACK_URL;

import com.google.common.collect.Multimap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.CommonTableExpression;
import org.jooq.Record1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.csp.core.bugtracker.core.UnknownConnectorKindException;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerProviderDescriptor;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bdd.BddScriptLanguage;
import org.squashtest.tm.domain.bdd.Keyword;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.service.bugtracker.BugTrackersService;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.display.referential.ReferentialDataProvider;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.documentation.DocumentationLinkProvider;
import org.squashtest.tm.service.internal.bugtracker.BugTrackerConnectorFactory;
import org.squashtest.tm.service.internal.bugtracker.adapter.InternalBugtrackerConnector;
import org.squashtest.tm.service.internal.display.dto.BugTrackerBindingDto;
import org.squashtest.tm.service.internal.display.dto.BugTrackerDto;
import org.squashtest.tm.service.internal.display.dto.BugTrackerReferentialDto;
import org.squashtest.tm.service.internal.display.dto.GlobalConfigurationDto;
import org.squashtest.tm.service.internal.display.dto.KeywordDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneBindingDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.ProjectFilterDto;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.dto.DetailedUserDto;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.display.AclDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AiServerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomatedTestTechnologyDisplayDao;
import org.squashtest.tm.service.internal.repository.display.BugTrackerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldDao;
import org.squashtest.tm.service.internal.repository.display.InfoListDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProjectFilterDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionLinkTypeDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ScmServerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestAutomationServerDisplayDao;
import org.squashtest.tm.service.internal.security.AuthenticationProviderContext;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

@Service
@Transactional(readOnly = true)
public class ReferentialDataProviderImpl implements ReferentialDataProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReferentialDataProviderImpl.class);

    @Value("${squash.rest-api.jwt.secret:#{null}}")
    private String jwtSecret;

    @Inject private CustomProjectFinder projectFinder;
    @Inject private ProjectFilterDisplayDao projectFilterDao;
    @Inject private ProjectDisplayDao projectDao;
    @Inject private MilestoneDisplayDao milestoneDao;
    @Inject private CustomFieldDao customFieldDao;
    @Inject private InfoListDisplayDao infoListDao;
    @Inject private ConfigurationService configurationService;
    @Inject private AclDisplayDao aclDisplayDao;
    @Inject private BugTrackerDisplayDao bugTrackerDisplayDao;
    @Inject private BugTrackersService bugTrackersService;
    @Inject private TestAutomationServerDisplayDao testAutomationServerDisplayDao;
    @Inject private UserDisplayService userDisplayService;
    @Inject private RequirementVersionLinkTypeDisplayDao requirementVersionLinkTypeDisplayDao;
    @Inject private MessageSource translateService;
    @Inject private AutomatedTestTechnologyDisplayDao automatedTestTechnologyDisplayDao;
    @Inject private ScmServerDisplayDao scmServerDisplayDao;
    @Inject private AuthenticationProviderContext authenticationProviderContext;
    @Inject private PluginFinderService pluginFinderService;
    @Inject private BugTrackerConnectorFactory bugTrackerConnectorFactory;
    @Inject private UltimateLicenseAvailabilityService ultimateLicenseService;
    @Inject private AiServerDisplayDao aiServerDisplayDao;
    @Inject private PermissionEvaluationService permissionEvaluationService;
    @Inject private StoredCredentialsManager storedCredentialsManager;

    @PersistenceContext private EntityManager entityManager;

    @Autowired(required = false)
    private final Collection<TemplateConfigurablePlugin> templatePlugins = Collections.emptyList();

    @Autowired(required = false)
    private final Collection<DocumentationLinkProvider> documentationLinkProviders =
            Collections.emptyList();

    @Override
    public AdminReferentialData findAdminReferentialData() {
        DetailedUserDto currentUser = userDisplayService.findCurrentUser();
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin(
                currentUser.getPartyIds());
        AdminReferentialData referentialData = new AdminReferentialData();
        referentialData.setUser(currentUser);
        appendConfiguration(referentialData);
        appendCustomFields(referentialData);
        appendAvailableTestAutomationServerKinds(referentialData);
        appendAvailableScmServerKinds(referentialData);
        appendAiServers(referentialData);
        referentialData.setCanManageLocalPassword(
                authenticationProviderContext.isInternalProviderEnabled());
        appendTemplateConfigurablePlugins(referentialData);
        appendBugTrackers(referentialData);
        referentialData.setDocumentationLinks(getAllDocumentationLinks());
        referentialData.setPremiumPluginInstalled(isPremiumPluginInstalled());
        referentialData.setUltimateLicenseAvailable(isUltimateLicenseAvailable());
        referentialData.setJwtSecretDefined(jwtSecret != null);
        return referentialData;
    }

    @Override
    public ReferentialData findReferentialData() {
        ReferentialData referentialData = new ReferentialData();
        appendUser(referentialData);
        CommonTableExpression<Record1<Long>> projectIdsCte =
                projectFinder.findAllReadableIdsCte(referentialData.getUser());
        if (projectFinder.checkHasAtLeastOneReadableId(projectIdsCte)) {
            addProjectFilter(referentialData, referentialData.getUser());
            appendProjects(referentialData, referentialData.getUser(), projectIdsCte);
            appendInterProjectData(referentialData);
            appendRequirementVersionLinkTypes(referentialData);
            referentialData.setProjectPermissions(findProjectPermissions(referentialData.getUser()));
            appendTemplateConfigurablePlugins(referentialData);
        }

        referentialData.setDocumentationLinks(getAllDocumentationLinks());
        appendConfiguration(referentialData);
        appendBugTrackers(referentialData);
        referentialData.setCanManageLocalPassword(
                authenticationProviderContext.isInternalProviderEnabled());
        referentialData.setPremiumPluginInstalled(isPremiumPluginInstalled());
        referentialData.setUltimateLicenseAvailable(isUltimateLicenseAvailable());
        referentialData.setJwtSecretDefined(jwtSecret != null);
        return referentialData;
    }

    private void appendScmServers(ReferentialData referentialData) {
        referentialData.setScmServers(scmServerDisplayDao.findAll());
    }

    private void appendRequirementVersionLinkTypes(ReferentialData referentialData) {
        referentialData.setRequirementVersionLinkTypes(requirementVersionLinkTypeDisplayDao.findAll());
    }

    private void appendInterProjectData(ReferentialData referentialData) {
        appendInfoLists(referentialData);
        appendCustomFields(referentialData);
        appendMilestones(referentialData);
        appendAutomationServers(referentialData);
        appendAutomatedTestTechnologies(referentialData);
        appendScmServers(referentialData);
    }

    private void appendAutomatedTestTechnologies(ReferentialData referentialData) {
        referentialData.setAutomatedTestTechnologies(automatedTestTechnologyDisplayDao.findAll());
    }

    private void appendUser(AdminReferentialData referentialData) {
        DetailedUserDto currentUser = userDisplayService.findCurrentUser();
        referentialData.setUser(currentUser);
        referentialData.setAdmin(currentUser.isAdmin());
    }

    private void appendConfiguration(AdminReferentialData referentialData) {
        Map<String, String> coreConfig = this.configurationService.findAllConfiguration();
        referentialData.setGlobalConfiguration(GlobalConfigurationDto.create(coreConfig));
        appendLicenseInformation(referentialData, coreConfig);
        referentialData.setCallbackUrl(coreConfig.getOrDefault(SQUASH_CALLBACK_URL, null));
    }

    private void appendAutomationServers(ReferentialData referentialData) {
        referentialData.setAutomationServers(testAutomationServerDisplayDao.findAll());
    }

    private void appendBugTrackers(AdminReferentialData referentialData) {
        @SuppressWarnings("unchecked")
        final List<BugTracker> bugTrackers =
                entityManager.createQuery("SELECT bt FROM BugTracker bt").getResultList();

        referentialData.setBugTrackers(mapToBugTrackerReferentialDtos(bugTrackers));
    }

    private List<BugTrackerReferentialDto> mapToBugTrackerReferentialDtos(
            List<BugTracker> bugTrackers) {
        final Set<String> registeredKinds = bugTrackerConnectorFactory.getProviderKinds();

        return bugTrackers.stream()
                .map(
                        bugTracker -> {
                            final BugTrackerDto btDto = BugTrackerDto.from(bugTracker);
                            final BugTrackerReferentialDto refDto = BugTrackerReferentialDto.from(btDto);

                            if (registeredKinds.contains(refDto.getKind())) {
                                appendMessageKeysAndCacheInfo(bugTracker, refDto);
                            }

                            return refDto;
                        })
                .toList();
    }

    private void appendMessageKeysAndCacheInfo(
            BugTracker bugTracker, BugTrackerReferentialDto refDto) {
        try {
            final InternalBugtrackerConnector bugTrackerConnector =
                    bugTrackerConnectorFactory.createConnector(bugTracker);

            refDto.setLoginFieldKey(bugTrackerConnector.getKeyForLogin(bugTracker));
            refDto.setPasswordFieldKey(bugTrackerConnector.getKeyForPassword(bugTracker));

            final BugTrackerProviderDescriptor providerDescriptor =
                    bugTrackersService.getProviderDescriptor(refDto.getKind());
            refDto.setUseProjectPaths(providerDescriptor.usePathToProjects());
            refDto.setProjectHelpMessage(providerDescriptor.getProjectHelpMessage());

            refDto.setCacheAllowed(bugTrackerConnector.allowsReportingCache());
            ManageableCredentials cacheCredentials =
                    storedCredentialsManager.findReportingCacheCredentials(bugTracker.getId());
            refDto.setCacheConfigured(cacheCredentials != null);

        } catch (UnknownConnectorKindException ex) {
            String warnMessage =
                    String.format(
                            "Cannot find provider for BugTracker kind %s, referential data may be incomplete.",
                            refDto.getKind());
            LOGGER.warn(warnMessage, ex);
        }
    }

    private void appendCustomFields(AdminReferentialData referentialData) {
        referentialData.setCustomFields(customFieldDao.findAllWithPossibleValues());
    }

    private void appendInfoLists(ReferentialData referentialData) {
        referentialData.setInfoLists(infoListDao.findAllWithItems());
    }

    private void appendProjects(
            ReferentialData referentialData,
            UserDto currentUser,
            CommonTableExpression<Record1<Long>> projectIdsCte) {
        List<ProjectDto> projects = projectDao.getActiveProjectsByIds(projectIdsCte);
        customFieldDao.appendCustomFieldBindings(projects, projectIdsCte);
        appendBugTrackerBindings(projects);
        milestoneDao.appendMilestoneBinding(projects, projectIdsCte);
        appendProjectPermissions(projects, currentUser);
        appendDisabledExecutionStatus(projects, projectIdsCte);
        appendTranslatedKeywords(projects);
        referentialData.setProjects(projects);
        projectDao.appendActivatedPlugins(projects);
    }

    private void appendBugTrackerBindings(List<ProjectDto> projects) {
        projects.stream()
                .filter(project -> project.getBugtrackerId() != null)
                .forEach(
                        projectId -> {
                            BugTrackerBindingDto bugTrackerBindingDto = new BugTrackerBindingDto();
                            bugTrackerBindingDto.setBugTrackerId(projectId.getBugtrackerId());
                            bugTrackerBindingDto.setProjectId(projectId.getId());
                            projectId.setBugTrackerBinding(bugTrackerBindingDto);
                        });
    }

    private void appendMilestones(ReferentialData referentialData) {
        Set<Long> milestoneIds =
                referentialData.getProjects().stream()
                        .flatMap(
                                projectDto ->
                                        projectDto.getMilestoneBindings().stream()
                                                .map(MilestoneBindingDto::getMilestoneId))
                        .collect(Collectors.toSet());
        List<MilestoneDto> milestones = milestoneDao.findByIds(milestoneIds);
        referentialData.setMilestones(milestones);
    }

    private void appendDisabledExecutionStatus(
            List<ProjectDto> projects, CommonTableExpression<Record1<Long>> projectIdsCte) {
        if (!projects.isEmpty()) {
            Map<Long, List<String>> disabledExecutionStatus =
                    this.projectDao.getDisabledExecutionStatus(projectIdsCte);
            projects.stream()
                    .filter(project -> disabledExecutionStatus.containsKey(project.getId()))
                    .forEach(
                            projectDto -> {
                                List<String> disabledStatus = disabledExecutionStatus.get(projectDto.getId());
                                projectDto.setDisabledExecutionStatus(disabledStatus);
                            });
        }
    }

    private void appendProjectPermissions(List<ProjectDto> projects, UserDto currentUser) {
        if (!projects.isEmpty()) {
            List<Long> projectIds = projects.stream().map(ProjectDto::getId).toList();
            Map<Long, Multimap<String, String>> permissions =
                    aclDisplayDao.findPermissions(projectIds, currentUser.getPartyIds());
            projects.stream()
                    .filter(project -> permissions.containsKey(project.getId()))
                    .forEach(project -> project.setPermissions(permissions.get(project.getId()).asMap()));
        }
    }

    private void appendTranslatedKeywords(List<ProjectDto> projects) {
        projects.forEach(
                project -> {
                    Locale locale = BddScriptLanguage.valueOf(project.getBddScriptLanguage()).getLocale();
                    project.setKeywords(getTranslatedKeywordsList(locale));
                });
    }

    private List<KeywordDto> getTranslatedKeywordsList(Locale locale) {
        return Arrays.stream(Keyword.values())
                .map(
                        keyword ->
                                new KeywordDto(
                                        keyword.name(),
                                        translateService.getMessage(keyword.i18nKeywordNameKey(), null, locale)))
                .toList();
    }

    private void addProjectFilter(ReferentialData referentialData, UserDto currentUser) {
        ProjectFilterDto projectFilter =
                projectFilterDao.getProjectFilterByUserLogin(currentUser.getUsername());

        if (Objects.nonNull(projectFilter)) {
            referentialData.setProjectFilterStatus(projectFilter.getActivated());
            List<Long> projectsIds = projectFilterDao.getProjectIdsByProjectFilter(projectFilter.getId());
            referentialData.setFilteredProjectIds(projectsIds);
        }
    }

    private void appendLicenseInformation(
            AdminReferentialData referentialData, Map<String, String> coreConfig) {
        LicenseInformationDto licenseInfo = new LicenseInformationDto();
        licenseInfo.setPluginLicenseExpiration(
                coreConfig.getOrDefault(PLUGIN_LICENSE_EXPIRATION, null));
        licenseInfo.setActivatedUserExcess(coreConfig.getOrDefault(ACTIVATED_USER_EXCESS, null));
        referentialData.setLicenseInformation(licenseInfo);
    }

    private void appendAvailableTestAutomationServerKinds(AdminReferentialData referentialData) {
        referentialData.setAvailableTestAutomationServerKinds(
                testAutomationServerDisplayDao.findAllAvailableKinds());
    }

    private void appendAvailableScmServerKinds(AdminReferentialData referentialData) {
        referentialData.setAvailableScmServerKinds(scmServerDisplayDao.findAllAvailableKinds());
    }

    private void appendTemplateConfigurablePlugins(AdminReferentialData referentialData) {
        referentialData.setTemplateConfigurablePlugins(
                templatePlugins.stream()
                        .map(TemplateConfigurablePluginDto::fromTemplateConfigurablePlugin)
                        .toList());
    }

    private boolean isPremiumPluginInstalled() {
        return pluginFinderService.isPremiumPluginInstalled();
    }

    private boolean isUltimateLicenseAvailable() {
        return ultimateLicenseService.isAvailable();
    }

    private List<DocumentationLinkProvider.Link> getAllDocumentationLinks() {
        return documentationLinkProviders.stream()
                .flatMap(provider -> provider.getDocumentationLinks().stream())
                .toList();
    }

    private List<ProjectPermissionDto> findProjectPermissions(UserDto currentUser) {
        return userDisplayService.getAllProjectPermissionsUnsecured(currentUser.getUsername());
    }

    private void appendAiServers(AdminReferentialData adminReferentialData) {
        adminReferentialData.setAiServers(aiServerDisplayDao.findAll());
    }
}
