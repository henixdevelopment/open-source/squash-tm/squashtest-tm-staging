/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model.messages;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import org.opentestfactory.messages.OTFMessage;

public class ExecutionError extends OTFMessage {
    private static final String NAME_KEY = "name";
    private static final String WORKFLOW_ID_KEY = "workflow_id";
    private Map<String, Object> details;

    public ExecutionError(String apiVersion, String name, String workflowId) {
        this(apiVersion, name, workflowId, Collections.emptyMap());
    }

    public ExecutionError(
            String apiVersion, String name, String workflowId, Map<String, Object> details) {
        super(apiVersion);
        addMetadata(NAME_KEY, name);
        addMetadata(WORKFLOW_ID_KEY, workflowId);
        this.details =
                Objects.requireNonNull(
                        details,
                        "Details may not be null (use an empty collection or the alternate constructor to omit details)");
    }

    public String name() {
        return (String) getMetadata().get(NAME_KEY);
    }

    public String workflowId() {
        return (String) getMetadata().get(WORKFLOW_ID_KEY);
    }

    public Map<String, Object> getDetails() {
        return details;
    }
}
