/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;

import org.jooq.DSLContext;
import org.jooq.Table;
import org.springframework.context.MessageSource;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;

public class TestSuiteAutomatedSuiteGrid extends AbstractAutomatedSuiteGrid {

    private final Long testSuiteId;
    private final AutomatedSuiteDisplayDao automatedSuiteDisplayDao;
    private final PermissionEvaluationService permissionEvaluationService;

    public TestSuiteAutomatedSuiteGrid(
            Long testSuiteId,
            MessageSource messageSource,
            AutomatedSuiteDisplayDao automatedSuiteDisplayDao,
            PermissionEvaluationService permissionEvaluationService) {
        super(messageSource);
        this.testSuiteId = testSuiteId;
        this.automatedSuiteDisplayDao = automatedSuiteDisplayDao;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    @Override
    protected Long countRows(DSLContext dslContext, GridRequest request) {
        if (request.getFilterValues().isEmpty() && currentUserCanReadUnassigned(testSuiteId)) {
            return automatedSuiteDisplayDao.countAutomatedSuiteByTestSuiteId(testSuiteId);
        } else {
            return super.countRows(dslContext, request);
        }
    }

    @Override
    protected Table<?> getTable() {
        return getSelectFields()
                .from(AUTOMATED_SUITE)
                .join(TEST_SUITE)
                .on(AUTOMATED_SUITE.TEST_SUITE_ID.eq(TEST_SUITE.ID))
                .join(ITERATION_TEST_SUITE)
                .on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                .join(CAMPAIGN_ITERATION)
                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .join(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(TEST_SUITE.ID.eq(testSuiteId))
                .union(
                        getSelectFields()
                                .from(AUTOMATED_SUITE)
                                .join(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .join(ITEM_TEST_PLAN_EXECUTION)
                                .on(
                                        AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(
                                                ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .join(TEST_SUITE_TEST_PLAN_ITEM)
                                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                                .join(ITERATION_TEST_SUITE)
                                .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                                .join(CAMPAIGN_ITERATION)
                                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                                .join(CAMPAIGN_LIBRARY_NODE)
                                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                .where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(testSuiteId)))
                .asTable();
    }

    private boolean currentUserCanReadUnassigned(Long testSuiteId) {
        return this.permissionEvaluationService.hasRoleOrPermissionOnObject(
                Roles.ROLE_ADMIN,
                Permissions.READ_UNASSIGNED.name(),
                testSuiteId,
                TestSuite.SIMPLE_CLASS_NAME);
    }
}
