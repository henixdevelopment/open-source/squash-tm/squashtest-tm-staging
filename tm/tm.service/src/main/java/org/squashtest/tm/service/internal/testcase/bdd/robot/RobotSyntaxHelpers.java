/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public final class RobotSyntaxHelpers {

    public static final char NEW_LINE_CHAR = '\n';
    public static final String NEW_LINE = String.valueOf(NEW_LINE_CHAR);

    public static final String SETTING_SECTION_TITLE = "*** Settings ***";
    public static final String TEST_CASE_SECTION_TITLE = "*** Test Cases ***";
    public static final String KEYWORD_SECTION_TITLE = "*** Keywords ***";

    public static final String RETURN_RESERVED_WORD = "RETURN";
    public static final String CREATE_DICTIONARY_BUILTIN_KEYWORD = "Create Dictionary";
    public static final String CREATE_LIST_BUILTIN_KEYWORD = "Create List";
    public static final String SET_VARIABLE_BUILTIN_KEYWORD = "Set Variable";

    private RobotSyntaxHelpers() {
        throw new UnsupportedOperationException("This class is not meant to be instantiated.");
    }

    public static String scalarVariable(String name) {
        return String.format("${%s}", name);
    }

    public static String listVariable(String name) {
        return String.format("@{%s}", name);
    }

    public static String dictionaryVariable(String name) {
        return String.format("&{%s}", name);
    }

    public static String assignment(String assignee) {
        return String.format("%s =", assignee);
    }

    private static String wrappedIntoSquareBrackets(String value) {
        return String.format("[%s]", value);
    }

    public static String memberAccess(String settingName) {
        return wrappedIntoSquareBrackets(settingName);
    }

    public static String setting(String settingName) {
        return wrappedIntoSquareBrackets(settingName);
    }

    public static String setupSetting() {
        return setting("Setup");
    }

    public static String teardownSetting() {
        return setting("Teardown");
    }

    public static String documentationSetting() {
        return setting("Documentation");
    }

    public static List<List<String>> buildMultilineEntry(
            String firstColFirstRowContent, List<String> content) {
        List<List<String>> rows = new ArrayList<>();

        for (int i = 0; i < content.size(); ++i) {
            final List<String> cells = new ArrayList<>();
            cells.add(i == 0 ? firstColFirstRowContent : "...");

            if (StringUtils.isNotBlank(content.get(i))) {
                cells.add(content.get(i));
            }

            rows.add(cells);
        }

        return rows;
    }
}
