/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.servers;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.servers.OAuth2Credentials;
import org.squashtest.tm.domain.servers.ThirdPartyServer;
import org.squashtest.tm.service.internal.servers.ServerOAuth2Conf;

public class Oauth2Tokens implements ManageableCredentials {

    private static final Logger LOGGER = LoggerFactory.getLogger(Oauth2Tokens.class);

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("refresh_token")
    private String refreshToken;

    @JsonProperty("token_type")
    private String tokenType;

    @JsonProperty("expires_in")
    private Long expiresIn;

    @JsonProperty("expiration_date")
    private Long expirationDate;

    public Oauth2Tokens() {}

    public Oauth2Tokens(String accessToken, String refreshToken, String tokenType, Long expireIn) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.expiresIn = expireIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate() {
        this.expirationDate = System.currentTimeMillis() + (getExpiresIn() * 1000);
    }

    boolean isValid() {
        return !StringUtils.isAnyBlank(accessToken, refreshToken);
    }

    @Override
    public AuthenticationProtocol getImplementedProtocol() {
        return AuthenticationProtocol.OAUTH_2;
    }

    @Override
    public boolean allowsUserLevelStorage() {
        return true;
    }

    @Override
    public boolean allowsAppLevelStorage() {
        return true;
    }

    @Override
    public boolean allowsProjectLevelStorage() {
        return false;
    }

    @Override
    public Credentials build(
            StoredCredentialsManager storeManager, ThirdPartyServer server, String username) {
        LOGGER.debug("Building OAuth2Credentials");
        ServerAuthConfiguration conf =
                storeManager.unsecuredFindServerAuthConfiguration(server.getId());

        if (!isValid()) {
            LOGGER.debug(
                    "Attempted to build OAuth2 credentials for user '{}' but user tokens were invalidated and need to be recreated",
                    username);
        } else if (!canBuildWith(conf)) {
            LOGGER.error(
                    "Attempted to build OAuth2 credentials for user '{}' but could only find the user tokens. The rest of the configuration, "
                            + "usually held as app-level credentials, is absent or invalid.",
                    username);
        }
        return new OAuth2Credentials(accessToken, refreshToken, tokenType, expirationDate);
    }

    private boolean canBuildWith(ServerAuthConfiguration serverConf) {

        return (serverConf != null && ServerOAuth2Conf.class.isAssignableFrom(serverConf.getClass()));
    }
}
