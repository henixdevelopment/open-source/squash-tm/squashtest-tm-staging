/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace;

import gherkin.AstBuilder;
import gherkin.GherkinDialect;
import gherkin.GherkinDialectProvider;
import gherkin.Parser;
import gherkin.Token;
import gherkin.TokenMatcher;
import gherkin.TokenScanner;
import gherkin.ast.Background;
import gherkin.ast.Comment;
import gherkin.ast.DataTable;
import gherkin.ast.Examples;
import gherkin.ast.Feature;
import gherkin.ast.GherkinDocument;
import gherkin.ast.Location;
import gherkin.ast.Scenario;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CustomFieldXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.prittergherkin.GherkinDocumentPrinter;

public class GherkinScriptedStep {
    private static final Pattern BR_HTML_TAGS =
            Pattern.compile("(<\\s*br\\s*/\\s*>)|(<\\s*br\\s*>\\s*</\\s*br\\s*\\s*>)");
    private static final Pattern PARAMETER_GHERKIN_PATTERN = Pattern.compile("(?<!\\\\)<([^>]+)>");

    private String gherkinLanguage;
    private String featureName;
    private String scenarioName;
    private List<String> backgroundInstructions;
    private Map<Integer, Map<String, String>>
            datasetTable; // Integer : dataset row, String key : dataset name, String value : dataset

    // value

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public void setBackgroundInstructions(List<String> backgroundInstructions) {
        this.backgroundInstructions = backgroundInstructions;
    }

    public void setDatasetTable(Map<Integer, Map<String, String>> datasetTable) {
        this.datasetTable = datasetTable;
    }

    public String getGherkinLanguage() {
        return gherkinLanguage;
    }

    public void addFormattedBackground(String background, List<String> backgroundInstructions) {
        backgroundInstructions.addAll(List.of(BR_HTML_TAGS.split(background)));
    }

    public String generateGherkinScript(String language, List<CustomFieldXrayDto> gherkinStepCufs) {
        String steps = convertStepXrayToString(gherkinStepCufs);
        if (StringUtils.isNotBlank(steps)) {
            List<Token> tokens = getMatchedGherkinTokens(language, steps);
            if (tokens.isEmpty()) {
                return StringUtils.EMPTY;
            }
            gherkinLanguage = getGherkinScriptLanguage(tokens);
            GherkinKeywords gherkinKeywords = getGherkinKeywords(gherkinLanguage);
            steps = convertScriptToValidGherkinDocument(gherkinKeywords, gherkinLanguage, steps, tokens);
            // get last line to add comment with dataset not used
            int lastLine = steps.split("[\\n\\r]").length;
            Parser<GherkinDocument> parser = new Parser<>(new AstBuilder());
            GherkinDocument gherkinDocument = parser.parse(steps);
            gherkinDocument = mergeBackground(gherkinDocument, gherkinKeywords);
            gherkinDocument =
                    mergeDatasetTableGherkinDocument(gherkinDocument, gherkinKeywords, lastLine);
            return GherkinDocumentPrinter.print(gherkinDocument);
        } else {
            return StringUtils.EMPTY;
        }
    }

    // Modify xray script to valid Gherkin Document

    private String convertStepXrayToString(List<CustomFieldXrayDto> gherkinStepCufs) {
        return gherkinStepCufs.stream()
                .map(CustomFieldXrayDto::getValue)
                .filter(StringUtils::isNotEmpty)
                .map(cufValue -> BR_HTML_TAGS.matcher(cufValue).replaceAll(System.lineSeparator()))
                .collect(Collectors.joining(System.lineSeparator()));
    }

    private List<Token> getMatchedGherkinTokens(String language, String xrayScript) {
        List<Token> tokens;
        if (Objects.isNull(language)) {
            return getMatchedGherkinTokensEachLanguage(xrayScript);
        } else {
            tokens = getMatchedGherkinTokensPrecedingLanguage(xrayScript, language);
            if (tokens.isEmpty()) {
                tokens = getMatchedGherkinTokensEachLanguage(xrayScript);
            }
            return tokens;
        }
    }

    private List<Token> getMatchedGherkinTokensPrecedingLanguage(String xrayScript, String language) {
        try (StringReader stringReader = new StringReader(xrayScript)) {
            TokenScanner tokenScanner = new TokenScanner(stringReader);
            TokenMatcher tokenMatcher = new TokenMatcher(language);
            return matchGherkinTokens(tokenScanner, tokenMatcher);
        }
    }

    private List<Token> getMatchedGherkinTokensEachLanguage(String xrayScript) {
        return new GherkinDialectProvider()
                .getLanguages().stream()
                        .map(
                                language -> {
                                    try (StringReader stringReader = new StringReader(xrayScript)) {
                                        TokenScanner tokenScanner = new TokenScanner(stringReader);
                                        TokenMatcher tokenMatcher = new TokenMatcher(language);
                                        return matchGherkinTokens(tokenScanner, tokenMatcher);
                                    }
                                })
                        .filter(tokens -> !tokens.isEmpty())
                        .max(Comparator.comparingInt(List::size))
                        .orElseThrow(RuntimeException::new);
    }

    private List<Token> matchGherkinTokens(TokenScanner tokenScanner, TokenMatcher tokenMatcher) {
        List<Token> tokens = new ArrayList<>();
        Token token = tokenScanner.read();
        while (Objects.nonNull(token.line)) {
            tokenMatcher.match_FeatureLine(token);
            tokenMatcher.match_ScenarioLine(token);
            tokenMatcher.match_ScenarioOutlineLine(token);
            tokenMatcher.match_StepLine(token);
            tokenMatcher.match_ExamplesLine(token);
            tokenMatcher.match_BackgroundLine(token);
            if (Objects.nonNull(token.matchedType)) {
                tokens.add(token);
            }
            token = tokenScanner.read();
        }
        return tokens;
    }

    private String getGherkinScriptLanguage(List<Token> tokens) {
        // return max occurrence of same value token.matchedGherkinDialect.getLanguage()
        return tokens.stream()
                .map(token -> token.matchedGherkinDialect.getLanguage())
                .collect(Collectors.groupingBy(language -> language, Collectors.counting()))
                .entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .orElseThrow(RuntimeException::new);
    }

    private GherkinKeywords getGherkinKeywords(String language) {
        GherkinDialect gherkinDialect = new GherkinDialectProvider().getDialect(language, null);
        return new GherkinKeywords(gherkinDialect);
    }

    private String convertScriptToValidGherkinDocument(
            GherkinKeywords gherkinKeywords, String language, String script, List<Token> tokens) {
        Token token = tokens.get(0);
        StringBuilder stringBuilder = new StringBuilder();

        convertScriptAddGherkinLine(stringBuilder, gherkinKeywords.getLanguage(), language);
        switch (token.matchedType) {
            case FeatureLine -> stringBuilder.append(script);
            case ScenarioLine, ScenarioOutlineLine, BackgroundLine -> {
                convertScriptAddGherkinLine(stringBuilder, gherkinKeywords.getFeature(), featureName);
                stringBuilder.append(script);
            }
            default -> convertScriptHandleStepLine(stringBuilder, script, gherkinKeywords, tokens);
        }
        return stringBuilder.toString();
    }

    private void convertScriptHandleStepLine(
            StringBuilder stringBuilder,
            String script,
            GherkinKeywords gherkinKeywords,
            List<Token> tokens) {
        Optional<Token> scenarioToken =
                tokens.stream()
                        .filter(token -> token.matchedType == Parser.TokenType.ExamplesLine)
                        .findFirst();
        convertScriptAddGherkinLine(stringBuilder, gherkinKeywords.getFeature(), featureName);
        convertScriptAddGherkinLine(
                stringBuilder, gherkinKeywords.getScenario(scenarioToken.isPresent()), scenarioName);
        stringBuilder.append(script);
    }

    private void convertScriptAddGherkinLine(
            StringBuilder stringBuilder, String keyword, String value) {
        stringBuilder.append(keyword).append(":");
        if (StringUtils.isNotEmpty(value)) {
            stringBuilder.append(StringUtils.SPACE).append(value);
        }
        stringBuilder.append(System.lineSeparator());
    }

    private GherkinDocument mergeBackground(
            GherkinDocument gherkinDocument, GherkinKeywords gherkinKeywords) {
        if (backgroundInstructions == null || backgroundInstructions.isEmpty()) {
            return gherkinDocument;
        }

        List<ScenarioDefinition> children =
                new LinkedList<>(gherkinDocument.getFeature().getChildren());
        Optional<Background> backgroundOptional =
                children.stream()
                        .filter(Background.class::isInstance)
                        .map(Background.class::cast)
                        .findFirst();
        if (backgroundOptional.isPresent()) {
            Background background = backgroundOptional.get();
            List<Step> steps = new ArrayList<>(background.getSteps());
            Location lastLocation;
            if (steps.isEmpty()) {
                lastLocation = background.getLocation();
            } else {
                lastLocation = steps.get(steps.size() - 1).getLocation();
            }
            steps.addAll(
                    backgroundInstructions.stream()
                            .map(instruction -> new Step(lastLocation, "", instruction, null))
                            .toList());
            Background backgroundMerged =
                    new Background(
                            background.getLocation(),
                            background.getKeyword(),
                            background.getName(),
                            background.getDescription(),
                            steps);
            children.set(children.indexOf(background), backgroundMerged);
        } else {
            Location lastLocation = gherkinDocument.getFeature().getLocation();
            List<Step> steps =
                    backgroundInstructions.stream()
                            .map(instruction -> new Step(lastLocation, "", instruction, null))
                            .toList();
            Background backgroundMerged =
                    new Background(lastLocation, gherkinKeywords.getBackground(), null, null, steps);
            children.add(0, backgroundMerged);
        }
        backgroundInstructions.clear();
        Feature feature =
                new Feature(
                        gherkinDocument.getFeature().getTags(),
                        gherkinDocument.getFeature().getLocation(),
                        gherkinDocument.getFeature().getLanguage(),
                        gherkinDocument.getFeature().getKeyword(),
                        gherkinDocument.getFeature().getName(),
                        gherkinDocument.getFeature().getDescription(),
                        children);
        return new GherkinDocument(feature, gherkinDocument.getComments());
    }

    // Add dataset from Xray to Gherkin Document

    private GherkinDocument mergeDatasetTableGherkinDocument(
            GherkinDocument gherkinDocument, GherkinKeywords gherkinKeywords, int lastLine) {
        Set<String> matchDatasetParameters = new HashSet<>();
        if (datasetTable == null || datasetTable.isEmpty()) {
            return gherkinDocument;
        }
        // TreeSet to sort headers alphabetically
        Set<String> headers =
                datasetTable.values().stream()
                        .collect(TreeSet::new, (set, map) -> set.addAll(map.keySet()), Set::addAll);

        List<ScenarioDefinition> children =
                new LinkedList<>(gherkinDocument.getFeature().getChildren());
        for (ScenarioDefinition child : children) {
            if (child instanceof Scenario scenario) {
                matchDatasetParameters.addAll(
                        mergeDatasetHandleScenario(scenario, headers, gherkinKeywords, children));
            } else if (child instanceof ScenarioOutline scenarioOutline) {
                matchDatasetParameters.addAll(
                        mergeDatasetHandleScenarioOutline(scenarioOutline, headers, gherkinKeywords, children));
            }
        }
        headers.clear();
        clearDatasetTableElementMerged(matchDatasetParameters);
        List<Comment> comments = addDatasetNotUsedToComment(gherkinDocument, lastLine);

        Feature feature =
                new Feature(
                        gherkinDocument.getFeature().getTags(),
                        gherkinDocument.getFeature().getLocation(),
                        gherkinDocument.getFeature().getLanguage(),
                        gherkinDocument.getFeature().getKeyword(),
                        gherkinDocument.getFeature().getName(),
                        gherkinDocument.getFeature().getDescription(),
                        children);
        return new GherkinDocument(feature, comments);
    }

    private List<Comment> addDatasetNotUsedToComment(GherkinDocument gherkinDocument, int lastLine) {
        if (datasetTable.isEmpty()) {
            return gherkinDocument.getComments();
        }
        // Add 200 lines to avoid conflict added elements.
        lastLine = lastLine + 200;
        // TreeSet to sort headers alphabetically
        Set<String> headers =
                datasetTable.values().stream()
                        .collect(TreeSet::new, (set, map) -> set.addAll(map.keySet()), Set::addAll);
        Map<String, Integer> columnWidths = calculateColumnWidths(headers, datasetTable);

        List<Comment> comments = new ArrayList<>(gherkinDocument.getComments());
        comments.add(new Comment(new Location(lastLine++, 0), "# Dataset not used in the script"));
        comments.add(
                new Comment(new Location(lastLine++, 0), commentHeaderDataTable(headers, columnWidths)));
        for (Map<String, String> tableRow : datasetTable.values()) {
            comments.add(
                    new Comment(
                            new Location(lastLine++, 0), commentBodyDataTable(headers, tableRow, columnWidths)));
        }
        return comments;
    }

    private Map<String, Integer> calculateColumnWidths(
            Set<String> headers, Map<Integer, Map<String, String>> datasetTable) {
        Map<String, Integer> columnWidths = new HashMap<>();
        headers.forEach(header -> columnWidths.put(header, header.length()));
        for (Map<String, String> row : datasetTable.values()) {
            for (String header : headers) {
                String value = row.getOrDefault(header, "null");
                columnWidths.put(header, Math.max(columnWidths.get(header), value.length()));
            }
        }

        return columnWidths;
    }

    private String commentHeaderDataTable(Set<String> headers, Map<String, Integer> columnWidths) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("# |");
        headers.forEach(
                header -> {
                    String format = "%-" + columnWidths.get(header) + "s";
                    stringBuilder.append(" ").append(String.format(format, header)).append(" |");
                });
        return stringBuilder.toString();
    }

    private String commentBodyDataTable(
            Set<String> headers, Map<String, String> tableRow, Map<String, Integer> columnWidths) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("# |");
        headers.forEach(
                header -> {
                    String value = tableRow.getOrDefault(header, "null");
                    String format = "%-" + columnWidths.get(header) + "s";
                    stringBuilder.append(" ").append(String.format(format, value)).append(" |");
                });
        return stringBuilder.toString();
    }

    private Set<String> mergeDatasetHandleScenarioOutline(
            ScenarioOutline scenarioOutline,
            Set<String> headers,
            GherkinKeywords gherkinKeywords,
            List<ScenarioDefinition> children) {
        Set<String> matchDatasetParameters = new HashSet<>();
        getParametersInsideStep(scenarioOutline.getSteps(), headers, matchDatasetParameters);
        if (!matchDatasetParameters.isEmpty()) {
            List<Examples> examplesList = new ArrayList<>(scenarioOutline.getExamples());
            Location lastLocation = getLastLocationExample(examplesList, scenarioOutline);
            if (examplesList.size() == 1) {
                mergeExamples(examplesList, lastLocation, matchDatasetParameters, gherkinKeywords);
            } else {
                examplesList.add(createNewExamples(lastLocation, matchDatasetParameters, gherkinKeywords));
            }
            ScenarioOutline scenarioOutlineMerged =
                    new ScenarioOutline(
                            scenarioOutline.getTags(),
                            scenarioOutline.getLocation(),
                            gherkinKeywords.getScenario(true),
                            scenarioOutline.getName(),
                            scenarioOutline.getDescription(),
                            scenarioOutline.getSteps(),
                            examplesList);
            children.set(children.indexOf(scenarioOutline), scenarioOutlineMerged);
        }
        return matchDatasetParameters;
    }

    private Location getLastLocationExample(
            List<Examples> examplesList, ScenarioOutline scenarioOutline) {
        if (examplesList.isEmpty()) {
            return scenarioOutline.getLocation();
        } else {
            return examplesList.get(examplesList.size() - 1).getLocation();
        }
    }

    private Set<String> mergeDatasetHandleScenario(
            Scenario scenario,
            Set<String> headers,
            GherkinKeywords gherkinKeywords,
            List<ScenarioDefinition> children) {
        Set<String> matchDatasetParameters = new HashSet<>();
        getParametersInsideStep(scenario.getSteps(), headers, matchDatasetParameters);
        if (!matchDatasetParameters.isEmpty()) {
            Location lastLocation = scenario.getSteps().get(scenario.getSteps().size() - 1).getLocation();
            Examples example = createNewExamples(lastLocation, matchDatasetParameters, gherkinKeywords);
            ScenarioOutline scenarioOutline =
                    new ScenarioOutline(
                            scenario.getTags(),
                            scenario.getLocation(),
                            gherkinKeywords.getScenario(true),
                            scenario.getName(),
                            scenario.getDescription(),
                            scenario.getSteps(),
                            List.of(example));
            children.set(children.indexOf(scenario), scenarioOutline);
        }
        return matchDatasetParameters;
    }

    private void clearDatasetTableElementMerged(Set<String> matchDatasetParameters) {
        datasetTable.values().forEach(row -> row.keySet().removeIf(matchDatasetParameters::contains));
        datasetTable.values().removeIf(Map::isEmpty);
    }

    private void mergeDatasetHandleMatchParameters(
            String line, Set<String> headers, Set<String> matchDatasetParameters) {
        Matcher stepParameterMatch = PARAMETER_GHERKIN_PATTERN.matcher(line);
        while (stepParameterMatch.find()) {
            String parameter = stepParameterMatch.group(1);
            if (headers.contains(parameter)) {
                matchDatasetParameters.add(parameter);
            }
        }
    }

    private void getParametersInsideStep(
            List<Step> steps, Set<String> headers, Set<String> matchDatasetParameters) {
        for (Step step : steps) {
            mergeDatasetHandleMatchParameters(step.getText(), headers, matchDatasetParameters);
            if (step.getArgument() != null && step.getArgument() instanceof DataTable dataTable) {
                dataTable
                        .getRows()
                        .forEach(
                                row ->
                                        row.getCells()
                                                .forEach(
                                                        cell ->
                                                                mergeDatasetHandleMatchParameters(
                                                                        cell.getValue(), headers, matchDatasetParameters)));
            }
        }
    }

    private Examples createNewExamples(
            Location location, Set<String> matchDatasetParameters, GherkinKeywords gherkinKeywords) {
        List<TableCell> headerTableCells =
                matchDatasetParameters.stream()
                        .map(parameter -> new TableCell(location, parameter))
                        .toList();
        TableRow headerTable = new TableRow(location, headerTableCells);
        List<TableRow> bodyTables =
                datasetTable.values().stream()
                        .map(
                                datasetRowMap ->
                                        matchDatasetParameters.stream()
                                                .map(
                                                        parameter ->
                                                                new TableCell(
                                                                        location, datasetRowMap.getOrDefault(parameter, "null")))
                                                .toList())
                        .map(cells -> new TableRow(location, cells))
                        .toList();
        return new Examples(
                location,
                Collections.emptyList(),
                gherkinKeywords.getExample(),
                null,
                null,
                headerTable,
                bodyTables);
    }

    private void mergeExamples(
            List<Examples> examplesList,
            Location location,
            Set<String> matchDatasetParameters,
            GherkinKeywords gherkinKeywords) {
        Examples examples = examplesList.get(0);
        TableRow headerTable = examples.getTableHeader();
        Set<String> headers =
                headerTable.getCells().stream().map(TableCell::getValue).collect(Collectors.toSet());

        if (headers.containsAll(matchDatasetParameters)) {
            List<TableRow> datasetToMerge =
                    datasetTable.values().stream()
                            .map(
                                    datasetRowMap ->
                                            headerTable.getCells().stream()
                                                    .map(
                                                            cell ->
                                                                    new TableCell(
                                                                            location,
                                                                            datasetRowMap.getOrDefault(cell.getValue(), "null")))
                                                    .toList())
                            .map(cells -> new TableRow(location, cells))
                            .toList();
            List<TableRow> bodyTables = mergeBodyTable(examples, datasetToMerge);
            Examples examplesMerged =
                    new Examples(
                            examples.getLocation(),
                            examples.getTags(),
                            examples.getKeyword(),
                            examples.getName(),
                            examples.getDescription(),
                            examples.getTableHeader(),
                            bodyTables);
            examplesList.set(0, examplesMerged);

        } else {
            examplesList.add(createNewExamples(location, matchDatasetParameters, gherkinKeywords));
        }
    }

    private List<TableRow> mergeBodyTable(Examples examples, List<TableRow> datasetToMerge) {
        List<TableRow> bodyTables = new ArrayList<>(examples.getTableBody());
        datasetToMerge.stream()
                .filter(
                        row2 ->
                                bodyTables.stream()
                                        .noneMatch(row1 -> areCellsEqual(row1.getCells(), row2.getCells())))
                .forEach(bodyTables::add);
        return bodyTables;
    }

    private static boolean areCellsEqual(List<TableCell> cells1, List<TableCell> cells2) {
        if (cells1.size() != cells2.size()) {
            return false;
        }
        return cells1.stream()
                .map(TableCell::getValue)
                .toList()
                .equals(cells2.stream().map(TableCell::getValue).toList());
    }

    private record GherkinKeywords(GherkinDialect gherkinDialect) {
        private static final String LANGUAGE_TAG = "# language";

        public String getLanguage() {
            return LANGUAGE_TAG;
        }

        public String getFeature() {
            return gherkinDialect.getFeatureKeywords().get(0);
        }

        public String getBackground() {
            return gherkinDialect.getBackgroundKeywords().get(0);
        }

        public String getScenario(boolean isParameter) {
            if (isParameter) {
                return gherkinDialect.getScenarioOutlineKeywords().get(0);
            } else {
                return gherkinDialect.getScenarioKeywords().get(0);
            }
        }

        public String getExample() {
            return gherkinDialect.getExamplesKeywords().get(0);
        }
    }
}
