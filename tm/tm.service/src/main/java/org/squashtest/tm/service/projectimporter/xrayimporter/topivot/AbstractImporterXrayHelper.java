/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.xrayimporter.topivot;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.AbstractGenericItem;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception.ImportXrayException;

public abstract class AbstractImporterXrayHelper {
    private static final String ERROR_WRITING_TO_JSON =
            "Error during the generation of the pivot format while writing to the JSON file: ";

    @FunctionalInterface
    protected interface ConsumerThrowing<T, E extends Exception> {
        void accept(T t) throws E;
    }

    @FunctionalInterface
    protected interface BiConsumerThrowing<T, U, E extends Exception> {
        void accept(T t, U u) throws E;
    }

    protected static <T> Consumer<T> consumerThrowingIOException(ConsumerThrowing<T, IOException> t) {
        return i -> {
            try {
                t.accept(i);
            } catch (IOException e) {
                throw new ImportXrayException(ERROR_WRITING_TO_JSON, e);
            }
        };
    }

    protected static <T> Consumer<T> consumerThrowingRecordAndId(
            BiConsumerThrowing<T, AtomicInteger, IOException> records) {
        AtomicInteger idGenerator = new AtomicInteger(0);
        return i -> {
            try {
                records.accept(i, idGenerator);
            } catch (IOException e) {
                throw new ImportXrayException(ERROR_WRITING_TO_JSON, e);
            }
        };
    }

    protected void writeJson(
            JsonFactory jsonFactory,
            ConsumerThrowing<JsonGenerator, IOException> writeToJson,
            String fileName,
            ArchiveOutputStream<ZipArchiveEntry> archive,
            boolean isEmptyEntity)
            throws IOException {
        if (!isEmptyEntity) {
            ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(fileName);
            archive.putArchiveEntry(zipArchiveEntry);
            try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
                try (JsonGenerator jsonGenerator = jsonFactory.createGenerator(stream)) {
                    jsonGenerator.setCodec(new ObjectMapper());
                    jsonGenerator.writeStartObject();
                    writeToJson.accept(jsonGenerator);
                    jsonGenerator.writeEndObject();
                    jsonGenerator.flush();
                } catch (IOException e) {
                    throw new ImportXrayException(ERROR_WRITING_TO_JSON, e);
                }
                archive.write(stream.toByteArray());
            } finally {
                archive.closeArchiveEntry();
            }
        }
    }

    protected Document normalizeToHTMLDocument(String fieldValueToNormalize) {
        String fieldValue = "";
        if (Objects.nonNull(fieldValueToNormalize)) {
            fieldValue = fieldValueToNormalize;
        }
        Document document = Jsoup.parse(fieldValue, Parser.htmlParser());
        Element body = document.body();
        if (body.children().isEmpty() && StringUtils.isNotEmpty(fieldValue)) {
            body.appendElement("p").text(fieldValue);
        }
        return document;
    }

    protected String generateDescription(
            ItemXrayDto itemXrayDto, BiConsumer<Element, ItemXrayDto> bodyConsumer) {
        Document document = normalizeToHTMLDocument(itemXrayDto.getDescription());
        bodyConsumer.accept(document.body(), itemXrayDto);
        return document.body().children().stream().map(Node::toString).collect(Collectors.joining());
    }

    protected void addLinkPriorityStatusLabelToDescription(Element body, ItemXrayDto itemXrayDto) {
        // html build
        appendTextIfNotNull(
                itemXrayDto.getLink(),
                link ->
                        body.appendElement("p").appendChild(new Element("a").attr("href", link).text(link)));
        appendTextIfNotNull(
                itemXrayDto.getPriority(),
                priority ->
                        body.appendElement("p")
                                .text(String.format("XRay priority: %s", itemXrayDto.getPriority())));
        appendTextIfNotNull(
                itemXrayDto.getStatus(),
                status ->
                        body.appendElement("p")
                                .text(String.format("XRay status: %s", itemXrayDto.getStatus())));
        appendTextIfNotNull(
                itemXrayDto.getLabel(),
                label -> body.appendElement("p").text(String.format("XRay Labels: %s", label)));
    }

    protected void appendTextIfNotNull(String text, Consumer<String> consumer) {
        if (StringUtils.isNotEmpty(text)) {
            consumer.accept(text);
        }
    }

    // For correction called TC
    protected static Map<String, String> convertStringToMap(String mapToString) {
        mapToString = mapToString.replaceAll("(^\\{)|(}$)", StringUtils.EMPTY);
        return Arrays.stream(mapToString.split(","))
                .map(str -> StringUtils.trim(str).split("="))
                .collect(Collectors.toMap(str -> str[0], str -> str[1]));
    }

    protected <T extends AbstractGenericItem> void writeLog(PrintWriter logWriter, T item) {
        switch (item.getItemStatus()) {
            case SUCCESS -> logWriter.write(item.getMessageOnSuccess() + System.lineSeparator());
            case WARNING ->
                    item.getMessagesOnWarn()
                            .forEach(message -> logWriter.write(message + System.lineSeparator()));
            case FAILURE -> logWriter.write(item.getMessageOnError() + System.lineSeparator());
        }
    }
}
