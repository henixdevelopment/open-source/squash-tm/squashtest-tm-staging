/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

/**
 * Used when checking the data contained in a project before deleting or transforming it into a
 * template. Contains the ProjectAction enumeration which are the actions that will trigger the
 * check.
 */
public class ProjectDataInfo {

    public static final String DELETABLE_PROJECT_I18N_KEY =
            "sqtm-core.administration-workspace.projects.dialog.message.delete-project";
    public static final String NO_DELETABLE_PROJECT_I18N_KEY =
            "sqtm-core.administration-workspace.projects.dialog.message.cannot-delete-project-with-data";
    public static final String DELETABLE_PROJECT_I18N_KEY_WITH_LINKED_JOBS =
            "sqtm-core.administration-workspace.projects.dialog.message.delete-project-with-linked-jobs";
    public static final String CANNOT_COERCE_INTO_TEMPLATE_I18N_KEY =
            "sqtm-core.administration-workspace.projects.dialog.message.cannot-coerce-into-template";
    public static final String COERCE_INTO_TEMPLATE_I18N_KEY =
            "sqtm-core.administration-workspace.projects.dialog.message.coerce-into-template";

    public enum ProjectAction {
        TRANSFORMATION,
        DELETION
    }

    private boolean hasData;
    private String i18nMessageKey;

    public ProjectDataInfo(boolean isDeletable, String i18nMessageKey) {
        this.hasData = isDeletable;
        this.i18nMessageKey = i18nMessageKey;
    }

    public ProjectDataInfo() {}

    public boolean isHasData() {
        return hasData;
    }

    public void setHasData(boolean hasData) {
        this.hasData = hasData;
    }

    public String getI18nMessageKey() {
        return i18nMessageKey;
    }

    public void setI18nMessageKey(String i18nMessageKey) {
        this.i18nMessageKey = i18nMessageKey;
    }
}
