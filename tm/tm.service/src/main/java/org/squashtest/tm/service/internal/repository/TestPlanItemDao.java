/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem;

import java.util.List;

public interface TestPlanItemDao extends JpaRepository<TestPlanItem, Long> {
    @Query(
        value = """
        select tpi
        from TestPlanItem tpi
        left join fetch tpi.referencedTestCase
        left join fetch tpi.referencedDataset
        join fetch tpi.testPlan tp
        where tpi.id = :id
        """
    )
    TestPlanItem findByIdWithReferencedTestCaseAndDataset(@Param("id") Long id);

    @Query("""
        select tpi
        from TestPlanItem tpi
        left join fetch tpi.executions
        where tpi.referencedTestCase.id in :testCaseIds
    """)
    List<TestPlanItem> findByReferencedTestCasesWithExecutions(@Param("testCaseIds") List<Long> testCaseIds);
}
