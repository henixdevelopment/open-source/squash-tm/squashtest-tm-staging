/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.importer;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.XlsEntityKind;
import org.squashtest.tm.exception.xlsimport.MaxConcurrentImportsReachedException;
import org.squashtest.tm.exception.xlsimport.MaxItemsPerImportExceededException;
import org.squashtest.tm.service.importer.XlsImportLimitationHandler;

@Service("XlsImportLimitationHandler")
public class XlsImportLimitationHandlerImpl implements XlsImportLimitationHandler {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(XlsImportLimitationHandlerImpl.class);
    public static final AtomicInteger IMPORT_PROCESS_COUNTER = new AtomicInteger(0);

    @Value("${squash.xls-imports.max-concurrent-imports:#{null}}")
    Integer maxConcurrentImports;

    @Value("${squash.xls-imports.max-test-cases-per-import:#{null}}")
    Integer maxTestCasesPerImport;

    @Value("${squash.xls-imports.max-test-steps-per-import:#{null}}")
    Integer maxTestStepsPerImport;

    @Value("${squash.xls-imports.max-requirements-per-import:#{null}}")
    Integer maxRequirementsPerImport;

    @Override
    public void checkIfImportSlotIsAvailable() {
        if (Objects.nonNull(maxConcurrentImports)
                && IMPORT_PROCESS_COUNTER.get() > maxConcurrentImports) {
            String message =
                    String.format(
                            "The maximum number of concurrent imports (%s) has been reached. Please try again later.",
                            maxConcurrentImports);
            LOGGER.error(message);
            throw new MaxConcurrentImportsReachedException(message);
        }
    }

    @Override
    public void incrementImportProcessCounter() {
        IMPORT_PROCESS_COUNTER.incrementAndGet();
    }

    @Override
    public void decrementImportProcessCounter() {
        IMPORT_PROCESS_COUNTER.decrementAndGet();
    }

    @Override
    public void checkMaxNumberOfTestCasesInsideXlsFile(File excelFile) {
        if (Objects.isNull(maxTestCasesPerImport)) {
            return;
        }

        // counting rows in the first sheet which corresponds to test cases
        int rowsInXlsFile = getXlsSheetRowCount(excelFile, 0);
        if (rowsInXlsFile > maxTestCasesPerImport) {
            String message =
                    String.format(
                            "The number of test cases in the Excel file (%d) exceeds the maximum allowed (%d).",
                            rowsInXlsFile, maxTestCasesPerImport);

            LOGGER.error(message);
            throw new MaxItemsPerImportExceededException(
                    XlsEntityKind.TEST_CASES, rowsInXlsFile, maxTestCasesPerImport, message);
        }
    }

    @Override
    public void checkMaxNumberOfTestStepsInsideXlsFile(File excelFile) {
        if (Objects.isNull(maxTestStepsPerImport)) {
            return;
        }

        // counting rows in the second sheet which corresponds to test steps
        int rowsInXlsFile = getXlsSheetRowCount(excelFile, 1);
        if (rowsInXlsFile > maxTestStepsPerImport) {
            String message =
                    String.format(
                            "The number of test steps in the Excel file (%d) exceeds the maximum allowed (%d).",
                            rowsInXlsFile, maxTestStepsPerImport);

            LOGGER.error(message);
            throw new MaxItemsPerImportExceededException(
                    XlsEntityKind.TEST_STEPS, rowsInXlsFile, maxTestStepsPerImport, message);
        }
    }

    @Override
    public void checkMaxNumberOfRequirementsInsideXlsFile(File excelFile) {
        if (Objects.isNull(maxRequirementsPerImport)) {
            return;
        }

        // counting rows in the first sheet which corresponds to requirements
        int rowsInXlsFile = getXlsSheetRowCount(excelFile, 0);
        if (rowsInXlsFile > maxRequirementsPerImport) {
            String message =
                    String.format(
                            "The number of requirements in the Excel file (%d) exceeds the maximum allowed (%d).",
                            rowsInXlsFile, maxRequirementsPerImport);

            LOGGER.error(message);
            throw new MaxItemsPerImportExceededException(
                    XlsEntityKind.REQUIREMENTS, rowsInXlsFile, maxRequirementsPerImport, message);
        }
    }

    private int getXlsSheetRowCount(File excelFile, int sheetIndex) {
        int lineCount = 0;

        try (Workbook workbook = WorkbookFactory.create(excelFile)) {

            Sheet sheet = workbook.getSheetAt(sheetIndex);
            sheet.iterator();

            lineCount = getNonEmptyRowCount(sheet);

        } catch (IOException e) {
            LOGGER.error(
                    "Error while trying to count the number of imported entities in the Excel file ", e);
        }

        // we subtract 1 because the first row is the header
        return lineCount - 1;
    }

    // Before implementing this method, I tried to use the sheet.getPhysicalNumberOfRows() method of
    // POI (i.e. the method returned 175 rows which was correct).
    // Unfortunately when you delete rows with Excel and save the file, the method returns the
    // same value before deletion because they are still considered as physical.
    // The rows are empty but still considered as existing rows
    // (for example you delete 2 rows in Excel file, the methods continues to return
    // 175 rows instead of 173). It doesn't work with the getLastRowNum() either.
    private int getNonEmptyRowCount(Sheet sheet) {
        int nonEmptyRowCount = 0;

        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
            boolean rowEmpty = true;
            Row row = sheet.getRow(i);
            if (row != null) {
                for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
                    Cell cell = row.getCell(j);
                    if (cell != null && cell.getCellType() != CellType.BLANK) {
                        rowEmpty = false;
                        break;
                    }
                }
            }

            if (!rowEmpty) {
                nonEmptyRowCount++;
            }
        }

        return nonEmptyRowCount;
    }
}
