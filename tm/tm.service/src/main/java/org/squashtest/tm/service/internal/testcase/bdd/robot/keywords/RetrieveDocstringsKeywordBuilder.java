/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot.keywords;

import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.CREATE_DICTIONARY_BUILTIN_KEYWORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.RETURN_RESERVED_WORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.SET_VARIABLE_BUILTIN_KEYWORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.assignment;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.dictionaryVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.scalarVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.DOCSTRINGS_VARIABLE_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.DOCSTRING_NAME_FORMAT;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.FOUR_SPACES;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.RETRIEVE_DOCSTRINGS_KEYWORD_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.extractUsedDocstrings;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDocumentationSettingLines;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.isTestCaseUsingDocstrings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.service.internal.testcase.bdd.TextGridFormatter;
import org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.MultiLineStringBuilder;

public final class RetrieveDocstringsKeywordBuilder {

    private static final List<String> RETRIEVE_DOCSTRINGS_DOCUMENTATION_LINES =
            Arrays.asList(
                    "Retrieves Squash TM's docstrings and stores them in a dictionary.",
                    "",
                    "For instance, two docstrings have been defined in Squash TM,",
                    "the first one containing the string",
                    "\"I am the",
                    "FIRST    docstring\",",
                    "the second one containing the string \"I am the second docstring\"",
                    "",
                    "First, this keyword retrieves values and converts them to an inline string :",
                    "${docstring_1} =    Set Variable    I am the\\nFIRST\\tdocstring\"",
                    "",
                    "Then, this keyword stores the docstrings into the &{docstrings} dictionary",
                    "with each docstring name as key, and each docstring value as value :",
                    "${docstrings} =    Create Dictionary    docstring_1=${docstring_1}    docstring_2=${docstring_2}");

    private RetrieveDocstringsKeywordBuilder() {
        // Not meant to be instantiated
    }

    public static String buildRetrieveDocstrings(KeywordTestCase keywordTestCase) {
        final boolean isDocstringEnabled = isTestCaseUsingDocstrings(keywordTestCase);

        if (isDocstringEnabled) {
            final MultiLineStringBuilder stringBuilder = new MultiLineStringBuilder();
            final List<String> docstrings = extractUsedDocstrings(keywordTestCase);

            return stringBuilder
                    .appendNewLine()
                    .appendLine(RETRIEVE_DOCSTRINGS_KEYWORD_NAME)
                    .append(formatDocumentationSettingLines(RETRIEVE_DOCSTRINGS_DOCUMENTATION_LINES))
                    .appendNewLine()
                    .append(formatDocstringScalars(docstrings))
                    .appendNewLine()
                    .append(formatDocstringsDictionaryCreation(docstrings))
                    .appendNewLine()
                    .appendLine(
                            FOUR_SPACES
                                    + RETURN_RESERVED_WORD
                                    + FOUR_SPACES
                                    + dictionaryVariable(DOCSTRINGS_VARIABLE_NAME))
                    .toString();
        } else {
            return "";
        }
    }

    private static String formatDocstringScalars(List<String> docstrings) {
        final TextGridFormatter textGridFormatter = new TextGridFormatter();

        for (int i = 0; i < docstrings.size(); ++i) {
            final String docstringContent = preprocessDocstring(docstrings.get(i));
            final String docstringName = String.format(DOCSTRING_NAME_FORMAT, i + 1);

            textGridFormatter.addRow(
                    assignment(scalarVariable(docstringName)),
                    SET_VARIABLE_BUILTIN_KEYWORD,
                    docstringContent);
        }

        return textGridFormatter.format(TextGridFormatter.withRowPrefix(FOUR_SPACES));
    }

    private static String formatDocstringsDictionaryCreation(List<String> docstrings) {
        final List<String> cells = new ArrayList<>();

        cells.add(assignment(dictionaryVariable(DOCSTRINGS_VARIABLE_NAME)));
        cells.add(CREATE_DICTIONARY_BUILTIN_KEYWORD);

        for (int i = 0; i < docstrings.size(); ++i) {
            final String docstringName = String.format(DOCSTRING_NAME_FORMAT, i + 1);
            final String entry = docstringName + "=" + scalarVariable(docstringName);
            cells.add(entry);
        }

        return new TextGridFormatter()
                .addRow(cells)
                .format(TextGridFormatter.withRowPrefix(FOUR_SPACES));
    }

    /**
     * @param docstringContent source docstring
     * @return docstringContent with tabs and line breaks escaped
     */
    private static String preprocessDocstring(String docstringContent) {
        return docstringContent.replace("\n", "\\n").replace("\t", "\\t");
    }
}
