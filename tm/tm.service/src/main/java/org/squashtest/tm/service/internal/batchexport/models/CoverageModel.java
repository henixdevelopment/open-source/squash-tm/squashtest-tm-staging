/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.Comparator;
import org.squashtest.tm.service.internal.batchexport.models.RequirementExportModel.RequirementPathSortable;

public final class CoverageModel implements RequirementPathSortable {

    public static final Comparator<CoverageModel> TC_COMPARATOR =
            new Comparator<CoverageModel>() {
                @Override
                public int compare(CoverageModel o1, CoverageModel o2) {
                    return o1.getTcPath().compareTo(o2.getTcPath());
                }
            };

    public static final Comparator<RequirementPathSortable> REQ_COMPARATOR =
            RequirementModel.COMPARATOR;

    private String reqPath;
    private int reqVersion;
    private String tcPath;
    private String requirementProjectName;
    private Long requirementId;
    private Long tcId;

    public CoverageModel(int reqVersion, Long requirementId, Long tcId, String projectName) {
        super();
        this.reqVersion = reqVersion;
        this.requirementId = requirementId;
        this.tcId = tcId;
        this.requirementProjectName = projectName;
    }

    public String getRequirementProjectName() {
        return requirementProjectName;
    }

    public void setRequirementProjectName(String projectName) {
        this.requirementProjectName = projectName;
    }

    public String getReqPath() {
        return reqPath;
    }

    public void setReqPath(String reqPath) {
        this.reqPath = reqPath;
    }

    public int getReqVersion() {
        return reqVersion;
    }

    public void setReqVersion(int reqVersion) {
        this.reqVersion = reqVersion;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public Long getTcId() {
        return tcId;
    }

    public void setTcId(Long tcId) {
        this.tcId = tcId;
    }

    public String getTcPath() {
        return tcPath;
    }

    public void setTcPath(String tcPath) {
        this.tcPath = tcPath;
    }

    @Override
    public String getProjectName() {
        return getRequirementProjectName();
    }

    @Override
    public String getPath() {
        return getReqPath();
    }

    @Override
    public int getRequirementVersionNumber() {
        return getReqVersion();
    }
}
