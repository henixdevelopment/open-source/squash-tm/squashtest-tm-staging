/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.bugtracker.knownissues.remote;

import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssue;

/**
 * Holds together data about a RemoteIssue and its report context : what bugtracker, which
 * executions it's attached to, which requirement versions it is attached to,...
 */
public class RemoteKnownIssue {
    public final RemoteIssue remoteIssue;
    public final BugTracker bugTracker;
    public final LocalKnownIssue localKnownIssue;

    public RemoteKnownIssue(
            RemoteIssue remoteIssue, BugTracker bugTracker, LocalKnownIssue localKnownIssue) {
        this.remoteIssue = remoteIssue;
        this.bugTracker = bugTracker;
        this.localKnownIssue = localKnownIssue;
    }
}
