/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.environmentvariable;

import java.util.List;
import javax.validation.constraints.NotNull;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;

public interface EnvironmentVariableManagerService {

    void persist(@NotNull EnvironmentVariable environmentVariable);

    void deleteEnvironmentVariable(List<Long> environmentVariableIds);

    void changeName(Long evId, String newName);

    void changeOptionLabel(long evid, String optionLabel, String newLabel);

    void addOption(long evId, String option);

    void removeOptions(Long evId, List<String> optionLabels);

    void changeOptionsPosition(Long environmentVariableId, Integer position, List<String> labels);
}
