/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.actionword;

import static org.squashtest.tm.domain.actionword.ActionWordTreeDefinition.LIBRARY;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.actionword.ActionWordTreeDefinition;
import org.squashtest.tm.domain.actionword.ActionWordTreeEntity;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.annotation.IsUltimateLicenseAvailable;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.internal.display.dto.actionword.ActionWordPreview;
import org.squashtest.tm.service.internal.display.dto.actionword.LinkedToKeywordStepPreviewReport;
import org.squashtest.tm.service.internal.repository.ActionWordDao;
import org.squashtest.tm.service.internal.repository.ActionWordLibraryNodeDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

@Service
@Transactional
public class ActionWordLibraryNodeServiceImpl implements ActionWordLibraryNodeService {

    @Inject protected PermissionEvaluationService permissionService;
    @Inject private FeatureManager featureManager;
    @Inject private ActionWordLibraryNodeDao actionWordLibraryNodeDao;
    @Inject private ActionWordDao actionWordDao;
    @Inject private AWLNDeletionHandler deletionHandler;
    @Inject private ActionWordLibraryNodeCopier nodeCopier;
    @Inject private ActionWordLibraryNodeMover nodeMover;

    @Override
    public ActionWordLibraryNode findActionWordLibraryNodeById(Long nodeId) {
        return actionWordLibraryNodeDao.getReferenceById(nodeId);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize(
            "hasPermission(#nodeId, 'org.squashtest.tm.domain.actionword.ActionWordLibraryNode', 'READ') "
                    + OR_HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public ActionWordLibrary findLibraryByNodeId(Long nodeId) {
        return (ActionWordLibrary) findEntityAndCheckType(nodeId, LIBRARY);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize(
            "hasPermission(#nodeId, 'org.squashtest.tm.domain.actionword.ActionWordLibraryNode', 'READ') "
                    + OR_HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public ActionWord findActionWordByNodeId(Long nodeId) {
        return (ActionWord) findEntityAndCheckType(nodeId, ActionWordTreeDefinition.ACTION_WORD);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#parentId, 'org.squashtest.tm.domain.actionword.ActionWordLibraryNode', 'CREATE') "
                    + OR_HAS_ROLE_ADMIN)
    public ActionWordLibraryNode createNewNode(Long parentId, ActionWordTreeEntity entity)
            throws NameAlreadyInUseException {
        ActionWordLibraryNode parentNode = actionWordLibraryNodeDao.getReferenceById(parentId);

        boolean tokenCaseInsensitivity =
                featureManager.isEnabled(FeatureManager.Feature.CASE_INSENSITIVE_ACTIONS);
        ActionWordLibraryNode newNode =
                new ActionWordLibraryNodeBuilder(parentNode, entity, tokenCaseInsensitivity).build();
        return actionWordLibraryNodeDao.save(newNode);
    }

    @Override
    @IsUltimateLicenseAvailable
    public boolean simulateCopyNodes(List<Long> nodeIds, long targetId) {
        List<ActionWordLibraryNode> nodes = actionWordLibraryNodeDao.findAllById(nodeIds);
        ActionWordLibraryNode target = actionWordLibraryNodeDao.getReferenceById(targetId);
        boolean caseInsensitivity =
                featureManager.isEnabled(FeatureManager.Feature.CASE_INSENSITIVE_ACTIONS);
        return nodeCopier.simulateCopyNodes(nodes, target, caseInsensitivity);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#targetId, 'org.squashtest.tm.domain.actionword.ActionWordLibraryNode', 'CREATE') "
                    + OR_HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public List<ActionWordLibraryNode> copyNodes(List<Long> nodeIds, long targetId) {
        List<ActionWordLibraryNode> nodes = actionWordLibraryNodeDao.findAllById(nodeIds);
        ActionWordLibraryNode target = actionWordLibraryNodeDao.getReferenceById(targetId);
        boolean caseInsensitivity =
                featureManager.isEnabled(FeatureManager.Feature.CASE_INSENSITIVE_ACTIONS);
        return nodeCopier.copyNodes(nodes, target, caseInsensitivity);
    }

    @Override
    public void moveNodes(List<Long> nodeIds, long targetId) {
        PermissionsUtils.checkPermission(
                permissionService,
                nodeIds,
                Permissions.DELETE.name(),
                ActionWordLibraryNode.class.getName());
        List<ActionWordLibraryNode> nodes = actionWordLibraryNodeDao.findAllById(nodeIds);
        ActionWordLibraryNode target = actionWordLibraryNodeDao.getReferenceById(targetId);
        boolean caseInsensitivity =
                featureManager.isEnabled(FeatureManager.Feature.CASE_INSENSITIVE_ACTIONS);
        nodeMover.moveNodes(nodes, target, caseInsensitivity);
    }

    @Override
    public ActionWordLibraryNode findNodeFromEntity(ActionWordTreeEntity actionWordTreeEntity) {
        return actionWordLibraryNodeDao.findNodeFromEntity(actionWordTreeEntity);
    }

    private ActionWordTreeEntity findEntityAndCheckType(
            Long nodeId, ActionWordTreeDefinition entityType) {
        ActionWordLibraryNode node = findActionWordLibraryNodeById(nodeId);

        if (node == null || node.getEntityType() != entityType) {
            String message = "The node of id %d doesn't exist or doesn't represent a %s entity.";
            throw new IllegalArgumentException(String.format(message, nodeId, entityType.getTypeName()));
        }

        ActionWordTreeEntity entity = node.getEntity();
        if (entity == null) {
            String message = "The node of id %d represents a null entity.";
            throw new IllegalArgumentException(String.format(message, nodeId));
        }
        return entity;
    }

    @Override
    public void renameNodeFromActionWord(ActionWord actionWord) {
        ActionWordLibraryNode actionWordLibraryNode = findNodeFromEntity(actionWord);
        actionWordLibraryNode.renameNode(actionWord.createWord());
    }

    @Override
    @IsUltimateLicenseAvailable
    public OperationReport delete(List<Long> nodeIds) {
        List<Long> nodeIdsToDelete =
                actionWordLibraryNodeDao.getActionWordLibraryNodeIdsWithoutSteps(nodeIds);
        PermissionsUtils.checkPermission(
                permissionService,
                nodeIdsToDelete,
                Permissions.DELETE.name(),
                ActionWordLibraryNode.class.getName());
        return deletionHandler.deleteNodes(nodeIdsToDelete);
    }

    @Override
    @IsUltimateLicenseAvailable
    public String findActionWordPathByPreviewNode(ActionWordPreview node) {
        StringBuilder result = new StringBuilder(node.actionWordName());
        addParentNameIntoNodePath(result, node.projectName());
        return result.toString();
    }

    // For now, the parent of an action word is only LIBRARY type
    private void addParentNameIntoNodePath(StringBuilder builder, String projectName) {
        builder.insert(0, projectName + "/");
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public List<String> findAllCaseInsensitiveDuplicateActions() {
        return actionWordDao.findAllCaseInsensitiveDuplicateActions();
    }

    @Override
    @IsUltimateLicenseAvailable
    public List<LinkedToKeywordStepPreviewReport> previewAffectedNodes(List<Long> nodeIds) {
        List<LinkedToKeywordStepPreviewReport> reports = new ArrayList<>();
        List<ActionWordPreview> previewNodes =
                actionWordLibraryNodeDao.fetchActionWordPreviewDtos(nodeIds);

        for (ActionWordPreview previewNode : previewNodes) {
            String nodePath = findActionWordPathByPreviewNode(previewNode);
            int keywordTestStepCount = previewNode.associatedKeywordTestStepCount();
            if (keywordTestStepCount > 0) {
                reports.add(new LinkedToKeywordStepPreviewReport(nodePath, keywordTestStepCount));
            }
        }

        return reports;
    }
}
