/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.testcase.TestCaseImportance;

public enum XrayPriority {
    LOWEST("lowest", RequirementCriticality.MINOR, TestCaseImportance.LOW),
    LOW("low", RequirementCriticality.MINOR, TestCaseImportance.LOW),
    MEDIUM("medium", RequirementCriticality.MAJOR, TestCaseImportance.MEDIUM),
    HIGH("high", RequirementCriticality.MAJOR, TestCaseImportance.HIGH),
    HIGHEST("highest", RequirementCriticality.CRITICAL, TestCaseImportance.VERY_HIGH),
    DEFAULT("", RequirementCriticality.UNDEFINED, TestCaseImportance.defaultValue());

    private final String xrayField;
    private final RequirementCriticality requirementCriticality;
    private final TestCaseImportance testCaseImportance;

    XrayPriority(
            String xrayField,
            RequirementCriticality requirementCriticality,
            TestCaseImportance testCaseImportance) {
        this.xrayField = xrayField;
        this.requirementCriticality = requirementCriticality;
        this.testCaseImportance = testCaseImportance;
    }

    public String getXrayField() {
        return xrayField;
    }

    public RequirementCriticality getRequirementCriticality() {
        return requirementCriticality;
    }

    public TestCaseImportance getTestCaseImportance() {
        return testCaseImportance;
    }

    public static String getXrayFieldMapping() {
        return Arrays.stream(XrayPriority.values())
                .filter(xrayPriority -> xrayPriority != DEFAULT)
                .map(XrayPriority::getXrayField)
                .collect(Collectors.joining(", "));
    }

    public static XrayPriority getPriority(String priority) {
        if (Objects.isNull(priority)) {
            return DEFAULT;
        }
        return Arrays.stream(XrayPriority.values())
                .filter(
                        xrayPriority ->
                                xrayPriority.getXrayField().toLowerCase().contains(priority.toLowerCase()))
                .findFirst()
                .orElse(DEFAULT);
    }
}
