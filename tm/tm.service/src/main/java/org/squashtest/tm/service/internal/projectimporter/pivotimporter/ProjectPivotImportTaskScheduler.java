/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.concurrent.DelegatingSecurityContextRunnable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus;
import org.squashtest.tm.service.internal.projectimporter.concurrency.SynchronisedSectionWrapper;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;

@Service("ProjectPivotImportTaskScheduler")
public class ProjectPivotImportTaskScheduler {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ProjectPivotImportTaskScheduler.class);

    // The delay between two imports phases represented in seconds
    private static final int DEFAULT_DELAY = 3600;
    private static final int MIN_DELAY = 30;
    public static final String PIVOT_FORMAT_IMPORT_USER = "project.importer";
    private static final String PENDING_IMPORTS_QUERY =
            "SELECT p FROM PivotFormatImport p left join fetch p.project WHERE p.status = :status";

    @Named("squashtest.tm.service.ThreadPoolTaskScheduler")
    private final TaskScheduler taskScheduler;

    private final SynchronisedSectionWrapper synchronisedSectionWrapper;
    private final Environment environment;
    private final GlobalProjectPivotImporterService globalProjectPivotImporterService;

    @PersistenceContext private EntityManager entityManager;

    public ProjectPivotImportTaskScheduler(
            TaskScheduler taskScheduler,
            SynchronisedSectionWrapper synchronisedSectionWrapper,
            Environment environment,
            GlobalProjectPivotImporterService globalProjectPivotImporterService) {
        this.taskScheduler = taskScheduler;
        this.synchronisedSectionWrapper = synchronisedSectionWrapper;
        this.environment = environment;
        this.globalProjectPivotImporterService = globalProjectPivotImporterService;
    }

    @PostConstruct
    public void handlePendingPivotFormatImports() {
        taskScheduler.scheduleWithFixedDelay(executePivotFormatImportTask(), getDelay());
    }

    private int getDelay() {
        String property = environment.getProperty("squash.project-imports.delay");
        int delay = DEFAULT_DELAY;
        if (StringUtils.isNotBlank(property)) {
            try {
                delay = Integer.parseInt(property);
                delay = Math.max(delay, MIN_DELAY);
                LOGGER.info(
                        "Found the property 'squash.project-imports.delay'. Delay between each import phases will be  : "
                                + delay
                                + " seconds.");
            } catch (NumberFormatException e) {
                LOGGER.error(
                        "Impossible to parse the property 'squash.project-imports.delay' as a number. Please provide a valid synchronisation delay.");
                throw e;
            }
        }
        delay = delay * 1000; // convert to millisecond as specified by spring
        return delay;
    }

    private Runnable executePivotFormatImportTask() {

        return new DelegatingSecurityContextRunnable(
                synchronisedSectionWrapper.wrapAsSynchronisedSection(performPivotFormatImport()));
    }

    private Runnable performPivotFormatImport() {
        return () -> {
            try {
                performCronAuthentication();
                List<PivotFormatImport> pendingImports = fetchPendingImports();

                for (PivotFormatImport pendingImport : pendingImports) {
                    if (Objects.nonNull(pendingImport.getProject())) {
                        globalProjectPivotImporterService.importInExistingProject(pendingImport);
                    } else {
                        globalProjectPivotImporterService.importProject(pendingImport);
                    }
                }
            } finally {
                SecurityContextHolder.clearContext();
            }
        };
    }

    private List<PivotFormatImport> fetchPendingImports() {
        return entityManager
                .createQuery(PENDING_IMPORTS_QUERY, PivotFormatImport.class)
                .setParameter("status", PivotFormatImportStatus.PENDING)
                .getResultList();
    }

    private void performCronAuthentication() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Performing internal authentication for project import...");
        }

        SecurityContextHolder.clearContext();

        Authentication authentication =
                new AnonymousAuthenticationToken(
                        PIVOT_FORMAT_IMPORT_USER,
                        PIVOT_FORMAT_IMPORT_USER,
                        Collections.singletonList(new SimpleGrantedAuthority(Roles.ROLE_ADMIN)));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Project importer - Authenticated as {}", PIVOT_FORMAT_IMPORT_USER);
        }
    }
}
