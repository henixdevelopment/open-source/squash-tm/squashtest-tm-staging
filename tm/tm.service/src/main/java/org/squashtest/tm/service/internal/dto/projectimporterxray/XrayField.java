/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray;

import java.util.Arrays;

public final class XrayField {
    public static final String PIVOT_FILENAME = "pivot";
    public static final String PIVOT_LOG_FILENAME = "pivot_log";

    public static final String ITEM = "item";
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String PROJECT = "project";
    public static final String DESCRIPTION = "description";
    public static final String KEY = "key";
    public static final String SUMMARY = "summary";
    public static final String PRIORITY = "priority";
    public static final String STATUS = "status";
    public static final String CREATED = "created";
    public static final String REPORTER = "reporter";
    public static final String LABEL = "label";
    public static final String CUSTOM_FIELD = "customfield";
    public static final String TYPE = "type";
    public static final String ATTRIBUTE_KEY = "key";
    public static final String ATTRIBUTE_NAME = "name";
    public static final String ISSUE_LINKS = "issuelinks";
    public static final String ISSUE_LINK_TYPE = "issuelinktype";
    public static final String INWARD_LINK = "inwardlinks";
    public static final String OUTWARD_LINK = "outwardlinks";
    public static final String ISSUE_LINK_TYPE_NAME = "name";
    public static final String ISSUE_KEY = "issuekey";

    //  PIVOT_ID
    public static final String BASE_PIVOT_ID_STORY = "RQ";
    public static final String BASE_PIVOT_ID_TEST_FOLDER = "DS";
    public static final String BASE_PIVOT_ID_TEST = "TC";
    public static final String BASE_PIVOT_ID_CALLED_TC = "CTC";
    public static final String BASE_PIVOT_ID_STEP = "AS";
    public static final String BASE_PIVOT_ID_DATASET_PARAM = "DS_P";
    public static final String BASE_PIVOT_ID_DATASET = "DS";
    public static final String BASE_PIVOT_ID_CAMPAIGN = "CP";
    public static final String BASE_PIVOT_ID_ITERATION = "IT";

    // Date
    public static final String BEGIN_DATE = "Begin Date";
    public static final String END_DATE = "End Date";
    public static final String DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss Z";

    public static final class CustomField {
        public static final String CUSTOM_FIELD_NAME = "customfieldname";
        public static final String CUSTOM_FIELD_VALUES = "customfieldvalues";
        public static final String CUSTOM_FIELD_VALUE = "customfieldvalue";
        public static final String CUSTOM_FIELD_STEPS = "steps";
        public static final String CUSTOM_FIELD_STEP = "step";
        public static final String CUSTOM_FIELD_STEP_INDEX = "index";
        public static final String CUSTOM_FIELD_STEP_ACTION = "action";
        public static final String CUSTOM_FIELD_STEP_DATA = "data";
        public static final String CUSTOM_FIELD_STEP_EXPECTED_RESULT = "expected_result";
        public static final String CUSTOM_FIELD_STEP_CALL_TEST = "call-test";
        public static final String CUSTOM_FIELD_STEP_CALL_TEST_PARAMETER = "parameter";
        public static final String CUSTOM_FIELD_DATASET = "dataset";
        public static final String CUSTOM_FIELD_DATASET_PARAMETER = "parameter";
        public static final String CUSTOM_FIELD_DATASET_ROW = "row";
        public static final String CUSTOM_FIELD_DATASET_NAME = "name";
        public static final String CUSTOM_FIELD_DATASET_VALUE = "value";

        private CustomField() {
            throw new IllegalStateException("Utility class");
        }
    }

    public enum Type {
        STORY("Story"),
        TEST_EXECUTION("Test Execution"),
        SUB_TEST_EXECUTION("Sub Test Execution"),
        TEST_SET("Test Set"),
        TEST_PLAN("Test Plan"),
        TEST("Test"),
        PRECONDITION("Pre-Condition"),
        UNSUPPORTED_ISSUE("unsupported issue");

        private final String name;

        Type(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public static Type convertType(String xrayType) {
            return Arrays.stream(Type.values())
                    .filter(type -> xrayType.toLowerCase().contains(type.name.toLowerCase()))
                    .findFirst()
                    .orElse(UNSUPPORTED_ISSUE);
        }
    }

    public enum CustomFieldKey {
        DATE_TIME("customfieldtypes:datetime"),
        PRECONDITION_VALUE("precondition-editor-custom-field"),
        TEST_ASSOCIATED_PRECONDITION("test-precondition-custom-field"),
        TEST_TYPE("xray:test-type-custom-field"),
        TEST_REPOSITORY_PATH("test-repository-path"),
        TEST_STEP_CUCUMBER("steps-editor-custom-field"),
        TEST_PLAN_ASSOCIATED_TESTS("tests-associated-with-test-plan-custom-field"),
        EXECUTION_ASSOCIATED_TESTS("testexec-tests-custom-field"),
        EXECUTION_ASSOCIATED_TEST_PLAN("xray:test-plan-custom-field"),
        TEST_ASSOCIATED_TEST_SET("test-sets-tests-custom-field"),
        GENERIC_DEFINITION("xray:path-editor-custom-field"),
        DEFAULT("default");

        private final String key;

        CustomFieldKey(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }

        public static CustomFieldKey convertKey(String xrayKey) {
            return Arrays.stream(CustomFieldKey.values())
                    .filter(key -> xrayKey.toLowerCase().contains(key.key.toLowerCase()))
                    .findFirst()
                    .orElse(DEFAULT);
        }
    }

    private XrayField() {
        throw new IllegalStateException("Utility class");
    }
}
