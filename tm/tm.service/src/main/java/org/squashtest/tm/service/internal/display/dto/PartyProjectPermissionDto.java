/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.acl.AclGroup;

/** Represents a permission on a project for a team or user. */
public class PartyProjectPermissionDto {

    long projectId;

    /**
     * Display value for the party name. For a user that's 'Firstname Lastname (Login)' and for a team
     * that's its name
     */
    String partyName;

    long partyId;

    /** Is the user active? For a team, it will always be set to true. */
    boolean active;

    AclGroup permissionGroup;

    /** Is the party a team? */
    boolean team;

    public PartyProjectPermissionDto(
            long projectId,
            String partyName,
            long partyId,
            boolean active,
            AclGroup permissionGroup,
            boolean team) {
        this.projectId = projectId;
        this.partyName = partyName;
        this.partyId = partyId;
        this.active = active;
        this.permissionGroup = permissionGroup;
        this.team = team;
    }

    /**
     * implicitly used in a jooq query from {@link
     * org.squashtest.tm.service.display.project.ProjectDisplayService} which handles <code>
     * List<PartyProjectPermissionDto> getPartyProjectPermissions(Long projectId)</code>
     */
    public PartyProjectPermissionDto(
            long projectId,
            String partyName,
            long partyId,
            boolean active,
            long groupId,
            String groupQualifiedName,
            boolean team) {
        this(projectId, partyName, partyId, active, new AclGroup(groupId, groupQualifiedName), team);
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public long getPartyId() {
        return partyId;
    }

    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public AclGroup getPermissionGroup() {
        return permissionGroup;
    }

    public void setPermissionGroup(AclGroup permissionGroup) {
        this.permissionGroup = permissionGroup;
    }

    public boolean isTeam() {
        return team;
    }

    public void setIsActive(boolean active) {
        this.active = active;
    }

    public boolean getActive() {
        return active;
    }

    public void setTeam(boolean team) {
        this.team = team;
    }
}
