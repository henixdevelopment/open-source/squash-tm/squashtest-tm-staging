/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.tf;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.inline;
import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_TEST;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_TEST_TECHNOLOGY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.AutomationRequest.AUTOMATION_REQUEST;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNED_TO;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNED_USER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AT_TECHNOLOGY_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AUTOMATED_TEST_FULL_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AUTOMATED_TEST_REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AUTOMATION_PRIORITY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AUTOMATION_REQUEST_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CHILDREN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CONFLICT_ASSOCIATION;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DEPTH;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.IS_MANUAL;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KEYWORD_TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUEST_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SCM_REPOSITORY_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SCRIPTED_TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TA_SERVER_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TCLN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TC_MILESTONE_LOCKED;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_AUTOMATION_SERVER_KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_PATH;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TRANSMITTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.UUID;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SelectHavingStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.jooq.domain.tables.TclnRelationshipClosure;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

public class AutomationRequestGrid extends AbstractGrid {
    private final List<Long> projectIds;
    private final boolean assigneeIsNull;
    private final Long assigneeId;

    public AutomationRequestGrid(List<Long> projectIds) {
        this(projectIds, false, null);
    }

    public AutomationRequestGrid(List<Long> projectIds, boolean assigneeIsNull, Long assigneeId) {
        this.projectIds = projectIds;
        this.assigneeIsNull = assigneeIsNull;
        this.assigneeId = assigneeId;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(PROJECT_ID)),
                new GridColumn(DSL.field(TCLN_ID)),
                new GridColumn(DSL.field(AUTOMATION_REQUEST_ID)),
                new GridColumn(DSL.field(PROJECT_NAME)),
                new GridColumn(DSL.field(NAME)),
                new GridColumn(DSL.field(ASSIGNED_TO)),
                new GridColumn(DSL.field(REFERENCE)),
                new GridColumn(DSL.field(AUTOMATION_PRIORITY)),
                new LevelEnumColumn(AutomationRequestStatus.class, DSL.field(REQUEST_STATUS, String.class)),
                new GridColumn(DSL.field(TRANSMITTED_ON)),
                new GridColumn(DSL.field(ASSIGNED_ON)),
                new GridColumn(DSL.field(SCRIPTED_TEST_CASE_ID)),
                new GridColumn(DSL.field(KEYWORD_TEST_CASE_ID)),
                new GridColumn(DSL.field(LOGIN)),
                new GridColumn(DSL.field(AT_TECHNOLOGY_ID)),
                new GridColumn(DSL.field(RequestAliasesConstants.AUTOMATED_TEST_TECHNOLOGY)),
                new GridColumn(DSL.field(SCM_REPOSITORY_ID)),
                new GridColumn(DSL.field(AUTOMATED_TEST_REFERENCE)),
                new GridColumn(DSL.field(CONFLICT_ASSOCIATION)),
                new GridColumn(DSL.field(UUID)),
                new GridColumn(DSL.field(AUTOMATED_TEST_FULL_NAME)),
                new GridColumn(DSL.field(ASSIGNED_USER)),
                new GridColumn(DSL.field(TC_MILESTONE_LOCKED)),
                new GridColumn(DSL.field(KIND)),
                new GridColumn(DSL.field(IS_MANUAL)),
                new GridColumn(DSL.field(TA_SERVER_ID)),
                new GridColumn(DSL.field(TEST_AUTOMATION_SERVER_KIND)),
                new GridColumn(DSL.field(TEST_CASE_PATH)));
    }

    @Override
    protected Table<?> getTable() {
        final SelectHavingStep<?> testCasePath = getTestCasePath();

        return DSL.select(
                        PROJECT.PROJECT_ID.as(PROJECT_ID),
                        TEST_CASE_LIBRARY_NODE.TCLN_ID.as(TCLN_ID),
                        AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.as(AUTOMATION_REQUEST_ID),
                        PROJECT.NAME.as(PROJECT_NAME),
                        TEST_CASE_LIBRARY_NODE.NAME.as(NAME),
                        AUTOMATION_REQUEST.ASSIGNED_TO.as(ASSIGNED_TO),
                        TEST_CASE.REFERENCE.as(REFERENCE),
                        AUTOMATION_REQUEST.AUTOMATION_PRIORITY.as(AUTOMATION_PRIORITY),
                        AUTOMATION_REQUEST.REQUEST_STATUS.as(REQUEST_STATUS),
                        AUTOMATION_REQUEST.TRANSMITTED_ON.as(TRANSMITTED_ON),
                        AUTOMATION_REQUEST.ASSIGNED_ON.as(ASSIGNED_ON),
                        SCRIPTED_TEST_CASE.TCLN_ID.as(SCRIPTED_TEST_CASE_ID),
                        KEYWORD_TEST_CASE.TCLN_ID.as(KEYWORD_TEST_CASE_ID),
                        TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY.as(LOGIN),
                        AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID.as(AT_TECHNOLOGY_ID),
                        AUTOMATED_TEST_TECHNOLOGY.NAME.as(RequestAliasesConstants.AUTOMATED_TEST_TECHNOLOGY),
                        TEST_CASE.SCM_REPOSITORY_ID.as(SCM_REPOSITORY_ID),
                        TEST_CASE.AUTOMATED_TEST_REFERENCE.as(AUTOMATED_TEST_REFERENCE),
                        AUTOMATION_REQUEST.CONFLICT_ASSOCIATION.as(CONFLICT_ASSOCIATION),
                        TEST_CASE.UUID.as(UUID),
                        DSL.concat(inline("/"), TEST_AUTOMATION_PROJECT.LABEL, inline("/"), AUTOMATED_TEST.NAME)
                                .as(AUTOMATED_TEST_FULL_NAME),
                        CORE_USER.LOGIN.as(ASSIGNED_USER),
                        countLockedMilestones().as(TC_MILESTONE_LOCKED),
                        DSL.when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), "GHERKIN")
                                .when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), "KEYWORD")
                                .otherwise("STANDARD")
                                .as(KIND),
                        AUTOMATION_REQUEST.IS_MANUAL.as(IS_MANUAL),
                        PROJECT.TA_SERVER_ID.as(TA_SERVER_ID),
                        TEST_AUTOMATION_SERVER.KIND.as(TEST_AUTOMATION_SERVER_KIND),
                        generateConcatenatedTestCasePath().as(TEST_CASE_PATH))
                .from(TEST_CASE_LIBRARY_NODE)
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .leftJoin(SCRIPTED_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                .leftJoin(KEYWORD_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                .leftJoin(AUTOMATED_TEST_TECHNOLOGY)
                .on(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.eq(AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID))
                .leftJoin(CORE_USER)
                .on(AUTOMATION_REQUEST.ASSIGNED_TO.eq(CORE_USER.PARTY_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftJoin(TEST_AUTOMATION_SERVER)
                .on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
                .leftJoin(AUTOMATED_TEST)
                .on(TEST_CASE.TA_TEST.eq(AUTOMATED_TEST.TEST_ID))
                .leftJoin(TEST_AUTOMATION_PROJECT)
                .on(AUTOMATED_TEST.PROJECT_ID.eq(TEST_AUTOMATION_PROJECT.TA_PROJECT_ID))
                .leftJoin(testCasePath)
                .on(TEST_CASE.TCLN_ID.eq(testCasePath.field(TCLN_ID, Long.class)))
                .where(whereCondition())
                .groupBy(
                        PROJECT.PROJECT_ID,
                        TEST_CASE_LIBRARY_NODE.TCLN_ID,
                        AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID,
                        TEST_CASE.TCLN_ID,
                        SCRIPTED_TEST_CASE.TCLN_ID,
                        KEYWORD_TEST_CASE.TCLN_ID,
                        AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID,
                        TEST_AUTOMATION_PROJECT.TA_PROJECT_ID,
                        CORE_USER.PARTY_ID,
                        TEST_AUTOMATION_SERVER.SERVER_ID,
                        AUTOMATED_TEST.TEST_ID)
                .asTable();
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(TCLN_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    private Condition whereCondition() {

        Condition baseCondition =
                PROJECT
                        .PROJECT_ID
                        .in(this.projectIds)
                        .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                        .and(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(AutomationWorkflowType.NATIVE.name()))
                        .and(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()));

        if (assigneeIsNull) {
            baseCondition = baseCondition.and(AUTOMATION_REQUEST.ASSIGNED_TO.isNull());
        } else if (Objects.nonNull(assigneeId)) {
            baseCondition = baseCondition.and(AUTOMATION_REQUEST.ASSIGNED_TO.eq(assigneeId));
        }

        return baseCondition;
    }

    private SelectHavingStep<?> getTestCasePath() {
        TclnRelationshipClosure children = TCLN_RELATIONSHIP_CLOSURE.as(CHILDREN);

        return DSL.select(
                        TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.as(TCLN_ID),
                        TCLN_RELATIONSHIP_CLOSURE.DEPTH.as(DEPTH),
                        TEST_CASE_LIBRARY_NODE.NAME.as(NAME))
                .from(TCLN_RELATIONSHIP_CLOSURE)
                .join(TEST_CASE_LIBRARY_NODE)
                .on(TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .join(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .join(children)
                .on(TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(children.ANCESTOR_ID))
                .leftJoin(TEST_CASE_FOLDER)
                .on(TEST_CASE_FOLDER.TCLN_ID.eq(children.DESCENDANT_ID))
                .where(TEST_CASE_FOLDER.TCLN_ID.isNull())
                .and(TCLN_RELATIONSHIP_CLOSURE.DEPTH.ne((short) 0));
    }

    private Field<String> generateConcatenatedTestCasePath() {
        final SelectHavingStep<?> testCasePath = getTestCasePath();
        final Field<String> nodePath =
                DSL.concat(
                        PROJECT.NAME,
                        val(" / "),
                        groupConcat(testCasePath.field(NAME, String.class))
                                .orderBy(testCasePath.field(DEPTH, Integer.class).desc())
                                .separator(" / "));

        return DSL.when(DSL.count(testCasePath.field(NAME, String.class)).ne(0), nodePath)
                .otherwise(PROJECT.NAME);
    }

    private Field<Integer> countLockedMilestones() {
        return DSL.select(countDistinct(MILESTONE.MILESTONE_ID))
                .from(MILESTONE)
                .join(MILESTONE_TEST_CASE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
                .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .and(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name()))
                .asField();
    }
}
