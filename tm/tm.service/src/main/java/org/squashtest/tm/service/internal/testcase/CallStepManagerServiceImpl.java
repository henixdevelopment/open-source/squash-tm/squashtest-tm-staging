/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.domain.testcase.ThrowIfNotStandardTestCaseVisitor;
import org.squashtest.tm.exception.CyclicStepCallException;
import org.squashtest.tm.exception.NonClassicStepCallException;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.CallStepImportData;
import org.squashtest.tm.service.internal.display.dto.testcase.AbstractTestStepDto;
import org.squashtest.tm.service.internal.display.dto.testcase.PasteTestStepOperationReport;
import org.squashtest.tm.service.internal.repository.CustomDatasetParamValueDao;
import org.squashtest.tm.service.internal.repository.DatasetParamValueDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.TestStepDao;
import org.squashtest.tm.service.internal.repository.display.TestStepDisplayDao;
import org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testcase.CallStepManagerService;
import org.squashtest.tm.service.testcase.DatasetModificationService;
import org.squashtest.tm.service.testcase.TestCaseCyclicCallChecker;
import org.squashtest.tm.service.testcase.TestCaseImportanceManagerService;

@Service("squashtest.tm.service.CallStepManagerService")
@Transactional
public class CallStepManagerServiceImpl
        implements CallStepManagerService, TestCaseCyclicCallChecker {

    @Inject private TestCaseDao testCaseDao;

    @Inject private TestStepDao testStepDao;

    @Inject private TestCaseCallTreeFinder callTreeFinder;

    @Inject private TestCaseImportanceManagerService testCaseImportanceManagerService;

    @Inject private DatasetModificationService datasetModificationService;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject private TestStepDisplayDao testStepDisplayDao;

    @Inject private CustomDatasetParamValueDao customDatasetParamValueDao;

    @Inject private DatasetParamValueDao datasetParamValueDao;

    @Inject private TestCaseLoader testCaseLoader;

    @PersistenceContext private EntityManager entityManager;

    @Override
    @PreAuthorize(
            "(hasPermission(#parentTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE') "
                    + "and hasPermission(#calledTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'READ')) "
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCase.class)
    public CallTestStep addCallTestStep(@Id long parentTestCaseId, long calledTestCaseId) {

        return addCallTestStepUnsecured(parentTestCaseId, calledTestCaseId);
    }

    @Override
    public CallTestStep addCallTestStepUnsecured(@Id long parentTestCaseId, long calledTestCaseId) {

        return doAddCallTestStep(parentTestCaseId, calledTestCaseId, null);
    }

    @Override
    @PreAuthorize(
            "(hasPermission(#parentTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE') "
                    + "and hasPermission(#calledTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'READ')) "
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCase.class)
    public CallTestStep addCallTestStep(@Id long parentTestCaseId, long calledTestCaseId, int index) {

        return doAddCallTestStep(parentTestCaseId, calledTestCaseId, index);
    }

    @Override
    public CallTestStep addCallTestStepUnsecured(
            long parentTestCaseId, long calledTestCaseId, int index) {

        return doAddCallTestStep(parentTestCaseId, calledTestCaseId, index);
    }

    private CallTestStep doAddCallTestStep(
            long parentTestCaseId, long calledTestCaseId, Integer index) {
        checkCyclicCalls(parentTestCaseId, calledTestCaseId);

        List<TestCase> testCases =
                testCaseLoader.load(
                        List.of(parentTestCaseId, calledTestCaseId),
                        EnumSet.of(TestCaseLoader.Options.FETCH_STEPS));

        TestCase parentTestCase =
                testCases.stream().filter(tc -> tc.getId() == parentTestCaseId).findFirst().orElseThrow();

        TestCase calledTestCase =
                testCases.stream().filter(tc -> tc.getId() == calledTestCaseId).findFirst().orElseThrow();

        CallTestStep newStep = new CallTestStep();
        newStep.setCalledTestCase(calledTestCase);
        newStep.setTestCase(parentTestCase);

        testStepDao.persist(newStep);

        if (index == null) {
            parentTestCase.addStep(newStep);
        } else {
            parentTestCase.addStep(index, newStep);
        }

        testCaseImportanceManagerService.changeImportanceIfCallStepAddedToTestCases(
                calledTestCase, parentTestCase);

        return newStep;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#parentTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE') "
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCase.class)
    public PasteTestStepOperationReport addCallTestSteps(
            @Id long parentTestCaseId, List<Long> calledTestCaseIds, Integer index) {

        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                calledTestCaseIds,
                Permissions.READ.name(),
                TestCaseLibraryNode.class.getName());

        TestCase parentTestCase =
                testCaseLoader.load(parentTestCaseId, EnumSet.of(TestCaseLoader.Options.FETCH_STEPS));

        ThrowIfNotStandardTestCaseVisitor testCaseVisitor =
                new ThrowIfNotStandardTestCaseVisitor(new NonClassicStepCallException());

        parentTestCase.accept(testCaseVisitor);

        // Todo BMS : use Library node loader
        List<TestCase> testCases =
                entityManager
                        .createQuery(
                                "select tc from TestCase tc,"
                                        + " TestCasePathEdge closure where closure.ancestorId in :ids and closure.descendantId = tc.id",
                                TestCase.class)
                        .setParameter("ids", calledTestCaseIds)
                        .getResultList();

        HibernateConfig.enableBatch(entityManager, HibernateConfig.BATCH_50);

        List<CallTestStep> newSteps = new ArrayList<>();

        for (TestCase calledTestCase : testCases) {

            checkCyclicCalls(parentTestCaseId, calledTestCase.getId());

            calledTestCase.accept(testCaseVisitor);

            CallTestStep newStep = new CallTestStep();
            newStep.setCalledTestCase(calledTestCase);
            newStep.setTestCase(parentTestCase);
            testStepDao.persist(newStep);
            newSteps.add(newStep);
            if (index != null) {
                parentTestCase.addStep(index, newStep);
            } else {
                parentTestCase.addStep(newStep);
            }

            testCaseImportanceManagerService.changeImportanceIfCallStepAddedToTestCases(
                    calledTestCase, parentTestCase);
        }

        // flush all new steps for having their ids
        entityManager.flush();
        Set<Long> ids = newSteps.stream().map(TestStep::getId).collect(Collectors.toSet());

        HibernateConfig.disabledBatch(entityManager);

        return this.craftOperationReport(parentTestCase, ids);
    }

    private PasteTestStepOperationReport craftOperationReport(TestCase testCase, Set<Long> ids) {
        PasteTestStepOperationReport operationReport = new PasteTestStepOperationReport();
        List<AbstractTestStepDto> testSteps = this.testStepDisplayDao.getTestSteps(ids);
        operationReport.addTestSteps(testSteps);
        operationReport.setTestCaseImportance(testCase.getImportance().name());
        return operationReport;
    }

    private void checkCyclicCalls(long callingTestCaseId, long calledTestCaseId) {
        // [SQUASH-1022] Checking that callingTestCase is not in call tree of
        // calledTestCase should be enough.
        checkCyclicCallOneWay(calledTestCaseId, callingTestCaseId);
    }

    private void checkCyclicCallOneWay(long callingTestCaseId, long calledTestCaseId) {
        if (callingTestCaseId == calledTestCaseId) {
            throw new CyclicStepCallException();
        }

        Set<Long> callTree = callTreeFinder.getTestCaseCallTree(callingTestCaseId);

        if (callTree.contains(calledTestCaseId)) {
            throw new CyclicStepCallException();
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#testCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'READ')"
                    + OR_HAS_ROLE_ADMIN)
    public TestCase findTestCase(long testCaseId) {
        return testCaseLoader.load(testCaseId);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'READ')"
                    + OR_HAS_ROLE_ADMIN)
    public void checkForCyclicStepCallBeforePaste(long destinationTestCaseId, String[] pastedStepId) {
        List<Long> idsAsList = parseLong(pastedStepId);
        checkForCyclicStepCallBeforePaste(destinationTestCaseId, idsAsList);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'READ')"
                    + OR_HAS_ROLE_ADMIN)
    public void checkForCyclicStepCallBeforePaste(
            long destinationTestCaseId, List<Long> pastedStepId) {
        List<Long> firstCalledTestCasesIds = testCaseDao.findCalledTestCaseOfCallSteps(pastedStepId);

        // 1> check that first called test cases are not the destination one.
        if (firstCalledTestCasesIds.contains(destinationTestCaseId)) {
            throw new CyclicStepCallException();
        }

        // 2> check that each first called test case doesn't have the destination one in it's callTree
        for (Long testCaseId : firstCalledTestCasesIds) {
            Set<Long> callTree = callTreeFinder.getTestCaseCallTree(testCaseId);
            if (callTree.contains(destinationTestCaseId)) {
                throw new CyclicStepCallException();
            }
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'READ')"
                    + OR_HAS_ROLE_ADMIN)
    public void checkForCyclicStepCallBeforePaste(Long destinationTestCaseId, Long calledTestCaseId) {

        // 1> check that first called test cases are not the destination one.
        if (calledTestCaseId.equals(destinationTestCaseId)) {
            throw new CyclicStepCallException();
        }

        // 2> check that each first called test case doesn't have the destination one in it's callTree
        Set<Long> callTree = callTreeFinder.getTestCaseCallTree(calledTestCaseId);
        if (callTree.contains(destinationTestCaseId)) {
            throw new CyclicStepCallException();
        }
    }

    private List<Long> parseLong(String[] stringArray) {
        List<Long> longList = new ArrayList<>();
        for (String aStringArray : stringArray) {
            longList.add(Long.parseLong(aStringArray));
        }
        return longList;
    }

    @Override
    @Transactional(readOnly = true)
    public void checkNoCyclicCall(TestCase testCase) throws CyclicStepCallException {
        long rootTestCaseId = testCase.getId();

        List<Long> firstCalledTestCasesIds =
                testCaseDao.findAllDistinctTestCasesIdsCalledByTestCase(rootTestCaseId);
        // 1> find first called test cases and check they are not the parent one
        if (firstCalledTestCasesIds.contains(rootTestCaseId)) {
            throw new CyclicStepCallException();
        }
        // 2> check that each first called test case doesn't have the destination one in it's callTree
        for (Long testCaseId : firstCalledTestCasesIds) {
            Set<Long> callTree = callTreeFinder.getTestCaseCallTree(testCaseId);
            if (callTree.contains(rootTestCaseId)) {
                throw new CyclicStepCallException();
            }
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#callStepId, 'org.squashtest.tm.domain.testcase.CallTestStep', 'WRITE')"
                    + OR_HAS_ROLE_ADMIN)
    public void setParameterAssignationMode(
            long callStepId, ParameterAssignationMode mode, Long datasetId) {

        // a class cast exception would be welcome if the call step id is not appropriate
        // so I let it out of a try block.
        CallTestStep step = (CallTestStep) testStepDao.findById(callStepId);
        Long callerId = step.getTestCase().getId();

        doSetParameterAssignationMode(mode, datasetId, step, callerId);
    }

    @Override
    public void setParameterAssignationModeUnsecured(
            CallTestStep callTestStep,
            Long callerTestCaseId,
            ParameterAssignationMode mode,
            Long datasetId) {
        doSetParameterAssignationMode(mode, datasetId, callTestStep, callerTestCaseId);
    }

    private void doSetParameterAssignationMode(
            ParameterAssignationMode mode, Long datasetId, CallTestStep step, Long callerId) {
        switch (mode) {
            case NOTHING:
                step.setCalledDataset(null);
                step.setDelegateParameterValues(false);
                deleteOrphanDatasetParamValuesByTestCaseCallerId(callerId);
                break;

            case DELEGATE:
                step.setCalledDataset(null);
                step.setDelegateParameterValues(true);
                break;

            case CALLED_DATASET:
                checkNullDataset(datasetId);
                step.setCalledDataset(datasetModificationService.findById(datasetId));
                step.setDelegateParameterValues(false);
                deleteOrphanDatasetParamValuesByTestCaseCallerId(callerId);
                break;

            default:
                throw new IllegalArgumentException(
                        "ParameterAssignationMode '"
                                + mode
                                + "' is not handled here, please find a dev and make him do the job");
        }

        datasetModificationService.cascadeDatasetsUpdate(callerId);
    }

    private void checkNullDataset(Long datasetId) {
        if (datasetId == null) {
            throw new IllegalArgumentException(
                    "attempted to bind no dataset (datasetid is null) to a call step, yet the parameter assignation mode is 'CALLED_DATASET'");
        }
    }

    @Override
    public void deleteOrphanDatasetParamValuesByTestCaseCallerId(Long testCaseCallerId) {
        List<Long> orphanDatasetParamValueIds =
                customDatasetParamValueDao.findOrphanDatasetParamValuesByTestCaseCallerId(testCaseCallerId);
        List<DatasetParamValue> datasetParamValuesToDelete =
                datasetParamValueDao.findAllById(orphanDatasetParamValueIds);

        if (!datasetParamValuesToDelete.isEmpty()) {
            datasetParamValueDao.deleteAll(datasetParamValuesToDelete);
        }
    }

    @Override
    public void addImportCallSteps(
            Project project, List<Long> callerTestCaseIds, List<Batch<CallStepImportData>> batches) {
        Map<Long, TestCase> callerTestCases =
                testCaseLoader
                        .load(callerTestCaseIds, EnumSet.of(TestCaseLoader.Options.FETCH_STEPS))
                        .stream()
                        .collect(Collectors.toMap(TestCaseLibraryNode::getId, Function.identity()));

        Set<Long> allCalledIds =
                batches.stream()
                        .flatMap(b -> b.getEntities().stream())
                        .map(CallStepImportData::getCalledTestCaseId)
                        .collect(Collectors.toSet());

        Map<Long, TestCase> calledTestCases =
                testCaseLoader
                        .load(
                                allCalledIds,
                                EnumSet.of(
                                        TestCaseLoader.Options.FETCH_STEPS, TestCaseLoader.Options.FETCH_DATASETS))
                        .stream()
                        .collect(Collectors.toMap(TestCaseLibraryNode::getId, Function.identity()));

        Map<TestCase, Set<Long>> calledIdsByCaller =
                batchCallStepsAddition(batches, callerTestCases, calledTestCases);

        testCaseImportanceManagerService.changeImportanceIfCallStepAddedToTestCases(calledIdsByCaller);
        updateParametersAssignation(batches);

        entityManager.flush();
        entityManager.clear();
    }

    private void updateParametersAssignation(List<Batch<CallStepImportData>> batches) {
        Set<Long> callerIds = new HashSet<>();

        for (Batch<CallStepImportData> batch : batches) {
            for (CallStepImportData callStepData : batch.getEntities()) {
                verifyCalledDatasetMode(callStepData);
                CallTestStep callStep = callStepData.getCallTestStep();
                ParameterAssignationMode mode = callStepData.getParameterMode();
                switch (mode) {
                    case NOTHING:
                        callStep.setCalledDataset(null);
                        callStep.setDelegateParameterValues(false);
                        callerIds.add(batch.getTargetId());
                        break;

                    case DELEGATE:
                        callStep.setCalledDataset(null);
                        callStep.setDelegateParameterValues(true);
                        break;

                    case CALLED_DATASET:
                        callStep.setCalledDataset(callStepData.getDataset());
                        callStep.setDelegateParameterValues(false);
                        callerIds.add(batch.getTargetId());
                        break;

                    default:
                        throw new IllegalArgumentException(
                                "ParameterAssignationMode '"
                                        + mode
                                        + "' is not handled here, please find a dev and make him do the job");
                }
            }
        }

        datasetModificationService.cascadeDatasetsUpdate(callerIds);
    }

    private static void verifyCalledDatasetMode(CallStepImportData callStepData) {
        if (!ParameterAssignationMode.CALLED_DATASET.equals(callStepData.getParameterMode())) {
            return;
        }

        TestCase called = callStepData.getCallTestStep().getCalledTestCase();
        String datasetName = callStepData.getCalledDatasetName();
        if (!Strings.isBlank(datasetName)) {
            Dataset dataset =
                    called.getDatasets().stream()
                            .filter(ds -> ds.getName().equals(datasetName))
                            .findFirst()
                            .orElse(null);

            if (dataset != null) {
                callStepData.setDataset(dataset);
            } else {
                callStepData.setParameterMode(ParameterAssignationMode.NOTHING);
            }
        }
    }

    private Map<TestCase, Set<Long>> batchCallStepsAddition(
            List<Batch<CallStepImportData>> batches,
            Map<Long, TestCase> callerTestCases,
            Map<Long, TestCase> calledTestCases) {
        Map<TestCase, Set<Long>> calledIdsByCaller = new HashMap<>();

        for (Batch<CallStepImportData> batch : batches) {
            TestCase callerTestCase = callerTestCases.get(batch.getTargetId());

            List<CallStepImportData> callSteps = batch.getEntities();
            callSteps.sort(
                    Comparator.comparing(
                            CallStepImportData::getIndex, Comparator.nullsLast(Comparator.naturalOrder())));

            for (CallStepImportData callStepData : callSteps) {
                TestCase calledTestCase = calledTestCases.get(callStepData.getCalledTestCaseId());

                Integer index = callStepData.getIndex();

                CallTestStep callTestStep = new CallTestStep();
                callTestStep.setCalledTestCase(calledTestCase);
                callTestStep.setTestCase(callerTestCase);

                testStepDao.persist(callTestStep);

                if (index != null && index >= 0 && index < callerTestCase.getSteps().size()) {
                    callerTestCase.addStep(index, callTestStep);
                } else {
                    callerTestCase.addStep(callTestStep);
                }

                callStepData.setCallTestStep(callTestStep);
            }
            calledIdsByCaller
                    .computeIfAbsent(callerTestCase, k -> new HashSet<>())
                    .add(callerTestCase.getId());
        }

        return calledIdsByCaller;
    }
}
