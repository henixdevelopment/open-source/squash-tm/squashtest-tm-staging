/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport;

import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.squashtest.tm.service.internal.batchexport.models.CoverageModel;
import org.squashtest.tm.service.internal.library.HibernatePathService;
import org.squashtest.tm.service.internal.library.PathService;
import org.squashtest.tm.service.internal.repository.hibernate.EasyConstructorResultTransformer;

public final class ExportDaoUtils {

    private ExportDaoUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static void setPathForCoverage(
            EntityManager em, PathService pathService, List<CoverageModel> coverageModels) {
        for (CoverageModel model : coverageModels) {
            model.setReqPath(
                    getRequirementPath(em, model.getRequirementId(), model.getRequirementProjectName()));
            model.setTcPath(pathService.buildTestCasePath(model.getTcId()));
        }
    }

    public static String getRequirementPath(
            EntityManager em, Long requirementId, String requirementProjectName) {
        StringBuilder sb = new StringBuilder(HibernatePathService.PATH_SEPARATOR);
        sb.append(requirementProjectName);
        sb.append(HibernatePathService.PATH_SEPARATOR);
        String pathFromFolder = getPathFromFolder(em, requirementId);
        String pathFromParents = getPathFromParentsRequirements(em, requirementId);
        sb.append(pathFromFolder);
        sb.append(pathFromParents);
        return HibernatePathService.escapePath(sb.toString());
    }

    private static String getPathFromFolder(EntityManager em, Long requirementId) {
        String result = requirementVersionQuery(em, "requirement.findReqFolderPath", requirementId, "");
        return result.isEmpty() ? result : result + HibernatePathService.PATH_SEPARATOR;
    }

    private static String getPathFromParentsRequirements(EntityManager em, Long requirementId) {
        return requirementVersionQuery(em, "requirement.findReqParentPath", requirementId, "");
    }

    @SuppressWarnings("unchecked")
    public static <R> R requirementVersionQuery(
            EntityManager em, String queryName, Long requirementId, R defaultValue) {
        Session session = getStatelessSession(em);
        Query q = session.getNamedQuery(queryName);
        q.setParameter("requirementId", requirementId);
        R result = (R) q.uniqueResult();
        return result != null ? result : defaultValue;
    }

    @SuppressWarnings("unchecked")
    public static <R> List<R> loadModels(
            EntityManager em, String queryName, List<Long> ids, String paramName, Class<R> resclass) {
        ids = !ids.isEmpty() ? ids : Collections.singletonList(-1L);
        Session session = getStatelessSession(em);
        Query q = session.getNamedQuery(queryName);
        q.setParameterList(paramName, ids, LongType.INSTANCE);
        q.setResultTransformer(new EasyConstructorResultTransformer(resclass));
        return q.list();
    }

    public static Session getStatelessSession(EntityManager em) {
        Session s = em.unwrap(Session.class);
        s.setHibernateFlushMode(FlushMode.MANUAL);
        return s;
    }
}
