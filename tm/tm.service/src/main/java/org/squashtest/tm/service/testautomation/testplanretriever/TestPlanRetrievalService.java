/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever;

import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.squashtest.tm.api.plugin.EntityType;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.customfield.TagsValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.campaign.AutomatedExecutionCreationService;
import org.squashtest.tm.service.internal.dto.AutomatedTestCaseDTO;
import org.squashtest.tm.service.internal.dto.AutomatedTestPlanDTO;
import org.squashtest.tm.service.internal.dto.ExecutionOrderDTO;
import org.squashtest.tm.service.internal.dto.TestPlanContextDTO;
import org.squashtest.tm.service.internal.dto.TriggerRequestDTO;
import org.squashtest.tm.service.internal.dto.TriggerRequestExtendedDTO;
import org.squashtest.tm.service.internal.repository.CustomFieldDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.TagDao;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig;
import org.squashtest.tm.service.internal.testautomation.testplanretriever.RestAutomatedSuiteManagerService;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NotFoundCustomFieldException;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.TestPlanException;

/**
 * @author lpoma
 */
@Service
@Transactional
public class TestPlanRetrievalService<
        T extends TriggerRequestDTO, C extends CustomFieldValuesForExec> {

    @PersistenceContext private EntityManager entityManager;

    @Inject private UltimateLicenseAvailabilityService ultimateLicenseService;

    @Inject private CustomFieldDao cufDao;

    @Inject private TagDao tagDao;

    @Inject protected IterationTestPlanDao itpiDao;

    @Inject protected RestAutomatedSuiteManagerService<C> restService;

    @Inject protected RestTestPlanFinder restTestPlanFinder;

    @Inject private EntityPathHeaderDao pathHeaderDao;

    @Inject protected ProjectDao projectDao;

    @Inject private AutomatedExecutionCreationService automatedExecutionCreationService;

    @Value("${info.app.version}")
    protected String squashTMVersion;

    @BatchPreventConcurrent(entityType = IterationTestPlanItem.class)
    public AutomatedTestPlanDTO getIterationTestPlan(
            T triggerRequest, Iteration iteration, @Ids List<Long> itemTestPlanIds)
            throws TestPlanException {
        String path = pathHeaderDao.buildIterationPathHeader(iteration.getId());

        TestPlanContextDTO testPlanContext =
                new TestPlanContextDTO(
                        EntityType.ITERATION, iteration.getName(), path, iteration.getUuid(), squashTMVersion);

        AutomatedSuite suite = restService.createAutomatedSuiteLinkedToIteration(iteration);

        Project project = projectDao.fetchByIterationIdForAutomatedExecutionCreation(iteration.getId());

        return getAutomatedTestPlanDTO(
                triggerRequest, itemTestPlanIds, suite, testPlanContext, project);
    }

    @BatchPreventConcurrent(entityType = IterationTestPlanItem.class)
    public AutomatedTestPlanDTO getTestSuiteTestPlan(
            T triggerRequest, TestSuite testSuite, @Ids List<Long> itemTestPlanIds)
            throws TestPlanException {
        String path = pathHeaderDao.buildTestSuitePathHeader(testSuite.getId());

        TestPlanContextDTO testPlanContext =
                new TestPlanContextDTO(
                        EntityType.TEST_SUITE, testSuite.getName(), path, testSuite.getUuid(), squashTMVersion);

        AutomatedSuite suite = restService.createAutomatedSuiteLinkedToTestSuite(testSuite);

        Project project = projectDao.fetchByTestSuiteForAutomatedExecutionCreation(testSuite.getId());

        return getAutomatedTestPlanDTO(
                triggerRequest, itemTestPlanIds, suite, testPlanContext, project);
    }

    private AutomatedTestPlanDTO getAutomatedTestPlanDTO(
            T triggerRequest,
            List<Long> itemIds,
            AutomatedSuite suite,
            TestPlanContextDTO testPlanContext,
            Project project)
            throws NotFoundCustomFieldException {

        Map<Long, Long> itemExecutionMap = getItemExecutionMap(triggerRequest, itemIds, suite, project);

        List<IterationTestPlanItem> items = itpiDao.fetchWithServerByIds(itemExecutionMap.keySet());

        ExecutionOrderDTO executionOrder = getExecutionOrder(items, itemExecutionMap);

        return new AutomatedTestPlanDTO(suite.getId(), executionOrder, testPlanContext);
    }

    private Map<Long, Long> getItemExecutionMap(
            T triggerRequest, List<Long> itemIds, AutomatedSuite suite, Project project)
            throws NotFoundCustomFieldException {
        HibernateConfig.enableBatch(entityManager, 10);

        Map<Long, Long> itemExecutionMap = new HashMap<>();

        List<List<Long>> partitionedIds = Lists.partition(itemIds, 10);

        for (List<Long> ids : partitionedIds) {
            List<IterationTestPlanItem> items = fetchIterationTestPlanItems(triggerRequest, ids);

            itemExecutionMap.putAll(
                    automatedExecutionCreationService.createAutomatedExecutions(suite, items, project));
        }

        return itemExecutionMap;
    }

    protected List<IterationTestPlanItem> fetchIterationTestPlanItems(
            T triggerRequest, List<Long> itemIds) throws NotFoundCustomFieldException {
        List<IterationTestPlanItem> items = itpiDao.fetchForAutomatedExecutionCreation(itemIds);
        return activateFilter(triggerRequest, items);
    }

    private List<IterationTestPlanItem> activateFilter(
            T triggerRequest, List<IterationTestPlanItem> items) throws NotFoundCustomFieldException {
        if (!ultimateLicenseService.isAvailable()) {
            return items;
        }

        TriggerRequestExtendedDTO extendedUltimateTriggerRequest =
                (TriggerRequestExtendedDTO) triggerRequest;

        if (StringUtils.hasLength(extendedUltimateTriggerRequest.getTagLabel())) {
            if (null
                    != cufDao.findByLabelAndInputType(
                            extendedUltimateTriggerRequest.getTagLabel(), InputType.TAG)) {
                items = filterByTag(extendedUltimateTriggerRequest, items);
            } else {
                throw new NotFoundCustomFieldException();
            }
        }
        return items;
    }

    /**
     * This method is used to filter the full ITPI list belonging to the given Iteration. It gets all
     * tags values linked to the Test Cases in this iteration, retains only those that correspond to
     * the value(s) given by the API client and finally returns the filtered ITPI list.
     *
     * @param triggerRequest
     * @param items
     * @return
     */
    private List<IterationTestPlanItem> filterByTag(
            TriggerRequestExtendedDTO triggerRequest, List<IterationTestPlanItem> items) {
        List<Long> testCasesIds =
                items.stream().map(item -> item.getReferencedTestCase().getId()).toList();

        List<TagsValue> tagsValues =
                getTagsValuesByTagLabelAndTestCaseIds(testCasesIds, triggerRequest.getTagLabel());
        List<String> givenValues = cleanAndSplitValueList(triggerRequest.getTagValue());

        List<Long> testCasesWithTags =
                tagsValues.stream()
                        .filter(tagsValue -> tagsValue.getValues().stream().anyMatch(givenValues::contains))
                        .map(TagsValue::getBoundEntityId)
                        .distinct()
                        .toList();

        return items.stream()
                .filter(item -> testCasesWithTags.contains(item.getReferencedTestCase().getId()))
                .toList();
    }

    private List<TagsValue> getTagsValuesByTagLabelAndTestCaseIds(
            List<Long> testCasesIds, String tagLabel) {
        return tagDao.getTagsValuesByTagLabelAndTestCaseIds(testCasesIds, tagLabel);
    }

    private List<String> cleanAndSplitValueList(String valuesAsString) {
        return Stream.of(valuesAsString.trim().split("\\|"))
                .map(String::trim)
                .filter(value -> !value.isEmpty())
                .toList();
    }

    private ExecutionOrderDTO createExecutionOrder(
            Collection<Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>>
                    testListWithParams) {
        ExecutionOrderDTO executionOrder = new ExecutionOrderDTO();
        testListWithParams.forEach(
                testWithParams -> {
                    AutomatedTestCaseDTO testCaseDTO =
                            new AutomatedTestCaseDTO(
                                    testWithParams.getLeft().getId(),
                                    testWithParams.getMiddle(),
                                    testWithParams.getRight());
                    executionOrder.getTestList().add(testCaseDTO);
                    LoggerFactory.getLogger(TestPlanRetrievalService.class)
                            .debug(
                                    "Including test {} to execution order.",
                                    testCaseDTO.getParamMap().get("TC_AUTOMATED_TEST_REFERENCE"));
                });
        return executionOrder;
    }

    private ExecutionOrderDTO getExecutionOrder(
            List<IterationTestPlanItem> items, Map<Long, Long> itemExecutionMap) {
        Collection<Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>>
                testListWithParams = restService.prepareExecutionOrder(items, itemExecutionMap);
        return createExecutionOrder(testListWithParams);
    }
}
