/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.SprintReqVersion;

import javax.persistence.QueryHint;

public interface SprintReqVersionDao extends JpaRepository<SprintReqVersion, Long> {

    /**
     * Retrieves all sprint requirement versions for a given sprint and a list of requirement version
     * ids.
     *
     * @param sprintId : the id of the sprint
     * @param requirementVersionIds : a list of requirementVersionIds
     * @return : either : - an empty list (meaning that the requirement version ids are not bound to
     *     the sprint yet) - or a list of SprintReqVersions containing the SprintReqVersion objects
     *     which link the sprint and the requirement versions.
     */
    List<SprintReqVersion> findAllBySprintIdAndRequirementVersionIdInOrderByRequirementVersionId(
            Long sprintId, List<Long> requirementVersionIds);

    @Query("""
        select sr from SprintReqVersion sr
        left join fetch sr.testPlan tp
        left join fetch tp.testPlanItems tpi
        left join fetch sr.sprint sp
        where sr.id = :sprintReqVersionId
    """)
    SprintReqVersion findByIdWithTestPlanAndSprint(@Param("sprintReqVersionId") Long sprintReqVersionId);

    @Query(
            "update SprintReqVersion sr set sr.requirementVersion = null where sr.requirementVersion.id in "
                    + "(select rv.id from RequirementVersion rv where rv.requirement.id in (:requirementIds))")
    @Modifying
    void removeReqVersionsFromSprintReqVersion(@Param("requirementIds") List<Long> requirementIds);

    @UsedInPlugin("xsquash-4-jira")
    @Query("""
        update SprintReqVersion sr
        set
            sr.mode = :#{T(org.squashtest.tm.domain.requirement.ManagementMode).NATIVE},
            sr.reference = case when sr.requirementVersion is not null then null else sr.reference end,
            sr.name = case when sr.requirementVersion is not null then null else sr.name end,
            sr.status = case when sr.requirementVersion is not null then null else sr.status end,
            sr.criticality = case when sr.requirementVersion is not null then null else sr.criticality end,
            sr.category = case when sr.requirementVersion is not null then null else sr.category end
        where sr.sprint.id
            in (select sp.id from Sprint sp where sp.remoteSynchronisation.id in (:remoteSyncIds))
    """)
    @Modifying
    void setAllSprintReqVersionToNativeByRemoteSyncIds(@Param("remoteSyncIds") List<Long> remoteSyncIds);

    @Query("""
        update SprintReqVersion sr
        set
            sr.mode = :#{T(org.squashtest.tm.domain.requirement.ManagementMode).NATIVE},
            sr.reference = case when sr.requirementVersion is not null then null else sr.reference end,
            sr.name = case when sr.requirementVersion is not null then null else sr.name end,
            sr.status = case when sr.requirementVersion is not null then null else sr.status end,
            sr.criticality = case when sr.requirementVersion is not null then null else sr.criticality end,
            sr.category = case when sr.requirementVersion is not null then null else sr.category end
        where sr.sprint.id = (:sprintId)
    """)
    @Modifying
    void setAllSprintReqVersionsToNativeBySprintId(@Param("sprintId") Long sprintId);

    @Query(
            "update SprintReqVersion sr "
                    + "set sr.reference = :reference, "
                    + "sr.name = :name, "
                    + "sr.status = :status, "
                    + "sr.criticality = :criticality, "
                    + "sr.category = :category, "
                    + "sr.description = :description "
                    + "where sr.id = :id")
    @Modifying
    void updateForDenormalization(
            @Param("id") Long id,
            @Param("reference") String reference,
            @Param("name") String name,
            @Param("status") String status,
            @Param("criticality") String criticality,
            @Param("category") String category,
            @Param("description") String description);

    @Query("""
        select sr from SprintReqVersion sr
        left join fetch sr.sprint sp
        left join fetch sr.testPlan tp
        left join fetch tp.testPlanItems tpi
        where tpi.id = :testPlanItemId
    """)
    SprintReqVersion findWithSprintByTestPlanItemId(@Param("testPlanItemId") Long testPlanItemId);

    @Query("""
        select sr from SprintReqVersion sr
        left join fetch sr.sprint sp
        left join fetch sr.testPlan tp
        left join fetch tp.testPlanItems tpi
        where tpi.id in :testPlanItemIds
    """)
    List<SprintReqVersion> findWithSprintByTestPlanItemIds(@Param("testPlanItemIds") List<Long> testPlanItemIds);

    @QueryHints({
        @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_PASS_DISTINCT_THROUGH, value = "false")
    })
    @Query("""
        select distinct sr.id from SprintReqVersion sr
        inner join sr.testPlan tp
        inner join tp.testPlanItems tpi
        inner join tpi.executions e
        where sr.id in (:sprintReqVersionIds)
    """)
    List<Long> findSprintReqVersionWithExecutions(
        @Param("sprintReqVersionIds") Collection<Long> sprintReqVersionIds);

    @Query("""
        select sr.id from SprintReqVersion sr
        where sr.sprint.id in (:squashSprintIds)
    """)
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    List<Long> findIdsBySprintIds(@Param("squashSprintIds") List<Long> squashSprintIds);
}
