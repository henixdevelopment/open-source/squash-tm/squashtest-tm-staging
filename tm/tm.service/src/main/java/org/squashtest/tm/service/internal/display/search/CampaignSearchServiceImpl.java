/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.CAMPAIGN_ID;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.ITEM_TEST_PLAN_ID;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.ITERATION_ID;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITERATION_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTED;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LATEST_EXECUTION_ID;

import com.google.common.collect.ImmutableList;
import java.util.List;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.query.NaturalJoinStyle;
import org.squashtest.tm.domain.query.QueryModel;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.display.search.CampaignSearchService;
import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.search.filter.FilterHandlers;
import org.squashtest.tm.service.internal.display.search.filter.FilterValueHandlers;
import org.squashtest.tm.service.internal.repository.ColumnPrototypeDao;
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.query.QueryProcessingService;

@Service
@Transactional(readOnly = true)
public class CampaignSearchServiceImpl extends AbstractSearchService
        implements CampaignSearchService {

    private final CustomItpiLastExecutionFilterDao customItpiLastExecutionFilterDao;

    private final DSLContext dslContext;

    public CampaignSearchServiceImpl(
            QueryProcessingService queryService,
            ColumnPrototypeDao columnPrototypeDao,
            EntityManager entityManager,
            ProjectFinder projectFinder,
            FilterHandlers filterHandlers,
            FilterValueHandlers gridFilterValueHandlers,
            CustomItpiLastExecutionFilterDao customItpiLastExecutionFilterDao,
            DSLContext dslContext) {
        super(
                queryService,
                columnPrototypeDao,
                entityManager,
                projectFinder,
                filterHandlers,
                gridFilterValueHandlers,
                dslContext);
        this.customItpiLastExecutionFilterDao = customItpiLastExecutionFilterDao;
        this.dslContext = dslContext;
    }

    @Override
    protected QueryModel prepareBaseQuery(GridRequest request) {
        QueryModel queryModel = super.prepareBaseQuery(request);
        queryModel.setJoinStyle(NaturalJoinStyle.LEFT_JOIN);
        return queryModel;
    }

    @Override
    protected List<String> getIdentifierColumnName() {
        return ImmutableList.of(ITEM_TEST_PLAN_ID, ITERATION_ID, CAMPAIGN_ID);
    }

    @Override
    protected String getDefaultSortColumnName() {
        return ITEM_TEST_PLAN_ID;
    }

    @Override
    public ResearchResult search(GridRequest gridRequest) {
        if (shouldSearchForLastExecutedScope(gridRequest)) {
            return searchForLastExecutedScope(gridRequest);
        } else {
            return super.search(gridRequest);
        }
    }

    private boolean shouldSearchForLastExecutedScope(GridRequest gridRequest) {
        return gridRequest.getFilterValues().stream()
                .anyMatch(filter -> filter.getValues().contains(LAST_EXECUTED));
    }

    private ResearchResult searchForLastExecutedScope(GridRequest gridRequest) {

        assertCurrentUserHasPermissionToSearch();

        Table<?> rankedIds = customItpiLastExecutionFilterDao.getTableForTCInDynamicScope(gridRequest);

        Field<Long> totalCountField = DSL.count().over().cast(Long.class).as(ITERATION_COUNT);
        Field<Long> latestExecutionIdField = rankedIds.field(LATEST_EXECUTION_ID, Long.class);

        int pageSize = gridRequest.getSize();
        int offset =
                gridRequest.getPage() != null && pageSize > 0 ? gridRequest.getPage() * pageSize : 0;
        List<? extends SortField<?>> sortFields = fieldTestCaseSearchSorts(rankedIds, gridRequest);

        Result<Record2<Long, Long>> result =
                dslContext
                        .select(latestExecutionIdField, totalCountField)
                        .from(rankedIds)
                        .orderBy(sortFields)
                        .limit(offset, pageSize)
                        .fetch();

        List<Long> itemIds = result.getValues(latestExecutionIdField);
        Long totalCount = result.isEmpty() ? 0L : result.get(0).get(totalCountField);

        return new ResearchResult(itemIds, totalCount);
    }

    protected EntityType getLibraryEntityType() {
        return EntityType.CAMPAIGN_LIBRARY;
    }

    protected TableField<ProjectRecord, Long> getLibraryIdField() {
        return PROJECT.CL_ID;
    }

    private List<? extends SortField<?>> fieldTestCaseSearchSorts(
            Table<?> rankedIds, GridRequest request) {
        return request.getSort().stream()
                .map(sort -> customItpiLastExecutionFilterDao.convertGridSortToOrderBy(rankedIds, sort))
                .toList();
    }
}
