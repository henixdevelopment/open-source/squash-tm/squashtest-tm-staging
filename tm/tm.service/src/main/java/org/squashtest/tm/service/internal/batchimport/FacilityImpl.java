/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.service.importer.ImportStatus;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ActionStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.CallStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetParamValueInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.Instruction;
import org.squashtest.tm.service.internal.batchimport.instruction.LinkedLowLevelRequirementInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementLinkInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.RequirementFacility;
import org.squashtest.tm.service.internal.batchimport.testcase.TestCaseFacility;

/** Implementation of batch import methods that will actually update the database. */
@Component
@Scope("prototype")
public class FacilityImpl extends AbstractEntityFacilitySupport implements Facility {

    private static final String UNEXPECTED_ERROR_DURING_CREATION =
            "unexpected error occurred when creating {} in project : {}";

    private static final Logger LOGGER = LoggerFactory.getLogger(FacilityImpl.class);

    private final RequirementFacility requirementFacility;
    private final TestCaseFacility testCaseFacility;

    public FacilityImpl(
            RequirementFacility requirementFacility,
            TestCaseFacility testCaseFacility,
            ValidationFacility injectedValidator,
            CustomFieldTransator customFieldTransator) {
        this.requirementFacility = requirementFacility;
        this.testCaseFacility = testCaseFacility;
        this.validator = injectedValidator;
        initializeCustomFieldTransator(customFieldTransator);

        this.requirementFacility.initializeValidator(injectedValidator);
        this.requirementFacility.initializeCustomFieldTransator(customFieldTransator);
        this.testCaseFacility.initializeValidator(injectedValidator);
        this.testCaseFacility.initializeCustomFieldTransator(customFieldTransator);
    }

    // ************************ public (and nice looking) code
    // **************************************

    /**
     * @see
     *     org.squashtest.tm.service.internal.batchimport.Facility#updateTestCase(TestCaseInstruction)
     */
    @Override
    public LogTrain updateTestCase(TestCaseInstruction instr) {
        return testCaseFacility.updateTestCase(instr);
    }

    @Override
    public LogTrain deleteTestCase(TestCaseTarget target) {
        return testCaseFacility.deleteTestCase(target);
    }

    @Override
    public LogTrain updateActionStep(
            TestStepTarget target, ActionTestStep testStep, Map<String, String> cufValues) {

        return testCaseFacility.updateActionStep(target, testStep, cufValues);
    }

    @Override
    public LogTrain updateCallStep(
            TestStepTarget target,
            CallTestStep testStep,
            TestCaseTarget calledTestCase,
            CallStepParamsInfo paramInfo,
            ActionTestStep actionStepBackup) {

        return testCaseFacility.updateCallStep(
                target, testStep, calledTestCase, paramInfo, actionStepBackup);
    }

    @Override
    public LogTrain deleteTestStep(TestStepTarget target) {

        return testCaseFacility.deleteTestStep(target);
    }

    @Override
    public LogTrain updateParameter(ParameterTarget target, Parameter param) {

        return testCaseFacility.updateParameter(target, param);
    }

    @Override
    public LogTrain deleteParameter(ParameterTarget target) {

        return testCaseFacility.deleteParameter(target);
    }

    @Override
    public LogTrain failsafeUpdateParameterValue(
            DatasetTarget dataset, ParameterTarget param, String value, boolean isUpdate) {

        return testCaseFacility.failsafeUpdateParameterValue(dataset, param, value, isUpdate);
    }

    @Override
    public LogTrain deleteDataset(DatasetTarget dataset) {

        return testCaseFacility.deleteDataset(dataset);
    }

    @Override
    public LogTrain deleteRequirementVersion(RequirementVersionInstruction instr) {
        return requirementFacility.deleteRequirementVersion(instr);
    }

    @Override
    public LogTrain deleteRequirementLink(RequirementLinkInstruction instr) {
        return requirementFacility.deleteRequirementLink(instr);
    }

    @Override
    public LogTrain createLinkedLowLevelRequirement(LinkedLowLevelRequirementInstruction instr) {
        return requirementFacility.createLinkedLowLevelRequirement(instr);
    }

    // --------------------- Batch operations ---------------------

    @Override
    public void createTestCases(List<TestCaseInstruction> instructions, Project project) {

        validator.createTestCases(instructions, project);

        List<TestCaseInstruction> validTestCases =
                instructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            testCaseFacility.createTestCases(validTestCases, project);
        } catch (Exception ex) {
            validTestCases.forEach(
                    i -> {
                        addUnexpectedError(i, ex);
                        validator.getModel().setNotExists(i.getTarget());
                    });

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION, "test cases", project.getName(), ex);
        }
    }

    @Override
    public void createParameters(List<ParameterInstruction> instructions, Project project) {
        validator.createParameters(instructions, project);

        List<ParameterInstruction> validParameters =
                instructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            testCaseFacility.createParameters(validParameters);
        } catch (Exception ex) {
            validParameters.forEach(i -> addUnexpectedError(i, ex));

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION, "parameters", project.getName(), ex);
        }
    }

    @Override
    public void createDatasets(List<DatasetInstruction> instructions, Project project) {
        validator.createDatasets(instructions, project);

        List<DatasetInstruction> validDatasets =
                instructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            testCaseFacility.createDatasets(validDatasets);
            validator.getModel().addDatasets(validDatasets.stream().map(Instruction::getTarget).toList());
        } catch (Exception ex) {
            validDatasets.forEach(i -> addUnexpectedError(i, ex));

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION, "datasets", project.getName(), ex);
        }
    }

    protected void addUnexpectedError(Instruction<?> instruction, Throwable ex) {
        instruction.addLogEntry(
                ImportStatus.FAILURE, Messages.ERROR_UNEXPECTED_ERROR, null, ex.getClass().getName());
    }

    @Override
    public void addActionSteps(List<ActionStepInstruction> instructions, Project project) {
        validator.addActionSteps(instructions, project);

        List<ActionStepInstruction> validSteps =
                instructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            testCaseFacility.addActionSteps(validSteps, project);
        } catch (Exception ex) {
            validSteps.forEach(i -> addUnexpectedError(i, ex));

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION,
                    "action steps",
                    project.getName(),
                    ex);
        }
    }

    @Override
    public void addCallSteps(List<CallStepInstruction> instructions, Project project) {
        testCaseFacility.addCallSteps(instructions, project);
    }

    @Override
    public void addDatasetParametersValues(
            List<DatasetParamValueInstruction> instructions, Project project) {
        validator.addDatasetParametersValues(instructions, project);

        List<DatasetParamValueInstruction> validParametersValues =
                instructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            testCaseFacility.addDatasetParametersValues(validParametersValues);
        } catch (Exception ex) {
            validParametersValues.forEach(i -> addUnexpectedError(i, ex));

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION,
                    "parameter values",
                    project.getName(),
                    ex);
        }
    }

    @Override
    public void createRequirementVersions(
            List<RequirementVersionInstruction> instructions, Project project) {

        validator.createRequirementVersions(instructions, project);

        List<RequirementVersionInstruction> createRequirementInstructions = new ArrayList<>();
        List<RequirementVersionInstruction> addVersionInstructions = new ArrayList<>();

        instructions.stream()
                .filter(i -> !i.hasCriticalErrors())
                .forEach(
                        instruction -> {
                            RequirementVersionTarget target = instruction.getTarget();
                            Long reqId = validator.getModel().getRequirementId(target);
                            if (reqId == null) {
                                createRequirementInstructions.add(instruction);
                            } else {
                                addVersionInstructions.add(instruction);
                            }
                        });

        try {
            requirementFacility.createRequirementVersions(
                    createRequirementInstructions, addVersionInstructions, project);
        } catch (Exception ex) {
            instructions.stream()
                    .filter(e -> !e.hasCriticalErrors())
                    .forEach(
                            i -> {
                                addUnexpectedError(i, ex);
                                validator.getModel().setNotExists(i.getTarget());
                            });

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION,
                    "requirement versions",
                    project.getName(),
                    ex);
        }
    }

    @Override
    public void createCoverages(List<CoverageInstruction> coverageInstructions, Project project) {
        validator.createCoverages(coverageInstructions, project);

        List<CoverageInstruction> validCoverages =
                coverageInstructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            requirementFacility.createCoverages(validCoverages);
        } catch (Exception ex) {
            validCoverages.forEach(i -> addUnexpectedError(i, ex));

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION, "coverages", project.getName(), ex);
        }
    }

    @Override
    public void updateRequirementVersions(
            List<RequirementVersionInstruction> updateInstructions, Project project) {
        validator.updateRequirementVersions(updateInstructions, project);

        List<RequirementVersionInstruction> validInstructions =
                updateInstructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            requirementFacility.updateRequirementVersions(validInstructions, project);
        } catch (Exception ex) {
            updateInstructions.stream()
                    .filter(e -> !e.hasCriticalErrors())
                    .forEach(
                            i -> {
                                addUnexpectedError(i, ex);
                                validator.getModel().setNotExists(i.getTarget());
                            });

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_WHILE_IMPORTING,
                    "requirement versions",
                    project.getName(),
                    ex);
        }
    }

    @Override
    public void createOrUpdateRequirementLinks(
            List<RequirementLinkInstruction> requirementLinkInstructions, Project project) {
        validator.createOrUpdateRequirementLinks(requirementLinkInstructions, project);

        List<RequirementLinkInstruction> validLinks =
                requirementLinkInstructions.stream().filter(i -> !i.hasCriticalErrors()).toList();

        try {
            requirementFacility.createOrUpdateLinks(validLinks);
        } catch (Exception ex) {
            validLinks.forEach(i -> addUnexpectedError(i, ex));

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION, "links", project.getName(), ex);
        }
    }
}
