/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders.common;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.TypedQuery;

public class HintOptions extends HashSet<HintOption> {

    public static final HintOptions READ_AND_CACHE =
            new HintOptions(
                    Set.of(HintOption.CACHEABLE, HintOption.NO_DISTINCT_IN_DB, HintOption.READ_ONLY));

    public static final HintOptions CACHE =
            new HintOptions(Set.of(HintOption.CACHEABLE, HintOption.NO_DISTINCT_IN_DB));

    public HintOptions() {}

    public HintOptions(Collection<HintOption> options) {
        super(options);
    }

    public <T> TypedQuery<T> apply(TypedQuery<T> query) {
        TypedQuery<T> hintedQuery = query;
        for (HintOption option : this) {
            hintedQuery = option.apply(hintedQuery);
        }
        return hintedQuery;
    }
}
