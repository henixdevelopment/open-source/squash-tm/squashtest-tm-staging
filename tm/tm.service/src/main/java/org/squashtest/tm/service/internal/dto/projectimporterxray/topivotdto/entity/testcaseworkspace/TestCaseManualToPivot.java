/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;

public class TestCaseManualToPivot extends CommonTestCase {
    @JsonIgnore private String xrayKey;

    @JsonIgnore private AtomicInteger datasetId;

    @JsonIgnore private AtomicInteger datasetParamId;

    @JsonIgnore private AtomicInteger actionStepId;

    @JsonProperty(JsonImportField.DATASET_PARAMS)
    private List<DatasetParamToPivot> datasetParams = new ArrayList<>();

    @JsonProperty(JsonImportField.DATASETS)
    private List<DatasetToPivot> datasets = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.PREREQUISITE)
    private String prerequisite;

    @JsonProperty(JsonImportField.ACTION_STEPS)
    private List<ActionStepToPivot> actionSteps = new ArrayList<>();

    public String getIdDatasetParam(String name) {
        return datasetParams.stream()
                .filter(datasetParamToPivot -> datasetParamToPivot.getName().equalsIgnoreCase(name))
                .map(DatasetParamToPivot::getPivotId)
                .findFirst()
                .orElse(null);
    }

    public String getDatasetPivotForCalledTC(Map<String, String> calledTcParameters) {
        return datasets.stream()
                .filter(datasetToPivot -> datasetToPivot.isParametersPresent(calledTcParameters))
                .map(DatasetToPivot::getPivotId)
                .findFirst()
                .orElse(null);
    }

    // Check if datasetParm already exist in Xray dataset
    public boolean isDatasetParamExist(String paramName) {
        return datasetParams.stream()
                .anyMatch(datasetParam -> datasetParam.getName().equalsIgnoreCase(paramName));
    }

    public String getXrayKey() {
        return xrayKey;
    }

    public void setXrayKey(String xrayKey) {
        this.xrayKey = xrayKey;
    }

    public List<DatasetParamToPivot> getDatasetParams() {
        return datasetParams;
    }

    public void setDatasetParams(List<DatasetParamToPivot> datasetParams) {
        this.datasetParams = datasetParams;
    }

    public List<DatasetToPivot> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<DatasetToPivot> datasets) {
        this.datasets = datasets;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public List<ActionStepToPivot> getActionSteps() {
        return actionSteps;
    }

    public void setActionSteps(List<ActionStepToPivot> actionSteps) {
        this.actionSteps = actionSteps;
    }

    public AtomicInteger getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(AtomicInteger datasetId) {
        this.datasetId = datasetId;
    }

    public AtomicInteger getDatasetParamId() {
        return datasetParamId;
    }

    public void setDatasetParamId(AtomicInteger datasetParamId) {
        this.datasetParamId = datasetParamId;
    }

    public AtomicInteger getActionStepId() {
        return actionStepId;
    }

    public void setActionStepId(AtomicInteger actionStepId) {
        this.actionStepId = actionStepId;
    }
}
