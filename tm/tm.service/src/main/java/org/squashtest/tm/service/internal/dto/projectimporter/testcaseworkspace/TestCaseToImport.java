/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.infolist.ListItemReference;
import org.squashtest.tm.domain.infolist.SystemListItem;
import org.squashtest.tm.domain.testcase.ExploratoryTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.service.internal.dto.projectimporter.AttachmentToImport;

public class TestCaseToImport {

    private static final String TC_NAT_PREFIX = "NAT_";
    private static final String TC_TYP_PREFIX = "TYP_";
    private String internalId;
    private String parentId;
    private EntityType parentType;
    private String name;
    private String testCaseKind;
    private String description;
    private String reference;
    private TestCaseImportance importance;
    private TestCaseStatus status;
    private String type;
    private String nature;
    private String prerequisite;
    private String script;
    private String charter;
    private Integer sessionDuration;
    private Map<Long, RawValue> customFields;
    private List<ActionTestStepToImport> actionTestSteps = new ArrayList<>();
    private List<KeywordTestStepToImport> keywordTestSteps = new ArrayList<>();
    private List<String> verifiedRequirementIds;

    private List<DatasetParamToImport> datasetParams = new ArrayList<>();
    private List<DatasetToImport> dataSets = new ArrayList<>();
    private List<AttachmentToImport> attachments = new ArrayList<>();

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public EntityType getParentType() {
        return parentType;
    }

    public void setParentType(EntityType parentType) {
        this.parentType = parentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTestCaseKind() {
        return testCaseKind;
    }

    public void setTestCaseKind(String testCaseKind) {
        this.testCaseKind = testCaseKind;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public TestCaseImportance getImportance() {
        return importance;
    }

    public void setImportance(TestCaseImportance importance) {
        this.importance = importance;
    }

    public TestCaseStatus getStatus() {
        return status;
    }

    public void setStatus(TestCaseStatus status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @CleanHtml
    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public Map<Long, RawValue> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<Long, RawValue> customFields) {
        this.customFields = customFields;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    @CleanHtml
    public String getCharter() {
        return charter;
    }

    public void setCharter(String charter) {
        this.charter = charter;
    }

    public Integer getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(Integer sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public List<ActionTestStepToImport> getActionTestSteps() {
        return actionTestSteps;
    }

    public void setActionTestSteps(List<ActionTestStepToImport> actionTestSteps) {
        this.actionTestSteps = actionTestSteps;
    }

    public List<KeywordTestStepToImport> getKeywordTestSteps() {
        return keywordTestSteps;
    }

    public void setKeywordTestSteps(List<KeywordTestStepToImport> keywordTestSteps) {
        this.keywordTestSteps = keywordTestSteps;
    }

    public List<String> getVerifiedRequirementIds() {
        return verifiedRequirementIds;
    }

    public void setVerifiedRequirementIds(List<String> verifiedRequirementIds) {
        this.verifiedRequirementIds = verifiedRequirementIds;
    }

    public TestCase toTestCase() {
        TestCaseKind kind = TestCaseKind.valueOf(testCaseKind);
        TestCase testCase =
                switch (kind) {
                    case STANDARD -> toStandardTestCase();
                    case KEYWORD -> toKeywordTestCase();
                    case GHERKIN -> toGherkinTestCase();
                    case EXPLORATORY -> toExploratoryTestCase();
                };

        testCase.setDescription(description);

        if (Objects.nonNull(reference)) {
            testCase.setReference(reference);
        }

        if (Objects.nonNull(importance)) {
            testCase.setImportance(importance);
        }

        if (Objects.nonNull(status)) {
            testCase.setStatus(status);
        }

        if (Objects.nonNull(nature)) {
            testCase.setNature(new ListItemReference(TC_NAT_PREFIX + nature));
        } else {
            testCase.setNature(new ListItemReference(SystemListItem.SYSTEM_TC_NATURE));
        }

        if (Objects.nonNull(type)) {
            testCase.setType(new ListItemReference(TC_TYP_PREFIX + type));
        } else {
            testCase.setType(new ListItemReference(SystemListItem.SYSTEM_TC_TYPE));
        }

        return testCase;
    }

    private TestCase toStandardTestCase() {
        TestCase testCase = new TestCase();
        testCase.setName(name);

        if (Objects.nonNull(getPrerequisite())) {
            testCase.setPrerequisite(getPrerequisite());
        }

        return testCase;
    }

    private KeywordTestCase toKeywordTestCase() {
        KeywordTestCase testCase = new KeywordTestCase();
        testCase.setName(name);
        return testCase;
    }

    private ScriptedTestCase toGherkinTestCase() {
        ScriptedTestCase testCase = new ScriptedTestCase(name);
        if (Objects.nonNull(script)) {
            testCase.setScript(script);
        }
        return testCase;
    }

    private TestCase toExploratoryTestCase() {
        ExploratoryTestCase testCase = new ExploratoryTestCase();
        testCase.setName(name);

        if (Objects.nonNull(getCharter())) {
            testCase.setCharter(getCharter());
        }

        testCase.setSessionDuration(sessionDuration);
        return testCase;
    }

    public List<DatasetParamToImport> getDatasetParams() {
        return datasetParams;
    }

    public void setDatasetParams(List<DatasetParamToImport> datasetParams) {
        this.datasetParams = datasetParams;
    }

    public List<DatasetToImport> getDataSets() {
        return dataSets;
    }

    public void setDataSets(List<DatasetToImport> dataSets) {
        this.dataSets = dataSets;
    }

    public List<AttachmentToImport> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentToImport> attachments) {
        this.attachments = attachments;
    }
}
