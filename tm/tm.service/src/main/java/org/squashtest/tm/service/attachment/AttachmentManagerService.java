/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.attachment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import org.springframework.security.access.AccessDeniedException;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.attachment.ExternalContentCoordinates;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;

public interface AttachmentManagerService extends AttachmentFinderService {

    /**
     * @param attachmentListId
     * @param rawAttachment
     * @param permissionOwner {@link EntityReference} to the owner of the permission. Sometimes it's
     *     not the holder of the attachmentList (ex: TestStep permission are checked at TestCase
     *     level.)
     * @param holderType {@link EntityType} of the holder of the attachment list
     * @return the ID of the newly created Attachment
     */
    AttachmentDto addAttachment(
            long attachmentListId,
            RawAttachment rawAttachment,
            EntityReference permissionOwner,
            EntityType holderType)
            throws IOException;

    /**
     * This method is used in plugins, this is why it's not private.
     *
     * @param attachmentListId
     * @param rawAttachment
     * @param holderType
     * @return the ID of the newly created Attachment
     * @throws IOException
     */
    @UsedInPlugin("rest-api")
    AttachmentDto addAttachment(
            long attachmentListId, RawAttachment rawAttachment, EntityType holderType) throws IOException;

    AttachmentDto addAttachment(AttachmentList attachmentList, RawAttachment rawAttachment)
            throws IOException;

    @UsedInPlugin("rest-api")
    AttachmentDto updateAttachmentContent(Attachment attachment, InputStream newContent)
            throws IOException;

    /**
     * Remove attachment from attachment list
     *
     * @param attachmentListId id of {@link AttachmentList}
     * @param attachmentIds id of {@link Attachment}
     * @param permissionOwner {@link EntityReference} to the owner of the permission. Sometimes it's
     *     not the holder of the * attachmentList (ex: TestStep permission are checked at TestCase
     *     level.)
     * @param holderType {@link EntityType} of the holder of the attachment list
     * @throws IOException
     */
    void removeListOfAttachments(
            long attachmentListId,
            List<Long> attachmentIds,
            EntityReference permissionOwner,
            EntityType holderType)
            throws IOException, AccessDeniedException;

    @UsedInPlugin("rest-api")
    void renameAttachment(long attachmentId, String newName);

    /**
     * Writes attachment content into the given stream.
     *
     * @param attachmentId
     * @param os
     * @throws IOException
     */
    void writeContent(long attachmentId, OutputStream os) throws IOException;

    /**
     * Copy content. Should only be used in case of file repository. Our nice rich domain model should
     * do the copy when needed if database repo but in case of file repository we need service to do
     * the stuff so....
     *
     * @param attachment the COPY not the source !!!. The source id is embedded as @Transient
     *     attribute in the attachment by the model at copy time. See {@link
     *     Attachment#attachmentToCopyId}
     */
    void copyContent(Attachment attachment);

    void copyContentsOnExternalRepository(AttachmentHolder attachmentHolder);

    void batchCopyContentsOnExternalRepository(List<AttachmentHolder> attachmentHolders);

    List<ExternalContentCoordinates> getListIDbyContentIdForAttachmentLists(
            List<Long> attachmentsList);

    void deleteContents(List<ExternalContentCoordinates> contentIListId);

    void removeAttachmentsAndLists(List<Long> attachmentListIds);

    List<Long> getAttachmentsListsFromRequirementFolders(List<Long> requirementLibraryNodeIds);

    List<ExternalContentCoordinates> getListPairContentIDListIDForExecutionSteps(
            Collection<ExecutionStep> executionSteps);

    List<ExternalContentCoordinates> getListPairContentIDListIDForExecutionIds(
            List<Long> executionStepsIds);

    List<ExternalContentCoordinates> getListPairContentIDListIDForAutomatedSuiteIds(
            List<String> automatedSuiteIds);

    String handleRichTextAttachments(String html, AttachmentList attachmentList);

    String copyAttachmentsFromRichText(
            String html,
            Long attachmentListId,
            Long copiedAttachmentListId,
            EntityReference entityReference);

    String updateRichTextUrlsOnEntityCopy(
            AttachmentList sourceAttachmentList, AttachmentList copyAttachmentList, String richText);

    void removeAttachmentsAndContents(
            List<Long> attachmentLists, List<ExternalContentCoordinates> externalContentCoordinates);
}
