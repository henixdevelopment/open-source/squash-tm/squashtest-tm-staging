/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;

import java.util.List;
import java.util.Set;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverviewReviewStatus;
import org.squashtest.tm.service.internal.display.dto.execution.ExploratorySessionOverviewView;
import org.squashtest.tm.service.internal.repository.display.ExploratorySessionOverviewDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class ExploratorySessionOverviewDisplayDaoImpl
        implements ExploratorySessionOverviewDisplayDao {

    private final DSLContext dslContext;

    public ExploratorySessionOverviewDisplayDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public ExploratorySessionOverviewView findById(long id) {
        return dslContext
                .select(
                        EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.as(RequestAliasesConstants.ID),
                        EXPLORATORY_SESSION_OVERVIEW.NAME,
                        EXPLORATORY_SESSION_OVERVIEW.CHARTER,
                        EXPLORATORY_SESSION_OVERVIEW.SESSION_DURATION,
                        EXPLORATORY_SESSION_OVERVIEW.REFERENCE,
                        EXPLORATORY_SESSION_OVERVIEW.DUE_DATE,
                        EXPLORATORY_SESSION_OVERVIEW.SESSION_STATUS,
                        EXPLORATORY_SESSION_OVERVIEW.COMMENTS,
                        EXPLORATORY_SESSION_OVERVIEW.ATTACHMENT_LIST_ID,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
                        CAMPAIGN_ITERATION.ITERATION_ID,
                        ITERATION_TEST_PLAN_ITEM.CREATED_BY,
                        ITERATION_TEST_PLAN_ITEM.CREATED_ON,
                        ITERATION_TEST_PLAN_ITEM.LAST_MODIFIED_BY,
                        ITERATION_TEST_PLAN_ITEM.LAST_MODIFIED_ON,
                        ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS,
                        ITERATION_TEST_PLAN_ITEM.TCLN_ID)
                .from(EXPLORATORY_SESSION_OVERVIEW)
                .join(ITEM_TEST_PLAN_LIST)
                .on(
                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID))
                .join(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                .join(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
                .join(ITERATION_TEST_PLAN_ITEM)
                .on(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID))
                .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(id))
                .fetchOneInto(ExploratorySessionOverviewView.class);
    }

    @Override
    public Long findIterationIdByOverviewId(Long overviewId) {
        return dslContext
                .select(ITEM_TEST_PLAN_LIST.ITERATION_ID)
                .from(ITEM_TEST_PLAN_LIST)
                .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                .on(
                        EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                .fetchOneInto(Long.class);
    }

    @Override
    public Long findProjectIdBySessionOverviewId(Long overviewId) {
        final Long iterationTestPlanItemId =
                dslContext
                        .select(EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID)
                        .from(EXPLORATORY_SESSION_OVERVIEW)
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                        .fetchOneInto(Long.class);

        if (iterationTestPlanItemId != null) {
            return dslContext
                    .select(PROJECT.PROJECT_ID)
                    .from(PROJECT)
                    .innerJoin(CAMPAIGN_LIBRARY_NODE)
                    .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                    .innerJoin(CAMPAIGN_ITERATION)
                    .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                    .innerJoin(ITEM_TEST_PLAN_LIST)
                    .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                    .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                    .on(
                            EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                    ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                    .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                    .fetchOneInto(Long.class);
        } else {
            return dslContext
                    .select(PROJECT.PROJECT_ID)
                    .from(PROJECT)
                    .innerJoin(TEST_PLAN)
                    .on(TEST_PLAN.CL_ID.eq(PROJECT.CL_ID))
                    .innerJoin(TEST_PLAN_ITEM)
                    .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(TEST_PLAN.TEST_PLAN_ID))
                    .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                    .on(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                    .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                    .fetchOneInto(Long.class);
        }
    }

    @Override
    public String inferReviewStatus(long overviewId) {
        Set<Boolean> executionReviewStatuses =
                dslContext
                        .selectDistinct(EXPLORATORY_EXECUTION.REVIEWED)
                        .from(EXPLORATORY_EXECUTION)
                        .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                        .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                        .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                        .on(
                                EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                        .fetchSet(EXPLORATORY_EXECUTION.REVIEWED);

        if (executionReviewStatuses.size() == 1 && executionReviewStatuses.contains(true)) {
            return ExploratorySessionOverviewReviewStatus.DONE.name();
        } else if ((executionReviewStatuses.size() == 1 && executionReviewStatuses.contains(false))
                || executionReviewStatuses.isEmpty()) {
            return ExploratorySessionOverviewReviewStatus.TO_DO.name();
        } else {
            return ExploratorySessionOverviewReviewStatus.RUNNING.name();
        }
    }

    @Override
    public Integer countExecutions(long overviewId) {
        final Long iterationTestPlanItemId =
                dslContext
                        .select(EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID)
                        .from(EXPLORATORY_SESSION_OVERVIEW)
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                        .fetchOneInto(Long.class);

        if (iterationTestPlanItemId != null) {
            return dslContext
                    .selectCount()
                    .from(EXPLORATORY_EXECUTION)
                    .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                    .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                    .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                    .on(
                            EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                    ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                    .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                    .fetchOneInto(Integer.class);
        } else {
            return dslContext
                    .selectCount()
                    .from(EXPLORATORY_EXECUTION)
                    .innerJoin(EXECUTION)
                    .on(EXECUTION.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                    .innerJoin(TEST_PLAN_ITEM)
                    .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXECUTION.TEST_PLAN_ITEM_ID))
                    .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                    .on(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                    .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                    .fetchOneInto(Integer.class);
        }
    }

    @Override
    public List<Long> findAlreadyAssignedUserIds(long overviewId) {
        final Long iterationTestPlanItemId =
                dslContext
                        .select(EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID)
                        .from(EXPLORATORY_SESSION_OVERVIEW)
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                        .fetchOneInto(Long.class);

        if (iterationTestPlanItemId != null) {
            return dslContext
                    .select(EXPLORATORY_EXECUTION.ASSIGNEE_ID)
                    .from(EXPLORATORY_EXECUTION)
                    .innerJoin(EXECUTION)
                    .on(EXECUTION.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                    .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                    .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                    .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                    .on(
                            EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                    ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                    .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                    .fetchInto(Long.class);
        } else {
            return dslContext
                    .select(EXPLORATORY_EXECUTION.ASSIGNEE_ID)
                    .from(EXPLORATORY_EXECUTION)
                    .innerJoin(EXECUTION)
                    .on(EXECUTION.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                    .innerJoin(TEST_PLAN_ITEM)
                    .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXECUTION.TEST_PLAN_ITEM_ID))
                    .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                    .on(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                    .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                    .fetchInto(Long.class);
        }
    }

    @Override
    public Integer countSessionOverviewsByTestCaseId(Long testCaseId) {
        return dslContext
                .selectCount()
                .from(EXPLORATORY_SESSION_OVERVIEW)
                .leftJoin(ITERATION_TEST_PLAN_ITEM)
                .on(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID))
                .leftJoin(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID))
                .where(
                        ITERATION_TEST_PLAN_ITEM
                                .TCLN_ID
                                .eq(testCaseId)
                                .or(TEST_PLAN_ITEM.TCLN_ID.eq(testCaseId)))
                .fetchOneInto(Integer.class);
    }
}
