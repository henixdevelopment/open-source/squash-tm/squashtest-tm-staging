/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.campaign.TestPlanStatistics;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;

public interface CustomTestSuiteDao {

    /**
     * Will fill a {@link TestPlanStatistics} bean with infos taken from the test plan of the {@link
     * TestSuite} matching the given id.
     *
     * @param testSuitId : the id of the concerned {@link TestSuite}
     * @return the filled {@link TestPlanStatistics} bean
     */
    TestPlanStatistics getTestSuiteStatistics(long testSuitId);

    Map<Long, ExecutionStatusReport> getStatusReport(List<Long> ids);

    List<Long> findAllIdsByExecutionIds(List<Long> executionIds);

    List<TestSuite> findAllByIds(Collection<Long> suiteIds);

    List<TestSuite> findTestSuitesWhereMilestoneIsNotLocked(Collection<Long> suiteIds);

    List<Long> findTestSuiteAttachmentListIds(Collection<Long> testSuiteIds);

    List<String> findTestSuitesAutomatedSuiteIds(Collection<Long> testSuiteIds);

    void removeTestSuites(Collection<Long> testSuiteIds);

    TestSuite findByUUID(String targetUUID);

    List<TestSuite> loadNodeForPaste(Collection<Long> testSuiteIds);

    boolean hasDeletedTestCaseInTestPlan(long testSuiteId);

    TestSuite loadForExecutionResume(long testSuiteId);
}
