/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.internal.display.dto.campaign.TestSuiteDto;

public interface TestSuiteDisplayDao {

    TestSuiteDto findById(long testSuiteId);

    List<NamedReference> findForIteration(Long iterationId);

    Long findIterationIdByTestsuiteId(Long testSuiteId);

    HashMap<Long, String> getExecutionStatusMap(Long testSuiteId);

    int getNbTestPlanItem(Long testSuiteId, String login);

    ExecutionStatus findTestSuiteStatusById(Long testSuiteId);

    Set<Long> findCampaignIdsBySuiteIds(Set<Long> longs);

    List<NamedReference> findNamedReferences(List<Long> targetIds);

    Map<Long, List<Long>> findExecutionIdsBySuiteIds(List<Long> targetIds);

    /**
     * Given a list of test suite IDs, find the IDs of the suites the test plan items are linked to.
     * e.g. if a suite S1 contains the item A, and the item A belongs to suites S1 and S2, then the
     * result will contain an entry with key = S1 and value = [S1, S2].
     *
     * @param testSuiteIds the list of test suite IDs
     * @return a map of test suite IDs to the IDs of the suites the contained test plan items are
     *     linked to.
     */
    Map<Long, Set<Long>> findLinkedSuites(Set<Long> testSuiteIds);
}
