/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.user;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.utils.CurrentUserHelper;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.projectfilter.ProjectFilter;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.project.ProjectFilterModificationService;

@Service("squash.api.CurrentUserHelper")
@Transactional
public class CurrentUserHelperImpl implements CurrentUserHelper {

    private final CustomProjectFinder projectFinder;
    private final ProjectFilterModificationService filterModificationService;

    public CurrentUserHelperImpl(
            CustomProjectFinder projectFinder,
            ProjectFilterModificationService filterModificationService) {
        this.projectFinder = projectFinder;
        this.filterModificationService = filterModificationService;
    }

    @Override
    public List<Long> findReadableProjectIds() {
        return projectFinder.findAllReadableIds();
    }

    @Override
    public List<Long> findFilteredReadableProjectIds() {
        ProjectFilter projectFilter = filterModificationService.findProjectFilterByUserLogin();
        if (projectFilter != null && projectFilter.getActivated()) {
            final List<Long> readableProjectIds = this.findReadableProjectIds();
            return projectFilter.getProjects().stream()
                    .map(GenericProject::getId)
                    .filter(readableProjectIds::contains)
                    .toList();
        } else {
            return this.findReadableProjectIds();
        }
    }
}
