/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

public class GridResponse {
    /** The total number of rows available for the query. */
    private long count;

    /** The name of the attribute in DataRow#data that uniquely identifies a row in the grid. */
    private String idAttribute;

    /**
     * The rows of data in the grid. When pagination is enabled, this will be a subset of the total
     * number of rows.
     */
    private List<DataRow> dataRows = new ArrayList<>();

    /**
     * The ids of the columns that are currently active in the grid. This is used to determine which
     * columns should be displayed in the grid.
     */
    private List<String> activeColumnIds;

    /**
     * If present, the page number of the response. This may differ from the requested page number if
     * the requested page number is greater than the number of pages available.
     */
    private Integer page;

    public static Integer getPageNumberWithinBounds(GridRequest gridRequest, Long totalCount) {
        return getPageNumberWithinBounds(gridRequest.getPage(), gridRequest.getSize(), totalCount);
    }

    /**
     * Returns the actual page number when the requested page number is greater than the number of
     * pages available.
     *
     * @param requestedPageNumber - the page number as found in the GridRequest
     * @param requestedPageSize - the page size as found in the GridRequest
     * @param totalCount - the total number of rows available for the query
     * @return the actual page number or the requested page number if any argument is null
     */
    public static Integer getPageNumberWithinBounds(
            Integer requestedPageNumber, Integer requestedPageSize, Long totalCount) {
        if (totalCount == null || requestedPageNumber == null || requestedPageSize == null) {
            return requestedPageNumber;
        }

        if (totalCount == 0) {
            return 0;
        }

        int pageMax = (int) Math.floor((totalCount - 1.0) / requestedPageSize);
        return Math.min(pageMax, requestedPageNumber);
    }

    public void sanitizeField(String fieldId) {
        dataRows.forEach(
                dataRow -> {
                    Object fieldValue = dataRow.getData().get(fieldId);
                    if (fieldValue != null) {
                        String sanitized = HTMLCleanupUtils.htmlToTrimmedText(fieldValue.toString());
                        dataRow.getData().put(fieldId, sanitized);
                    }
                });
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<DataRow> getDataRows() {
        return dataRows;
    }

    public void setDataRows(List<DataRow> dataRows) {
        this.dataRows = dataRows;
    }

    public void addDataRow(DataRow dataRow) {
        dataRows.add(dataRow);
    }

    public String getIdAttribute() {
        return idAttribute;
    }

    public void setIdAttribute(String idAttribute) {
        this.idAttribute = idAttribute;
    }

    public List<String> getActiveColumnIds() {
        return activeColumnIds;
    }

    public void setActiveColumnIds(List<String> activeColumnIds) {
        this.activeColumnIds = activeColumnIds;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
