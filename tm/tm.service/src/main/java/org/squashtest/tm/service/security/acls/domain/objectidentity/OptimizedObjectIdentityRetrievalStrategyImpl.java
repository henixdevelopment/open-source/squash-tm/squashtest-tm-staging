/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.security.acls.domain.objectidentity;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.security.acls.domain.objectidentity.retrievers.OptimizedObjectIdentityRetriever;

@Component
public class OptimizedObjectIdentityRetrievalStrategyImpl
        implements OptimizedObjectIdentityRetrievalStrategy {

    private final Map<String, OptimizedObjectIdentityRetriever> retrieverByClassName;

    public OptimizedObjectIdentityRetrievalStrategyImpl(
            List<OptimizedObjectIdentityRetriever> retrievers) {
        this.retrieverByClassName =
                Collections.unmodifiableMap(
                        retrievers.stream()
                                .collect(
                                        Collectors.toMap(
                                                OptimizedObjectIdentityRetriever::handledClass, Function.identity())));
    }

    @Override
    public boolean isHandled(String className) {
        requireNonNull(className);
        return this.retrieverByClassName.containsKey(className);
    }

    @Override
    public ObjectIdentity createObjectIdentity(Serializable id, String type) {
        requireNonNull(id);
        requireNonNull(type);
        OptimizedObjectIdentityRetriever optimizedObjectIdentityRetriever =
                this.retrieverByClassName.get(type);
        return optimizedObjectIdentityRetriever.createObjectIdentity(id);
    }
}
