/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders.common;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.persistence.TypedQuery;

public class FetchPlanBuilder<KEY, TYPE> {
    private HintOptions hintOptions = new HintOptions();
    private Set<TypedQuery<TYPE>> joinOptionQueries = new HashSet<>();
    private Set<KEY> ids;
    private TypedQuery<TYPE> mainQuery;

    public FetchPlanBuilder(Collection<KEY> ids) {
        this.ids = new HashSet<>(Objects.requireNonNull(ids));
    }

    public FetchPlanBuilder<KEY, TYPE> addJoinQuery(Supplier<TypedQuery<TYPE>> querySupplier) {
        this.joinOptionQueries.add(querySupplier.get());
        return this;
    }

    public FetchPlanBuilder<KEY, TYPE> addHint(HintOption hintOption) {
        this.hintOptions.add(hintOption);
        return this;
    }

    public FetchPlanBuilder<KEY, TYPE> addHints(HintOptions hintOptions) {
        this.hintOptions.addAll(hintOptions);
        return this;
    }

    public FetchPlanBuilder<KEY, TYPE> withMainQuery(Supplier<TypedQuery<TYPE>> mainQuerySupplier) {
        this.mainQuery = mainQuerySupplier.get();
        return this;
    }

    public FetchPlan<KEY, TYPE> build() {
        Set<JoinOption<KEY, TYPE>> joinOptions =
                joinOptionQueries.stream()
                        .map(typeTypedQuery -> new JoinOption<KEY, TYPE>(hintOptions, typeTypedQuery))
                        .collect(Collectors.toSet());

        return new FetchPlan<>(hintOptions, joinOptions, mainQuery, ids);
    }
}
