/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import static org.squashtest.tm.service.security.Authorizations.LINK_REQVERSION_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.collection.DefaultPagingAndSorting;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.core.foundation.collection.PagingBackedPagedCollectionHolder;
import org.squashtest.tm.domain.IdentifiedUtil;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.exception.requirement.AbstractVerifiedRequirementException;
import org.squashtest.tm.exception.requirement.RequirementAlreadyVerifiedException;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionCoverageDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testcase.TestCaseImportanceManagerService;
import org.squashtest.tm.service.testcase.VerifyingTestCaseManagerService;

@Service("squashtest.tm.service.VerifyingTestCaseManagerService")
@Transactional
public class VerifyingTestCaseManagerServiceImpl implements VerifyingTestCaseManagerService {

    @Inject private TestCaseDao testCaseDao;
    @Inject private RequirementVersionDao requirementVersionDao;
    @Inject private TestCaseImportanceManagerService testCaseImportanceManagerService;
    @Inject private RequirementVersionCoverageDao requirementVersionCoverageDao;
    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject
    @Qualifier("squashtest.tm.repository.TestCaseLibraryNodeDao")
    private LibraryNodeDao<TestCaseLibraryNode> testCaseLibraryNodeDao;

    @Override
    @PreAuthorize(LINK_REQVERSION_OR_ROLE_ADMIN)
    public Map<String, Collection<?>> addVerifyingTestCasesToRequirementVersion(
            List<Long> testCasesIds, long requirementVersionId) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                testCasesIds,
                Permissions.READ.name(),
                TestCase.class.getName());

        // nodes are returned unsorted
        List<TestCaseLibraryNode> nodes = testCaseLibraryNodeDao.findAllByIds(testCasesIds);

        List<TestCase> testCases = new TestCaseNodeWalker().walk(nodes);
        List<Long> ids = IdentifiedUtil.extractIds(testCases);
        Collection<AbstractVerifiedRequirementException> rejections = Collections.emptyList();
        if (!testCases.isEmpty()) {
            rejections = doAddVerifyingTestCasesToRequirementVersion(testCases, requirementVersionId);
        }
        Map<String, Collection<?>> result = new HashMap<>(2);
        result.put(IDS_KEY, ids);
        result.put(REJECTION_KEY, rejections);
        return result;
    }

    private Collection<AbstractVerifiedRequirementException>
            doAddVerifyingTestCasesToRequirementVersion(
                    List<TestCase> testCases, long requirementVersionId) {
        RequirementVersion requirementVersion =
                requirementVersionDao.getReferenceById(requirementVersionId);
        return doAddVerifyingTestCasesToRequirementVersion(testCases, requirementVersion);
    }

    private Collection<AbstractVerifiedRequirementException>
            doAddVerifyingTestCasesToRequirementVersion(
                    List<TestCase> testCases, RequirementVersion requirementVersion) {

        List<AbstractVerifiedRequirementException> rejections = new ArrayList<>(testCases.size());

        Iterator<TestCase> iterator = testCases.iterator();
        while (iterator.hasNext()) {
            TestCase testCase = iterator.next();
            try {
                RequirementVersionCoverage coverage =
                        new RequirementVersionCoverage(requirementVersion, testCase);
                requirementVersionCoverageDao.persist(coverage);
            } catch (RequirementAlreadyVerifiedException ex) {
                rejections.add(ex);
                iterator.remove();
            }
        }
        testCaseImportanceManagerService.changeImportanceIfRelationsAddedToReq(
                testCases, requirementVersion);

        return rejections;
    }

    @Override
    @PreAuthorize(LINK_REQVERSION_OR_ROLE_ADMIN)
    public void removeVerifyingTestCasesFromRequirementVersion(
            List<Long> testCasesIds, long requirementVersionId) {

        List<TestCase> testCases = testCaseDao.findAllByIds(testCasesIds);

        if (!testCases.isEmpty()) {
            requirementVersionCoverageDao.checkIfReqVersionIsLinkable(requirementVersionId);

            List<Long> coveragesToDelete =
                    requirementVersionCoverageDao.byRequirementVersionAndTestCases(
                            testCasesIds, requirementVersionId);

            requirementVersionCoverageDao.delete(coveragesToDelete);

            testCaseImportanceManagerService.changeImportanceIfRelationsRemovedFromReq(
                    testCasesIds, requirementVersionId);
        }
    }

    @Override
    @PreAuthorize(LINK_REQVERSION_OR_ROLE_ADMIN)
    public void removeVerifyingTestCaseFromRequirementVersion(
            long testCaseId, long requirementVersionId) {
        RequirementVersionCoverage coverage =
                requirementVersionCoverageDao.byRequirementVersionAndTestCase(
                        requirementVersionId, testCaseId);
        coverage.checkCanRemoveTestCaseFromRequirementVersion();
        requirementVersionCoverageDao.delete(List.of(coverage.getId()));
        testCaseImportanceManagerService.changeImportanceIfRelationsRemovedFromReq(
                Arrays.asList(testCaseId), requirementVersionId);
    }

    @Override
    @Transactional(readOnly = true)
    public PagedCollectionHolder<List<TestCase>> findAllByRequirementVersion(
            long requirementVersionId, PagingAndSorting pagingAndSorting) {
        List<TestCase> verifiers =
                testCaseDao.findAllByVerifiedRequirementVersion(requirementVersionId, pagingAndSorting);

        long verifiersCount = testCaseDao.countByVerifiedRequirementVersion(requirementVersionId);

        return new PagingBackedPagedCollectionHolder<>(pagingAndSorting, verifiersCount, verifiers);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TestCase> findAllByRequirementVersion(long requirementVersionId) {

        DefaultPagingAndSorting pas = new DefaultPagingAndSorting("Project.name", true);
        return findAllByRequirementVersion(requirementVersionId, pas).getPagedItems();
    }
}
