/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.List;
import org.squashtest.tm.api.wizard.SupervisionScreenData;

public class SynchronisationPluginViewDto {
    private String id;
    private String name;
    private boolean hasSupervisionScreen;
    private SupervisionScreenData supervisionScreenData;
    private List<RemoteSynchronisationDto> remoteSynchronisations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RemoteSynchronisationDto> getRemoteSynchronisations() {
        return remoteSynchronisations;
    }

    public void setRemoteSynchronisations(List<RemoteSynchronisationDto> remoteSynchronisations) {
        this.remoteSynchronisations = remoteSynchronisations;
    }

    public boolean isHasSupervisionScreen() {
        return hasSupervisionScreen;
    }

    public void setHasSupervisionScreen(boolean hasSupervisionScreen) {
        this.hasSupervisionScreen = hasSupervisionScreen;
    }

    public SupervisionScreenData getSupervisionScreenData() {
        return supervisionScreenData;
    }

    public void setSupervisionScreenData(SupervisionScreenData supervisionScreenData) {
        this.supervisionScreenData = supervisionScreenData;
    }
}
