/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.service.deletion.BoundToLockedMilestonesReport;
import org.squashtest.tm.service.deletion.BoundToMultipleMilestonesReport;
import org.squashtest.tm.service.deletion.MilestoneModeNoFolderDeletion;
import org.squashtest.tm.service.deletion.NotBoundToActiveMilestonesReport;
import org.squashtest.tm.service.deletion.SingleOrMultipleMilestonesReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

public abstract class AbstractLockedNodeDetectionService {
    private final ActiveMilestoneHolder activeMilestoneHolder;

    protected AbstractLockedNodeDetectionService(ActiveMilestoneHolder activeMilestoneHolder) {
        this.activeMilestoneHolder = activeMilestoneHolder;
    }

    protected List<SuppressionPreviewReport> detectLockedByMilestone(
            List<Long> nodeIds, String reportType) {
        // check nodes locked by milestones
        List<Long> lockedByMilestones = findNodesWhichMilestonesForbidsDeletion(nodeIds);
        if (lockedByMilestones.isEmpty()) {
            return Collections.emptyList();
        }
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        SuppressionPreviewReport suppressionPreviewReport =
                new BoundToLockedMilestonesReport(reportType);
        suppressionPreviewReport.addAllLockedNodes(lockedByMilestones);
        reportList.add(suppressionPreviewReport);
        return reportList;
    }

    protected List<SuppressionPreviewReport> detectLockedWithActiveMilestone(
            List<Long> nodeIds, String reportType) {
        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        if (activeMilestone.isEmpty()) {
            return Collections.emptyList();
        }
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        List<Long>[] separateIds = getIdsSeparateFolderFromNodeIds(nodeIds);
        addNoFoldersAllowedToReportList(separateIds[0], reportList, reportType);
        List<Long> boundNodes = findNodeIdsHavingMultipleMilestones(separateIds[1]);
        addMultipleMilestoneBindingToReportList(separateIds[1], boundNodes, reportList, reportType);
        addAdditionalReportWithActiveMilestoneToReportList(
                separateIds[1], activeMilestone.get().getId(), reportList, reportType);
        return reportList;
    }

    private void addNoFoldersAllowedToReportList(
            List<Long> folderIds, List<SuppressionPreviewReport> reportList, String reportType) {
        // no folder shall be deleted
        if (folderIds.isEmpty()) {
            return;
        }
        SuppressionPreviewReport suppressionPreviewReport =
                new MilestoneModeNoFolderDeletion(reportType);
        suppressionPreviewReport.addAllLockedNodes(folderIds);
        reportList.add(suppressionPreviewReport);
    }

    protected void addNonBoundNodesWithActiveMilestoneToReportList(
            List<Long> nonBoundNodes, List<SuppressionPreviewReport> reportList, String reportType) {
        if (!nonBoundNodes.isEmpty()) {
            SuppressionPreviewReport suppressionPreviewReport =
                    new NotBoundToActiveMilestonesReport(reportType);
            suppressionPreviewReport.addAllLockedNodes(nonBoundNodes);
            reportList.add(suppressionPreviewReport);
        }
    }

    protected void addMultipleMilestoneBindingToReportList(
            List<Long> nodeIds,
            List<Long> boundNodes,
            List<SuppressionPreviewReport> reportList,
            String reportType) {
        if (!boundNodes.isEmpty()) {
            SuppressionPreviewReport suppressionPreviewReport =
                    getReportMultipleMilestoneBinding(nodeIds, boundNodes, reportType);
            // deletion : no node bound to more than one milestone shall be deleted
            suppressionPreviewReport.addAllLockedNodes(boundNodes);
            reportList.add(suppressionPreviewReport);
        }
    }

    protected SuppressionPreviewReport getReportMultipleMilestoneBinding(
            List<Long> nodeIds, List<Long> boundNodes, String reportType) {
        // report case 1 : all nodes are bound to multiple milestones
        if (nodeIds.size() == boundNodes.size()) {
            return new BoundToMultipleMilestonesReport(reportType);
        }
        // report case 2 : there is a mixed cases of node that will be removed
        // from the milestone and some will be removed - period
        else {
            return new SingleOrMultipleMilestonesReport(reportType);
        }
    }

    protected abstract List<Long> findNodesWhichMilestonesForbidsDeletion(List<Long> nodeIds);

    protected abstract void addAdditionalReportWithActiveMilestoneToReportList(
            List<Long> nodeIds,
            Long activeMilestoneId,
            List<SuppressionPreviewReport> reportList,
            String reportType);

    protected abstract List<Long>[] getIdsSeparateFolderFromNodeIds(List<Long> nodeIds);

    protected abstract List<Long> findNodeIdsHavingMultipleMilestones(List<Long> nodeIds);
}
