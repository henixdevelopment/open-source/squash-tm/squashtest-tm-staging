/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_TAG;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_RENDERING_LOCATION;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION_EVENT;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType;
import org.squashtest.tm.service.internal.api.repository.HibernateSessionClearing;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

public class JdbcExecutionDeletionHandler extends AbstractJdbcDeletionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcExecutionDeletionHandler.class);

    private final Set<Long> executionIds;

    private final Condition inExecutionIds;

    public JdbcExecutionDeletionHandler(
            Collection<Long> executionIds,
            DSLContext dslContext,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper,
            String operationId,
            EntityManager entityManager) {
        super(dslContext, attachmentRepository, reorderHelper, operationId, entityManager);
        this.executionIds = new HashSet<>(executionIds);
        this.inExecutionIds = EXECUTION.EXECUTION_ID.in(executionIds);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @HibernateSessionClearing
    public void deleteExecutions() {
        logStartProcess();
        clearPersistenceContext();
        storeEntitiesToDeleteIntoWorkingTable();
        performDeletions();
        cleanWorkingTable();
        logEndProcess();
    }

    private void logEndProcess() {
        LOGGER.info(
                String.format(
                        "Deleted executions %s. Time elapsed %s",
                        inExecutionIds, startDate.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
    }

    private void performDeletions() {
        deleteDenormalizedCustomFieldValues();
        deleteDenormalizedEnvironmentVariables();
        deleteDenormalizedEnvironmentTags();
        deleteExecutionSteps();
        deleteKeywordExecution();
        deleteExecutionEntities();
        deleteIssueLists();
        deleteCustomFieldValues();
        deleteAttachmentLists();
        deleteAttachmentContents();
    }

    private void deleteDenormalizedCustomFieldValues() {
        workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_VALUE_OPTION.DFV_ID);
        workingTables.delete(
                DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_RENDERING_LOCATION.DFV_ID);
        workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_VALUE.DFV_ID);
        logDelete(DENORMALIZED_FIELD_VALUE);
    }

    private void deleteDenormalizedEnvironmentVariables() {
        workingTables.delete(
                DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID, DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID);
        logDelete(DENORMALIZED_ENVIRONMENT_VARIABLE);
    }

    private void deleteDenormalizedEnvironmentTags() {
        workingTables.delete(DENORMALIZED_ENVIRONMENT_TAG.DET_ID, DENORMALIZED_ENVIRONMENT_TAG.DET_ID);
        logDelete(DENORMALIZED_ENVIRONMENT_TAG);
    }

    private void deleteExecutionSteps() {
        workingTables.delete(EXECUTION.EXECUTION_ID, EXECUTION_EXECUTION_STEPS.EXECUTION_ID);
        workingTables.delete(EXECUTION_STEP.EXECUTION_STEP_ID, EXECUTION_STEP.EXECUTION_STEP_ID);
        logDelete(EXECUTION_STEP);
    }

    private void deleteKeywordExecution() {
        workingTables.delete(EXECUTION.EXECUTION_ID, KEYWORD_EXECUTION.EXECUTION_ID);
        logDelete(KEYWORD_EXECUTION);
    }

    private void deleteExecutionEntities() {
        workingTables.delete(
                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID,
                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, SCRIPTED_EXECUTION.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, SESSION_NOTE.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, EXPLORATORY_EXECUTION_EVENT.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, EXPLORATORY_EXECUTION.EXECUTION_ID);
        workingTables.delete(EXECUTION.EXECUTION_ID, EXECUTION.EXECUTION_ID);
        logDelete(EXECUTION);
    }

    private void deleteIssueLists() {
        workingTables.delete(ISSUE_LIST.ISSUE_LIST_ID, ISSUE.ISSUE_LIST_ID);
        workingTables.delete(ISSUE_LIST.ISSUE_LIST_ID, ISSUE_LIST.ISSUE_LIST_ID);
        logDelete(ISSUE_LIST);
    }

    private void storeEntitiesToDeleteIntoWorkingTable() {
        addDenormalizedFieldValue();
        addDenormalizedEnvironmentVariable();
        addDenormalizedEnvironmentTag();
        addIssueList();
        addExecution();
        addExecutionSteps();
        addCustomFieldValues();
        addAttachmentList();
        addAutomatedExecutionFailureDetails();
        logReferenceEntitiesComplete();
    }

    private void addAttachmentList() {
        workingTables.addEntity(
                ATTACHMENT_LIST.ATTACHMENT_LIST_ID,
                () ->
                        makeSelectAttachmentList(EXECUTION.EXECUTION_ID, EXECUTION.ATTACHMENT_LIST_ID)
                                .union(
                                        makeSelectAttachmentList(
                                                EXECUTION_STEP.EXECUTION_STEP_ID, EXECUTION_STEP.ATTACHMENT_LIST_ID)));
    }

    private void addCustomFieldValues() {
        workingTables.addEntity(
                CUSTOM_FIELD_VALUE.CFV_ID,
                () ->
                        makeSelectCustomFieldValues(
                                        EXECUTION_STEP.EXECUTION_STEP_ID, BindableEntity.EXECUTION_STEP)
                                .union(
                                        makeSelectCustomFieldValues(EXECUTION.EXECUTION_ID, BindableEntity.EXECUTION)));
    }

    private void addExecutionSteps() {
        workingTables.addEntity(
                EXECUTION_STEP.EXECUTION_STEP_ID,
                () ->
                        makeSelectClause(EXECUTION_STEP.EXECUTION_STEP_ID)
                                .from(EXECUTION)
                                .innerJoin(EXECUTION_EXECUTION_STEPS)
                                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                .innerJoin(EXECUTION_STEP)
                                .on(
                                        EXECUTION_STEP.EXECUTION_STEP_ID.eq(
                                                EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                                .where(inExecutionIds));
    }

    private void addExecution() {
        workingTables.addEntity(
                EXECUTION.EXECUTION_ID,
                () -> makeSelectClause(EXECUTION.EXECUTION_ID).from(EXECUTION).where(inExecutionIds));
    }

    private void addIssueList() {
        workingTables.addEntity(
                ISSUE_LIST.ISSUE_LIST_ID,
                () ->
                        makeSelectClause(ISSUE_LIST.ISSUE_LIST_ID)
                                .from(EXECUTION)
                                .innerJoin(ISSUE_LIST)
                                .on(EXECUTION.ISSUE_LIST_ID.eq(ISSUE_LIST.ISSUE_LIST_ID))
                                .where(inExecutionIds)
                                .union(
                                        makeSelectClause(ISSUE_LIST.ISSUE_LIST_ID)
                                                .from(EXECUTION)
                                                .innerJoin(EXECUTION_EXECUTION_STEPS)
                                                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                                .innerJoin(EXECUTION_STEP)
                                                .on(
                                                        EXECUTION_STEP.EXECUTION_STEP_ID.eq(
                                                                EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                                                .innerJoin(ISSUE_LIST)
                                                .on(EXECUTION_STEP.ISSUE_LIST_ID.eq(ISSUE_LIST.ISSUE_LIST_ID))
                                                .where(inExecutionIds)));
    }

    private void addAutomatedExecutionFailureDetails() {
        workingTables.addEntity(
                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID,
                () ->
                        makeSelectClause(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID)
                                .from(EXECUTION)
                                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                .innerJoin(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                                .on(
                                        AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID.eq(
                                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID))
                                .where(inExecutionIds));
    }

    private void addDenormalizedEnvironmentTag() {
        workingTables.addEntity(
                DENORMALIZED_ENVIRONMENT_TAG.DET_ID,
                () ->
                        makeSelectClause(DENORMALIZED_ENVIRONMENT_TAG.DET_ID)
                                .from(EXECUTION)
                                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                .innerJoin(DENORMALIZED_ENVIRONMENT_TAG)
                                .on(
                                        DENORMALIZED_ENVIRONMENT_TAG.HOLDER_ID.eq(
                                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID))
                                .where(inExecutionIds));
    }

    private void addDenormalizedEnvironmentVariable() {
        workingTables.addEntity(
                DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID,
                () ->
                        makeSelectClause(DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID)
                                .from(EXECUTION)
                                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                .innerJoin(DENORMALIZED_ENVIRONMENT_VARIABLE)
                                .on(
                                        DENORMALIZED_ENVIRONMENT_VARIABLE.HOLDER_ID.eq(
                                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID))
                                .where(inExecutionIds));
    }

    private void addDenormalizedFieldValue() {
        workingTables.addEntity(
                DENORMALIZED_FIELD_VALUE.DFV_ID,
                () ->
                        makeSelectClause(DENORMALIZED_FIELD_VALUE.DFV_ID)
                                .from(EXECUTION)
                                .innerJoin(DENORMALIZED_FIELD_VALUE)
                                .on(
                                        DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID.eq(
                                                EXECUTION.EXECUTION_ID))
                                .where(
                                        inExecutionIds.and(
                                                DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(
                                                        DenormalizedFieldHolderType.EXECUTION.name())))
                                .union(
                                        makeSelectClause(DENORMALIZED_FIELD_VALUE.DFV_ID)
                                                .from(EXECUTION)
                                                .innerJoin(EXECUTION_EXECUTION_STEPS)
                                                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                                .innerJoin(EXECUTION_STEP)
                                                .on(
                                                        EXECUTION_STEP.EXECUTION_STEP_ID.eq(
                                                                EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                                                .innerJoin(DENORMALIZED_FIELD_VALUE)
                                                .on(
                                                        DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID.eq(
                                                                EXECUTION_STEP.EXECUTION_STEP_ID))
                                                .where(
                                                        inExecutionIds.and(
                                                                DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(
                                                                        DenormalizedFieldHolderType.EXECUTION_STEP.name())))));
    }

    private void logStartProcess() {
        LOGGER.info(
                String.format(
                        "Deleted executions %s. Time elapsed %s",
                        executionIds, startDate.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
    }
}
