/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution;

import java.util.Collections;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.exception.UnknownEntityException;
import org.squashtest.tm.service.execution.SessionNoteService;
import org.squashtest.tm.service.internal.repository.SessionNoteDao;
import org.squashtest.tm.service.internal.repository.display.SessionNoteDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

@Service
@Transactional(readOnly = true)
public class SessionNoteServiceImpl implements SessionNoteService {

    private final SessionNoteDisplayDao sessionNoteDisplayDao;

    private final SessionNoteDao sessionNoteDao;

    private final PermissionEvaluationService permissionService;

    @PersistenceContext private EntityManager em;

    public SessionNoteServiceImpl(
            SessionNoteDisplayDao sessionNoteDisplayDao,
            SessionNoteDao sessionNoteDao,
            PermissionEvaluationService permissionService) {
        this.sessionNoteDisplayDao = sessionNoteDisplayDao;
        this.sessionNoteDao = sessionNoteDao;
        this.permissionService = permissionService;
    }

    @Override
    public Project findProjectBySessionNoteId(long noteId) {
        checkReadExecutionPermission(noteId);

        Long projectId = sessionNoteDisplayDao.findProjectIdBySessionNoteId(noteId);
        if (projectId == null) {
            throw new UnknownEntityException(noteId, SessionNote.class);
        }

        return em.find(Project.class, projectId);
    }

    @Override
    public SessionNote findSessionNoteById(long noteId) {
        checkReadExecutionPermission(noteId);
        return em.find(SessionNote.class, noteId);
    }

    private void checkReadExecutionPermission(long noteId) {
        Long executionId = sessionNoteDao.findExploratoryExecutionId(noteId);
        PermissionsUtils.checkPermission(
                permissionService,
                Collections.singletonList(executionId),
                Permissions.READ.name(),
                Execution.class.getName());
    }
}
