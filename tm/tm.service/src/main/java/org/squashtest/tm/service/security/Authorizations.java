/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.security;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;
import static org.squashtest.tm.api.security.acls.Roles.ROLE_TA_API_CLIENT;

/**
 * Defines constants for authorization rules.
 *
 * @author Gregory Fouquet
 */
public final class Authorizations {

    /* -- ADMIN -- */

    public static final String HAS_ROLE_ADMIN = "hasRole('" + ROLE_ADMIN + "')";
    public static final String OR_HAS_ROLE_ADMIN = " or " + HAS_ROLE_ADMIN;

    public static final String HAS_ROLE_TA_API_CLIENT = "hasRole('" + ROLE_TA_API_CLIENT + "')";
    public static final String OR_HAS_ROLE_TA_API_CLIENT = " or " + HAS_ROLE_TA_API_CLIENT;

    /* -- MILESTONES -- */

    public static final String MILESTONE_FEAT_ENABLED = "@featureManager.isEnabled('MILESTONE')";

    /* -- PROJECTS -- */

    public static final String MANAGE_PROJECT_OR_ROLE_ADMIN =
            "hasPermission(#projectId, 'org.squashtest.tm.domain.project.Project', 'MANAGE_PROJECT') "
                    + OR_HAS_ROLE_ADMIN;
    public static final String MANAGE_MILESTONE_OR_ROLE_ADMIN =
            "hasPermission(#projectId, 'org.squashtest.tm.domain.project.Project', 'MANAGE_MILESTONE') "
                    + OR_HAS_ROLE_ADMIN;
    public static final String MANAGE_PROJECT_CLEARANCE_OR_ROLE_ADMIN =
            "hasPermission(#projectId, 'org.squashtest.tm.domain.project.Project', 'MANAGE_PROJECT_CLEARANCE') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String IMPORT_PROJECT_OR_ROLE_ADMIN =
            "hasPermission(#projectId, 'org.squashtest.tm.domain.project.Project', 'IMPORT') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- REQUIREMENT LIBRARY -- */

    public static final String READ_REQUIREMENT_LIBRARY_OR_HAS_ROLE_ADMIN =
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.requirement.RequirementLibrary', 'READ') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String CREATE_REQLIBRARY =
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.requirement.RequirementLibrary' , 'CREATE') ";
    public static final String CREATE_REQLIBRARY_OR_ROLE_ADMIN =
            CREATE_REQLIBRARY + OR_HAS_ROLE_ADMIN;

    /* -- REQUIREMENT LIBRARY NODE -- */

    public static final String READ_REQUIREMENT_LIBRARY_NODE_OR_ROLE_ADMIN =
            "hasPermission(#rlnId, 'org.squashtest.tm.domain.requirement.RequirementLibraryNode', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    /* -- REQUIREMENT FOLDERS -- */

    public static final String CREATE_REQFOLDER_OR_ROLE_ADMIN =
            "hasPermission(#folderId, 'org.squashtest.tm.domain.requirement.RequirementFolder' , 'CREATE') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- HIGH LEVEL REQUIREMENTS -- */
    public static final String LINK_HIGH_LEVEL_REQUIREMENT =
            "hasPermission(#highLevelRequirementId, 'org.squashtest.tm.domain.requirement.HighLevelRequirement', 'LINK')";
    public static final String LINK_HIGH_LEVEL_REQUIREMENT_OR_ROLE_ADMIN =
            LINK_HIGH_LEVEL_REQUIREMENT + OR_HAS_ROLE_ADMIN;

    /* -- REQUIREMENTS -- */

    public static final String READ_REQUIREMENT_OR_ROLE_ADMIN =
            "hasPermission(#requirementId, 'org.squashtest.tm.domain.requirement.Requirement', 'READ')"
                    + OR_HAS_ROLE_ADMIN;
    public static final String READ_REQUIREMENT_FOLDER_OR_ROLE_ADMIN =
            "hasPermission(#requirementFolderId, 'org.squashtest.tm.domain.requirement.RequirementFolder', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    public static final String CREATE_REQUIREMENT_OR_ROLE_ADMIN =
            "hasPermission(#requirementId, 'org.squashtest.tm.domain.requirement.Requirement', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN;

    public static final String LINK_REQUIREMENT =
            "hasPermission(#requirementId, 'org.squashtest.tm.domain.requirement.Requirement', 'LINK')";
    public static final String LINK_REQUIREMENT_OR_ROLE_ADMIN = LINK_REQUIREMENT + OR_HAS_ROLE_ADMIN;
    public static final String LINK_REQUIREMENT_TO_SPRINT =
            "hasPermission(#sprintId, 'org.squashtest.tm.domain.campaign.Sprint', 'LINK')";
    public static final String DELETE_SPRINT =
            "hasPermission(#sprintId, 'org.squashtest.tm.domain.campaign.Sprint', 'DELETE')";

    /* -- REQUIREMENT VERSIONS -- */

    public static final String READ_REQVERSION =
            "hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion' , 'READ')";
    public static final String READ_REQVERSION_OR_ROLE_ADMIN = READ_REQVERSION + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_REQVERSION =
            "hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'WRITE')";
    public static final String WRITE_REQVERSION_OR_ROLE_ADMIN = WRITE_REQVERSION + OR_HAS_ROLE_ADMIN;

    public static final String LINK_REQVERSION =
            "hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'LINK')";
    public static final String LINK_REQVERSION_OR_ROLE_ADMIN = LINK_REQVERSION + OR_HAS_ROLE_ADMIN;

    /* -- TEST CASE LIBRARY NODES -- */

    public static final String READ_TEST_CASE_LIBRARY_NODE_OR_ROLE_ADMIN =
            "hasPermission(#tclnId, 'org.squashtest.tm.domain.testcase.TestCaseLibraryNode', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    /* -- TEST CASES -- */

    public static final String READ_TC =
            "hasPermission(#testCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'READ')";
    public static final String READ_TCF =
            "hasPermission(#testCaseFolderId, 'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'READ')";
    private static final String READ_TEST_CASE_LIBRARY =
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.testcase.TestCaseLibrary' , 'READ')";
    public static final String READ_TC_OR_ROLE_ADMIN = READ_TC + OR_HAS_ROLE_ADMIN;
    public static final String READ_TCF_OR_ROLE_ADMIN = READ_TCF + OR_HAS_ROLE_ADMIN;
    public static final String READ_TEST_CASE_LIBRARY_OR_ROLE_ADMIN =
            READ_TEST_CASE_LIBRARY + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_TC =
            "hasPermission(#testCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE')";
    public static final String WRITE_TC_OR_ROLE_ADMIN = WRITE_TC + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_PARENT_TC_OR_ROLE_ADMIN =
            "hasPermission(#parentTestCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE')"
                    + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_AS_AUTOMATION_TC =
            "hasPermission(#testCaseId, 'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE_AS_AUTOMATION')";
    public static final String WRITE_OR_WRITE_AS_AUTOMATION_TC_OR_ROLE_ADMIN =
            WRITE_TC_OR_ROLE_ADMIN + " or " + WRITE_AS_AUTOMATION_TC;

    /* TEST STEPS */

    public static final String LINK_TESTSTEP =
            "hasPermission(#testStepId, 'org.squashtest.tm.domain.testcase.TestStep' , 'LINK')";
    public static final String LINK_TESTSTEP_OR_ROLE_ADMIN = LINK_TESTSTEP + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_TESTSTEP =
            "hasPermission(#testStepId, 'org.squashtest.tm.domain.testcase.TestStep', 'WRITE')";
    public static final String WRITE_TESTSTEP_OR_ROLE_ADMIN = WRITE_TESTSTEP + OR_HAS_ROLE_ADMIN;

    /* -- CAMPAIGN LIBRARY NODES -- */

    public static final String READ_CAMPAIGN_LIBRARY_NODE_OR_ROLE_ADMIN =
            "hasPermission(#clnId, 'org.squashtest.tm.domain.campaign.CampaignLibraryNode', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    /* -- CAMPAIGN FOLDERS -- */
    public static final String READ_CAMPFOLDER_OR_ROLE_ADMIN =
            "hasPermission(#campFolderId, 'org.squashtest.tm.domain.campaign.CampaignFolder', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    /* -- ITERATIONS -- */

    private static final String READ_ITERATION =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration' , 'READ')";
    public static final String READ_ITERATION_OR_ROLE_ADMIN = READ_ITERATION + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_ITERATION_OR_ROLE_ADMIN =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'WRITE') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String EXECUTE_ITERATION_OR_ROLE_ADMIN =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'EXECUTE')"
                    + OR_HAS_ROLE_ADMIN;

    public static final String CREATE_ITERATION_OR_ROLE_ADMIN =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'CREATE') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String DELETE_ITERATION_OR_ROLE_ADMIN =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'DELETE') "
                    + OR_HAS_ROLE_ADMIN;
    public static final String LINK_ITERATION_OR_ROLE_ADMIN =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'LINK')"
                    + OR_HAS_ROLE_ADMIN;
    /* -- TEST SUITES -- */
    public static final String READ_TESTSUITE =
            "hasPermission(#testSuiteId, 'org.squashtest.tm.domain.campaign.TestSuite', 'READ')";
    public static final String READ_TS_OR_ROLE_ADMIN = READ_TESTSUITE + OR_HAS_ROLE_ADMIN;

    public static final String EXECUTE_TS_OR_ROLE_ADMIN =
            "hasPermission(#testSuiteId, 'org.squashtest.tm.domain.campaign.TestSuite', 'EXECUTE')"
                    + OR_HAS_ROLE_ADMIN;

    /* -- CAMPAIGNS -- */

    public static final String READ_CAMPAIGN =
            "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign' , 'READ')";
    public static final String READ_CAMPAIGN_OR_ROLE_ADMIN = READ_CAMPAIGN + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_CAMPAIGN =
            "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign' , 'WRITE')";
    public static final String WRITE_CAMPAIGN_OR_ROLE_ADMIN = WRITE_CAMPAIGN + OR_HAS_ROLE_ADMIN;

    public static final String CREATE_CAMPAIGN_OR_ROLE_ADMIN =
            "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'CREATE') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String READ_CAMPAIGN_LIBRARY =
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.campaign.CampaignLibrary' ,'READ') ";
    public static final String READ_CAMPAIGN_LIBRARY_OR_HAS_ROLE_ADMIN =
            READ_CAMPAIGN_LIBRARY + OR_HAS_ROLE_ADMIN;

    /* -- SPRINT GROUPS -- */
    public static final String READ_SPRINT_GROUP_OR_ADMIN =
            "hasPermission(#sprintGroupId, 'org.squashtest.tm.domain.campaign.SprintGroup' ,'READ') "
                    + OR_HAS_ROLE_ADMIN;
    public static final String WRITE_SPRINT_GROUP_OR_ADMIN =
            "hasPermission(#sprintGroupId, 'org.squashtest.tm.domain.campaign.SprintGroup' ,'WRITE') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- SPRINTS -- */
    public static final String READ_SPRINT_OR_ADMIN =
            "hasPermission(#sprintId, 'org.squashtest.tm.domain.campaign.Sprint' ,'READ') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_SPRINT_OR_ADMIN =
            "hasPermission(#sprintId, 'org.squashtest.tm.domain.campaign.Sprint' ,'WRITE')"
                    + OR_HAS_ROLE_ADMIN;

    /* -- SPRINT REQ VERSION -- */
    public static final String READ_SPRINT_REQ_VERSION_OR_ADMIN =
            "hasPermission(#sprintReqVersionId, 'org.squashtest.tm.domain.campaign.SprintReqVersion' ,'READ') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String LINK_SPRINT_REQ_VERSION_OR_ADMIN =
            "hasPermission(#sprintReqVersionId, 'org.squashtest.tm.domain.campaign.SprintReqVersion' ,'LINK') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_SPRINT_REQ_VERSION_OR_ADMIN =
            "hasPermission(#sprintReqVersionId, 'org.squashtest.tm.domain.campaign.SprintReqVersion' ,'WRITE') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- EXECUTIONS -- */

    public static final String READ_EXECUTION_OR_ROLE_ADMIN =
            "hasPermission(#executionId, 'org.squashtest.tm.domain.execution.Execution', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    public static final String EXECUTE_EXECUTION_OR_ROLE_ADMIN =
            "hasPermission(#executionId, 'org.squashtest.tm.domain.execution.Execution', 'EXECUTE') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- EXECUTION STEPS -- */

    public static final String EXECUTE_EXECSTEP_OR_ROLE_ADMIN =
            "hasPermission(#executionStepId, 'org.squashtest.tm.domain.execution.ExecutionStep', 'EXECUTE') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- TEST PLAN ITEM -- */
    public static final String READ_TPI_OR_ROLE_ADMIN =
            "hasPermission(#testPlanItemId, 'org.squashtest.tm.domain.campaign.testplan.TestPlanItem' ,'READ') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String WRITE_TPI_OR_ROLE_ADMIN =
            "hasPermission(#testPlanItemId, 'org.squashtest.tm.domain.campaign.testplan.TestPlanItem' ,'WRITE') "
                    + OR_HAS_ROLE_ADMIN;

    public static final String EXECUTE_TPI_OR_ROLE_ADMIN =
            "hasPermission(#testPlanItemId, 'org.squashtest.tm.domain.campaign.testplan.TestPlanItem' ,'EXECUTE') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- ITERATION TEST PLAN ITEM -- */

    public static final String EXECUTE_ITPI =
            "hasPermission(#testPlanItemId, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem', 'EXECUTE') ";
    public static final String EXECUTE_ITPI_OR_ROLE_ADMIN = EXECUTE_ITPI + OR_HAS_ROLE_ADMIN;

    /* -- CUSTOM REPORT LIBRARY -- */

    public static final String READ_CUSTOM_REPORT_LIBRARY_OR_ROLE_ADMIN =
            "hasPermission(#customReportLibraryId, 'org.squashtest.tm.domain.customreport.CustomReportLibrary' ,'READ') "
                    + OR_HAS_ROLE_ADMIN;

    /* -- CUSTOM REPORT LIBRARY NODE -- */

    public static final String READ_CUSTOM_REPORT_LIBRARY_NODE =
            "hasPermission(#treeNodeId, 'org.squashtest.tm.domain.customreport.CustomReportLibraryNode' ,'READ') ";
    public static final String READ_CUSTOM_REPORT_LIBRARY_NODE_OR_ROLE_ADMIN =
            READ_CUSTOM_REPORT_LIBRARY_NODE + OR_HAS_ROLE_ADMIN;

    /* -- SESSION OVERVIEW -- */

    public static final String READ_SESSION_OVERVIEW_OR_ROLE_ADMIN =
            "hasPermission(#sessionOverviewId, 'org.squashtest.tm.domain.campaign.ExploratorySessionOverview', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    private Authorizations() {
        super();
    }
}
