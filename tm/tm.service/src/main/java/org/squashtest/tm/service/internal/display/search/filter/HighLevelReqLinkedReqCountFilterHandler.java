/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.HIGH_LEVEL_REQUIREMENT_LINKEDREQCOUNT;
import static org.squashtest.tm.domain.requirement.QRequirement.requirement;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import java.util.Set;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.requirement.QHighLevelRequirement;
import org.squashtest.tm.domain.requirement.QRequirement;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

@Component
public class HighLevelReqLinkedReqCountFilterHandler implements FilterHandler {

    private final Set<String> handledPrototypes =
            Sets.newHashSet(HIGH_LEVEL_REQUIREMENT_LINKEDREQCOUNT);

    @Override
    public boolean canHandleFilter(GridFilterValue filter) {
        return this.handledPrototypes.contains(filter.getColumnPrototype());
    }

    @Override
    public void handleFilter(
            ExtendedHibernateQuery<?> query, GridFilterValue filter, GridRequest gridRequest) {
        QRequirement requirement2 = new QRequirement("requirement2");
        QHighLevelRequirement highLevelRequirement = new QHighLevelRequirement("highLevelRequirement");

        HibernateQuery<Integer> subquery =
                new ExtendedHibernateQuery<>()
                        .select(Expressions.ONE)
                        .from(requirement2)
                        .rightJoin(highLevelRequirement)
                        .on(highLevelRequirement.id.eq(requirement2.highLevelRequirement.id))
                        .where(requirement.id.eq(highLevelRequirement.id))
                        .groupBy(requirement2.highLevelRequirement.id);
        appendHavingClause(subquery, requirement2.highLevelRequirement, filter);

        query.where(subquery.exists());
    }

    private void appendHavingClause(
            HibernateQuery<Integer> subquery,
            QHighLevelRequirement highLevelReq,
            GridFilterValue filter) {
        int min = Integer.parseInt(filter.getValues().get(0));
        switch (filter.getOperation()) {
            case "BETWEEN":
                int max = Integer.parseInt(filter.getValues().get(1));
                subquery.having(highLevelReq.id.count().between(min, max));
                break;
            case "GREATER":
                subquery.having(highLevelReq.id.count().gt(min));
                break;
            case "GREATER_EQUAL":
                subquery.having(highLevelReq.id.count().goe(min));
                break;
            case "LOWER":
                subquery.having(highLevelReq.id.count().lt(min));
                break;
            case "LOWER_EQUAL":
                subquery.having(highLevelReq.id.count().loe(min));
                break;
            case "EQUALS":
                subquery.having(highLevelReq.id.count().eq((long) min));
                break;
            case "NOT_EQUALS":
                subquery.having(highLevelReq.id.count().ne((long) min));
                break;
            default:
                throw new IllegalArgumentException("Unknown operation");
        }
    }
}
