/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement;

import java.util.List;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.CoverageImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.ExistLinkImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.LinkImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementFolderImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementVersionImportDto;

public interface RequirementImportService {

    void addRequirementsToLibrary(Long libraryId, Project project, List<RequirementImportDto> batch);

    void addRequirementsToFolders(
            List<Long> folderIds, Project project, List<Batch<RequirementImportDto>> batches);

    void addRequirementsToRequirements(
            List<Long> requirementIds, Project project, List<Batch<RequirementImportDto>> batches);

    void addFoldersToLibrary(
            Long libraryId, Project project, List<RequirementFolderImportDto> batches);

    void addFoldersToFolders(
            List<Long> folderIds, Project project, List<Batch<RequirementFolderImportDto>> batches);

    void addVersions(
            List<Long> requirementIds, Project project, List<Batch<RequirementVersionImportDto>> batches);

    void createCoverages(List<Long> testCaseIds, List<Batch<CoverageImportDto>> batchList);

    void createLinks(List<Batch<LinkImportDto>> batches);

    void updateLinks(List<ExistLinkImportDto> batch);
}
