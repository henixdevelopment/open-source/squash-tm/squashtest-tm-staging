/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.repository.display.SingleHierarchyTreeBrowserDao;

@Repository
public class SingleHierarchyTreeBrowserDaoImpl implements SingleHierarchyTreeBrowserDao {

    public static final String LIBRARY = "LIBRARY";
    private final DSLContext dsl;
    private final Map<NodeType, SingleHierarchyLibraryLookupDefinition>
            singleHierarchyLibrariesLookupDefinitions;
    private final Map<NodeType, SingleHierarchyAncestorLookupDefinition>
            singleHierarchyAncestorLookupDefinitions;
    private final Map<NodeType, SingleHierarchyDescendantLookupDefinition>
            singleHierarchyDescendantLookupDefinitions;

    public SingleHierarchyTreeBrowserDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
        this.singleHierarchyAncestorLookupDefinitions =
                EnumSet.allOf(SingleHierarchyAncestorLookupDefinition.class).stream()
                        .collect(
                                Collectors.toMap(
                                        SingleHierarchyAncestorLookupDefinition::getLibraryNodeType,
                                        Function.identity()));
        this.singleHierarchyLibrariesLookupDefinitions =
                EnumSet.allOf(SingleHierarchyLibraryLookupDefinition.class).stream()
                        .collect(
                                Collectors.toMap(
                                        SingleHierarchyLibraryLookupDefinition::getLibraryNodeType,
                                        Function.identity()));
        this.singleHierarchyDescendantLookupDefinitions =
                EnumSet.allOf(SingleHierarchyDescendantLookupDefinition.class).stream()
                        .collect(
                                Collectors.toMap(
                                        SingleHierarchyDescendantLookupDefinition::getLibraryNodeType,
                                        Function.identity()));
    }

    @UsedInPlugin("rest-api")
    public Set<NodeReference> findLibraryReferences(
            NodeWorkspace workspace, Collection<Long> projectIds) {
        SingleHierarchyLibraryLookupDefinition lookupDefinition =
                singleHierarchyLibrariesLookupDefinitions.get(workspace.getLibraryType());
        return dsl
                .select(lookupDefinition.getNodeIdColumn())
                .from(PROJECT)
                .innerJoin(lookupDefinition.getNodeTable())
                .on(lookupDefinition.getLibraryColumnInProject().eq(lookupDefinition.getEntityIdColumn()))
                .and(lookupDefinition.getEntityTypeColumn().eq(LIBRARY))
                .where(PROJECT.PROJECT_TYPE.eq(Project.PROJECT_TYPE))
                .and(PROJECT.PROJECT_ID.in(projectIds))
                .fetch(lookupDefinition.getNodeIdColumn())
                .stream()
                .map(id -> new NodeReference(workspace.getLibraryType(), id))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    @UsedInPlugin("rest-api")
    public ListMultimap<NodeReference, NodeReference> findChildrenReference(
            Set<NodeReference> parentReferences) {
        Set<Long> parentIds =
                parentReferences.stream().map(NodeReference::getId).collect(Collectors.toSet());
        Map<Long, NodeReference> parentById =
                parentReferences.stream()
                        .collect(Collectors.toMap(NodeReference::getId, Function.identity()));
        ListMultimap<NodeReference, NodeReference> childrenReferences = ArrayListMultimap.create();

        Optional<NodeReference> nodeReference = parentReferences.stream().findFirst();
        if (nodeReference.isPresent()) {
            SingleHierarchyDescendantLookupDefinition lookupDefinition =
                    findLookupDefinition(nodeReference.get());
            dsl.select(
                            lookupDefinition.getNodePrimaryKey(),
                            lookupDefinition.getDiscriminatorField(),
                            lookupDefinition.getAncestorField())
                    .from(lookupDefinition.getNodeTable())
                    .innerJoin(lookupDefinition.getHierarchyTable())
                    .on(lookupDefinition.getDescendantField().eq(lookupDefinition.getNodePrimaryKey()))
                    .where(lookupDefinition.getAncestorField().in(parentIds))
                    .orderBy(lookupDefinition.getOrderField())
                    .forEach(
                            record -> {
                                NodeType childType =
                                        lookupDefinition.discriminateDescendant(
                                                record.get(lookupDefinition.getDiscriminatorField()));
                                Long childId = record.get(lookupDefinition.getNodePrimaryKey());
                                Long parentId = record.get(lookupDefinition.getAncestorField());
                                NodeReference childReference = new NodeReference(childType, childId);
                                childrenReferences.put(parentById.get(parentId), childReference);
                            });
        }
        return childrenReferences;
    }

    private SingleHierarchyDescendantLookupDefinition findLookupDefinition(
            NodeReference nodeReference) {
        NodeType nodeType = nodeReference.getNodeType();
        if (!nodeType.isLibrary()) {
            nodeType = nodeType.getLibraryType();
        }
        return singleHierarchyDescendantLookupDefinitions.get(nodeType);
    }

    @Override
    public Set<NodeReference> findAncestors(NodeReferences nodeReferences) {
        Set<NodeReference> filteredReferences = nodeReferences.extractNonLibraries();
        return filteredReferences.stream()
                .findFirst()
                .map(
                        nodeReference ->
                                getAncestorReferences(
                                        singleHierarchyAncestorLookupDefinitions.get(
                                                nodeReference.getNodeType().getLibraryType()),
                                        filteredReferences))
                .orElseGet(HashSet::new);
    }

    private Set<NodeReference> getAncestorReferences(
            SingleHierarchyAncestorLookupDefinition ancestorLookupDefinition,
            Set<NodeReference> descendants) {
        if (descendants.isEmpty()) {
            return new HashSet<>();
        }
        Set<Long> descendantIds =
                descendants.stream().map(NodeReference::getId).collect(Collectors.toSet());
        List<NodeReference> nodeReferences =
                this.dsl
                        .select(
                                ancestorLookupDefinition.getAncestorField(),
                                ancestorLookupDefinition.getDiscriminatorField())
                        .from(ancestorLookupDefinition.getHierarchyTable())
                        .innerJoin(ancestorLookupDefinition.getNodeTable())
                        .on(
                                ancestorLookupDefinition
                                        .getNodePrimaryKey()
                                        .eq(ancestorLookupDefinition.getAncestorField()))
                        .where(ancestorLookupDefinition.getDescendantField().in(descendantIds))
                        .and(ancestorLookupDefinition.getAncestorField().notIn(descendantIds))
                        .fetch(
                                record -> {
                                    NodeType nodeType =
                                            ancestorLookupDefinition.discriminateAncestor(
                                                    record.get(ancestorLookupDefinition.getDiscriminatorField()));
                                    Long id = record.get(ancestorLookupDefinition.getAncestorField());
                                    return new NodeReference(nodeType, id);
                                });

        return new HashSet<>(nodeReferences);
    }
}
