/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;

import java.util.List;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.synchronisation.SynchronisationKind;
import org.squashtest.tm.service.internal.repository.CustomRemoteSynchronisationDao;

@Repository("squashtest.tm.repository.RemoteSynchronisationDao")
@Primary
public class CustomRemoteSynchronisationDaoImpl implements CustomRemoteSynchronisationDao {

    @Inject private DSLContext dslContext;

    @Override
    public List<String> findAllRemoteSynchronisationOptionsFromProjectName(String projectName) {
        String xsquash4GitLab = SynchronisationKind.XSQUASH4GITLAB.getName();
        String xsquash4Jira = SynchronisationKind.XSQUASH4JIRA.getName();

        return dslContext
                .select(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_OPTIONS)
                .from(REMOTE_SYNCHRONISATION)
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(REMOTE_SYNCHRONISATION.PROJECT_ID))
                .where(PROJECT.NAME.eq(projectName))
                .and(
                        REMOTE_SYNCHRONISATION
                                .KIND
                                .eq(xsquash4GitLab)
                                .or(REMOTE_SYNCHRONISATION.KIND.eq(xsquash4Jira)))
                .fetchInto(String.class);
    }

    @Override
    public List<String> findAllReqSyncExtenderKeys(Long remoteSyncId) {
        return dslContext
                .select(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID)
                .from(REQUIREMENT_SYNC_EXTENDER)
                .where(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.eq(remoteSyncId))
                .orderBy(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID)
                .fetch(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID, String.class);
    }

    @Override
    public List<Long> findActiveRemoteSyncIdsByPluginId(String pluginId) {
        return dslContext
                .select(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID)
                .from(REMOTE_SYNCHRONISATION)
                .where(REMOTE_SYNCHRONISATION.KIND.eq(pluginId))
                .and(REMOTE_SYNCHRONISATION.SYNC_ENABLE.isTrue())
                .orderBy(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID.desc())
                .fetch(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID, Long.class);
    }
}
