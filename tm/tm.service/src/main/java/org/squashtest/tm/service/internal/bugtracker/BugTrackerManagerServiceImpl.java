/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.bugtracker.CannotDeleteBugtrackerLinkedToSynchronisationException;
import org.squashtest.tm.service.bugtracker.BugTrackerManagerService;
import org.squashtest.tm.service.bugtracker.BugTrackerModificationService;
import org.squashtest.tm.service.bugtracker.BugTrackersLocalService;
import org.squashtest.tm.service.internal.repository.BugTrackerDao;
import org.squashtest.tm.service.internal.repository.BugtrackerProjectDao;
import org.squashtest.tm.service.internal.repository.IssueDao;
import org.squashtest.tm.service.internal.repository.RemoteSynchronisationDao;
import org.squashtest.tm.service.internal.repository.RequirementSyncExtenderDao;
import org.squashtest.tm.service.project.CustomGenericProjectManager;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

@Transactional
@Service("squashtest.tm.service.BugTrackerManagerService")
public class BugTrackerManagerServiceImpl implements BugTrackerManagerService {

    @Inject private BugTrackerDao bugTrackerDao;

    @Inject private BugTrackerModificationService bugTrackerModificationService;

    @Inject private IssueDao issueDao;

    @Inject private BugTrackersLocalService bugtrackersLocalService;

    @Inject private RequirementSyncExtenderDao syncreqDao;

    @Inject private RemoteSynchronisationDao remoteSynchronisationDao;

    @Inject private StoredCredentialsManager credentialsManager;

    @Inject private BugtrackerProjectDao bugtrackerProjectDao;

    @Inject private CustomGenericProjectManager customGenericProjectManager;

    @PersistenceContext private EntityManager entityManager;

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addBugTracker(BugTracker bugTracker) {
        String name = bugTracker.getName();
        BugTracker existing = bugTrackerDao.findByName(name);
        if (existing == null) {
            bugTrackerDao.save(bugTracker);
        } else {
            throw new NameAlreadyInUseException(NameAlreadyInUseException.EntityType.BUG_TRACKER, name);
        }

        // Updating the bugtracker after it was persisted is kind of a hack :  currently there's no way
        // to tell which
        // protocols are allowed for a given bugtracker kind. Connectors need an existing bugtracker
        // instance to do so.
        makeBugTrackerAuthProtocolConsistent(bugTracker);
    }

    private void makeBugTrackerAuthProtocolConsistent(BugTracker bugTracker) {
        List<AuthenticationProtocol> supportedProtocols =
                Arrays.asList(bugTrackerModificationService.getSupportedProtocols(bugTracker));

        if (!supportedProtocols.isEmpty()
                && !supportedProtocols.contains(bugTracker.getAuthenticationProtocol())) {
            bugTracker.setAuthenticationProtocol(supportedProtocols.get(0));
        }
    }

    @Override
    public Set<String> findBugTrackerKinds() {
        return bugtrackersLocalService.getProviderKinds();
    }

    @Override
    public List<BugTracker> findByKind(String kind) {
        return bugTrackerDao.findByKind(kind);
    }

    @Override
    public BugTracker findById(long bugTrackerId) {
        return bugTrackerDao.getReferenceById(bugTrackerId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Page<BugTracker> findSortedBugtrackers(Pageable pageable) {
        return bugTrackerDao.findAll(pageable);
    }

    @Override
    public List<BugTracker> findDistinctBugTrackersForProjects(List<Long> projectIds) {
        if (projectIds.isEmpty()) {
            return Collections.emptyList();
        }
        return bugTrackerDao.findDistinctBugTrackersForProjects(projectIds);
    }

    @Override
    public List<BugTracker> findDistinctBugTrackersForProjectWithOtherJiraBt(
            List<Long> projectIds, List<String> jiraKinds) {
        return bugTrackerDao.findDistinctBugTrackersForProjectsWithJiraBt(projectIds, jiraKinds);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteBugTrackers(final List<Long> bugtrackerIds) {

        Map<Long, List<RemoteSynchronisation>> remoteSyncsByBugTrackerId =
                remoteSynchronisationDao.findByServers(bugtrackerIds).stream()
                        .collect(
                                Collectors.groupingBy(
                                        remoteSynchronisation -> remoteSynchronisation.getServer().getId()));

        for (Map.Entry<Long, List<RemoteSynchronisation>> entry :
                remoteSyncsByBugTrackerId.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                BugTracker bugTracker = bugTrackerDao.findById(entry.getKey()).orElseThrow();
                String synchronizationList = buildSynchronizationList(entry.getValue());
                throw new CannotDeleteBugtrackerLinkedToSynchronisationException(
                        bugTracker.getId(), bugTracker.getName(), synchronizationList);
            }
        }

        deleteIssuesLinkedToBugtrackers(bugtrackerIds);
        deleteLinkedSyncedRequirements(bugtrackerIds);
        deleteCredAndConfForBugTrackers(bugtrackerIds);
        unbindBugTrackersFromProjects(bugtrackerIds);
        doDeleteBugTrackers(bugtrackerIds);
    }

    private String buildSynchronizationList(List<RemoteSynchronisation> synchronizations) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<ul>");
        synchronizations.forEach(
                sync ->
                        stringBuilder.append(
                                String.format("<li>%s - %s</li>", sync.getName(), sync.getProject().getName())));
        stringBuilder.append("</ul>");
        return stringBuilder.toString();
    }

    private void deleteBugtrackersProjects(List<Long> bugTrackerIds) {
        bugtrackerProjectDao.deleteAllByBugTrackerIds(bugTrackerIds);
    }

    private void deleteIssuesLinkedToBugtrackers(List<Long> bugTrackerIds) {
        issueDao.deleteAllByBugTrackerIds(bugTrackerIds);
    }

    private void deleteLinkedSyncedRequirements(List<Long> bugTrackerIds) {
        syncreqDao.deleteAllByBugTrackerIds(bugTrackerIds);
    }

    private void deleteCredAndConfForBugTrackers(List<Long> bugTrackerIds) {
        bugTrackerIds.forEach(
                bugTrackerId -> {
                    credentialsManager.deleteAppLevelCredentials(bugTrackerId);
                    credentialsManager.deleteServerAuthConfiguration(bugTrackerId);
                });
        entityManager.flush();
    }

    private void unbindBugTrackersFromProjects(List<Long> bugTrackerIds) {
        deleteBugtrackersProjects(bugTrackerIds);
        customGenericProjectManager.unbindBugTrackers(bugTrackerIds);
    }

    private void doDeleteBugTrackers(List<Long> bugTrackerIds) {
        bugTrackerDao.deleteAllByIds(bugTrackerIds);
    }
}
