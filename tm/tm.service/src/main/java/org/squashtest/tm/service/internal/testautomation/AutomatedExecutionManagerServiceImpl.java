/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import java.net.URL;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;
import org.squashtest.tm.core.foundation.exception.InvalidUrlException;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.service.campaign.AutomatedSuiteModificationService;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao;
import org.squashtest.tm.service.internal.repository.CustomAutomatedExecExtenderDao;
import org.squashtest.tm.service.testautomation.AutomatedExecutionManagerService;

/**
 * @author Gregory Fouquet
 */
@Service("squashtest.tm.service.testautomation.AutomatedExecutionManagerService")
@Transactional
public class AutomatedExecutionManagerServiceImpl implements AutomatedExecutionManagerService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(AutomatedExecutionManagerServiceImpl.class);

    private final AutomatedExecutionExtenderDao automatedExecutionDao;
    private final ExecutionProcessingService execProcService;
    private final CustomTestSuiteModificationService customTestSuiteModificationService;
    private final AutomatedSuiteModificationService automatedSuiteModificationService;
    private final CustomAutomatedExecExtenderDao customAutomatedExecExtenderDao;

    public AutomatedExecutionManagerServiceImpl(
            AutomatedExecutionExtenderDao automatedExecutionDao,
            ExecutionProcessingService execProcService,
            CustomTestSuiteModificationService customTestSuiteModificationService,
            AutomatedSuiteModificationService automatedSuiteModificationService,
            CustomAutomatedExecExtenderDao customAutomatedExecExtenderDao) {
        this.automatedExecutionDao = automatedExecutionDao;
        this.execProcService = execProcService;
        this.customTestSuiteModificationService = customTestSuiteModificationService;
        this.automatedSuiteModificationService = automatedSuiteModificationService;
        this.customAutomatedExecExtenderDao = customAutomatedExecExtenderDao;
    }

    private void changeState(AutomatedExecutionExtender exec, TestExecutionStatus stateChange) {
        exec.setDuration(stateChange.getDuration());
        exec.setResultSummary(HtmlUtils.htmlEscape(stateChange.getStatusMessage()));
        exec.setExecutionStatus(coerce(stateChange.getStatus()));

        automatedSuiteModificationService.updateExecutionStatus(exec.getAutomatedSuite());
        customTestSuiteModificationService.updateExecutionStatus(
                exec.getExecution().getTestPlan().getTestSuites());

        try {
            URL result = UrlUtils.toUrlOrNull(stateChange.getResultUrl());
            exec.setResultURL(result);
        } catch (InvalidUrlException ex) {
            LOGGER.warn(
                    "Received a result url which does not math any valid url pattern: {}",
                    stateChange.getResultUrl(),
                    ex);
        }

        execProcService.updateExecutionMetadata(exec);
    }

    private ExecutionStatus coerce(
            org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus status) {
        return ExecutionStatus.valueOf(status.name());
    }

    @Override
    public void changeExecutionState(long id, @NotNull TestExecutionStatus stateChange) {
        changeState(automatedExecutionDao.findById(id), stateChange);
    }

    @Override
    public String getExecutionStatus(long automExecExtenderId) {
        return customAutomatedExecExtenderDao.findExecutionStatusByExtenderId(automExecExtenderId);
    }
}
