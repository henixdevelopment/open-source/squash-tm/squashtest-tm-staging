/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.denormalizedenvironment;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentTag;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentTagManagerService;
import org.squashtest.tm.service.internal.repository.DenormalizedEnvironmentTagDao;

@Service("squashtest.tm.service.DenormalizedEnvironmentTagManagerService")
@Transactional
public class DenormalizedEnvironmentTagManagerServiceImpl
        implements DenormalizedEnvironmentTagManagerService {

    private final DenormalizedEnvironmentTagDao denormalizedEnvironmentTagDao;

    public DenormalizedEnvironmentTagManagerServiceImpl(
            DenormalizedEnvironmentTagDao denormalizedEnvironmentTagDao) {
        this.denormalizedEnvironmentTagDao = denormalizedEnvironmentTagDao;
    }

    @Override
    public void createAllDenormalizedEnvironmentTagsForAutomatedExecution(
            List<String> environmentTags, AutomatedExecutionExtender extender) {
        List<DenormalizedEnvironmentTag> denormalizedEnvironmentTags =
                environmentTags.stream()
                        .map(
                                environmentTag ->
                                        new DenormalizedEnvironmentTag(
                                                extender.getId(),
                                                DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER,
                                                environmentTag))
                        .toList();

        denormalizedEnvironmentTagDao.saveAll(denormalizedEnvironmentTags);
    }

    @Override
    public void removeDenormalizedTagsOnExecutionDelete(
            AutomatedExecutionExtender automatedExecutionExtender) {
        DenormalizedEnvironmentHolderType holderType =
                DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER;
        List<DenormalizedEnvironmentTag> tags =
                denormalizedEnvironmentTagDao.findAllByHolderIdAndHolderType(
                        automatedExecutionExtender.getId(), holderType);

        if (!tags.isEmpty()) {
            denormalizedEnvironmentTagDao.deleteAll(tags);
        }
    }
}
