/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.attachment;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.attachment.FileViewerService;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;
import org.squashtest.tm.service.internal.display.dto.FileViewerDto;
import org.squashtest.tm.service.internal.display.dto.FileViewerRequest;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;

@Service
@Transactional(readOnly = true)
public class FileViewerServiceImpl implements FileViewerService {

    private static final String IS_DIRECTORY = "isDirectory";
    private static final String NAME = "name";

    private static final FileViewerExtension[] UNSAFE_EXTENSIONS = {
        FileViewerExtension.HTML, FileViewerExtension.SVG
    };

    @Inject private AttachmentRepository attachmentRepository;

    @Inject private FeatureManager featureManager;

    @Autowired(required = false)
    private HttpServletRequest httpServletRequest;

    @Override
    public FileViewerDto preview(FileViewerRequest viewerRequest) throws IOException {
        final boolean unsafeAttachmentPreviewEnabled =
                featureManager.isEnabled(FeatureManager.Feature.UNSAFE_ATTACHMENT_PREVIEW);
        if (!unsafeAttachmentPreviewEnabled
                && List.of(UNSAFE_EXTENSIONS).contains(viewerRequest.getFileType())) {
            return new FileViewerDto(FileViewerExtension.HTML, "Unsafe files preview is disabled.");
        }

        switch (viewerRequest.getFileType()) {
            case TXT, XML, JSON, LOG, JS, CSS -> {
                String content = IOUtils.toString(getStream(viewerRequest), Charset.defaultCharset());
                return new FileViewerDto(viewerRequest.getFileType(), content);
            }
            case HTML -> {
                String content = IOUtils.toString(getStream(viewerRequest), Charset.defaultCharset());
                content = appendHtmlBaseURI(content, viewerRequest.getTarget(), viewerRequest.getSource());
                return new FileViewerDto(viewerRequest.getFileType(), content);
            }
            case PDF, PNG, JPEG, JPG, SVG, GIF, BMP -> {
                byte[] buffer = getContentBytes(viewerRequest);
                return new FileViewerDto(viewerRequest.getFileType(), buffer);
            }
            default ->
                    throw new IllegalArgumentException(
                            "Invalid extension + " + viewerRequest.getFileType() + " for preview");
        }
    }

    private String appendHtmlBaseURI(String content, String target, String source) {
        if (target == null) {
            return content;
        }

        Document document = Jsoup.parse(content);

        String baseUri = buildHtmlBaseURI(target, source);

        Element script = new Element("base");
        script.attr("href", baseUri);
        document.head().prependChild(script);

        return document.html();
    }

    private String buildHtmlBaseURI(String source, String id) {
        final String separator = "/";
        final String baseUrl =
                httpServletRequest.getContextPath() + "/backend/file-viewer/archive-source/" + id + "/";

        if (source.split(separator).length > 1) {
            final String targetPrefix = "target:";
            String targetPath = targetPrefix + source.substring(0, source.lastIndexOf(separator));
            return baseUrl + targetPath + separator;
        } else {
            return baseUrl;
        }
    }

    @Override
    public TreeGridResponse getArchiveFileTree(FileViewerRequest viewerRequest) throws IOException {
        List<DataRow> dataRows = new ArrayList<>();

        TarArchiveInputStream tarInput = new TarArchiveInputStream(getSourceStream(viewerRequest));
        processArchiveIntoDataRows(tarInput, dataRows);

        TreeGridResponse treeGridResponse = new TreeGridResponse();
        treeGridResponse.setDataRows(dataRows);

        return treeGridResponse;
    }

    private static void processArchiveIntoDataRows(
            TarArchiveInputStream tarInput, List<DataRow> dataRows) throws IOException {
        Map<Path, DataRow> map = new HashMap<>();

        TarArchiveEntry entry;
        while ((entry = tarInput.getNextEntry()) != null) {
            Path entryPath = Paths.get(entry.getName());

            DataRow row = new DataRow();
            String rowId = entry.getName();
            row.setId(rowId);

            Path parentPath = entryPath.getParent();
            if (parentPath != null) {
                processParent(dataRows, row, parentPath, map, rowId);
            }

            if (entry.isDirectory()) {
                row.setState(DataRow.State.leaf);
                row.setData(Map.of(NAME, getFileName(entryPath), IS_DIRECTORY, true));
                map.put(entryPath, row);
            } else {
                row.setState(DataRow.State.leaf);
                row.setData(Map.of(NAME, getFileName(entryPath), IS_DIRECTORY, false));
            }

            dataRows.add(row);
        }
    }

    private static void processParent(
            List<DataRow> dataRows, DataRow row, Path parentPath, Map<Path, DataRow> map, String rowId) {
        row.setParentRowId(parentPath.toString());

        DataRow parent = map.get(parentPath);
        if (parent != null) {
            if (parent.getState().equals(DataRow.State.leaf)) {
                parent.setState(DataRow.State.closed);
            }
            parent.getChildren().add(rowId);
        } else {
            DataRow parentRow = new DataRow();
            parentRow.setId(parentPath.toString());
            parentRow.setState(DataRow.State.closed);
            parentRow.setChildren(List.of(rowId));
            parentRow.setData(Map.of(NAME, getFileName(parentPath), IS_DIRECTORY, true));
            map.put(parentPath, parentRow);
            dataRows.add(parentRow);
            if (parentPath.getParent() != null) {
                processParent(dataRows, parentRow, parentPath.getParent(), map, parentPath.toString());
            }
        }
    }

    private static String getFileName(Path entryPath) {
        return entryPath.getFileName().toString();
    }

    private InputStream getSourceStream(FileViewerRequest viewerRequest) throws IOException {
        String source = viewerRequest.getSource();
        return viewerRequest.isAttachment()
                ? attachmentRepository.getContentStream(Long.parseLong(source))
                : new FileInputStream(source);
    }

    private InputStream getStream(FileViewerRequest viewerRequest) throws IOException {
        if (viewerRequest.getTarget() != null) {
            return getInputStreamFromArchive(
                    getSourceStream(viewerRequest), viewerRequest.getTarget(), viewerRequest.getSource());
        }
        return getSourceStream(viewerRequest);
    }

    private TarArchiveInputStream getInputStreamFromArchive(
            InputStream tarStream, String target, String source) throws IOException {
        Path path = Paths.get(target);
        TarArchiveInputStream tarInput = new TarArchiveInputStream(tarStream);
        TarArchiveEntry entry;

        while ((entry = tarInput.getNextEntry()) != null) {
            Path entryPath = Paths.get(entry.getName());
            if (entryPath.equals(path)) {
                return tarInput;
            }
        }
        throw new FileNotFoundException("File " + target + " not found in archive " + source);
    }

    private byte[] getContentBytes(FileViewerRequest viewerRequest) throws IOException {
        if (viewerRequest.getTarget() != null) {
            return getContentBytesFromArchive(viewerRequest);
        }
        String source = viewerRequest.getSource();
        return viewerRequest.isAttachment()
                ? attachmentRepository.getContentBytes(Long.parseLong(source))
                : Files.readAllBytes(Paths.get(source));
    }

    private byte[] getContentBytesFromArchive(FileViewerRequest viewerRequest) throws IOException {
        Path path = Paths.get(viewerRequest.getTarget());
        TarArchiveInputStream tarInput = new TarArchiveInputStream(getSourceStream(viewerRequest));
        TarArchiveEntry entry;
        while ((entry = tarInput.getNextEntry()) != null) {
            Path entryPath = Paths.get(entry.getName());
            if (entryPath.equals(path)) {
                ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int length;
                while ((length = tarInput.read(buffer)) != -1) {
                    byteOutput.write(buffer, 0, length);
                }
                return byteOutput.toByteArray();
            }
        }
        throw new FileNotFoundException(
                "File " + viewerRequest.getTarget() + " not found in archive " + viewerRequest.getSource());
    }
}
