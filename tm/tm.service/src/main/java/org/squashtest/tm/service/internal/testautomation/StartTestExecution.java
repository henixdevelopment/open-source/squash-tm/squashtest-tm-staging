/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.opentestfactory.messages.Status;
import org.squashtest.squash.automation.tm.commons.ExpectedSuiteDefinition;
import org.squashtest.squash.automation.tm.testplan.library.model.ExecutionConfiguration;
import org.squashtest.squash.automation.tm.testplan.library.model.TestPlan;
import org.squashtest.squash.automation.tm.testplan.library.model.testspecs.Test;
import org.squashtest.squash.automation.tm.testplan.library.model.testspecs.TestSuite;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.internal.testautomation.httpclient.BusClientFactory;
import org.squashtest.tm.service.internal.testautomation.httpclient.MessageRefusedException;
import org.squashtest.tm.service.internal.testautomation.httpclient.SquashAutomBusClient;
import org.squashtest.tm.service.internal.testautomation.httpclient.SquashAutomWorkflowClient;
import org.squashtest.tm.service.internal.testautomation.httpclient.WorkflowClientFactory;
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext;
import org.squashtest.tm.service.internal.testautomation.model.messages.ExecutionError;
import org.squashtest.tm.service.internal.testautomation.model.messages.PublicationStatus;
import org.squashtest.tm.service.internal.testautomation.model.messages.Workflow;
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowHandle;
import org.squashtest.tm.service.internal.testautomation.resultpublisher.ExpectedSuiteDefinitionBuilder;
import org.squashtest.tm.service.testautomation.model.EnvironmentVariableValue;
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;

public class StartTestExecution {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartTestExecution.class);

    private static final String API_VERSION = "opentestfactory.org/v1alpha1";
    private static final String EXECUTION_ERROR_NAME = "Failed.squashAutomExecution";

    private static final String WORKFLOW_NAME_FORMAT = "Workflow for Squash TM %s: %s";
    private static final String TS_CUF_PREFIX = "TS_CUF";
    private static final String CPG_CUF_PREFIX = "CPG_CUF";
    private static final String IT_CUF_PREFIX = "IT_CUF";
    private static final String TC_EXECUTION_ID = "TC_EXECUTION_ID";

    private final BuildDef buildDef;

    private final Collection<SquashAutomExecutionConfiguration> configurations;

    private final String suiteId;

    private final String callbackUrl;

    private final String automatedServerToken;

    private final SquashAutomWorkflowClient workflowClient;

    private final SquashAutomBusClient busClient;
    private final TestPlanContext testPlanContext;
    private final EntityPathHeaderDao entityPathHeaderDao;
    boolean isUltimateLicenseAvailable;

    public StartTestExecution(
            BuildDef buildDef,
            Collection<SquashAutomExecutionConfiguration> configurations,
            String suiteId,
            TestPlanContext testPlanContext,
            String callbackUrl,
            String automatedServerToken,
            WorkflowClientFactory workflowClientFactory,
            BusClientFactory busClientFactory,
            EntityPathHeaderDao entityPathHeaderDao,
            int requestTimeoutSeconds,
            boolean isUltimateLicenseAvailable) {
        this.buildDef = buildDef;
        this.configurations = configurations;
        this.suiteId = suiteId;
        this.testPlanContext = testPlanContext;
        this.workflowClient = getWorkflowClient(buildDef, workflowClientFactory, requestTimeoutSeconds);
        this.busClient =
                busClientFactory.getClient(
                        getBusUrl(buildDef.getServer()),
                        buildDef.getCredentials().getToken(),
                        requestTimeoutSeconds);
        this.entityPathHeaderDao = entityPathHeaderDao;
        this.callbackUrl = callbackUrl;
        this.automatedServerToken = automatedServerToken;
        this.isUltimateLicenseAvailable = isUltimateLicenseAvailable;
    }

    private SquashAutomWorkflowClient getWorkflowClient(
            BuildDef buildDef, WorkflowClientFactory workflowClientFactory, int requestTimeoutSeconds) {
        return workflowClientFactory.getClient(
                buildDef.getServer().getUrl(),
                buildDef.getServer().getObserverUrl(),
                buildDef.getServer().getKillswitchUrl(),
                buildDef.getCredentials().getToken(),
                requestTimeoutSeconds);
    }

    public String run() throws MessageRefusedException {
        TestPlan testPlan = createTestPlan();

        Workflow workflow = createWorkflow(testPlan);

        String workflowId = launchWorkflow(workflow);

        transmitExpectedSuiteData(
                workflowId,
                testPlan,
                workflow,
                callbackUrl,
                automatedServerToken,
                testPlanContext.getSquashTMVersion());

        return workflowId;
    }

    private Workflow createWorkflow(TestPlan testPlan) {

        String workflowName =
                String.format(
                        WORKFLOW_NAME_FORMAT,
                        testPlanContext.getEntityTypeAsString(),
                        testPlanContext.getEntityPath());

        String additionalConfiguration = getAutomationServerAdditionalConfiguration(testPlan);
        return new Workflow(
                API_VERSION, workflowName, testPlan, testPlanContext, additionalConfiguration);
    }

    /**
     * Retrieves the additionalConfiguration matching the automation server of the tests executed in
     * the testPlan.
     *
     * <p>For now, the additionalConfiguration can only be defined at the automation server level. A
     * testPlan contains tests that may come from several projects, but they all have a unique
     * automation server. By retrieving one of the ExecutionConfiguration of one of these projects, we
     * retrieve the additionalConfiguration of the automation server.
     */
    private String getAutomationServerAdditionalConfiguration(TestPlan testPlan) {
        List<Test> tests = testPlan.getTestSuite().getTest();
        if (tests.isEmpty()) {
            return "";
        }
        Long projectId = tests.get(0).getTmProjectId();
        ExecutionConfiguration execConf =
                testPlan.getConfigurations().stream()
                        .filter(conf -> conf.getTmProjectId().equals(projectId))
                        .findFirst()
                        .orElse(null);
        if (execConf != null) {
            return execConf.getAdditionalConfiguration();
        }
        return "";
    }

    private TestPlan createTestPlan() {
        TestPlan testPlan = new TestPlan();
        testPlan.setSuiteId(suiteId);
        List<Test> tests = createTestList();
        testPlan.setTestSuite(new TestSuite(tests));
        testPlan.setNamespace(buildDef.getNamespace());
        testPlan.setConfigurations(createConfigurations(configurations));
        return testPlan;
    }

    /**
     * Convert a Collection of {@link SquashAutomExecutionConfiguration}s into a List of {@link
     * ExecutionConfiguration}.
     *
     * @param sourceConfigurations collection of {@link SquashAutomExecutionConfiguration}s to convert
     * @return a List of {@link ExecutionConfiguration}
     */
    private List<ExecutionConfiguration> createConfigurations(
            Collection<SquashAutomExecutionConfiguration> sourceConfigurations) {
        return sourceConfigurations.stream()
                .map(
                        sourceConfiguration ->
                                new ExecutionConfiguration(
                                        sourceConfiguration.getProjectId(),
                                        sourceConfiguration.getEnvironmentTags(),
                                        adaptVariables(sourceConfiguration.getEnvironmentVariables()),
                                        sourceConfiguration.getAdditionalConfiguration()))
                .toList();
    }

    /**
     * For reasons of backward compatibility between the version of squash TM and that of the
     * orchestrator we send only the uninterpreted variables which have characters not authorized in
     * version 4.1 in an object Map<String, EnvironmentVariableValue>
     *
     * @param environmentVariables
     * @return
     */
    private Map<String, Object> adaptVariables(
            Map<String, EnvironmentVariableValue> environmentVariables) {
        Map<String, Object> result = new HashMap<>();
        final String NOT_INTERPRETED_VALID_PATTERN = "^[a-zA-Z0-9-._\\s]*$";

        environmentVariables.forEach(
                (variableKey, variableValue) -> {
                    if (variableValue.isVerbatim()) {
                        Pattern pattern = Pattern.compile(NOT_INTERPRETED_VALID_PATTERN);
                        Matcher matcher = pattern.matcher(variableValue.getValue());
                        if (matcher.matches()) {
                            result.put(variableKey, variableValue.getValue());
                        } else {
                            result.put(variableKey, variableValue);
                        }
                    } else {
                        result.put(variableKey, variableValue.getValue());
                    }
                });

        return result;
    }

    private List<Test> createTestList() {
        Set<Long> testCaseIds =
                buildDef.getParameterizedItems().stream()
                        .map(param -> param.getIterationTestPlanItem().getReferencedTestCase().getId())
                        .collect(Collectors.toSet());

        Map<Long, String> testCasePaths = entityPathHeaderDao.buildTestCasePathByIds(testCaseIds);

        return buildDef.getParameterizedItems().stream()
                .map(
                        paramedItem -> {
                            Test test = new Test();
                            IterationTestPlanItem item = paramedItem.getIterationTestPlanItem();
                            test.setId(item.getId().toString());
                            TestCase testCase = item.getReferencedTestCase();
                            test.setTmProjectId(testCase.getProject().getId());
                            Map<String, String> optimizedForPluginVersionMap =
                                    removeCampaignCustomFieldsIfCommunity(paramedItem.getCustomFields());
                            test.setParam(optimizedForPluginVersionMap);
                            String testCasePath = testCasePaths.get(testCase.getId());
                            buildTestCaseMetadata(test, testCase, testCasePath);
                            return test;
                        })
                .toList();
    }

    private Map<String, String> removeCampaignCustomFieldsIfCommunity(
            Map<String, Object> customFields) {
        if (isUltimateLicenseAvailable) {
            return customFields.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
        }
        return customFields.entrySet().stream()
                .filter(
                        entry ->
                                !entry.getKey().startsWith(TS_CUF_PREFIX)
                                        && !entry.getKey().startsWith(CPG_CUF_PREFIX)
                                        && !entry.getKey().startsWith(IT_CUF_PREFIX)
                                        && !entry.getKey().equals(TC_EXECUTION_ID))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
    }

    private void buildTestCaseMetadata(Test test, TestCase testCase, String path) {
        test.addMetadata("name", testCase.getName());
        test.addMetadata("reference", testCase.getAutomatedTestReference());
        test.addMetadata("uuid", testCase.getUuid());
        test.addMetadata("nature", testCase.getNature().getCode());
        test.addMetadata("importance", testCase.getImportance().name());
        test.addMetadata("type", testCase.getType().getCode());
        test.addMetadata("technology", testCase.getAutomatedTestTechnology().getName());
        test.addMetadata("path", getPathAsArray(path));
    }

    private List<String> getPathAsArray(String testCasePath) {
        return new ArrayList<>(Arrays.asList(testCasePath.split(" > ")));
    }

    private String launchWorkflow(Workflow workflow) throws MessageRefusedException {
        WorkflowHandle wfHandle = workflowClient.launch(workflow);
        final String workflowStartLog =
                "Workflow '"
                        + workflow.name()
                        + "' sent and received with workflow_id '"
                        + wfHandle.getWorkflowId()
                        + "'";
        LOGGER.info(workflowStartLog);

        return wfHandle.getWorkflowId();
    }

    private void transmitExpectedSuiteData(
            String workflowId,
            TestPlan plan,
            Workflow workflow,
            String callbackUrl,
            String automatedServerToken,
            String squashTmVersion)
            throws MessageRefusedException {

        ExpectedSuiteDefinitionBuilder suiteDefBuilder = new ExpectedSuiteDefinitionBuilder();
        ExpectedSuiteDefinition suiteDefinition =
                suiteDefBuilder.buildExpectedSuiteData(
                        plan, workflowId, workflow, callbackUrl, automatedServerToken, squashTmVersion);
        PublicationStatus publicationResult = busClient.publish(suiteDefinition);

        if (publicationResult.getStatus() == Status.StatusValue.Failure) {
            LOGGER.error(
                    "Failed to publish AutomatedSuite definition for generator {} in workflow {} : {}, {}",
                    workflow.name(),
                    workflowId,
                    publicationResult.getReason(),
                    publicationResult.getDetails());

            Map<String, Object> details = new HashMap<>(publicationResult.getDetails());
            details.put("publisher.error", "failed to publish ExpectedSuite definition.");
            busClient.publish(new ExecutionError(API_VERSION, EXECUTION_ERROR_NAME, workflowId, details));
        } else {
            LOGGER.info("Successfully published AutomatedSuite definition for workflow {}", workflowId);
        }
    }

    private String getBusUrl(TestAutomationServer testAutomationServer) {
        return Optional.ofNullable(testAutomationServer.getEventBusUrl())
                .orElse(testAutomationServer.getUrl());
    }
}
