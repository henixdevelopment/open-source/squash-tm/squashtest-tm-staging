/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.report;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.squashtest.tm.jooq.domain.Tables.REPORT_DEFINITION;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.List;
import java.util.Objects;
import org.jooq.DSLContext;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import org.jooq.tools.json.ParseException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.report.ReportFinderService;

@Service("squashtest.tm.service.ReportFinderService")
@Transactional
public class ReportFinderServiceImpl implements ReportFinderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportFinderServiceImpl.class);
    private final DSLContext dsl;

    public ReportFinderServiceImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public String fetchTemplateFromParametersByReportDefinitionId(Long reportDefinitionId)
            throws ParseException {
        String parameters =
                dsl.select(REPORT_DEFINITION.PARAMETERS)
                        .from(REPORT_DEFINITION)
                        .where(REPORT_DEFINITION.REPORT_ID.eq(reportDefinitionId))
                        .fetchOne()
                        .value1();

        JSONObject templateFileNameObject = retrieveTemplateFileNameFromParameters(parameters);
        return Objects.isNull(templateFileNameObject)
                ? EMPTY
                : (String) templateFileNameObject.get("val");
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public boolean isTemplateUsedInReport(String templateFileName, String namespace) {
        List<String> parametersList =
                dsl.select(REPORT_DEFINITION.PARAMETERS)
                        .from(REPORT_DEFINITION)
                        .where(REPORT_DEFINITION.PLUGIN_NAMESPACE.eq(namespace))
                        .and(REPORT_DEFINITION.PARAMETERS.contains(templateFileName))
                        .fetchInto(String.class);

        for (String parameters : parametersList) {
            if (checkIfTemplateUsed(templateFileName, parameters)) {
                return true;
            }
        }

        return false;
    }

    private boolean checkIfTemplateUsed(String templateFileName, String parameters) {
        JSONObject templateFileNameObject = null;
        try {
            templateFileNameObject = retrieveTemplateFileNameFromParameters(parameters);
        } catch (ParseException e) {
            LOGGER.debug("An error occurred while retrieving template file name from parameters", e);
        }
        return Objects.nonNull(templateFileNameObject)
                && templateFileName.equals(templateFileNameObject.get("val"));
    }

    private JSONObject retrieveTemplateFileNameFromParameters(String parameters)
            throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject parametersJson = (JSONObject) parser.parse(parameters);
        return (JSONObject) parametersJson.get("templateFileName");
    }
}
