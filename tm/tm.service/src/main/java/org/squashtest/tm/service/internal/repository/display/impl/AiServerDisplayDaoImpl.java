/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.AI_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;

import java.util.List;
import java.util.Objects;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.AiServerAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.AiServerDto;
import org.squashtest.tm.service.internal.repository.display.AiServerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class AiServerDisplayDaoImpl implements AiServerDisplayDao {

    private final DSLContext dslContext;

    public AiServerDisplayDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public AiServerAdminViewDto findAiServerById(Long aiServerId) {
        AiServerAdminViewDto result =
                dslContext
                        .select(
                                AI_SERVER.SERVER_ID.as(RequestAliasesConstants.ID),
                                THIRD_PARTY_SERVER.DESCRIPTION,
                                THIRD_PARTY_SERVER.LAST_MODIFIED_ON,
                                THIRD_PARTY_SERVER.LAST_MODIFIED_BY,
                                THIRD_PARTY_SERVER.CREATED_ON,
                                THIRD_PARTY_SERVER.CREATED_BY,
                                AI_SERVER.PAYLOAD_TEMPLATE,
                                AI_SERVER.JSON_PATH,
                                THIRD_PARTY_SERVER.NAME,
                                THIRD_PARTY_SERVER.URL,
                                THIRD_PARTY_SERVER.AUTH_POLICY,
                                THIRD_PARTY_SERVER.AUTH_PROTOCOL)
                        .from(AI_SERVER)
                        .innerJoin(THIRD_PARTY_SERVER)
                        .on(AI_SERVER.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                        .where(AI_SERVER.SERVER_ID.eq(aiServerId))
                        .fetchOneInto(AiServerAdminViewDto.class);

        return Objects.requireNonNull(
                result, String.format("Could not find AI server with ID %d.", aiServerId));
    }

    @Override
    public List<AiServerDto> findAll() {
        return dslContext
                .select(
                        AI_SERVER.SERVER_ID.as(RequestAliasesConstants.ID),
                        THIRD_PARTY_SERVER.DESCRIPTION,
                        AI_SERVER.PAYLOAD_TEMPLATE,
                        THIRD_PARTY_SERVER.LAST_MODIFIED_BY,
                        THIRD_PARTY_SERVER.LAST_MODIFIED_ON,
                        THIRD_PARTY_SERVER.CREATED_BY,
                        THIRD_PARTY_SERVER.CREATED_ON,
                        AI_SERVER.JSON_PATH,
                        THIRD_PARTY_SERVER.URL,
                        THIRD_PARTY_SERVER.NAME)
                .from(AI_SERVER)
                .innerJoin(THIRD_PARTY_SERVER)
                .on(THIRD_PARTY_SERVER.SERVER_ID.eq(AI_SERVER.SERVER_ID))
                .orderBy(THIRD_PARTY_SERVER.NAME)
                .fetchInto(AiServerDto.class);
    }
}
