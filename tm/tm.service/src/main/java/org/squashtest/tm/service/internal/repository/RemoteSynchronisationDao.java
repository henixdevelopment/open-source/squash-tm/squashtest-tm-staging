/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.servers.ThirdPartyServer;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;

public interface RemoteSynchronisationDao
        extends JpaRepository<RemoteSynchronisation, Long>, CustomRemoteSynchronisationDao {

    List<RemoteSynchronisation> findByKind(String kind);

    List<RemoteSynchronisation> findByNameAndKind(String name, String kind);

    List<RemoteSynchronisation> findByProjectIdAndKind(Long projectId, String kind);

    List<RemoteSynchronisation> findByProjectIdInAndKind(List<Long> projectIds, String kind);

    /**
     * Return the list of {@link RemoteSynchronisation} using the given {@link ThirdPartyServer}
     *
     * @param serverId {@link ThirdPartyServer} id
     * @return list of {@link RemoteSynchronisation} with their {@link
     *     org.squashtest.tm.domain.project.Project}
     */
    @Query
    List<RemoteSynchronisation> findWithProjectByServer(@Param("serverId") Long serverId);

    @Query("select rs from RemoteSynchronisation rs where rs.server.id in :serverIds")
    List<RemoteSynchronisation> findByServers(@Param("serverIds") List<Long> serverIds);

    @Query
    List<RemoteSynchronisation> findByProjectId(@Param("projectId") Long projectId);

    @Query
    List<RemoteSynchronisation> findByOwnerId(@Param("ownerId") Long userId);

    @Query
    @Modifying
    void deleteByProjectId(@Param("projectId") long projectId);
}
