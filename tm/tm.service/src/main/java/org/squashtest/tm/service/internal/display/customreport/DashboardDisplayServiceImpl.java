/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.customreport;

import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.display.customreport.DashboardDisplayService;
import org.squashtest.tm.service.internal.repository.display.PartyPreferenceDisplayDao;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional(readOnly = true)
public class DashboardDisplayServiceImpl implements DashboardDisplayService {

    private UserAccountService userAccountService;

    private PartyPreferenceDisplayDao partyPreferenceDisplayDao;

    public DashboardDisplayServiceImpl(
            UserAccountService userAccountService, PartyPreferenceDisplayDao partyPreferenceDisplayDao) {
        this.userAccountService = userAccountService;
        this.partyPreferenceDisplayDao = partyPreferenceDisplayDao;
    }

    @Override
    public List<Workspace> findFavoriteWorkspaceForDashboardAndCurrentUser(Long dashboardId) {

        Long userId = userAccountService.findCurrentUser().getId();
        List<String> preferenceKeys =
                partyPreferenceDisplayDao.findPreferenceKeysForFavoriteDashboardAndUser(
                        dashboardId, userId);
        List<CorePartyPreference> corePartyPreferences =
                Arrays.stream(CorePartyPreference.values())
                        .filter(preference -> preferenceKeys.contains(preference.getPreferenceKey()))
                        .toList();

        return corePartyPreferences.stream().map(this::getFavoriteWorkspace).toList();
    }

    private Workspace getFavoriteWorkspace(CorePartyPreference preference) {
        return switch (preference) {
            case FAVORITE_DASHBOARD_HOME -> Workspace.HOME;
            case FAVORITE_DASHBOARD_CAMPAIGN -> Workspace.CAMPAIGN;
            case FAVORITE_DASHBOARD_REQUIREMENT -> Workspace.REQUIREMENT;
            case FAVORITE_DASHBOARD_TEST_CASE -> Workspace.TEST_CASE;
            default ->
                    throw new IllegalArgumentException(
                            "This preference is not a favorite dashboard pref " + preference.getPreferenceKey());
        };
    }
}
