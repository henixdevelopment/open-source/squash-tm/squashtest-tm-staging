/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.annotation;

import javax.inject.Inject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;

@Aspect
@Component
public class IsUltimateLicenseAvailableAspect {

    @Inject private UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService;

    boolean isUltimateLicenseAvailable() {
        return ultimateLicenseAvailabilityService.isAvailable();
    }

    @Around("@annotation(IsUltimateLicenseAvailable)")
    public Object checkUltimateLicense(ProceedingJoinPoint joinPoint) throws Throwable {
        String errorMessage =
                "Forbidden " + HttpStatus.FORBIDDEN.value() + " Ultimate License Not Available";
        if (!isUltimateLicenseAvailable()) {
            throw new AccessDeniedException(errorMessage);
        }
        return joinPoint.proceed();
    }
}
