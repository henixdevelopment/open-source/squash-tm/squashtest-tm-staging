/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import static org.squashtest.tm.service.internal.repository.EntityGraphName.ACTION_WORD_LIBRARY;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENTS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENT_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.AUTOMATION_REQUEST_LIBRARY;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.BUGTRACKER_PROJECTS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CUSTOM_REPORT_LIBRARY;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ENABLED_PLUGINS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.REQUIREMENT_LIBRARY;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_AUTOMATION_PROJECTS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_CASE_LIBRARY;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_PROJECT_BY_ID;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.project.ProjectVisitor;
import org.squashtest.tm.domain.projectfilter.ProjectFilter;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestLibrary;
import org.squashtest.tm.exception.library.CannotDeleteProjectException;
import org.squashtest.tm.service.customfield.CustomFieldBindingModificationService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.actionword.ActionWordArranger;
import org.squashtest.tm.service.internal.actionword.ProjectLinkedToActionWordDeletionValidator;
import org.squashtest.tm.service.internal.campaign.CampaignNodeDeletionHandler;
import org.squashtest.tm.service.internal.library.NodeDeletionHandler;
import org.squashtest.tm.service.internal.project.ProjectDeletionHandler;
import org.squashtest.tm.service.internal.project.ProjectHelper;
import org.squashtest.tm.service.internal.repository.ActionWordLibraryNodeDao;
import org.squashtest.tm.service.internal.repository.CustomReportLibraryNodeDao;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.ProjectFilterDao;
import org.squashtest.tm.service.internal.repository.RemoteSynchronisationDao;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;
import org.squashtest.tm.service.internal.requirement.RequirementNodeDeletionHandler;
import org.squashtest.tm.service.internal.testcase.TestCaseNodeDeletionHandler;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.security.ObjectIdentityService;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;
import org.squashtest.tm.service.tf.AutomationRequestModificationService;

@Component("squashtest.tm.service.deletion.ProjectDeletionHandler")
public class ProjectDeletionHandlerImpl implements ProjectDeletionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectDeletionHandlerImpl.class);

    @Inject private ProjectDao projectDao;
    @Inject private GenericProjectDao genericProjectDao;
    @Inject private CampaignNodeDeletionHandler campaignDeletionHandler;
    @Inject private TestCaseNodeDeletionHandler testCaseDeletionHandker;
    @Inject private RequirementNodeDeletionHandler requirementDeletionHandler;

    @Inject private AutomationRequestModificationService autorequestModificationService;

    @Inject private ObjectIdentityService objectIdentityService;

    @Inject private ProjectsPermissionManagementService projectPermissionManagementService;

    @PersistenceContext private EntityManager em;

    @Inject private CustomReportLibraryNodeDao crlnDao;

    @Inject private ActionWordLibraryNodeDao awlnDao;

    @Autowired
    private ProjectLinkedToActionWordDeletionValidator projectLinkedToActionWordDeletionValidator;

    @Inject private CustomReportLibraryNodeService crlnService;

    @Inject private CustomFieldBindingModificationService bindingService;

    @Inject private MilestoneBindingManagerService milestoneBindingManager;

    @Inject private RemoteSynchronisationDao remoteSynchronisationDao;

    @Inject private ProjectFilterDao projectFilterDao;

    @Inject private TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService;

    @Inject private ActionWordArranger actionWordArranger;

    @Inject private StoredCredentialsManager storedCredentialsManager;

    @Inject private EnvironmentVariableBindingService environmentVariableBindingService;

    @Inject private GridConfigurationService gridConfigurationService;

    @Override
    public void deleteProject(long projectId) {
        GenericProject project = genericProjectDao.getReferenceById(projectId);

        // This must be done before removing the cufBindings to avoid deletion cascading
        if (ProjectHelper.isTemplate(project)) {
            projectDao.unbindAllFromTemplate(project.getId());
        }

        actionWordArranger.arrangeActionWordsBeforeProjectDeleting(projectId);
        milestoneBindingManager.unbindAllMilestonesFromProject(project);
        bindingService.removeCustomFieldBindings(projectId);
        environmentVariableBindingService.unbindAllByEntityIdAndType(
                projectId, EVBindableEntity.PROJECT);
        gridConfigurationService.deleteColumnConfigForProject(projectId);
        doDeleteProject(projectId);
    }

    @Override
    public void checkProjectContainsOnlyFolders(long projectId) {

        long nonFolder = projectDao.countNonFoldersInProject(projectId);
        long nonActionWordFolder = projectDao.countNonFolderInActionWord(projectId);

        LOGGER.debug("The project #{} contains {} non folder library nodes", projectId, nonFolder);
        if (nonFolder > 0L
                || !projectLinkedToActionWordDeletionValidator.isDeletable(nonActionWordFolder)) {
            throw new CannotDeleteProjectException("non-folders are found in the project");
        }
    }

    @Override
    public void checkProjectContainsOnlyFolders(Project project) {
        checkProjectContainsOnlyFolders(project.getId());
    }

    private void doDeleteProject(long projectId) {
        LOGGER.debug("The project #{} is being deleted", projectId);

        deleteAllLibrariesContent(projectId);

        GenericProject project = loadProjectForDeletion(projectId);

        project.accept(
                new ProjectVisitor() {
                    @Override
                    public void visit(ProjectTemplate projectTemplate) {
                        // NOOP
                    }

                    @Override
                    public void visit(Project project) {
                        removeProjectFromFilters(project);
                    }
                });

        CustomReportLibrary customReportLibrary = project.getCustomReportLibrary();
        deleteCustomReportLibrary(customReportLibrary);

        ActionWordLibrary actionWordLibrary = project.getActionWordLibrary();
        deleteActionWordLibraryNode(actionWordLibrary);

        deleteProjectCredentials(project);

        /*Tm-903*/
        removeRemoteSynchronisation(projectId);

        removeACLsForProjectAndLibraries(project);
        deleteTemplateConfigurableProjectBindings(project);
        genericProjectDao.delete(project);
    }

    private GenericProject loadProjectForDeletion(long projectId) {
        return new EntityGraphQueryBuilder<>(em, GenericProject.class, FIND_PROJECT_BY_ID)
                .addAttributeNodes(
                        CUSTOM_REPORT_LIBRARY,
                        AUTOMATION_REQUEST_LIBRARY,
                        ACTION_WORD_LIBRARY,
                        REQUIREMENT_LIBRARY,
                        CAMPAIGN_LIBRARY,
                        TEST_CASE_LIBRARY,
                        BUGTRACKER_PROJECTS,
                        TEST_AUTOMATION_PROJECTS,
                        ATTACHMENT_LIST)
                .addSubGraph(ATTACHMENT_LIST, ATTACHMENTS)
                .addSubGraph(CUSTOM_REPORT_LIBRARY, ATTACHMENT_LIST)
                .addSubgraphToSubgraph(CUSTOM_REPORT_LIBRARY, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubGraph(ACTION_WORD_LIBRARY, ATTACHMENT_LIST)
                .addSubgraphToSubgraph(ACTION_WORD_LIBRARY, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubGraph(REQUIREMENT_LIBRARY, ATTACHMENT_LIST, ENABLED_PLUGINS)
                .addSubgraphToSubgraph(REQUIREMENT_LIBRARY, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubGraph(CAMPAIGN_LIBRARY, ATTACHMENT_LIST, ENABLED_PLUGINS)
                .addSubgraphToSubgraph(CAMPAIGN_LIBRARY, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubGraph(TEST_CASE_LIBRARY, ATTACHMENT_LIST, ENABLED_PLUGINS)
                .addSubgraphToSubgraph(TEST_CASE_LIBRARY, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubGraph(AUTOMATION_REQUEST_LIBRARY, ATTACHMENT_LIST)
                .addSubgraphToSubgraph(AUTOMATION_REQUEST_LIBRARY, ATTACHMENT_LIST, ATTACHMENTS)
                .execute(Collections.singletonMap(ParameterNames.PROJECT_ID, projectId));
    }

    // Avoid hanging bindings
    private void deleteTemplateConfigurableProjectBindings(GenericProject project) {
        templateConfigurablePluginBindingService.removeAllForGenericProject(project.getId());
    }

    @Override
    public void deleteAllLibrariesContent(Long projectId) {

        List<Long> testCaseLibraryNodeIds = projectDao.findTestCaseLibraryNodesIds(projectId);
        deleteLibraryContent(testCaseLibraryNodeIds, testCaseDeletionHandker);

        List<Long> requirementLibraryNodeIds = projectDao.findRequirementLibraryNodesIds(projectId);
        deleteLibraryContent(requirementLibraryNodeIds, requirementDeletionHandler);

        deleteCustomReportLibraryContent(projectId);

        List<Long> campaignLibraryNodeIds = projectDao.findCampaignLibraryNodesIds(projectId);
        deleteLibraryContent(campaignLibraryNodeIds, campaignDeletionHandler);

        autorequestModificationService.deleteRequestByProjectId(projectId);
    }

    private void deleteCustomReportLibraryContent(Long projectId) {
        // 1 delete library content ie folders as we cannot have other things because eit was checked
        // early
        List<Long> descendantIds = crlnDao.findAllNodeIdsForLibraryEntity(projectId);
        crlnService.delete(descendantIds);
    }

    private void deleteCustomReportLibrary(CustomReportLibrary customReportLibrary) {
        // 2 Check if node exist because of Issue 6499 which was basically executing only part of the
        // process (Thanks to the re-indexation optimization mess)
        if (crlnDao.countNodeFromEntity(customReportLibrary).equals(1L)) {
            // 3 delete the node representing the library. The deletion of the CustomReportLibrary entity
            // will be handled by cascade delete on Project in the last part of the deletion process
            deleteCustomReportLibraryNode(customReportLibrary);
        }
    }

    private void deleteCustomReportLibraryNode(CustomReportLibrary customReportLibrary) {
        CustomReportLibraryNode node = crlnDao.findNodeFromEntity(customReportLibrary);
        node.setLibrary(null);
        node.setEntity(null);
        em.remove(node);
        em.flush();
    }

    private void deleteActionWordLibraryNode(ActionWordLibrary actionWordLibrary) {
        ActionWordLibraryNode node = awlnDao.findNodeFromEntity(actionWordLibrary);
        node.setLibrary(null);
        node.setEntity(null);
        em.remove(node);
        em.flush();
    }

    private void deleteProjectCredentials(GenericProject project) {
        storedCredentialsManager.deleteAllProjectCredentials(project.getId());
    }

    private void removeACLsForProjectAndLibraries(GenericProject project) {
        long rlId = project.getRequirementLibrary().getId();
        long tclId = project.getTestCaseLibrary().getId();
        long clId = project.getCampaignLibrary().getId();
        long crlId = project.getCustomReportLibrary().getId();
        long awlId = project.getActionWordLibrary().getId();
        long arlId = project.getAutomationRequestLibrary().getId();

        // remove arse for libraries
        projectPermissionManagementService.removeAllPermissionsFromObject(
                RequirementLibrary.class, rlId);
        projectPermissionManagementService.removeAllPermissionsFromObject(TestCaseLibrary.class, tclId);
        projectPermissionManagementService.removeAllPermissionsFromObject(CampaignLibrary.class, clId);
        projectPermissionManagementService.removeAllPermissionsFromObject(
                CustomReportLibrary.class, crlId);
        projectPermissionManagementService.removeAllPermissionsFromObject(
                ActionWordLibrary.class, awlId);
        projectPermissionManagementService.removeAllPermissionsFromObject(
                AutomationRequestLibrary.class, arlId);
        // remove aoi for libaries
        objectIdentityService.removeObjectIdentity(rlId, RequirementLibrary.class);
        objectIdentityService.removeObjectIdentity(tclId, TestCaseLibrary.class);
        objectIdentityService.removeObjectIdentity(clId, CampaignLibrary.class);
        objectIdentityService.removeObjectIdentity(crlId, CustomReportLibrary.class);
        objectIdentityService.removeObjectIdentity(awlId, ActionWordLibrary.class);
        objectIdentityService.removeObjectIdentity(arlId, AutomationRequestLibrary.class);
        // remove arse for project
        // and remove aoi for project
        project.accept(
                new ProjectVisitor() {
                    @Override
                    public void visit(ProjectTemplate projectTemplate) {
                        projectPermissionManagementService.removeAllPermissionsFromObject(
                                ProjectTemplate.class, projectTemplate.getId());
                        objectIdentityService.removeObjectIdentity(
                                projectTemplate.getId(), ProjectTemplate.class);
                    }

                    @Override
                    public void visit(Project project) {
                        projectPermissionManagementService.removeAllPermissionsFromObject(
                                Project.class, project.getId());
                        objectIdentityService.removeObjectIdentity(project.getId(), Project.class);
                    }
                });
    }

    private void removeRemoteSynchronisation(Long projectId) {
        List<RemoteSynchronisation> listRemoteSync =
                remoteSynchronisationDao.findByProjectId(projectId);
        remoteSynchronisationDao.deleteAll(listRemoteSync);
    }

    @Override
    public void removeProjectFromFilters(Project project) {
        List<ProjectFilter> projectFilters = projectFilterDao.findByProjectsId(project.getId());
        for (ProjectFilter projectFilter : projectFilters) {
            projectFilter.removeProject(project);
        }
    }

    @Override
    public void checkProjectHasActivePlugin(Project project) {
        checkProjectHasActivePlugin(project.getId());
    }

    @Override
    public void checkProjectHasActivePlugin(long projectId) {
        BigInteger numberActivePlugin = projectDao.countActivePluginInProject(projectId);

        if ((numberActivePlugin.longValue()) > 0L) {
            throw new CannotDeleteProjectException("Active plugins exist in the project");
        }
    }

    private void deleteLibraryContent(
            List<Long> libraryNodeIds, NodeDeletionHandler<?, ?> deletionHandler) {
        if (!libraryNodeIds.isEmpty()) {
            deletionHandler.deleteNodes(libraryNodeIds);
        }
    }
}
