/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.actionword;

import static org.squashtest.tm.api.security.acls.Permissions.CREATE;
import static org.squashtest.tm.api.security.acls.Permissions.DELETE;
import static org.squashtest.tm.api.security.acls.Permissions.EXECUTE;
import static org.squashtest.tm.api.security.acls.Permissions.EXPORT;
import static org.squashtest.tm.api.security.acls.Permissions.WRITE;
import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;

import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.actionword.ActionWordTreeDefinition;
import org.squashtest.tm.domain.tree.TreeLibraryNode;
import org.squashtest.tm.service.internal.dto.json.JsTreeNode;
import org.squashtest.tm.service.security.PermissionEvaluationService;

/**
 * @author qtran - created on 28/07/2020
 */
@Component("actionWord.nodeBuilder")
public class ActionWordTreeNodeBuilder {
    private static final String[] PERM_NAMES = {
        WRITE.name(), CREATE.name(), DELETE.name(), EXECUTE.name(), EXPORT.name()
    };
    private static final Permissions[] NODE_PERMISSIONS = {WRITE, CREATE, DELETE, EXECUTE, EXPORT};

    public static final String ACTION_WORD_LIBRARY_ID_BUILDER = "ActionWordLibrary-";
    public static final String ACTION_WORD_ID_BUILDER = "ActionWord-";

    private final PermissionEvaluationService permissionEvaluationService;

    @Inject
    public ActionWordTreeNodeBuilder(PermissionEvaluationService permissionEvaluationService) {
        super();
        this.permissionEvaluationService = permissionEvaluationService;
    }

    public List<JsTreeNode> build(List<ActionWordLibraryNode> nodes) {
        return nodes.stream().map(this::build).toList();
    }

    public JsTreeNode build(ActionWordLibraryNode awln) {
        JsTreeNode builtNode = new JsTreeNode();
        builtNode.setTitle(HtmlUtils.htmlEscape(awln.getName()));
        builtNode.addAttr("resId", String.valueOf(awln.getId()));
        builtNode.addAttr("name", HtmlUtils.htmlEscape(awln.getName()));

        doPermissionCheck(builtNode, awln);

        ActionWordTreeDefinition entityType = awln.getEntityType();

        switch (entityType) {
            case LIBRARY:
                doLibraryBuild(builtNode, awln);
                break;
            case ACTION_WORD:
                doActionWordBuild(builtNode, awln);
                break;
            case FOLDER:
                throw new UnsupportedOperationException("To be continued...");
            default:
                throw new UnsupportedOperationException(
                        "The node builder isn't implemented for node of type : " + entityType);
        }

        return builtNode;
    }

    private void doPermissionCheck(JsTreeNode builtNode, ActionWordLibraryNode awln) {
        Map<String, Boolean> permByName =
                permissionEvaluationService.hasRoleOrPermissionsOnObject(ROLE_ADMIN, PERM_NAMES, awln);
        for (Permissions perm : NODE_PERMISSIONS) {
            builtNode.addAttr(perm.name(), permByName.get(perm.name()).toString());
        }
    }

    private void doLibraryBuild(JsTreeNode builtNode, ActionWordLibraryNode awln) {
        setNodeHTMLId(builtNode, ACTION_WORD_LIBRARY_ID_BUILDER + awln.getId());
        setNodeRel(builtNode, "drive");
        setNodeResType(builtNode, "action-word-libraries");
        setStateForNodeContainer(builtNode, awln);
    }

    private void setNodeHTMLId(JsTreeNode builtNode, String id) {
        builtNode.addAttr("id", id);
    }

    private void setNodeRel(JsTreeNode builtNode, String rel) {
        builtNode.addAttr("rel", rel);
    }

    private void setNodeResType(JsTreeNode builtNode, String resType) {
        builtNode.addAttr("resType", resType);
    }

    private void setStateForNodeContainer(JsTreeNode builtNode, TreeLibraryNode tln) {
        if (tln.hasContent()) {
            builtNode.setState(JsTreeNode.State.closed);
        } else {
            builtNode.setState(JsTreeNode.State.leaf);
        }
    }

    private void doActionWordBuild(JsTreeNode builtNode, ActionWordLibraryNode awln) {
        setNodeHTMLId(builtNode, ACTION_WORD_ID_BUILDER + awln.getId());
        setNodeRel(builtNode, "action-word");
        setNodeResType(builtNode, "action-words");
        setStateForNodeContainer(builtNode, awln);
    }
}
