/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.service.internal.dto.projectimporter.AttachmentToImport;

public class ActionTestStepToImport {

    private String internalId;
    private String action;
    private String expectedResult;

    private List<String> verifiedRequirementIds;
    private Map<Long, RawValue> customFields = new HashMap<>();
    private List<AttachmentToImport> attachments = new ArrayList<>();

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    @CleanHtml
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @CleanHtml
    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    public Map<Long, RawValue> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<Long, RawValue> customFields) {
        this.customFields = customFields;
    }

    public List<String> getVerifiedRequirementIds() {
        return verifiedRequirementIds;
    }

    public void setVerifiedRequirementIds(List<String> verifiedRequirementIds) {
        this.verifiedRequirementIds = verifiedRequirementIds;
    }

    public List<AttachmentToImport> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentToImport> attachments) {
        this.attachments = attachments;
    }

    public ActionTestStep toActionTestStep() {
        ActionTestStep actionTestStep = new ActionTestStep();
        if (Objects.nonNull(action)) {
            actionTestStep.setAction(action);
        }

        if (Objects.nonNull(expectedResult)) {
            actionTestStep.setExpectedResult(expectedResult);
        }

        return actionTestStep;
    }
}
