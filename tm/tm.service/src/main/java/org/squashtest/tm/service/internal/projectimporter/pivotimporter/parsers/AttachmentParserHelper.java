/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter.parsers;

import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.dto.projectimporter.AttachmentToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;

public final class AttachmentParserHelper {

    private AttachmentParserHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static List<AttachmentToImport> parseAttachments(JsonParser jsonParser)
            throws IOException {
        List<AttachmentToImport> attachments = new ArrayList<>();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                parseAttachment(jsonParser, attachments);
            }
        }
        return attachments;
    }

    private static void parseAttachment(JsonParser jsonParser, List<AttachmentToImport> attachments)
            throws IOException {
        AttachmentToImport attachment = new AttachmentToImport();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            switch (fieldName) {
                case JsonImportField.ZIP_IMPORT_FILE_NAME ->
                        attachment.setZipImportFileName(jsonParser.getText());
                case JsonImportField.ORIGINAL_FILE_NAME ->
                        attachment.setOriginalFileName(jsonParser.getText());
                default -> {
                    // continue parsing
                }
            }
        }
        attachments.add(attachment);
    }
}
