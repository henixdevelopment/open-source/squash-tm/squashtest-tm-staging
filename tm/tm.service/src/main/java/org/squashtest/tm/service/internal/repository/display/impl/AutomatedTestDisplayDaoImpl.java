/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.tables.AutomatedTest.AUTOMATED_TEST;

import javax.inject.Inject;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.AutomatedTestDto;
import org.squashtest.tm.service.internal.repository.display.AutomatedTestDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class AutomatedTestDisplayDaoImpl implements AutomatedTestDisplayDao {

    @Inject private DSLContext dsl;

    @Override
    public AutomatedTestDto findByTestCaseId(Long testCaseId) {
        return dsl.select(
                        AUTOMATED_TEST.TEST_ID.as(RequestAliasesConstants.ID),
                        AUTOMATED_TEST.NAME,
                        DSL.concat(
                                        DSL.value("/"),
                                        TEST_AUTOMATION_PROJECT.LABEL,
                                        DSL.value("/"),
                                        AUTOMATED_TEST.NAME)
                                .as("FULL_LABEL"))
                .from(TEST_CASE)
                .innerJoin(AUTOMATED_TEST)
                .on(AUTOMATED_TEST.TEST_ID.eq(TEST_CASE.TA_TEST))
                .innerJoin(TEST_AUTOMATION_PROJECT)
                .on(AUTOMATED_TEST.PROJECT_ID.eq(TEST_AUTOMATION_PROJECT.TA_PROJECT_ID))
                .where(TEST_CASE.TCLN_ID.eq(testCaseId))
                .fetchOneInto(AutomatedTestDto.class);
    }
}
