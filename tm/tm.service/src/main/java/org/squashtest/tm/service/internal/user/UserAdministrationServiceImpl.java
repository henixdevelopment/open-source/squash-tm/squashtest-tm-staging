/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.user;

import static org.squashtest.tm.domain.users.UsersGroup.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.UnauthorizedPasswordChange;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.domain.users.UsersGroup;
import org.squashtest.tm.exception.IllegalUserGroupTransitionException;
import org.squashtest.tm.exception.user.ActiveUserDeleteException;
import org.squashtest.tm.exception.user.LoginAlreadyExistsException;
import org.squashtest.tm.exception.user.MilestoneOwnerDeleteException;
import org.squashtest.tm.exception.user.UserWithSynchronisationsDeleteException;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.feature.FeatureManager.Feature;
import org.squashtest.tm.service.internal.display.dto.MilestonePossibleOwnerDto;
import org.squashtest.tm.service.internal.dto.DetailedUserDto;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.ProfileDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.RemoteSynchronisationDao;
import org.squashtest.tm.service.internal.repository.TeamDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.repository.UsersGroupDao;
import org.squashtest.tm.service.internal.security.UserBuilder;
import org.squashtest.tm.service.license.LicenseHelperService;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.milestone.MilestoneManagerService;
import org.squashtest.tm.service.project.ProjectFilterModificationService;
import org.squashtest.tm.service.security.AdministratorAuthenticationService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;
import org.squashtest.tm.service.user.AuthenticatedUser;
import org.squashtest.tm.service.user.UserAccountService;
import org.squashtest.tm.service.user.UserAdministrationService;

/**
 * @author bsiri
 */
@Service("squashtest.tm.service.AdministrationService")
@Transactional
public class UserAdministrationServiceImpl implements UserAdministrationService {

    private static final String PLUGIN_LICENSE_EXPIRATION = "plugin.license.expiration";
    private static final String ACTIVATED_USER_EXCESS = "activated.user.excess";
    public static final String AUTOMATION_WORKSPACE_URL = "/automation-workspace";
    public static final String HOME_WORKSPACE_URL = "/home-workspace";

    @Inject private UserAccountService userAccountService;
    @Inject private UserDisplayService userDisplayService;

    @Inject private ProjectDao projectDao;

    @Inject private UserDao userDao;

    @Inject private UsersGroupDao groupDao;

    @Inject private ConfigurationService configurationService;

    @Inject private TeamDao teamDao;

    @Inject private ObjectAclService aclService;

    @Inject private AdministratorAuthenticationService adminAuthentService;

    @Inject private MilestoneManagerService milestoneManagerService;

    @Inject private FeatureManager features;

    @Inject private LicenseHelperService licenseHelperService;

    @Inject private SessionRegistry sessionRegistry;

    @Inject private RemoteSynchronisationDao remoteSynchronisationDao;

    @Inject private ProjectFilterModificationService projectFilterModificationService;

    @Inject private PermissionEvaluationService permissionEvaluationService;
    @Inject private UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService;
    @Inject private ProfileDao profileDao;

    @PersistenceContext private EntityManager em;

    public void setAdministratorAuthenticationService(
            AdministratorAuthenticationService adminService) {
        this.adminAuthentService = adminService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /* **************** delegate user section, so is security ************ */

    @Override
    public void modifyUserFirstName(long userId, String newName) {
        userAccountService.modifyUserFirstName(userId, newName);
    }

    @Override
    public void modifyUserLastName(long userId, String newName) {
        userAccountService.modifyUserLastName(userId, newName);
    }

    @Override
    public void modifyUserLogin(long userId, String newLogin) {
        userAccountService.modifyUserLogin(userId, newLogin);
    }

    @Override
    public void modifyUserEmail(long userId, String newEmail) {
        userAccountService.modifyUserEmail(userId, newEmail);
    }

    /* ********************** proper admin section ******************* */

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public AuthenticatedUser findUserById(long userId) {
        User user = userDao.getReferenceById(userId);
        boolean hasAuth = adminAuthentService.userExists(user.getLogin());
        return new AuthenticatedUser(user, hasAuth);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public List<UsersGroup> findAllUsersGroupOrderedByQualifiedName() {
        return groupDao.findAllGroupsOrderedByQualifiedName();
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addUser(User user, long groupId, String password) {
        licenseHelperService.assertLicenseAllowsToAddOrActivateUser();

        // FIXME : check the auth login is available when time has come
        createUserWithoutCredentials(user, groupId);
        adminAuthentService.createNewUserPassword(
                user.getLogin(),
                password,
                user.getActive(),
                true,
                true,
                true,
                new ArrayList<GrantedAuthority>());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void setUserGroupAuthority(long userId, long groupId) {
        UsersGroup group = groupDao.getReferenceById(groupId);
        if (TEST_AUTOMATION_SERVER.equals(group.getQualifiedName())) {
            throw new IllegalUserGroupTransitionException();
        }

        User user = userDao.getReferenceById(userId);
        if (TEST_AUTOMATION_SERVER.equals(user.getGroup().getQualifiedName())) {
            throw new IllegalUserGroupTransitionException();
        }

        user.setGroup(group);
        aclService.refreshAcls();
        expireSessionInformationIfUserIsLogged(user.getLogin());
    }

    /**
     * Expires session information of the {@link User} with the given login if it is currently logged
     * in.
     *
     * @param login login of the {@link User}
     */
    private void expireSessionInformationIfUserIsLogged(String login) {
        org.springframework.security.core.userdetails.UserDetails user =
                sessionRegistry.getAllPrincipals().stream()
                        .map(
                                springUser ->
                                        (org.springframework.security.core.userdetails.UserDetails) springUser)
                        .filter(inspectedUser -> login.equals(inspectedUser.getUsername()))
                        .findAny()
                        .orElse(null);
        if (Objects.nonNull(user)) {
            List<SessionInformation> targetUserSessionInformationList =
                    sessionRegistry.getAllSessions(user, false);
            targetUserSessionInformationList.forEach(SessionInformation::expireNow);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deactivateUser(long userId) {
        User user = userDao.getReferenceById(userId);
        checkHasSynchronisations(user);
        checkActiveUser(user);
        userAccountService.deactivateUser(userId);
        adminAuthentService.deactivateAccount(user.getLogin());
        aclService.refreshAcls();
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void activateUser(long userId) {
        userAccountService.activateUser(userId);
        User user = userDao.getReferenceById(userId);
        adminAuthentService.activateAccount(user.getLogin());
        // TM-547
        aclService.refreshAcls();

        aclService.refreshAcls();
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deactivateUsers(Collection<Long> userIds) {
        for (Long id : userIds) {
            deactivateUser(id);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void activateUsers(Collection<Long> userIds) {
        for (Long id : userIds) {
            activateUser(id);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteUsers(Collection<Long> userIds) {

        checkUsersOwnMilestones(userIds);

        for (Long id : userIds) {
            User user = userDao.getReferenceById(id);
            checkHasSynchronisations(user);
            checkActiveUser(user);
            projectFilterModificationService.removeProjectFiltersByUserLogin(user.getLogin());
            userAccountService.deleteUser(id);
            adminAuthentService.deleteAccount(user.getLogin());
            userDao.delete(user);
        }
        aclService.refreshAcls();
    }

    private void checkUsersOwnMilestones(Collection<Long> userIds) {
        if (milestoneManagerService.hasMilestone(new ArrayList<>(userIds))) {
            throw new MilestoneOwnerDeleteException();
        }
    }

    private void checkHasSynchronisations(User user) {
        if (!remoteSynchronisationDao.findByOwnerId(user.getId()).isEmpty()) {
            throw new UserWithSynchronisationsDeleteException();
        }
    }

    private void checkActiveUser(User user) {
        String activeUserName = UserContextHolder.getUsername();
        if (user.getLogin().equals(activeUserName)) {
            throw new ActiveUserDeleteException();
        }
    }

    @Override
    public List<RemoteSynchronisation> getSynchronisationsByUser(Long userId) {
        return remoteSynchronisationDao.findByOwnerId(userId);
    }

    @Override
    public List<Project> findAllProjects() {
        return projectDao.findAll();
    }

    @Override
    public Map<String, String> findPostLoginInformation() {
        Map<String, String> result = new HashMap<>();
        String expiration = configurationService.findConfiguration(PLUGIN_LICENSE_EXPIRATION);
        String excess = configurationService.findConfiguration(ACTIVATED_USER_EXCESS);

        if (hasInformation(expiration, excess)) {
            UserDto current = userAccountService.findCurrentUserDto();
            if (current.isAdmin()) {
                if (expiration != null && !expiration.isEmpty()) {
                    retrieveInformationDate(result, expiration);
                }
                if (excess != null && !excess.isEmpty()) {
                    retrieveInformationUser(result, excess);
                }
            }
        }
        return result;
    }

    private Map<String, String> retrieveInformationDate(
            Map<String, String> result, String expiration) {
        String messageDate;
        Integer expi = Integer.parseInt(expiration);
        if (expi < 0) {
            messageDate = "warning3";
            result.put("messageDate", messageDate);
            result.put("daysRemaining", expi.toString());
        }
        return result;
    }

    private Map<String, String> retrieveInformationUser(Map<String, String> result, String excess) {
        String[] excesses = excess.split("-");
        if (excesses.length == 3 && !Boolean.parseBoolean(excesses[2])) {
            result.put("messageUser", "warning2");
            result.put("currentUserNb", excesses[0]);
            result.put("maxUserNb", excesses[1]);
        }
        return result;
    }

    private boolean hasInformation(String informationDate, String informationUser) {
        return (informationDate != null && !informationDate.isEmpty())
                || (informationUser != null && !informationUser.isEmpty());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void resetUserPassword(long userId, String newPassword) {
        User user = userDao.getReferenceById(userId);
        adminAuthentService.resetUserPassword(user.getLogin(), newPassword);
    }

    /**
     * @see UserAdministrationService#deassociateTeams(long, List)
     */
    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deassociateTeams(long userId, List<Long> teamIds) {
        User user = userDao.getReferenceById(userId);
        List<Team> teams = teamDao.findAllById(teamIds);
        for (Team team : teams) {
            team.removeMember(user);
        }
        user.removeTeams(teamIds);
        aclService.refreshAcls();
    }

    /**
     * @see UserAdministrationService#associateToTeams(long, List)
     */
    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Set<String> associateToTeams(long userId, List<Long> teamIds) {
        User user = userDao.getReferenceById(userId);
        List<Team> teams = teamDao.findAllById(teamIds);
        Set<String> teamsWithCustomProfiles = removeTeamsWithCustomProfile(teams, teamIds);

        for (Team team : teams) {
            team.addMember(user);
            user.addTeam(team);
        }

        aclService.refreshAcls();

        return teamsWithCustomProfiles;
    }

    private Set<String> removeTeamsWithCustomProfile(List<Team> teams, List<Long> teamIds) {
        if (ultimateLicenseAvailabilityService.isAvailable()) {
            return Collections.emptySet();
        }
        Set<String> teamsWithCustomProfiles = new HashSet<>();
        Map<Long, List<String>> teamsAndProfileNames = profileDao.findProfileNamesByPartyIds(teamIds);

        teamsAndProfileNames.forEach(
                (teamId, profileNames) ->
                        removeTeamWithCustomProfile(teams, teamId, profileNames, teamsWithCustomProfiles));
        return teamsWithCustomProfiles;
    }

    private void removeTeamWithCustomProfile(
            List<Team> teams,
            long teamId,
            List<String> profileNames,
            Set<String> teamsWithCustomProfiles) {
        if (profileNames.stream().anyMatch(profileName -> !AclGroup.isSystem(profileName))) {
            String teamName = findTeamNameById(teams, teamId);
            if (Objects.nonNull(teamName)) {
                teamsWithCustomProfiles.add(teamName);
                teams.removeIf(team -> team.getId().equals(teamId));
            }
        }
    }

    private String findTeamNameById(List<Team> teams, Long teamId) {
        return teams.stream()
                .filter(team -> team.getId().equals(teamId))
                .map(Team::getName)
                .findFirst()
                .orElse(null);
    }

    /**
     * @see UserAdministrationService#findAllNonAssociatedTeams(long)
     */
    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public List<Team> findAllNonAssociatedTeams(long userId) {
        return teamDao.findAllNonAssociatedTeams(userId);
    }

    /**
     * This is not secured on purpose.
     *
     * @see UserAdministrationService#findByLogin(java.lang.String)
     */
    @Override
    public User findByLogin(String login) {
        return userDao.findUserByLogin(login);
    }

    /**
     * @see UserAdministrationService#createUserFromLogin(java.lang.String)
     */
    @Override
    public User createUserFromLogin(@NotNull String login) throws LoginAlreadyExistsException {
        licenseHelperService.assertLicenseAllowsToAddOrActivateUser();

        String loginTrim = login.trim();
        checkLoginAvailability(loginTrim);

        User user = User.createFromLogin(loginTrim);
        UsersGroup defaultGroup = groupDao.findByQualifiedName(UsersGroup.USER);
        user.setGroup(defaultGroup);

        userDao.save(user);
        return user;
    }

    /**
     * @see
     *     UserAdministrationService#createUserWithoutCredentials(org.squashtest.tm.domain.users.User,
     *     long)
     */
    @Override
    public void createUserWithoutCredentials(User user, long groupId) {
        checkLoginAvailability(user.getLogin());

        UsersGroup group = groupDao.getReferenceById(groupId);
        user.setGroup(group);

        userDao.save(user);
    }

    @Override
    public void createUserWithoutCredentials(User user, String usergroupName) {
        checkLoginAvailability(user.getLogin());

        UsersGroup group = groupDao.findByQualifiedName(usergroupName);
        user.setGroup(group);

        userDao.save(user);
    }

    /**
     * @see UserAdministrationService#createAuthentication(long, java.lang.String)
     */
    @Override
    public void createAuthentication(long userId, String password)
            throws LoginAlreadyExistsException {

        if (!adminAuthentService.canModifyUser()) {
            throw new UnauthorizedPasswordChange(
                    "The authentication service do not allow users to change their passwords using Squash");
        }

        User user = userDao.getReferenceById(userId);

        if (!adminAuthentService.userExists(user.getLogin())) {
            UserDetails auth =
                    UserBuilder.forUser(user.getLogin()).password(password).active(user.getActive()).build();
            adminAuthentService.createUser(auth);

        } else {
            throw new LoginAlreadyExistsException(
                    "Authentication data for user '" + user.getLogin() + "' already exists");
        }
    }

    @Override
    public List<MilestonePossibleOwnerDto> findAllAdminOrManager() {
        permissionEvaluationService.checkAtLeastOneMilestoneManagementPermissionOrAdmin();
        return userDao.findAllAdminOrManager();
    }

    @Override
    public void checkLoginAvailability(String login) {
        boolean caseInsensitive = features.isEnabled(Feature.CASE_INSENSITIVE_LOGIN);

        if (caseInsensitive && userDao.findUserByCiLogin(login) != null
                || !caseInsensitive && userDao.findUserByLogin(login) != null) {
            throw new LoginAlreadyExistsException(
                    "User " + login + " cannot be created because it already exists");
        }
    }

    /**
     * @see org.squashtest.tm.service.user.UserManagerService#findAllDuplicateLogins()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> findAllDuplicateLogins() {
        return em.unwrap(Session.class).getNamedQuery("User.findAllDuplicateLogins").list();
    }

    /**
     * @see org.squashtest.tm.service.user.UserManagerService#findCaseAwareLogin(java.lang.String)
     */
    @Override
    public String findCaseAwareLogin(String login) {
        Query query = em.unwrap(Session.class).getNamedQuery("User.findCaseAwareLogin");
        query.setParameter("login", login);
        return (String) query.uniqueResult();
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public int countAllActiveUsers() {
        return userDao.countAllActiveUsers();
    }

    @Override
    public void changeCanDeleteFromFront(List<Long> userIds, boolean canDeleteFromFront) {
        List<User> users = userDao.findAllById(userIds);
        users.forEach(user -> user.setCanDeleteFromFront(canDeleteFromFront));
    }

    @Override
    public void changeCanDeleteFromFront(long userId, boolean canDeleteFromFront) {
        User user = userDao.getReferenceById(userId);
        user.setCanDeleteFromFront(canDeleteFromFront);
    }

    @Override
    public String findLoginByUserId(long userId) {
        return userDao.findUserLoginById(userId);
    }

    @Override
    public String getDefaultAuthenticatedRedirectUrlForUserAuthority() {
        return isUserProgrammerButNotTester() ? AUTOMATION_WORKSPACE_URL : HOME_WORKSPACE_URL;
    }

    private boolean isUserProgrammerButNotTester() {
        DetailedUserDto currentUser = userDisplayService.findCurrentUser();
        return currentUser.isAutomationProgrammer() && !currentUser.isFunctionalTester();
    }
}
