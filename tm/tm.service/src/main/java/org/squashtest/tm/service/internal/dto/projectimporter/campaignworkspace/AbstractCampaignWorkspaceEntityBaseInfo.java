/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.service.internal.dto.projectimporter.AttachmentToImport;

public abstract class AbstractCampaignWorkspaceEntityBaseInfo {

    protected String internalId;
    protected String parentId;
    protected String name;
    protected String description;
    protected Map<Long, RawValue> customFields;
    protected List<String> testPlanTestCaseIds;
    protected List<AttachmentToImport> attachments = new ArrayList<>();

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<Long, RawValue> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<Long, RawValue> customFields) {
        this.customFields = customFields;
    }

    public List<String> getTestPlanTestCaseIds() {
        return testPlanTestCaseIds;
    }

    public void setTestPlanTestCaseIds(List<String> testPlanTestCaseIds) {
        this.testPlanTestCaseIds = testPlanTestCaseIds;
    }

    public List<AttachmentToImport> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentToImport> attachments) {
        this.attachments = attachments;
    }
}
