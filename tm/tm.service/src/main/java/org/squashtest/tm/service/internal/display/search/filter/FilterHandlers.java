/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import java.util.List;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

@Component
public class FilterHandlers {

    private List<FilterHandler> filterHandlers;

    public FilterHandlers(List<FilterHandler> filterHandlers) {
        this.filterHandlers = filterHandlers;
    }

    public boolean isHandledOutOfQueryEngine(GridFilterValue filter) {
        List<FilterHandler> handlers = findHandlers(filter);
        if (handlers.size() > 1) {
            throw new RuntimeException("There can be only one special filter handler for a filter value");
        }
        return handlers.size() == 1;
    }

    private List<FilterHandler> findHandlers(GridFilterValue filter) {
        return this.filterHandlers.stream()
                .filter(filterHandler -> filterHandler.canHandleFilter(filter))
                .toList();
    }

    public void handleFiltersOutOfQueryEngine(
            ExtendedHibernateQuery<?> query, GridRequest gridRequest) {
        gridRequest.getFilterValues().stream()
                .filter(this::isHandledOutOfQueryEngine)
                .forEach(
                        filter -> {
                            List<FilterHandler> handlers = findHandlers(filter);
                            handlers.get(0).handleFilter(query, filter, gridRequest);
                        });
    }
}
