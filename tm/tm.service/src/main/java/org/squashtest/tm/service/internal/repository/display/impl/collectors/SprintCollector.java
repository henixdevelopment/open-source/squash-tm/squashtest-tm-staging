/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.field;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;

import com.google.common.base.Strings;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.requirement.RemoteRequirementPerimeterStatus;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Component
public class SprintCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

    private static final String IS_SYNCHRONIZED = "IS_SYNCHRONIZED";
    private static final String HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ =
            "HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ";

    private static final String IS_DELETED_SPRINT = "IS_DELETED_SPRINT";

    public SprintCollector(
            DSLContext dsl,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            ActiveMilestoneHolder activeMilestoneHolder,
            MilestoneDisplayDao milestoneDisplayDao) {
        super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
    }

    @Override
    public NodeType getHandledEntityType() {
        return NodeType.SPRINT;
    }

    @Override
    protected Map<Long, DataRow> doCollect(List<Long> ids) {
        Map<Long, DataRow> sprints = collectSprints(ids);
        appendHasOutOfPerimeterOrDeletedRemoteReq(sprints);
        return sprints;
    }

    private Map<Long, DataRow> collectSprints(List<Long> ids) {
        return dsl
                .select(
                        // @formatter:off
                        CAMPAIGN_LIBRARY_NODE.CLN_ID,
                        CAMPAIGN_LIBRARY_NODE.NAME.as(NAME_ALIAS),
                        SPRINT.REFERENCE,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS),
                        field(SPRINT.REMOTE_SYNCHRONISATION_ID.isNotNull()).as(IS_SYNCHRONIZED),
                        REMOTE_SYNCHRONISATION.LAST_SYNC_STATUS,
                        SPRINT.REMOTE_SYNCHRONISATION_ID,
                        SPRINT.STATUS,
                        SPRINT.REMOTE_STATE)
                .from(CAMPAIGN_LIBRARY_NODE)
                .innerJoin(SPRINT)
                .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(SPRINT.CLN_ID))
                .leftJoin(REMOTE_SYNCHRONISATION)
                .on(SPRINT.REMOTE_SYNCHRONISATION_ID.eq(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.in(ids))
                .groupBy(
                        CAMPAIGN_LIBRARY_NODE.CLN_ID,
                        CAMPAIGN_LIBRARY_NODE.NAME,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
                        SPRINT.CLN_ID,
                        REMOTE_SYNCHRONISATION.LAST_SYNC_STATUS,
                        SPRINT.REMOTE_SYNCHRONISATION_ID,
                        SPRINT.REFERENCE)
                .fetch()
                .stream()
                // @formatter:on
                .collect(
                        Collectors.toMap(
                                tuple -> tuple.get(CAMPAIGN_LIBRARY_NODE.CLN_ID),
                                tuple -> {
                                    DataRow dataRow = new DataRow();
                                    dataRow.setId(
                                            new NodeReference(NodeType.SPRINT, tuple.get(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                                    .toNodeId());
                                    dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
                                    dataRow.setState(DataRow.State.leaf);
                                    dataRow.setData(tuple.intoMap());

                                    if (!Strings.isNullOrEmpty(tuple.get(SPRINT.REFERENCE, String.class))) {
                                        dataRow
                                                .getData()
                                                .replace(
                                                        NAME_ALIAS,
                                                        tuple.get(SPRINT.REFERENCE)
                                                                + " - "
                                                                + tuple.get(CAMPAIGN_LIBRARY_NODE.NAME));
                                    }

                                    return dataRow;
                                }));
    }

    private void appendHasOutOfPerimeterOrDeletedRemoteReq(Map<Long, DataRow> sprints) {
        sprints.forEach(
                (aLong, dataRow) -> {
                    boolean hasOutOfPerimeterOrDeletedRemoteReq = false;
                    boolean sprintIsDeleted = false;
                    boolean sprintIsSynchronized = (boolean) dataRow.getData().get(IS_SYNCHRONIZED);
                    if (sprintIsSynchronized) {
                        long sprintId =
                                Long.parseLong(dataRow.getId().replace(NodeType.SPRINT.getTypeName() + "-", ""));
                        sprintIsDeleted = checkIfSprintIsDeleted(sprintId);

                        if (!sprintIsDeleted) {
                            List<Long> folderAndSubFolders = findSynchronizedFolderAndSubFolders(sprintId);
                            List<Long> sprintReqVersionInFoldersAndSubFolders =
                                    findSynchronizedSprintReqVersionsInsideFolders(folderAndSubFolders);
                            hasOutOfPerimeterOrDeletedRemoteReq =
                                    checkIfSprintsHaveOutOfPerimeterOrDeletedRemoteReq(
                                            sprintReqVersionInFoldersAndSubFolders);
                        }
                    }
                    dataRow.addData(IS_DELETED_SPRINT, sprintIsDeleted);
                    dataRow.addData(
                            HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ, hasOutOfPerimeterOrDeletedRemoteReq);
                });
    }

    private boolean checkIfSprintIsDeleted(Long sprintId) {
        Integer count =
                dsl.select(count(SPRINT.REMOTE_STATE))
                        .from(SPRINT)
                        .where(SPRINT.CLN_ID.eq(sprintId))
                        .and(SPRINT.REMOTE_STATE.eq(String.valueOf(SprintStatus.DELETED)))
                        .fetchOneInto(int.class);
        return count > 0;
    }

    private List<Long> findSynchronizedFolderAndSubFolders(long rootFolderId) {
        return findSynchronizedSubFolders(rootFolderId);
    }

    private List<Long> findSynchronizedSubFolders(Long folderId) {
        return dsl.select(SPRINT.CLN_ID)
                .from(CLN_RELATIONSHIP_CLOSURE)
                .innerJoin(SPRINT)
                .on(SPRINT.CLN_ID.eq(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .where(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(folderId))
                .fetch(SPRINT.CLN_ID);
    }

    private List<Long> findSynchronizedSprintReqVersionsInsideFolders(List<Long> sprintIds) {
        return dsl.select(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID)
                .from(SPRINT_REQ_VERSION)
                .where(SPRINT_REQ_VERSION.SPRINT_ID.in(sprintIds))
                .fetch(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID);
    }

    private boolean checkIfSprintsHaveOutOfPerimeterOrDeletedRemoteReq(
            List<Long> sprintReqVersionInFoldersAndSubFolders) {
        Integer count =
                dsl.select(count(SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_PERIMETER_STATUS))
                        .from(SPRINT_REQUIREMENT_SYNC_EXTENDER)
                        .where(
                                SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_VERSION_ID.in(
                                        sprintReqVersionInFoldersAndSubFolders))
                        .and(
                                SPRINT_REQUIREMENT_SYNC_EXTENDER.REMOTE_PERIMETER_STATUS.in(
                                        RemoteRequirementPerimeterStatus.OUT_OF_CURRENT_PERIMETER.name(),
                                        RemoteRequirementPerimeterStatus.NOT_FOUND.name()))
                        .fetchOneInto(int.class);
        return count > 0;
    }
}
