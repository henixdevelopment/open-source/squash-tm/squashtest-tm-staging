/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.remote;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.service.bugtracker.BugTrackersLocalService;
import org.squashtest.tm.service.bugtracker.knownissues.local.ExecutionLocalKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.local.FailureDetailLocalKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssueVisitor;
import org.squashtest.tm.service.bugtracker.knownissues.local.RequirementVersionLocalKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.remote.RemoteKnownIssue;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;

/**
 * Process RemoteKnownIssues into a GridResponse
 *
 * @see RemoteKnownIssue
 * @see RemoteKnownIssueFinderImpl
 */
@Service
public class KnownIssuesGridResponseBuilder implements LocalKnownIssueVisitor {

    private final BugTrackersLocalService bugTrackersLocalService;
    private final ExecutionDao executionDao;
    private final RequirementVersionDao requirementVersionDao;

    public KnownIssuesGridResponseBuilder(
            BugTrackersLocalService bugTrackersLocalService,
            ExecutionDao executionDao,
            RequirementVersionDao requirementVersionDao) {
        this.bugTrackersLocalService = bugTrackersLocalService;
        this.executionDao = executionDao;
        this.requirementVersionDao = requirementVersionDao;
    }

    public GridResponse asKnownIssuesGridResponse(
            List<RemoteKnownIssue> knownIssues, GridRequest gridRequest, long totalCount) {
        GridResponse response = new GridResponse();
        response.setCount(totalCount);
        response.setDataRows(knownIssues.stream().map(this::asDataRow).toList());
        response.setPage(GridResponse.getPageNumberWithinBounds(gridRequest, totalCount));
        return response;
    }

    public GridResponse asKnownIssuesGridResponseWithoutPaging(
            List<RemoteKnownIssue> knownIssues, long totalCount) {
        GridResponse response = new GridResponse();
        response.setCount(totalCount);
        response.setDataRows(knownIssues.stream().map(this::asDataRow).toList());
        return response;
    }

    private DataRow asDataRow(RemoteKnownIssue remoteKnownIssue) {
        DataRow dataRow = new DataRow();
        dataRow.setId(generateId(remoteKnownIssue));
        dataRow.setProjectId(remoteKnownIssue.localKnownIssue.projectId);
        dataRow.setData(getRowData(remoteKnownIssue));
        return dataRow;
    }

    private Map<String, Object> getRowData(RemoteKnownIssue remoteKnownIssue) {
        final RemoteIssue issue = remoteKnownIssue.remoteIssue;

        final Map<String, Object> rowData = new HashMap<>();
        rowData.put("remoteId", issue.getId());
        rowData.put("summary", issue.getSummary());
        rowData.put("priority", findPriority(issue));
        rowData.put("status", findStatus(issue));
        rowData.put("assignee", findAssignee(issue));
        rowData.put("remoteKey", issue.getRemoteKey());
        rowData.put("btProject", issue.getProject().getName());
        rowData.put("url", getIssueUrl(remoteKnownIssue));
        rowData.put("reportSites", getReportSites(remoteKnownIssue));
        appendExecutionsInfo(remoteKnownIssue, rowData);

        return rowData;
    }

    private void appendExecutionsInfo(RemoteKnownIssue remoteKnownIssue, Map<String, Object> row) {
        remoteKnownIssue.localKnownIssue.accept(this, row);
    }

    private List<KnownIssueReportSite> getReportSites(RemoteKnownIssue remoteKnownIssue) {
        return remoteKnownIssue.localKnownIssue.executionIds.stream()
                .map(
                        execId -> {
                            final Execution execution =
                                    executionDao
                                            .findById(execId)
                                            .orElseThrow(
                                                    () -> new RuntimeException("Cannot find Execution with id " + execId));
                            return new KnownIssueReportSite(
                                    execId,
                                    execution.getExecutionOrder(),
                                    execution.getName(),
                                    TestCaseExecutionMode.EXPLORATORY.equals(execution.getExecutionMode()),
                                    findSuiteNames(execution));
                        })
                .toList();
    }

    private String getIssueUrl(RemoteKnownIssue remoteKnownIssue) {
        return bugTrackersLocalService
                .getIssueUrl(remoteKnownIssue.remoteIssue.getId(), remoteKnownIssue.bugTracker)
                .toExternalForm();
    }

    @Override
    public void appendRequirementVersionData(
            RequirementVersionLocalKnownIssue localIssue, Map<String, Object> rowData) {
        rowData.put(
                "verifiedRequirementVersions",
                localIssue.requirementVersionIds.stream()
                        .map(
                                id -> {
                                    final RequirementVersion requirementVersion =
                                            requirementVersionDao
                                                    .findById(id)
                                                    .orElseThrow(
                                                            () ->
                                                                    new RuntimeException(
                                                                            "Cannot find requirement version with id " + id));
                                    return new NamedReference(id, requirementVersion.getFullName());
                                })
                        .toList());
    }

    @Override
    public void appendExecutionData(ExecutionLocalKnownIssue issue, Map<String, Object> rowData) {
        rowData.put("executionSteps", issue.stepOrders);
        rowData.put("issueIds", issue.issueIds);
    }

    @Override
    public void appendFailureDetailData(
            FailureDetailLocalKnownIssue issue, Map<String, Object> rowData) {
        rowData.put("issueIds", issue.issueIds);
    }

    private static List<String> findSuiteNames(Execution execution) {
        return Optional.ofNullable(execution.getTestPlan())
                .map(IterationTestPlanItem::getTestSuites)
                .orElseGet(Collections::emptyList)
                .stream()
                .map(TestSuite::getName)
                .toList();
    }

    private static String findAssignee(RemoteIssue issue) {
        return issue.getAssignee() == null ? "" : issue.getAssignee().getName();
    }

    private static String findStatus(RemoteIssue issue) {
        return issue.getStatus() == null ? "" : issue.getStatus().getName();
    }

    private static String findPriority(RemoteIssue issue) {
        return issue.getPriority() == null ? "" : issue.getPriority().getName();
    }

    private String generateId(RemoteKnownIssue remoteKnownIssue) {
        return String.format(
                "%s-%s-%s-%s",
                remoteKnownIssue.localKnownIssue.projectId,
                remoteKnownIssue.bugTracker.getId(),
                remoteKnownIssue.remoteIssue.getRemoteKey(),
                remoteKnownIssue.remoteIssue.getProject().getName());
    }

    /** Describes the execution in which an issue was reported */
    static class KnownIssueReportSite {
        private long executionId;
        private int executionOrder;
        private String executionName;
        private boolean isExploratoryExecution;
        private List<String> suiteNames;

        public KnownIssueReportSite(
                long executionId,
                int executionOrder,
                String executionName,
                boolean isExploratoryExecution,
                List<String> suiteNames) {
            this.executionId = executionId;
            this.executionOrder = executionOrder;
            this.executionName = executionName;
            this.isExploratoryExecution = isExploratoryExecution;
            this.suiteNames = suiteNames;
        }

        public long getExecutionId() {
            return executionId;
        }

        public void setExecutionId(long executionId) {
            this.executionId = executionId;
        }

        public int getExecutionOrder() {
            return executionOrder;
        }

        public void setExecutionOrder(int executionOrder) {
            this.executionOrder = executionOrder;
        }

        public String getExecutionName() {
            return executionName;
        }

        public void setExecutionName(String executionName) {
            this.executionName = executionName;
        }

        public boolean isExploratoryExecution() {
            return isExploratoryExecution;
        }

        public void setExploratoryExecution(boolean exploratoryExecution) {
            isExploratoryExecution = exploratoryExecution;
        }

        public List<String> getSuiteNames() {
            return suiteNames;
        }

        public void setSuiteNames(List<String> suiteNames) {
            this.suiteNames = suiteNames;
        }
    }
}
