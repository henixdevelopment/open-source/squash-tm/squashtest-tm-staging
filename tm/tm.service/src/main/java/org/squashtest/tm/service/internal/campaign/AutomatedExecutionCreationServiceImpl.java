/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanExecution.ITEM_TEST_PLAN_EXECUTION;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolder;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.service.campaign.AutomatedExecutionCreationService;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.denormalizedfield.PrivateDenormalizedFieldValueService;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;

@Service
@Transactional
public class AutomatedExecutionCreationServiceImpl implements AutomatedExecutionCreationService {

    @Inject private PrivateCustomFieldValueService customFieldValuesService;

    @Inject private ExecutionDao executionDao;

    @Inject private PrivateDenormalizedFieldValueService denormalizedFieldValueService;

    @Inject private IterationTestPlanDao iterationTestPlanDao;

    @PersistenceContext private EntityManager entityManager;

    @Inject DSLContext dslContext;

    @Override
    public Map<Long, Long> createAutomatedExecutions(
            AutomatedSuite suite, List<IterationTestPlanItem> itemTestPlan, Project project) {
        List<Execution> createdExecutions = new ArrayList<>();

        Map<Long, Long> itemExecutionMap = new HashMap<>();

        itemTestPlan.forEach(item -> createExecution(item, suite, itemExecutionMap, createdExecutions));

        entityManager.flush();
        entityManager.clear();

        insertBatchIemTestPlanExecutionLink(itemExecutionMap);

        addExternalFields(createdExecutions, project);

        return itemExecutionMap;
    }

    private void insertBatchIemTestPlanExecutionLink(Map<Long, Long> itemExecutionMap) {
        Map<Long, Integer> nextItemExecutionOrderMap =
                iterationTestPlanDao.getNextTestPlanExecutionOrders(itemExecutionMap.keySet());

        List<Query> inserts = new ArrayList<>();

        itemExecutionMap.forEach(
                (itemId, executionId) -> {
                    Integer executionOrder = nextItemExecutionOrderMap.get(itemId);
                    if (executionOrder != null) {
                        inserts.add(
                                dslContext
                                        .insertInto(ITEM_TEST_PLAN_EXECUTION)
                                        .set(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID, itemId)
                                        .set(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID, executionId)
                                        .set(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER, executionOrder));
                    }
                });

        dslContext.batch(inserts).execute();
    }

    private void createExecution(
            IterationTestPlanItem item,
            AutomatedSuite suite,
            Map<Long, Long> itemExecutionMap,
            List<Execution> createdExecutions) {

        Execution execution = item.createExecution(null, null);
        AutomatedExecutionExtender extender = new AutomatedExecutionExtender();
        extender.setExecution(execution);
        execution.setAutomatedExecutionExtender(extender);
        executionDao.save(execution);
        item.notifyAutomatedExecutionAddition(execution);
        suite.addExtender(execution.getAutomatedExecutionExtender());
        createdExecutions.add(execution);
        itemExecutionMap.put(item.getId(), execution.getId());
    }

    private void createCustomFieldsForExecutionAndExecutionSteps(
            List<Execution> executions, Project project) {
        customFieldValuesService.createAllCustomFieldValues(executions, project);

        List<ExecutionStep> steps =
                executions.stream().flatMap(exec -> exec.getSteps().stream()).toList();

        customFieldValuesService.createAllCustomFieldValues(steps, project);
    }

    private void createDenormalizedFieldsForExecutionAndExecutionSteps(List<Execution> executions) {
        Map<Long, List<DenormalizedFieldHolder>> map =
                executions.stream()
                        .map(DenormalizedFieldHolder.class::cast)
                        .collect(
                                Collectors.groupingBy(
                                        holder -> ((Execution) holder).getReferencedTestCase().getId()));

        denormalizedFieldValueService.createBatchDenormalizedFieldValues(map, BindableEntity.TEST_CASE);
        denormalizedFieldValueService.createAllDenormalizedFieldValuesForSteps(executions);
    }

    private void addExternalFields(List<Execution> createdExecutions, Project project) {
        createCustomFieldsForExecutionAndExecutionSteps(createdExecutions, project);
        createDenormalizedFieldsForExecutionAndExecutionSteps(createdExecutions);

        entityManager.flush();
        entityManager.clear();
    }
}
