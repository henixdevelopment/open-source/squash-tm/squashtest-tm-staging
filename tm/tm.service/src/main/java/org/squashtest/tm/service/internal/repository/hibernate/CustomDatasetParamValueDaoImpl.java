/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.DATASET_PARAM_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.PARAMETER;

import java.util.List;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.repository.CustomDatasetParamValueDao;

@Repository
public class CustomDatasetParamValueDaoImpl implements CustomDatasetParamValueDao {

    private final DSLContext dsl;

    public CustomDatasetParamValueDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    public List<Long> findOrphanDatasetParamValuesByTestCaseCallerId(Long testCaseCallerId) {
        return dsl.selectDistinct(DATASET_PARAM_VALUE.DATASET_PARAM_VALUE_ID)
                .from(DATASET_PARAM_VALUE)
                .join(DATASET)
                .on(DATASET_PARAM_VALUE.DATASET_ID.eq(DATASET.DATASET_ID))
                .join(PARAMETER)
                .on(DATASET_PARAM_VALUE.PARAM_ID.eq(PARAMETER.PARAM_ID))
                .leftJoin(CALL_TEST_STEP)
                .on(PARAMETER.TEST_CASE_ID.eq(CALL_TEST_STEP.CALLED_TEST_CASE_ID))
                .where(DATASET.TEST_CASE_ID.ne(PARAMETER.TEST_CASE_ID))
                .and(
                        CALL_TEST_STEP
                                .DELEGATE_PARAMETER_VALUES
                                .isFalse()
                                .or(CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES.isNull()))
                .and(DATASET.TEST_CASE_ID.eq(testCaseCallerId))
                .fetchInto(Long.class);
    }
}
