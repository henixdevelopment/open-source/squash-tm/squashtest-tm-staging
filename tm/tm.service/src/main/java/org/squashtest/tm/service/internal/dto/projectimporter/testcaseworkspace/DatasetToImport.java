/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.service.testcase.ParameterFinder;

public class DatasetToImport {
    private String internalId;
    private String name;
    private List<DatasetParamValueToImport> paramValues = new ArrayList<>();

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DatasetParamValueToImport> getParamValues() {
        return paramValues;
    }

    public void setParamValues(List<DatasetParamValueToImport> paramValues) {
        this.paramValues = paramValues;
    }

    public Dataset toDataset(ParameterFinder parameterFinder, Map<String, Long> datasetParamIdsMap) {
        Dataset dataset = new Dataset();
        dataset.setName(name);
        List<Long> squashParamIds =
                paramValues.stream()
                        .map(value -> datasetParamIdsMap.get(value.getInternalParamId()))
                        .toList();
        List<Parameter> parameters = parameterFinder.findByIds(squashParamIds);

        for (DatasetParamValueToImport value : paramValues) {
            Long squashParamId = datasetParamIdsMap.get(value.getInternalParamId());
            Parameter parameter =
                    parameters.stream()
                            .filter(p -> p.getId().equals(squashParamId))
                            .findFirst()
                            .orElseThrow();
            new DatasetParamValue(parameter, dataset, value.getValue());
        }
        return dataset;
    }
}
