/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.column.requirement;

import static org.squashtest.tm.service.internal.batchimport.column.ColumnProcessingMode.MANDATORY;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.LINKED_LOW_LEVEL_REQS_SHEET;

import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.service.internal.batchimport.column.ColumnProcessingMode;
import org.squashtest.tm.service.internal.batchimport.column.TemplateColumn;
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet;

public enum LinkedLowLevelRequirementsSheetColumn implements TemplateColumn {
    HIGH_LEVEL_REQ_PATH,
    STANDARD_REQ_PATH;

    @Override
    public String getHeader() {
        return name();
    }

    @Override
    public TemplateWorksheet getWorksheet() {
        return LINKED_LOW_LEVEL_REQS_SHEET;
    }

    @Override
    public String getFullName() {
        return StringUtils.join(".", Arrays.asList(getWorksheet().sheetName, getHeader()));
    }

    @Override
    public ColumnProcessingMode getProcessingMode() {
        return MANDATORY;
    }
}
