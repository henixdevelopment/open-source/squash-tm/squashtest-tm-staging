/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.spi;

import java.util.Arrays;
import org.squashtest.csp.core.bugtracker.core.AuthenticationFieldsTranslationKeys;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException;
import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerCacheInfo;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerInterfaceDescriptor;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;

/**
 * Base interface for all three kinds of connector (namely {@link BugTrackerConnector}, {@link
 * AdvancedBugTrackerConnector} and {@link OslcBugTrackerConnector}).
 *
 * <p>The purpose of this interface is mainly to factor in one place the common features of these
 * connectors rather than for class design concerns. This is sort of a bad design but the
 * alternative would be code copy pasta in several places.
 */
public interface BugtrackerConnectorBase {

    /**
     * Declares which authentication protocols are supported by this BugTrackerConnector. Default
     * implementation returns [{@link AuthenticationProtocol#BASIC_AUTH}]
     *
     * @return
     */
    default AuthenticationProtocol[] getSupportedAuthProtocols() {
        return new AuthenticationProtocol[] {AuthenticationProtocol.BASIC_AUTH};
    }

    /**
     * Declares whether the given connector supports a given connection protocol.
     *
     * @param mode
     */
    default boolean supports(AuthenticationProtocol mode) {
        return Arrays.asList(getSupportedAuthProtocols()).contains(mode);
    }

    /**
     * Authenticates to the bug tracker with the given credentials. If authentication does not fail,
     * it should not be required again at least for the current thread.
     *
     * @param credentials the credentials
     * @throw UnsupportedAuthenticationModeException if the connector cannot use the given credentials
     */
    void authenticate(Credentials credentials) throws UnsupportedAuthenticationModeException;

    /**
     * will check if the current credentials are actually acknowledged by the bugtracker
     *
     * @param credentials
     * @return nothing
     * @throw UnsupportedAuthenticationModeException if the connector cannot use the given credentials
     * @throw {@link BugTrackerNoCredentialsException} if the credentials are invalid
     * @throw {@link BugTrackerRemoteException} for other network exceptions.
     */
    void checkCredentials(Credentials credentials)
            throws BugTrackerNoCredentialsException, BugTrackerRemoteException;

    default boolean areCredentialsValid(Credentials credentials) {
        // TODO : it would be better to do it the other way around: bugtrackers implement this method
        // and checkCredentials throws an exception if areCredentialsValid returns false
        try {
            checkCredentials(credentials);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Returns an {@link BugTrackerInterfaceDescriptor}
     *
     * @return
     */
    BugTrackerInterfaceDescriptor getInterfaceDescriptor();

    default String getKeyForLogin(BugTracker bugTracker) {
        return AuthenticationFieldsTranslationKeys.LOGIN.key;
    }

    default String getKeyForPassword(BugTracker bugTracker) {
        if (AuthenticationProtocol.BASIC_AUTH.equals(bugTracker.getAuthenticationProtocol())) {
            return AuthenticationFieldsTranslationKeys.PASSWORD.key;
        } else {
            return AuthenticationFieldsTranslationKeys.TOKEN.key;
        }
    }

    default boolean allowsReportingCache() {
        return false;
    }

    default void refreshCache(BugTracker bugTracker) {
        if (allowsReportingCache()) {
            // If your connector supports cache, you should implement this method
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    default void refreshCache(BugTracker bugTracker, long projectId) {
        if (allowsReportingCache()) {
            // If your connector supports cache, you should implement this method
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    default BugTrackerCacheInfo getCacheInfo() {
        if (allowsReportingCache()) {
            // If your connector supports cache, you should implement this method
            throw new UnsupportedOperationException("Not implemented");
        }

        return null;
    }
}
