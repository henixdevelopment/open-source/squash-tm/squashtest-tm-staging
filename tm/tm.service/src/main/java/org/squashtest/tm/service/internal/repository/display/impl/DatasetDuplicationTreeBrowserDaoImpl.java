/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;

import com.google.common.collect.ListMultimap;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.service.internal.repository.ParameterDao;
import org.squashtest.tm.service.internal.repository.display.DatasetDuplicationTreeBrowserDao;
import org.squashtest.tm.service.internal.repository.display.MultipleHierarchyTreeBrowserDao;
import org.squashtest.tm.service.internal.testcase.ParameterComparator;

@Repository
public class DatasetDuplicationTreeBrowserDaoImpl implements DatasetDuplicationTreeBrowserDao {

    private final ParameterDao parameterDao;
    private final DSLContext dsl;

    private final MultipleHierarchyTreeBrowserDao multipleHierarchyTreeBrowserDao;

    public DatasetDuplicationTreeBrowserDaoImpl(
            ParameterDao parameterDao,
            DSLContext dsl,
            MultipleHierarchyTreeBrowserDao multipleHierarchyTreeBrowserDao) {
        this.parameterDao = parameterDao;
        this.dsl = dsl;
        this.multipleHierarchyTreeBrowserDao = multipleHierarchyTreeBrowserDao;
    }

    @Override
    public Set<NodeReference> findLibraryReferences(
            NodeWorkspace workspace, Collection<Long> projectIds) {
        return multipleHierarchyTreeBrowserDao.findLibraryReferences(workspace, projectIds);
    }

    @Override
    public ListMultimap<NodeReference, NodeReference> findChildrenReference(
            Set<NodeReference> parentReferences) {
        return multipleHierarchyTreeBrowserDao.findChildrenReference(parentReferences);
    }

    @Override
    public Set<NodeReference> findAncestors(NodeReferences nodeReferences) {
        return multipleHierarchyTreeBrowserDao.findAncestors(nodeReferences);
    }

    @Override
    public Set<NodeReference> getEligibleTestCaseNodeReferences(
            Set<NodeReference> nodesToFilter, List<String> sourceDataSetParameters) {

        Set<Long> testCaseIds =
                nodesToFilter.stream()
                        .filter(node -> NodeType.TEST_CASE.equals(node.getNodeType()))
                        .map(NodeReference::getId)
                        .collect(Collectors.toSet());

        List<Long> tclnIds = filterEligibleTclnIds(testCaseIds, sourceDataSetParameters);

        return nodesToFilter.stream()
                .filter(
                        nodeReference ->
                                !NodeType.TEST_CASE.equals(nodeReference.getNodeType())
                                        || tclnIds.contains(nodeReference.getId()))
                .collect(Collectors.toSet());
    }

    public List<Long> filterEligibleTclnIds(
            Collection<Long> testCaseIds, List<String> sourceParameters) {
        if (testCaseIds.isEmpty()) {
            return Collections.emptyList();
        }

        List<Long> validTestCaseIds =
                dsl.select(TEST_CASE.TCLN_ID)
                        .from(TEST_CASE)
                        .leftJoin(SCRIPTED_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                        .leftJoin(EXPLORATORY_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                        .leftJoin(MILESTONE_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
                        .leftJoin(MILESTONE)
                        .on(
                                MILESTONE_TEST_CASE
                                        .MILESTONE_ID
                                        .eq(MILESTONE.MILESTONE_ID)
                                        .and(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name())))
                        .where(
                                TEST_CASE
                                        .TCLN_ID
                                        .in(testCaseIds)
                                        .and(SCRIPTED_TEST_CASE.TCLN_ID.isNull())
                                        .and(EXPLORATORY_TEST_CASE.TCLN_ID.isNull())
                                        .and(MILESTONE.MILESTONE_ID.isNull()))
                        .fetchInto(Long.class);

        if (validTestCaseIds.isEmpty()) {
            return validTestCaseIds;
        }

        Map<Long, List<String>> parameterNamesByTestCaseId =
                parameterDao.findOwnParametersNamesByTestCaseIds(testCaseIds);

        return validTestCaseIds.stream()
                .filter(
                        id ->
                                ParameterComparator.checkHasMatchingParameters(
                                        sourceParameters,
                                        parameterNamesByTestCaseId.getOrDefault(id, Collections.emptyList())))
                .toList();
    }
}
