/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to represent a grid request. Equivalent of spring Pageable + Filters + Group
 * Columns...
 */
public class GridRequest {
    private Integer page = 0;
    private Integer size = 0;

    private boolean searchOnMultiColumns;

    private List<String> simplifiedColumnDisplayGridIds;

    private String gridId;

    private List<GridSort> sort = new ArrayList<>();
    private List<GridFilterValue> filterValues = new ArrayList<>();
    // scope is a list of entity references represented as String.
    // aka Project-1, TestCaseFolder-4...
    private List<String> scope = new ArrayList<>();
    // LMU: some entity references need to be handled differently from the rest of the ones in scope.
    // So, we set them in excluded scope to store the information for when/if it is needed downstream
    // (i.e. campaign search with Test Suites)
    private List<String> excludedScope = new ArrayList<>();
    private boolean extendedHighLvlReqScope = true;

    public String getGridId() {
        return gridId;
    }

    public void setGridId(String gridId) {
        this.gridId = gridId;
    }

    public List<GridSort> getSort() {
        return sort;
    }

    public void setSort(List<GridSort> sort) {
        this.sort = sort;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<GridFilterValue> getFilterValues() {
        return filterValues;
    }

    public void setFilterValues(List<GridFilterValue> filterValues) {
        this.filterValues = filterValues;
    }

    public List<String> getScope() {
        return scope;
    }

    public void setScope(List<String> scope) {
        this.scope = scope;
    }

    public boolean isSearchOnMultiColumns() {
        return searchOnMultiColumns;
    }

    public void setSearchOnMultiColumns(boolean searchOnMultiColumns) {
        this.searchOnMultiColumns = searchOnMultiColumns;
    }

    public boolean isExtendedHighLvlReqScope() {
        return extendedHighLvlReqScope;
    }

    public void setExtendedHighLvlReqScope(boolean extendedHighLvlReqScope) {
        this.extendedHighLvlReqScope = extendedHighLvlReqScope;
    }

    public List<String> getSimplifiedColumnDisplayGridIds() {
        return simplifiedColumnDisplayGridIds;
    }

    public void setSimplifiedColumnDisplayGridIds(List<String> simplifiedColumnDisplayGridIds) {
        this.simplifiedColumnDisplayGridIds = simplifiedColumnDisplayGridIds;
    }

    public List<String> getExcludedScope() {
        return excludedScope;
    }

    public void setExcludedScope(List<String> excludedScope) {
        this.excludedScope = excludedScope;
    }

    public GridRequest toNonPaginatedRequest() {
        GridRequest gridRequest = new GridRequest();
        gridRequest.setSort(this.getSort());
        gridRequest.setFilterValues(this.getFilterValues());
        gridRequest.setSearchOnMultiColumns(this.isSearchOnMultiColumns());
        gridRequest.setScope(this.getScope());
        gridRequest.setExtendedHighLvlReqScope(this.isExtendedHighLvlReqScope());
        gridRequest.setPage(null);
        gridRequest.setSize(null);
        return gridRequest;
    }

    public GridRequest copy() {
        GridRequest gridRequest = new GridRequest();
        gridRequest.setSort(this.getSort());
        gridRequest.setFilterValues(this.getFilterValues());
        gridRequest.setScope(this.getScope());
        gridRequest.setExtendedHighLvlReqScope(this.isExtendedHighLvlReqScope());
        gridRequest.setPage(this.getPage());
        gridRequest.setSize(this.getSize());
        gridRequest.setSearchOnMultiColumns(this.isSearchOnMultiColumns());
        return gridRequest;
    }
}
