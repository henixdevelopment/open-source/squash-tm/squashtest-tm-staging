/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution;

import static org.squashtest.tm.jooq.domain.tables.ExploratorySessionOverview.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanList.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ConsumerForExploratoryExecution;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.exception.requirement.MilestoneForbidModificationException;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.campaign.SprintExecutionCreationService;
import org.squashtest.tm.service.campaign.SprintTestPlanItemManagerService;
import org.squashtest.tm.service.execution.ExploratorySessionOverviewModificationService;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.security.UserContextService;

@Service("squashtest.tm.service.ExploratorySessionOverviewModificationService")
@Transactional
public class ExploratorySessionOverviewModificationServiceImpl
        implements ExploratorySessionOverviewModificationService {

    private static final String EXECUTE_OVERVIEW_OR_ADMIN =
            "hasPermission(#overviewId, 'org.squashtest.tm.domain.campaign.ExploratorySessionOverview' ,'EXECUTE') "
                    + OR_HAS_ROLE_ADMIN;

    private final DSLContext dslContext;
    private final IterationModificationService iterationModificationService;
    private final IterationTestPlanManagerService iterationTestPlanManagerService;
    private final MilestoneDao milestoneDao;
    private final UserDao userDao;
    private final UserContextService userContextService;
    private final SprintExecutionCreationService sprintExecutionCreationService;
    private final SprintTestPlanItemManagerService sprintTestPlanItemManagerService;
    private final SprintDisplayDao sprintDisplayDao;
    private final CustomTestSuiteModificationService customTestSuiteModificationService;

    @PersistenceContext private EntityManager entityManager;

    public ExploratorySessionOverviewModificationServiceImpl(
            DSLContext dslContext,
            IterationModificationService iterationModificationService,
            IterationTestPlanManagerService iterationTestPlanManagerService,
            MilestoneDao milestoneDao,
            UserDao userDao,
            UserContextService userContextService,
            SprintExecutionCreationService sprintExecutionCreationService,
            SprintTestPlanItemManagerService sprintTestPlanItemManagerService,
            SprintDisplayDao sprintDisplayDao,
            CustomTestSuiteModificationService customTestSuiteModificationService) {
        this.dslContext = dslContext;
        this.iterationModificationService = iterationModificationService;
        this.iterationTestPlanManagerService = iterationTestPlanManagerService;
        this.milestoneDao = milestoneDao;
        this.userDao = userDao;
        this.userContextService = userContextService;
        this.sprintExecutionCreationService = sprintExecutionCreationService;
        this.sprintTestPlanItemManagerService = sprintTestPlanItemManagerService;
        this.sprintDisplayDao = sprintDisplayDao;
        this.customTestSuiteModificationService = customTestSuiteModificationService;
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void updateDueDate(Long overviewId, Date dueDate) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);
        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);
        exploratorySessionOverview.updateDueDate(dueDate);
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void updateExecutionStatus(Long overviewId, String executionStatus) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);
        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);
        IterationTestPlanItem iterationTestPlanItem =
                exploratorySessionOverview.getIterationTestPlanItem();

        if (iterationTestPlanItem != null) {
            iterationTestPlanItem.setExecutionStatus(ExecutionStatus.valueOf(executionStatus));
            iterationTestPlanItem.setLastExecutedBy(userContextService.getUsername());
            iterationTestPlanItem.setLastExecutedOn(new Date());
            iterationTestPlanItem.updateLastModificationWithCurrentUser();
            List<TestSuite> testSuitesToUpdate = iterationTestPlanItem.getTestSuites();
            customTestSuiteModificationService.updateExecutionStatus(testSuitesToUpdate);
        } else {
            final User user = getCurrentUser();
            exploratorySessionOverview
                    .getTestPlanItem()
                    .applyFastPass(ExecutionStatus.valueOf(executionStatus), user);
        }
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void updateSessionDuration(Long overviewId, Integer sessionDuration) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);
        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);
        exploratorySessionOverview.updateSessionDuration(sessionDuration);
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public ExploratoryExecution addNewExecution(Long overviewId) {
        return addNewExecutionUnsecured(overviewId);
    }

    private ExploratoryExecution addNewExecutionUnsecured(Long overviewId) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);

        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);

        final IterationTestPlanItem iterationTestPlanItem =
                exploratorySessionOverview.getIterationTestPlanItem();

        Execution execution;

        if (iterationTestPlanItem != null) {
            execution = iterationModificationService.addManualExecution(iterationTestPlanItem.getId());
            iterationTestPlanItem.updateLastModificationWithCurrentUser();
            updateExploratoryExecutionInfoFromSessionOverview(execution, exploratorySessionOverview);
        } else {
            execution =
                    sprintExecutionCreationService.createExecution(
                            exploratorySessionOverview.getTestPlanItem().getId());
        }

        final AtomicReference<ExploratoryExecution> exploratoryExecution = new AtomicReference<>();
        final ConsumerForExploratoryExecution consumer =
                new ConsumerForExploratoryExecution(exploratoryExecution::set);
        execution.accept(consumer);
        return exploratoryExecution.get();
    }

    // TODO: this is duplicated in SprintExecutionCreationService. This should be removed after
    // refactoring
    private void updateExploratoryExecutionInfoFromSessionOverview(
            Execution execution, ExploratorySessionOverview exploratorySessionOverview) {
        execution.setName(buildExecutionName(exploratorySessionOverview));
        execution.setReference(exploratorySessionOverview.getReference());
    }

    private String buildExecutionName(ExploratorySessionOverview exploratorySessionOverview) {
        if (exploratorySessionOverview.getReference().isEmpty()) {
            return exploratorySessionOverview.getName();
        } else {
            return exploratorySessionOverview.getReference()
                    + " - "
                    + exploratorySessionOverview.getName();
        }
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void startSessionAndUpdateExecutionStatus(Long overviewId) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);
        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);
        exploratorySessionOverview.startSession(getCurrentUser());
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void endSession(Long overviewId) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);
        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);
        exploratorySessionOverview.endSession();
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void updateComments(Long overviewId, String comments) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);
        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);
        exploratorySessionOverview.updateComments(comments);
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void deleteExecutions(Long overviewId, List<Long> executionIds) {
        ExploratorySessionOverview exploratorySessionOverview =
                entityManager.find(ExploratorySessionOverview.class, overviewId);
        checkBlockingMilestoneOrClosedSprint(exploratorySessionOverview);

        final Long iterationId = findIterationId(overviewId);

        if (iterationId != null) {
            iterationTestPlanManagerService.removeExecutionsFromTestPlanItem(executionIds, iterationId);
            exploratorySessionOverview.getIterationTestPlanItem().updateLastModificationWithCurrentUser();
        } else {
            final Long testPlanItemId = exploratorySessionOverview.getTestPlanItem().getId();
            sprintTestPlanItemManagerService.deleteExecutionsAndRemoveFromTestPlanItem(
                    executionIds, testPlanItemId);
        }
    }

    @Override
    @PreAuthorize(EXECUTE_OVERVIEW_OR_ADMIN)
    public void addExecutionsWithUsers(Long overviewId, List<Long> userIds) {
        List<User> users = userDao.findAllById(userIds);
        users.forEach(user -> addNewExecutionWithAssignee(overviewId, user));
    }

    private void addNewExecutionWithAssignee(Long overviewId, User user) {
        ExploratoryExecution exploratoryExecution = addNewExecutionUnsecured(overviewId);
        exploratoryExecution.setAssigneeUser(user);
    }

    private void checkBlockingMilestoneOrClosedSprint(ExploratorySessionOverview overview) {
        final IterationTestPlanItem item = overview.getIterationTestPlanItem();

        if (item != null
                && milestoneDao.isIterationTestPlanItemBoundToBlockingMilestone(item.getId())) {
            throw new MilestoneForbidModificationException(
                    "This test plan item is linked to a blocking milestone and cannot be modified.");
        }

        if (SprintStatus.CLOSED.equals(
                sprintDisplayDao.getSprintStatusBySessionOverviewId(overview.getId()))) {
            throw new SprintClosedException();
        }
    }

    private Long findIterationId(Long sessionOverviewId) {
        return dslContext
                .select(ITEM_TEST_PLAN_LIST.ITERATION_ID)
                .from(ITEM_TEST_PLAN_LIST)
                .join(EXPLORATORY_SESSION_OVERVIEW)
                .on(
                        EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(sessionOverviewId))
                .fetchOneInto(Long.class);
    }

    private User getCurrentUser() {
        final String username = userContextService.getUsername();
        return userDao.findUserByLogin(username);
    }
}
