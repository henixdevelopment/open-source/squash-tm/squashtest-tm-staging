/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.campaign.export.CampaignExportCSVModel;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.library.NewFolderDto;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.deletion.CampaignLibraryNodesToDelete;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.library.LibraryNavigationService;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;

public interface CampaignLibraryNavigationService
        extends LibraryNavigationService<CampaignLibrary, CampaignFolder, CampaignLibraryNode>,
                CampaignLibraryFinderService {

    String DESTINATION_ID = "destinationId";
    String TARGET_ID = "targetId";

    /**
     * Adds a Campaign to the root of the Library, and its initial custom field values. The initial
     * custom field values are passed as a Map<Long, String>, that maps the id of the {@link
     * CustomField} to the values of the corresponding {@link CustomFieldValue}. Read that last
     * sentence again.
     *
     * @param libraryId
     * @param campaign
     * @param customFieldValues
     */
    Campaign addCampaignToCampaignLibrary(
            @Id long libraryId, Campaign campaign, Map<Long, RawValue> customFieldValues);

    Campaign addCampaignToCampaignLibraryUnsecured(
            long libraryId, Campaign campaign, Map<Long, RawValue> customFieldValues);

    void addSprintToCampaignLibrary(@Id long libraryId, Sprint sprint);

    void addSprintToCampaignLibrary(
            @Id long libraryId, Sprint sprint, Map<Long, RawValue> customFieldValues);

    SprintGroup addSprintGroupToCampaignLibrary(@Id long libraryId, SprintGroup sprintGroup);

    void addSprintGroupToCampaignLibrary(
            @Id long libraryId, SprintGroup sprintGroup, Map<Long, RawValue> customFieldValues);

    /**
     * Adds a campaign to a folder, and its initial custom field values. The initial custom field
     * values are passed as a Map<Long, String>, that maps the id of the {@link CustomField} to the
     * values of the corresponding {@link CustomFieldValue}. Read that last sentence again.
     *
     * @param libraryId
     * @param campaign
     * @param customFieldValues
     */
    Campaign addCampaignToCampaignFolder(
            @Id long folderId, Campaign campaign, Map<Long, RawValue> customFieldValues);

    Campaign addCampaignToCampaignFolderUnsecured(
            long folderId, Campaign campaign, Map<Long, RawValue> customFieldValues);

    void addSprintToCampaignFolder(@Id long folderId, Sprint sprint);

    void addSprintToCampaignFolder(
            @Id long folderId, Sprint sprint, Map<Long, RawValue> customFieldValues);

    SprintGroup addSprintGroupToCampaignFolder(@Id long folderId, SprintGroup sprintGroup);

    SprintGroup addSprintGroupToCampaignFolder(
            @Id long folderId, SprintGroup sprintGroup, Map<Long, RawValue> customFieldValues);

    void addFolderToSprintGroup(@Id long destinationId, CampaignFolder newFolder);

    CampaignFolder addFolderToSprintGroup(
            @Id long destinationId, CampaignFolder newFolder, Map<Long, RawValue> customFields);

    CampaignFolder addFolderToSprintGroup(@Id long destinationId, NewFolderDto folderDto);

    void addSprintToSprintGroup(@Id long sprintGroupId, Sprint sprint);

    void addSprintToSprintGroup(
            @Id long sprintGroupId, Sprint sprint, Map<Long, RawValue> customFieldValues);

    /**
     * Create a hierarchy of Campaign library node. The type of node depends of the path and existing
     * node(s) in hierarchy :
     *
     * <ul>
     *   <li>The last node will always be a {@link SprintGroup}
     *   <li>All its parents (if any) will be {@link CampaignFolder}
     *   <li>If the beginning of the path already exists as {@link CampaignLibraryNode}, create only
     *       parts that are missing (if any)
     * </ul>
     *
     * @param folderPath the complete path:
     *     <ul>
     *       <li>(eg1: <code>/Project/Folder1/Folder2/SprintGroup</code>)
     *       <li>(eg2: <code>/Project/SprintGroup</code>)
     *     </ul>
     *
     * @return The ID of the created node. Thus, it will always be the id of a {@link SprintGroup}.
     * @throws IllegalArgumentException
     *     <ul>
     *       <li>if folderPath is too short
     *       <li>if the project from the folderPath does not exist
     *     </ul>
     */
    Long createSprintGroupAndFoldersHierarchy(String folderPath);

    void moveIterationsWithinCampaign(
            @Id(DESTINATION_ID) long destinationId, Long[] nodeIds, int position);

    void copyIterationsToCampaign(
            @Id long campaignId, Long[] iterationsIds, ClipboardPayload clipboardPayload);

    /**
     * that method should investigate the consequences of the deletion request of iterations, and
     * return a report about what will happen.
     *
     * @param targetIds
     * @return
     */
    List<SuppressionPreviewReport> simulateIterationDeletion(List<Long> targetIds);

    /**
     * that method should delete the iterations. It still takes care of non deletable iterations so
     * the implementation should filter out the ids who can't be deleted.
     *
     * @param targetIds
     * @return
     */
    @UsedInPlugin("rest-api")
    OperationReport deleteIterations(@Ids List<Long> targetIds);

    /**
     * that method should investigate the consequences of the deletion request of tes suites, and
     * return a report about what will happen.
     *
     * @param targetIds
     * @return
     */
    List<SuppressionPreviewReport> simulateSuiteDeletion(List<Long> targetIds);

    /**
     * that method should delete test suites, and remove its references in iteration and iteration
     * test plan item
     *
     * @param removeFromIter
     * @param testSuites
     * @return
     */
    OperationReport deleteSuites(@Ids List<Long> suiteIds, boolean removeFromIter);

    /**
     * given a campaign Id, returns a model. It's made of rows and cell, and have a row header, check
     * the relevant methods. Note that the actual model will differ according to the export type : "L"
     * (light), "S" (standard), "F" (full).
     *
     * @param campaignId
     * @return
     */
    CampaignExportCSVModel exportCampaignToCSV(Long campaignId, String exportType);

    TestSuite addTestSuiteToIteration(
            @Id Long iterationId, TestSuite suite, Map<Long, RawValue> customFieldValues);

    TestSuite addTestSuiteToIterationUnsecured(
            @Id Long iterationId, TestSuite suite, Map<Long, RawValue> customFieldValues);

    OperationReport deleteNodes(CampaignLibraryNodesToDelete deletionNodes, boolean iter);

    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    void validatePathForSync(String projectName, String synchronisationPath);

    StatisticsBundle compileStatisticsFromSelection(
            NodeReferences nodeReferences,
            List<EntityReference> entityReferences,
            boolean lastExecutionScope);

    // ####################### STATISTICS BUNDLES ############################

    StatisticsBundle gatherLibraryStatisticsBundle(long treeNodeId, boolean lastExecutionScope);

    StatisticsBundle gatherCampaignStatisticsBundle(long campaignId, boolean lastExecutionScope);

    StatisticsBundle gatherFolderStatisticsBundle(Long folderId, boolean lastExecutionScope);

    // ####################### PREVENT CONCURRENCY OVERRIDES ############################

    @Override
    void copyNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids("sourceNodesIds") Long[] sourceNodesIds,
            ClipboardPayload clipboardPayload);

    @Override
    void copyNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload);

    void copyNodesToSprintGroup(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetIds,
            ClipboardPayload clipboardPayload);

    @Override
    void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload);

    @Override
    void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            int position,
            ClipboardPayload clipboardPayload);

    @Override
    void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload);

    @Override
    void moveNodesToLibrary(@Id(DESTINATION_ID) long destinationId, @Ids(TARGET_ID) Long[] targetId);

    @Override
    void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            int position,
            ClipboardPayload clipboardPayload);

    @Override
    void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId, @Ids(TARGET_ID) Long[] targetId, int position);

    void moveNodesToSprintGroup(
            Long destinationId, Long[] movedNodeIds, ClipboardPayload clipboardPayload);

    @Override
    OperationReport deleteNodes(@Ids("targetIds") List<Long> targetIds);

    // ###################### /PREVENT CONCURRENCY OVERIDES ############################

}
