/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.exploratory;

import static org.squashtest.tm.service.security.Authorizations.WRITE_TC_OR_ROLE_ADMIN;

import javax.inject.Inject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.testcase.ExploratoryTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.internal.repository.ExploratoryTestCaseDao;
import org.squashtest.tm.service.testcase.exploratory.ExploratoryTestCaseService;

@Service
@Transactional
public class ExploratoryTestCaseServiceImpl implements ExploratoryTestCaseService {

    @Inject private ExploratoryTestCaseDao exploratoryTestCaseDao;

    @Override
    @PreAuthorize(WRITE_TC_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = TestCase.class)
    public void updateCharter(@Id long testCaseId, String charter) {
        ExploratoryTestCase exploratoryTestCase =
                exploratoryTestCaseDao.findById(testCaseId).orElseThrow();
        if (charter.isEmpty()) {
            exploratoryTestCase.setCharter(null);
        } else {
            exploratoryTestCase.setCharter(charter);
        }
        exploratoryTestCase.updateLastModificationWithCurrentUser();
    }

    @Override
    @PreAuthorize(WRITE_TC_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = TestCase.class)
    public void updateSessionDuration(@Id long testCaseId, int sessionDurationInMinutes) {
        ExploratoryTestCase exploratoryTestCase =
                exploratoryTestCaseDao.findById(testCaseId).orElseThrow();
        if (sessionDurationInMinutes == 0) {
            exploratoryTestCase.setSessionDuration(null);
        } else {
            exploratoryTestCase.setSessionDuration(sessionDurationInMinutes);
        }
        exploratoryTestCase.updateLastModificationWithCurrentUser();
    }
}
