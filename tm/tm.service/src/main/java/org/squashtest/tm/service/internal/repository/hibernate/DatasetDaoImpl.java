/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.PARAMETER;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.PARAMETER_VALUES;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DATASETS_BY_TESTCASE_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DATASETS_NAMES_BY_TESTCASE_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CALLER_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CTE;
import static org.squashtest.tm.service.internal.repository.ParameterNames.DATASET_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.SOURCE_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.TEST_CASE_IDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.DSLContext;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.service.internal.repository.CustomDatasetDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;

public class DatasetDaoImpl implements CustomDatasetDao {

    @PersistenceContext private EntityManager em;
    @Inject private DSLContext dslContext;

    @SuppressWarnings("unchecked")
    @Override
    public List<Dataset> findOwnDatasetsByTestCases(Collection<Long> testCaseIds) {
        if (!testCaseIds.isEmpty()) {
            Query query = em.createNamedQuery("Dataset.findOwnDatasetsByTestCases");
            query.setParameter(TEST_CASE_IDS, testCaseIds);
            return query.getResultList();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Dataset> findAllDelegateDatasets(Long testCaseId) {
        List<Dataset> allDatasets = new LinkedList<>();

        Set<Long> exploredTc = new HashSet<>();
        List<Long> srcTc = new LinkedList<>();
        List<Long> destTc;

        Query next = em.createNamedQuery("dataset.findTestCasesThatInheritParameters");

        srcTc.add(testCaseId);

        while (!srcTc.isEmpty()) {

            next.setParameter("srcIds", srcTc);
            destTc = next.getResultList();

            if (!destTc.isEmpty()) {
                allDatasets.addAll(findOwnDatasetsByTestCases(destTc));
            }

            exploredTc.addAll(srcTc);
            srcTc = destTc;
            srcTc.removeAll(exploredTc);
        }

        return allDatasets;
    }

    @Override
    public Map<Long, List<Dataset>> findAllDatasetsByTestCaseIds(Collection<Long> sourceTestCaseIds) {
        Map<Long, List<Long>> callerTestCaseIdsByCalled =
                findTestCaseIdsInheritParameters(sourceTestCaseIds);

        Set<Long> allTestCaseIds =
                Stream.concat(
                                sourceTestCaseIds.stream(),
                                callerTestCaseIdsByCalled.values().stream().flatMap(List::stream))
                        .collect(Collectors.toSet());

        Map<Long, List<Dataset>> datasetsByTestCaseId = findOwnDatasetsByTestCaseIds(allTestCaseIds);

        return sourceTestCaseIds.stream()
                .map(
                        testCaseId -> {
                            List<Dataset> datasets =
                                    new ArrayList<>(datasetsByTestCaseId.getOrDefault(testCaseId, List.of()));

                            callerTestCaseIdsByCalled.getOrDefault(testCaseId, Collections.emptyList()).stream()
                                    .map(callerId -> datasetsByTestCaseId.getOrDefault(callerId, List.of()))
                                    .forEach(datasets::addAll);

                            return datasets.isEmpty() ? null : Map.entry(testCaseId, datasets);
                        })
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map<Long, List<Long>> findTestCaseIdsInheritParameters(Collection<Long> testCaseIds) {
        var cte =
                name(CTE)
                        .fields(SOURCE_ID, CALLER_ID)
                        .as(
                                select(CALL_TEST_STEP.CALLED_TEST_CASE_ID, TEST_CASE_STEPS.TEST_CASE_ID)
                                        .from(CALL_TEST_STEP)
                                        .innerJoin(TEST_CASE_STEPS)
                                        .on(CALL_TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
                                        .where(
                                                CALL_TEST_STEP
                                                        .CALLED_TEST_CASE_ID
                                                        .in(testCaseIds)
                                                        .and(CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES.eq(Boolean.TRUE)))
                                        .union(
                                                select(
                                                                field(name(CTE, SOURCE_ID), Long.class),
                                                                TEST_CASE_STEPS.TEST_CASE_ID)
                                                        .from(name(CTE))
                                                        .innerJoin(CALL_TEST_STEP)
                                                        .on(field(name(CTE, CALLER_ID)).eq(CALL_TEST_STEP.CALLED_TEST_CASE_ID))
                                                        .innerJoin(TEST_CASE_STEPS)
                                                        .on(CALL_TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))));

        return dslContext
                .withRecursive(cte)
                .selectFrom(cte)
                .fetchGroups(field(SOURCE_ID, Long.class), field(CALLER_ID, Long.class));
    }

    @Override
    public Map<Long, List<Dataset>> findOwnDatasetsByTestCaseIds(Collection<Long> testCaseIds) {
        List<Dataset> datasets =
                new EntityGraphQueryBuilder<>(em, Dataset.class, FIND_DATASETS_BY_TESTCASE_IDS)
                        .addAttributeNodes(PARAMETER_VALUES)
                        .addSubGraph(PARAMETER_VALUES, PARAMETER)
                        .executeDistinctList(Map.of(IDS, testCaseIds));

        Map<Long, List<Dataset>> result = new HashMap<>();

        for (Dataset dataset : datasets) {
            result.computeIfAbsent(dataset.getTestCase().getId(), k -> new ArrayList<>()).add(dataset);
        }

        return result;
    }

    @Override
    public void removeDatasetFromTestPlanItems(Long datasetId) {
        Query query = em.createNamedQuery("dataset.removeDatasetFromItsIterationTestPlanItems");
        query.setParameter(DATASET_ID, datasetId);
        query.executeUpdate();

        Query query2 = em.createNamedQuery("dataset.removeDatasetFromItsCampaignTestPlanItems");
        query2.setParameter(DATASET_ID, datasetId);
        query2.executeUpdate();

        Query query3 = em.createNamedQuery("dataset.removeDatasetFromItsSprintReqVersionTestPlanItems");
        query3.setParameter(DATASET_ID, datasetId);
        query3.executeUpdate();
    }

    @Override
    public void removeDatasetsFromTestPlanItems(List<Long> testCaseIds) {
        Query query = em.createNamedQuery("dataset.removeDatasetsFromItsIterationTestPlanItems");
        query.setParameter(TEST_CASE_IDS, testCaseIds);
        query.executeUpdate();

        Query query2 = em.createNamedQuery("dataset.removeDatasetsFromItsCampaignTestPlanItems");
        query2.setParameter(TEST_CASE_IDS, testCaseIds);
        query2.executeUpdate();

        Query query3 =
                em.createNamedQuery("dataset.removeDatasetsFromItsSprintReqVersionTestPlanItems");
        query3.setParameter(TEST_CASE_IDS, testCaseIds);
        query3.executeUpdate();
    }

    @Override
    public Dataset findByTestCaseIdAndNameWithDatasetParamValues(Long testCaseId, String name) {
        Query query = em.createNamedQuery("Dataset.findByTestCaseIdAndNameWithDatasetParamValues");
        query.setParameter(TEST_CASE_ID, testCaseId);
        query.setParameter(ParameterNames.NAME, name);
        return (Dataset) query.getSingleResult();
    }

    @Override
    public void removeDatasetInBatchByTestCaseIds(List<Long> testCaseIds) {
        Query deleteDatasetSetParamValuesQuery =
                em.createNamedQuery("Dataset.batchDeleteDatasetParamValueByTestCaseIds");
        deleteDatasetSetParamValuesQuery.setParameter(TEST_CASE_IDS, testCaseIds);
        deleteDatasetSetParamValuesQuery.executeUpdate();

        Query deleteDatasetsQuery = em.createNamedQuery("Dataset.batchDeleteDatasetByTestCaseIds");
        deleteDatasetsQuery.setParameter(TEST_CASE_IDS, testCaseIds);
        deleteDatasetsQuery.executeUpdate();
    }

    @Override
    public Map<Long, List<String>> findOwnDatasetNamesByTestCaseIds(Collection<Long> testCaseIds) {
        try (Stream<Object[]> resultStream = getDatasetsNamesByTCIdsStream(testCaseIds)) {
            return resultStream.collect(
                    Collectors.groupingBy(
                            result -> (Long) result[0],
                            Collectors.mapping(result -> (String) result[1], Collectors.toList())));
        }
    }

    private Stream<Object[]> getDatasetsNamesByTCIdsStream(Collection<Long> testCaseIds) {
        return em.createQuery(FIND_DATASETS_NAMES_BY_TESTCASE_IDS, Object[].class)
                .setParameter(IDS, testCaseIds)
                .getResultStream();
    }
}
