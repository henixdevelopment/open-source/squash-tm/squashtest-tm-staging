/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception;

import java.io.Serial;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.core.foundation.exception.ActionException;

public class XrayParsingException extends ActionException {
    @Serial private static final long serialVersionUID = -8935819669759117567L;

    private final String messageKey;
    private final Object[] messageArgs;

    public XrayParsingException(String fileName, String message) {
        super(message);
        this.messageArgs = new Object[] {fileName, escapeMessageHtml(message)};
        messageKey = "sqtm-core.exception.xray-import.xray-parsing-exception";
    }

    public XrayParsingException(String fileName, String message, Throwable cause) {
        super(message, cause);
        this.messageArgs = new Object[] {fileName, escapeMessageHtml(message)};
        messageKey = "sqtm-core.exception.xray-import.xray-parsing-exception";
    }

    public XrayParsingException(String fileName, int line, String message, Throwable cause) {
        super(message, cause);
        this.messageArgs = new Object[] {fileName, escapeMessageHtml(message), line};
        messageKey = "sqtm-core.exception.xray-import.xray-parsing-exception-line";
    }

    public XrayParsingException(
            String fileName, String itemName, int line, String message, Throwable cause) {
        super(message, cause);
        this.messageArgs = new Object[] {fileName, escapeMessageHtml(message), line, itemName};
        messageKey = "sqtm-core.exception.xray-import.xray-parsing-exception-line-item-name";
    }

    @Override
    public String getI18nKey() {
        return messageKey;
    }

    @Override
    public Object[] messageArgs() {
        return messageArgs;
    }

    private String escapeMessageHtml(String message) {
        return HtmlUtils.htmlEscape(message);
    }
}
