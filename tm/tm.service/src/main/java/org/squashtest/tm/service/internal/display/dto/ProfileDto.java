/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.acl.AclGroup;

public class ProfileDto {

    private final long id;
    private final String qualifiedName;
    private final int partyCount;
    private final boolean active;
    private final boolean system;

    public ProfileDto(long id, String qualifiedName, boolean active) {
        this(id, qualifiedName, 0, active);
    }

    public ProfileDto(long id, String qualifiedName, int partyCount, boolean active) {
        this.id = id;
        this.qualifiedName = qualifiedName;
        this.partyCount = partyCount;
        this.active = active;
        this.system = AclGroup.isSystem(qualifiedName);
    }

    public long getId() {
        return id;
    }

    public String getQualifiedName() {
        return qualifiedName;
    }

    public int getPartyCount() {
        return partyCount;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isSystem() {
        return system;
    }
}
