/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.List;

public class CustomFieldViewDto extends CustomFieldDto {

    private List<BoundProjectToCufDto> boundProjectsToCuf;

    public CustomFieldViewDto(CustomFieldDto customFieldDto) {
        this.id = customFieldDto.getId();
        this.code = customFieldDto.getCode();
        this.defaultValue = customFieldDto.getDefaultValue();
        this.label = customFieldDto.getLabel();
        this.name = customFieldDto.getName();
        this.inputType = customFieldDto.getInputType();
        this.largeDefaultValue = customFieldDto.getLargeDefaultValue();
        this.numericDefaultValue = customFieldDto.getNumericDefaultValue();
        this.optional = customFieldDto.isOptional();
        this.options = customFieldDto.getOptions();
    }

    public List<BoundProjectToCufDto> getBoundProjectsToCuf() {
        return boundProjectsToCuf;
    }

    public void setBoundProjectsToCuf(List<BoundProjectToCufDto> boundProjectsToCuf) {
        this.boundProjectsToCuf = boundProjectsToCuf;
    }
}
