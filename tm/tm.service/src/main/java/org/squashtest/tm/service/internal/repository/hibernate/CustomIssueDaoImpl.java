/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import java.util.List;
import java.util.function.Supplier;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bugtracker.IssueDetector;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.testautomation.FailureDetail;
import org.squashtest.tm.service.internal.repository.CustomIssueDao;

@Repository
public class CustomIssueDaoImpl implements CustomIssueDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomIssueDaoImpl.class);

    @PersistenceContext private EntityManager entityManager;

    @Override
    public IssueDetector findIssueDetectorByIssue(long id) {
        List<Supplier<IssueDetector>> operations =
                List.of(
                        () -> findExecutionByIssue(id),
                        () -> findExecutionStepByIssue(id),
                        () -> findSessionNoteByIssue(id),
                        () -> findFailureDetailByIssue(id));

        for (Supplier<IssueDetector> operation : operations) {
            try {
                return operation.get();
            } catch (NoResultException e) {
                // Continue to next operation
            }
        }
        throw new NoResultException("No IssueDetector found for issue id: " + id);
    }

    private SessionNote findSessionNoteByIssue(long issueId) throws NoResultException {
        return (SessionNote)
                entityManager
                        .createNamedQuery("Issue.findSessionNote")
                        .setParameter("id", issueId)
                        .getSingleResult();
    }

    private FailureDetail findFailureDetailByIssue(long issueId) throws NoResultException {
        return (FailureDetail)
                entityManager
                        .createNamedQuery("Issue.findFailureDetail")
                        .setParameter("id", issueId)
                        .getSingleResult();
    }

    private Execution findExecutionByIssue(long issueId) throws NoResultException {
        return (Execution)
                entityManager
                        .createNamedQuery("Issue.findExecution")
                        .setParameter("id", issueId)
                        .getSingleResult();
    }

    private ExecutionStep findExecutionStepByIssue(long issueId) throws NoResultException {
        return (ExecutionStep)
                entityManager
                        .createNamedQuery("Issue.findExecutionStep")
                        .setParameter("id", issueId)
                        .getSingleResult();
    }

    @Override
    public Execution findExecutionRelatedToIssue(long id) {
        Execution exec = null;

        try {
            exec = findExecutionByIssue(id);

        } catch (NoResultException e) {
            try {
                ExecutionStep step = findExecutionStepByIssue(id);

                if (step.getExecution() != null) {
                    exec = step.getExecution();
                }
                // WARNING! it was previously catching all Exceptions
            } catch (NoResultException ex) {
                // NOOP - not too sure if this can happen, former hibernate based code would return null in
                // this case
                LOGGER.warn("Could not find execution step for issue id {}", id, ex);
            }
        }

        return exec;
    }
}
