/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.prittergherkin;

import gherkin.GherkinDialect;
import gherkin.GherkinDialectProvider;
import gherkin.ast.Background;
import gherkin.ast.Comment;
import gherkin.ast.DataTable;
import gherkin.ast.DocString;
import gherkin.ast.Examples;
import gherkin.ast.Feature;
import gherkin.ast.GherkinDocument;
import gherkin.ast.Node;
import gherkin.ast.Scenario;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
import gherkin.ast.Tag;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class WalkGherkinDocument<A extends GherkinAccumulator> {
    private final List<Comment> comments;

    public WalkGherkinDocument(List<Comment> comments) {
        this.comments = comments;
    }

    public A walkGherkinDocument(
            GherkinDocument gherkinDocument,
            A initialValue,
            GherkinDocumentHandlers<A> gherkinDocumentHandlers) {
        A acc = initialValue;
        if (Objects.isNull(gherkinDocument.getFeature())) {
            return acc;
        }
        Feature feature = gherkinDocument.getFeature();

        acc = walkFeature(feature, gherkinDocumentHandlers, acc);

        if (!comments.isEmpty()) {
            gherkinDocumentHandlers.appendNewLine(acc);
            for (Comment comment : comments) {
                acc = walkComment(comment, gherkinDocumentHandlers, acc);
            }
        }

        return acc;
    }

    private A walkComment(
            Comment comment, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        if (acc.getDeepestLine() < comment.getLocation().getLine()) {
            acc = gherkinDocumentHandlers.handleComment(comment, acc);
        }
        return acc;
    }

    private A walkFeature(
            Feature feature, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(feature.getLocation().getLine());
        acc =
                walkTags(
                        feature.getTags() == null ? Collections.emptyList() : feature.getTags(),
                        gherkinDocumentHandlers,
                        acc);
        acc = gherkinDocumentHandlers.handleFeature(feature, acc);
        GherkinDialect gherkinDialect =
                new GherkinDialectProvider(feature.getLanguage()).getDefaultDialect();
        for (ScenarioDefinition child : feature.getChildren()) {
            if (containsIgnoreCase(gherkinDialect.getBackgroundKeywords(), child.getKeyword())) {
                acc = walkBackground((Background) child, gherkinDocumentHandlers, acc);
            } else if (containsIgnoreCase(gherkinDialect.getScenarioKeywords(), child.getKeyword())) {
                acc = walkScenario((Scenario) child, gherkinDocumentHandlers, acc);
            } else if (containsIgnoreCase(
                    gherkinDialect.getScenarioOutlineKeywords(), child.getKeyword())) {
                acc = walkScenarioOutline((ScenarioOutline) child, gherkinDocumentHandlers, acc);
            }
        }
        return acc;
    }

    private A walkTags(List<Tag> tags, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        for (Tag tag : tags) {
            acc = walkTag(tag, gherkinDocumentHandlers, acc);
        }
        return acc;
    }

    private A walkTag(Tag tag, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(tag.getLocation().getLine());
        acc = gherkinDocumentHandlers.handleTag(tag, acc);
        return acc;
    }

    private A walkSteps(List<Step> steps, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        for (Step step : steps) {
            acc = walkStep(step, gherkinDocumentHandlers, acc);
        }
        return acc;
    }

    private A walkStep(Step step, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(step.getLocation().getLine());
        acc = gherkinDocumentHandlers.handleStep(step, acc);
        Node argument = step.getArgument();
        if (Objects.nonNull(argument)) {
            if (argument instanceof DocString docString) {
                acc = gherkinDocumentHandlers.handleDocString(docString, acc);
            } else if (argument instanceof DataTable dataTable) {
                acc = gherkinDocumentHandlers.handleDataTable(dataTable, acc);
                acc = walkTableRows(dataTable.getRows(), gherkinDocumentHandlers, acc);
            }
        }
        return acc;
    }

    private A walkTableRows(
            List<TableRow> tableRows, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        for (TableRow tableRow : tableRows) {
            acc = walkTableRow(tableRow, gherkinDocumentHandlers, acc);
        }
        return acc;
    }

    private A walkTableRow(
            TableRow tableRow, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(tableRow.getLocation().getLine());
        acc = gherkinDocumentHandlers.handleTableRow(tableRow, acc);
        for (TableCell tableCell : tableRow.getCells()) {
            acc = gherkinDocumentHandlers.handleTableCell(tableCell, acc);
        }
        return acc;
    }

    private A walkScenario(
            Scenario scenario, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(scenario.getLocation().getLine());
        acc =
                walkTags(
                        scenario.getTags() != null ? scenario.getTags() : Collections.emptyList(),
                        gherkinDocumentHandlers,
                        acc);
        acc = gherkinDocumentHandlers.handleScenario(scenario, acc);
        acc = walkSteps(scenario.getSteps(), gherkinDocumentHandlers, acc);
        return acc;
    }

    private A walkScenarioOutline(
            ScenarioOutline scenarioOutline, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(scenarioOutline.getLocation().getLine());
        acc =
                walkTags(
                        scenarioOutline.getTags() != null ? scenarioOutline.getTags() : Collections.emptyList(),
                        gherkinDocumentHandlers,
                        acc);
        acc = gherkinDocumentHandlers.handleScenarioOutline(scenarioOutline, acc);
        acc = walkSteps(scenarioOutline.getSteps(), gherkinDocumentHandlers, acc);

        if (scenarioOutline.getExamples() != null) {
            for (Examples examples : scenarioOutline.getExamples()) {
                acc = walkExamples(examples, gherkinDocumentHandlers, acc);
            }
        }
        return acc;
    }

    private A walkExamples(
            Examples examples, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(examples.getLocation().getLine());
        acc =
                walkTags(
                        examples.getTags() != null ? examples.getTags() : Collections.emptyList(),
                        gherkinDocumentHandlers,
                        acc);
        acc = gherkinDocumentHandlers.handleExamples(examples, acc);
        if (Objects.nonNull(examples.getTableHeader())) {
            acc = walkTableRow(examples.getTableHeader(), gherkinDocumentHandlers, acc);
            acc = walkTableRows(examples.getTableBody(), gherkinDocumentHandlers, acc);
        }
        return acc;
    }

    private A walkBackground(
            Background background, GherkinDocumentHandlers<A> gherkinDocumentHandlers, A acc) {
        acc.setDeepestLine(background.getLocation().getLine());
        acc = gherkinDocumentHandlers.handleBackground(background, acc);
        return walkSteps(background.getSteps(), gherkinDocumentHandlers, acc);
    }

    private static boolean containsIgnoreCase(List<String> list, String keyword) {
        return list.stream().anyMatch(item -> item.equalsIgnoreCase(keyword));
    }
}
