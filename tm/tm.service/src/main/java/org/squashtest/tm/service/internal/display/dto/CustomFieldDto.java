/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;

public class CustomFieldDto {

    protected Long id;
    protected String code;
    protected String inputType;
    protected String name;
    protected String label;
    protected String defaultValue;
    protected String largeDefaultValue;
    protected BigDecimal numericDefaultValue;
    protected boolean optional;
    protected List<CustomFieldOptionDto> options = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @CleanHtml
    public String getLargeDefaultValue() {
        return largeDefaultValue;
    }

    public void setLargeDefaultValue(String largeDefaultValue) {
        this.largeDefaultValue = largeDefaultValue;
    }

    public BigDecimal getNumericDefaultValue() {
        return numericDefaultValue;
    }

    public void setNumericDefaultValue(BigDecimal numericDefaultValue) {
        this.numericDefaultValue = numericDefaultValue;
    }

    public List<CustomFieldOptionDto> getOptions() {
        return options;
    }

    public void setOptions(List<CustomFieldOptionDto> options) {
        this.options = options;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }
}
