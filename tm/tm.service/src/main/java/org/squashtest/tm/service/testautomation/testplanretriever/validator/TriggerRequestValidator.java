/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.service.internal.dto.TriggerRequestDTO;

@Component
public class TriggerRequestValidator implements Validator {

    private static final String UUID_REGEXP =
            "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}";

    @Override
    public boolean supports(Class<?> clazz) {
        return TriggerRequestDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        TriggerRequestDTO triggerRequest = (TriggerRequestDTO) target;

        if (!triggerRequest.getTargetUUID().matches(UUID_REGEXP)) {
            errors.rejectValue(
                    "targetUUID", "wrong format", "This attribute does not have the right format.");
        }
    }
}
