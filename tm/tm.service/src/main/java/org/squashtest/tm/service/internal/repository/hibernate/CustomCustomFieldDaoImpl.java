/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_PROJECT_CUFS_BY_PROJECT_IDS_AND_ENTITIES;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ENTITIES;
import static org.squashtest.tm.service.internal.repository.ParameterNames.PROJECT_IDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.service.internal.repository.CustomCustomFieldDao;

@Repository
public class CustomCustomFieldDaoImpl implements CustomCustomFieldDao {

    @PersistenceContext private EntityManager em;

    @Override
    public Map<String, Map<BindableEntity, List<CustomField>>> findByProjectIdsAndEntities(
            Collection<Long> projectIds, Collection<BindableEntity> entities) {
        Map<String, Map<BindableEntity, List<CustomField>>> result = new HashMap<>();

        em.createQuery(FIND_PROJECT_CUFS_BY_PROJECT_IDS_AND_ENTITIES, Object[].class)
                .setParameter(PROJECT_IDS, projectIds)
                .setParameter(ENTITIES, entities)
                .getResultStream()
                .forEach(
                        r -> {
                            String projectName = (String) r[0];
                            BindableEntity entity = (BindableEntity) r[1];
                            CustomField cuf = (CustomField) r[2];

                            result
                                    .computeIfAbsent(projectName, k -> new HashMap<>())
                                    .computeIfAbsent(entity, k -> new ArrayList<>())
                                    .add(cuf);
                        });

        return result;
    }
}
