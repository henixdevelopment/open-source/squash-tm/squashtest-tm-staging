/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ActionStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.CallStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetParamValueInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.LinkedLowLevelRequirementInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementLinkInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;

/** Interface for batch import instructions methods. */
public interface Facility {
    LogTrain deleteTestCase(TestCaseTarget target);

    LogTrain updateActionStep(
            TestStepTarget target, ActionTestStep testStep, Map<String, String> cufValues);

    LogTrain updateCallStep(
            TestStepTarget target,
            CallTestStep testStep,
            TestCaseTarget calledTestCase,
            CallStepParamsInfo paramInfo,
            ActionTestStep actionStepBackup);

    LogTrain deleteTestStep(TestStepTarget target);

    LogTrain updateParameter(ParameterTarget target, Parameter param);

    LogTrain deleteParameter(ParameterTarget target);

    LogTrain deleteDataset(DatasetTarget dataset);

    /**
     * Will update the value for the given parameter in the given dataset. If the dataset doesn't
     * exist for this dataset, it will be created. If the parameter doesn't exist or is not available
     * to this dataset the method fails. In all cases the methods returns a log.
     */
    LogTrain failsafeUpdateParameterValue(
            DatasetTarget dataset, ParameterTarget param, String value, boolean isUpdate);

    /** Does exactly the same as the method above but with other arguments. */
    LogTrain updateTestCase(TestCaseInstruction instr);

    /**
     * Will create a RequirementVersion. If the Requirement it depends on doesn't exist it will be
     * created on the fly.
     */

    /**
     * Will delete a RequirementVersion when implemented some day, today it is not and will log a
     * Failure if invoked.
     */
    LogTrain deleteRequirementVersion(RequirementVersionInstruction instr);

    LogTrain createLinkedLowLevelRequirement(LinkedLowLevelRequirementInstruction instr);

    /**
     * Will remove a link between two requirements
     *
     * @param instr
     * @return
     */
    LogTrain deleteRequirementLink(RequirementLinkInstruction instr);

    // ----------------------- Batch operation -----------------------

    void createTestCases(List<TestCaseInstruction> createInstructions, Project project);

    void createParameters(List<ParameterInstruction> createParameters, Project project);

    void createDatasets(List<DatasetInstruction> instructions, Project project);

    void addActionSteps(List<ActionStepInstruction> actionStepInstructions, Project project);

    void addCallSteps(List<CallStepInstruction> callStepInstructions, Project project);

    void addDatasetParametersValues(List<DatasetParamValueInstruction> instructions, Project project);

    void createRequirementVersions(
            List<RequirementVersionInstruction> requirementVersionInstructions, Project project);

    /** Updates a RequiremenVersion with a new content. */
    void updateRequirementVersions(
            List<RequirementVersionInstruction> updateInstructions, Project project);

    void createCoverages(List<CoverageInstruction> coverageInstructions, Project project);

    void createOrUpdateRequirementLinks(
            List<RequirementLinkInstruction> requirementLinkInstructions, Project project);
}
