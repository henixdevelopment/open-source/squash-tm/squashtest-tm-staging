/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution.automatedexecution;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.execution.ExecutionFlag;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.service.execution.automatedexecution.AutomatedExecutionFlagService;
import org.squashtest.tm.service.internal.dto.AutomatedExecutionUpdateData;
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao;
import org.squashtest.tm.service.internal.repository.CustomAutomatedExecExtenderDao;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState;

@Service
@Transactional
public class AutomatedExecutionFlagServiceImpl implements AutomatedExecutionFlagService {

    private static final int RECENT_EXTENDER_ID_LIMIT = 10;
    private final AutomatedExecutionExtenderDao automatedExecutionDao;
    private final CustomAutomatedExecExtenderDao customAutomatedExecExtenderDao;
    private final AutomatedExecutionExtenderDao automatedExecutionExtenderDao;
    private final PluginFinderService pluginFinderService;

    public AutomatedExecutionFlagServiceImpl(
            AutomatedExecutionExtenderDao automatedExecutionDao,
            CustomAutomatedExecExtenderDao customAutomatedExecExtenderDao,
            AutomatedExecutionExtenderDao automatedExecutionExtenderDao,
            PluginFinderService pluginFinderService) {
        this.automatedExecutionDao = automatedExecutionDao;
        this.customAutomatedExecExtenderDao = customAutomatedExecExtenderDao;
        this.automatedExecutionExtenderDao = automatedExecutionExtenderDao;
        this.pluginFinderService = pluginFinderService;
    }

    @Override
    public void updateFlag(long extenderId, ExecutionFlag flag) {
        if (pluginFinderService.isPremiumPluginInstalled() && isProjectBoundToBugtracker(extenderId)) {
            AutomatedExecutionExtender extender = automatedExecutionDao.findById(extenderId);
            if (extender != null) {
                extender.setFlag(flag);
            }
        }
    }

    @Override
    public void handleFlagReset(long extenderId) {
        if (pluginFinderService.isPremiumPluginInstalled()
                && isProjectBoundToBugtracker(extenderId)
                && areAllFailureDetailLinked(extenderId)) {
            updateFlag(extenderId, null);
        }
    }

    @Override
    public void updateExecutionFlag(
            AutomatedExecutionState stateChange, AutomatedExecutionUpdateData updater) {
        long extenderId = updater.extenderId();
        if (!isProjectBoundToBugtracker(extenderId)) {
            return;
        }
        org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus status =
                stateChange.getTfTestExecutionStatus().getStatus();
        if (org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.FAILURE.equals(status)
                && isAnyFailureDetailUnlinked(extenderId)) {
            updateFlag(
                    extenderId,
                    isExecutionFlaky(extenderId) ? ExecutionFlag.FLAKY : ExecutionFlag.TO_BE_ANALYSED);
        } else if (org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.SUCCESS.equals(
                        status)
                && isLastExecutionFailureWithFailureDetailIssue(extenderId)) {
            updateFlag(extenderId, ExecutionFlag.FIXED);
        }
    }

    @Override
    public List<Long> findExtenderIdsByIssueId(long issueId) {
        return pluginFinderService.isPremiumPluginInstalled()
                ? customAutomatedExecExtenderDao.findExtenderIdsByIssueId(issueId)
                : List.of();
    }

    private boolean isProjectBoundToBugtracker(long extenderId) {
        AutomatedExecutionExtender extender = automatedExecutionExtenderDao.findById(extenderId);
        return extender != null && extender.getExecution().getProject().isBoundToBugtracker();
    }

    private boolean isAnyFailureDetailUnlinked(long extenderId) {
        return !customAutomatedExecExtenderDao.areAllFailureDetailLinkedToIssueByExtenderId(extenderId);
    }

    private boolean areAllFailureDetailLinked(long extenderId) {
        return customAutomatedExecExtenderDao.areAllFailureDetailLinkedToIssueByExtenderId(extenderId);
    }

    private boolean isAnyFailureDetailLinked(long extenderId) {
        return customAutomatedExecExtenderDao.isAnyFailureDetailLinkedToIssueByExtenderId(extenderId);
    }

    @Override
    public void updateToBeAnalysedFlag(List<Long> extenderIds) {
        if (pluginFinderService.isPremiumPluginInstalled()) {
            Map<Long, Boolean> isAnyFailureDetailUnlinkedMap =
                    customAutomatedExecExtenderDao.getSomeFailureDetailDoesNotHaveIssueByExtenderId(
                            extenderIds);
            for (Long extenderId : extenderIds) {
                if (isAnyFailureDetailUnlinkedMap.get(extenderId)) {
                    updateFlag(extenderId, ExecutionFlag.TO_BE_ANALYSED);
                }
            }
        }
    }

    private boolean isExecutionFlaky(long extenderId) {
        if (isAnyFailureDetailLinked(extenderId)) {
            return false;
        }
        List<Long> recentExtenderIds =
                customAutomatedExecExtenderDao.findRecentExtenderIdsByExtenderId(
                        extenderId, RECENT_EXTENDER_ID_LIMIT);
        return isFlakyByDifferentStatus(recentExtenderIds);
    }

    private boolean isFlakyByDifferentStatus(List<Long> recentExtenderIds) {
        Map<Long, ExecutionStatus> executionStatuses =
                customAutomatedExecExtenderDao.findExecutionStatusesByExtenderIds(recentExtenderIds);
        if (executionStatuses == null || executionStatuses.isEmpty()) {
            return false;
        }
        return isStatusTransitionDetected(executionStatuses);
    }

    private boolean isStatusTransitionDetected(Map<Long, ExecutionStatus> executionStatuses) {
        ExecutionStatus previousStatus = null;
        int count = 0;
        for (Map.Entry<Long, ExecutionStatus> entry : executionStatuses.entrySet()) {
            ExecutionStatus currentStatus = entry.getValue();
            if (previousStatus != null) {
                count += transitionCount(previousStatus, currentStatus);
            }
            previousStatus = currentStatus;
            if (count > 1) {
                return true;
            }
        }
        return false;
    }

    private int transitionCount(ExecutionStatus previousStatus, ExecutionStatus currentStatus) {
        if ((previousStatus == ExecutionStatus.SUCCESS && currentStatus == ExecutionStatus.FAILURE)
                || (previousStatus == ExecutionStatus.FAILURE
                        && currentStatus == ExecutionStatus.SUCCESS)) {
            return 1;
        }
        return 0;
    }

    private boolean isLastExecutionFailureWithFailureDetailIssue(long extenderId) {
        Long lastExtenderId = customAutomatedExecExtenderDao.findLastExtenderIdByExtenderId(extenderId);
        if (lastExtenderId != null) {
            String lastStatus =
                    customAutomatedExecExtenderDao.findExecutionStatusByExtenderId(lastExtenderId);
            return ExecutionStatus.FAILURE.name().equals(lastStatus)
                    && isAnyFailureDetailLinked(lastExtenderId);
        }
        return false;
    }
}
