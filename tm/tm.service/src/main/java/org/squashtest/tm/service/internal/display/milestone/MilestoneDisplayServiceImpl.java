/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.milestone;

import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.display.milestone.MilestoneDisplayService;
import org.squashtest.tm.service.internal.display.dto.MilestoneAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.MilestoneGrid;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.milestone.MilestoneManagerService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional
public class MilestoneDisplayServiceImpl implements MilestoneDisplayService {

    private final DSLContext dsl;
    private final MilestoneDisplayDao milestoneDisplayDao;
    private final UserAccountService userAccountService;
    private final MilestoneManagerService milestoneManagerService;
    private final PermissionEvaluationService permissionEvaluationService;

    @Inject
    MilestoneDisplayServiceImpl(
            DSLContext dsl,
            UserAccountService userAccountService,
            MilestoneDisplayDao milestoneDisplayDao,
            MilestoneManagerService milestoneManagerService,
            PermissionEvaluationService permissionEvaluationService) {
        this.userAccountService = userAccountService;
        this.milestoneDisplayDao = milestoneDisplayDao;
        this.dsl = dsl;
        this.milestoneManagerService = milestoneManagerService;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    @Override
    public GridResponse findAll(GridRequest request) {
        UserDto currentUser = userAccountService.findCurrentUserDto();

        if (!currentUser.isAdmin()) {
            permissionEvaluationService.checkAtLeastOneMilestoneManagementPermissionOrAdmin(
                    currentUser.getPartyIds());
        }

        Long userId = currentUser.getUserId();

        MilestoneGrid milestoneGrid = new MilestoneGrid(userId, currentUser.isAdmin());
        GridResponse response = milestoneGrid.getRows(request, dsl);
        response.sanitizeField("description");

        return response;
    }

    @Override
    public MilestoneAdminViewDto getMilestoneView(long milestoneId) {
        milestoneManagerService.checkIfCanManageMilestonesOrAdmin(
                Collections.singletonList(milestoneId));

        List<MilestoneDto> milestones =
                milestoneDisplayDao.findByIds(Collections.singleton(milestoneId));
        MilestoneAdminViewDto milestoneAdminViewDto =
                MilestoneAdminViewDto.fromMilestoneDto(milestones.get(0));
        milestoneAdminViewDto.setCanEdit(milestoneManagerService.canEditMilestone(milestoneId));
        milestoneDisplayDao.appendBoundProjectsToMilestoneInformation(milestoneAdminViewDto);
        return milestoneAdminViewDto;
    }
}
