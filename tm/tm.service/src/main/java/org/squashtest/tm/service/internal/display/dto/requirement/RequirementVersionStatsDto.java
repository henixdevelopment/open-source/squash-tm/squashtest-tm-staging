/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.requirement;

public class RequirementVersionStatsDto {
    private Integer allTestCaseCount = 0;
    private Integer executedTestCase = 0;
    private Integer plannedTestCase = 0;
    private Integer redactedTestCase = 0;
    private Integer validatedTestCases = 0;
    private Integer verifiedTestCase = 0;

    public Integer getAllTestCaseCount() {
        return allTestCaseCount;
    }

    public void setAllTestCaseCount(Integer allTestCaseCount) {
        this.allTestCaseCount = allTestCaseCount;
    }

    public Integer getExecutedTestCase() {
        return executedTestCase;
    }

    public void setExecutedTestCase(Integer executedTestCase) {
        this.executedTestCase = executedTestCase;
    }

    public Integer getPlannedTestCase() {
        return plannedTestCase;
    }

    public void setPlannedTestCase(Integer plannedTestCase) {
        this.plannedTestCase = plannedTestCase;
    }

    public Integer getRedactedTestCase() {
        return redactedTestCase;
    }

    public void setRedactedTestCase(Integer redactedTestCase) {
        this.redactedTestCase = redactedTestCase;
    }

    public Integer getValidatedTestCases() {
        return validatedTestCases;
    }

    public void setValidatedTestCases(Integer validatedTestCases) {
        this.validatedTestCases = validatedTestCases;
    }

    public Integer getVerifiedTestCase() {
        return verifiedTestCase;
    }

    public void setVerifiedTestCase(Integer verifiedTestCase) {
        this.verifiedTestCase = verifiedTestCase;
    }
}
