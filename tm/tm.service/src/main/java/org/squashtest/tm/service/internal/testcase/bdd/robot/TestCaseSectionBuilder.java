/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot;

import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.squashtest.tm.domain.bdd.util.ActionWordUtil.wrapWithDoubleQuotes;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.NEW_LINE;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.NEW_LINE_CHAR;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.assignment;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.dictionaryVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.DATASET_VARIABLE_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.DATATABLES_VARIABLE_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.DOCSTRINGS_VARIABLE_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.FOUR_SPACES;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.MultiLineStringBuilder;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.RETRIEVE_DATASET_KEYWORD_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.RETRIEVE_DATATABLES_KEYWORD_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.RETRIEVE_DOCSTRINGS_KEYWORD_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDatatableAccessor;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDocstringAccessor;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDocumentationSettingLines;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.isTestCaseUsingDatasets;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.isTestCaseUsingDatatables;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.isTestCaseUsingDocstrings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import org.squashtest.tm.domain.actionword.ActionWordFragmentVisitor;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bdd.ActionWordFragment;
import org.squashtest.tm.domain.bdd.ActionWordParameter;
import org.squashtest.tm.domain.bdd.ActionWordParameterValue;
import org.squashtest.tm.domain.bdd.ActionWordText;
import org.squashtest.tm.domain.bdd.util.ActionWordUtil;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.service.internal.testcase.bdd.TextGridFormatter;

public final class TestCaseSectionBuilder {

    private static final char SPACE_CHAR = ' ';

    private TestCaseSectionBuilder() {
        throw new UnsupportedOperationException("This class is not meant to be instantiated.");
    }

    public static String buildTestCasesSection(KeywordTestCase keywordTestCase) {
        final List<TestStep> steps = keywordTestCase.getSteps();
        final String testCaseName = keywordTestCase.getName();
        final MultiLineStringBuilder stringBuilder = new MultiLineStringBuilder();

        stringBuilder
                // *** Test Cases ***
                .appendLine(RobotSyntaxHelpers.TEST_CASE_SECTION_TITLE)

                // Name of my test case
                .appendLine(testCaseName)

                //     [Documentation]    Name of my test case
                .append(formatDocumentationSettingLines(Collections.singletonList(testCaseName)))
                .appendNewLine()

                //     &{datatables} =    Retrieve Datatables
                //     ...
                .append(formatTestCaseParameterLines(keywordTestCase))

                //     Given my test step
                //     ...
                .append(buildStepsLines(steps))
                .appendNewLine();

        return stringBuilder.toString();
    }

    private static String formatTestCaseParameterLines(KeywordTestCase keywordTestCase) {
        final List<List<String>> paramLines = buildParamLines(keywordTestCase);

        if (!paramLines.isEmpty()) {
            final TextGridFormatter gridFormatter = new TextGridFormatter();
            gridFormatter.addRows(paramLines);
            return gridFormatter.format(TextGridFormatter.withRowPrefix(FOUR_SPACES)) + NEW_LINE;
        } else {
            return "";
        }
    }

    private static String buildStepsLines(List<TestStep> steps) {
        final StringBuilder stepBuilder = new StringBuilder();

        int dataTableCounter = 1;
        int docStringCounter = 1;

        if (!steps.isEmpty()) {
            for (TestStep step : steps) {
                KeywordTestStep keywordStep = (KeywordTestStep) step;
                String stepScript = writeBddStepScript(keywordStep, dataTableCounter, docStringCounter);
                if (!isBlank(keywordStep.getDatatable())) {
                    dataTableCounter++;
                } else if (!isBlank(keywordStep.getDocstring())) {
                    docStringCounter++;
                }
                stepBuilder.append(FOUR_SPACES).append(stepScript).append(NEW_LINE_CHAR);
            }
            stepBuilder.append(NEW_LINE_CHAR);
        }

        return stepBuilder.toString();
    }

    public static String writeBddStepScript(
            KeywordTestStep testStep, int dataTableCounter, int docStringCounter) {
        ActionWord actionWord = testStep.getActionWord();
        List<ActionWordFragment> fragments = actionWord.getFragments();
        List<ActionWordParameterValue> parameterValues = testStep.getParamValues();

        String keywordScript = testStep.getKeyword().getLabel();
        String actionWordScript = generateStepScriptFromActionWordFragments(fragments, parameterValues);

        String dataTable = testStep.getDatatable();
        String docString = testStep.getDocstring();
        String comment = testStep.getComment();
        StringBuilder stepBuilder =
                new StringBuilder().append(keywordScript).append(SPACE_CHAR).append(actionWordScript);

        if (!isBlank(dataTable)) {
            stepBuilder
                    .append(SPACE_CHAR)
                    .append(wrapWithDoubleQuotes(formatDatatableAccessor(dataTableCounter)));
        } else if (!isBlank(docString)) {
            stepBuilder
                    .append(SPACE_CHAR)
                    .append(wrapWithDoubleQuotes(formatDocstringAccessor(docStringCounter)));
        }

        if (!isBlank(comment)) {
            String[] commentLines = comment.split("\n");
            for (String commentLine : commentLines) {
                stepBuilder
                        .append(NEW_LINE_CHAR)
                        .append(FOUR_SPACES)
                        .append('#')
                        .append(SPACE_CHAR)
                        .append(commentLine);
            }
        }
        return stepBuilder.toString();
    }

    private static String generateStepScriptFromActionWordFragments(
            List<ActionWordFragment> fragments, List<ActionWordParameterValue> parameterValues) {
        StringBuilder stringBuilder = new StringBuilder();
        Consumer<ActionWordParameter> consumer =
                parameter -> appendParamValueToGenerateScript(parameter, parameterValues, stringBuilder);
        final ActionWordFragmentVisitor visitor =
                new ActionWordFragmentVisitor() {
                    @Override
                    public void visit(ActionWordText text) {
                        stringBuilder.append(text.getUnescapedText());
                    }

                    @Override
                    public void visit(ActionWordParameter parameter) {
                        consumer.accept(parameter);
                    }
                };

        for (ActionWordFragment fragment : fragments) {
            fragment.accept(visitor);
        }
        return stringBuilder.toString();
    }

    private static void appendParamValueToGenerateScript(
            ActionWordParameter parameter,
            List<ActionWordParameterValue> parameterValues,
            StringBuilder stringBuilder) {
        Optional<ActionWordParameterValue> paramValue =
                parameterValues.stream()
                        .filter(
                                pv ->
                                        pv.getActionWordParam() != null
                                                && pv.getActionWordParam().getId().equals(parameter.getId()))
                        .findAny();
        paramValue.ifPresent(
                actionWordParameterValue ->
                        updateBuilderWithParamValue(stringBuilder, actionWordParameterValue));
    }

    private static void updateBuilderWithParamValue(
            StringBuilder stringBuilder, ActionWordParameterValue actionWordParameterValue) {
        String paramValue = actionWordParameterValue.getValue();
        if ("\"\"".equals(paramValue)) {
            stringBuilder.append(paramValue);
        } else if (actionWordParameterValue.isLinkedToTestCaseParam()) {
            String replacedCharactersString =
                    ActionWordUtil.substituteBracketsWithSquareBrackets(paramValue);
            replacedCharactersString = ActionWordUtil.removeEscapeCharacters(replacedCharactersString);

            final String dictionaryAccess =
                    RobotSyntaxHelpers.scalarVariable(DATASET_VARIABLE_NAME) + replacedCharactersString;

            stringBuilder.append(wrapWithDoubleQuotes(dictionaryAccess));
        } else {
            String unescapedString = ActionWordUtil.removeEscapeCharacters(paramValue);
            stringBuilder.append(wrapWithDoubleQuotes(unescapedString));
        }
    }

    private static List<List<String>> buildParamLines(KeywordTestCase keywordTestCase) {
        final List<List<String>> lines = new ArrayList<>();

        if (isTestCaseUsingDatasets(keywordTestCase)) {
            lines.add(buildCallToRetrieveKeyword(DATASET_VARIABLE_NAME, RETRIEVE_DATASET_KEYWORD_NAME));
        }

        if (isTestCaseUsingDatatables(keywordTestCase)) {
            lines.add(
                    buildCallToRetrieveKeyword(DATATABLES_VARIABLE_NAME, RETRIEVE_DATATABLES_KEYWORD_NAME));
        }

        if (isTestCaseUsingDocstrings(keywordTestCase)) {
            lines.add(
                    buildCallToRetrieveKeyword(DOCSTRINGS_VARIABLE_NAME, RETRIEVE_DOCSTRINGS_KEYWORD_NAME));
        }

        return lines;
    }

    private static List<String> buildCallToRetrieveKeyword(String assigneeName, String keywordName) {
        return Arrays.asList(assignment(dictionaryVariable(assigneeName)), keywordName);
    }
}
