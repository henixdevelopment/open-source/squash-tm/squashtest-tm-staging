/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.PIVOT_FORMAT_IMPORT;

import java.sql.Timestamp;
import java.util.Date;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus;

@Repository
@Transactional
public class PivotFormatImportDaoImpl implements CustomPivotFormatImportDao {

    @Inject private DSLContext dsl;

    @Override
    public void updatePivotFormatImportStatus(
            Long pivotFormatImportId, PivotFormatImportStatus status) {
        dsl.update(PIVOT_FORMAT_IMPORT)
                .set(PIVOT_FORMAT_IMPORT.STATUS, status.name())
                .where(PIVOT_FORMAT_IMPORT.PFI_ID.eq(pivotFormatImportId))
                .execute();
    }

    @Override
    public void updatePivotFormatImportSuccessfullyImportedOn(Long pivotFormatImportId, Date date) {
        Timestamp timestamp = new java.sql.Timestamp(date.getTime());
        dsl.update(PIVOT_FORMAT_IMPORT)
                .set(PIVOT_FORMAT_IMPORT.SUCCESSFULLY_IMPORTED_ON, timestamp)
                .where(PIVOT_FORMAT_IMPORT.PFI_ID.eq(pivotFormatImportId))
                .execute();
    }
}
