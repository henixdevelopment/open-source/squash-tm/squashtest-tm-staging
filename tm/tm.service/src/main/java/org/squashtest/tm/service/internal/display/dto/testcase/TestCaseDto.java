/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.AutomatedTestDto;
import org.squashtest.tm.service.internal.display.dto.AutomationRequestDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionCoverageDto;

public class TestCaseDto {

    private Long id;
    private Long projectId;
    private String name;
    private String reference;
    private String importance;
    private String status;
    private Long nature;
    private Long type;
    private String description;
    private Boolean importanceAuto;
    private String automatable;
    private String prerequisite;
    private AutomationRequestDto automationRequest;
    private List<AbstractTestStepDto> testSteps = new ArrayList<>();
    private List<MilestoneDto> milestones = new ArrayList<>();
    private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
    private List<ParameterDto> parameters = new ArrayList<>();
    private List<DataSetDto> datasets = new ArrayList<>();
    private List<DatasetParamValueDto> datasetParamValues = new ArrayList<>();
    private List<RequirementVersionCoverageDto> coverages = new ArrayList<>();
    private Long attachmentListId;
    private AttachmentListDto attachmentList;
    private String uuid;
    private AutomatedTestDto automatedTest;
    private List<ExecutionDto> executions = new ArrayList<>();
    private int nbIssues;
    private List<CalledTestCaseDto> calledTestCases = new ArrayList<>();
    private Date createdOn;
    private String createdBy;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private String executionMode;
    private String lastExecutionStatus;
    private Long nbExecutions;
    private Integer nbSessions;
    private String script;
    private String charter;
    private Integer sessionDuration;
    private Long scriptedTestCaseId;
    private Long keywordTestCaseId;
    private Long exploratoryTestCaseId;
    private Boolean actionWordLibraryActive;
    private String automatedTestReference;
    private Long automatedTestTechnology;
    private Long scmRepositoryId;
    private String configuredRemoteFinalStatus;
    private boolean draftedByAi;

    private String path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<AbstractTestStepDto> getTestSteps() {
        return testSteps;
    }

    public void setTestSteps(List<AbstractTestStepDto> testSteps) {
        this.testSteps = testSteps;
    }

    public String getImportance() {
        return importance;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getImportanceAuto() {
        return importanceAuto;
    }

    public void setImportanceAuto(Boolean importanceAuto) {
        this.importanceAuto = importanceAuto;
    }

    public List<MilestoneDto> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<MilestoneDto> milestones) {
        this.milestones = milestones;
    }

    public Long getNature() {
        return nature;
    }

    public void setNature(Long nature) {
        this.nature = nature;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getAutomatable() {
        return automatable;
    }

    public void setAutomatable(String automatable) {
        this.automatable = automatable;
    }

    public AutomationRequestDto getAutomationRequest() {
        return automationRequest;
    }

    public void setAutomationRequest(AutomationRequestDto automationRequest) {
        this.automationRequest = automationRequest;
    }

    public List<CustomFieldValueDto> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public List<ParameterDto> getParameters() {
        return parameters;
    }

    public void setParameters(List<ParameterDto> parameters) {
        this.parameters = parameters;
    }

    public List<DataSetDto> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<DataSetDto> datasets) {
        this.datasets = datasets;
    }

    public List<DatasetParamValueDto> getDatasetParamValues() {
        return datasetParamValues;
    }

    public void setDatasetParamValues(List<DatasetParamValueDto> datasetParamValues) {
        this.datasetParamValues = datasetParamValues;
    }

    public List<RequirementVersionCoverageDto> getCoverages() {
        return coverages;
    }

    public void setCoverages(List<RequirementVersionCoverageDto> coverages) {
        this.coverages = coverages;
    }

    public Long getAttachmentListId() {
        return attachmentListId;
    }

    public void setAttachmentListId(Long attachmentListId) {
        this.attachmentListId = attachmentListId;
    }

    public AttachmentListDto getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(AttachmentListDto attachmentList) {
        this.attachmentList = attachmentList;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public AutomatedTestDto getAutomatedTest() {
        return automatedTest;
    }

    public void setAutomatedTest(AutomatedTestDto automatedTest) {
        this.automatedTest = automatedTest;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public TestCaseKind getKind() {
        return TestCaseKind.fromLeftJoinIds(
                this.scriptedTestCaseId, this.keywordTestCaseId, this.exploratoryTestCaseId);
    }

    public void setKind(String kind) {
        throw new UnsupportedOperationException(
                "As 1.22 merged into 2.0 this attribute is deprecated. The getter is kept to ensure compatibility");
    }

    public List<ExecutionDto> getExecutions() {
        return executions;
    }

    public void setExecutions(List<ExecutionDto> executions) {
        this.executions = executions;
    }

    public List<CalledTestCaseDto> getCalledTestCases() {
        return calledTestCases;
    }

    public void setCalledTestCases(List<CalledTestCaseDto> calledTestCases) {
        this.calledTestCases = calledTestCases;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(String executionMode) {
        this.executionMode = executionMode;
    }

    public String getLastExecutionStatus() {
        return lastExecutionStatus;
    }

    public void setLastExecutionStatus(String lastExecutionStatus) {
        this.lastExecutionStatus = lastExecutionStatus;
    }

    public Long getNbExecutions() {
        return nbExecutions;
    }

    public void setNbExecutions(Long nbExecutions) {
        this.nbExecutions = nbExecutions;
    }

    public Integer getNbSessions() {
        return nbSessions;
    }

    public void setNbSessions(Integer nbSessions) {
        this.nbSessions = nbSessions;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public void setScriptedTestCaseId(Long scriptedTestCaseId) {
        this.scriptedTestCaseId = scriptedTestCaseId;
    }

    public void setKeywordTestCaseId(Long keywordTestCaseId) {
        this.keywordTestCaseId = keywordTestCaseId;
    }

    public Boolean getActionWordLibraryActive() {
        return actionWordLibraryActive;
    }

    public void setActionWordLibraryActive(Boolean actionWordLibraryActive) {
        this.actionWordLibraryActive = actionWordLibraryActive;
    }

    public int getNbIssues() {
        return nbIssues;
    }

    public void setNbIssues(int nbIssues) {
        this.nbIssues = nbIssues;
    }

    public Long getScriptedTestCaseId() {
        return scriptedTestCaseId;
    }

    public Long getKeywordTestCaseId() {
        return keywordTestCaseId;
    }

    public Long getExploratoryTestCaseId() {
        return exploratoryTestCaseId;
    }

    public void setExploratoryTestCaseId(Long exploratoryTestCaseId) {
        this.exploratoryTestCaseId = exploratoryTestCaseId;
    }

    public String getAutomatedTestReference() {
        return automatedTestReference;
    }

    public void setAutomatedTestReference(String automatedTestReference) {
        this.automatedTestReference = automatedTestReference;
    }

    public Long getAutomatedTestTechnology() {
        return automatedTestTechnology;
    }

    public void setAutomatedTestTechnology(Long automatedTestTechnology) {
        this.automatedTestTechnology = automatedTestTechnology;
    }

    public Long getScmRepositoryId() {
        return scmRepositoryId;
    }

    public void setScmRepositoryId(Long scmRepositoryId) {
        this.scmRepositoryId = scmRepositoryId;
    }

    public String getConfiguredRemoteFinalStatus() {
        return configuredRemoteFinalStatus;
    }

    public void setConfiguredRemoteFinalStatus(String configuredRemoteFinalStatus) {
        this.configuredRemoteFinalStatus = configuredRemoteFinalStatus;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCharter() {
        return charter;
    }

    public void setCharter(String charter) {
        this.charter = charter;
    }

    public Integer getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(Integer sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public boolean getDraftedByAi() {
        return draftedByAi;
    }

    public void setDraftedByAi(boolean draftedByAi) {
        this.draftedByAi = draftedByAi;
    }
}
