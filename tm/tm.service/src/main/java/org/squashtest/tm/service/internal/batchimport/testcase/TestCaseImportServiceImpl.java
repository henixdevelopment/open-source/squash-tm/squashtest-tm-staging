/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseFolderImportData;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseImportData;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.TestCaseFolderDao;
import org.squashtest.tm.service.internal.repository.TestCaseLibraryDao;
import org.squashtest.tm.service.internal.testcase.NatureTypeChainFixer;
import org.squashtest.tm.service.milestone.MilestoneMembershipManager;

@Service
@Transactional
public class TestCaseImportServiceImpl implements TestCaseImportService {

    private final TestCaseLibraryDao testCaseLibraryDao;
    private final TestCaseFolderDao testCaseFolderDao;
    private final TestCaseDao testCaseDao;
    private final EntityManager entityManager;
    private final PrivateCustomFieldValueService customFieldValueService;
    private final MilestoneMembershipManager milestoneService;

    public TestCaseImportServiceImpl(
            TestCaseLibraryDao testCaseLibraryDao,
            TestCaseFolderDao testCaseFolderDao,
            TestCaseDao testCaseDao,
            EntityManager entityManager,
            PrivateCustomFieldValueService customFieldValueService,
            MilestoneMembershipManager milestoneService) {
        this.testCaseLibraryDao = testCaseLibraryDao;
        this.testCaseFolderDao = testCaseFolderDao;
        this.testCaseDao = testCaseDao;
        this.entityManager = entityManager;
        this.customFieldValueService = customFieldValueService;
        this.milestoneService = milestoneService;
    }

    @Override
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public void addTestCasesToLibrary(
            @Id Long libraryId, Batch<TestCaseImportData> batch, Project project) {
        TestCaseLibrary library = testCaseLibraryDao.loadForNodeAddition(libraryId);

        for (TestCaseImportData importedTestCase : batch.getEntities()) {
            TestCase tc = importedTestCase.getTestCase();
            Integer position = importedTestCase.getPosition();

            if (position != null) {
                library.addContent(tc, position);
            } else {
                library.addContent(tc);
            }

            replaceInfoListReferences(tc);
            testCaseDao.persist(tc);
        }

        List<TestCaseImportData> importDataList = batch.getEntities();

        initializeCustomFieldValues(importDataList, project);
        initializeMilestoneBindings(importDataList);

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    @BatchPreventConcurrent(entityType = TestCaseFolder.class)
    public void addTestCasesToFolders(
            @Ids List<Long> folderIds, Project project, List<Batch<TestCaseImportData>> batchList) {
        Map<Long, TestCaseFolder> testCaseFolderById =
                testCaseFolderDao.loadForNodeAddition(folderIds).stream()
                        .collect(Collectors.toMap(TestCaseLibraryNode::getId, Function.identity()));

        for (Batch<TestCaseImportData> batch : batchList) {
            TestCaseFolder folder = testCaseFolderById.get(batch.getTargetId());

            for (TestCaseImportData importedTestCase : batch.getEntities()) {
                TestCase tc = importedTestCase.getTestCase();
                Integer position = importedTestCase.getPosition();

                if (position != null) {
                    folder.addContent(tc, position);
                } else {
                    folder.addContent(tc);
                }

                replaceInfoListReferences(tc);
                testCaseDao.safePersist(tc);
            }
        }

        List<TestCaseImportData> importDataList =
                batchList.stream().flatMap(a -> a.getEntities().stream()).toList();

        initializeCustomFieldValues(importDataList, project);
        initializeMilestoneBindings(importDataList);

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public void addFoldersToLibrary(
            @Id Long libraryId, Batch<TestCaseFolderImportData> libraryFolders) {
        TestCaseLibrary library = testCaseLibraryDao.loadForNodeAddition(libraryId);

        List<TestCaseFolder> folders =
                libraryFolders.getEntities().stream().map(TestCaseFolderImportData::mainFolder).toList();

        NatureTypeChainFixer fixer = new NatureTypeChainFixer();

        for (TestCaseFolder folder : folders) {
            library.addContent(folder);
            fixer.fix(folder);
            testCaseFolderDao.persist(folder);
        }

        batchFolderCustomFieldInitialization(folders, library.getProject().getId());

        List<TestCaseImportData> importDataList =
                libraryFolders.getEntities().stream()
                        .flatMap(data -> data.testCaseDataInNodes().stream())
                        .toList();

        initializeCustomFieldValues(importDataList, library.getProject().getProject());
        initializeMilestoneBindings(importDataList);

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    @BatchPreventConcurrent(entityType = TestCaseFolder.class)
    public void addFoldersToFolders(
            @Ids List<Long> folderIds, List<Batch<TestCaseFolderImportData>> batchList) {
        Map<Long, TestCaseFolder> testCaseFolderById =
                testCaseFolderDao.loadForNodeAddition(folderIds).stream()
                        .collect(Collectors.toMap(TestCaseLibraryNode::getId, Function.identity()));

        NatureTypeChainFixer fixer = new NatureTypeChainFixer();

        for (Batch<TestCaseFolderImportData> batch : batchList) {
            TestCaseFolder target = testCaseFolderById.get(batch.getTargetId());
            for (TestCaseFolderImportData folderImportData : batch.getEntities()) {
                TestCaseFolder folder = folderImportData.mainFolder();
                target.addContent(folder);
                fixer.fix(folder);
                testCaseFolderDao.persist(folder);
            }
        }

        List<TestCaseFolder> folders =
                batchList.stream()
                        .flatMap(batch -> batch.getEntities().stream())
                        .map(TestCaseFolderImportData::mainFolder)
                        .toList();

        Project project = testCaseFolderById.values().iterator().next().getProject();

        batchFolderCustomFieldInitialization(folders, project.getId());

        List<TestCaseImportData> importDataList =
                batchList.stream()
                        .flatMap(batch -> batch.getEntities().stream())
                        .flatMap(data -> data.testCaseDataInNodes().stream())
                        .toList();

        initializeCustomFieldValues(importDataList, project);
        initializeMilestoneBindings(importDataList);

        entityManager.flush();
        entityManager.clear();
    }

    private void batchFolderCustomFieldInitialization(
            List<TestCaseFolder> newFolders, Long projectId) {
        customFieldValueService.batchFolderCustomFieldValuesCreation(
                newFolders, BindableEntity.TESTCASE_FOLDER, projectId);
    }

    public void replaceInfoListReferences(TestCase testCase) {
        NatureTypeChainFixer.fix(testCase);
    }

    private void initializeCustomFieldValues(
            List<TestCaseImportData> testCasesData, Project project) {
        List<TestCase> testCases = testCasesData.stream().map(TestCaseImportData::getTestCase).toList();

        customFieldValueService.createAllCustomFieldValues(testCases, project);

        Map<TestCase, Map<Long, RawValue>> testCaseCustomFields =
                testCasesData.stream()
                        .filter(data -> !data.getCustomFields().isEmpty())
                        .collect(
                                Collectors.toMap(
                                        TestCaseImportData::getTestCase, TestCaseImportData::getCustomFields));

        customFieldValueService.initBatchCustomFieldValues(testCaseCustomFields);
    }

    private void initializeMilestoneBindings(List<TestCaseImportData> testCasesData) {
        Map<TestCase, List<Long>> testCaseMilestones =
                testCasesData.stream()
                        .filter(data -> !data.getMilestoneIds().isEmpty())
                        .collect(
                                Collectors.toMap(
                                        TestCaseImportData::getTestCase, TestCaseImportData::getMilestoneIds));

        milestoneService.bindHoldersToMilestones(testCaseMilestones);
    }
}
