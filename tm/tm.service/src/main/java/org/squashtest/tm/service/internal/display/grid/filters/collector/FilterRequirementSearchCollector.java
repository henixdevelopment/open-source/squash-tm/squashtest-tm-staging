/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.filters.collector;

import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK_TYPE;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingConditionStep;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.jooq.domain.tables.Resource;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.filters.GridFilterConditionBuilder;
import org.squashtest.tm.service.internal.display.grid.filters.GridFilterOperation;
import org.squashtest.tm.service.internal.display.search.filter.CurrentVersionFilterHandler;

public class FilterRequirementSearchCollector extends DefaultFilterCollector {
    private static final Pattern CUF_FILTER_PATTERN = Pattern.compile("CUF_FILTER_(\\d+)");
    private static final String HAS_DESCRIPTION_FALSE = "NO_DESCRIPTION";
    private static final String HAS_CHILDREN_FALSE = "NO_CHILDREN";
    private static final String HAS_PARENT_FALSE = "NO_PARENT";
    private static final String TYPE_LINK_AT_LEAST_ONE = "AT_LEAST_ONE";
    private static final String TYPE_LINK_NONE = "NONE";

    public FilterRequirementSearchCollector(
            Map<String, GridColumn> aliasToFieldDictionary, boolean searchingOnMultiColumns) {
        super(aliasToFieldDictionary, searchingOnMultiColumns);
    }

    @Override
    protected Condition convertFilterToCondition(GridFilterValue gridFilterValue) {
        ComplexeFilter complexeFilter = ComplexeFilter.getComplexeFilter(gridFilterValue.getId());
        // if the filter is a complexe filter, we use the specific implementation, otherwise we use the
        // default one with aliasDictionary
        if (complexeFilter != null) {
            return complexeFilter.getCondition().apply(gridFilterValue);
        } else {
            return super.convertFilterToCondition(gridFilterValue);
        }
    }

    private enum ComplexeFilter {
        REQUIREMENT_VERSION_CURRENT_VERSION(
                "REQUIREMENT_VERSION_CURRENT_VERSION", ComplexeFilter::requirementVersionCurrentVersion),
        REQUIREMENT_KIND("REQUIREMENT_KIND", ComplexeFilter::natureRequirement),
        ATTACHMENT_COUNT("ATTACHMENT_COUNT", ComplexeFilter::attachmentCount),
        HAS_DESCRIPTION("HAS_DESCRIPTION", ComplexeFilter::hasDescription),
        COVERAGES_COUNT("COVERAGES_COUNT", ComplexeFilter::coveragesCount),
        HAS_CHILDREN("HAS_CHILDREN", ComplexeFilter::hasChildren),
        HAS_PARENT("HAS_PARENT", ComplexeFilter::hasParent),
        HAS_LINK_TYPE("HAS_LINK_TYPE", ComplexeFilter::hasLinkType),
        REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT(
                "REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT",
                ComplexeFilter::requirementBoundToHighLevelRequirement),
        LINKED_STANDARD_REQUIREMENT(
                "LINKED_STANDARD_REQUIREMENT", ComplexeFilter::linkedStandardRequirement),
        CUF_FILTER("CUF_FILTER", ComplexeFilter::cufFilter);

        private final String alias;
        private final Function<GridFilterValue, Condition> getCondition;

        ComplexeFilter(String alias, Function<GridFilterValue, Condition> getCondition) {
            this.alias = alias;
            this.getCondition = getCondition;
        }

        private static String getNormalizeFieldName(String camelCase) {
            Converter<String, String> converter =
                    CaseFormat.LOWER_CAMEL.converterTo(CaseFormat.UPPER_UNDERSCORE);
            return converter.convert(camelCase);
        }

        private Function<GridFilterValue, Condition> getCondition() {
            return getCondition;
        }

        private static ComplexeFilter getComplexeFilter(String alias) {
            if (CUF_FILTER_PATTERN.matcher(alias).find()) {
                return CUF_FILTER;
            } else {
                String name = getNormalizeFieldName(alias);
                return Arrays.stream(ComplexeFilter.values())
                        .filter(filter -> filter.alias.equals(name))
                        .findFirst()
                        .orElse(null);
            }
        }

        // FILTER IMPLEMENTATION
        private static Condition requirementVersionCurrentVersion(GridFilterValue gridFilterValue) {
            String value = gridFilterValue.getValues().get(0);
            if (value.equals(
                    CurrentVersionFilterHandler.CurrentVersionFilterValue.LAST_NON_OBSOLETE.name())) {
                var rvSubQuery = REQUIREMENT_VERSION.as("rvSubQuery");
                return REQUIREMENT_VERSION.VERSION_NUMBER.eq(
                        DSL.select(DSL.max(rvSubQuery.VERSION_NUMBER))
                                .from(rvSubQuery)
                                .where(rvSubQuery.REQUIREMENT_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                                .and(rvSubQuery.REQUIREMENT_STATUS.notEqual(RequirementStatus.OBSOLETE.name())));
            } else if (value.equals(
                    CurrentVersionFilterHandler.CurrentVersionFilterValue.CURRENT.name())) {
                var rvSubQuery = REQUIREMENT_VERSION.as("rvSubQuery");
                return REQUIREMENT_VERSION.VERSION_NUMBER.eq(
                        DSL.select(DSL.max(rvSubQuery.VERSION_NUMBER))
                                .from(rvSubQuery)
                                .where(rvSubQuery.REQUIREMENT_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID)));
            } else {
                return DSL.noCondition();
            }
        }

        private static Condition natureRequirement(GridFilterValue gridFilterValue) {
            String requirement = Requirement.CLASS_NAME;
            String highLevelRequirement = HighLevelRequirement.class.getCanonicalName();

            List<String> values = gridFilterValue.getValues();
            if (values.size() == 1) {
                if (requirement.equals(values.get(0))) {
                    return DSL.field(HIGH_LEVEL_REQUIREMENT.RLN_ID).isNull();
                } else if (highLevelRequirement.equals(values.get(0))) {
                    return DSL.field(HIGH_LEVEL_REQUIREMENT.RLN_ID).isNotNull();
                }
            }
            return DSL.noCondition();
        }

        private static Condition attachmentCount(GridFilterValue gridFilterValue) {
            Condition condition =
                    GridFilterConditionBuilder.getConditionBuilder(
                                    DSL.count(ATTACHMENT.ATTACHMENT_ID), gridFilterValue)
                            .build();
            RequirementVersion rvSQ = REQUIREMENT_VERSION.as("ReqVersionSubQuery");
            Resource rSQ = RESOURCE.as("ResourceSubQuery");
            return DSL.exists(
                    DSL.selectOne()
                            .from(rvSQ)
                            .join(rSQ)
                            .on(rvSQ.RES_ID.eq(rSQ.RES_ID))
                            .leftJoin(ATTACHMENT)
                            .on(rSQ.ATTACHMENT_LIST_ID.eq(ATTACHMENT.ATTACHMENT_LIST_ID))
                            .where(REQUIREMENT_VERSION.RES_ID.eq(rvSQ.RES_ID))
                            .groupBy(rSQ.RES_ID)
                            .having(condition));
        }

        private static Condition hasDescription(GridFilterValue gridFilterValue) {
            String value = gridFilterValue.getValues().get(0);
            Field<String> descriptionField = DSL.field(RESOURCE.DESCRIPTION);
            if (value.equals(HAS_DESCRIPTION.name())) {
                return DSL.length(descriptionField).gt(0);
            } else if (value.equals(HAS_DESCRIPTION_FALSE)) {
                return DSL.length(descriptionField).eq(0);
            } else {
                throw new UnsupportedOperationException(
                        "Unknown value for has_description filter: " + value);
            }
        }

        private static Condition coveragesCount(GridFilterValue gridFilterValue) {
            Condition condition =
                    GridFilterConditionBuilder.getConditionBuilder(
                                    DSL.count(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID), gridFilterValue)
                            .build();
            return DSL.exists(
                    DSL.selectOne()
                            .from(REQUIREMENT_VERSION_COVERAGE)
                            .where(
                                    REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(
                                            REQUIREMENT_VERSION.RES_ID))
                            .groupBy(REQUIREMENT_VERSION.RES_ID)
                            .having(condition));
        }

        private static Condition hasChildren(GridFilterValue gridFilterValue) {
            SelectHavingConditionStep<Record1<Integer>> selectDescendantReq =
                    DSL.selectOne()
                            .from(RLN_RELATIONSHIP)
                            .where(RLN_RELATIONSHIP.ANCESTOR_ID.eq(REQUIREMENT.RLN_ID))
                            .groupBy(RLN_RELATIONSHIP.ANCESTOR_ID)
                            .having(DSL.count(RLN_RELATIONSHIP.DESCENDANT_ID).greaterThan(0));
            String value = gridFilterValue.getValues().get(0);
            if (value.equals(HAS_CHILDREN.name())) {
                return DSL.exists(selectDescendantReq);
            } else if (value.equals(HAS_CHILDREN_FALSE)) {
                return DSL.notExists(selectDescendantReq);
            } else {
                throw new UnsupportedOperationException("Unknown value for has_children filter: " + value);
            }
        }

        private static Condition hasParent(GridFilterValue gridFilterValue) {
            SelectHavingConditionStep<Record1<Integer>> selectParentReq =
                    DSL.selectOne()
                            .from(REQUIREMENT)
                            .join(RLN_RELATIONSHIP)
                            .on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP.ANCESTOR_ID))
                            .where(RLN_RELATIONSHIP.DESCENDANT_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                            .groupBy(REQUIREMENT.RLN_ID)
                            .having(DSL.count(REQUIREMENT.RLN_ID).greaterThan(0));
            String value = gridFilterValue.getValues().get(0);
            if (value.equals(HAS_PARENT.name())) {
                return DSL.exists(selectParentReq);
            } else if (value.equals(HAS_PARENT_FALSE)) {
                return DSL.notExists(selectParentReq);
            } else {
                throw new UnsupportedOperationException("Unknown value for has_children filter: " + value);
            }
        }

        private static Condition hasLinkType(GridFilterValue gridFilterValue) {
            List<String> values = gridFilterValue.getValues();
            String operation = gridFilterValue.getOperation();
            Select<?> condition =
                    DSL.selectOne()
                            .from(REQUIREMENT_VERSION_LINK)
                            .join(REQUIREMENT_VERSION_LINK_TYPE)
                            .on(REQUIREMENT_VERSION_LINK.LINK_TYPE_ID.eq(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID))
                            .where(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_LINK.REQUIREMENT_VERSION_ID))
                            .and(
                                    REQUIREMENT_VERSION_LINK
                                            .LINK_DIRECTION
                                            .eq(false)
                                            .and(REQUIREMENT_VERSION_LINK_TYPE.ROLE_2_CODE.in(values))
                                            .or(
                                                    REQUIREMENT_VERSION_LINK
                                                            .LINK_DIRECTION
                                                            .eq(true)
                                                            .and(REQUIREMENT_VERSION_LINK_TYPE.ROLE_1_CODE.in(values))));
            if (operation.equals(TYPE_LINK_AT_LEAST_ONE)) {
                return DSL.exists(condition);
            } else if (operation.equals(TYPE_LINK_NONE)) {
                return DSL.notExists(condition);
            } else {
                throw new UnsupportedOperationException(
                        "Unknown operation for has_link_type filter: " + operation);
            }
        }

        private static Condition requirementBoundToHighLevelRequirement(
                GridFilterValue gridFilterValue) {
            SelectConditionStep<Record1<Integer>> selectBoundHLReq =
                    DSL.selectOne()
                            .from(HIGH_LEVEL_REQUIREMENT)
                            .where(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                            .and(HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull());
            boolean bound = Boolean.parseBoolean(gridFilterValue.getValues().get(0));
            if (bound) {
                return DSL.exists(selectBoundHLReq);
            } else {
                return DSL.notExists(selectBoundHLReq);
            }
        }

        private static Condition linkedStandardRequirement(GridFilterValue gridFilterValue) {
            Condition condition =
                    GridFilterConditionBuilder.getConditionBuilder(
                                    DSL.count(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID), gridFilterValue)
                            .build();
            return DSL.exists(
                    DSL.selectOne()
                            .from(REQUIREMENT_VERSION_COVERAGE)
                            .where(
                                    REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(
                                            REQUIREMENT_VERSION.RES_ID))
                            .and(HIGH_LEVEL_REQUIREMENT.RLN_ID.isNull())
                            .groupBy(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID)
                            .having(condition));
        }

        private static Condition cufFilter(GridFilterValue gridFilterValue) {
            Long cufId =
                    CUF_FILTER_PATTERN
                            .matcher(gridFilterValue.getId())
                            .results()
                            .map(matchResult -> Long.parseLong(matchResult.group(1)))
                            .findFirst()
                            .orElseThrow();
            // Only tag can have multiple values
            if (gridFilterValue.getColumnPrototype().toUpperCase().contains("CUF_TAG")) {
                return cufFilterMultipleValue(gridFilterValue, cufId);
            } else if (gridFilterValue.getColumnPrototype().toUpperCase().contains("CUF_NUMERIC")) {
                Condition condition = createCufNumericCondition(gridFilterValue);
                return cufFilterSingleValue(cufId, condition);
            } else {
                // CUF_RICH_TEXT cannot be filtered
                Condition condition =
                        GridFilterConditionBuilder.getConditionBuilder(
                                        CUSTOM_FIELD_VALUE.VALUE, gridFilterValue)
                                .build();
                return cufFilterSingleValue(cufId, condition);
            }
        }

        // This method avoids multiple impacts by changing the implementation of EqualConditionBuilder
        private static Condition createCufNumericCondition(GridFilterValue gridFilterValue) {
            GridFilterOperation gridFilterOperation =
                    GridFilterOperation.valueOf(gridFilterValue.getOperation());
            if (GridFilterOperation.EQUALS.equals(gridFilterOperation)) {
                return CUSTOM_FIELD_VALUE.NUMERIC_VALUE.eq(
                        GridFilterConditionBuilder.getGridFilterValueConvert(
                                CUSTOM_FIELD_VALUE.NUMERIC_VALUE, gridFilterValue.getValues().get(0)));
            } else {
                return GridFilterConditionBuilder.getConditionBuilder(
                                CUSTOM_FIELD_VALUE.NUMERIC_VALUE, gridFilterValue, gridFilterOperation)
                        .build();
            }
        }

        private static Condition cufFilterSingleValue(Long cufId, Condition condition) {
            return DSL.exists(
                    DSL.selectOne()
                            .from(CUSTOM_FIELD_VALUE)
                            .where(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.eq(REQUIREMENT_VERSION.RES_ID))
                            .and(
                                    CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(
                                            BindableEntity.REQUIREMENT_VERSION.name()))
                            .and(CUSTOM_FIELD_VALUE.CF_ID.eq(cufId))
                            .and(condition));
        }

        private static Condition cufFilterMultipleValue(GridFilterValue gridFilterValue, Long cufId) {
            String gridFilterValueOperation = gridFilterValue.getOperation();
            SelectConditionStep<Record1<Integer>> commonQuery =
                    DSL.selectOne()
                            .from(CUSTOM_FIELD_VALUE)
                            .join(CUSTOM_FIELD_VALUE_OPTION)
                            .on(CUSTOM_FIELD_VALUE_OPTION.CFV_ID.eq(CUSTOM_FIELD_VALUE.CFV_ID))
                            .where(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.eq(REQUIREMENT_VERSION.RES_ID))
                            .and(
                                    CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(
                                            BindableEntity.REQUIREMENT_VERSION.name()))
                            .and(CUSTOM_FIELD_VALUE.CF_ID.eq(cufId));
            if ("AND".equalsIgnoreCase(gridFilterValueOperation)) {
                Condition condition =
                        GridFilterConditionBuilder.getConditionBuilder(
                                        CUSTOM_FIELD_VALUE_OPTION.LABEL, gridFilterValue, GridFilterOperation.IN)
                                .build();
                return DSL.exists(
                        commonQuery
                                .and(condition)
                                .groupBy(REQUIREMENT_VERSION.RES_ID)
                                .having(
                                        DSL.count(CUSTOM_FIELD_VALUE_OPTION.LABEL)
                                                .eq(gridFilterValue.getValues().size())));
            } else {
                Condition condition =
                        GridFilterConditionBuilder.getConditionBuilder(
                                        CUSTOM_FIELD_VALUE_OPTION.LABEL, gridFilterValue)
                                .build();
                return DSL.exists(commonQuery.and(condition));
            }
        }
    }
}
