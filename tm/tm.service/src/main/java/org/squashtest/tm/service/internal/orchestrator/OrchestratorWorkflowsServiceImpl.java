/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.orchestrator;

import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.internal.dto.WorkflowLogsDTO;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.orchestrator.OrchestratorOperationService;
import org.squashtest.tm.service.orchestrator.OrchestratorWorkflowsService;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;
import org.squashtest.tm.service.servers.CredentialsProvider;

@Service
public class OrchestratorWorkflowsServiceImpl implements OrchestratorWorkflowsService {

    private final AutomatedSuiteDao automatedSuiteDao;
    private final GenericProjectDao genericProjectDao;
    private final OrchestratorOperationService orchestratorOperationService;
    private final CredentialsProvider credentialsProvider;

    public OrchestratorWorkflowsServiceImpl(
            AutomatedSuiteDao automatedSuiteDao,
            GenericProjectDao genericProjectDao,
            OrchestratorOperationService orchestratorOperationService,
            CredentialsProvider credentialsProvider) {
        this.automatedSuiteDao = automatedSuiteDao;
        this.genericProjectDao = genericProjectDao;
        this.orchestratorOperationService = orchestratorOperationService;
        this.credentialsProvider = credentialsProvider;
    }

    @Override
    public WorkflowLogsDTO getWorkflowLogs(String workflowId, Long projectId) {

        TestAutomationServer orchestratorServer = genericProjectDao.findTestAutomationServer(projectId);

        TokenAuthCredentials credentials =
                credentialsProvider
                        .getProjectLevelCredentials(orchestratorServer.getId(), projectId)
                        .orElse(null);

        OrchestratorResponse<String> response =
                orchestratorOperationService.performOrchestratorOperation(
                        orchestratorServer.getId(),
                        (connector, server) -> connector.getWorkflowLogs(server, credentials, workflowId));

        AutomatedSuiteWorkflow automatedSuiteWorkflow =
                automatedSuiteDao.getAutomatedSuiteWorkflowByWorkflowId(workflowId);

        if (!response.isReachable()) {
            return WorkflowLogsDTO.withUnreachableOrchestrator();
        } else if (automatedSuiteWorkflow == null) {
            return WorkflowLogsDTO.withWorkflowAlreadyDone(response.getResponse());
        } else {
            return WorkflowLogsDTO.withResponse(response.getResponse());
        }
    }
}
