/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Stream;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.Subgraph;
import javax.persistence.TypedQuery;
import org.hibernate.jpa.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class EntityGraphQueryBuilder<T> {
    private static final String DELIMITER = ".";
    private final EntityManager entityManager;
    private final Class<T> entityType;
    private final String baseQuery;
    private final EntityGraph<T> entityGraph;
    private final Map<String, Subgraph<?>> subgraphs = new HashMap<>();

    public EntityGraphQueryBuilder(
            EntityManager entityManager, Class<T> entityType, String baseQuery) {
        this.entityManager = entityManager;
        this.entityType = entityType;
        this.baseQuery = baseQuery;
        this.entityGraph = entityManager.createEntityGraph(entityType);
    }

    public static String buildParentAttribute(String... attributes) {
        return String.join(DELIMITER, attributes);
    }

    public EntityGraphQueryBuilder<T> addAttributeNodes(String... attributeNodes) {
        entityGraph.addAttributeNodes(attributeNodes);
        return this;
    }

    public EntityGraphQueryBuilder<T> addSubGraph(
            String attributeName, String... subGraphAttributes) {
        Subgraph<?> subGraph = entityGraph.addSubgraph(attributeName);
        subGraph.addAttributeNodes(subGraphAttributes);
        subgraphs.put(attributeName, subGraph);
        return this;
    }

    public EntityGraphQueryBuilder<T> addSubGraph(
            String attributeName, Class<?> aClass, String... subGraphAttributes) {
        Subgraph<?> subGraph = entityGraph.addSubgraph(attributeName, aClass);
        subGraph.addAttributeNodes(subGraphAttributes);
        subgraphs.put(aClass.getSimpleName(), subGraph);
        return this;
    }

    public EntityGraphQueryBuilder<T> addSubgraphToSubgraph(
            String parentAttributeName, String attributeName, String... subGraphAttributes) {
        Subgraph<?> parentSubgraph = subgraphs.get(parentAttributeName);
        if (parentSubgraph == null) {
            throw new IllegalArgumentException("Parent subgraph not found for: " + parentAttributeName);
        }
        Subgraph<?> subGraph = parentSubgraph.addSubgraph(attributeName);
        subGraph.addAttributeNodes(subGraphAttributes);
        subgraphs.put(String.join(DELIMITER, parentAttributeName, attributeName), subGraph);

        return this;
    }

    public T execute(Map<String, Object> parameters) {
        TypedQuery<T> query = buildQuery(parameters);

        return query.getSingleResult();
    }

    public List<T> executeList(Map<String, Object> parameters) {
        TypedQuery<T> query = buildQuery(parameters);

        return query.getResultList();
    }

    public List<T> executeDistinctList(Map<String, Object> parameters) {
        TypedQuery<T> query = buildDistinctQuery(parameters);

        return query.getResultList();
    }

    public Page<T> executePageable(
            Map<String, Object> parameters, Pageable pageable, String countQuery) {
        TypedQuery<T> query;

        if (pageable.getSort().isUnsorted()) {
            query = buildQuery(parameters);
        } else {
            query = buildSortedQuery(parameters, pageable.getSort());
        }

        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        List<T> resultList = query.getResultList();

        TypedQuery<Long> typedCountQuery = entityManager.createQuery(countQuery, Long.class);
        if (parameters != null) {
            parameters.forEach(typedCountQuery::setParameter);
        }
        long total = typedCountQuery.getSingleResult();

        return new PageImpl<>(resultList, pageable, total);
    }

    private TypedQuery<T> buildSortedQuery(Map<String, Object> parameters, Sort sort) {
        StringJoiner sortingJoiner = new StringJoiner(", ", " ORDER BY ", "");
        sort.forEach(
                order -> {
                    String property = order.getProperty();
                    String direction = order.isAscending() ? "ASC" : "DESC";
                    sortingJoiner.add(property + " " + direction);
                });

        String sortedQuery = baseQuery.concat(sortingJoiner.toString());

        TypedQuery<T> query = entityManager.createQuery(sortedQuery, entityType);
        query.setHint(QueryHints.HINT_FETCHGRAPH, entityGraph);

        if (parameters != null) {
            parameters.forEach(query::setParameter);
        }

        return query;
    }

    public Stream<T> executeStream(Map<String, Object> parameters) {
        TypedQuery<T> query = buildQuery(parameters);

        return query.getResultStream();
    }

    private TypedQuery<T> buildQuery(Map<String, Object> parameters) {
        TypedQuery<T> query = entityManager.createQuery(baseQuery, entityType);
        query.setHint(QueryHints.HINT_FETCHGRAPH, entityGraph);

        if (parameters != null) {
            parameters.forEach(query::setParameter);
        }

        return query;
    }

    private TypedQuery<T> buildDistinctQuery(Map<String, Object> parameters) {
        TypedQuery<T> query = buildQuery(parameters);
        query.setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
        return query;
    }
}
