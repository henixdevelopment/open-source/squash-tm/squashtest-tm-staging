/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.httpclient;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpMessage;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.opentestfactory.messages.Status;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.internal.dto.WorkflowDto;
import org.squashtest.tm.service.internal.testautomation.codec.ObjectMapperFactory;
import org.squashtest.tm.service.internal.testautomation.model.messages.ChannelStatus;
import org.squashtest.tm.service.internal.testautomation.model.messages.KillWorkflowStatus;
import org.squashtest.tm.service.internal.testautomation.model.messages.VersionStatus;
import org.squashtest.tm.service.internal.testautomation.model.messages.Workflow;
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowData;
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowHandle;
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowsStatus;
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;

public class SquashAutomWorkflowClientImpl implements SquashAutomWorkflowClient, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SquashAutomWorkflowClientImpl.class);

    private static final int HTTP_ERROR_CODE = 400;

    private static final String PATH_TO_CHANNELS_ENDPOINT = "./channels";
    private static final String PATH_TO_WORKFLOWS_ENDPOINT = "./workflows";
    private static final String PATH_TO_VERSION_ENDPOINT = "./version";
    private static final String PATH_TO_LOGS_ENDPOINT = "/logs";
    private static final String GET_WORKFLOWS_PARAM = "?expand=manifest";

    private static final String NO_TOKEN_DEBUG_MESSAGE =
            "No authentication token, sending anonymously";
    private static final String GENERIC_REST_CALL_ERROR_MESSAGE =
            "Failed to manage REST call for technical reasons";

    private final String authToken;
    private final String receptionEndpoint;
    private final String observerEndpoint;
    private final String killSwitchEndpoint;
    private final int timeoutSeconds;

    public SquashAutomWorkflowClientImpl(
            String receptionEndpoint,
            String observerEndpoint,
            String killSwitchEndpoint,
            String authToken,
            int timeoutSeconds) {
        this.receptionEndpoint = receptionEndpoint;
        this.observerEndpoint = observerEndpoint;
        this.killSwitchEndpoint = killSwitchEndpoint;
        this.authToken = authToken;
        this.timeoutSeconds = timeoutSeconds;
    }

    @Override
    public List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(
            TestAutomationServer server) throws MessageRefusedException {
        ChannelStatus channelStatus = performJsonGetRestCall(buildChannelsUrl(), ChannelStatus.class);
        return channelStatus.getAutomatedExecutionEnvironments();
    }

    @Override
    public OrchestratorResponse<OrchestratorConfVersions> fetchOrchestratorConfVersions(
            TestAutomationServer server)
            throws MessageRefusedException, UnexpectedServerResponseException {
        VersionStatus versionStatus = performJsonGetRestCall(buildVersionUrl(), VersionStatus.class);
        return new OrchestratorResponse<>(versionStatus.getOrchestratorConfigurations(), true);
    }

    @Override
    public OrchestratorResponse<List<WorkflowDto>> fetchProjectWorkflows(
            TestAutomationServer server, Long projectId) throws MessageRefusedException {
        WorkflowsStatus workflowsStatus =
                performJsonGetRestCall(buildWorkflowsUrl(), WorkflowsStatus.class);
        List<WorkflowDto> workflows =
                workflowsStatus.getProjectWorkflows(projectId.toString()).stream()
                        .map(WorkflowData::toWorkflowDto)
                        .toList();
        return new OrchestratorResponse<>(workflows, true);
    }

    @Override
    public OrchestratorResponse<String> fetchWorkflowLogs(String workflowId)
            throws MessageRefusedException {
        String logs = performStringGetRestCall(buildWorkflowLogsUrl(workflowId));
        return new OrchestratorResponse<>(logs, true);
    }

    @Override
    public OrchestratorResponse<Void> killWorkflow(
            TestAutomationServer server, Long projectId, String workflowId)
            throws MessageRefusedException {
        try {
            KillWorkflowStatus response =
                    performJsonDeleteCall(buildKillWorkflowUrl(workflowId), KillWorkflowStatus.class);
            if (response.getCode() == 200) {
                return new OrchestratorResponse<>(true);
            } else {
                LOGGER.error(
                        "Failed to kill workflow {} on project {} : {}",
                        workflowId,
                        projectId,
                        response.getMessage());
                return new OrchestratorResponse<>(false);
            }
        } catch (MalformedURLException | ClientRuntimeException ex) {
            LOGGER.error(
                    "Failed to kill workflow {} on project {} : {}, check URL validity",
                    workflowId,
                    projectId,
                    ex.getMessage());
            return new OrchestratorResponse<>(false);
        }
    }

    private String buildWorkflowsUrl() {
        String baseUrl = StringUtils.isBlank(observerEndpoint) ? receptionEndpoint : observerEndpoint;
        return UrlUtils.appendPath(baseUrl, PATH_TO_WORKFLOWS_ENDPOINT + GET_WORKFLOWS_PARAM)
                .toExternalForm();
    }

    private String buildWorkflowLogsUrl(String workflowId) {
        String baseUrl = StringUtils.isBlank(observerEndpoint) ? receptionEndpoint : observerEndpoint;
        return UrlUtils.appendPath(
                        baseUrl, PATH_TO_WORKFLOWS_ENDPOINT + "/" + workflowId + PATH_TO_LOGS_ENDPOINT)
                .toExternalForm();
    }

    private String buildKillWorkflowUrl(String workflowId) throws MalformedURLException {
        if (!StringUtils.isBlank(killSwitchEndpoint)) {
            return UrlUtils.appendPath(killSwitchEndpoint, PATH_TO_WORKFLOWS_ENDPOINT + "/" + workflowId)
                    .toExternalForm();
        } else {
            final int defaultKillSwitchPort = 7776;
            String baseUrl = StringUtils.isBlank(observerEndpoint) ? receptionEndpoint : observerEndpoint;
            URL originalUrl = new URL(baseUrl);
            URL killSwitchUrl =
                    new URL(
                            originalUrl.getProtocol(),
                            originalUrl.getHost(),
                            defaultKillSwitchPort,
                            originalUrl.getFile());
            LOGGER.warn(
                    "No KillSwitchUrl, using constructed default (BaseUrl: "
                            + originalUrl
                            + ", Port: "
                            + defaultKillSwitchPort
                            + " => "
                            + killSwitchUrl
                            + ")");
            return UrlUtils.appendPath(
                            killSwitchUrl.toString(), PATH_TO_WORKFLOWS_ENDPOINT + "/" + workflowId)
                    .toExternalForm();
        }
    }

    public String buildChannelsUrl() {
        String baseUrl = StringUtils.isBlank(observerEndpoint) ? receptionEndpoint : observerEndpoint;
        return UrlUtils.appendPath(baseUrl, PATH_TO_CHANNELS_ENDPOINT).toExternalForm();
    }

    public String buildVersionUrl() {
        String baseUrl = StringUtils.isBlank(observerEndpoint) ? receptionEndpoint : observerEndpoint;
        return UrlUtils.appendPath(baseUrl, PATH_TO_VERSION_ENDPOINT).toExternalForm();
    }

    private <T extends Status<?>> T performJsonPostRestCall(
            Workflow workflow, String endpoint, Class<T> responseType) throws MessageRefusedException {
        try {
            final String payload = ObjectMapperFactory.getJsonObjectMapper().writeValueAsString(workflow);
            HttpPost request = new HttpPost();
            request.setEntity(new StringEntity(payload, ContentType.APPLICATION_JSON));
            return performRequest(request, endpoint, responseType);
        } catch (IOException ex) {
            throw new ClientRuntimeException(GENERIC_REST_CALL_ERROR_MESSAGE, ex, endpoint);
        }
    }

    private String performStringGetRestCall(String endpoint)
            throws MessageRefusedException, UnexpectedServerResponseException {
        HttpGet request = new HttpGet();
        return performRequest(request, endpoint, String.class);
    }

    private <T extends Status<?>> T performJsonGetRestCall(String endpoint, Class<T> responseType)
            throws MessageRefusedException, UnexpectedServerResponseException {
        HttpGet request = new HttpGet();
        return performRequest(request, endpoint, responseType);
    }

    private <T extends Status<?>> T performJsonDeleteCall(String endpoint, Class<T> responseType)
            throws MessageRefusedException {
        HttpDelete request = new HttpDelete();
        return performRequest(request, endpoint, responseType);
    }

    private <T> T performRequest(HttpRequestBase request, String endpoint, Class<T> responseType)
            throws MessageRefusedException {
        try {
            URI uri = new URI(endpoint);
            request.setURI(uri);
            request.setConfig(new RequestConfigurationFactory().getRequestConfig(uri, timeoutSeconds));
            appendAuthorizationHeaderOrLogAnonymousMode(request);

            try (CloseableHttpClient client = HttpClients.createSystem();
                    CloseableHttpResponse closeableResponse = client.execute(request)) {
                Response response = new Response(closeableResponse);
                return handleStatusResponse(response, uri, responseType);
            }
        } catch (URISyntaxException | IOException ex) {
            throw new ClientRuntimeException(GENERIC_REST_CALL_ERROR_MESSAGE, ex, endpoint);
        }
    }

    @Override
    public WorkflowHandle launch(Workflow workflow) throws MessageRefusedException {
        final String workflowsUrl =
                UrlUtils.appendPath(receptionEndpoint, PATH_TO_WORKFLOWS_ENDPOINT).toExternalForm();
        return performJsonPostRestCall(workflow, workflowsUrl, WorkflowHandle.class);
    }

    private void appendAuthorizationHeaderOrLogAnonymousMode(HttpMessage request) {
        if (StringUtils.isEmpty(this.authToken)) {
            LOGGER.debug(NO_TOKEN_DEBUG_MESSAGE);
        } else {
            request.addHeader(new BasicHeader("Authorization", "Bearer " + authToken));
        }
    }

    /**
     * Handles a {@link Response}, manages possible errors, and converts it into a given {@link
     * Status} subclass.
     *
     * @param response the http response
     * @param endPoint the endpoint (used to precise error messages)
     * @param responseClass into which the response must be converted
     * @return the given Response converted into the given class
     * @throws MessageRefusedException if the response has an error code
     * @throws IOException if an error occurs when reading the response
     */
    private <T> T handleStatusResponse(Response response, URI endPoint, Class<T> responseClass)
            throws MessageRefusedException, IOException, UnexpectedServerResponseException {
        try {
            if (response.getStatusLine().getStatusCode() >= HTTP_ERROR_CODE) {
                if (hasJsonMimeType(response)) {
                    throw new MessageRefusedException(endPoint, response.getStatusLine().getStatusCode());
                } else {
                    throw new UnexpectedServerResponseException(endPoint, response.getStatusLine());
                }
            } else if (responseClass == String.class) {
                return (T) EntityUtils.toString(response.getEntity());
            } else {
                return readValueWithJsonObjectMapper(response, responseClass);
            }
        } finally {
            response.getEntity().getContent().close();
        }
    }

    private boolean hasJsonMimeType(Response resp) {
        final String jsonMimeType = ContentType.APPLICATION_JSON.getMimeType();
        final String actualMimeType = resp.mimeType();
        return jsonMimeType.equals(actualMimeType);
    }

    private <T> T readValueWithJsonObjectMapper(Response response, Class<T> responseClass)
            throws IOException {
        return ObjectMapperFactory.getJsonObjectMapper()
                .readValue(response.getEntity().getContent(), responseClass);
    }
}
