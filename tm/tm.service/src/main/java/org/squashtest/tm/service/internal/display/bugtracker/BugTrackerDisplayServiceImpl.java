/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.bugtracker;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.csp.core.bugtracker.core.UnknownConnectorKindException;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.service.bugtracker.BugTrackerFinderService;
import org.squashtest.tm.service.display.bugtracker.BugTrackerDisplayService;
import org.squashtest.tm.service.internal.bugtracker.BugTrackerConnectorFactory;
import org.squashtest.tm.service.internal.bugtracker.adapter.InternalBugtrackerConnector;
import org.squashtest.tm.service.internal.display.dto.BugTrackerViewDto;
import org.squashtest.tm.service.internal.display.dto.CredentialsDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.BugTrackerGrid;
import org.squashtest.tm.service.internal.repository.display.BugTrackerDisplayDao;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

@Service
@Transactional
public class BugTrackerDisplayServiceImpl implements BugTrackerDisplayService {

    private final DSLContext dsl;
    private final BugTrackerDisplayDao bugTrackerDisplayDao;
    private final StoredCredentialsManager credentialsManager;
    private final BugTrackerFinderService bugTrackerFinderService;
    private final BugTrackerConnectorFactory connectorFactory;

    @Inject
    BugTrackerDisplayServiceImpl(
            DSLContext dsl,
            BugTrackerDisplayDao bugTrackerDisplayDao,
            StoredCredentialsManager credentialsManager,
            BugTrackerFinderService bugTrackerFinderService,
            BugTrackerConnectorFactory connectorFactory) {
        this.dsl = dsl;
        this.bugTrackerDisplayDao = bugTrackerDisplayDao;
        this.credentialsManager = credentialsManager;
        this.bugTrackerFinderService = bugTrackerFinderService;
        this.connectorFactory = connectorFactory;
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public GridResponse findAll(GridRequest gridRequest) {
        return new BugTrackerGrid().getRows(gridRequest, dsl);
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public BugTrackerViewDto getBugTrackerView(Long bugTrackerId)
            throws UnknownConnectorKindException {
        final BugTrackerViewDto dto = bugTrackerDisplayDao.getBugTrackerView(bugTrackerId);

        appendBugTrackerKinds(dto);
        appendAppLevelCredentials(bugTrackerId, dto);
        appendAuthConfiguration(bugTrackerId, dto);

        final BugTracker bugTracker = bugTrackerFinderService.findById(bugTrackerId);
        final InternalBugtrackerConnector connector = connectorFactory.createConnector(bugTracker);
        appendSupportedAuthProtocols(connector, dto);
        appendReportingCacheInfo(bugTracker, dto, connector);

        return dto;
    }

    private void appendBugTrackerKinds(BugTrackerViewDto dto) {
        final Set<String> bugTrackerKinds =
                bugTrackerFinderService.findBugTrackerKinds().stream()
                        .map(HTMLCleanupUtils::cleanHtml)
                        .collect(Collectors.toSet());
        dto.setBugTrackerKinds(bugTrackerKinds);
    }

    private void appendAppLevelCredentials(Long bugTrackerId, BugTrackerViewDto dto) {
        final ManageableCredentials credentials =
                credentialsManager.findAppLevelCredentials(bugTrackerId);
        dto.setCredentials(CredentialsDto.from(credentials));
    }

    private void appendAuthConfiguration(Long bugTrackerId, BugTrackerViewDto dto) {
        final ServerAuthConfiguration configuration =
                credentialsManager.findServerAuthConfiguration(bugTrackerId);
        dto.setAuthConfiguration(configuration);
    }

    private void appendSupportedAuthProtocols(
            InternalBugtrackerConnector connector, BugTrackerViewDto dto) {
        final List<String> supportedProtocols =
                Arrays.stream(connector.getSupportedAuthProtocols()).map(Enum::name).toList();
        dto.setSupportedAuthenticationProtocols(supportedProtocols);
    }

    private void appendReportingCacheInfo(
            BugTracker bugTracker, BugTrackerViewDto dto, InternalBugtrackerConnector connector) {
        final boolean allowsReportingCache = connector.allowsReportingCache();
        dto.setAllowsReportingCache(connector.allowsReportingCache());

        if (!allowsReportingCache) {
            return;
        }

        final ManageableCredentials manageableCredentials =
                credentialsManager.findReportingCacheCredentials(dto.getId());

        if (manageableCredentials != null) {
            final CredentialsDto credentialsDto = CredentialsDto.from(manageableCredentials);
            dto.setReportingCacheCredentials(credentialsDto);

            final Credentials credentials =
                    manageableCredentials.build(credentialsManager, bugTracker, null);
            dto.setHasCacheCredentialsError(!connector.areCredentialsValid(credentials));

            dto.setCacheInfo(connector.getCacheInfo());
        }
    }
}
