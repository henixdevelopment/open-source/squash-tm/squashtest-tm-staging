/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import javax.inject.Inject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.TestAutomationServerCredentialsService;
import org.squashtest.tm.service.testautomation.TestAutomationServerManagerService;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;

@Service("TestAutomationServerCredentialsService")
@Transactional
public class TestAutomationServerCredentialsServiceImpl
        implements TestAutomationServerCredentialsService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestAutomationServerCredentialsServiceImpl.class);
    private final TestAutomationConnectorRegistry taConnectorRegistry;
    private final TestAutomationServerManagerService testAutomationServerManagerService;
    private final StoredCredentialsManager storedCredentialsManager;

    @Inject
    public TestAutomationServerCredentialsServiceImpl(
            TestAutomationConnectorRegistry taConnectorRegistry,
            TestAutomationServerManagerService testAutomationServerManagerService,
            StoredCredentialsManager storedCredentialsManager) {
        this.taConnectorRegistry = taConnectorRegistry;
        this.testAutomationServerManagerService = testAutomationServerManagerService;
        this.storedCredentialsManager = storedCredentialsManager;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public AuthenticationProtocol[] getSupportedProtocols(TestAutomationServer server) {
        return taConnectorRegistry.createConnector(server).getSupportedProtocols();
    }

    @Override
    public void validateCredentials(long serverId, ManageableCredentials manageableCredentials) {
        TestAutomationServer server = testAutomationServerManagerService.findById(serverId);
        TestAutomationConnector connector = taConnectorRegistry.getConnectorForKind(server.getKind());
        Credentials credentials = manageableCredentials.build(storedCredentialsManager, server, null);
        try {
            connector.checkCredentials(server, credentials);
        } catch (TestAutomationException exception) {
            String serverName = server.getName();
            String serverUrl = server.getUrl();
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn(
                        "Failed to authenticate to server {} with url {}", serverName, serverUrl, exception);
            }
            throw exception;
        }
    }
}
