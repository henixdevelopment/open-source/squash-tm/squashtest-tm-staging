/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

@Component
@Profile("mariadb")
public class FullTextMariaFilterValueHandler implements FilterValueHandler {

    private final Set<String> handledPrototypes =
            Sets.newHashSet(
                    "TEST_CASE_DESCRIPTION", "TEST_CASE_PREQUISITE", "REQUIREMENT_VERSION_DESCRIPTION");

    @Override
    public boolean canHandleGridFilterValue(GridFilterValue filter) {
        return handledPrototypes.contains(filter.getColumnPrototype());
    }

    @Override
    public void handleGridFilterValue(GridFilterValue filter) {
        List<String> value = filter.getValues();
        List<String> htmlValues = value.stream().map(this::formatValue).toList();
        filter.setValues(htmlValues);
    }

    private String formatValue(String value) {
        // Format data to html encoding
        String newValue = HtmlUtils.htmlEscape(value);
        // Remove spaces at the beginning and at the end, and split if one or more spaces
        List<String> sValues = Arrays.asList(newValue.trim().split("\\s+"));
        newValue = sValues.stream().map(v -> "+\"" + v + "\"").collect(Collectors.joining(" "));
        return newValue;
    }
}
