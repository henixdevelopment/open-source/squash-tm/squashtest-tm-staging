/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.user;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.users.ApiToken;
import org.squashtest.tm.domain.users.ApiTokenPermission;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.JwtTokenImpossibleExtractionException;
import org.squashtest.tm.exception.apitoken.WrongExpiryDateException;
import org.squashtest.tm.exception.user.ApiTokenCreationException;
import org.squashtest.tm.exception.user.ApiTokenDeletionException;
import org.squashtest.tm.exception.user.ApiTokenFetchingException;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.internal.dto.ApiTokenDto;
import org.squashtest.tm.service.internal.repository.ApiTokenDao;
import org.squashtest.tm.service.internal.repository.CustomApiTokenDao;
import org.squashtest.tm.service.jwt.JwtTokenService;
import org.squashtest.tm.service.user.ApiTokenService;
import org.squashtest.tm.service.user.UserAccountService;
import org.squashtest.tm.service.user.UserManagerService;

@Service
@Transactional
public class ApiTokenServiceImpl implements ApiTokenService {

    private final UserAccountService userAccountService;
    private final ApiTokenDao apiTokenDao;
    private final CustomApiTokenDao customApiTokenDao;
    private final JwtTokenService jwtTokenService;
    private final UserManagerService userManagerService;

    @Value("${squash.rest-api.jwt.secret:#{null}}")
    private String jwtSecret;

    @PersistenceContext EntityManager entityManager;

    public ApiTokenServiceImpl(
            UserAccountService userAccountService,
            ApiTokenDao apiTokenDao,
            CustomApiTokenDao customApiTokenDao,
            JwtTokenService jwtTokenService,
            UserManagerService userManagerService) {
        this.userAccountService = userAccountService;
        this.apiTokenDao = apiTokenDao;
        this.customApiTokenDao = customApiTokenDao;
        this.jwtTokenService = jwtTokenService;
        this.userManagerService = userManagerService;
    }

    // User self-management

    @Override
    public ApiTokenDto generateApiToken(String name, Date expiryDate, String permissions) {
        checkExpiryDateConformity(expiryDate);
        User user = userAccountService.findCurrentUser();

        return doGenerateApiToken(name, expiryDate, permissions, user);
    }

    @Override
    public Page<ApiToken> findAllByUserId(long userId, Pageable pageable) {
        User currentUser = userManagerService.findByLogin(UserContextHolder.getUsername());
        if (currentUser.getId() == userId) {
            return apiTokenDao.findAllByUserId(userId, pageable);
        } else {
            throw new ApiTokenFetchingException("Unable to retrieve tokens belonging to another user.");
        }
    }

    @Override
    public void deletePersonalApiToken(long tokenId) {
        ApiToken apiToken = apiTokenDao.findById(tokenId).orElseThrow();
        if (UserContextHolder.getUsername().equals(apiToken.getUser().getLogin())) {
            apiTokenDao.deleteById(tokenId);
        } else {
            throw new ApiTokenDeletionException(
                    "Cannot delete a token that does not belong to the current user.");
        }
    }

    @Override
    public void selfDestroyApiToken(String token) {
        String tokenUuid = extractUuidFromJwtToken(token);
        apiTokenDao.deleteByUuid(tokenUuid);
    }

    // Administrator management for Test automation server users

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public ApiTokenDto generateApiTokenForTestAutoServer(
            long userId, String name, Date expiryDate, String permissions) {
        User testAutoServerUser = entityManager.find(User.class, userId);
        if (!customApiTokenDao.isTestAutomationServerUser(userId)) {
            throw new ApiTokenCreationException();
        }
        checkExpiryDateConformity(expiryDate);

        return doGenerateApiToken(name, expiryDate, permissions, testAutoServerUser);
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public Page<ApiToken> findAllTestAutoServerApiTokens(long userId, Pageable pageable) {
        if (customApiTokenDao.isTestAutomationServerUser(userId)) {
            return apiTokenDao.findAllByUserId(userId, pageable);
        } else {
            throw new ApiTokenFetchingException(
                    "Administrator can only retrieve tokens from a test automation server user.");
        }
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public void deleteTestAutoServerApiToken(long tokenId) {
        ApiToken apiToken = apiTokenDao.findById(tokenId).orElseThrow();
        if (customApiTokenDao.isTestAutomationServerUser(apiToken.getUser().getId())) {
            apiTokenDao.deleteById(tokenId);
        } else {
            throw new ApiTokenDeletionException(
                    "Administrator can only delete tokens from a test automation server user.");
        }
    }

    private static void checkExpiryDateConformity(Date expiryDate) {
        if (isDateAfterOneYearFromToday(expiryDate)) {
            throw new WrongExpiryDateException("Expiry date cannot be set later one year from today.");
        }
        if (isDateBeforeTomorrow(expiryDate)) {
            throw new WrongExpiryDateException("Expiry date cannot be set before tomorrow.");
        }
    }

    private ApiTokenDto doGenerateApiToken(
            String name, Date expiryDate, String permissions, User user) {
        UUID uuid = UUID.randomUUID();
        ApiToken newApiToken = persistApiToken(user, uuid.toString(), name, expiryDate, permissions);
        Date jwtExpiryDate = parseDateAtMidnightInUtc(newApiToken);

        String generatedJwtToken =
                jwtTokenService.generateJwt(
                        newApiToken.getUser().getId().toString(),
                        newApiToken.getUuid(),
                        jwtExpiryDate,
                        newApiToken.getCreatedOn(),
                        ApiTokenPermission.valueOf(permissions).name(),
                        jwtSecret);

        return new ApiTokenDto(newApiToken, generatedJwtToken);
    }

    private static boolean isDateAfterOneYearFromToday(Date givenDate) {
        LocalDate localGivenDate = givenDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate today = LocalDate.now();
        LocalDate oneYearFromToday = today.plusYears(1);
        return localGivenDate.isAfter(oneYearFromToday);
    }

    private static boolean isDateBeforeTomorrow(Date givenDate) {
        LocalDate localGivenDate = givenDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        return localGivenDate.isBefore(tomorrow);
    }

    private static Date parseDateAtMidnightInUtc(ApiToken newApiToken) {
        LocalDate localDate = LocalDate.parse(newApiToken.getExpiryDate());
        LocalDateTime localDateTime = LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
        Instant instant = localDateTime.toInstant(ZoneOffset.UTC);
        return Date.from(instant);
    }

    private String extractUuidFromJwtToken(String token) {
        String[] parts = token.split("\\.");

        if (parts.length < 2) {
            throw new IllegalArgumentException("Invalid JWT token");
        }

        String payload = new String(Base64.getUrlDecoder().decode(parts[1]));

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> claims;
        try {
            claims = objectMapper.readValue(payload, new TypeReference<>() {});
        } catch (JsonProcessingException e) {
            throw new JwtTokenImpossibleExtractionException(e);
        }

        return claims.get("uuid").toString();
    }

    private ApiToken persistApiToken(
            User user, String uuid, String name, Date expiryDate, String permissions) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(("yyyy-MM-dd"));
        String expiryDateString = simpleDateFormat.format(expiryDate);

        ApiToken apiToken =
                new ApiToken(
                        uuid,
                        user,
                        name,
                        new Date(),
                        UserContextHolder.getUsername(),
                        expiryDateString,
                        ApiTokenPermission.valueOf(permissions).name());

        return apiTokenDao.save(apiToken);
    }
}
