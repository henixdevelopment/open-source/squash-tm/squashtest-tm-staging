/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.httpclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.opentestfactory.messages.OTFMessage;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.testautomation.codec.ObjectMapperFactory;
import org.squashtest.tm.service.internal.testautomation.model.messages.PublicationStatus;

public class SquashAutomBusClientImpl implements SquashAutomBusClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(SquashAutomBusClientImpl.class);
    private static final String GENERIC_REST_CALL_ERROR_MESSAGE =
            "Failed to manage REST call for technical reasons";

    private final String busEndpoint;
    private final String authToken;
    private final int requestTimeoutSeconds;

    public SquashAutomBusClientImpl(String busEndpoint, String authToken, int requestTimeoutSeconds) {
        this.busEndpoint = busEndpoint;
        this.authToken = authToken;
        this.requestTimeoutSeconds = requestTimeoutSeconds;
    }

    @Override
    public PublicationStatus publish(OTFMessage event) throws MessageRefusedException {
        final String publicationUrl =
                UrlUtils.appendPath(busEndpoint, "./publications").toExternalForm();
        return performJsonRESTCall(event, publicationUrl);
    }

    protected PublicationStatus performJsonRESTCall(OTFMessage event, String endpoint)
            throws MessageRefusedException {
        try {
            URI methodURI = new URI(endpoint);
            final Request request = createRequest(methodURI, event);
            if (StringUtils.isEmpty(this.authToken)) {
                LOGGER.debug("No authentication token, sending anonymously");
            } else {
                request.addHeader(new BasicHeader("Authorization", "Bearer " + authToken));
            }
            Response r = request.execute();
            return handleResponse(r, methodURI);
        } catch (URISyntaxException | IOException ex) {
            throw new ClientRuntimeException(GENERIC_REST_CALL_ERROR_MESSAGE, ex, endpoint);
        }
    }

    private Request createRequest(URI endpoint, OTFMessage event) throws JsonProcessingException {

        final String payload = ObjectMapperFactory.getJsonObjectMapper().writeValueAsString(event);

        HttpPost request = new HttpPost(endpoint);
        request.setConfig(
                new RequestConfigurationFactory().getRequestConfig(endpoint, requestTimeoutSeconds));
        request.setEntity(new StringEntity(payload, ContentType.APPLICATION_JSON));
        return new HttpPostRequest(request);
    }

    private PublicationStatus handleResponse(Response resp, URI endPoint)
            throws IOException, MessageRefusedException {
        try {
            if (resp.getStatusLine().getStatusCode() >= 400) {
                if (sameMimeType(resp)) {
                    throw new MessageRefusedException(endPoint, resp.getStatusLine().getStatusCode());
                } else {
                    throw new UnexpectedServerResponseException(endPoint, resp.getStatusLine());
                }
            } else {
                return ObjectMapperFactory.getJsonObjectMapper()
                        .readValue(resp.getEntity().getContent(), PublicationStatus.class);
            }
        } finally {
            resp.getEntity().getContent().close();
        }
    }

    private boolean sameMimeType(Response resp) {
        final String refCtString = ContentType.APPLICATION_JSON.getMimeType();
        final String actualCTString = resp.mimeType();
        return refCtString.equals(actualCTString);
    }
}
