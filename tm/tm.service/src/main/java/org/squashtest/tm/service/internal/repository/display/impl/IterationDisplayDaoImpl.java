/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.val;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.campaign.Iteration.FULL_NAME_SEPARATOR;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.tables.CampaignIteration.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.tables.CampaignLibraryNode.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanList.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.tables.Iteration.ITERATION;
import static org.squashtest.tm.jooq.domain.tables.IterationTestPlanItem.ITERATION_TEST_PLAN_ITEM;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.jooq.tools.StringUtils;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationDto;
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class IterationDisplayDaoImpl implements IterationDisplayDao {

    private DSLContext dsl;

    public IterationDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public IterationDto findById(long iterationId) {
        return dsl.select(
                        ITERATION.ITERATION_ID.as(RequestAliasesConstants.ID),
                        ITERATION.NAME,
                        ITERATION.REFERENCE,
                        ITERATION.DESCRIPTION,
                        ITERATION.UUID,
                        ITERATION.ITERATION_STATUS,
                        ITERATION.CREATED_BY,
                        ITERATION.CREATED_ON,
                        ITERATION.LAST_MODIFIED_BY,
                        ITERATION.LAST_MODIFIED_ON,
                        ITERATION.ACTUAL_END_AUTO,
                        ITERATION.ACTUAL_END_DATE,
                        ITERATION.ACTUAL_START_AUTO,
                        ITERATION.ACTUAL_START_DATE,
                        ITERATION.SCHEDULED_END_DATE,
                        ITERATION.SCHEDULED_START_DATE,
                        ITERATION.ATTACHMENT_LIST_ID,
                        DSL.field(count(ITERATION_TEST_PLAN_ITEM.DATASET_ID).gt(0)).as("HAS_DATASETS"),
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID)
                .from(ITERATION)
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .leftJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .leftJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .where(ITERATION.ITERATION_ID.eq(iterationId))
                .groupBy(ITERATION.ITERATION_ID, CAMPAIGN_LIBRARY_NODE.PROJECT_ID)
                .fetchOne()
                .into(IterationDto.class);
    }

    @Override
    public HashMap<Long, String> getExecutionStatusMap(Long iterationId) {
        return (HashMap<Long, String>)
                dsl.select(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS)
                        .from(ITERATION)
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                        .where(ITERATION.ITERATION_ID.eq(iterationId))
                        .fetch()
                        .intoMap(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS);
    }

    @Override
    public int getNbTestPlanItem(Long iterationId, String login) {
        SelectConditionStep<Record1<Integer>> allTestPlanItemsFromIteration =
                dsl.selectCount()
                        .from(ITERATION)
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                        .innerJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(
                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .leftJoin(CORE_USER)
                        .on(CORE_USER.PARTY_ID.eq(ITERATION_TEST_PLAN_ITEM.USER_ID))
                        .where(ITERATION.ITERATION_ID.eq(iterationId));
        if (login != null) {
            allTestPlanItemsFromIteration = allTestPlanItemsFromIteration.and(CORE_USER.LOGIN.eq(login));
        }

        Integer count =
                allTestPlanItemsFromIteration.groupBy(ITERATION.ITERATION_ID).fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public List<String> retrieveFullNameByIterationIds(
            List<Long> iterationIds, List<Long> projectIds) {
        Field<String> fullName =
                when(
                                ITERATION.REFERENCE.isNull().or(ITERATION.REFERENCE.eq(StringUtils.EMPTY)),
                                ITERATION.NAME)
                        .otherwise(concat(ITERATION.REFERENCE, val(FULL_NAME_SEPARATOR), ITERATION.NAME));

        return dsl.select(fullName)
                .from(ITERATION)
                .join(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .join(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(ITERATION.ITERATION_ID.in(iterationIds))
                .and(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(fullName)
                .fetch(fullName);
    }

    @Override
    public List<Long> findDistinctProjectIdsByIterationIds(Set<Long> iterationIds) {
        return dsl.selectDistinct(CAMPAIGN_LIBRARY_NODE.PROJECT_ID)
                .from(CAMPAIGN_ITERATION)
                .leftJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(CAMPAIGN_ITERATION.ITERATION_ID.in(iterationIds))
                .fetchInto(Long.class);
    }

    @Override
    public Set<Long> findCampaignIdsByIterationIds(Set<Long> iterationIds) {
        return dsl.selectDistinct(CAMPAIGN_ITERATION.CAMPAIGN_ID)
                .from(CAMPAIGN_ITERATION)
                .where(CAMPAIGN_ITERATION.ITERATION_ID.in(iterationIds))
                .fetchSet(CAMPAIGN_ITERATION.CAMPAIGN_ID);
    }

    @Override
    public List<NamedReference> findNamedReferences(List<Long> targetIds) {
        return dsl.select(ITERATION.ITERATION_ID, ITERATION.NAME)
                .from(ITERATION)
                .where(ITERATION.ITERATION_ID.in(targetIds))
                .fetch()
                .into(NamedReference.class);
    }

    @Override
    public Map<Long, List<Long>> findExecutionIdsByIterationIds(List<Long> targetIds) {
        return dsl.select(ITEM_TEST_PLAN_LIST.ITERATION_ID, ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID)
                .from(ITEM_TEST_PLAN_LIST)
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .where(ITEM_TEST_PLAN_LIST.ITERATION_ID.in(targetIds))
                .fetchGroups(ITEM_TEST_PLAN_LIST.ITERATION_ID, ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID);
    }
}
