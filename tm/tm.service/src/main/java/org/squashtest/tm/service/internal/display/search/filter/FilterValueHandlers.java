/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import java.util.List;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

@Component
public class FilterValueHandlers {

    private List<FilterValueHandler> gridFilterValueHandlers;

    public FilterValueHandlers(List<FilterValueHandler> gridFilterValueHandlers) {
        this.gridFilterValueHandlers = gridFilterValueHandlers;
    }

    public boolean isHandledOutOfQueryEngine(GridFilterValue filter) {
        List<FilterValueHandler> handlers = findHandlers(filter);
        if (handlers.size() > 1) {
            throw new RuntimeException("There can be only one special filter handler for a filter value");
        }
        return handlers.size() == 1;
    }

    private List<FilterValueHandler> findHandlers(GridFilterValue filter) {
        return this.gridFilterValueHandlers.stream()
                .filter(filterHandler -> filterHandler.canHandleGridFilterValue(filter))
                .toList();
    }

    public void handleGridFilterValuesOutOfQueryEngine(GridFilterValue gridFilterValue) {
        if (isHandledOutOfQueryEngine(gridFilterValue)) {
            List<FilterValueHandler> handlers = findHandlers(gridFilterValue);
            handlers.get(0).handleGridFilterValue(gridFilterValue);
        }
    }
}
