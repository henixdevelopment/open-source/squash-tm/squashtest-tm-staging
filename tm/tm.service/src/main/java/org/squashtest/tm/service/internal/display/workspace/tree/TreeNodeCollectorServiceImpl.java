/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.workspace.tree;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;

@Service
@Transactional(readOnly = true)
public class TreeNodeCollectorServiceImpl implements TreeNodeCollectorService {

    private Map<NodeType, TreeNodeCollector> treeNodeCollectors;

    public TreeNodeCollectorServiceImpl(List<TreeNodeCollector> treeNodeCollectors) {
        this.treeNodeCollectors =
                treeNodeCollectors.stream()
                        .collect(
                                Collectors.toMap(TreeNodeCollector::getHandledEntityType, Function.identity()));
    }

    @Override
    public Map<NodeReference, DataRow> collectNodes(Set<NodeReference> nodeReferences) {
        Map<NodeReference, DataRow> childrenRows = new LinkedHashMap<>();
        Map<NodeType, List<NodeReference>> childrenByType =
                nodeReferences.stream().collect(Collectors.groupingBy(NodeReference::getNodeType));
        childrenByType.forEach(
                (nodeType, childrenRefs) -> {
                    List<Long> ids = childrenRefs.stream().map(NodeReference::getId).toList();
                    Map<Long, DataRow> dataRows = this.provide(nodeType).collect(ids);
                    dataRows.forEach(
                            (id, dataRow) -> childrenRows.put(new NodeReference(nodeType, id), dataRow));
                });
        return childrenRows;
    }

    @Override
    public DataRow collectNode(NodeType nodeType, Identified entity) {
        Long entityId = entity.getId();
        TreeNodeCollector treeNodeCollector = this.provide(nodeType);
        Map<Long, DataRow> nodes = treeNodeCollector.collect(Collections.singletonList(entityId));
        return nodes.get(entityId);
    }

    private TreeNodeCollector provide(NodeType nodeType) {
        if (this.treeNodeCollectors.containsKey(nodeType)) {
            return treeNodeCollectors.get(nodeType);
        }
        throw new UnsupportedOperationException(
                "No TreeNodeCollector implemented for type " + nodeType);
    }
}
