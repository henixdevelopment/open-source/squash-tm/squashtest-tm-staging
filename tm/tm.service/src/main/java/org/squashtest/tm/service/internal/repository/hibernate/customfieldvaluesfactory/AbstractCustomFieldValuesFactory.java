/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory;

import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;

import java.math.BigDecimal;
import org.jooq.DSLContext;
import org.jooq.Record8;
import org.jooq.Select;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;

public abstract class AbstractCustomFieldValuesFactory {

    private final DSLContext dsl;

    protected AbstractCustomFieldValuesFactory(DSLContext dsl) {
        this.dsl = dsl;
    }

    public void insertValues(CustomFieldBinding binding) {
        dsl.insertInto(
                        CUSTOM_FIELD_VALUE,
                        CUSTOM_FIELD_VALUE.CFB_ID,
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE,
                        CUSTOM_FIELD_VALUE.CF_ID,
                        CUSTOM_FIELD_VALUE.VALUE,
                        CUSTOM_FIELD_VALUE.LARGE_VALUE,
                        CUSTOM_FIELD_VALUE.NUMERIC_VALUE,
                        CUSTOM_FIELD_VALUE.FIELD_TYPE,
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID)
                .select(buildSelectClause(binding))
                .execute();
    }

    abstract Select<Record8<Long, String, Long, String, String, BigDecimal, String, Long>>
            buildSelectClause(CustomFieldBinding binding);
}
