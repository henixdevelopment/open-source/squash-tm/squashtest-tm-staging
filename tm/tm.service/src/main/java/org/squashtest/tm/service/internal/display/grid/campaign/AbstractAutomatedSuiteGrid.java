/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.coalesce;
import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.groupConcatDistinct;
import static org.jooq.impl.DSL.inline;
import static org.jooq.impl.DSL.substring;
import static org.springframework.context.i18n.LocaleContextHolder.getLocale;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_TAG;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CREATED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CREATED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ENVIRONMENT_TAGS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ENVIRONMENT_VARIABLES;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITERATION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_MODIFIED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAUNCHED_FROM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SUITE_ID;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.SelectHavingStep;
import org.jooq.SelectSelectStep;
import org.jooq.impl.DSL;
import org.springframework.context.MessageSource;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

public abstract class AbstractAutomatedSuiteGrid extends AbstractGrid {

    private static final String AUTOMATED_EXECUTION_EXTENDER_TYPE =
            DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER.name();

    protected final MessageSource messageSource;

    protected AbstractAutomatedSuiteGrid(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(SUITE_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return CAMPAIGN_LIBRARY_NODE.PROJECT_ID;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(CREATED_ON).coerce(Timestamp.class)),
                new GridColumn(DSL.field(CREATED_BY)),
                new GridColumn(DSL.field(EXECUTION_STATUS)),
                new GridColumn(DSL.field(LAST_MODIFIED_ON).coerce(Timestamp.class)),
                new GridColumn(DSL.field(SUITE_ID)),
                new GridColumn(DSL.field(ENVIRONMENT_VARIABLES)),
                new GridColumn(DSL.field(ENVIRONMENT_TAGS)),
                new GridColumn(DSL.field(LAUNCHED_FROM)),
                new GridColumn(DSL.field(PROJECT_ID)));
    }

    protected SelectSelectStep<
                    Record10<
                            Timestamp, String, String, Timestamp, String, String, String, Long, String, Long>>
            getSelectFields() {
        return DSL.select(
                AUTOMATED_SUITE.CREATED_ON.as(CREATED_ON),
                AUTOMATED_SUITE.CREATED_BY.as(CREATED_BY),
                AUTOMATED_SUITE.EXECUTION_STATUS.as(EXECUTION_STATUS),
                AUTOMATED_SUITE.LAST_MODIFIED_ON.as(LAST_MODIFIED_ON),
                AUTOMATED_SUITE.SUITE_ID.as(SUITE_ID),
                DSL.field(getDenormalizedEnvironmentVariables())
                        .coerce(String.class)
                        .as(ENVIRONMENT_VARIABLES),
                DSL.field(getDenormalizedEnvironmentTags()).coerce(String.class).as(ENVIRONMENT_TAGS),
                AUTOMATED_SUITE.ITERATION_ID.as(ITERATION_ID),
                DSL.when(
                                AUTOMATED_SUITE.ITERATION_ID.isNotNull(),
                                this.messageSource.getMessage("iteration.header.title", null, getLocale()))
                        .otherwise(
                                this.messageSource.getMessage("test-suite.launcher.title", null, getLocale()))
                        .as(LAUNCHED_FROM),
                CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID));
    }

    public SelectHavingStep<Record1<String>> getDenormalizedEnvironmentVariables() {
        return DSL.select(
                        DSL.groupConcat(
                                        concat(
                                                DENORMALIZED_ENVIRONMENT_VARIABLE.NAME,
                                                DSL.val(" : "),
                                                DENORMALIZED_ENVIRONMENT_VARIABLE.VALUE))
                                .separator(", ")
                                .as(ENVIRONMENT_VARIABLES))
                .from(DENORMALIZED_ENVIRONMENT_VARIABLE)
                .join(AUTOMATED_EXECUTION_EXTENDER)
                .on(
                        DENORMALIZED_ENVIRONMENT_VARIABLE.HOLDER_ID.eq(
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID))
                .where(
                        DENORMALIZED_ENVIRONMENT_VARIABLE
                                .HOLDER_TYPE
                                .eq(AUTOMATED_EXECUTION_EXTENDER_TYPE)
                                .and(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.eq(AUTOMATED_SUITE.SUITE_ID)));
    }

    public SelectHavingStep<Record1<String>> getDenormalizedEnvironmentTags() {

        return DSL.select(
                        substring(
                                        concat(
                                                coalesce(
                                                        concat(
                                                                inline(", "),
                                                                groupConcatDistinct(DENORMALIZED_ENVIRONMENT_TAG.VALUE)
                                                                        .separator(", ")),
                                                        ""),
                                                coalesce(
                                                        concat(
                                                                inline(", "),
                                                                groupConcatDistinct(AUTOMATED_EXECUTION_EXTENDER.TEST_TECHNOLOGY)
                                                                        .separator(", ")),
                                                        "")),
                                        3 // Skip the first 2 characters, which are ", "
                                        )
                                .as(ENVIRONMENT_TAGS))
                .from(AUTOMATED_EXECUTION_EXTENDER)
                .leftJoin(DENORMALIZED_ENVIRONMENT_TAG)
                .on(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.eq(DENORMALIZED_ENVIRONMENT_TAG.HOLDER_ID))
                .and(DENORMALIZED_ENVIRONMENT_TAG.HOLDER_TYPE.eq(AUTOMATED_EXECUTION_EXTENDER_TYPE))
                .where(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.eq(AUTOMATED_SUITE.SUITE_ID));
    }
}
