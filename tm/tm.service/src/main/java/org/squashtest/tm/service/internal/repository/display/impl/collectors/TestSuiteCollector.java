/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Component
public class TestSuiteCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

    public TestSuiteCollector(
            DSLContext dsl,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            ActiveMilestoneHolder activeMilestoneHolder,
            MilestoneDisplayDao milestoneDisplayDao) {
        super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
    }

    @Override
    protected Map<Long, DataRow> doCollect(List<Long> ids) {
        Map<Long, DataRow> testSuites = collectTestSuites(ids);
        appendMilestonesInheritedFromCampaign(testSuites);
        return testSuites;
    }

    private Map<Long, DataRow> collectTestSuites(List<Long> ids) {
        return dsl
                .select(
                        // @formatter:off
                        TEST_SUITE.ID,
                        TEST_SUITE.NAME,
                        TEST_SUITE.EXECUTION_STATUS,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS),
                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(CAMPAIGN_ID_ALIAS))
                .from(TEST_SUITE)
                .leftJoin(ITERATION_TEST_SUITE)
                .on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                .leftJoin(CAMPAIGN_ITERATION)
                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .leftJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(TEST_SUITE.ID.in(ids))
                // @formatter:on
                .fetch()
                .stream()
                .collect(
                        Collectors.toMap(
                                tuple -> tuple.get(TEST_SUITE.ID),
                                tuple -> {
                                    DataRow dataRow = new DataRow();
                                    dataRow.setId(
                                            new NodeReference(NodeType.TEST_SUITE, tuple.get(TEST_SUITE.ID)).toNodeId());
                                    dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
                                    dataRow.setData(tuple.intoMap());
                                    return dataRow;
                                }));
    }

    @Override
    public NodeType getHandledEntityType() {
        return NodeType.TEST_SUITE;
    }
}
