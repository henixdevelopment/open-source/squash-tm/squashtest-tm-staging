/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.library.structures.LibraryGraph;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.deletion.LinkedToIterationPreviewReport;
import org.squashtest.tm.service.deletion.NotDeletablePreviewReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.campaign.LockedTestCaseNodeDetectionService;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.TestCaseDeletionDao;
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Service
public class LockedTestCaseNodeDetectionServiceImpl extends AbstractLockedNodeDetectionService
        implements LockedTestCaseNodeDetectionService {
    private static final String TEST_CASES_TYPE = "test-cases";

    private final TestCaseDeletionDao deletionDao;
    private final TestCaseDao testCaseDao;
    private final TestCaseCallTreeFinder calltreeFinder;

    @Autowired
    public LockedTestCaseNodeDetectionServiceImpl(
            ActiveMilestoneHolder activeMilestoneHolder,
            TestCaseDeletionDao deletionDao,
            TestCaseDao testCaseDao,
            TestCaseCallTreeFinder calltreeFinder) {
        super(activeMilestoneHolder);
        this.deletionDao = deletionDao;
        this.testCaseDao = testCaseDao;
        this.calltreeFinder = calltreeFinder;
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedByCallSteps(
            List<Long> nodeIds, List<SuppressionPreviewReport> testCaseReports) {
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        List<Long> candidateIds = new ArrayList<>(nodeIds);
        // remove nodes locked by milestones
        candidateIds.removeAll(getLockedByMilestoneRule(testCaseReports));

        // compute the graph of called test cases
        LibraryGraph<NamedReference, LibraryGraph.SimpleNode<NamedReference>> calltree =
                calltreeFinder.getCallerGraph(candidateIds);

        LockedFileInferenceGraph graph = new LockedFileInferenceGraph();
        graph.init(calltree);
        graph.setCandidatesToDeletion(candidateIds);
        graph.resolveLockedFiles();

        // when nonDeletableData is not empty, some of those nodes belongs to
        // the deletion request itself
        // and the other ones are those that still need to be deleted.

        if (graph.hasLockedFiles()) {
            NotDeletablePreviewReport report = new NotDeletablePreviewReport();
            graph.collectLockedCandidates().forEach(node -> report.addName(node.getName()));
            graph.collectLockers().forEach(node -> report.addWhy(node.getName()));
            graph.collectLockedCandidates().forEach(node -> report.addLockedNode(node.getKey().getId()));
            reportList.add(report);
        }
        return reportList;
    }

    private List<Long> getLockedByMilestoneRule(List<SuppressionPreviewReport> testCaseReports) {
        return testCaseReports.stream()
                .filter(SuppressionPreviewReport::isMilestoneReport)
                .flatMap(report -> report.getLockedNodes().stream())
                .toList();
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedByExecutedTestCases(List<Long> nodeIds) {
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        List<TestCase> linkedNodes = testCaseDao.findAllLinkedToIteration(nodeIds);
        if (!linkedNodes.isEmpty()) {
            LinkedToIterationPreviewReport report = new LinkedToIterationPreviewReport();
            linkedNodes.forEach(node -> report.addName(node.getName()));
            reportList.add(report);
        }
        return reportList;
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedByMilestone(List<Long> nodeIds) {
        return super.detectLockedByMilestone(nodeIds, TEST_CASES_TYPE);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedWithActiveMilestone(List<Long> nodeIds) {
        return super.detectLockedWithActiveMilestone(nodeIds, TEST_CASES_TYPE);
    }

    @Override
    protected List<Long>[] getIdsSeparateFolderFromNodeIds(List<Long> nodeIds) {
        return deletionDao.separateFolderFromTestCaseIds(nodeIds);
    }

    @Override
    protected List<Long> findNodeIdsHavingMultipleMilestones(List<Long> nodeIds) {
        return testCaseDao.findNodeIdsHavingMultipleMilestones(nodeIds);
    }

    protected List<Long> findNodesWhichMilestonesForbidsDeletion(List<Long> nodeIds) {
        return deletionDao.findTestCasesWhichMilestonesForbidsDeletion(nodeIds);
    }

    @Override
    protected void addAdditionalReportWithActiveMilestoneToReportList(
            List<Long> nodeIds,
            Long activeMilestoneId,
            List<SuppressionPreviewReport> reportList,
            String reportType) {
        List<Long> nonBoundNodes = testCaseDao.findNonBoundTestCases(nodeIds, activeMilestoneId);
        addNonBoundNodesWithActiveMilestoneToReportList(nonBoundNodes, reportList, reportType);
    }
}
