/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter;

import org.squashtest.tm.api.plugin.UsedInPlugin;

public final class JsonImportField {
    public static final String ACTION = "action";
    public static final String ACTION_STEPS = "action_steps";
    public static final String ACTION_WORD = "action_word";
    public static final String ACTUAL_END_DATE_AUTO = "actual_end_date_auto";
    public static final String ACTUAL_END_DATE = "actual_end_date";
    public static final String ACTUAL_START_DATE_AUTO = "actual_start_date_auto";
    public static final String ACTUAL_START_DATE = "actual_start_date";
    public static final String ATTACHMENTS = "attachments";
    public static final String BOUND_ENTITIES = "bound_entities";
    public static final String CALLED_ID = "called_id";
    public static final String CALLER_ID = "caller_id";
    public static final String CALLED_TEST_CASES = "called_test_cases";
    public static final String CAMPAIGNS = "campaigns";
    public static final String CAMPAIGN_FOLDERS = "campaign_folders";
    public static final String CATEGORY = "category";
    public static final String CHARTER = "charter";

    @UsedInPlugin("rest-api-admin")
    public static final String CODE = "code";

    public static final String COLOR = "color";
    public static final String COMMENT = "comment";
    public static final String CRITICALITY = "criticality";
    public static final String CUSTOM_FIELDS = "custom_fields";

    public static final String DATASET_ID = "dataset_id";
    public static final String DATASET_PARAMS = "dataset_params";
    public static final String DATA_TABLE = "data_table";
    public static final String DATASETS = "datasets";
    public static final String DEFAULT_VALUE = "default_value";
    public static final String DESCRIPTION = "description";
    public static final String DOC_STRING = "doc_string";
    public static final String EXECUTION_STEPS = "execution_steps";
    public static final String EXECUTIONS = "executions";
    public static final String EXPECTED_RESULT = "expected_result";
    public static final String ID = "id";
    public static final String IMPORTANCE = "importance";
    public static final String INDEX = "index";
    public static final String INPUT_TYPE = "input_type";
    public static final String ITERATION_ID = "iteration_id";
    public static final String ITERATIONS = "iterations";
    public static final String KEYWORD = "keyword";
    public static final String KEYWORD_STEPS = "keyword_steps";
    public static final String LABEL = "label";
    public static final String LAST_EXECUTED_BY = "last_executed_by";
    public static final String LAST_EXECUTED_ON = "last_executed_on";
    public static final String NAME = "name";
    public static final String NATURE = "nature";
    public static final String OPTIONAL = "optional";
    public static final String OPTIONS = "options";
    public static final String ORIGINAL_FILE_NAME = "original_file_name";
    public static final String PARAM_ID = "param_id";
    public static final String PARAM_VALUES = "param_values";
    public static final String PARAMETER_ASSIGNATION_MODE = "parameter_assignation_mode";
    public static final String PARENT_ID = "parent_id";
    public static final String PARENT_TYPE = "parent_type";
    public static final String PREREQUISITE = "prerequisite";
    public static final String PROJECT = "project";
    public static final String REFERENCE = "reference";
    public static final String REQUIREMENTS = "requirements";
    public static final String REQUIREMENT_FOLDERS = "requirement_folders";
    public static final String SCHEDULED_END_DATE = "scheduled_end_date";
    public static final String SCHEDULED_START_DATE = "scheduled_start_date";
    public static final String SESSION_DURATION = "session_duration";
    public static final String STATUS = "status";
    public static final String SCRIPT = "script";
    public static final String TEST_CASE_FOLDERS = "test_case_folders";
    public static final String TEST_CASE_ID = "test_case_id";
    public static final String TEST_CASE_KIND = "test_case_kind";
    public static final String TEST_CASES = "test_cases";

    public static final String TEST_PLAN_TEST_CASE_IDS = "test_plan_test_case_ids";
    public static final String TEST_STEP_ID = "test_step_id";
    public static final String TEST_SUITE_ID = "test_suite_id";
    public static final String TEST_SUITES = "test_suites";
    public static final String TYPE = "type";
    public static final String VALUE = "value";
    public static final String VERIFIED_REQUIREMENTS = "verified_requirements";
    public static final String ZIP_IMPORT_FILE_NAME = "zip_import_file_name";

    private JsonImportField() {
        throw new IllegalStateException("Utility class");
    }
}
