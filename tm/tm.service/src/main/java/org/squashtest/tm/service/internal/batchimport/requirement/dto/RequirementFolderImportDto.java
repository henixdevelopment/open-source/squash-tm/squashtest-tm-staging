/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.tree.ImportedRequirementNode;
import org.squashtest.tm.service.internal.library.LibraryUtils;

public class RequirementFolderImportDto {

    private final RequirementFolder root;

    private final List<RequirementFolder> generatedFolders = new ArrayList<>();

    private final List<RequirementVersionImportDto> generatedVersions = new ArrayList<>();

    public RequirementFolderImportDto(RequirementFolder root) {
        this.root = root;
    }

    public static RequirementFolderImportDto fromRequirementNode(
            ImportedRequirementNode node,
            Map<RequirementTarget, List<RequirementVersionImportDto>> requirementTargetVersions,
            Project project) {

        RequirementFolder folder = createFolder(node);
        folder.notifyAssociatedWithProject(project);

        RequirementFolderImportDto root = new RequirementFolderImportDto(folder);

        addChildren(node, requirementTargetVersions, root, folder);

        return root;
    }

    private static RequirementFolder createFolder(ImportedRequirementNode node) {
        RequirementFolder folder = new RequirementFolder();
        folder.setName(node.getName());
        folder.setDescription("");
        return folder;
    }

    private static void addFolderChild(
            ImportedRequirementNode current,
            RequirementFolder parent,
            List<String> parentContentNames,
            Map<RequirementTarget, List<RequirementVersionImportDto>> requirementTargetVersions,
            RequirementFolderImportDto root) {
        RequirementFolder folder = createFolder(current);

        LibraryUtils.fixConflictNames(parentContentNames, folder);

        parent.addContent(folder);

        addChildren(current, requirementTargetVersions, root, folder);

        root.addGeneratedFolder(folder);
    }

    private static void addChildren(
            ImportedRequirementNode current,
            Map<RequirementTarget, List<RequirementVersionImportDto>> requirementTargetVersions,
            RequirementFolderImportDto root,
            RequirementFolder folder) {
        if (current.getContents().isEmpty()) {
            return;
        }

        List<String> contentNames = new ArrayList<>();

        for (ImportedRequirementNode child : current.getContents()) {
            if (child.isRequirement()) {
                addRequirementChild(child, folder, contentNames, requirementTargetVersions, root);
            } else {
                addFolderChild(child, folder, contentNames, requirementTargetVersions, root);
            }
        }
    }

    private static void addRequirementChild(
            ImportedRequirementNode current,
            RequirementFolder parentFolder,
            List<String> parentContentNames,
            Map<RequirementTarget, List<RequirementVersionImportDto>> requirementTargetVersions,
            RequirementFolderImportDto root) {
        RequirementImportDto requirementDto =
                RequirementImportDto.fromRequirementNode(
                        current, requirementTargetVersions, parentFolder.getProject());

        LibraryUtils.fixConflictNames(parentContentNames, requirementDto.getRequirement());

        parentFolder.addContent(requirementDto.getRequirement());

        root.addGeneratedVersions(requirementDto.getGeneratedVersions());
    }

    private void addGeneratedVersions(List<RequirementVersionImportDto> versions) {
        generatedVersions.addAll(versions);
    }

    private void addGeneratedFolder(RequirementFolder folder) {
        generatedFolders.add(folder);
    }

    public RequirementFolder getFolder() {
        return root;
    }

    public List<RequirementFolder> getGeneratedFolders() {
        return generatedFolders;
    }

    public List<RequirementVersionImportDto> getGeneratedVersions() {
        return generatedVersions;
    }
}
