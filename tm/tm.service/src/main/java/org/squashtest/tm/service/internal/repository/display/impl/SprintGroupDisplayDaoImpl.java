/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_GROUP;

import java.util.List;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.sprintgroup.SprintGroupDto;
import org.squashtest.tm.service.internal.repository.display.SprintGroupDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class SprintGroupDisplayDaoImpl implements SprintGroupDisplayDao {
    private final DSLContext dsl;

    public SprintGroupDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public SprintGroupDto getSprintGroupDtoById(long sprintGroupId) {
        return dsl.select(
                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID),
                        CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID,
                        CAMPAIGN_LIBRARY_NODE.DESCRIPTION,
                        CAMPAIGN_LIBRARY_NODE.NAME,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
                        CAMPAIGN_LIBRARY_NODE.CREATED_BY,
                        CAMPAIGN_LIBRARY_NODE.CREATED_ON,
                        CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_BY,
                        CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_ON)
                .from(CAMPAIGN_LIBRARY_NODE)
                .innerJoin(SPRINT_GROUP)
                .on(SPRINT_GROUP.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(sprintGroupId))
                .fetchOneInto(SprintGroupDto.class);
    }

    @Override
    public List<Long> findContainedSprintGroupIds(List<Long> campaignLibraryNodeIds) {
        return dsl.select(SPRINT_GROUP.CLN_ID)
                .from(SPRINT_GROUP)
                .leftJoin(CLN_RELATIONSHIP_CLOSURE)
                .on(SPRINT_GROUP.CLN_ID.eq(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .where(
                        CLN_RELATIONSHIP_CLOSURE
                                .ANCESTOR_ID
                                .in(campaignLibraryNodeIds)
                                .or(SPRINT_GROUP.CLN_ID.in(campaignLibraryNodeIds)))
                .fetchInto(Long.class);
    }

    @Override
    public Long findParentSprintGroupId(long nodeId) {
        return dsl.select(SPRINT_GROUP.CLN_ID)
                .from(SPRINT_GROUP)
                .join(CLN_RELATIONSHIP_CLOSURE)
                .on(SPRINT_GROUP.CLN_ID.eq(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
                .where(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(nodeId))
                .fetchOneInto(Long.class);
    }
}
