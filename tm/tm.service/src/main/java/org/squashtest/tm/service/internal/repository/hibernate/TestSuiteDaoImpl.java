/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENTS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENT_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CONTENT;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_PLAN;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_TESTSUITE_BY_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.squashtest.tm.domain.campaign.TestPlanStatistics;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;
import org.squashtest.tm.service.internal.repository.CustomTestSuiteDao;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;
import org.squashtest.tm.service.statistics.CountOnEnum;

public class TestSuiteDaoImpl implements CustomTestSuiteDao {
    private static final String TEST_SUITE_COUNT_STATUS = "TestSuite.countStatuses";
    private static final String TEST_SUITE_IDS = "testSuiteIds";

    @PersistenceContext private EntityManager entityManager;

    @Override
    public TestPlanStatistics getTestSuiteStatistics(final long testSuiteId) {

        Query q = entityManager.createNamedQuery(TEST_SUITE_COUNT_STATUS);
        q.setParameter("id", testSuiteId);
        List<Object[]> result = q.getResultList();

        LinkedHashMap<ExecutionStatus, Integer> canonicalStatusMap =
                CountOnEnum.fromTuples(result, ExecutionStatus.class)
                        .getStatistics(
                                ExecutionStatus::getCanonicalStatus, ExecutionStatus.getCanonicalStatusSet());

        return new TestPlanStatistics(canonicalStatusMap);
    }

    @Override
    public Map<Long, ExecutionStatusReport> getStatusReport(List<Long> ids) {
        Map<Long, ExecutionStatusReport> reportsMap = new HashMap<>();

        Query query = entityManager.createNamedQuery("TestSuite.getReportsByIds");
        query.setParameter("ids", ids);

        List<Object[]> tuples = query.getResultList();

        if (tuples.isEmpty()) {
            ids.stream().forEach(id -> reportsMap.put(id, new ExecutionStatusReport()));
        } else {
            for (Object[] tuple : tuples) {
                Long testId = (Long) tuple[0];
                ExecutionStatus execution = (ExecutionStatus) tuple[1];
                Long count = (Long) tuple[2];

                ExecutionStatusReport report =
                        reportsMap.computeIfAbsent(testId, k -> new ExecutionStatusReport());
                report.set(execution.getCanonicalStatus(), count.intValue());
            }
        }
        return reportsMap;
    }

    // ************************ utils ********************

    @Override
    public List<Long> findAllIdsByExecutionIds(List<Long> executionIds) {
        if (executionIds.isEmpty()) {
            return new ArrayList<>();
        }
        Query query = entityManager.createNamedQuery("TestSuite.findAllIdsByExecutionIds");
        query.setParameter("executionIds", executionIds);
        return query.getResultList();
    }

    @Override
    public List<TestSuite> findAllByIds(Collection<Long> suiteIds) {
        if (suiteIds.isEmpty()) {
            return new ArrayList<>();
        }
        Query query = entityManager.createNamedQuery("TestSuite.findAllByIds");
        query.setParameter("suiteIds", suiteIds);
        return query.getResultList();
    }

    @Override
    public List<TestSuite> findTestSuitesWhereMilestoneIsNotLocked(Collection<Long> suiteIds) {
        if (suiteIds.isEmpty()) {
            return new ArrayList<>();
        }
        Query query =
                entityManager.createNamedQuery("TestSuite.findTestSuitesWhereMilestoneIsNoteLocked");
        query.setParameter("suiteIds", suiteIds);
        return query.getResultList();
    }

    @Override
    public List<Long> findTestSuiteAttachmentListIds(Collection<Long> testSuiteIds) {
        if (!testSuiteIds.isEmpty()) {
            Query query = entityManager.createNamedQuery("TestSuite.findAllAttachmentLists");
            query.setParameter(TEST_SUITE_IDS, testSuiteIds);
            return query.getResultList();
        }
        return Collections.emptyList();
    }

    @Override
    public List<String> findTestSuitesAutomatedSuiteIds(Collection<Long> testSuiteIds) {
        if (!testSuiteIds.isEmpty()) {
            Query query = entityManager.createNamedQuery("TestSuite.findTestSuitesAutomatedSuiteIds");
            query.setParameter(TEST_SUITE_IDS, testSuiteIds);
            return query.getResultList();
        }
        return Collections.emptyList();
    }

    @Override
    public void removeTestSuites(Collection<Long> testSuiteIds) {

        Query query = entityManager.createNamedQuery("TestSuite.removeAll");
        query.setParameter(TEST_SUITE_IDS, testSuiteIds);
        query.executeUpdate();
    }

    @Override
    public TestSuite findByUUID(String targetUUID) {
        return entityManager
                .createQuery("SELECT ts FROM TestSuite ts WHERE ts.uuid = :uuid", TestSuite.class)
                .setParameter("uuid", targetUUID)
                .getSingleResult();
    }

    @Override
    public List<TestSuite> loadNodeForPaste(Collection<Long> testSuiteIds) {
        String testPlanSession =
                EntityGraphQueryBuilder.buildParentAttribute(TEST_PLAN, EXPLORATORY_SESSION_OVERVIEW);
        String testPlanSessionAttachmentList =
                EntityGraphQueryBuilder.buildParentAttribute(testPlanSession, ATTACHMENT_LIST);

        return new EntityGraphQueryBuilder<>(
                        entityManager, TestSuite.class, FIND_DISTINCT_TESTSUITE_BY_IDS)
                .addAttributeNodes(ATTACHMENT_LIST, TEST_PLAN)
                .addSubGraph(ATTACHMENT_LIST, ATTACHMENTS)
                .addSubgraphToSubgraph(ATTACHMENT_LIST, ATTACHMENTS, CONTENT)
                .addSubGraph(TEST_PLAN, EXPLORATORY_SESSION_OVERVIEW)
                .addSubgraphToSubgraph(TEST_PLAN, EXPLORATORY_SESSION_OVERVIEW, ATTACHMENT_LIST)
                .addSubgraphToSubgraph(testPlanSession, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubgraphToSubgraph(testPlanSessionAttachmentList, ATTACHMENTS, CONTENT)
                .executeDistinctList(Map.of(IDS, testSuiteIds));
    }

    @Override
    public boolean hasDeletedTestCaseInTestPlan(long testSuiteId) {
        return entityManager.createQuery("""
                 select count(t) > 0
                 from TestSuite t
                 join t.testPlan tp
                 where t.id = :testSuiteId and tp.referencedTestCase is null""", Boolean.class)
            .setParameter("testSuiteId", testSuiteId)
            .getSingleResult();
    }

    @Override
    public TestSuite loadForExecutionResume(long testSuiteId) {
        return entityManager.createQuery("""
                 select ts
                 from TestSuite ts
                 left join fetch ts.testPlan tp
                 left join fetch tp.referencedTestCase tc
                 left join fetch tc.steps
                 left join fetch tp.exploratorySessionOverview
                 left join fetch tp.executions exec
                 left join fetch exec.steps
                 left join fetch exec.automatedExecutionExtender
                 where ts.id = :id""", TestSuite.class)
            .setParameter(ID, testSuiteId)
            .getSingleResult();
    }
}
