/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customreport;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_CUSTOM_REPORT_LIBRARY_NODE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.customreport.CustomReportFolder;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition;
import org.squashtest.tm.domain.customreport.CustomReportTreeEntity;
import org.squashtest.tm.domain.customreport.CustomReportTreeLibraryNode;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.domain.tree.TreeEntity;
import org.squashtest.tm.domain.tree.TreeLibraryNode;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.repository.CustomReportLibraryNodeDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

@Service("org.squashtest.tm.service.customreport.CustomReportLibraryNodeService")
@Transactional
public class CustomReportLibraryNodeServiceImpl implements CustomReportLibraryNodeService {

    @Inject protected PermissionEvaluationService permissionService;

    @Inject private CustomReportLibraryNodeDao customReportLibraryNodeDao;

    @PersistenceContext private EntityManager em;

    @Inject private CRLNDeletionHandler deletionHandler;

    @Inject private CRLNCopier nodeCopier;

    @Inject private CRLNMover nodeMover;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Override
    public CustomReportLibraryNode findCustomReportLibraryNodeById(Long id) {
        return customReportLibraryNodeDao.getReferenceById(id);
    }

    @Override
    @PreAuthorize(READ_CUSTOM_REPORT_LIBRARY_NODE + OR_HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public CustomReportLibrary findLibraryByTreeNodeId(Long treeNodeId) {
        TreeEntity entity = findEntityAndCheckType(treeNodeId, CustomReportTreeDefinition.LIBRARY);
        return (CustomReportLibrary) entity; // NOSONAR cast is checked by findEntityAndCheckType method
    }

    @Override
    @PreAuthorize(READ_CUSTOM_REPORT_LIBRARY_NODE + OR_HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public CustomReportFolder findFolderByTreeNodeId(Long treeNodeId) {
        TreeEntity entity = findEntityAndCheckType(treeNodeId, CustomReportTreeDefinition.FOLDER);
        return (CustomReportFolder) entity; // NOSONAR cast is checked by findEntityAndCheckType method
    }

    @Override
    @PreAuthorize(READ_CUSTOM_REPORT_LIBRARY_NODE + OR_HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public ChartDefinition findChartDefinitionByNodeId(Long treeNodeId) {
        TreeEntity entity = findEntityAndCheckType(treeNodeId, CustomReportTreeDefinition.CHART);
        return (ChartDefinition) entity; // NOSONAR cast is checked by findEntityAndCheckType method
    }

    @Override
    @PreAuthorize(READ_CUSTOM_REPORT_LIBRARY_NODE + OR_HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public ReportDefinition findReportDefinitionByNodeId(Long treeNodeId) {
        TreeEntity entity = findEntityAndCheckType(treeNodeId, CustomReportTreeDefinition.REPORT);
        return (ReportDefinition) entity; // NOSONAR cast is checked by findEntityAndCheckType method
    }

    @Override
    public CustomReportCustomExport findCustomExportByNodeId(Long treeNodeId) {
        TreeEntity entity =
                findEntityAndCheckType(treeNodeId, CustomReportTreeDefinition.CUSTOM_EXPORT);
        return (CustomReportCustomExport) entity;
    }

    @Override
    @PreAuthorize(READ_CUSTOM_REPORT_LIBRARY_NODE + OR_HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public CustomReportDashboard findCustomReportDashboardById(Long treeNodeId) {
        TreeEntity entity = findEntityAndCheckType(treeNodeId, CustomReportTreeDefinition.DASHBOARD);
        return (CustomReportDashboard)
                entity; // NOSONAR cast is checked by findEntityAndCheckType method
    }

    @Override
    @PreAuthorize(
            "hasPermission(#parentId,'org.squashtest.tm.domain.customreport.CustomReportLibraryNode' ,'CREATE') "
                    + OR_HAS_ROLE_ADMIN)
    public CustomReportLibraryNode createNewNode(Long parentId, CustomReportTreeEntity entity) {
        CustomReportLibraryNode parentNode = customReportLibraryNodeDao.getReferenceById(parentId);

        CustomReportLibraryNode newNode =
                new CustomReportLibraryNodeBuilder(parentNode, entity).build();
        customReportLibraryNodeDao.save(newNode);
        em.flush();
        em.clear(); // needed to force hibernate to reload the persisted entities...

        return customReportLibraryNodeDao.getReferenceById(newNode.getId());
    }

    @Override
    public List<SuppressionPreviewReport> simulateDeletion(List<Long> nodeIds) {
        return deletionHandler.simulateDeletion(nodeIds);
    }

    @Override
    public OperationReport delete(List<Long> nodeIds) {
        PermissionsUtils.checkPermission(
                permissionService,
                nodeIds,
                Permissions.DELETE.name(),
                CustomReportLibraryNode.class.getName());

        return deletionHandler.deleteNodes(nodeIds);
    }

    @Override
    public List<Long> findDescendantIds(List<Long> nodeIds) {
        return customReportLibraryNodeDao.findAllDescendantIds(nodeIds);
    }

    @Override
    public List<Long> findAllFirstLevelDescendantIds(List<Long> nodeIds) {
        return customReportLibraryNodeDao.findAllFirstLevelDescendantIds(nodeIds);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#nodeId, 'org.squashtest.tm.domain.customreport.CustomReportLibraryNode' ,'WRITE') "
                    + OR_HAS_ROLE_ADMIN)
    public void renameNode(Long nodeId, String newName) throws DuplicateNameException {
        CustomReportLibraryNode crln = customReportLibraryNodeDao.getReferenceById(nodeId);
        crln.renameNode(newName);
    }

    @Override
    public void renameNode(
            Long entityId, String newName, Class<? extends CustomReportTreeEntity> entityClass)
            throws DuplicateNameException {
        CustomReportTreeEntity treeEntity = em.find(entityClass, entityId);
        CustomReportLibraryNode crln = customReportLibraryNodeDao.findNodeFromEntity(treeEntity);
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                Collections.singletonList(crln.getId()),
                "WRITE",
                CustomReportLibraryNode.class.getName());
        crln.renameNode(newName);
    }

    @Override
    public List<Long> findAncestorIds(Long nodeId) {
        return customReportLibraryNodeDao.findAncestorIds(nodeId);
    }

    @Override
    public List<Object[]> findAncestor(Long nodeId) {
        return customReportLibraryNodeDao.findAncestor(nodeId);
    }

    @Override
    public CustomReportLibraryNode findNodeFromEntity(CustomReportTreeEntity treeEntity) {
        return customReportLibraryNodeDao.findNodeFromEntity(treeEntity);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#target, 'org.squashtest.tm.domain.customreport.CustomReportLibraryNode' ,'WRITE') "
                    + OR_HAS_ROLE_ADMIN)
    public List<TreeLibraryNode> copyNodes(
            List<CustomReportLibraryNode> nodes,
            CustomReportLibraryNode target,
            ClipboardPayload clipboardPayload) {
        return makeCopy(nodes, target, clipboardPayload);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#targetId, 'org.squashtest.tm.domain.customreport.CustomReportLibraryNode' ,'CREATE') "
                    + OR_HAS_ROLE_ADMIN)
    public List<TreeLibraryNode> copyNodes(
            List<Long> nodeIds, Long targetId, ClipboardPayload clipboardPayload) {
        List<CustomReportLibraryNode> nodes = customReportLibraryNodeDao.findAllById(nodeIds);
        CustomReportLibraryNode target = customReportLibraryNodeDao.getReferenceById(targetId);
        return makeCopy(nodes, target, clipboardPayload);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#targetId, 'org.squashtest.tm.domain.customreport.CustomReportLibraryNode' ,'CREATE') "
                    + OR_HAS_ROLE_ADMIN)
    public void moveNodes(List<Long> nodeIds, Long targetId) {
        List<CustomReportLibraryNode> nodes = customReportLibraryNodeDao.findAllById(nodeIds);
        CustomReportLibraryNode target = customReportLibraryNodeDao.getReferenceById(targetId);
        nodeMover.moveNodes(nodes, target);
    }

    // --------------- PRIVATE METHODS --------------

    private TreeEntity findEntityAndCheckType(Long nodeId, CustomReportTreeDefinition entityDef) {
        CustomReportTreeLibraryNode node = findCustomReportLibraryNodeById(nodeId);

        if (node == null || node.getEntityType() != entityDef) {
            String message = "the node for given id %d doesn't exist or doesn't represent a %s entity";
            throw new IllegalArgumentException(String.format(message, nodeId, entityDef.getTypeName()));
        }

        TreeEntity entity = node.getEntity();

        if (entity == null) {
            String message = "the node for given id %d represent a null entity";
            throw new IllegalArgumentException(String.format(message, nodeId));
        }
        return entity;
    }

    private List<TreeLibraryNode> makeCopy(
            List<CustomReportLibraryNode> nodes,
            CustomReportLibraryNode target,
            ClipboardPayload clipboardPayload) {
        List<TreeLibraryNode> copies = new ArrayList<>();
        copies.addAll(nodeCopier.copyNodes(nodes, target, clipboardPayload));
        return copies;
    }
}
