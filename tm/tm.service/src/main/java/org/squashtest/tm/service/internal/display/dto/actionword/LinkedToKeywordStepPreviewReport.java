/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.actionword;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.springframework.context.MessageSource;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;

public class LinkedToKeywordStepPreviewReport implements SuppressionPreviewReport {

    private static final String NODE_DELETION_MESSAGE_KEY_FIRST =
            "actionword.workspace.message.deletionWarning.first";
    private static final String NODE_DELETION_MESSAGE_KEY_SECOND =
            "actionword.workspace.message.deletionWarning.second";
    private static final String NODE_DELETION_MESSAGE_KEY_THIRD =
            "actionword.workspace.message.deletionWarning.third";
    private static final String SPACE_SEPARATOR = " ";
    private final String path;
    private final Integer count;
    private final Set<Long> lockedNodes = new HashSet<>();

    public LinkedToKeywordStepPreviewReport(String path, Integer count) {
        this.path = path;
        this.count = count;
    }

    @Override
    public String toString(MessageSource source, Locale locale) {
        StringBuilder builder = new StringBuilder();
        builder
                .append(source.getMessage(NODE_DELETION_MESSAGE_KEY_FIRST, null, locale))
                .append(SPACE_SEPARATOR)
                .append(path)
                .append(SPACE_SEPARATOR)
                .append(source.getMessage(NODE_DELETION_MESSAGE_KEY_SECOND, null, locale))
                .append(SPACE_SEPARATOR)
                .append(count)
                .append(SPACE_SEPARATOR)
                .append(source.getMessage(NODE_DELETION_MESSAGE_KEY_THIRD, null, locale));

        return builder.toString();
    }

    @Override
    public Set<Long> getLockedNodes() {
        return lockedNodes;
    }

    @Override
    public void addAllLockedNodes(List<Long> lockedNodes) {
        this.lockedNodes.addAll(lockedNodes);
    }

    @Override
    public void addLockedNode(Long lockedNode) {
        lockedNodes.add(lockedNode);
    }
}
