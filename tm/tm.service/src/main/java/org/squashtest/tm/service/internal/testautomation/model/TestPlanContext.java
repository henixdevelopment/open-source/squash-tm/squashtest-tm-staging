/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.squashtest.tm.api.plugin.EntityType;

public class TestPlanContext {
    private final EntityType entityType;
    private final String entityName;
    private final String entityPath;
    private final String entityUuid;
    private final String squashTMVersion;

    public TestPlanContext(
            EntityType entityType,
            String entityName,
            String path,
            String entityUuid,
            String squashTMVersion) {
        this.entityUuid = entityUuid;
        validate(entityType, entityName, path);
        this.entityName = entityName;
        this.entityType = entityType;
        this.entityPath = path;
        this.squashTMVersion = squashTMVersion;
    }

    private static void validate(EntityType entityType, String entityName, String path) {
        if (!entityType.equals(EntityType.ITERATION) && !entityType.equals(EntityType.TEST_SUITE)) {
            throw new IllegalArgumentException("Unsupported type " + entityType);
        }
        if (path == null || path.isBlank()) {
            throw new IllegalArgumentException("Path cannot be blank.");
        }
        if (entityName == null || entityName.isBlank()) {
            throw new IllegalArgumentException("Entity name cannot be blank.");
        }
    }

    public String getEntityTypeAsString() {
        return normalizeEntityType(entityType);
    }

    private String normalizeEntityType(EntityType entityType) {
        return entityType.name().toLowerCase().replace("_", " ");
    }

    public String getEntityPath() {
        return entityPath + " > " + entityName;
    }

    public List<String> getEntityPathAsList() {
        List<String> path = new ArrayList<String>(Arrays.asList(entityPath.split(" > ")));
        path.add(entityName);
        return path;
    }

    public String getEntityUuid() {
        return entityUuid;
    }

    public String getSquashTMVersion() {
        return squashTMVersion;
    }
}
