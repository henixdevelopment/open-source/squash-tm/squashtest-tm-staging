/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_STEPS_DETAILS_BY_TEST_CASE_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.POSITION;
import static org.squashtest.tm.service.internal.repository.ParameterNames.TEST_CASE_ID;

import java.util.Collection;
import java.util.List;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.service.internal.repository.TestStepDao;

@Repository
public class HibernateTestStepDao extends HibernateEntityDao<TestStep> implements TestStepDao {
    private static final String FIND_BY_TC_AND_POSITION = "testStep.findByTestCaseAndPosition";

    @Override
    public List<TestStep> findListById(final List<Long> testStepIds) {
        SetQueryParametersCallback callback = new TestStepIdsQueryParametersCallback(testStepIds);

        return executeListNamedQuery("testStep.findOrderedListById", callback);
    }

    @Override
    public int findPositionOfStep(Long testStepId) {
        Query query = currentSession().getNamedQuery("testStep.findPositionOfStep");
        query.setParameter("stepId", testStepId, LongType.INSTANCE);
        return (Integer) query.uniqueResult();
    }

    private static final class TestStepIdsQueryParametersCallback
            implements SetQueryParametersCallback {

        private List<Long> testStepIds;

        private TestStepIdsQueryParametersCallback(List<Long> testStepIds) {
            this.testStepIds = testStepIds;
        }

        @Override
        public void setQueryParameters(Query query) {
            query.setParameterList("testStepIds", testStepIds, LongType.INSTANCE);
        }
    }

    @Override
    public ActionTestStep findActionTestStepById(long testStepId) {
        return (ActionTestStep) currentSession().get(ActionTestStep.class, testStepId);
    }

    /**
     * @see HibernateTestStepDao#stringIsFoundInStepsOfTestCase(String, long)
     */
    @Override
    public boolean stringIsFoundInStepsOfTestCase(String stringToFind, long testCaseId) {
        Query query = currentSession().getNamedQuery("testStep.stringIsFoundInStepsOfTestCase");
        query.setParameter("testCaseId", testCaseId);
        query.setParameter("stringToFind", "%" + stringToFind + "%");
        return (Long) query.uniqueResult() > 0;
    }

    /**
     * @see HibernateTestStepDao#stringIsFoundInStepsOfKeywordTestCase(String, long)
     */
    @Override
    public boolean stringIsFoundInStepsOfKeywordTestCase(String stringToFind, long testCaseId) {
        Query query = currentSession().getNamedQuery("testStep.stringIsFoundInStepsOfKeywordTestCase");
        query.setParameter("testCaseId", testCaseId);
        query.setParameter("stringToFind", stringToFind);
        return (Long) query.uniqueResult() > 0;
    }

    @Override
    public List<TestStep> findByIdOrderedByIndex(List<Long> testStepIds) {
        Query query = currentSession().getNamedQuery("testStep.findByIdOrderedByIndex");
        query.setParameterList("testStepIds", testStepIds);
        return query.list();
    }

    @Override
    public TestCase findTestCaseFromActionTestStep(long testStepId) {
        Query query = currentSession().getNamedQuery("testStep.findTestCaseFromActionTestStep");
        query.setParameter("testStepId", testStepId, LongType.INSTANCE);
        return (TestCase) query.uniqueResult();
    }

    public List<Object[]> findTestStepsDetails(Collection<Long> testCaseIds) {
        return entityManager
                .createQuery(FIND_STEPS_DETAILS_BY_TEST_CASE_IDS, Object[].class)
                .setParameter(IDS, testCaseIds)
                .getResultList();
    }

    @Override
    public TestStep findByTestCaseAndPosition(Long testCaseId, Integer index) {
        return entityManager
                .createNamedQuery(FIND_BY_TC_AND_POSITION, TestStep.class)
                .setParameter(TEST_CASE_ID, testCaseId)
                .setParameter(POSITION, index)
                .getSingleResult();
    }
}
