/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;

public class DatasetToPivot {
    @JsonProperty(JsonImportField.ID)
    private String pivotId;

    @JsonProperty(JsonImportField.NAME)
    private String name;

    @JsonProperty(JsonImportField.PARAM_VALUES)
    private List<ParamValue> paramValues = new ArrayList<>();

    public String getPivotId() {
        return pivotId;
    }

    public void setPivotId(String testCasePivotId, Integer id) {
        this.pivotId = String.format("%s_%s%s", testCasePivotId, XrayField.BASE_PIVOT_ID_DATASET, id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isParametersPresent(Map<String, String> parameters) {
        if (parameters.size() != paramValues.size()) {
            return false;
        }
        return paramValues.stream()
                .allMatch(
                        paramValue ->
                                parameters.containsKey(paramValue.getName())
                                        && parameters.get(paramValue.getName()).equals(paramValue.getValue()));
    }

    public List<ParamValue> getParamValues() {
        return paramValues;
    }

    public void setParamValues(List<ParamValue> paramValues) {
        this.paramValues = paramValues;
    }

    public static class ParamValue {
        @JsonProperty(JsonImportField.PARAM_ID)
        private String id;

        @JsonProperty(JsonImportField.VALUE)
        private String value;

        @JsonIgnore private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
