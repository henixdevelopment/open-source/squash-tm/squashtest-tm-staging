/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.internal.dto.projectimporter.FolderToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.ProjectIdsReferences;
import org.squashtest.tm.service.internal.library.LibraryUtils;
import org.squashtest.tm.service.projectimporter.pivotimporter.AttachmentPivotImportService;
import org.squashtest.tm.service.projectimporter.pivotimporter.FolderPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.FolderParser;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;

@Service("FolderPivotImporterService")
public class FolderPivotImporterServiceImpl implements FolderPivotImporterService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(FolderPivotImporterServiceImpl.class);
    private final RequirementLibraryNavigationService requirementLibraryNavigationService;
    private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;
    private final CampaignLibraryNavigationService campaignLibraryNavigationService;
    private final FolderParser folderParser;

    private final AttachmentPivotImportService attachmentPivotImportService;
    @PersistenceContext private EntityManager entityManager;

    public FolderPivotImporterServiceImpl(
            RequirementLibraryNavigationService requirementLibraryNavigationService,
            TestCaseLibraryNavigationService testCaseLibraryNavigationService,
            CampaignLibraryNavigationService campaignLibraryNavigationService,
            FolderParser folderParser,
            AttachmentPivotImportService attachmentPivotImportService) {
        this.requirementLibraryNavigationService = requirementLibraryNavigationService;
        this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
        this.campaignLibraryNavigationService = campaignLibraryNavigationService;
        this.folderParser = folderParser;
        this.attachmentPivotImportService = attachmentPivotImportService;
    }

    @Override
    public void importFoldersByJsonFileName(
            ZipFile zipFile,
            JsonImportFile jsonFile,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {

        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER,
                zipFile.getName(),
                PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, true),
                pivotFormatImport);

        ZipEntry entry = zipFile.getEntry(jsonFile.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleFoldersInJsonFile(
                        jsonInputStream,
                        jsonFile,
                        projectIdsReferences,
                        pivotImportMetadata,
                        pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER,
                        PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, true),
                        pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER,
                        PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, true),
                        pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleFoldersInJsonFile(
            InputStream jsonInputStream,
            JsonImportFile jsonFile,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseFoldersArray(
                        jsonFile, projectIdsReferences, pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
    }

    private void parseFoldersArray(
            JsonImportFile jsonFile,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<FolderToImport> foldersToImport = new ArrayList<>();
        String jsonImportField = findJsonImportFieldForJsonFile(jsonFile);

        if (!jsonImportField.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                FolderToImport folderToImport =
                        folderParser.parseFolder(jsonParser, jsonFile, pivotImportMetadata, pivotFormatImport);
                foldersToImport.add(folderToImport);
            }

            if (foldersToImport.size() == GlobalProjectPivotImporterService.BATCH_SIZE) {
                createFolders(
                        foldersToImport,
                        projectIdsReferences,
                        pivotImportMetadata,
                        jsonFile,
                        pivotFormatImport);
            }
        }

        if (!foldersToImport.isEmpty()) {
            createFolders(
                    foldersToImport, projectIdsReferences, pivotImportMetadata, jsonFile, pivotFormatImport);
        }
    }

    private String findJsonImportFieldForJsonFile(JsonImportFile jsonFileName) {
        return switch (jsonFileName) {
            case REQUIREMENT_FOLDERS -> JsonImportField.REQUIREMENT_FOLDERS;
            case TEST_CASE_FOLDERS -> JsonImportField.TEST_CASE_FOLDERS;
            case CAMPAIGN_FOLDERS -> JsonImportField.CAMPAIGN_FOLDERS;
            default -> throw new IllegalArgumentException("Invalid json file name: " + jsonFileName);
        };
    }

    private void createFolders(
            List<FolderToImport> foldersToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            JsonImportFile jsonFile,
            PivotFormatImport pivotFormatImport) {
        for (FolderToImport folderToImport : foldersToImport) {
            PivotFormatLoggerHelper.logEntityCreationStarted(
                    LOGGER,
                    PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, false),
                    folderToImport.getInternalId(),
                    pivotFormatImport);
            try {
                createFolder(folderToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logEntityCreatedSuccessfully(
                        LOGGER,
                        PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, false),
                        folderToImport.getFolder().getName(),
                        folderToImport.getInternalId(),
                        pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.handleEntityCreationFailed(
                        LOGGER,
                        PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, false),
                        folderToImport.getFolder().getName(),
                        folderToImport.getInternalId(),
                        pivotFormatImport,
                        e);
            }
        }

        foldersToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void createFolder(
            FolderToImport folderToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        EntityType type = folderToImport.getParentType();
        String internalId = folderToImport.getInternalId();
        Map<Long, RawValue> cufs = folderToImport.getFolder().getCustomFields();

        switch (type) {
            case REQUIREMENT_LIBRARY -> {
                RequirementFolder squashFolder =
                        requirementLibraryNavigationService.addFolderToLibrary(
                                projectIdsReferences.getRequirementLibraryId(),
                                (RequirementFolder)
                                        folderToImport.getFolder().toFolder(EntityType.REQUIREMENT_FOLDER),
                                cufs);
                pivotImportMetadata.getRequirementFoldersIdsMap().put(internalId, squashFolder.getId());

                attachmentPivotImportService.addAttachmentsToEntity(
                        folderToImport.getAttachments(),
                        new AttachmentHolderInfo(
                                squashFolder.getId(),
                                squashFolder.getAttachmentList().getId(),
                                EntityType.REQUIREMENT_FOLDER,
                                folderToImport.getInternalId()),
                        pivotFormatImport,
                        pivotImportMetadata);
            }
            case REQUIREMENT_FOLDER -> {
                Long squashTargetId =
                        pivotImportMetadata.getRequirementFoldersIdsMap().get(folderToImport.getParentId());
                RequirementFolder squashFolder =
                        requirementLibraryNavigationService.addFolderToFolder(
                                squashTargetId,
                                (RequirementFolder)
                                        folderToImport.getFolder().toFolder(EntityType.REQUIREMENT_FOLDER),
                                cufs);
                pivotImportMetadata.getRequirementFoldersIdsMap().put(internalId, squashFolder.getId());

                attachmentPivotImportService.addAttachmentsToEntity(
                        folderToImport.getAttachments(),
                        new AttachmentHolderInfo(
                                squashFolder.getId(),
                                squashFolder.getAttachmentList().getId(),
                                EntityType.REQUIREMENT_FOLDER,
                                folderToImport.getInternalId()),
                        pivotFormatImport,
                        pivotImportMetadata);
            }
            case TEST_CASE_LIBRARY -> {
                List<String> contentNames =
                        testCaseLibraryNavigationService.findContentNamesByLibraryId(
                                (projectIdsReferences.getTestCaseLibraryId()));
                fixConflictNames(contentNames, folderToImport);

                TestCaseFolder squashFolder =
                        testCaseLibraryNavigationService.addFolderToLibrary(
                                projectIdsReferences.getTestCaseLibraryId(),
                                (TestCaseFolder) folderToImport.getFolder().toFolder(EntityType.TEST_CASE_FOLDER),
                                cufs);
                pivotImportMetadata.getTestCaseFoldersIdsMap().put(internalId, squashFolder.getId());

                attachmentPivotImportService.addAttachmentsToEntity(
                        folderToImport.getAttachments(),
                        new AttachmentHolderInfo(
                                squashFolder.getId(),
                                squashFolder.getAttachmentList().getId(),
                                EntityType.TEST_CASE_FOLDER,
                                folderToImport.getInternalId()),
                        pivotFormatImport,
                        pivotImportMetadata);
            }
            case TEST_CASE_FOLDER -> {
                Long squashTargetId =
                        pivotImportMetadata.getTestCaseFoldersIdsMap().get(folderToImport.getParentId());
                Map<Long, List<String>> folderContentNames =
                        testCaseLibraryNavigationService.findContentNamesByFolderIds(
                                Collections.singletonList(squashTargetId));
                if (folderContentNames.get(squashTargetId) != null) {
                    fixConflictNames(folderContentNames.get(squashTargetId), folderToImport);
                }

                TestCaseFolder squashFolder =
                        testCaseLibraryNavigationService.addFolderToFolder(
                                squashTargetId,
                                (TestCaseFolder) folderToImport.getFolder().toFolder(EntityType.TEST_CASE_FOLDER),
                                cufs);
                pivotImportMetadata.getTestCaseFoldersIdsMap().put(internalId, squashFolder.getId());

                attachmentPivotImportService.addAttachmentsToEntity(
                        folderToImport.getAttachments(),
                        new AttachmentHolderInfo(
                                squashFolder.getId(),
                                squashFolder.getAttachmentList().getId(),
                                EntityType.TEST_CASE_FOLDER,
                                folderToImport.getInternalId()),
                        pivotFormatImport,
                        pivotImportMetadata);
            }
            case CAMPAIGN_LIBRARY -> {
                CampaignFolder squashFolder =
                        campaignLibraryNavigationService.addFolderToLibrary(
                                projectIdsReferences.getCampaignLibraryId(),
                                (CampaignFolder) folderToImport.getFolder().toFolder(EntityType.CAMPAIGN_FOLDER),
                                cufs);
                pivotImportMetadata.getCampaignFoldersIdsMap().put(internalId, squashFolder.getId());

                attachmentPivotImportService.addAttachmentsToEntity(
                        folderToImport.getAttachments(),
                        new AttachmentHolderInfo(
                                squashFolder.getId(),
                                squashFolder.getAttachmentList().getId(),
                                EntityType.CAMPAIGN_FOLDER,
                                folderToImport.getInternalId()),
                        pivotFormatImport,
                        pivotImportMetadata);
            }
            case CAMPAIGN_FOLDER -> {
                Long squashTargetId =
                        pivotImportMetadata.getCampaignFoldersIdsMap().get(folderToImport.getParentId());
                CampaignFolder squashFolder =
                        campaignLibraryNavigationService.addFolderToFolder(
                                squashTargetId,
                                (CampaignFolder) folderToImport.getFolder().toFolder(EntityType.CAMPAIGN_FOLDER),
                                cufs);
                pivotImportMetadata.getCampaignFoldersIdsMap().put(internalId, squashFolder.getId());

                attachmentPivotImportService.addAttachmentsToEntity(
                        folderToImport.getAttachments(),
                        new AttachmentHolderInfo(
                                squashFolder.getId(),
                                squashFolder.getAttachmentList().getId(),
                                EntityType.CAMPAIGN_FOLDER,
                                folderToImport.getInternalId()),
                        pivotFormatImport,
                        pivotImportMetadata);
            }
            default ->
                    throw new IllegalArgumentException(
                            GlobalProjectPivotImporterService.ENTITY_TYPE_NOT_HANDLED + type);
        }
    }

    private static void fixConflictNames(List<String> contentNames, FolderToImport folderToImport) {

        String newName =
                LibraryUtils.generateNonClashingName(
                        folderToImport.getFolder().getName(), contentNames, Sizes.NAME_MAX);
        if (!newName.equals(folderToImport.getFolder().getName())) {
            folderToImport.getFolder().setName(newName);
        }
        contentNames.add(newName);
    }
}
