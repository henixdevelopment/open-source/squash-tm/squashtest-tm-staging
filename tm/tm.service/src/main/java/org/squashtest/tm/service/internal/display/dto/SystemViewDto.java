/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.AdministrationStatistics;
import org.squashtest.tm.service.system.BasicLicenseInfo;

public class SystemViewDto {
    AdministrationStatistics statistics;
    String appVersion;
    List<String> plugins;
    String whiteList;
    String uploadSizeLimit;
    String importSizeLimit;
    String callbackUrl;
    boolean stackTracePanelIsVisible;
    boolean stackTraceFeatureIsEnabled;
    boolean autoconnectOnConnection;
    boolean caseInsensitiveLogin;
    boolean caseInsensitiveActions;
    List<String> duplicateLogins;
    List<String> duplicateActions;
    String welcomeMessage;
    String loginMessage;
    String bannerMessage;
    List<String> logFiles;
    BasicLicenseInfo licenseInfo;
    List<ReportTemplateModel> reportTemplateModels;
    List<SynchronisationPluginViewDto> synchronisationPlugins;
    boolean unsafeAttachmentPreviewEnabled;

    public AdministrationStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(AdministrationStatistics statistics) {
        this.statistics = statistics;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public List<String> getPlugins() {
        return plugins;
    }

    public void setPlugins(List<String> plugins) {
        this.plugins = plugins;
    }

    public String getWhiteList() {
        return whiteList;
    }

    public void setWhiteList(String whiteList) {
        this.whiteList = whiteList;
    }

    public String getUploadSizeLimit() {
        return uploadSizeLimit;
    }

    public void setUploadSizeLimit(String uploadSizeLimit) {
        this.uploadSizeLimit = uploadSizeLimit;
    }

    public String getImportSizeLimit() {
        return importSizeLimit;
    }

    public void setImportSizeLimit(String importSizeLimit) {
        this.importSizeLimit = importSizeLimit;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public boolean isStackTracePanelIsVisible() {
        return stackTracePanelIsVisible;
    }

    public void setStackTracePanelIsVisible(boolean stackTracePanelIsVisible) {
        this.stackTracePanelIsVisible = stackTracePanelIsVisible;
    }

    public boolean isStackTraceFeatureIsEnabled() {
        return stackTraceFeatureIsEnabled;
    }

    public void setStackTraceFeatureIsEnabled(boolean stackTraceFeatureIsEnabled) {
        this.stackTraceFeatureIsEnabled = stackTraceFeatureIsEnabled;
    }

    public boolean isAutoconnectOnConnection() {
        return autoconnectOnConnection;
    }

    public void setAutoconnectOnConnection(boolean autoconnectOnConnection) {
        this.autoconnectOnConnection = autoconnectOnConnection;
    }

    public boolean isCaseInsensitiveLogin() {
        return caseInsensitiveLogin;
    }

    public void setCaseInsensitiveLogin(boolean caseInsensitiveLogin) {
        this.caseInsensitiveLogin = caseInsensitiveLogin;
    }

    public boolean isCaseInsensitiveActions() {
        return caseInsensitiveActions;
    }

    public void setCaseInsensitiveActions(boolean caseInsensitiveActions) {
        this.caseInsensitiveActions = caseInsensitiveActions;
    }

    public List<String> getDuplicateLogins() {
        return duplicateLogins;
    }

    public void setDuplicateLogins(List<String> duplicateLogins) {
        this.duplicateLogins = duplicateLogins;
    }

    public List<String> getDuplicateActions() {
        return duplicateActions;
    }

    public void setDuplicateActions(List<String> duplicateActions) {
        this.duplicateActions = duplicateActions;
    }

    @CleanHtml
    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    @CleanHtml
    public String getLoginMessage() {
        return loginMessage;
    }

    public void setLoginMessage(String loginMessage) {
        this.loginMessage = loginMessage;
    }

    @CleanHtml
    public String getBannerMessage() {
        return bannerMessage;
    }

    public void setBannerMessage(String bannerMessage) {
        this.bannerMessage = bannerMessage;
    }

    public List<String> getLogFiles() {
        return logFiles;
    }

    public void setLogFiles(List<String> logFiles) {
        this.logFiles = logFiles;
    }

    public BasicLicenseInfo getLicenseInfo() {
        return licenseInfo;
    }

    public void setLicenseInfo(BasicLicenseInfo licenseInfo) {
        this.licenseInfo = licenseInfo;
    }

    public List<ReportTemplateModel> getReportTemplateModels() {
        return reportTemplateModels;
    }

    public void setReportTemplateModels(List<ReportTemplateModel> reportTemplateModels) {
        this.reportTemplateModels = reportTemplateModels;
    }

    public List<SynchronisationPluginViewDto> getSynchronisationPlugins() {
        return synchronisationPlugins;
    }

    public void setSynchronisationPlugins(List<SynchronisationPluginViewDto> synchronisationPlugins) {
        this.synchronisationPlugins = synchronisationPlugins;
    }

    public boolean isUnsafeAttachmentPreviewEnabled() {
        return unsafeAttachmentPreviewEnabled;
    }

    public void setUnsafeAttachmentPreviewEnabled(boolean unsafeAttachmentPreviewEnabled) {
        this.unsafeAttachmentPreviewEnabled = unsafeAttachmentPreviewEnabled;
    }
}
