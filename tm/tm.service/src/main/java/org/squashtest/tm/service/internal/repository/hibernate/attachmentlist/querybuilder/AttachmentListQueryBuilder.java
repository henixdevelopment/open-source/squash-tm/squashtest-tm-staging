/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.attachmentlist.querybuilder;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.service.internal.repository.hibernate.attachmentlist.querybuilder.context.AttachmentListLockedMilestonesQueryContext;
import org.squashtest.tm.service.internal.repository.hibernate.attachmentlist.querybuilder.strategy.AttachmentListLockedMilestonesQueryStrategy;

public class AttachmentListQueryBuilder {

    private static final String HOLDER_BASE_QUERY = "select holder from ";
    private static final String HOLDER_REQUIREMENT_FOLDER_QUERY =
            " holder where holder.resource.attachmentList.id = :attachmentListId";
    private static final String HOLDER_DEFAULT_QUERY =
            " holder where holder.attachmentList.id = :attachmentListId";

    private static final Map<String, String> customQueries = new HashMap<>();

    static {
        customQueries.put(
                EntityType.REQUIREMENT_FOLDER.getSimpleClassName(), HOLDER_REQUIREMENT_FOLDER_QUERY);
        customQueries.put(EntityType.REQUIREMENT.getSimpleClassName(), HOLDER_REQUIREMENT_FOLDER_QUERY);
    }

    private static final Map<EntityType, Supplier<AttachmentListLockedMilestonesQueryStrategy>>
            STRATEGY_MAP = new EnumMap<>(EntityType.class);

    static {
        STRATEGY_MAP.put(
                EntityType.REQUIREMENT_VERSION, RequirementVersionAttachmentListQueryStrategy::new);
        STRATEGY_MAP.put(EntityType.TEST_CASE, TestCaseAttachmentListQueryStrategy::new);
        STRATEGY_MAP.put(EntityType.CAMPAIGN, CampaignAttachmentListQueryStrategy::new);
        STRATEGY_MAP.put(EntityType.ITERATION, IterationAttachmentListQueryStrategy::new);
        STRATEGY_MAP.put(EntityType.EXECUTION, ExecutionAttachmentListQueryStrategy::new);
        STRATEGY_MAP.put(EntityType.EXECUTION_STEP, ExecutionStepAttachmentListQueryStrategy::new);
        STRATEGY_MAP.put(EntityType.ACTION_TEST_STEP, ActionTestStepAttachmentListQueryStrategy::new);
    }

    public boolean isLockedMilestonesQuerySupported(EntityType entityType) {
        return STRATEGY_MAP.containsKey(entityType);
    }

    public String getLockedMilestonesQuery(EntityType entityType) {
        AttachmentListLockedMilestonesQueryContext context =
                new AttachmentListLockedMilestonesQueryContext();

        Supplier<AttachmentListLockedMilestonesQueryStrategy> strategySupplier =
                STRATEGY_MAP.get(entityType);
        if (strategySupplier == null) {
            throw new IllegalArgumentException("Unsupported entity type: " + entityType);
        }

        context.setStrategy(strategySupplier.get());

        return context.getLockedMilestonesQuery();
    }

    public String getHolderEntityQuery(EntityType entityType) {
        String className = entityType.getSimpleClassName();

        StringBuilder query = new StringBuilder(HOLDER_BASE_QUERY).append(className);
        query.append(customQueries.getOrDefault(className, HOLDER_DEFAULT_QUERY));

        return query.toString();
    }
}
