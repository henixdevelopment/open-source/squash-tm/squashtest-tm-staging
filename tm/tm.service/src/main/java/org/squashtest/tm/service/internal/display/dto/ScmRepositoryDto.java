/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.scm.ScmRepository;

public class ScmRepositoryDto {
    private Long scmRepositoryId;
    private Long serverId;
    private String name;
    private String repositoryPath;
    private String workingFolderPath;
    private String workingBranch;

    public ScmRepositoryDto() {}

    public ScmRepositoryDto(ScmRepository scmRepository) {
        this.scmRepositoryId = scmRepository.getId();
        this.serverId = scmRepository.getScmServer().getId();
        this.name = scmRepository.getName();
        this.repositoryPath = scmRepository.getRepositoryPath();
        this.workingFolderPath = scmRepository.getWorkingFolderPath();
        this.workingBranch = scmRepository.getWorkingBranch();
    }

    public Long getScmRepositoryId() {
        return scmRepositoryId;
    }

    public void setScmRepositoryId(Long scmRepositoryId) {
        this.scmRepositoryId = scmRepositoryId;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRepositoryPath() {
        return repositoryPath;
    }

    public void setRepositoryPath(String repositoryPath) {
        this.repositoryPath = repositoryPath;
    }

    public String getWorkingFolderPath() {
        return workingFolderPath;
    }

    public void setWorkingFolderPath(String workingFolderPath) {
        this.workingFolderPath = workingFolderPath;
    }

    public String getWorkingBranch() {
        return workingBranch;
    }

    public void setWorkingBranch(String workingBranch) {
        this.workingBranch = workingBranch;
    }
}
