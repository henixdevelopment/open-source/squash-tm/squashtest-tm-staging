/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.resultpublisher;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.squashtest.squash.automation.tm.commons.ExpectedSuiteDefinition;
import org.squashtest.squash.automation.tm.commons.SquashTestMessageApi;
import org.squashtest.squash.automation.tm.testplan.library.model.TestPlan;
import org.squashtest.tm.service.internal.testautomation.model.messages.Workflow;

public class ExpectedSuiteDefinitionBuilder {
    public ExpectedSuiteDefinition buildExpectedSuiteData(
            TestPlan plan,
            String workflowId,
            Workflow workflow,
            String callbackUrl,
            String automatedServerToken,
            String squashTmVersion) {
        Map<String, Object> with = new HashMap<>();
        with.put("squashTMUrl", callbackUrl);
        with.put("automatedServerToken", automatedServerToken);
        with.put("suiteId", plan.getSuiteId());
        with.put("testExecutions", plan.getTestExecutionToTransmit());

        ExpectedSuiteDefinition suiteDefinition =
                new ExpectedSuiteDefinition(SquashTestMessageApi.CURRENT_SQUASHTEST_API_VERSION, with);
        suiteDefinition.addMetadata("workflow_id", workflowId);
        suiteDefinition.addMetadata("job_id", workflowId);
        suiteDefinition.addMetadata("job_origin", Collections.singletonList(workflowId));
        suiteDefinition.addMetadata("name", workflow.name());
        suiteDefinition.addMetadata("squash_tm_version", squashTmVersion);

        return suiteDefinition;
    }
}
