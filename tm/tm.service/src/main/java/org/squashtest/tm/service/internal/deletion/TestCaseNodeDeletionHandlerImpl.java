/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.attachment.ExternalContentCoordinates;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.domain.testcase.TestStepVisitor;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.campaign.LockedTestCaseNodeDetectionService;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.repository.AutomatedTestDao;
import org.squashtest.tm.service.internal.repository.AutomationRequestDao;
import org.squashtest.tm.service.internal.repository.CustomSprintReqVersionDao;
import org.squashtest.tm.service.internal.repository.FolderDao;
import org.squashtest.tm.service.internal.repository.TestCaseDeletionDao;
import org.squashtest.tm.service.internal.repository.TestCaseFolderDao;
import org.squashtest.tm.service.internal.testcase.TestCaseNodeDeletionHandler;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.testcase.CallStepManagerService;
import org.squashtest.tm.service.testcase.DatasetModificationService;
import org.squashtest.tm.service.testcase.ParameterModificationService;
import org.squashtest.tm.service.testcase.TestCaseImportanceManagerService;

@Component("squashtest.tm.service.deletion.TestCaseNodeDeletionHandler")
@Transactional
public class TestCaseNodeDeletionHandlerImpl
        extends AbstractNodeDeletionHandler<TestCaseLibraryNode, TestCaseFolder>
        implements TestCaseNodeDeletionHandler {

    @Inject private LockedTestCaseNodeDetectionService lockedTestCaseNodeDetectionService;

    @Inject private TestCaseFolderDao folderDao;

    @Inject private TestCaseDeletionDao deletionDao;

    @Inject private TestCaseImportanceManagerService testCaseImportanceManagerService;

    @Inject private DatasetModificationService datasetService;

    @Inject private ParameterModificationService parameterService;

    @Inject private PrivateCustomFieldValueService customValueService;

    @Inject private AutomatedTestDao autoTestDao;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private AutomationRequestDao requestDao;

    @Inject private CallStepManagerService callStepManagerService;

    @Inject private CustomSprintReqVersionDao customSprintReqVersionDao;

    @Override
    protected FolderDao<TestCaseFolder, TestCaseLibraryNode> getFolderDao() {
        return folderDao;
    }

    /* *******************************************************************************
     *
     * DELETION SIMULATION
     *
     * ******************************************************************************/

    @Override
    protected List<SuppressionPreviewReport> diagnoseSuppression(List<Long> nodeIds) {
        List<SuppressionPreviewReport> reportList = new ArrayList<>();
        reportList.addAll(lockedTestCaseNodeDetectionService.detectLockedByExecutedTestCases(nodeIds));
        reportList.addAll(lockedTestCaseNodeDetectionService.detectLockedByMilestone(nodeIds));
        reportList.addAll(lockedTestCaseNodeDetectionService.detectLockedWithActiveMilestone(nodeIds));
        reportList.addAll(
                lockedTestCaseNodeDetectionService.detectLockedByCallSteps(nodeIds, reportList));
        return reportList;
    }

    /* *******************************************************************************
     *
     * ACTUAL DELETION
     *
     * ******************************************************************************/

    @Override
    protected List<Long> detectLockedNodes(final List<Long> nodeIds) {
        return diagnoseSuppression(nodeIds).stream()
                .flatMap(report -> report.getLockedNodes().stream())
                .distinct()
                .toList();
    }

    @Override
    /*
     * Will batch-remove some TestCaseLibraryNodes. Since we may have to delete lots of nested entities we cannot afford
     * to use Hibernate orm abilities : you don't want one fetch-query per entity or the DB admin from hell will eat
     * you.
     *
     * Note : We only need to take care of the attachments and steps, the rest will cascade thanks to the ON CASCADE
     * clauses in the other tables.
     */
    protected OperationReport batchDeleteNodes(List<Long> ids) {

        OperationReport report = new OperationReport();

        if (!ids.isEmpty()) {

            DeletableIds deletableIds = findSeparateIds(ids);

            List<Long> folderIds = deletableIds.getFolderIds();
            List<Long> tcIds = deletableIds.getTestCaseIds();

            List<Long> stepIds = deletionDao.findTestSteps(tcIds);

            List<Long> testCaseAttachmentIds = deletionDao.findTestCaseAttachmentListIds(tcIds);
            List<Long> testStepAttachmentIds = deletionDao.findTestStepAttachmentListIds(stepIds);
            List<Long> testCaseFolderAttachmentIds =
                    deletionDao.findTestCaseFolderAttachmentListIds(folderIds);

            // We merge the attachment list ids for
            // test cases, test step and folder first so that
            // we can make one only one query against the database.
            testCaseAttachmentIds.addAll(testStepAttachmentIds);
            testCaseAttachmentIds.addAll(testCaseFolderAttachmentIds);
            List<ExternalContentCoordinates> listPairContentIDListID =
                    attachmentManager.getListIDbyContentIdForAttachmentLists(testCaseAttachmentIds);

            List<Long> automationRequestIds = requestDao.getReqIdsByTcIds(tcIds);
            deletionDao.removeAutomationRequestLibraryContent(automationRequestIds);

            deletionDao.removeCampaignTestPlanInboundReferences(tcIds);
            deletionDao.removeOrSetIterationTestPlanInboundReferencesToNull(tcIds);

            deletionDao.setExecutionInboundReferencesToNull(tcIds);
            deletionDao.setExecStepInboundReferencesToNull(stepIds);

            deletionDao.removeFromVerifyingTestStepsList(stepIds);
            deletionDao.removeFromVerifyingTestCaseLists(tcIds);

            customValueService.deleteAllCustomFieldValues(BindableEntity.TEST_STEP, stepIds);
            deletionDao.removeAllSteps(stepIds);
            attachmentManager.removeAttachmentsAndLists(testStepAttachmentIds);

            customValueService.deleteAllCustomFieldValues(BindableEntity.TESTCASE_FOLDER, folderIds);
            customValueService.deleteAllCustomFieldValues(BindableEntity.TEST_CASE, tcIds);

            datasetService.removeAllByTestCaseIds(tcIds);
            parameterService.removeAllByTestCaseIds(tcIds);

            customSprintReqVersionDao.removeTestPlanItemOrNullifyReferencedTestCase(tcIds);

            deletionDao.removeEntities(deletableIds.getAllIds());

            // remove All AttachmentContents for FileSystemRepository and orphan in DB
            attachmentManager.deleteContents(listPairContentIDListID);

            report.addRemoved(folderIds, "folder");
            report.addRemoved(tcIds, "test-case");

            // Last, take care of the automated tests that could end up as "orphans" after the mass
            // deletion
            autoTestDao.pruneOrphans();
        }

        return report;
    }

    @Override
    protected OperationReport batchUnbindFromMilestone(List<Long> ids) {

        List<Long> remainingIds = deletionDao.findRemainingTestCaseIds(ids);

        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();

        // some node should not be unbound.
        List<Long> lockedIds = deletionDao.findTestCasesWhichMilestonesForbidsDeletion(remainingIds);
        remainingIds.removeAll(lockedIds);

        OperationReport report = new OperationReport();

        deletionDao.unbindFromMilestone(remainingIds, activeMilestone.get().getId());

        report.addRemoved(remainingIds, "test-case");

        return report;
    }

    /* ************************ TestCaseNodeDeletionHandler impl ***************************** */

    /*
     * deleting a test step means : - delete its attachments, - delete itself.
     */
    @Override
    public void deleteStep(TestCase owner, TestStep step) {

        int index = owner.getPositionOfStep(step.getId());

        if (index == -1) {
            return;
        }

        owner.getSteps().remove(index);

        List<Long> stepId = new LinkedList<>();
        stepId.add(step.getId());
        deletionDao.setExecStepInboundReferencesToNull(stepId);

        TestStepVisitor testStepVisitor =
                new TestStepVisitor() {
                    @Override
                    public void visit(ActionTestStep visited) {
                        customValueService.deleteAllCustomFieldValues(visited);
                        deleteActionStep(visited);
                        customValueService.deleteAllCustomFieldValues(visited);
                    }

                    @Override
                    public void visit(CallTestStep visited) {
                        doDeleteTestStep(visited);
                        deleteOrphanDatasetParamValues(visited.isDelegateParameterValues(), owner.getId());
                        testCaseImportanceManagerService.changeImportanceIfCallStepRemoved(
                                visited.getCalledTestCase(), owner);
                    }

                    @Override
                    public void visit(KeywordTestStep visited) {
                        doDeleteTestStep(visited);
                    }
                };

        step.accept(testStepVisitor);
    }

    /* ************************ privates stuffs ************************ */
    private void deleteActionStep(ActionTestStep step) {
        AttachmentList attachmentList = step.getAttachmentList();
        // save ListId, contentID for FileSystem Repository
        List<ExternalContentCoordinates> listPairContenIDListID =
                getExternalAttachmentContentCoordinatesOfObject(step);
        doDeleteTestStep(step); // Cascade AttachmentList -> include AttachmentList and Attachments
        attachmentManager.removeAttachmentsAndLists(
                makeListAttachmentListIdFordAttachmentList(attachmentList));
        attachmentManager.deleteContents(listPairContenIDListID);
    }

    private void doDeleteTestStep(TestStep step) {
        deletionDao.removeEntity(step);
    }

    private DeletableIds findSeparateIds(List<Long> ids) {
        List<Long>[] separatedIds = deletionDao.separateFolderFromTestCaseIds(ids);
        return new DeletableIds(separatedIds[0], separatedIds[1]);
    }

    private void deleteOrphanDatasetParamValues(
            boolean isDelegateParameterValues, Long testCaseCallerId) {
        if (isDelegateParameterValues) {
            callStepManagerService.deleteOrphanDatasetParamValuesByTestCaseCallerId(testCaseCallerId);
        }
    }

    @Override
    protected boolean isMilestoneMode() {
        return activeMilestoneHolder.getActiveMilestone().isPresent();
    }

    private static final class DeletableIds {
        private final List<Long> folderIds;
        private final List<Long> testCaseIds;

        public DeletableIds(List<Long> folderIds, List<Long> testCaseIds) {
            super();
            this.folderIds = folderIds;
            this.testCaseIds = testCaseIds;
        }

        public List<Long> getFolderIds() {
            return folderIds;
        }

        public List<Long> getTestCaseIds() {
            return testCaseIds;
        }

        public List<Long> getAllIds() {
            ArrayList<Long> all = new ArrayList<>(folderIds.size() + testCaseIds.size());
            all.addAll(folderIds);
            all.addAll(testCaseIds);
            return all;
        }
    }
}
