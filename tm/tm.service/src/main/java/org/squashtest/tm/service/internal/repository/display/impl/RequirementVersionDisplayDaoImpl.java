/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.val;
import static org.jooq.impl.DSL.when;
import static org.jooq.tools.StringUtils.EMPTY;
import static org.squashtest.tm.domain.requirement.RequirementVersion.FULL_NAME_SEPARATOR;
import static org.squashtest.tm.jooq.domain.Tables.AI_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUIREMENT_LIBRARY_NODE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUIREMENT_VERSION_REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.RESOURCE_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STATUS;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Table;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.service.internal.display.dto.requirement.AbstractRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.DetailedStepViewRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto.LinkedHighLevelRequirementDto;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Repository
public class RequirementVersionDisplayDaoImpl implements RequirementVersionDisplayDao {

    private static final String HAS_EXTENDER = "HAS_EXTENDER";
    private static final String SERVER_NAME = "SERVER_NAME";
    private static final String REMOTE_SYNCHRONISATION_KIND = "REMOTE_SYNCHRONISATION_KIND";
    private static final String AI_SERVER_ID = "AI_SERVER_ID";
    private static final String CAN_USE_AI_FEATURE = "CAN_USE_AI_FEATURE";

    private final DSLContext dsl;
    private final ActiveMilestoneHolder activeMilestoneHolder;

    public RequirementVersionDisplayDaoImpl(
            DSLContext dsl, ActiveMilestoneHolder activeMilestoneHolder) {
        this.dsl = dsl;
        this.activeMilestoneHolder = activeMilestoneHolder;
    }

    @Override
    public Map<Long, Long> findCurrentRequirementVersions(List<Long> requirementIds) {
        Optional<Milestone> activeMilestoneId = activeMilestoneHolder.getActiveMilestone();
        if (activeMilestoneId.isPresent()) {
            return findRequirementVersionIdsInMilestoneMode(
                    requirementIds, activeMilestoneId.get().getId());
        } else {
            return findRequirementVersionIdsWithoutMilestone(requirementIds);
        }
    }

    @Override
    public Long findCurrentRequirementVersions(Long requirementId) {
        Map<Long, Long> currentRequirementVersions =
                this.findCurrentRequirementVersions(Collections.singletonList(requirementId));
        return currentRequirementVersions.get(requirementId);
    }

    @Override
    public List<String> retrieveFullNameByRequirementLibraryNodeIds(
            List<Long> requirementLibraryNodeIds, List<Long> projectIds) {
        Table<Record3<String, String, Long>> reqReferenceAndNameTable =
                getReqReferenceWithNameAndIdTable(requirementLibraryNodeIds);

        Field<String> reference =
                reqReferenceAndNameTable.field(REQUIREMENT_VERSION_REFERENCE, String.class);
        Field<String> name = reqReferenceAndNameTable.field(RESOURCE_NAME, String.class);
        Field<Long> nodeId = reqReferenceAndNameTable.field(REQUIREMENT_LIBRARY_NODE_ID, Long.class);
        Field<String> fullName =
                when(Objects.requireNonNull(reference).eq(EMPTY), name)
                        .otherwise(concat(reference, val(FULL_NAME_SEPARATOR), name));

        return dsl.select(fullName)
                .from(reqReferenceAndNameTable)
                .join(REQUIREMENT_LIBRARY_NODE)
                .on(Objects.requireNonNull(nodeId).eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                .where(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(fullName)
                .fetch(fullName);
    }

    private Table<Record3<String, String, Long>> getReqReferenceWithNameAndIdTable(
            List<Long> requirementLibraryNodeIds) {
        return dsl.select(
                        when(REQUIREMENT_VERSION.REFERENCE.isNull(), EMPTY)
                                .otherwise(REQUIREMENT_VERSION.REFERENCE)
                                .as(REQUIREMENT_VERSION_REFERENCE),
                        RESOURCE.NAME.as(RESOURCE_NAME),
                        REQUIREMENT_VERSION.REQUIREMENT_ID.as(REQUIREMENT_LIBRARY_NODE_ID))
                .from(RESOURCE)
                .join(REQUIREMENT_VERSION)
                .on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
                .where(REQUIREMENT_VERSION.REQUIREMENT_ID.in(requirementLibraryNodeIds))
                .union(
                        dsl.select(
                                        val(EMPTY).as(REQUIREMENT_VERSION_REFERENCE),
                                        RESOURCE.NAME.as(RESOURCE_NAME),
                                        REQUIREMENT_FOLDER.RLN_ID.as(REQUIREMENT_LIBRARY_NODE_ID))
                                .from(RESOURCE)
                                .join(REQUIREMENT_FOLDER)
                                .on(RESOURCE.RES_ID.eq(REQUIREMENT_FOLDER.RES_ID))
                                .where(REQUIREMENT_FOLDER.RLN_ID.in(requirementLibraryNodeIds)))
                .asTable();
    }

    @Override
    public RequirementVersionDto findRequirementVersion(Long currentVersionId) {
        RequirementVersionDto requirementVersion =
                findBaseRequirementVersion(currentVersionId, RequirementVersionDto.class);
        appendLinkedHighLevelRequirement(requirementVersion);
        return requirementVersion;
    }

    @Override
    public HighLevelRequirementVersionDto findHighLevelRequirementVersion(Long currentVersionId) {
        return findBaseRequirementVersion(currentVersionId, HighLevelRequirementVersionDto.class);
    }

    private void appendLinkedHighLevelRequirement(RequirementVersionDto requirementVersion) {
        LinkedHighLevelRequirementDto linkedHighLevelRequirement =
                findLinkedHighLevelRequirement(requirementVersion.getRequirementId());
        requirementVersion.setLinkedHighLevelRequirement(linkedHighLevelRequirement);
    }

    @Override
    public LinkedHighLevelRequirementDto findLinkedHighLevelRequirement(Long requirementId) {
        Requirement lowLevelRequirement = REQUIREMENT.as("LOW_LEVEL_REQUIREMENT");
        Requirement highLevelRequirement = REQUIREMENT.as("HIGH_LEVEL_REQUIREMENT");
        return dsl.select(
                        highLevelRequirement.RLN_ID.as("REQUIREMENT_ID"),
                        RESOURCE.RES_ID.as("REQUIREMENT_VERSION_ID"),
                        RESOURCE.NAME,
                        REQUIREMENT_VERSION.REFERENCE,
                        PROJECT.NAME.as(PROJECT_NAME))
                .from(lowLevelRequirement)
                .innerJoin(highLevelRequirement)
                .on(highLevelRequirement.RLN_ID.eq(lowLevelRequirement.HIGH_LEVEL_REQUIREMENT_ID))
                .innerJoin(RESOURCE)
                .on(RESOURCE.RES_ID.eq(highLevelRequirement.CURRENT_VERSION_ID))
                .innerJoin(REQUIREMENT_VERSION)
                .on(REQUIREMENT_VERSION.RES_ID.eq(highLevelRequirement.CURRENT_VERSION_ID))
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(highLevelRequirement.RLN_ID))
                .innerJoin(PROJECT)
                .on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(lowLevelRequirement.RLN_ID.eq(requirementId))
                .fetchOneInto(LinkedHighLevelRequirementDto.class);
    }

    private <T extends AbstractRequirementVersionDto> T findBaseRequirementVersion(
            Long currentVersionId, Class<T> baseClass) {
        return dsl.select(
                        REQUIREMENT_VERSION.RES_ID.as(RequestAliasesConstants.ID),
                        REQUIREMENT_VERSION.REFERENCE,
                        REQUIREMENT_VERSION.REQUIREMENT_ID,
                        REQUIREMENT_VERSION.VERSION_NUMBER,
                        REQUIREMENT_VERSION.CATEGORY,
                        REQUIREMENT_VERSION.CRITICALITY,
                        REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS),
                        RESOURCE.NAME,
                        RESOURCE.ATTACHMENT_LIST_ID,
                        RESOURCE.CREATED_BY,
                        RESOURCE.CREATED_ON,
                        RESOURCE.LAST_MODIFIED_BY,
                        RESOURCE.LAST_MODIFIED_ON,
                        RESOURCE.DESCRIPTION,
                        REQUIREMENT_LIBRARY_NODE.PROJECT_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_URL,
                        REMOTE_SYNCHRONISATION.SYNC_STATUS,
                        REMOTE_SYNCHRONISATION.KIND.as(REMOTE_SYNCHRONISATION_KIND),
                        REMOTE_SYNCHRONISATION.SERVER_ID,
                        THIRD_PARTY_SERVER.NAME.as(SERVER_NAME),
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_PERIMETER_STATUS,
                        AI_SERVER.SERVER_ID.as(AI_SERVER_ID),
                        field(AI_SERVER.PAYLOAD_TEMPLATE.isNotNull().and(AI_SERVER.JSON_PATH.isNotNull()))
                                .as(CAN_USE_AI_FEATURE),
                        field(REQUIREMENT_SYNC_EXTENDER.REQ_SYNC_ID.isNotNull()).as(HAS_EXTENDER))
                .from(REQUIREMENT_VERSION)
                .innerJoin(RESOURCE)
                .using(REQUIREMENT_VERSION.RES_ID)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .leftJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .leftJoin(REMOTE_SYNCHRONISATION)
                .on(
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.eq(
                                REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID))
                .leftJoin(THIRD_PARTY_SERVER)
                .on(REMOTE_SYNCHRONISATION.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .leftJoin(AI_SERVER)
                .on(PROJECT.AI_SERVER_ID.eq(AI_SERVER.SERVER_ID))
                .where(REQUIREMENT_VERSION.RES_ID.eq(currentVersionId))
                .fetchOneInto(baseClass);
    }

    @Override
    public DetailedStepViewRequirementVersionDto findById(Long requirementVersionId) {
        return dsl.select(
                        REQUIREMENT_VERSION.CATEGORY,
                        REQUIREMENT_VERSION.CRITICALITY,
                        REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS),
                        REQUIREMENT_VERSION.VERSION_NUMBER,
                        RESOURCE.DESCRIPTION)
                .from(REQUIREMENT_VERSION)
                .innerJoin(RESOURCE)
                .using(REQUIREMENT_VERSION.RES_ID)
                .where(REQUIREMENT_VERSION.RES_ID.eq(requirementVersionId))
                .fetchOneInto(DetailedStepViewRequirementVersionDto.class);
    }

    private Map<Long, Long> findRequirementVersionIdsWithoutMilestone(List<Long> requirementIds) {
        return findCurrentVersionByRequirement(requirementIds);
    }

    private Map<Long, Long> findCurrentVersionByRequirement(List<Long> requirementIds) {
        return dsl.select(REQUIREMENT.RLN_ID, REQUIREMENT.CURRENT_VERSION_ID)
                .from(REQUIREMENT)
                .where(REQUIREMENT.RLN_ID.in(requirementIds))
                .fetchMap(REQUIREMENT.RLN_ID, REQUIREMENT.CURRENT_VERSION_ID);
    }

    private Map<Long, Long> findRequirementVersionIdsInMilestoneMode(
            List<Long> requirementIds, Long activeMilestoneId) {
        Map<Long, Long> requirementToCurrentVersionIds =
                findCurrentVersionByRequirement(requirementIds);

        Map<Long, Long> milestoneRequirementVersionIds =
                dsl.select(REQUIREMENT_VERSION.REQUIREMENT_ID, REQUIREMENT_VERSION.RES_ID)
                        .from(REQUIREMENT_VERSION)
                        .innerJoin(MILESTONE_REQ_VERSION)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                        .where(REQUIREMENT_VERSION.REQUIREMENT_ID.in(requirementIds))
                        .and(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(activeMilestoneId))
                        .fetchMap(REQUIREMENT_VERSION.REQUIREMENT_ID, REQUIREMENT_VERSION.RES_ID);

        return requirementIds.stream()
                .collect(
                        Collectors.toMap(
                                Function.identity(),
                                requirementId ->
                                        Optional.ofNullable(
                                                        milestoneRequirementVersionIds.getOrDefault(
                                                                requirementId,
                                                                requirementToCurrentVersionIds.getOrDefault(requirementId, null)))
                                                .orElseThrow(
                                                        () ->
                                                                new IllegalArgumentException(
                                                                        "No version found for requirement " + requirementId))));
    }
}
