/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation;

import java.util.List;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.supervision.model.AutomatedSuiteOverview;

public interface AutomatedSuiteManagerService {

    /** Finds a suite given its id. */
    AutomatedSuite findById(String id);

    /**
     * Creates then run the suite. The specification contains both creation instruction and the
     * execution instruction (the execution configuration). It can deal with ITPIs automated with
     * Squash TF or Squash AUTOM.
     */
    AutomatedSuiteOverview createAndExecute(
            AutomatedSuiteCreationSpecification specification, List<Long> itemTestPlanIds);

    /**
     * Creates a new AutomatedSuite based on the whole test plan of an {@link Iteration}, given its
     * ID. Only automated tests planned in the test plan will be included. The automated executions
     * are ordered according to the test plan.
     */
    @UsedInPlugin("api-rest")
    AutomatedSuite createFromIterationTestPlan(long iterationId, List<Long> testPlanIds);

    /**
     * Creates a new AutomatedSuite based on the whole test plan of a {@link TestSuite}, given its ID.
     * Only automated tests planned in the test plan will be included. The automated executions are
     * ordered according to the test plan.
     */
    @UsedInPlugin("api-rest")
    AutomatedSuite createFromTestSuiteTestPlan(long testSuiteId, List<Long> testPlanIds);

    /** Clean all AutomatedSuites which are older than the lifetime configured in their project. */
    void cleanOldSuites();

    void cleanOldSuitesForProject(Long projectId);

    /**
     * Count the number of AutomatedSuites and AutomatedExecutions which are older than the lifetime
     * configured in their project.
     *
     * @return An AutomationDeletionCount containing old suites count and old executions count.
     */
    AutomationDeletionCount countOldAutomatedSuitesAndExecutions();

    AutomationDeletionCount countOldAutomatedSuitesAndExecutionsForProject(Long projectId);

    /** Runs an automatedSuite (Squash TF executions only) with empty configuration. */
    @UsedInPlugin("rest-api")
    void start(AutomatedSuite suite);

    /**
     * Creates a new AutomatedSuite based on a collection of {@link IterationTestPlanItem}, given
     * their ID. Only automated tests will be included. The automated executions are ordered according
     * to the iteration's test plan.
     */
    @UsedInPlugin("rest-api")
    AutomatedSuite createFromItemsAndIteration(List<Long> testPlanIds, long iterationId);

    boolean stopWorkflows(String suiteId, List<String> workflows);

    void deleteAutomatedSuites(List<String> automatedSuiteIds);

    void pruneAutomatedSuites(List<String> automatedSuiteIds, boolean complete);

    void pruneAttachments(Long projectId, boolean complete);
}
