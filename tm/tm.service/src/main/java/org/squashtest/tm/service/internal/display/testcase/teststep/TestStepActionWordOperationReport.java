/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.testcase.teststep;

import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;

public class TestStepActionWordOperationReport {

    private String action;
    private String styledAction;
    private Long actionWordId;
    private TestCaseParameterOperationReport paramOperationReport;

    public TestStepActionWordOperationReport(
            String action,
            String styledAction,
            Long actionWordId,
            TestCaseParameterOperationReport paramOperationReport) {
        this.action = action;
        this.styledAction = styledAction;
        this.actionWordId = actionWordId;
        this.paramOperationReport = paramOperationReport;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @CleanHtml
    public String getStyledAction() {
        return styledAction;
    }

    public void setStyledAction(String styledAction) {
        this.styledAction = styledAction;
    }

    public Long getActionWordId() {
        return actionWordId;
    }

    public void setActionWordId(Long actionWordId) {
        this.actionWordId = actionWordId;
    }

    public TestCaseParameterOperationReport getParamOperationReport() {
        return paramOperationReport;
    }

    public void setParamOperationReport(TestCaseParameterOperationReport paramOperationReport) {
        this.paramOperationReport = paramOperationReport;
    }
}
