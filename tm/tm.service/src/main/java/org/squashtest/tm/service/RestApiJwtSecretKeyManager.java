/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service;

import io.jsonwebtoken.security.Keys;
import java.util.Base64;
import javax.crypto.SecretKey;

public final class RestApiJwtSecretKeyManager {

    private static volatile SecretKey cachedJwtSecretKey = null;

    private RestApiJwtSecretKeyManager() {}

    public static SecretKey getRestApiJwtSecretKey(String base64EncodedSecret) {
        if (cachedJwtSecretKey == null) {
            synchronized (RestApiJwtSecretKeyManager.class) {
                if (cachedJwtSecretKey == null) {
                    try {
                        cachedJwtSecretKey =
                                Keys.hmacShaKeyFor(Base64.getDecoder().decode(base64EncodedSecret));
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException("The provided secret is not encoded in Base64.", e);
                    }
                }
            }
        }
        return cachedJwtSecretKey;
    }
}
