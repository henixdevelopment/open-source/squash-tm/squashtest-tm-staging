/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.sum;
import static org.jooq.impl.DSL.timestampAdd;
import static org.squashtest.tm.domain.campaign.QIteration.iteration;
import static org.squashtest.tm.domain.campaign.QIterationTestPlanItem.iterationTestPlanItem;
import static org.squashtest.tm.domain.campaign.QTestSuite.testSuite;
import static org.squashtest.tm.domain.execution.ExecutionStatus.READY;
import static org.squashtest.tm.domain.execution.ExecutionStatus.RUNNING;
import static org.squashtest.tm.domain.testautomation.QAutomatedTest.automatedTest;
import static org.squashtest.tm.domain.testautomation.QTestAutomationProject.testAutomationProject;
import static org.squashtest.tm.domain.testcase.QTestCase.testCase;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_SUITE_WORKFLOWS;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENT_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.AUTOMATED_TEST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CAMPAIGN;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.INFO_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ISSUE_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ITERATION;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.NATURE;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.PARAMETERS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.REFERENCED_DATASET;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.REFERENCED_TEST_CASE;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_AUTOMATED_SUITE_BY_ID_WITH_EXTENDERS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_AUTOMATED_EXECUTION_EXTENDERS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_ITPIS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_TEST_CASES_BY_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AUTOMATED_EXECUTION_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AUTOMATED_SUITE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.OLD_AUTOMATED_EXECUTION_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.OLD_AUTOMATED_SUITE_COUNT;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.DatePart;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record7;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.SelectSelectStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.core.foundation.collection.ColumnFiltering;
import org.squashtest.tm.core.foundation.collection.PagingAndMultiSorting;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.exception.execution.NotUniqueTechnologyForRepositoryException;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.service.internal.dto.AutomatedSuiteDto;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.internal.repository.EntityGraphName;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;
import org.squashtest.tm.service.testautomation.AutomationDeletionCount;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview.SquashAutomProjectPreview;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview.TestCasePreviewInfo;

@Repository
public class HibernateAutomatedSuiteDao implements AutomatedSuiteDao {

    private static final String AUTOMATED_SUITE_COUNT_STATUS = "automatedSuite.countStatuses";

    private static final String ID = "id";

    private static final String UNCHECKED = "unchecked";

    private static final String CREATED_ON = "CREATED_ON";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String EXECUTION_STATUS = "EXECUTION_STATUS";
    private static final String LAST_MODIFIED_ON = "LAST_MODIFIED_ON";
    private static final String LAST_MODIFIED_BY = "LAST_MODIFIED_BY";
    private static final String SUITE_ID = "SUITE_ID";
    private static final String SUITE_ID_PARAM = "suiteId";
    private static final String REPOSITORY = "REPOSITORY";
    private static final String ATTACHMENT_LIST_ID = "ATTACHMENT_LIST_ID";

    @PersistenceContext private EntityManager em;

    @Inject private DSLContext dslContext;

    @Override
    public void delete(String id) {
        AutomatedSuite suite = findById(id);
        em.remove(suite);
    }

    @Override
    public void delete(AutomatedSuite suite) {
        em.remove(suite);
    }

    @Override
    public AutomatedSuite createNewSuite() {
        AutomatedSuite suite = new AutomatedSuite();
        em.persist(suite);
        return suite;
    }

    @Override
    public AutomatedSuite createNewSuite(Iteration iteration) {
        AutomatedSuite suite = new AutomatedSuite(iteration);
        em.persist(suite);
        return suite;
    }

    @Override
    public AutomatedSuite createNewSuite(TestSuite testSuite) {
        AutomatedSuite suite = new AutomatedSuite(testSuite);
        em.persist(suite);
        return suite;
    }

    @Override
    public AutomatedSuite findById(String id) {
        return em.getReference(AutomatedSuite.class, id);
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<AutomatedSuite> findAll() {
        Query query = em.createNamedQuery("automatedSuite.findAll");
        return query.getResultList();
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<AutomatedSuite> findAllByIds(Collection<String> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        } else {
            Query query = em.createNamedQuery("automatedSuite.findAllById");
            query.setParameter("suiteIds", ids);
            return query.getResultList();
        }
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public Collection<AutomatedExecutionExtender> findAllExtenders(String suiteId) {
        Query query = em.createNamedQuery("automatedSuite.findAllExtenders");
        query.setParameter(SUITE_ID_PARAM, suiteId);
        return query.getResultList();
    }

    @Override
    public Collection<AutomatedExecutionExtender> findAllWaitingExtenders(String suiteId) {
        return findAllExtendersByStatus(suiteId, READY);
    }

    @Override
    public Collection<AutomatedExecutionExtender> findAllRunningExtenders(String suiteId) {
        return findAllExtendersByStatus(suiteId, RUNNING);
    }

    @Override
    public Collection<AutomatedExecutionExtender> findAllCompletedExtenders(String suiteId) {
        return findAllExtendersByStatus(suiteId, ExecutionStatus.getTerminatedStatusSet());
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public Collection<AutomatedExecutionExtender> findAllExtendersByStatus(
            final String suiteId, final Collection<ExecutionStatus> statusList) {

        Query query = em.createNamedQuery("automatedSuite.findAllExtendersHavingStatus");

        query.setParameter(SUITE_ID_PARAM, suiteId);

        query.setParameter("statusList", statusList);

        return query.getResultList();
    }

    @Override
    public List<AutomatedExecutionExtender> findAndFetchForAutomatedExecutionCreation(
            String suiteId) {
        List<AutomatedExecutionExtender> extenders =
                new EntityGraphQueryBuilder<>(
                                em,
                                AutomatedExecutionExtender.class,
                                FIND_DISTINCT_AUTOMATED_EXECUTION_EXTENDERS_BY_IDS)
                        .addAttributeNodes(EntityGraphName.EXECUTION)
                        .addSubGraph(EntityGraphName.EXECUTION, ISSUE_LIST, ATTACHMENT_LIST)
                        .executeDistinctList(Map.of(ID, suiteId));

        loadExtenderAttributes(extenders);

        return extenders;
    }

    private void loadExtenderAttributes(List<AutomatedExecutionExtender> extenders) {
        Set<Long> iterationTestPlanIds =
                extenders.stream()
                        .map(extender -> extender.getIterationTestPlanItem().getId())
                        .collect(Collectors.toSet());

        new EntityGraphQueryBuilder<>(em, IterationTestPlanItem.class, FIND_DISTINCT_ITPIS_BY_IDS)
                .addAttributeNodes(
                        REFERENCED_TEST_CASE,
                        EXPLORATORY_SESSION_OVERVIEW,
                        ITERATION,
                        REFERENCED_DATASET,
                        EntityGraphName.TEST_SUITES)
                .addSubGraph(ITERATION, CAMPAIGN)
                .executeDistinctList(Map.of(IDS, iterationTestPlanIds));

        Set<Long> testCaseIds =
                extenders.stream()
                        .map(extender -> extender.getIterationTestPlanItem().getReferencedTestCase().getId())
                        .collect(Collectors.toSet());

        new EntityGraphQueryBuilder<>(em, TestCase.class, FIND_DISTINCT_TEST_CASES_BY_IDS)
                .addAttributeNodes(AUTOMATED_TEST, NATURE, PARAMETERS, EntityGraphName.AUTOMATION_REQUEST)
                .addSubGraph(NATURE, INFO_LIST)
                .executeDistinctList(Map.of(IDS, testCaseIds));
    }

    @Override
    public List<AutomatedSuiteDto> findAutomatedSuitesByIterationID(
            Long iterationId, PagingAndMultiSorting paging, ColumnFiltering filter) {

        int offset = paging.getFirstItemIndex();
        int limit = paging.getPageSize();

        return getSelectFields()
                .from(Tables.AUTOMATED_SUITE)
                .innerJoin(Tables.ITERATION)
                .on(Tables.AUTOMATED_SUITE.ITERATION_ID.eq(Tables.ITERATION.ITERATION_ID))
                .where(Tables.ITERATION.ITERATION_ID.eq(iterationId))
                .union(
                        getSelectFields()
                                .from(Tables.AUTOMATED_SUITE)
                                .innerJoin(Tables.TEST_SUITE)
                                .on(Tables.AUTOMATED_SUITE.TEST_SUITE_ID.eq(Tables.TEST_SUITE.ID))
                                .innerJoin(Tables.ITERATION_TEST_SUITE)
                                .on(Tables.TEST_SUITE.ID.eq(Tables.ITERATION_TEST_SUITE.TEST_SUITE_ID))
                                .innerJoin(Tables.ITERATION)
                                .on(Tables.ITERATION_TEST_SUITE.ITERATION_ID.eq(Tables.ITERATION.ITERATION_ID))
                                .where(Tables.ITERATION.ITERATION_ID.eq(iterationId)))
                .union(
                        getSelectFields()
                                .from(Tables.AUTOMATED_SUITE)
                                .innerJoin(Tables.AUTOMATED_EXECUTION_EXTENDER)
                                .on(
                                        Tables.AUTOMATED_SUITE.SUITE_ID.eq(
                                                Tables.AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .innerJoin(Tables.EXECUTION)
                                .on(
                                        Tables.AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(
                                                Tables.EXECUTION.EXECUTION_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_EXECUTION)
                                .on(Tables.EXECUTION.EXECUTION_ID.eq(Tables.ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                                .on(
                                        Tables.ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                                Tables.ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.ITERATION)
                                .on(Tables.ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(Tables.ITERATION.ITERATION_ID))
                                .where(Tables.ITERATION.ITERATION_ID.eq(iterationId)))
                .orderBy(org.jooq.impl.DSL.field(CREATED_ON).desc())
                .offset(offset)
                .limit(limit)
                .fetchInto(AutomatedSuiteDto.class);
    }

    @Override
    public List<AutomatedSuiteDto> findAutomatedSuitesByTestSuiteID(
            Long suiteId, PagingAndMultiSorting paging, ColumnFiltering filter) {

        int offset = paging.getFirstItemIndex();
        int limit = paging.getPageSize();

        return getSelectFields()
                .from(Tables.AUTOMATED_SUITE)
                .innerJoin(Tables.TEST_SUITE)
                .on(Tables.AUTOMATED_SUITE.TEST_SUITE_ID.eq(Tables.TEST_SUITE.ID))
                .where(Tables.TEST_SUITE.ID.eq(suiteId))
                .union(
                        getSelectFields()
                                .from(Tables.AUTOMATED_SUITE)
                                .innerJoin(Tables.AUTOMATED_EXECUTION_EXTENDER)
                                .on(
                                        Tables.AUTOMATED_SUITE.SUITE_ID.eq(
                                                Tables.AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .innerJoin(Tables.EXECUTION)
                                .on(
                                        Tables.AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(
                                                Tables.EXECUTION.EXECUTION_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_EXECUTION)
                                .on(Tables.EXECUTION.EXECUTION_ID.eq(Tables.ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                                .on(
                                        Tables.ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                                Tables.ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(TEST_SUITE_TEST_PLAN_ITEM)
                                .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.TEST_SUITE)
                                .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(Tables.TEST_SUITE.ID))
                                .where(Tables.TEST_SUITE.ID.eq(suiteId)))
                .orderBy(org.jooq.impl.DSL.field(CREATED_ON).desc())
                .offset(offset)
                .limit(limit)
                .fetchInto(AutomatedSuiteDto.class);
    }

    private SelectSelectStep<Record7<Timestamp, String, String, Timestamp, String, String, Long>>
            getSelectFields() {
        return dslContext.select(
                AUTOMATED_SUITE.CREATED_ON.as(CREATED_ON),
                AUTOMATED_SUITE.CREATED_BY.as(CREATED_BY),
                AUTOMATED_SUITE.EXECUTION_STATUS.as(EXECUTION_STATUS),
                AUTOMATED_SUITE.LAST_MODIFIED_ON.as(LAST_MODIFIED_ON),
                AUTOMATED_SUITE.LAST_MODIFIED_BY.as(LAST_MODIFIED_BY),
                AUTOMATED_SUITE.SUITE_ID.as(SUITE_ID),
                AUTOMATED_SUITE.ATTACHMENT_LIST_ID.as(ATTACHMENT_LIST_ID));
    }

    @Override
    public long countSuitesByIterationId(Long iterationId, ColumnFiltering filter) {

        return dslContext
                .select(Tables.AUTOMATED_SUITE.SUITE_ID)
                .from(Tables.AUTOMATED_SUITE)
                .innerJoin(Tables.ITERATION)
                .on(Tables.AUTOMATED_SUITE.ITERATION_ID.eq(Tables.ITERATION.ITERATION_ID))
                .where(Tables.ITERATION.ITERATION_ID.eq(iterationId))
                .union(
                        dslContext
                                .select(Tables.AUTOMATED_SUITE.SUITE_ID)
                                .from(Tables.AUTOMATED_SUITE)
                                .innerJoin(Tables.TEST_SUITE)
                                .on(Tables.AUTOMATED_SUITE.TEST_SUITE_ID.eq(Tables.TEST_SUITE.ID))
                                .innerJoin(Tables.ITERATION_TEST_SUITE)
                                .on(Tables.TEST_SUITE.ID.eq(Tables.ITERATION_TEST_SUITE.TEST_SUITE_ID))
                                .innerJoin(Tables.ITERATION)
                                .on(Tables.ITERATION_TEST_SUITE.ITERATION_ID.eq(Tables.ITERATION.ITERATION_ID))
                                .where(Tables.ITERATION.ITERATION_ID.eq(iterationId)))
                .union(
                        dslContext
                                .select(Tables.AUTOMATED_SUITE.SUITE_ID)
                                .from(Tables.AUTOMATED_SUITE)
                                .innerJoin(Tables.AUTOMATED_EXECUTION_EXTENDER)
                                .on(
                                        Tables.AUTOMATED_SUITE.SUITE_ID.eq(
                                                Tables.AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .innerJoin(Tables.EXECUTION)
                                .on(
                                        Tables.AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(
                                                Tables.EXECUTION.EXECUTION_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_EXECUTION)
                                .on(Tables.EXECUTION.EXECUTION_ID.eq(Tables.ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                                .on(
                                        Tables.ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                                Tables.ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.ITERATION)
                                .on(Tables.ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(Tables.ITERATION.ITERATION_ID))
                                .where(Tables.ITERATION.ITERATION_ID.eq(iterationId)))
                .fetch()
                .stream()
                .count();
    }

    @Override
    public long countSuitesByTestSuiteId(Long suiteId, ColumnFiltering filter) {

        return dslContext
                .select(Tables.AUTOMATED_SUITE.SUITE_ID)
                .from(Tables.AUTOMATED_SUITE)
                .innerJoin(Tables.TEST_SUITE)
                .on(Tables.AUTOMATED_SUITE.TEST_SUITE_ID.eq(Tables.TEST_SUITE.ID))
                .where(Tables.TEST_SUITE.ID.eq(suiteId))
                .union(
                        dslContext
                                .select(Tables.AUTOMATED_SUITE.SUITE_ID)
                                .from(Tables.AUTOMATED_SUITE)
                                .innerJoin(Tables.AUTOMATED_EXECUTION_EXTENDER)
                                .on(
                                        Tables.AUTOMATED_SUITE.SUITE_ID.eq(
                                                Tables.AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .innerJoin(Tables.EXECUTION)
                                .on(
                                        Tables.AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(
                                                Tables.EXECUTION.EXECUTION_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_EXECUTION)
                                .on(Tables.EXECUTION.EXECUTION_ID.eq(Tables.ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                                .on(
                                        Tables.ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                                Tables.ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(TEST_SUITE_TEST_PLAN_ITEM)
                                .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(Tables.TEST_SUITE)
                                .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(Tables.TEST_SUITE.ID))
                                .where(Tables.TEST_SUITE.ID.eq(suiteId)))
                .fetch()
                .stream()
                .count();
    }

    @Override
    public ExecutionStatusReport getStatusReport(String uuid) {
        ExecutionStatusReport report = new ExecutionStatusReport();

        Query query = em.createNamedQuery(AUTOMATED_SUITE_COUNT_STATUS);
        query.setParameter(ID, uuid);

        List<Object[]> tuples = query.getResultList();

        for (Object[] tuple : tuples) {
            report.set(((ExecutionStatus) tuple[0]).getCanonicalStatus(), ((Long) tuple[1]).intValue());
        }

        return report;
    }

    // TODO : either make it private (core Squash at least doesn't call it anywhere but here), either
    // declare it in the interface
    public Collection<AutomatedExecutionExtender> findAllExtendersByStatus(
            String suiteId, ExecutionStatus... statusArray) {
        Collection<ExecutionStatus> statusList = Arrays.asList(statusArray);
        return findAllExtendersByStatus(suiteId, statusList);
    }

    @Override
    public List<Couple<TestAutomationProject, Long>> findAllCalledByTestPlan(
            EntityReference context, Collection<Long> testPlanSubset, Long userId) {

        // init the query
        JPAQuery<Couple<TestAutomationProject, Long>> query =
                createBaseTestplanQueryFromSpec(context, testPlanSubset, userId);

        // now set the select clause
        query =
                query.select(
                        Projections.constructor(
                                Couple.class,
                                testAutomationProject,
                                iterationTestPlanItem.count().as("itemCount")));

        // and the group by, order etc
        query = query.groupBy(testAutomationProject).orderBy(testAutomationProject.label.asc());

        // return
        return query.fetch();
    }

    @Override
    public List<String> findTestPathForAutomatedSuiteAndProject(
            EntityReference context,
            Collection<Long> testPlanSubset,
            long automationProjectId,
            Long userId) {

        // init the query
        JPAQuery<String> query = createBaseTestplanQueryFromSpec(context, testPlanSubset, userId);

        // select clause
        query =
                query.select(testAutomationProject.label.concat("/").concat(automatedTest.name).as("path"));

        // another where clause
        query = query.where(testAutomationProject.id.eq(automationProjectId));

        // order by
        query.orderBy(automatedTest.name.asc());

        return query.fetch();
    }

    /*
    Private function used by findAllCalledByTestPlan and findTestPathForAutomatedSuiteAndProject.
    This function will create a headless base query that will care neither of the select clause nor group by etc.
    The caller will do whatever it needs with the result.
    */
    private <T> JPAQuery<T> createBaseTestplanQueryFromSpec(
            EntityReference context, Collection<Long> testPlanSubset, Long userId) {
        // context must be not null and reference either an iteration or a test suite.
        assertContextIsValid(context);

        EntityType type = context.getType();
        Long id = context.getId();

        // init the querydsl context
        JPAQueryFactory factory = new JPAQueryFactory(em);
        JPAQuery<T> query = null;

        // initialize the initial selected entity
        if (type == EntityType.ITERATION) {
            query =
                    (JPAQuery<T>)
                            factory
                                    .from(iteration)
                                    .innerJoin(iteration.testPlans, iterationTestPlanItem)
                                    .where(iteration.id.eq(id));

            if (Objects.nonNull(userId)) {
                query = query.where(iterationTestPlanItem.user.id.eq(userId));
            }
        } else {
            query =
                    (JPAQuery<T>)
                            factory
                                    .from(testSuite)
                                    .innerJoin(testSuite.testPlan, iterationTestPlanItem)
                                    .where(testSuite.id.eq(id));
        }

        // if a test plan subset is defined, apply it
        // note : this is the second time we invoke where(...), hopefully it is treated as a AND
        // condition regarding
        // the first clause, and that is what we need. Otherwise we would need to build the where clause
        // apart.
        if (testPlanSubset != null && !testPlanSubset.isEmpty()) {
            query = query.where(iterationTestPlanItem.id.in(testPlanSubset));
        }

        // the rest of the query
        query =
                query
                        .innerJoin(iterationTestPlanItem.referencedTestCase, testCase)
                        .innerJoin(testCase.automatedTest, automatedTest)
                        .innerJoin(automatedTest.project, testAutomationProject);

        return query;
    }

    private void assertContextIsValid(EntityReference context) {
        if (context == null
                || !(context.getType() != EntityType.ITERATION
                        || context.getType() != EntityType.TEST_SUITE)) {
            throw new IllegalArgumentException(
                    "invalid context : expected a reference to an Iteration or a TestSuite, but got "
                            + context);
        }
    }

    @Override
    public AutomationDeletionCount countOldAutomatedSuitesAndExecutions() {
        LocalDateTime todayLocalDateTime = LocalDateTime.now();
        Instant todayInstant = todayLocalDateTime.atZone(ZoneId.systemDefault()).toInstant();
        Timestamp todayTimestamp = Timestamp.from(todayInstant);
        return countOldAutomatedSuiteAndExecutionsByCondition(
                timestampAdd(AUTOMATED_SUITE.CREATED_ON, PROJECT.AUTOMATED_SUITES_LIFETIME, DatePart.DAY)
                        .lessThan(todayTimestamp));
    }

    @Override
    public AutomationDeletionCount countOldAutomatedSuitesAndExecutionsByProjectId(Long projectId) {
        LocalDateTime todayLocalDateTime = LocalDateTime.now();
        Instant todayInstant = todayLocalDateTime.atZone(ZoneId.systemDefault()).toInstant();
        Timestamp todayTimestamp = Timestamp.from(todayInstant);
        return countOldAutomatedSuiteAndExecutionsByCondition(
                PROJECT
                        .PROJECT_ID
                        .eq(projectId)
                        .and(
                                timestampAdd(
                                                AUTOMATED_SUITE.CREATED_ON, PROJECT.AUTOMATED_SUITES_LIFETIME, DatePart.DAY)
                                        .lessThan(todayTimestamp)));
    }

    private AutomationDeletionCount countOldAutomatedSuiteAndExecutionsByCondition(
            Condition condition) {
        Condition iterationAndTestSuiteCondition =
                condition.and(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.isNull());
        Table<Record2<Integer, Integer>> countTable =
                countOldAutomatedSuitesAndExecutionsLinkedToExecutionsWhereCondition(condition)
                        .union(
                                countOldAutomatedSuitesLinkedToIterationWhereCondition(
                                        iterationAndTestSuiteCondition))
                        .union(
                                countOldAutomatedSuitesLinkedToTestSuiteWhereCondition(
                                        iterationAndTestSuiteCondition))
                        .asTable();
        return dslContext
                .select(
                        sum(countTable.field(AUTOMATED_SUITE_COUNT, Integer.class))
                                .as(OLD_AUTOMATED_SUITE_COUNT),
                        sum(countTable.field(AUTOMATED_EXECUTION_COUNT, Integer.class))
                                .as(OLD_AUTOMATED_EXECUTION_COUNT))
                .from(countTable)
                .fetchOneInto(AutomationDeletionCount.class);
    }

    private SelectConditionStep<Record2<Integer, Integer>>
            countOldAutomatedSuitesAndExecutionsLinkedToExecutionsWhereCondition(Condition condition) {
        return dslContext
                .select(
                        countDistinct(AUTOMATED_SUITE.SUITE_ID).as(AUTOMATED_SUITE_COUNT),
                        countDistinct(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID).as(AUTOMATED_EXECUTION_COUNT))
                .from(AUTOMATED_SUITE)
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(
                        AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(
                                ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(condition);
    }

    private SelectConditionStep<Record2<Integer, Integer>>
            countOldAutomatedSuitesLinkedToIterationWhereCondition(Condition condition) {
        return dslContext
                .select(countDistinct(AUTOMATED_SUITE.SUITE_ID), DSL.field("0", Integer.class))
                .from(AUTOMATED_SUITE)
                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(AUTOMATED_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(condition);
    }

    private SelectConditionStep<Record2<Integer, Integer>>
            countOldAutomatedSuitesLinkedToTestSuiteWhereCondition(Condition condition) {
        return dslContext
                .select(countDistinct(AUTOMATED_SUITE.SUITE_ID), DSL.field("0", Integer.class))
                .from(AUTOMATED_SUITE)
                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .innerJoin(ITERATION_TEST_SUITE)
                .on(ITERATION_TEST_SUITE.TEST_SUITE_ID.eq(AUTOMATED_SUITE.TEST_SUITE_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(condition);
    }

    @Override
    public List<String> getOldAutomatedSuiteIds() {
        LocalDateTime todayLocalDateTime = LocalDateTime.now();
        Instant todayInstant = todayLocalDateTime.atZone(ZoneId.systemDefault()).toInstant();
        Timestamp todayTimestamp = Timestamp.from(todayInstant);
        return getOldAutomatedSuitesIdsByCondition(
                timestampAdd(AUTOMATED_SUITE.CREATED_ON, PROJECT.AUTOMATED_SUITES_LIFETIME, DatePart.DAY)
                        .lessThan(todayTimestamp));
    }

    @Override
    public List<String> getOldAutomatedSuiteIdsByProjectId(Long projectId) {
        LocalDateTime todayLocalDateTime = LocalDateTime.now();
        Instant todayInstant = todayLocalDateTime.atZone(ZoneId.systemDefault()).toInstant();
        Timestamp todayTimestamp = Timestamp.from(todayInstant);
        return getOldAutomatedSuitesIdsByCondition(
                PROJECT
                        .PROJECT_ID
                        .eq(projectId)
                        .and(
                                timestampAdd(
                                                AUTOMATED_SUITE.CREATED_ON, PROJECT.AUTOMATED_SUITES_LIFETIME, DatePart.DAY)
                                        .lessThan(todayTimestamp)));
    }

    private List<String> getOldAutomatedSuitesIdsByCondition(Condition condition) {
        Condition iterationAndTestSuiteCondition =
                condition.and(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.isNull());

        return getOldAutomatedSuitesIdLinkedToExecutionWhereCondition(condition)
                .union(
                        getOldAutomatedSuitesIdLinkedToIterationWhereCondition(iterationAndTestSuiteCondition))
                .union(
                        getOldAutomatedSuitesIdLinkedToTestSuiteWhereCondition(iterationAndTestSuiteCondition))
                .fetch(AUTOMATED_SUITE.SUITE_ID, String.class);
    }

    private SelectConditionStep<Record1<String>>
            getOldAutomatedSuitesIdLinkedToExecutionWhereCondition(Condition condition) {
        return dslContext
                .selectDistinct(AUTOMATED_SUITE.SUITE_ID)
                .from(AUTOMATED_SUITE)
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(
                        AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(
                                ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(condition);
    }

    private SelectConditionStep<Record1<String>>
            getOldAutomatedSuitesIdLinkedToIterationWhereCondition(Condition condition) {
        return dslContext
                .selectDistinct(AUTOMATED_SUITE.SUITE_ID)
                .from(AUTOMATED_SUITE)
                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(AUTOMATED_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(condition);
    }

    private SelectConditionStep<Record1<String>>
            getOldAutomatedSuitesIdLinkedToTestSuiteWhereCondition(Condition condition) {
        return dslContext
                .selectDistinct(AUTOMATED_SUITE.SUITE_ID)
                .from(AUTOMATED_SUITE)
                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .innerJoin(ITERATION_TEST_SUITE)
                .on(ITERATION_TEST_SUITE.TEST_SUITE_ID.eq(AUTOMATED_SUITE.TEST_SUITE_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(condition);
    }

    @Override
    public void deleteAllByIds(List<String> automatedSuiteIds) {
        if (automatedSuiteIds.isEmpty()) {
            return;
        }
        Query deleteQuery = em.createNamedQuery("AutomatedSuite.deleteAllByIds");
        deleteQuery.setParameter("automatedSuiteIds", automatedSuiteIds);
        deleteQuery.executeUpdate();
    }

    @Override
    public boolean itpiSelectionContainsSquashAutomTest(
            EntityReference context, List<Long> testPlanSubsetIds) {
        assertContextIsValid(context);

        EntityType contextType = context.getType();
        Long entityId = context.getId();
        boolean contextIsIteration = EntityType.ITERATION.equals(contextType);

        SelectOnConditionStep<?> queryBeginning =
                getBeginningOfContainsSquashAutomTestQuery(contextIsIteration);
        SelectOnConditionStep<?> queryWithCommonInnerJoins =
                buildCommonInnerJoinClauseOfContainsSquashAutomTestQuery(queryBeginning);
        SelectConditionStep<?> queryWithWhereClause =
                buildWhereClauseOfSquashAutomQueryWithContext(
                        entityId, contextIsIteration, queryWithCommonInnerJoins);
        SelectConditionStep<?> queryWithAndClauses =
                buildCommonWhereClauseOfSquashAutomQuery(queryWithWhereClause);
        SelectConditionStep<?> queryWithPotentialItemSubset =
                buildAndClauseOfSquashAutomQueryWithTestPlanSubset(testPlanSubsetIds, queryWithAndClauses);

        return dslContext.fetchExists(queryWithPotentialItemSubset);
    }

    private SelectOnConditionStep<?> getBeginningOfContainsSquashAutomTestQuery(
            boolean contextIsIteration) {
        if (contextIsIteration) {
            return getBeginningOfContainsSquashAutomTestQueryWithIterationContext();
        } else {
            return getBeginningOfContainsSquashAutomTestQueryWithTestSuiteContext();
        }
    }

    private SelectOnConditionStep<?>
            getBeginningOfContainsSquashAutomTestQueryWithIterationContext() {
        SelectSelectStep<?> selectQuery = dslContext.select(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID);
        return buildFromClauseOfSquashAutomQueryForIterationContext(selectQuery, null);
    }

    private SelectOnConditionStep<?>
            getBeginningOfContainsSquashAutomTestQueryWithTestSuiteContext() {
        SelectSelectStep<?> selectQuery = dslContext.select(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID);
        return buildFromClauseOfSquashAutomQueryForTestSuiteContext(selectQuery);
    }

    private SelectOnConditionStep<?> buildCommonInnerJoinClauseOfContainsSquashAutomTestQuery(
            SelectOnConditionStep<?> queryBeginning) {
        return queryBeginning
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID))
                .innerJoin(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(TEST_AUTOMATION_SERVER)
                .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID));
    }

    @Override
    public Map<Long, SquashAutomProjectPreview> findAllSquashAutomProjectPreviews(
            EntityReference context, List<Long> testPlanSubsetIds, @Nullable Long userId) {
        assertContextIsValid(context);
        if (testPlanSubsetIds.size() != 1) {
            checkUniqueTechnologyInSameRepository(context, testPlanSubsetIds);
        }

        EntityType contextType = context.getType();
        Long entityId = context.getId();
        boolean contextIsIteration = EntityType.ITERATION.equals(contextType);

        Map<Long, SquashAutomProjectPreview> automProjectPreviewsMap = new HashMap<>();

        SelectSelectStep<?> selectQuery = getSelectClauseOfProjectPreviewsQuery();
        SelectOnConditionStep<?> queryBeginning =
                buildBeginningOfProjectPreviewsQueryWithContext(contextIsIteration, selectQuery, userId);
        SelectOnConditionStep<?> queryWithCommonInnerJoins =
                buildCommonWithClauseForProjectPreviewsQuery(queryBeginning);
        SelectConditionStep<?> queryWithWhereClause =
                buildWhereClauseOfSquashAutomQueryWithContext(
                        entityId, contextIsIteration, queryWithCommonInnerJoins);
        SelectConditionStep<?> queryWithAndClauses =
                buildCommonWhereClauseOfSquashAutomQuery(queryWithWhereClause);
        SelectConditionStep<?> queryWithPotentialItemSubset =
                buildAndClauseOfSquashAutomQueryWithTestPlanSubset(testPlanSubsetIds, queryWithAndClauses);

        queryWithPotentialItemSubset
                .fetch()
                .forEach(
                        record -> {
                            long projectId = record.get(PROJECT.PROJECT_ID);
                            SquashAutomProjectPreview automProjectPreview =
                                    automProjectPreviewsMap.get(projectId);
                            if (Objects.isNull(automProjectPreview)) {
                                automProjectPreview =
                                        new SquashAutomProjectPreview(
                                                projectId,
                                                record.get(PROJECT.NAME),
                                                record.get(THIRD_PARTY_SERVER.SERVER_ID),
                                                record.get(THIRD_PARTY_SERVER.NAME));
                                automProjectPreviewsMap.put(projectId, automProjectPreview);
                            }

                            checkTestCaseIsAutomatedAndAddToAutomProjectPreview(record, automProjectPreview);
                        });

        return automProjectPreviewsMap;
    }

    @Override
    public List<Long> findAttachmentListIdsByIds(List<String> automatedSuiteIds) {
        return dslContext
                .select(AUTOMATED_SUITE.ATTACHMENT_LIST_ID)
                .from(AUTOMATED_SUITE)
                .where(AUTOMATED_SUITE.SUITE_ID.in(automatedSuiteIds))
                .fetchInto(Long.class);
    }

    @Override
    public AutomatedSuiteWorkflow getAutomatedSuiteWorkflowByWorkflowId(String workflowId) {
        return dslContext
                .select(
                        AUTOMATED_SUITE_WORKFLOWS.WORKFLOW_ID,
                        AUTOMATED_SUITE_WORKFLOWS.SUITE_ID,
                        AUTOMATED_SUITE_WORKFLOWS.PROJECT_ID)
                .from(AUTOMATED_SUITE_WORKFLOWS)
                .where(AUTOMATED_SUITE_WORKFLOWS.WORKFLOW_ID.eq(workflowId))
                .fetchOneInto(AutomatedSuiteWorkflow.class);
    }

    @Override
    public Map<String, List<AutomatedSuiteWorkflow>> getAutomatedSuiteWorkflowsBySuiteIds(
            List<String> suiteIds) {
        return dslContext
                .select(
                        AUTOMATED_SUITE_WORKFLOWS.WORKFLOW_ID,
                        AUTOMATED_SUITE_WORKFLOWS.SUITE_ID,
                        AUTOMATED_SUITE_WORKFLOWS.PROJECT_ID)
                .from(AUTOMATED_SUITE_WORKFLOWS)
                .where(AUTOMATED_SUITE_WORKFLOWS.SUITE_ID.in(suiteIds))
                .fetchGroups(AUTOMATED_SUITE_WORKFLOWS.SUITE_ID, AutomatedSuiteWorkflow.class);
    }

    @Override
    public void deleteAutomatedSuiteWorkflowsBySuiteIds(List<String> suiteIds) {
        dslContext
                .deleteFrom(AUTOMATED_SUITE_WORKFLOWS)
                .where(AUTOMATED_SUITE_WORKFLOWS.SUITE_ID.in(suiteIds))
                .execute();
    }

    @Override
    public Map<Long, Long> getAutomatedExecutionIdsWithAttachmentListIds(
            List<String> automatedSuiteIds, boolean complete) {
        Condition baseCondition = AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.in(automatedSuiteIds);

        Condition condition =
                complete
                        ? baseCondition
                        : baseCondition.and(EXECUTION.EXECUTION_STATUS.eq(ExecutionStatus.SUCCESS.name()));

        return dslContext
                .select(EXECUTION.EXECUTION_ID, EXECUTION.ATTACHMENT_LIST_ID)
                .from(EXECUTION)
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(EXECUTION.EXECUTION_ID.eq(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID))
                .where(condition)
                .fetchMap(EXECUTION.EXECUTION_ID, EXECUTION.ATTACHMENT_LIST_ID);
    }

    @Override
    public AutomatedSuite createNewSuiteWithLinkToTestSuite(TestSuite testSuite) {
        AutomatedSuite suite = new AutomatedSuite(testSuite);
        em.persist(suite);
        return suite;
    }

    @Override
    public AutomatedSuite createNewSuiteWithLinkToIteration(Iteration iteration) {
        AutomatedSuite suite = new AutomatedSuite(iteration);
        em.persist(suite);
        return suite;
    }

    private void checkTestCaseIsAutomatedAndAddToAutomProjectPreview(
            Record record, SquashAutomProjectPreview automProjectPreview) {
        /*  SQUASH-6133/6110 only return test cases that are considered automated
        i.e. with NONE workflow and autom fields or with NATIVE & NATIVE SIMPLIFIED workflow,
        is automatable and has an AUTOMATED request status */
        if ("NONE".equalsIgnoreCase(record.get(PROJECT.AUTOMATION_WORKFLOW_TYPE))
                || ("Y".equals(record.get(TEST_CASE.AUTOMATABLE))
                        && "AUTOMATED".equals(record.get(AUTOMATION_REQUEST.REQUEST_STATUS)))) {
            automProjectPreview
                    .getTestCases()
                    .add(
                            new TestCasePreviewInfo(
                                    record.get(TEST_CASE.REFERENCE),
                                    record.get(TEST_CASE_LIBRARY_NODE.NAME),
                                    record.get(DATASET.NAME),
                                    record.get(TEST_CASE.IMPORTANCE)));
        }
    }

    private SelectSelectStep<?> getSelectClauseOfProjectPreviewsQuery() {
        return dslContext.select(
                PROJECT.PROJECT_ID,
                PROJECT.NAME,
                PROJECT.AUTOMATION_WORKFLOW_TYPE,
                THIRD_PARTY_SERVER.SERVER_ID,
                THIRD_PARTY_SERVER.NAME,
                TEST_CASE.REFERENCE,
                TEST_CASE_LIBRARY_NODE.NAME,
                TEST_CASE.AUTOMATABLE,
                TEST_CASE.IMPORTANCE,
                AUTOMATION_REQUEST.REQUEST_STATUS,
                DATASET.NAME);
    }

    private SelectOnConditionStep<?> buildBeginningOfProjectPreviewsQueryWithContext(
            boolean contextIsIteration, SelectSelectStep<?> selectQuery, Long userId) {
        if (contextIsIteration) {
            return buildFromClauseOfSquashAutomQueryForIterationContext(selectQuery, userId);
        } else {
            return buildFromClauseOfSquashAutomQueryForTestSuiteContext(selectQuery);
        }
    }

    private SelectOnConditionStep<?> buildFromClauseOfSquashAutomQueryForIterationContext(
            SelectSelectStep<?> selectQuery, @Nullable Long userId) {
        return selectQuery
                .from(ITEM_TEST_PLAN_LIST)
                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITPIJoinCondition(userId));
    }

    private Condition ITPIJoinCondition(Long userId) {
        Condition condition =
                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID);
        return Objects.nonNull(userId)
                ? condition.and(ITERATION_TEST_PLAN_ITEM.USER_ID.eq(userId))
                : condition;
    }

    private SelectOnConditionStep<?> buildFromClauseOfSquashAutomQueryForTestSuiteContext(
            SelectSelectStep<?> selectQuery) {
        return selectQuery
                .from(TEST_SUITE_TEST_PLAN_ITEM)
                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID));
    }

    private SelectOnConditionStep<?> buildCommonWithClauseForProjectPreviewsQuery(
            SelectOnConditionStep<?> queryBeginning) {
        return queryBeginning
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID))
                .innerJoin(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(THIRD_PARTY_SERVER)
                .on(THIRD_PARTY_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
                .innerJoin(TEST_AUTOMATION_SERVER)
                .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID))
                .leftJoin(DATASET)
                .on(DATASET.DATASET_ID.eq(ITERATION_TEST_PLAN_ITEM.DATASET_ID));
    }

    private SelectConditionStep<?> buildWhereClauseOfSquashAutomQueryWithContext(
            Long entityId,
            boolean contextIsIteration,
            SelectOnConditionStep<?> queryWithCommonInnerJoins) {
        if (contextIsIteration) {
            return queryWithCommonInnerJoins.where(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(entityId));
        } else {
            return queryWithCommonInnerJoins.where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(entityId));
        }
    }

    private SelectConditionStep<?> buildCommonWhereClauseOfSquashAutomQuery(
            SelectConditionStep<?> queryWithWhereClause) {
        return queryWithWhereClause
                .and(TEST_AUTOMATION_SERVER.KIND.eq(TestAutomationServerKind.squashOrchestrator.name()))
                .and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
                .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
                .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull());
    }

    private SelectConditionStep<?> buildAndClauseOfSquashAutomQueryWithTestPlanSubset(
            List<Long> testPlanSubsetIds, SelectConditionStep<?> queryWithAndClauses) {
        if (Objects.nonNull(testPlanSubsetIds) && !testPlanSubsetIds.isEmpty()) {
            return queryWithAndClauses.and(
                    ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(testPlanSubsetIds));
        } else {
            return queryWithAndClauses;
        }
    }

    private void checkUniqueTechnologyInSameRepository(
            EntityReference context, List<Long> testPlanSubset) {

        Table<?> subRequestTable = getSubRequestTable(context, testPlanSubset);

        List<Long> repositoryIdsWithMultipleTechnologies =
                dslContext
                        .select(subRequestTable.field(REPOSITORY))
                        .from(subRequestTable)
                        .groupBy(subRequestTable.field(REPOSITORY))
                        .having(org.jooq.impl.DSL.count().gt(1))
                        .fetchInto(Long.class);

        if (!repositoryIdsWithMultipleTechnologies.isEmpty()) {
            throw new NotUniqueTechnologyForRepositoryException();
        }
    }

    private Table<?> getSubRequestTable(EntityReference context, List<Long> testPlanSubset) {
        SelectSelectStep<Record2<Long, Long>> select =
                dslContext.select(
                        Tables.TEST_CASE.SCM_REPOSITORY_ID.as(REPOSITORY),
                        Tables.TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.as("TECHNOLOGY"));

        SelectJoinStep<Record2<Long, Long>> from =
                select
                        .from(Tables.ITERATION_TEST_PLAN_ITEM)
                        .innerJoin(Tables.TEST_CASE)
                        .on(Tables.TEST_CASE.TCLN_ID.eq(Tables.ITERATION_TEST_PLAN_ITEM.TCLN_ID))
                        .innerJoin(Tables.TEST_CASE_LIBRARY_NODE)
                        .on(Tables.TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(Tables.TEST_CASE.TCLN_ID))
                        .innerJoin(Tables.PROJECT)
                        .on(Tables.PROJECT.PROJECT_ID.eq(Tables.TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                        .innerJoin(Tables.TEST_AUTOMATION_SERVER)
                        .on(Tables.TEST_AUTOMATION_SERVER.SERVER_ID.eq(Tables.PROJECT.TA_SERVER_ID));

        addJoinAndWhereClauses(context, testPlanSubset, from);

        return from.groupBy(
                        Tables.TEST_CASE.SCM_REPOSITORY_ID, Tables.TEST_CASE.AUTOMATED_TEST_TECHNOLOGY)
                .asTable();
    }

    private void addJoinAndWhereClauses(
            EntityReference context,
            List<Long> testPlanSubset,
            SelectJoinStep<Record2<Long, Long>> from) {
        if (testPlanSubset.size() > 1) {
            from.where(
                    Tables.ITERATION_TEST_PLAN_ITEM
                            .ITEM_TEST_PLAN_ID
                            .in(testPlanSubset)
                            .and(
                                    Tables.TEST_AUTOMATION_SERVER.KIND.eq(
                                            TestAutomationServerKind.squashOrchestrator.name())));
        } else if (EntityType.ITERATION.equals(context.getType())) {
            from.innerJoin(Tables.ITEM_TEST_PLAN_LIST)
                    .on(
                            Tables.ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                    Tables.ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                    .where(
                            Tables.ITEM_TEST_PLAN_LIST
                                    .ITERATION_ID
                                    .eq(context.getId())
                                    .and(
                                            Tables.TEST_AUTOMATION_SERVER.KIND.eq(
                                                    TestAutomationServerKind.squashOrchestrator.name())));
        } else if (EntityType.TEST_SUITE.equals(context.getType())) {
            from.innerJoin(Tables.TEST_SUITE_TEST_PLAN_ITEM)
                    .on(
                            Tables.TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(
                                    Tables.ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                    .where(
                            Tables.TEST_SUITE_TEST_PLAN_ITEM
                                    .SUITE_ID
                                    .eq(context.getId())
                                    .and(
                                            Tables.TEST_AUTOMATION_SERVER.KIND.eq(
                                                    TestAutomationServerKind.squashOrchestrator.name())));
        }
    }

    @Override
    public AutomatedSuite findByIdWithExtenders(String suiteId) {
        return em.createQuery(FIND_AUTOMATED_SUITE_BY_ID_WITH_EXTENDERS, AutomatedSuite.class)
                .setParameter(ID, suiteId)
                .getSingleResult();
    }

    @Override
    public boolean isAutomatedSuiteCreatedByTestAutomationServerUser(String automatedSuiteId) {
        return dslContext.fetchExists(
                dslContext
                        .select(AUTOMATED_SUITE.CREATED_BY)
                        .from(AUTOMATED_SUITE)
                        .join(CORE_USER)
                        .on(CORE_USER.LOGIN.eq(AUTOMATED_SUITE.CREATED_BY))
                        .join(CORE_GROUP_MEMBER)
                        .on(CORE_USER.PARTY_ID.eq(CORE_GROUP_MEMBER.PARTY_ID))
                        .join(CORE_GROUP)
                        .on(CORE_GROUP_MEMBER.GROUP_ID.eq(CORE_GROUP.ID))
                        .where(AUTOMATED_SUITE.SUITE_ID.eq(automatedSuiteId))
                        .and(
                                CORE_GROUP.QUALIFIED_NAME.eq(
                                        org.squashtest.tm.domain.users.UsersGroup.TEST_AUTOMATION_SERVER)));
    }
}
