/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.execution;

import java.util.Date;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.UserView;

public class ExploratorySessionOverviewView {

    private long id;
    private String name;
    private String reference;
    private long projectId;
    private long iterationId;
    private long testPlanItemId;
    private String charter;
    private int sessionDuration;
    private String path;
    private String executionStatus;
    private String sessionStatus;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private Date createdOn;
    private String createdBy;
    private Date dueDate;
    private List<UserView> assignableUsers;
    private int nbIssues;
    private int nbExecutions;
    private int nbNotes;
    private String inferredSessionReviewStatus;
    private String comments;
    private Long attachmentListId;
    private AttachmentListDto attachmentList;
    private List<MilestoneDto> milestones;
    private SprintStatus sprintStatus;
    private Long tclnId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @CleanHtml
    public String getCharter() {
        return charter;
    }

    public void setCharter(String charter) {
        this.charter = charter;
    }

    public int getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(int sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(String executionStatus) {
        this.executionStatus = executionStatus;
    }

    public String getSessionStatus() {
        return sessionStatus;
    }

    public void setSessionStatus(String sessionStatus) {
        this.sessionStatus = sessionStatus;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public List<UserView> getAssignableUsers() {
        return assignableUsers;
    }

    public void setAssignableUsers(List<UserView> assignableUsers) {
        this.assignableUsers = assignableUsers;
    }

    public long getIterationId() {
        return iterationId;
    }

    public void setIterationId(long iterationId) {
        this.iterationId = iterationId;
    }

    public long getTestPlanItemId() {
        return testPlanItemId;
    }

    public void setTestPlanItemId(long testPlanItemId) {
        this.testPlanItemId = testPlanItemId;
    }

    public int getNbIssues() {
        return nbIssues;
    }

    public void setNbIssues(int nbIssues) {
        this.nbIssues = nbIssues;
    }

    public int getNbExecutions() {
        return nbExecutions;
    }

    public void setNbExecutions(int nbExecutions) {
        this.nbExecutions = nbExecutions;
    }

    public int getNbNotes() {
        return nbNotes;
    }

    public void setNbNotes(int nbNotes) {
        this.nbNotes = nbNotes;
    }

    public String getInferredSessionReviewStatus() {
        return inferredSessionReviewStatus;
    }

    public void setInferredSessionReviewStatus(String inferredSessionReviewStatus) {
        this.inferredSessionReviewStatus = inferredSessionReviewStatus;
    }

    @CleanHtml
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getAttachmentListId() {
        return attachmentListId;
    }

    public void setAttachmentListId(Long attachmentListId) {
        this.attachmentListId = attachmentListId;
    }

    public AttachmentListDto getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(AttachmentListDto attachmentList) {
        this.attachmentList = attachmentList;
    }

    public List<MilestoneDto> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<MilestoneDto> milestones) {
        this.milestones = milestones;
    }

    public SprintStatus getSprintStatus() {
        return sprintStatus;
    }

    public void setSprintStatus(SprintStatus sprintStatus) {
        this.sprintStatus = sprintStatus;
    }

    public Long getTclnId() {
        return tclnId;
    }

    public void setTclnId(Long testCaseId) {
        this.tclnId = testCaseId;
    }
}
