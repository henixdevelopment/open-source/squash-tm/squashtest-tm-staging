/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.value;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK_TYPE;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RESOURCE;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SYNCHRONISATION_ID;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jooq.DSLContext;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.Record6;
import org.jooq.impl.SQLDataType;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.jooq.domain.tables.Resource;
import org.squashtest.tm.jooq.domain.tables.RlnResource;
import org.squashtest.tm.service.internal.dto.ReqVersionMilestone;
import org.squashtest.tm.service.internal.repository.RequirementImportDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionCoverageDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;

@Repository
public class RequirementImportDaoImpl implements RequirementImportDao {

    private static final RlnResource PARENT_RlN_RESOURCE = RLN_RESOURCE.as("PARENT_RLN_RESOURCE");
    private static final Resource PARENT_RESOURCE = RESOURCE.as("PARENT_RESOURCE");

    private final DSLContext dsl;
    private final RequirementVersionCoverageDao coverageDao;
    private final RequirementVersionDao requirementVersionDao;

    public RequirementImportDaoImpl(
            DSLContext dsl,
            RequirementVersionCoverageDao coverageDao,
            RequirementVersionDao requirementVersionDao) {
        this.dsl = dsl;
        this.coverageDao = coverageDao;
        this.requirementVersionDao = requirementVersionDao;
    }

    @Override
    public Stream<Record5<String, Long, Boolean, String, Long>> findRemoteSyncRequirements(
            Collection<String> remoteKeys, Collection<Long> remoteSyncId) {

        return dsl.select(
                        concat(
                                        PROJECT.NAME,
                                        value("/"),
                                        groupConcat(RESOURCE.NAME)
                                                .orderBy(RLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                                .separator("/"))
                                .cast(SQLDataType.VARCHAR(10000)),
                        REQUIREMENT.RLN_ID.as(ID),
                        HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull(),
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID)
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .innerJoin(RLN_RESOURCE)
                .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(RLN_RESOURCE.RLN_ID))
                .innerJoin(RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                .leftJoin(HIGH_LEVEL_REQUIREMENT)
                .on(REQUIREMENT.RLN_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                .where(
                        REQUIREMENT_SYNC_EXTENDER
                                .REMOTE_REQ_ID
                                .in(remoteKeys)
                                .and(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.in(remoteSyncId)))
                .groupBy(
                        PROJECT.NAME,
                        REQUIREMENT.RLN_ID,
                        HIGH_LEVEL_REQUIREMENT.RLN_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID)
                .fetchStream();
    }

    @Override
    public Stream<Record4<String, Long, Boolean, String>> findSyncRequirements(
            Collection<String> remoteKeys, String project) {
        return dsl.select(
                        concat(
                                        PROJECT.NAME,
                                        value("/"),
                                        groupConcat(RESOURCE.NAME)
                                                .orderBy(RLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                                .separator("/"))
                                .cast(SQLDataType.VARCHAR(10000)),
                        REQUIREMENT.RLN_ID.as(ID),
                        HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull(),
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID)
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .innerJoin(RLN_RESOURCE)
                .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(RLN_RESOURCE.RLN_ID))
                .innerJoin(RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                .leftJoin(HIGH_LEVEL_REQUIREMENT)
                .on(REQUIREMENT.RLN_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                .where(
                        REQUIREMENT_SYNC_EXTENDER
                                .REMOTE_REQ_ID
                                .in(remoteKeys)
                                .and(PROJECT.NAME.eq(project))
                                .and(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.isNull()))
                .groupBy(
                        PROJECT.NAME,
                        REQUIREMENT.RLN_ID,
                        HIGH_LEVEL_REQUIREMENT.RLN_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID)
                .fetchStream();
    }

    @Override
    public Stream<Record6<String, Long, Boolean, Boolean, String, Long>> findNodesByNamesAndProject(
            Collection<String> nodeNames, String projectName) {
        return dsl.select(
                        concat(
                                        PROJECT.NAME,
                                        value("/"),
                                        groupConcat(PARENT_RESOURCE.NAME)
                                                .orderBy(RLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                                .separator("/"))
                                .cast(SQLDataType.VARCHAR(10000)),
                        REQUIREMENT_LIBRARY_NODE.RLN_ID.as(ID),
                        REQUIREMENT.RLN_ID.isNotNull(),
                        HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull(),
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.as(SYNCHRONISATION_ID))
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(RLN_RESOURCE)
                .on(RLN_RESOURCE.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                .innerJoin(RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .innerJoin(PARENT_RlN_RESOURCE)
                .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(PARENT_RlN_RESOURCE.RLN_ID))
                .innerJoin(PARENT_RESOURCE)
                .on(PARENT_RlN_RESOURCE.RES_ID.eq(PARENT_RESOURCE.RES_ID))
                .leftJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .leftJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .leftJoin(HIGH_LEVEL_REQUIREMENT)
                .on(REQUIREMENT.RLN_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                .where(PROJECT.NAME.eq(projectName).and(RESOURCE.NAME.in(nodeNames)))
                .groupBy(
                        PROJECT.NAME,
                        REQUIREMENT_LIBRARY_NODE.RLN_ID,
                        REQUIREMENT.RLN_ID,
                        HIGH_LEVEL_REQUIREMENT.RLN_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID)
                .fetchStream();
    }

    @Override
    public Stream<Record3<String, Long, Boolean>> findBasicRequirementsByNamesAndProject(
            Collection<String> names, String project) {
        return dsl.select(
                        concat(
                                        PROJECT.NAME,
                                        value("/"),
                                        groupConcat(PARENT_RESOURCE.NAME)
                                                .orderBy(RLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                                .separator("/"))
                                .cast(SQLDataType.VARCHAR(10000)),
                        REQUIREMENT.RLN_ID.as(ID),
                        HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull())
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(RLN_RESOURCE)
                .on(REQUIREMENT.RLN_ID.eq(RLN_RESOURCE.RLN_ID))
                .innerJoin(RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .innerJoin(PARENT_RlN_RESOURCE)
                .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(PARENT_RlN_RESOURCE.RLN_ID))
                .innerJoin(PARENT_RESOURCE)
                .on(PARENT_RlN_RESOURCE.RES_ID.eq(PARENT_RESOURCE.RES_ID))
                .leftJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .leftJoin(HIGH_LEVEL_REQUIREMENT)
                .on(REQUIREMENT.RLN_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                .where(
                        PROJECT
                                .NAME
                                .eq(project)
                                .and(RESOURCE.NAME.in(names))
                                .and(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID.isNull()))
                .groupBy(PROJECT.NAME, REQUIREMENT.RLN_ID, HIGH_LEVEL_REQUIREMENT.RLN_ID)
                .fetchStream();
    }

    @Override
    public Stream<Record5<String, Long, Boolean, String, Long>> findRequirementsByNamesAndProjects(
            Collection<String> requirementNames, Collection<String> projects) {

        return dsl.select(
                        concat(
                                        PROJECT.NAME,
                                        value("/"),
                                        groupConcat(PARENT_RESOURCE.NAME)
                                                .orderBy(RLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                                .separator("/"))
                                .cast(SQLDataType.VARCHAR(10000)),
                        REQUIREMENT.RLN_ID.as(ID),
                        HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull(),
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.as(SYNCHRONISATION_ID))
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(RLN_RESOURCE)
                .on(REQUIREMENT.RLN_ID.eq(RLN_RESOURCE.RLN_ID))
                .innerJoin(RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .innerJoin(PARENT_RlN_RESOURCE)
                .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(PARENT_RlN_RESOURCE.RLN_ID))
                .innerJoin(PARENT_RESOURCE)
                .on(PARENT_RlN_RESOURCE.RES_ID.eq(PARENT_RESOURCE.RES_ID))
                .leftJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .leftJoin(HIGH_LEVEL_REQUIREMENT)
                .on(REQUIREMENT.RLN_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                .where(PROJECT.NAME.in(projects).and(RESOURCE.NAME.in(requirementNames)))
                .groupBy(
                        PROJECT.NAME,
                        REQUIREMENT.RLN_ID,
                        HIGH_LEVEL_REQUIREMENT.RLN_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                        REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID)
                .fetchStream();
    }

    @Override
    public Map<Long, List<Long>> findVerifiedTestCaseIdsByVersionIds(Set<Long> versionIds) {
        if (versionIds.isEmpty()) {
            return Collections.emptyMap();
        }

        return coverageDao.findVerifiedTestCaseIdsByVersionIds(versionIds);
    }

    @Override
    public Map<Long, Map<Long, Long>> findExistingCoveragesByRequirementIds(
            Collection<Long> requirementIds) {
        if (requirementIds.isEmpty()) {
            return Collections.emptyMap();
        }

        return coverageDao.findExistingCoveragesByRequirementIds(requirementIds);
    }

    @Override
    public Map<Long, RequirementStatus> findRequirementStatusesByVersionIds(
            Collection<Long> versionIds) {
        if (versionIds.isEmpty()) {
            return Collections.emptyMap();
        }

        return requirementVersionDao.findRequirementStatusesByVersionIds(versionIds);
    }

    @Override
    public Map<Long, Map<Long, Long>> findExistingLinksByVersionIds(Collection<Long> versionIds) {
        if (versionIds.isEmpty()) {
            return Collections.emptyMap();
        }

        var query =
                dsl.select(
                                REQUIREMENT_VERSION_LINK.REQUIREMENT_VERSION_ID,
                                REQUIREMENT_VERSION_LINK.RELATED_REQUIREMENT_VERSION_ID,
                                REQUIREMENT_VERSION_LINK.LINK_ID)
                        .from(REQUIREMENT_VERSION_LINK)
                        .where(
                                REQUIREMENT_VERSION_LINK
                                        .REQUIREMENT_VERSION_ID
                                        .in(versionIds)
                                        .and(REQUIREMENT_VERSION_LINK.RELATED_REQUIREMENT_VERSION_ID.in(versionIds)));

        try (var stream = query.fetchStream()) {
            return stream.collect(
                    Collectors.groupingBy(
                            Record3::value1, Collectors.toMap(Record3::value2, Record3::value3)));
        }
    }

    @Override
    public Map<Long, Map<Integer, ReqVersionMilestone>> findReqVersionAndMilestonesByReqId(
            Collection<Long> requirementIds) {
        if (requirementIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<Long, Map<Integer, ReqVersionMilestone>> result = new HashMap<>();

        var query =
                dsl.select(
                                REQUIREMENT_VERSION.REQUIREMENT_ID,
                                REQUIREMENT_VERSION.RES_ID,
                                REQUIREMENT_VERSION.VERSION_NUMBER,
                                MILESTONE.LABEL,
                                MILESTONE.STATUS)
                        .from(REQUIREMENT_VERSION)
                        .leftJoin(MILESTONE_REQ_VERSION)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                        .leftJoin(MILESTONE)
                        .on(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                        .where(REQUIREMENT_VERSION.REQUIREMENT_ID.in(requirementIds));

        try (var stream = query.fetchStream()) {
            stream.forEach(
                    r -> {
                        Long requirementId = r.value1();
                        Long reqVersionId = r.value2();
                        Integer versionNumber = r.value3();
                        String label = r.value4();
                        String status = r.value5();

                        Map<Integer, ReqVersionMilestone> reqVersionMilestoneByReq =
                                result.computeIfAbsent(requirementId, k -> new HashMap<>());

                        ReqVersionMilestone reqVersionMilestone =
                                reqVersionMilestoneByReq.computeIfAbsent(
                                        versionNumber, k -> new ReqVersionMilestone(reqVersionId));

                        if (label != null && status != null) {
                            reqVersionMilestone.addMilestone(label, status);
                        }
                    });
        }

        return result;
    }

    @Override
    public String getDefaultRequirementVersionLinkRole() {
        return dsl.select(REQUIREMENT_VERSION_LINK_TYPE.ROLE_2_CODE)
                .from(REQUIREMENT_VERSION_LINK_TYPE)
                .where(REQUIREMENT_VERSION_LINK_TYPE.IS_DEFAULT.eq(true))
                .fetchOneInto(String.class);
    }
}
