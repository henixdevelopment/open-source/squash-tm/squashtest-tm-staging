/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model.messages;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.opentestfactory.messages.Status;

public class WorkflowsStatus extends Status<Map<String, WorkflowData>> {

    private static final String ITEMS_KEY = "items";
    private static final String ORIGINATING_PROJECTS_KEY = "squashtest.org/originating-projects";

    public WorkflowsStatus(String apiVersion) {
        super(apiVersion);
    }

    public List<WorkflowData> getProjectWorkflows(String projectId) {
        Map<String, Map<String, WorkflowData>> details = getDetails();
        Map<String, WorkflowData> map = Objects.isNull(details) ? null : details.get(ITEMS_KEY);
        if (Objects.nonNull(map)) {
            return map.values().stream()
                    .filter(workflow -> isProjectIdPresentInWorkflow(projectId, workflow.metadata()))
                    .toList();
        }
        return Collections.emptyList();
    }

    private boolean isProjectIdPresentInWorkflow(String projectId, Metadata metadata) {
        if (metadata.annotations() != null
                && metadata.annotations().containsKey(ORIGINATING_PROJECTS_KEY)) {
            List<Object> projectIds = metadata.annotations().get(ORIGINATING_PROJECTS_KEY);
            return projectIds.contains(projectId);
        }
        return false;
    }
}
