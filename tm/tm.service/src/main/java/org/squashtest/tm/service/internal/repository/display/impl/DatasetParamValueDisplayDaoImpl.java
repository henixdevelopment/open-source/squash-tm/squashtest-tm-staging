/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.DATASET_PARAM_VALUE;

import java.util.List;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.testcase.DatasetParamValueDto;
import org.squashtest.tm.service.internal.repository.display.DatasetParamValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class DatasetParamValueDisplayDaoImpl implements DatasetParamValueDisplayDao {

    @Inject private DSLContext dsl;

    @Override
    public List<DatasetParamValueDto> findAllByParameterAndDataset(
            List<Long> parametersIds, List<Long> datasetIds) {
        return dsl.select(
                        DATASET_PARAM_VALUE.DATASET_PARAM_VALUE_ID.as(RequestAliasesConstants.ID),
                        DATASET_PARAM_VALUE.PARAM_VALUE.as("VALUE"),
                        DATASET_PARAM_VALUE.PARAM_ID.as("PARAMETER_ID"),
                        DATASET_PARAM_VALUE.DATASET_ID)
                .from(DATASET_PARAM_VALUE)
                .where(
                        DATASET_PARAM_VALUE
                                .PARAM_ID
                                .in(parametersIds)
                                .and(DATASET_PARAM_VALUE.DATASET_ID.in(datasetIds)))
                .fetchInto(DatasetParamValueDto.class);
    }
}
