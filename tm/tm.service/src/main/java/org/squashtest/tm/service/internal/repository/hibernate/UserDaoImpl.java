/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.hibernate.type.IntegerType.ZERO;
import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP_AUTHORITY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;

import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.users.QUser;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.internal.display.dto.MilestonePossibleOwnerDto;
import org.squashtest.tm.service.internal.repository.CustomUserDao;

public class UserDaoImpl implements CustomUserDao {

    private static final String FIND_ALL_ADMIN =
            "SELECT  member.PARTY_ID FROM  CORE_GROUP_MEMBER member "
                    + "inner join CORE_GROUP_AUTHORITY cga on cga.GROUP_ID=member.GROUP_ID "
                    + "WHERE cga.AUTHORITY = '"
                    + ROLE_ADMIN
                    + "' ";

    @PersistenceContext private EntityManager entityManager;

    @Inject private DSLContext dsl;

    // **************** private code ****************************

    @Override
    public List<MilestonePossibleOwnerDto> findAllAdminOrManager() {
        return dsl.selectDistinct(
                        CORE_USER.PARTY_ID.as(ID), CORE_USER.LOGIN, CORE_USER.FIRST_NAME, CORE_USER.LAST_NAME)
                .from(CORE_USER)
                .where(
                        CORE_USER.PARTY_ID.in(
                                dsl.select(CORE_GROUP_MEMBER.PARTY_ID)
                                        .from(CORE_GROUP_MEMBER)
                                        .join(CORE_GROUP_AUTHORITY)
                                        .on(CORE_GROUP_MEMBER.GROUP_ID.eq(CORE_GROUP_AUTHORITY.GROUP_ID))
                                        .where(CORE_GROUP_AUTHORITY.AUTHORITY.eq(ROLE_ADMIN))
                                        .union(
                                                dsl.select(CORE_USER.PARTY_ID)
                                                        .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                                                        .join(CORE_USER)
                                                        .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_USER.PARTY_ID))
                                                        .join(ACL_GROUP_PERMISSION)
                                                        .on(
                                                                ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(
                                                                        ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                                                        .where(
                                                                ACL_GROUP_PERMISSION.PERMISSION_MASK.eq(
                                                                        Permissions.MANAGE_MILESTONE.getMask())))
                                        .union(
                                                dsl.select(CORE_TEAM_MEMBER.USER_ID)
                                                        .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                                                        .join(CORE_TEAM)
                                                        .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_TEAM.PARTY_ID))
                                                        .join(CORE_TEAM_MEMBER)
                                                        .on(CORE_TEAM.PARTY_ID.eq(CORE_TEAM_MEMBER.TEAM_ID))
                                                        .join(ACL_GROUP_PERMISSION)
                                                        .on(
                                                                ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(
                                                                        ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                                                        .where(
                                                                ACL_GROUP_PERMISSION.PERMISSION_MASK.eq(
                                                                        Permissions.MANAGE_MILESTONE.getMask())))))
                .fetchInto(MilestonePossibleOwnerDto.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> findAllAdmin() {
        NativeQuery query = entityManager.unwrap(Session.class).createNativeQuery(FIND_ALL_ADMIN);
        query.setResultTransformer(new SqLIdResultTransformer());
        List<Long> ids = query.list();

        return new JPAQueryFactory(entityManager)
                .selectFrom(QUser.user)
                .where(QUser.user.id.in(ids))
                .fetch();
    }

    @Override
    public Long findUserId(String login) {
        return dsl.select(CORE_USER.PARTY_ID)
                .from(CORE_USER)
                .where(CORE_USER.LOGIN.eq(login))
                .fetchOne(CORE_USER.PARTY_ID);
    }

    @Override
    public int countAllActiveUsers() {
        Record1<Integer> count =
                dsl.selectCount().from(CORE_USER).where(CORE_USER.ACTIVE.isTrue()).fetchOne();
        return Objects.nonNull(count) ? count.value1() : ZERO;
    }

    @Override
    public String findUserLoginById(long userId) {
        return dsl.select(CORE_USER.LOGIN)
                .from(CORE_USER)
                .where(CORE_USER.PARTY_ID.eq(userId))
                .fetchOneInto(String.class);
    }

    @Override
    public List<String> filterActiveUserLogins(Collection<String> logins) {
        return dsl.select(CORE_USER.LOGIN)
                .from(CORE_USER)
                .where(CORE_USER.LOGIN.in(logins).and(CORE_USER.ACTIVE.eq(Boolean.TRUE)))
                .fetchInto(String.class);
    }
}
