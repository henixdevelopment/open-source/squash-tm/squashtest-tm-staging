/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.campaign;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.campaign.TestPlanStatistics;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.UserView;

public class TestSuiteDto {

    private Long id;
    private String uuid;
    private String name;
    private String description;
    private Long projectId;
    private Long attachmentListId;
    private AttachmentListDto attachmentList;
    private Date createdOn;
    private String createdBy;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private String executionStatus;
    private TestPlanStatistics testPlanStatistics;
    private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
    private int nbIssues;
    private HashMap<Long, String> executionStatusMap;
    private boolean hasDatasets;
    private List<UserView> users;
    private List<MilestoneDto> milestones = new ArrayList<>();
    private long nbAutomatedSuites;
    private int nbTestPlanItems;
    private String path;
    private Long iterationId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAttachmentListId() {
        return attachmentListId;
    }

    public void setAttachmentListId(Long attachmentListId) {
        this.attachmentListId = attachmentListId;
    }

    public AttachmentListDto getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(AttachmentListDto attachmentList) {
        this.attachmentList = attachmentList;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(String executionStatus) {
        this.executionStatus = executionStatus;
    }

    public TestPlanStatistics getTestPlanStatistics() {
        return testPlanStatistics;
    }

    public void setTestPlanStatistics(TestPlanStatistics testPlanStatistics) {
        this.testPlanStatistics = testPlanStatistics;
    }

    public List<CustomFieldValueDto> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public int getNbIssues() {
        return nbIssues;
    }

    public void setNbIssues(int nbIssues) {
        this.nbIssues = nbIssues;
    }

    public List<MilestoneDto> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<MilestoneDto> milestones) {
        this.milestones = milestones;
    }

    public List<UserView> getUsers() {
        return users;
    }

    public void setUsers(List<UserView> users) {
        this.users = users;
    }

    public boolean isHasDatasets() {
        return hasDatasets;
    }

    public void setHasDatasets(boolean hasDatasets) {
        this.hasDatasets = hasDatasets;
    }

    public HashMap<Long, String> getExecutionStatusMap() {
        return executionStatusMap;
    }

    public void setExecutionStatusMap(HashMap<Long, String> executionStatusMap) {
        this.executionStatusMap = executionStatusMap;
    }

    public long getNbAutomatedSuites() {
        return nbAutomatedSuites;
    }

    public void setNbAutomatedSuites(long nbAutomatedSuites) {
        this.nbAutomatedSuites = nbAutomatedSuites;
    }

    public int getNbTestPlanItems() {
        return nbTestPlanItems;
    }

    public void setNbTestPlanItems(int nbTestPlanItems) {
        this.nbTestPlanItems = nbTestPlanItems;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getIterationId() {
        return iterationId;
    }

    public void setIterationId(Long iterationId) {
        this.iterationId = iterationId;
    }
}
