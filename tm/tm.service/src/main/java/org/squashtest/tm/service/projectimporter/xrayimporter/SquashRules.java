/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.xrayimporter;

import java.util.stream.Stream;
import org.jooq.Record2;
import org.jooq.Record3;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.AbstractGenericItem;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayPriority;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayStatus;

public interface SquashRules {

    void handleDuplicateName(Stream<Record2<Long, String>> recordSelectDuplicate);

    void handleDuplicateNameWithParentType(
            Stream<Record3<Long, String, String>> recordSelectDuplicate);

    <T extends AbstractGenericItem> void markWithErrorAndThrow(T item, String message);

    <T extends AbstractGenericItem> void markWithSuccessIfStatusIsNull(T item);

    <T extends AbstractGenericItem> void markWithWarning(T item, String message);

    void validateMandatoryCommonField(ItemXrayDto itemXrayDto);

    <T extends AbstractGenericItem, R> void checkMandatoryField(T item, R value, String fieldName);

    String checkSanitizer(ItemXrayDto itemXrayDto, String elementToCheck, String fieldName);

    String checkDatasetName(ItemXrayDto itemXrayDto, String datasetParamName);

    String checkDatasetNameInsideStep(String datasetParam);

    String checkFieldLength(ItemXrayDto itemXrayDto, String string, String fieldName);

    XrayStatus checkStatus(ItemXrayDto itemXrayDto, String status);

    XrayPriority checkPriority(ItemXrayDto itemXrayDto, String priority);
}
