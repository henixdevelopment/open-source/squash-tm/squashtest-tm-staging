/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This file will store important data that will be used during the pivot import process. For
 * example, everytime we will create a squash entity during the import, we will use this class to
 * reference the created squash entity id matching with the entity id written in the json file into
 * a map so that it can be used during the import process to create links between entities.
 */
public class PivotImportMetadata {
    private final Map<String, SquashCustomFieldInfo> customFieldIdsMap = new HashMap<>();
    private final Map<String, Long> requirementIdsMap = new HashMap<>();
    private final Map<String, Long> requirementFoldersIdsMap = new HashMap<>();
    private final Map<String, Long> testCaseIdsMap = new HashMap<>();
    private final Map<String, Long> testCaseFoldersIdsMap = new HashMap<>();
    private final Map<String, Long> testStepsIdsMap = new HashMap<>();
    private final Map<String, Long> datasetParamIdsMap = new HashMap<>();
    private final Map<String, Long> datasetIdsMap = new HashMap<>();
    private final Map<String, Long> campaignFoldersIdsMap = new HashMap<>();
    private final Map<String, Long> campaignIdsMap = new HashMap<>();
    private final Map<String, Long> iterationIdsMap = new HashMap<>();
    private final Map<String, TestSuiteInfo> testSuiteIdsMap = new HashMap<>();

    private List<String> attachmentTypeWhiteList;
    private Long maxAttachmentSize;

    private final List<ImportWarningEntry> importWarningEntries = new ArrayList<>();

    public Map<String, SquashCustomFieldInfo> getCustomFieldIdsMap() {
        return customFieldIdsMap;
    }

    public Map<String, Long> getRequirementIdsMap() {
        return requirementIdsMap;
    }

    public Map<String, Long> getRequirementFoldersIdsMap() {
        return requirementFoldersIdsMap;
    }

    public Map<String, Long> getTestCaseIdsMap() {
        return testCaseIdsMap;
    }

    public Map<String, Long> getTestCaseFoldersIdsMap() {
        return testCaseFoldersIdsMap;
    }

    public Map<String, Long> getDatasetParamIdsMap() {
        return datasetParamIdsMap;
    }

    public Map<String, Long> getTestStepsIdsMap() {
        return testStepsIdsMap;
    }

    public Map<String, Long> getDatasetIdsMap() {
        return datasetIdsMap;
    }

    public Map<String, Long> getCampaignFoldersIdsMap() {
        return campaignFoldersIdsMap;
    }

    public Map<String, Long> getCampaignIdsMap() {
        return campaignIdsMap;
    }

    public Map<String, Long> getIterationIdsMap() {
        return iterationIdsMap;
    }

    public Map<String, TestSuiteInfo> getTestSuiteIdsMap() {
        return testSuiteIdsMap;
    }

    public List<String> getAttachmentTypeWhiteList() {
        return attachmentTypeWhiteList;
    }

    public void setAttachmentTypeWhiteList(List<String> attachmentTypeWhiteList) {
        this.attachmentTypeWhiteList = attachmentTypeWhiteList;
    }

    public Long getMaxAttachmentSize() {
        return maxAttachmentSize;
    }

    public void setMaxAttachmentSize(Long maxAttachmentSize) {
        this.maxAttachmentSize = maxAttachmentSize;
    }

    public List<ImportWarningEntry> getImportWarningEntries() {
        return importWarningEntries;
    }

    public boolean hasAtLeastOneImportedEntity() {
        return Stream.of(
                        customFieldIdsMap,
                        requirementFoldersIdsMap,
                        requirementIdsMap,
                        testCaseFoldersIdsMap,
                        testCaseIdsMap,
                        campaignFoldersIdsMap,
                        campaignIdsMap,
                        iterationIdsMap,
                        testStepsIdsMap)
                .anyMatch(map -> !map.isEmpty());
    }
}
