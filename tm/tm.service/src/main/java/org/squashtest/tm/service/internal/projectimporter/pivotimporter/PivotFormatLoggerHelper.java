/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import java.util.Objects;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.exception.pivotformatimport.CouldNotCreateEntityDuringImportException;
import org.squashtest.tm.exception.pivotformatimport.CouldNotParseEntityDuringImportException;
import org.squashtest.tm.exception.pivotformatimport.PivotImportAttachmentMaxSizeExceededException;
import org.squashtest.tm.exception.pivotformatimport.PivotImportAttachmentTypeNotAllowedException;
import org.squashtest.tm.service.internal.dto.projectimporter.ImportWarningEntry;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;

public final class PivotFormatLoggerHelper {
    public static final String CALLED_TEST_CASE = "called test case";
    public static final String CALLED_TEST_CASES = "called test cases";
    public static final String CAMPAIGN = "campaign";
    public static final String CAMPAIGNS = "campaigns";
    public static final String CAMPAIGN_FOLDER = "campaign folder";
    public static final String CAMPAIGN_FOLDERS = "campaign folders";

    public static final String CUSTOM_FIELD = "custom field";
    public static final String CUSTOM_FIELDS = "custom fields";

    public static final String EXECUTION = "execution";
    public static final String EXECUTION_STEP_INSIDE_EXECUTION = "an execution step inside execution";
    public static final String EXECUTIONS = "executions";
    public static final String ITERATION = "iteration";
    public static final String ITERATIONS = "iterations";
    public static final String REQUIREMENT = "requirement";
    public static final String REQUIREMENTS = "requirements";
    public static final String REQUIREMENT_FOLDER = "requirement folder";
    public static final String REQUIREMENT_FOLDERS = "requirement folders";
    public static final String TEST_CASE = "test case";
    public static final String TEST_CASES = "test cases";
    public static final String TEST_CASE_FOLDER = "test case folder";
    public static final String TEST_CASE_FOLDERS = "test case folders";
    public static final String TEST_STEP = "test step";
    public static final String TEST_SUITE = "test suite";
    public static final String TEST_SUITES = "test suites";

    private PivotFormatLoggerHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static void logImportStartedForEntitiesKind(
            Logger logger, String zipFileName, String entitiesKind, PivotFormatImport pivotFormatImport) {
        logger.info(
                "Starting to import {} from zip archive \"{}\" for import with id: {}",
                entitiesKind,
                zipFileName,
                pivotFormatImport.getId());
    }

    public static void logImportSuccessForEntitiesKind(
            Logger logger, String entitiesKind, PivotFormatImport pivotFormatImport) {
        logger.info(
                "Import id: {} - Successfully imported {} from json file",
                pivotFormatImport.getId(),
                entitiesKind);
    }

    public static void logImportFailureForEntitiesKind(
            Logger logger, String entitiesKind, PivotFormatImport pivotFormatImport) {
        logger.error(
                "Import id: {} - Failed to import {} from json file",
                pivotFormatImport.getId(),
                entitiesKind);
    }

    public static void logParsingSuccessForEntity(
            Logger logger,
            String entityKind,
            String entityName,
            String internalId,
            PivotFormatImport pivotFormatImport) {
        logger.debug(
                "Import id {} - The {} with name \"{}\" and internal id {} was successfully parsed",
                pivotFormatImport.getId(),
                entityKind,
                entityName,
                internalId);
    }

    public static void logParsingSuccessForUnnamedEntity(
            Logger logger, String entityKind, String internalId, PivotFormatImport pivotFormatImport) {
        logger.debug(
                "Import id {} - The {} and internal id {} was successfully parsed",
                pivotFormatImport.getId(),
                entityKind,
                internalId);
    }

    public static void handleParsingErrorForEntity(
            Logger logger,
            String entityKind,
            String internalId,
            PivotFormatImport pivotFormatImport,
            Exception e) {
        String message;
        if (Objects.nonNull(internalId)) {
            message =
                    String.format(
                            "Import id: %s - Failed to parse %s with internal id : %s",
                            pivotFormatImport.getId(), entityKind, internalId);
        } else {
            message =
                    String.format(
                            "Import id: %s - Failed to parse %s.", pivotFormatImport.getId(), entityKind);
        }
        logger.error(message);
        throw new CouldNotParseEntityDuringImportException(message, e);
    }

    public static void logEntityCreationStarted(
            Logger logger, String entityKind, String internalId, PivotFormatImport pivotFormatImport) {
        logger.info(
                "Import id: {} - Creating {} with internal id {}",
                pivotFormatImport.getId(),
                entityKind,
                internalId);
    }

    public static void logEntityCreatedSuccessfully(
            Logger logger,
            String entityKind,
            String entityName,
            String internalId,
            PivotFormatImport pivotFormatImport) {
        logger.info(
                "Import id: {} - Successfully created {} named \"{}\" with internal id {}",
                pivotFormatImport.getId(),
                entityKind,
                entityName,
                internalId);
    }

    public static void logUnnamedEntityCreatedSuccessfully(
            Logger logger, String entityKind, String internalId, PivotFormatImport pivotFormatImport) {
        logger.info(
                "Import id: {} - Successfully created {} with internal id {}",
                pivotFormatImport.getId(),
                entityKind,
                internalId);
    }

    public static void handleEntityCreationFailed(
            Logger logger,
            String entityKind,
            String entityName,
            String internalId,
            PivotFormatImport pivotFormatImport,
            Exception e) {
        String message =
                String.format(
                        "Import id: %s - Failed to create %s named \"%s\" with internal id %s",
                        pivotFormatImport.getId(), entityKind, entityName, internalId);
        logger.error(message);
        throw new CouldNotCreateEntityDuringImportException(message, e);
    }

    public static void handleUnnamedEntityCreationFailed(
            Logger logger,
            String entityKind,
            String internalId,
            PivotFormatImport pivotFormatImport,
            Exception e) {
        String message =
                String.format(
                        "Import id: %s - Failed to create %s with internal id %s",
                        pivotFormatImport.getId(), entityKind, internalId);
        logger.error(message);
        throw new CouldNotCreateEntityDuringImportException(message, e);
    }

    public static String getFolderEntityKindNameFromJsonFile(
            JsonImportFile jsonFileName, boolean isPlural) {
        String entityKindName = "";
        switch (jsonFileName) {
            case TEST_CASE_FOLDERS -> entityKindName = isPlural ? TEST_CASE_FOLDERS : TEST_CASE_FOLDER;
            case REQUIREMENT_FOLDERS ->
                    entityKindName = isPlural ? REQUIREMENT_FOLDERS : REQUIREMENT_FOLDER;
            case CAMPAIGN_FOLDERS -> entityKindName = isPlural ? CAMPAIGN_FOLDERS : CAMPAIGN_FOLDER;
            default ->
                    throw new IllegalArgumentException("Unknown json import file name: " + jsonFileName);
        }
        return entityKindName;
    }

    public static void handleAttachmentNotFound(
            Logger logger,
            String attachmentPath,
            AttachmentHolderInfo holderInfo,
            PivotFormatImport pivotFormatImport,
            PivotImportMetadata pivotImportMetadata) {

        String message =
                String.format(
                        "Import %s - Could not find attachment for %s with internal ID %s at the following path: %s ",
                        pivotFormatImport.getId(),
                        findEntityLabelByHolderType(holderInfo),
                        holderInfo.internalHolderId(),
                        attachmentPath);

        logger.error(message);

        pivotImportMetadata
                .getImportWarningEntries()
                .add(new ImportWarningEntry(holderInfo.holderType(), message));
    }

    public static void handleAttachmentTypeNotAllowed(
            Logger logger,
            String fileType,
            String fileName,
            AttachmentHolderInfo holderInfo,
            PivotFormatImport pivotFormatImport) {

        String message =
                String.format(
                        "Import %s - Could not add attachment for %s with internal ID %s. The extension \"%s\" of file \"%s\" is not allowed for attachment import.",
                        pivotFormatImport.getId(),
                        findEntityLabelByHolderType(holderInfo),
                        holderInfo.internalHolderId(),
                        fileType,
                        fileName);
        logger.error(message);
        throw new PivotImportAttachmentTypeNotAllowedException(message);
    }

    public static void handleAttachmentMaxSizeExceeded(
            Logger logger,
            String fileName,
            Long fileSize,
            Long maxAllowedSize,
            AttachmentHolderInfo holderInfo,
            PivotFormatImport pivotFormatImport) {

        String message =
                String.format(
                        "Import %s - Could not add attachment for %s with internal ID %s. The attachment \"%s\" has exceeded size limit (Max allowed size: %s bytes). File size: %s bytes",
                        pivotFormatImport.getId(),
                        findEntityLabelByHolderType(holderInfo),
                        holderInfo.internalHolderId(),
                        fileName,
                        maxAllowedSize,
                        fileSize);
        logger.error(message);
        throw new PivotImportAttachmentMaxSizeExceededException(message);
    }

    private static String findEntityLabelByHolderType(AttachmentHolderInfo holderInfo) {

        return switch (holderInfo.holderType()) {
            case TEST_CASE_FOLDER -> TEST_CASE_FOLDER;
            case TEST_CASE -> TEST_CASE;
            case ACTION_TEST_STEP -> TEST_STEP;
            case REQUIREMENT_FOLDER -> REQUIREMENT_FOLDER;
            case REQUIREMENT -> REQUIREMENT;
            case CAMPAIGN_FOLDER -> CAMPAIGN_FOLDER;
            case CAMPAIGN -> CAMPAIGN;
            case ITERATION -> ITERATION;
            case TEST_SUITE -> TEST_SUITE;
            case EXECUTION -> EXECUTION;
            case EXECUTION_STEP -> EXECUTION_STEP_INSIDE_EXECUTION;
            default ->
                    throw new IllegalArgumentException(
                            "Unknown attachment holder type: " + holderInfo.holderType());
        };
    }
}
