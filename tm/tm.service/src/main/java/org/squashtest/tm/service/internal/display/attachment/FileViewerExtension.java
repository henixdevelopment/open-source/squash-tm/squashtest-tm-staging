/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.attachment;

import org.springframework.http.MediaType;

public enum FileViewerExtension {
    HTML(MediaType.TEXT_HTML_VALUE, false),
    JPEG(MediaType.IMAGE_JPEG_VALUE, true),
    JPG(MediaType.IMAGE_JPEG_VALUE, true),
    JSON(MediaType.APPLICATION_JSON_VALUE, false),
    LOG(MediaType.TEXT_PLAIN_VALUE, false),
    PDF(MediaType.APPLICATION_PDF_VALUE, true),
    PNG(MediaType.IMAGE_PNG_VALUE, true),
    SVG("image/svg+xml", true),
    TAR(MediaType.TEXT_HTML_VALUE, false),
    TXT(MediaType.TEXT_PLAIN_VALUE, false),
    XML(MediaType.APPLICATION_XML_VALUE, false),
    JS("application/javascript", false),
    GIF(MediaType.IMAGE_GIF_VALUE, true),
    BMP("image/bmp", true),
    CSS("text/css", false);

    public final String media;
    public final boolean isOutputStream;

    FileViewerExtension(String media, boolean isOutputStream) {
        this.media = media;
        this.isOutputStream = isOutputStream;
    }
}
