/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders.testcase;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.Subgraph;
import javax.persistence.TypedQuery;
import org.hibernate.jpa.QueryHints;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.common.FetchPlanBuilder;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.common.HintOption;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;

@Repository
public class JpaTestCaseLoader implements TestCaseLoader {

    private final EntityManager entityManager;

    public JpaTestCaseLoader(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public EnumSet<TestCaseLoader.Options> defaultOptions() {
        return EnumSet.noneOf(TestCaseLoader.Options.class);
    }

    @Override
    public List<TestCase> load(Collection<Long> ids, EnumSet<TestCaseLoader.Options> options) {
        FetchPlanBuilder<Long, TestCase> fetchPlanBuilder = new FetchPlanBuilder<>(ids);
        assignInitialQuery(fetchPlanBuilder);
        addFetchOptions(options, fetchPlanBuilder);
        return fetchPlanBuilder.build().fetch();
    }

    private void addFetchOptions(
            EnumSet<TestCaseLoader.Options> options, FetchPlanBuilder<Long, TestCase> fetchPlanBuilder) {
        String[] fetchAttributes =
                options.stream()
                        .filter(opt -> opt.field != null)
                        .map(opt -> opt.field)
                        .toArray(String[]::new);

        if (fetchAttributes.length == 0) {
            return;
        }

        EntityGraph<TestCase> entityGraph = entityManager.createEntityGraph(TestCase.class);
        entityGraph.addAttributeNodes(fetchAttributes);

        if (options.contains(TestCaseLoader.Options.FETCH_ATTACHMENT_LIST)) {
            Subgraph<?> subGraph =
                    entityGraph.addSubgraph(TestCaseLoader.Options.FETCH_ATTACHMENT_LIST.field);
            subGraph.addAttributeNodes("attachments");
        }

        if (options.contains(TestCaseLoader.Options.FETCH_REQUIREMENT_VERSION_COVERAGES)) {
            Subgraph<?> subGraph =
                    entityGraph.addSubgraph(TestCaseLoader.Options.FETCH_REQUIREMENT_VERSION_COVERAGES.field);
            subGraph.addAttributeNodes("verifiedRequirementVersion");
            Subgraph<?> subGraph2 = subGraph.addSubgraph("verifiedRequirementVersion");
            subGraph2.addAttributeNodes("requirement");
        }

        HintOption option = new HintOption(HintOption.FETCH_GRAPH, entityGraph);

        fetchPlanBuilder.addHint(option);
    }

    private void assignInitialQuery(FetchPlanBuilder<Long, TestCase> fetchPlanBuilder) {
        fetchPlanBuilder.withMainQuery(this::getQuery);
    }

    private TypedQuery<TestCase> getQuery() {
        return entityManager
                .createQuery("select distinct tc from TestCase tc where tc.id in :ids", TestCase.class)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
    }
}
