/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.xrayimporter.topivot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Query;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Record3;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.sanitizehtml.HTMLSanitizeUtils;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.AbstractGenericItem;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayItemStatus;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayPriority;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayStatus;
import org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception.ImportXrayException;
import org.squashtest.tm.service.projectimporter.xrayimporter.SquashRules;
import org.squashtest.tm.service.projectimporter.xrayimporter.XrayTablesDao;

@Service
public class SquashRulesImpl implements SquashRules {
    private static final Pattern DATASET_NAME_REGEX = Pattern.compile("[^\\sA-Za-z0-9_-]");

    private final XrayTablesDao xrayTablesDao;

    public SquashRulesImpl(XrayTablesDao xrayTablesDao) {
        this.xrayTablesDao = xrayTablesDao;
    }

    @Override
    public void handleDuplicateName(Stream<Record2<Long, String>> recordSelectDuplicate) {
        List<Query> queries = new ArrayList<>();
        recordSelectDuplicate
                .collect(
                        Collectors.groupingBy(record2 -> record2.get(xrayTablesDao.getItemTable().SUMMARY)))
                .forEach(
                        (key, value) -> {
                            if (value.size() > 1) {
                                updateDuplicateName(value, queries);
                            }
                        });
        xrayTablesDao.executeBatchQueries(queries);
    }

    @Override
    public void handleDuplicateNameWithParentType(
            Stream<Record3<Long, String, String>> recordSelectDuplicate) {
        List<Query> queries = new ArrayList<>();
        recordSelectDuplicate
                .collect(
                        Collectors.groupingBy(
                                record2 -> {
                                    String summary = record2.get(xrayTablesDao.getItemTable().SUMMARY);
                                    String path = record2.get(xrayTablesDao.getCustomFieldTable().VALUE);
                                    return String.format("%s_%s", summary, path);
                                }))
                .forEach(
                        (key, value) -> {
                            if (value.size() > 1) {
                                updateDuplicateName(value, queries);
                            }
                        });
        xrayTablesDao.executeBatchQueries(queries);
    }

    private <T extends Record> void updateDuplicateName(
            List<T> listDuplicateNameRecord, List<Query> queries) {
        if (queries.size() > 50) {
            xrayTablesDao.executeBatchQueries(queries);
            queries.clear();
        }
        IntStream.range(1, listDuplicateNameRecord.size())
                .boxed()
                .forEach(
                        i ->
                                xrayTablesDao.addQueryUpdateItemName(
                                        listDuplicateNameRecord.get(i).get(xrayTablesDao.getItemTable().ID),
                                        String.format(
                                                "%s (%s)",
                                                listDuplicateNameRecord.get(i).get(xrayTablesDao.getItemTable().SUMMARY),
                                                i),
                                        queries));
    }

    @Override
    public <T extends AbstractGenericItem> void markWithErrorAndThrow(T item, String message) {
        item.setItemStatus(XrayItemStatus.FAILURE);
        item.setMessageOnError(String.format("%s - FAIL - %s", item.getKey(), message));
        throw new ImportXrayException(
                String.format("Error during generation of the %s : %s", item.getKey(), message));
    }

    @Override
    public <T extends AbstractGenericItem> void markWithSuccessIfStatusIsNull(T item) {
        if (Objects.isNull(item.getItemStatus())) {
            item.setItemStatus(XrayItemStatus.SUCCESS);
            item.setMessageOnSuccess(String.format("%s - SUCCESS", item.getKey()));
        }
    }

    @Override
    public <T extends AbstractGenericItem> void markWithWarning(T item, String message) {
        item.setItemStatus(XrayItemStatus.WARNING);
        item.getMessagesOnWarn().add(String.format("%s - WARNING - %s", item.getKey(), message));
    }

    @Override
    public void validateMandatoryCommonField(ItemXrayDto itemXrayDto) {
        checkMandatoryField(itemXrayDto, itemXrayDto.getSummary(), "Summary");
        itemXrayDto.setSummary(checkFieldLength(itemXrayDto, itemXrayDto.getSummary(), "Summary"));
        checkMandatoryField(itemXrayDto, itemXrayDto.getKey(), "Key");
    }

    @Override
    public <T extends AbstractGenericItem, R> void checkMandatoryField(
            T item, R value, String fieldName) {
        if (Objects.isNull(value)) {
            markWithErrorAndThrow(item, String.format("The field %s is null", fieldName));
        }
        if (value instanceof String && StringUtils.isBlank((String) value)) {
            markWithErrorAndThrow(item, String.format("The field %s is empty", fieldName));
        }
    }

    @Override
    public String checkSanitizer(ItemXrayDto itemXrayDto, String elementToCheck, String fieldName) {
        List<String> errorMessages = HTMLSanitizeUtils.checkHtml(elementToCheck);
        if (!errorMessages.isEmpty()) {
            String messageOnWarn =
                    String.format(
                            "%s is not a valid HTML (Formatting has been applied). Errors : %s",
                            fieldName, StringUtils.join(errorMessages, ", "));
            markWithWarning(itemXrayDto, messageOnWarn);
            return HTMLSanitizeUtils.cleanHtml(elementToCheck);
        }
        return elementToCheck;
    }

    @Override
    public String checkDatasetName(ItemXrayDto itemXrayDto, String datasetParam) {
        if (StringUtils.isBlank(datasetParam)) {
            return datasetParam;
        }
        datasetParam =
                checkDatasetName(
                        datasetParam,
                        datasetParamRaw ->
                                markWithWarning(
                                        itemXrayDto,
                                        String.format(
                                                "Dataset parameter '%s' contains invalid characters (Must be alphanumeric). These characters will be ignored.",
                                                datasetParamRaw)));
        datasetParam = checkFieldLength(itemXrayDto, datasetParam, "Dataset parameter");
        return datasetParam;
    }

    @Override
    public String checkDatasetNameInsideStep(String datasetParam) {
        if (StringUtils.isBlank(datasetParam)) {
            return datasetParam;
        }
        datasetParam = checkDatasetName(datasetParam, datasetParamRaw -> {});
        if (datasetParam.length() > 255) {
            return datasetParam.substring(0, 255);
        }
        return datasetParam;
    }

    private String checkDatasetName(String datasetParamRaw, Consumer<String> markTestCase) {
        datasetParamRaw = StringUtils.stripAccents(datasetParamRaw);
        datasetParamRaw = datasetParamRaw.replaceAll("\\s+", "_");
        datasetParamRaw = datasetParamRaw.replace("ß", "ss");

        String datasetParamName = datasetParamRaw;
        Matcher matcher = DATASET_NAME_REGEX.matcher(datasetParamName);
        if (matcher.find()) {
            datasetParamName = matcher.replaceAll(StringUtils.EMPTY);
            markTestCase.accept(datasetParamRaw);
        }
        return datasetParamName;
    }

    @Override
    public String checkFieldLength(ItemXrayDto itemXrayDto, String string, String fieldName) {
        if (string.length() > 255) {
            markWithWarning(
                    itemXrayDto, String.format("The field %s is too long (Max 255 characters)", fieldName));
            return string.substring(0, 255);
        }
        return string;
    }

    @Override
    public XrayStatus checkStatus(ItemXrayDto itemXrayDto, String status) {
        return checkField(
                itemXrayDto,
                "status",
                status,
                XrayStatus.DEFAULT,
                XrayStatus::getStatus,
                XrayStatus::getXrayFieldMapping);
    }

    @Override
    public XrayPriority checkPriority(ItemXrayDto itemXrayDto, String priority) {
        return checkField(
                itemXrayDto,
                "priority",
                priority,
                XrayPriority.DEFAULT,
                XrayPriority::getPriority,
                XrayPriority::getXrayFieldMapping);
    }

    private <T extends Enum<T>> T checkField(
            ItemXrayDto itemXrayDto,
            String fieldName,
            String value,
            T defaultValue,
            Function<String, T> getField,
            Supplier<String> getFieldMapping) {
        T field = getField.apply(value);
        if (field.equals(defaultValue)) {
            markWithWarning(
                    itemXrayDto,
                    String.format(
                            "The %s value '%s' does not match any of the allowed values: %s. The default will be used.",
                            fieldName, value, getFieldMapping.get()));
        }
        return field;
    }
}
