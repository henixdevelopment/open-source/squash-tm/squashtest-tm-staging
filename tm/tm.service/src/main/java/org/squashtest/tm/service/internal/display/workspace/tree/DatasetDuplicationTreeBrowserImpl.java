/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.workspace.tree;

import static org.squashtest.tm.domain.NodeReference.toNodeIds;

import com.google.common.collect.ListMultimap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.jooq.TableField;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.display.workspace.tree.DatasetDuplicationTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.dto.ProjectFilterDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.ParameterDao;
import org.squashtest.tm.service.internal.repository.display.DatasetDuplicationTreeBrowserDao;
import org.squashtest.tm.service.internal.repository.display.ProjectFilterDisplayDao;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional(readOnly = true)
public class DatasetDuplicationTreeBrowserImpl extends AbstractTreeBrowserImpl
        implements DatasetDuplicationTreeBrowser {

    private final DatasetDuplicationTreeBrowserDao datasetDuplicationTreeBrowserDao;
    private final ParameterDao parameterDao;

    private final UserAccountService userAccountService;

    private final CustomProjectFinder projectFinder;

    private final ProjectFilterDisplayDao projectFilterDao;

    public DatasetDuplicationTreeBrowserImpl(
            TreeNodeCollectorService treeNodeCollectorService,
            UserAccountService userAccountService,
            CustomProjectFinder projectFinder,
            ProjectFilterDisplayDao projectFilterDao,
            DatasetDuplicationTreeBrowserDao datasetDuplicationTreeBrowserDao,
            ParameterDao parameterDao) {
        super(treeNodeCollectorService, userAccountService, projectFinder, projectFilterDao);
        this.datasetDuplicationTreeBrowserDao = datasetDuplicationTreeBrowserDao;
        this.parameterDao = parameterDao;
        this.projectFinder = projectFinder;
        this.userAccountService = userAccountService;
        this.projectFilterDao = projectFilterDao;
    }

    @Override
    protected DatasetDuplicationTreeBrowserDao getTreeBrowserDao() {
        return datasetDuplicationTreeBrowserDao;
    }

    @Override
    protected int getPermissionMask() {
        return Permissions.WRITE.getMask();
    }

    private Set<NodeReference> filterNodesByDuplicationCriteria(
            Set<NodeReference> nodesToFilter, Long testCaseId) {
        List<String> sourceDatasetParameters = parameterDao.findOwnParameterNamesByTestCase(testCaseId);
        return getTreeBrowserDao()
                .getEligibleTestCaseNodeReferences(nodesToFilter, sourceDatasetParameters);
    }

    @Override
    public TreeGridResponse findSubHierarchyFilteredTestCases(
            Set<NodeReference> rootNodes, Set<NodeReference> openedNodeCandidates, Long testCaseId) {
        return super.findSubHierarchy(
                rootNodes,
                openedNodeCandidates,
                nodesToFilter -> filterNodesByDuplicationCriteria(nodesToFilter, testCaseId));
    }

    @Override
    protected void buildNodeHierarchy(
            Set<NodeReference> openedNodes,
            ListMultimap<NodeReference, NodeReference> childrenReferences,
            Map<NodeReference, DataRow> rows) {
        openedNodes.forEach(
                nodeReference -> {
                    if (rows.containsKey(nodeReference)) {
                        DataRow openedNode = rows.get(nodeReference);
                        openedNode.setState(DataRow.State.open);
                        List<NodeReference> childrenRef = childrenReferences.get(nodeReference);
                        List<NodeReference> filteredChildrenRef = new ArrayList<>();
                        childrenRef.forEach(
                                childRef -> {
                                    if (rows.containsKey(childRef)) {
                                        rows.get(childRef).setParentRowId(nodeReference.toNodeId());
                                        filteredChildrenRef.add(childRef);
                                    }
                                });
                        openedNode.setChildren(toNodeIds(filteredChildrenRef));
                    }
                });
    }

    @Override
    protected List<Long> getProjectIdsByPermissionAndClassName(
            int permission, String className, TableField<ProjectRecord, Long> libraryColumnField) {
        UserDto currentUser = userAccountService.findCurrentUserDto();

        List<Long> projectIdsByPermission =
                projectFinder.findAllProjectIdsByEligibleTCPermission(permission);

        ProjectFilterDto filter =
                projectFilterDao.getProjectFilterByUserLogin(currentUser.getUsername());
        if (Objects.nonNull(filter) && filter.getActivated()) {
            projectIdsByPermission.retainAll(
                    projectFilterDao.getProjectIdsByProjectFilter(filter.getId()));
        }
        return projectIdsByPermission;
    }
}
