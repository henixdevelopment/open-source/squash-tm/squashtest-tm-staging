/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.instruction.container;

import java.util.List;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.batchimport.Facility;
import org.squashtest.tm.service.internal.batchimport.instruction.ActionStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.CallStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.StepInstruction;

public class StepInstructionContainer extends InstructionContainer<StepInstruction> {

    public StepInstructionContainer(List<StepInstruction> instructions) {
        super(instructions);
    }

    List<ActionStepInstruction> filterActionStepInstruction(List<StepInstruction> instructions) {
        return instructions.stream()
                .filter(ActionStepInstruction.class::isInstance)
                .map(i -> (ActionStepInstruction) i)
                .toList();
    }

    List<CallStepInstruction> filterCallStepInstruction(List<StepInstruction> instructions) {
        return instructions.stream()
                .filter(CallStepInstruction.class::isInstance)
                .map(i -> (CallStepInstruction) i)
                .toList();
    }

    @Override
    protected void executeCreate(Facility facility, Project project) {
        List<StepInstruction> createInstructions = getCreateInstructions();

        List<ActionStepInstruction> actionStepInstructions =
                filterActionStepInstruction(createInstructions);

        if (!actionStepInstructions.isEmpty()) {
            facility.addActionSteps(actionStepInstructions, project);
        }

        List<CallStepInstruction> callStepInstructions = filterCallStepInstruction(createInstructions);

        if (!callStepInstructions.isEmpty()) {
            facility.addCallSteps(callStepInstructions, project);
        }
    }
}
