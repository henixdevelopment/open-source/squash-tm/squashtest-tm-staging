/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import java.util.Arrays;
import java.util.stream.Collectors;
import org.apache.commons.text.StringEscapeUtils;
import org.squashtest.tm.core.foundation.exception.ActionException;

public class ContextualizedActionWordException extends ActionException {
    final int errorPosition;
    final String errorContext;

    protected ContextualizedActionWordException(String message, String source, int errorPosition) {
        super(message);
        this.errorPosition = errorPosition;
        this.errorContext = buildErrorContext(source, errorPosition);
    }

    private static String buildErrorContext(String source, int position) {
        final int numWhiteSpaces = position - 1;
        final StringBuilder sb = new StringBuilder(StringEscapeUtils.escapeHtml4(source));

        if (numWhiteSpaces >= 0) {
            sb.append("\n")
                    .append(
                            Arrays.stream(new String[numWhiteSpaces]).map(s -> " ").collect(Collectors.joining()))
                    .append("^");
        }

        return sb.toString();
    }

    @Override
    public Object[] messageArgs() {
        return new Object[] {errorPosition, errorContext};
    }
}
