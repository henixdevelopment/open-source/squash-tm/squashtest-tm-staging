/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.Date;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportType;

public class PivotFormatImportDto {

    private long id;
    private String name;
    private String createdBy;
    private Date createdOn;
    private PivotFormatImportStatus status;
    private PivotFormatImportType type;
    private Date successfullyImportedOn;
    private String importLogFilePath;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public PivotFormatImportStatus getStatus() {
        return status;
    }

    public void setStatus(PivotFormatImportStatus status) {
        this.status = status;
    }

    public PivotFormatImportType getType() {
        return type;
    }

    public void setType(PivotFormatImportType type) {
        this.type = type;
    }

    public Date getSuccessfullyImportedOn() {
        return successfullyImportedOn;
    }

    public void setSuccessfullyImportedOn(Date successfullyImportedOn) {
        this.successfullyImportedOn = successfullyImportedOn;
    }

    public String getImportLogFilePath() {
        return importLogFilePath;
    }

    public void setImportLogFilePath(String importLogFilePath) {
        this.importLogFilePath = importLogFilePath;
    }
}
