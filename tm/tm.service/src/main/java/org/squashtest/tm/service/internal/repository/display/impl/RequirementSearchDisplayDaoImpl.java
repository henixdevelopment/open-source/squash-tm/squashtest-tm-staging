/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.groupConcatDistinct;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration.CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.tables.CustomField.CUSTOM_FIELD;
import static org.squashtest.tm.jooq.domain.tables.CustomFieldBinding.CUSTOM_FIELD_BINDING;
import static org.squashtest.tm.jooq.domain.tables.CustomFieldValue.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.tables.CustomFieldValueOption.CUSTOM_FIELD_VALUE_OPTION;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ATTACHMENTS_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.COVERAGES_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DESCRIPTION;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONES_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STATUS;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.GroupField;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.SelectField;
import org.jooq.SelectHavingStep;
import org.jooq.SelectSelectStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.requirement.RequirementNature;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.service.importer.EntityType;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.RequirementSearchDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class RequirementSearchDisplayDaoImpl implements RequirementSearchDisplayDao {

    private static final String REQ_MILESTONE_LOCKED_ALIAS = "REQ_MILESTONE_LOCKED";
    private static final String REQUIREMENT_SEARCH = "requirement-search";
    private static final String TEST_CASE_LIST = "TEST_CASE_LIST";
    private static final String SPRINT_LIST = "SPRINT_LIST";
    private Requirement innerRequirement = REQUIREMENT.as(RequestAliasesConstants.INNER_REQUIREMENT);
    private RequirementVersion innerVersions =
            REQUIREMENT_VERSION.as(RequestAliasesConstants.INNER_VERSIONS);
    private Field<Long> innerRequirementId =
            innerRequirement.RLN_ID.as(RequestAliasesConstants.INNER_REQUIREMENT_ID);
    private String versionCountAlias = RequestAliasesConstants.VERSIONS_COUNT;
    private Field<Integer> versionsCount = countDistinct(innerVersions.RES_ID).as(versionCountAlias);

    SelectHavingStep<Record2<Long, Integer>> innerVersionSelect =
            select(innerRequirementId, versionsCount)
                    .from(innerRequirement)
                    .innerJoin(innerVersions)
                    .on(innerRequirement.RLN_ID.eq(innerVersions.REQUIREMENT_ID))
                    .groupBy(innerRequirementId);

    private DSLContext jooq;

    private Table<Record> baseTable =
            REQUIREMENT_VERSION
                    .innerJoin(RESOURCE)
                    .on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
                    .innerJoin(REQUIREMENT)
                    .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
                    .innerJoin(REQUIREMENT_LIBRARY_NODE)
                    .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                    .innerJoin(PROJECT)
                    .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                    .leftJoin(HIGH_LEVEL_REQUIREMENT)
                    .on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.RLN_ID))
                    .leftJoin(innerVersionSelect)
                    .on(innerRequirementId.eq(REQUIREMENT.RLN_ID))
                    .leftJoin(MILESTONE_REQ_VERSION)
                    .on(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                    .leftJoin(MILESTONE)
                    .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID));

    private GroupField[] groupByFields =
            new GroupField[] {
                REQUIREMENT_VERSION.RES_ID,
                RESOURCE.RES_ID,
                REQUIREMENT.RLN_ID,
                REQUIREMENT_LIBRARY_NODE.RLN_ID,
                PROJECT.PROJECT_ID,
                innerRequirementId,
                HIGH_LEVEL_REQUIREMENT.RLN_ID
            };

    public RequirementSearchDisplayDaoImpl(DSLContext jooq) {
        this.jooq = jooq;
    }

    @Override
    public GridResponse getRows(List<Long> requirementVersionIds, GridRequest gridRequest) {

        GridResponse gridResponse = new GridResponse();

        SelectSelectStep<Record> selectBaseColumns = jooq.select(getFirstColumnsResults());
        List<String> simplifiedColumnDisplayGridIds = gridRequest.getSimplifiedColumnDisplayGridIds();
        appendCountDistinctFields(selectBaseColumns, simplifiedColumnDisplayGridIds);
        appendCufFields(selectBaseColumns, simplifiedColumnDisplayGridIds);
        selectBaseColumns
                .from(baseTable)
                .where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
                .groupBy(groupByFields)
                .stream()
                .forEach(
                        recordData -> {
                            DataRow dataRow = convertRecordIntoDataRow(recordData);
                            gridResponse.addDataRow(dataRow);
                        });

        return gridResponse;
    }

    private SelectField<?>[] getFirstColumnsResults() {
        return new SelectField<?>[] {
            REQUIREMENT_VERSION.RES_ID.as(RequestAliasesConstants.ID),
            REQUIREMENT_VERSION.REFERENCE,
            REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS),
            REQUIREMENT_VERSION.CRITICALITY,
            REQUIREMENT_VERSION.CATEGORY,
            REQUIREMENT_VERSION.VERSION_NUMBER,
            REQUIREMENT_VERSION.REQUIREMENT_ID,
            RESOURCE.NAME,
            RESOURCE.CREATED_BY,
            RESOURCE.LAST_MODIFIED_BY,
            RESOURCE.LAST_MODIFIED_ON,
            REQUIREMENT_LIBRARY_NODE.PROJECT_ID,
            RESOURCE.CREATED_ON,
            PROJECT.NAME.as(PROJECT_NAME),
            groupConcat(MILESTONE.LABEL)
                    .orderBy(MILESTONE.LABEL)
                    .separator(", ")
                    .as(RequestAliasesConstants.MILESTONE_LABELS),
            groupConcat(MILESTONE.STATUS)
                    .orderBy(MILESTONE.LABEL)
                    .separator(", ")
                    .as(RequestAliasesConstants.MILESTONE_STATUS),
            groupConcat(MILESTONE.END_DATE)
                    .orderBy(MILESTONE.LABEL)
                    .separator(", ")
                    .as(RequestAliasesConstants.MILESTONE_END_DATE),
            DSL.select(groupConcatDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
                    .from(REQUIREMENT_VERSION_COVERAGE)
                    .where(
                            REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                    .asField(TEST_CASE_LIST),
            DSL.select(groupConcatDistinct(SPRINT_REQ_VERSION.SPRINT_ID))
                    .from(SPRINT_REQ_VERSION)
                    .where(SPRINT_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                    .asField(SPRINT_LIST),
            DSL.selectCount()
                    .from(ATTACHMENT)
                    .where(ATTACHMENT.ATTACHMENT_LIST_ID.eq(RESOURCE.ATTACHMENT_LIST_ID))
                    .asField(ATTACHMENTS_ALIAS),
            DSL.min(versionsCount).as(versionCountAlias),
            when(HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull(), RequirementNature.HIGH_LEVEL.toString())
                    .otherwise(RequirementNature.STANDARD.toString())
                    .as(RequestAliasesConstants.NATURE),
            when(RESOURCE.DESCRIPTION.eq(""), false).otherwise(true).as(DESCRIPTION),
            when(
                            DSL.exists(
                                    DSL.selectOne()
                                            .from(RLN_RELATIONSHIP)
                                            .innerJoin(REQUIREMENT)
                                            .on(RLN_RELATIONSHIP.ANCESTOR_ID.eq(REQUIREMENT.RLN_ID))
                                            .where(
                                                    RLN_RELATIONSHIP.DESCENDANT_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))),
                            true)
                    .otherwise(false)
                    .as(RequestAliasesConstants.REQUIREMENT_HAS_PARENT_REQ),
            DSL.selectCount()
                    .from(RLN_RELATIONSHIP_CLOSURE)
                    .where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                    .and(RLN_RELATIONSHIP_CLOSURE.DEPTH.ne((short) 0))
                    .asField(RequestAliasesConstants.CHILD_OF_REQUIREMENT),
            DSL.selectCount()
                    .from(REQUIREMENT)
                    .where(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                    .asField(RequestAliasesConstants.LINKED_STANDARD_REQUIREMENT),
            when(
                            DSL.exists(
                                    DSL.selectOne()
                                            .from(REQUIREMENT_VERSION_LINK)
                                            .where(
                                                    REQUIREMENT_VERSION_LINK.RELATED_REQUIREMENT_VERSION_ID.eq(
                                                            REQUIREMENT_VERSION.RES_ID))),
                            true)
                    .otherwise(false)
                    .as(RequestAliasesConstants.HAS_LINK_TYPE),
            when(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.isNotNull(), true)
                    .otherwise(false)
                    .as(REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT),
        };
    }

    /**
     * If simplifiedColumnDisplayGridIds does not contain "requirement-search", adds to the
     * selectBaseColumns countdistinct fields. If simplifiedColumnDisplayGridIds contains
     * "requirement-search", adds the same columns but these columns will be set to null (for
     * performance issues).
     *
     * @param selectBaseColumns
     * @param simplifiedColumnDisplayGridIds
     */
    private void appendCountDistinctFields(
            SelectSelectStep<Record> selectBaseColumns, List<String> simplifiedColumnDisplayGridIds) {
        if (!simplifiedColumnDisplayGridIds.contains(REQUIREMENT_SEARCH)) {
            selectBaseColumns.select(
                    select(countDistinct(MILESTONE_REQ_VERSION.MILESTONE_ID))
                            .from(MILESTONE_REQ_VERSION)
                            .where(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                            .groupBy(REQUIREMENT_VERSION.RES_ID)
                            .asField(MILESTONES_ALIAS),
                    select(countDistinct(MILESTONE.MILESTONE_ID))
                            .from(MILESTONE)
                            .join(MILESTONE_REQ_VERSION)
                            .on(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                            .where(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                            .and(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name()))
                            .groupBy(REQUIREMENT_VERSION.RES_ID)
                            .asField(REQ_MILESTONE_LOCKED_ALIAS),
                    select(countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
                            .from(REQUIREMENT_VERSION_COVERAGE)
                            .where(
                                    REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(
                                            REQUIREMENT_VERSION.RES_ID))
                            .groupBy(REQUIREMENT_VERSION.RES_ID)
                            .asField(COVERAGES_ALIAS));
        } else {
            selectBaseColumns.select(
                    DSL.value((Integer) null).as(MILESTONES_ALIAS),
                    DSL.value((Integer) null).as(REQ_MILESTONE_LOCKED_ALIAS),
                    DSL.value((Integer) null).as(COVERAGES_ALIAS));
        }
    }

    /**
     * If simplifiedColumnDisplayGridIds does not contain "requirement-search", adds to the
     * selectBaseColumns a number of columns equal to the number of custom fields bound to the
     * requirements. These extra columns extract for each test case the value of the cuf. If
     * simplifiedColumnDisplayGridIds contains "requirement-search", adds the same columns but these
     * columns will remain empty (for performance issues).
     *
     * @param selectBaseColumns
     * @param simplifiedColumnDisplayGridIds
     */
    private void appendCufFields(
            SelectSelectStep<Record> selectBaseColumns, List<String> simplifiedColumnDisplayGridIds) {

        List<Long> cufIds =
                jooq.selectDistinct(CUSTOM_FIELD.CF_ID)
                        .from(CUSTOM_FIELD)
                        .innerJoin(CUSTOM_FIELD_BINDING)
                        .on(CUSTOM_FIELD.CF_ID.eq(CUSTOM_FIELD_BINDING.CF_ID))
                        .where(CUSTOM_FIELD_BINDING.BOUND_ENTITY.eq(EntityType.REQUIREMENT_VERSION.toString()))
                        .fetchInto(Long.class);

        if (!simplifiedColumnDisplayGridIds.contains(REQUIREMENT_SEARCH)) {
            for (Long cufId : cufIds) {
                selectBaseColumns.select(
                        DSL.select(
                                        when(
                                                        CUSTOM_FIELD_VALUE.FIELD_TYPE.eq(String.valueOf(InputType.TAG)),
                                                        groupConcat(CUSTOM_FIELD_VALUE_OPTION.LABEL)
                                                                .orderBy(CUSTOM_FIELD_VALUE_OPTION.POSITION)
                                                                .separator(", "))
                                                .otherwise(CUSTOM_FIELD_VALUE.VALUE))
                                .from(CUSTOM_FIELD_VALUE)
                                .leftOuterJoin(CUSTOM_FIELD_VALUE_OPTION)
                                .on(CUSTOM_FIELD_VALUE.CFV_ID.eq(CUSTOM_FIELD_VALUE_OPTION.CFV_ID))
                                .where(CUSTOM_FIELD_VALUE.CF_ID.eq(cufId))
                                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.eq(REQUIREMENT_VERSION.RES_ID))
                                .and(
                                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(
                                                EntityType.REQUIREMENT_VERSION.toString()))
                                .groupBy(CUSTOM_FIELD_VALUE.FIELD_TYPE, CUSTOM_FIELD_VALUE.VALUE)
                                .asField(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS + cufId));
            }
        } else {
            for (Long cufId : cufIds) {
                selectBaseColumns.select(
                        DSL.value((String) null).as(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS + cufId));
            }
        }
    }

    private DataRow convertRecordIntoDataRow(Record record) {
        DataRow dataRow = new DataRow();
        dataRow.setId(record.get(RequestAliasesConstants.ID).toString());
        dataRow.setProjectId(record.get(REQUIREMENT_LIBRARY_NODE.PROJECT_ID));
        Map<String, Object> rawData = record.intoMap();
        Map<String, Object> data = new HashMap<>();

        // Using 'Collectors.toMap' won't work for entries with null values
        for (Map.Entry<String, Object> entry : rawData.entrySet()) {
            data.put(convertField(entry.getKey()), entry.getValue());
        }
        dataRow.setData(data);
        return dataRow;
    }

    private String convertField(String fieldName) {
        if (fieldName.startsWith(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS)) {
            return fieldName;
        } else {
            Converter<String, String> converter =
                    CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
            return converter.convert(fieldName);
        }
    }
}
