/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import java.util.Collection;
import java.util.UUID;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;
import org.squashtest.tm.service.internal.deletion.jdbc.JdbcBatchReorderHelper;

@Component
public class JdbcSprintReqVersionDeletionHandlerFactory {

    private final DSLContext dslContext;
    private final EntityManager entityManager;
    private final AttachmentRepository attachmentRepository;
    private final JdbcBatchReorderHelper reorderHelper;

    public JdbcSprintReqVersionDeletionHandlerFactory(
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper) {
        this.dslContext = dslContext;
        this.entityManager = entityManager;
        this.attachmentRepository = attachmentRepository;
        this.reorderHelper = reorderHelper;
    }

    public JdbcSprintReqVersionDeletionHandler build(Collection<Long> sprintReqVersionIds) {
        return build(sprintReqVersionIds, UUID.randomUUID().toString());
    }

    public JdbcSprintReqVersionDeletionHandler build(
            Collection<Long> sprintReqVersionIds, String operationId) {
        return new JdbcSprintReqVersionDeletionHandler(
                dslContext,
                entityManager,
                attachmentRepository,
                reorderHelper,
                operationId,
                sprintReqVersionIds);
    }
}
