/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.field;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.requirement.RemoteRequirementPerimeterStatus;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Component
public class RequirementFolderCollector extends AbstractTreeNodeCollector
        implements TreeNodeCollector {

    private static final String HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ =
            "HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ";

    private static final String IS_SYNCHRONIZED = "IS_SYNCHRONIZED";

    public RequirementFolderCollector(
            DSLContext dsl,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            MilestoneDisplayDao milestoneDisplayDao,
            ActiveMilestoneHolder activeMilestoneHolder) {
        super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
    }

    @Override
    protected Map<Long, DataRow> doCollect(List<Long> ids) {
        Map<Long, DataRow> requirementFolders = collectRequirementFolderRows(ids);
        appendMilestonesByProject(requirementFolders);
        appendHasOutOfPerimeterOrDeletedRemoteReq(requirementFolders);
        return requirementFolders;
    }

    private Map<Long, DataRow> collectRequirementFolderRows(List<Long> ids) {
        return dsl
                .select(
                        // @formatter:off
                        REQUIREMENT_LIBRARY_NODE.RLN_ID,
                        REQUIREMENT_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS),
                        RESOURCE.NAME,
                        count(RLN_RELATIONSHIP.ANCESTOR_ID).as(CHILD_COUNT_ALIAS),
                        field(REQUIREMENT_FOLDER_SYNC_EXTENDER.RF_SYNC_EXTENDER_ID.isNotNull())
                                .as(IS_SYNCHRONIZED),
                        REMOTE_SYNCHRONISATION.LAST_SYNC_STATUS,
                        REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_NAME)
                .from(REQUIREMENT_LIBRARY_NODE)
                .leftJoin(RLN_RELATIONSHIP)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP.ANCESTOR_ID))
                .innerJoin(REQUIREMENT_FOLDER)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_FOLDER.RLN_ID))
                .innerJoin(RESOURCE)
                .on(REQUIREMENT_FOLDER.RES_ID.eq(RESOURCE.RES_ID))
                .leftJoin(REQUIREMENT_FOLDER_SYNC_EXTENDER)
                .on(REQUIREMENT_FOLDER_SYNC_EXTENDER.REQUIREMENT_FOLDER_ID.eq(REQUIREMENT_FOLDER.RLN_ID))
                .leftJoin(REMOTE_SYNCHRONISATION)
                .on(
                        REQUIREMENT_FOLDER_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.eq(
                                REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID))
                .where(REQUIREMENT_LIBRARY_NODE.RLN_ID.in(ids))
                .groupBy(
                        REQUIREMENT_LIBRARY_NODE.RLN_ID,
                        RESOURCE.RES_ID,
                        RLN_RELATIONSHIP.ANCESTOR_ID,
                        REQUIREMENT_FOLDER_SYNC_EXTENDER.RF_SYNC_EXTENDER_ID,
                        REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID)
                // @formatter:on
                .fetch()
                .stream()
                .collect(
                        Collectors.toMap(
                                tuple -> tuple.get(REQUIREMENT_LIBRARY_NODE.RLN_ID),
                                tuple -> {
                                    DataRow dataRow = new DataRow();
                                    dataRow.setId(
                                            new NodeReference(
                                                            NodeType.REQUIREMENT_FOLDER,
                                                            tuple.get(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                                                    .toNodeId());
                                    dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
                                    dataRow.setState(
                                            tuple.get(CHILD_COUNT_ALIAS, Integer.class) > 0
                                                    ? DataRow.State.closed
                                                    : DataRow.State.leaf);
                                    dataRow.setData(tuple.intoMap());
                                    return dataRow;
                                }));
    }

    private void appendHasOutOfPerimeterOrDeletedRemoteReq(Map<Long, DataRow> libraryRows) {
        libraryRows.forEach(
                (aLong, dataRow) -> {
                    boolean hasOutOfPerimeterOrDeletedRemoteReq = false;
                    boolean folderIsSynchronized = (boolean) dataRow.getData().get(IS_SYNCHRONIZED);
                    if (folderIsSynchronized) {
                        long folderId =
                                Long.parseLong(
                                        dataRow.getId().replace(NodeType.REQUIREMENT_FOLDER.getTypeName() + "-", ""));

                        List<Long> folderAndSubFolders = findSynchronizedSubFolders(folderId);
                        List<Long> requirementsInFoldersAndSubFolders =
                                findSynchronizedRequirementsInsideFolders(folderAndSubFolders);
                        List<Long> requirementsHavingChildren = new ArrayList<>();

                        requirementsInFoldersAndSubFolders.forEach(
                                requirementId -> {
                                    if (checkIfSynchronizedRequirementHasChildren(requirementId)) {
                                        requirementsHavingChildren.add(requirementId);
                                    }
                                });

                        List<Long> combinedResult =
                                Stream.of(folderAndSubFolders, requirementsHavingChildren)
                                        .flatMap(Collection::stream)
                                        .toList();

                        hasOutOfPerimeterOrDeletedRemoteReq =
                                checkIfFoldersHaveOutOfPerimeterOrDeletedRemoteReq(combinedResult);
                    }

                    dataRow.addData(
                            HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ, hasOutOfPerimeterOrDeletedRemoteReq);
                });
    }

    private List<Long> findSynchronizedSubFolders(Long folderId) {
        return dsl.select(REQUIREMENT_FOLDER.RLN_ID)
                .from(RLN_RELATIONSHIP_CLOSURE)
                .innerJoin(REQUIREMENT_FOLDER)
                .on(REQUIREMENT_FOLDER.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(folderId))
                .fetch(REQUIREMENT_FOLDER.RLN_ID);
    }

    private List<Long> findSynchronizedRequirementsInsideFolders(List<Long> folderIds) {
        return dsl.select(REQUIREMENT.RLN_ID)
                .from(RLN_RELATIONSHIP)
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP.DESCENDANT_ID))
                .where(RLN_RELATIONSHIP.ANCESTOR_ID.in(folderIds))
                .fetch(REQUIREMENT.RLN_ID);
    }

    private boolean checkIfSynchronizedRequirementHasChildren(Long requirementId) {
        Integer count =
                dsl.selectCount()
                        .from(RLN_RELATIONSHIP)
                        .where(RLN_RELATIONSHIP.ANCESTOR_ID.eq(requirementId))
                        .fetchOneInto(int.class);
        return count > 0;
    }

    private boolean checkIfFoldersHaveOutOfPerimeterOrDeletedRemoteReq(List<Long> folderIds) {
        Integer count =
                dsl.select(count(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_PERIMETER_STATUS))
                        .from(RLN_RELATIONSHIP)
                        .leftJoin(REQUIREMENT_SYNC_EXTENDER)
                        .on(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID.eq(RLN_RELATIONSHIP.DESCENDANT_ID))
                        .and(
                                REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_PERIMETER_STATUS.in(
                                        String.valueOf(RemoteRequirementPerimeterStatus.OUT_OF_CURRENT_PERIMETER),
                                        String.valueOf(RemoteRequirementPerimeterStatus.NOT_FOUND)))
                        .where(RLN_RELATIONSHIP.ANCESTOR_ID.in(folderIds))
                        .fetchOneInto(int.class);
        return count > 0;
    }

    @Override
    public NodeType getHandledEntityType() {
        return NodeType.REQUIREMENT_FOLDER;
    }
}
