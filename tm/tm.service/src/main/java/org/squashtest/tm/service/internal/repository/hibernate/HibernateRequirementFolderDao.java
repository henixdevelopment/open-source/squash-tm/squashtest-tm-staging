/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityGraph;
import org.hibernate.jpa.QueryHints;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.service.internal.repository.EntityGraphName;
import org.squashtest.tm.service.internal.repository.JpaQueryString;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.RequirementFolderDao;

import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

@Repository
public class HibernateRequirementFolderDao extends HibernateEntityDao<RequirementFolder>
        implements RequirementFolderDao {

    @SuppressWarnings("rawtypes")
    @Override
    public RequirementFolder findByContent(final RequirementLibraryNode node) {
        SetQueryParametersCallback callback = new SetNodeContentParameter(node);

        return executeEntityNamedQuery("requirementFolder.findByContent", callback);
    }

    @Override
    public List<String> findNamesInLibraryStartingWith(final long libraryId, final String nameStart) {
        SetQueryParametersCallback newCallBack1 =
                new ContainerIdNameStartParameterCallback(libraryId, nameStart);
        return executeListNamedQuery("requirementFolder.findNamesInLibraryStartingWith", newCallBack1);
    }

    @Override
    public List<String> findNamesInNodeStartingWith(final long nodeId, final String nameStart) {
        SetQueryParametersCallback newCallBack1 =
                new ContainerIdNameStartParameterCallback(nodeId, nameStart);
        return executeListNamedQuery(
                "requirementLibraryNode.findNamesInNodeStartingWith", newCallBack1);
    }

    @Override
    public RequirementFolder findByIdWithContent(Long id) {

        EntityGraph<RequirementFolder> graph = entityManager.createEntityGraph(RequirementFolder.class);
        graph.addSubgraph(EntityGraphName.CONTENT, RequirementLibraryNode.class);

        return entityManager
                .createQuery(JpaQueryString.FIND_REQUIREMENT_FOLDER_BY_ID, RequirementFolder.class)
                .setHint("javax.persistence.fetchgraph", graph)
                .setParameter(ParameterNames.ID, id)
                .getSingleResult();
    }

    @Override
    public RequirementFolder loadContainerForPaste(long id) {

        EntityGraph<RequirementFolder> graph = entityManager.createEntityGraph(RequirementFolder.class);
        graph
                .addSubgraph(EntityGraphName.CONTENT, Requirement.class)
                .addAttributeNodes(EntityGraphName.SYNC_EXTENDER, EntityGraphName.VERSIONS);

        return entityManager
                .createQuery(JpaQueryString.FIND_REQUIREMENT_FOLDER_BY_ID, RequirementFolder.class)
                .setHint(QueryHints.HINT_FETCHGRAPH, graph)
                .setParameter(ParameterNames.ID, id)
                .getSingleResult();
    }

    @Override
    public RequirementFolder loadForNodeAddition(Long folderId) {
        return entityManager.createQuery("""
                select folder from RequirementFolder folder
                join fetch folder.project project
                join fetch project.requirementCategories
                left join fetch folder.requirementFolderSyncExtender
                left join fetch folder.content content
                left join fetch content.syncExtender
                left join fetch content.requirementFolderSyncExtender
                where folder.id = :id""",
                RequirementFolder.class)
            .setParameter(ID, folderId)
            .getSingleResult();
    }

    @Override
    public List<RequirementFolder> loadForNodeAddition(Collection<Long> folderIds) {
        return entityManager.createQuery("""
                select distinct folder from RequirementFolder folder
                join fetch folder.project project
                join fetch project.requirementCategories
                left join fetch folder.requirementFolderSyncExtender
                left join fetch folder.content content
                left join fetch content.syncExtender
                left join fetch content.requirementFolderSyncExtender
                where folder.id in :ids""",
                RequirementFolder.class)
            .setParameter(IDS, folderIds)
            .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long[]> findPairedContentForList(final List<Long> ids) {

        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        NativeQuery query =
                currentSession()
                        .createNativeQuery(
                                NativeQueries.REQUIREMENT_FOLDER_SQL_FIND_PAIRED_CONTENT_FOR_FOLDERS);
        query.setParameterList("folderIds", ids, LongType.INSTANCE);
        query.addScalar("ancestor_id", LongType.INSTANCE);
        query.addScalar("descendant_id", LongType.INSTANCE);

        List<Object[]> result = query.list();

        return toArrayOfLong(result);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> findContentForList(List<Long> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        NativeQuery query =
                currentSession()
                        .createNativeQuery(NativeQueries.REQUIREMENT_FOLDER_SQL_FIND_CONTENT_FOR_FOLDER);
        query.setParameterList("folderIds", ids, LongType.INSTANCE);
        query.addScalar("descendant_id", LongType.INSTANCE);

        return query.list();
    }

    private List<Long[]> toArrayOfLong(List<Object[]> input) {
        List<Long[]> result = new ArrayList<>();

        for (Object[] pair : input) {
            Long[] newPair = new Long[] {(Long) pair[0], (Long) pair[1]};
            result.add(newPair);
        }

        return result;
    }

    @Override
    public RequirementFolder findParentOf(final Long id) {
        SetQueryParametersCallback newCallBack = new ContentIdParametterCallback(id);
        return executeEntityNamedQuery("requirementFolder.findParentOf", newCallBack);
    }

    private static final class ContentIdParametterCallback implements SetQueryParametersCallback {
        private long contentId;

        private ContentIdParametterCallback(long contentId) {
            this.contentId = contentId;
        }

        @Override
        public void setQueryParameters(Query query) {
            query.setParameter("contentId", contentId, LongType.INSTANCE);
        }
    }
}
