/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.domain.users.UsersGroup.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_PARTY;

import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.internal.repository.CustomApiTokenDao;

@Service
public class ApiTokenDaoImpl implements CustomApiTokenDao {

    @Inject private DSLContext dsl;

    @Override
    public boolean isTestAutomationServerUser(long userId) {
        return dsl.fetchExists(
                dsl.selectOne()
                        .from(CORE_GROUP_MEMBER)
                        .innerJoin(CORE_PARTY)
                        .on(CORE_PARTY.PARTY_ID.eq(CORE_GROUP_MEMBER.PARTY_ID))
                        .innerJoin(CORE_GROUP)
                        .on(CORE_GROUP.ID.eq(CORE_GROUP_MEMBER.GROUP_ID))
                        .where(CORE_PARTY.PARTY_ID.eq(userId))
                        .and(CORE_GROUP.QUALIFIED_NAME.eq(TEST_AUTOMATION_SERVER)));
    }
}
