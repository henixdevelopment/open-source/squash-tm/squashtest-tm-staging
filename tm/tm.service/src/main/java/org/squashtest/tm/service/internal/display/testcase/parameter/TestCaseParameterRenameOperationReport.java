/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.testcase.parameter;

import java.util.List;
import org.squashtest.tm.service.internal.display.dto.testcase.AbstractTestStepDto;

public class TestCaseParameterRenameOperationReport {

    private List<AbstractTestStepDto> testStepList;
    private String prerequisite;

    public TestCaseParameterRenameOperationReport(
            List<AbstractTestStepDto> testStepList, String prerequisite) {
        this.testStepList = testStepList;
        this.prerequisite = prerequisite;
    }

    public List<AbstractTestStepDto> getTestStepList() {
        return testStepList;
    }

    public void setTestStepList(List<AbstractTestStepDto> testStepList) {
        this.testStepList = testStepList;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }
}
