/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.copier;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.domain.library.TreeNode;

public class ChildEntityDto {
    private final Long parentId;
    private final List<TreeNode> children = new ArrayList<>();

    private int prefilterSize = 0;
    private Long targetContainerId;
    private NodeContainer<TreeNode> targetContainer;

    public ChildEntityDto(Long parentId) {
        this.parentId = parentId;
    }

    public Long getParentId() {
        return parentId;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public Long getTargetContainerId() {
        return targetContainerId;
    }

    public void setTargetContainerId(Long targetContainerId) {
        this.targetContainerId = targetContainerId;
    }

    public NodeContainer<TreeNode> getTargetContainer() {
        return targetContainer;
    }

    public void setTargetContainer(NodeContainer<TreeNode> targetContainer) {
        this.targetContainer = targetContainer;
    }

    public int getPrefilterSize() {
        return prefilterSize;
    }

    public void setPrefilterSize(int prefilterSize) {
        this.prefilterSize = prefilterSize;
    }
}
