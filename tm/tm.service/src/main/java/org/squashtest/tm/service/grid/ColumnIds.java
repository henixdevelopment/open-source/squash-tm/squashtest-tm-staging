/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.grid;

import java.util.Arrays;
import java.util.List;

public enum ColumnIds {
    ATTACHMENTS("attachments", "label.numberOfAttachments.info"),
    AUTOMATED_TEST_TECHNOLOGY("automatedTestTechnology", "test-case.automated-test-technology.label"),
    TEST_CASE_AUTOMATION_PRIORITY("automationPriority", "test-case.automation-priority.label"),
    COVERAGE_OF_ASSOCIATED_REQUIREMENTS("coverages", "label.long.numberOfAssociatedRequirements"),
    CALL_STEP_COUNT("callStepCount", "chart.column.TEST_CASE_CALLCOUNT"),
    EXECUTION_COUNT("executionCount", "chart.column.TEST_CASE_EXECOUNT"),
    ISSUE_COUNT("issueCount", "chart.column.ITERATION_ISSUECOUNT"),
    COVERAGE_OF_ASSOCIATED_TEST_CASE("coverages", "label.numberOfTestCases.info"),
    CREATED_BY("createdBy", "label.createdBy"),
    CREATED_ON("createdOn", "label.createdOn"),
    DATASETS("datasets", "label.DatasetNumber"),
    DRAFTED_BY_AI("draftedByAi", "label.DraftedByAi"),
    DESCRIPTION("description", "label.Description"),
    EXECUTION_STATUS("executionStatus", "label.Status"),
    EXECUTION_DETAILS("executionDetails", "label.executionDetails"),
    EXECUTION_REPORTS("executionReport", "label.executionReport"),
    ID("id", "label.id"),
    LABEL("label", "label.Label"),
    LAST_EXECUTED_ON("lastExecutedOn", "chart.column.ITEM_TEST_PLAN_LASTEXECON"),
    LAST_MODIFIED_BY("lastModifiedBy", "label.modifiedBy"),
    LAST_MODIFIED_ON("lastModifiedOn", "label.modifiedOn"),
    LINKED_STANDARD_REQUIREMENT(
            "linkedStandardRequirement", "requirement.linked-standard-requirement.label"),
    LAUNCHED_FROM("launchedFrom", "label.launchedFrom"),
    MILESTONES("milestones", "label.long.numberOfAssociatedMilestones"),
    MILESTONES_END_DATE("milestoneEndDate", "milestone.endDate"),
    MILESTONES_LABELS("milestoneLabels", "label.MilestonesName"),
    MILESTONES_STATUS("milestoneStatus", "milestone.status"),
    NAME("name", "label.Name"),
    NUMBER_OF_ASSOCIATED_ITERATIONS("iterations", "label.long.numberOfAssociatedIterations"),
    NUMBER_OF_ATTACHMENTS("attachments", "label.long.numberOfAttachments"),
    NUMBER_OF_TEST_STEPS("steps", "label.long.numberOfTestSteps"),
    PARAM_COUNT("paramCount", "label.numberOfParam"),
    PROJECT_NAME("projectName", "label.project"),
    REFERENCE("reference", "label.reference"),
    REQUEST_STATUS("requestStatus", "test-case.automation-status.label"),
    REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT(
            "requirementBoundToHighLevelRequirement",
            "requirement.bound-to-a-high-level-requirement.label"),
    REQUIREMENT_CATEGORY("category", "requirement.category.label"),
    REQUIREMENT_CRITICALITY("criticality", "requirement.criticality.label"),
    REQUIREMENT_ID("requirementId", "label.requirementId"),
    REQUIREMENT_NATURE("nature", "requirement.nature.label"),
    REQUIREMENT_VERSION_HAS_CHILDREN("childOfRequirement", "requirement-version.has-children.label"),
    REQUIREMENT_VERSION_HAS_PARENT("hasParent", "requirement-version.has-parent.label"),
    REQUIREMENT_VERSION_HAS_LINK_TYPE(
            "hasLinkType", "requirement-version.linked-requirement-version.panel.title"),
    REQ_VERSION_BOUND_TO_ITEM("reqVersionBoundToItem", "req-version-bound-to-item.label"),
    STATUS("status", "label.Status"),
    TEST_CASE_AUTOMATION_INDICATOR("automatable", "test-case.automation-indicator.label"),
    TEST_CASE_HASAUTOSCRIPT("hasAutoScript", "chart.column.TEST_CASE_HASAUTOSCRIPT"),
    TEST_CASE_HAS_BOUND_SCM_REPOSITORY("hasBoundScmRepository", "label.ScmRepository"),
    TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE(
            "hasBoundAutomatedTestReference", "automation.datatable.headers.automatedtestreference"),
    TEST_CASE_IMPORTANCE("importance", "test-case.importance.label"),
    TEST_CASE_KIND("kind", "test-case.format.label"),
    TEST_CASE_NATURE("nature", "test-case.nature.label"),
    TEST_CASE_STATUS("status", "test-case.status.label"),
    TEST_CASE_TYPE("type", "test-case.type.label"),
    TEST_CASE_NAME("testCaseName", "chart.entityType.TEST_CASE"),
    TRANSMITTED_ON("transmittedOn", "automation.datatable.headers.transmittedon"),
    VERSION_COUNT("versionNumber", "label.version"),
    VERSION_ID("versionId", "label.numberOfTestCases.info"),
    VERSION_NUMBER("versionsCount", "label.numberOfVersions.info");

    public static final List<String> COLUMNS_NOT_TRIGGERING_REQ_REQUEST =
            Arrays.asList(
                    PROJECT_NAME.getColumnId(),
                    REQUIREMENT_ID.getColumnId(),
                    ID.getColumnId(),
                    REFERENCE.getColumnId(),
                    NAME.getColumnId(),
                    STATUS.getColumnId(),
                    REQUIREMENT_CRITICALITY.getColumnId(),
                    REQUIREMENT_CATEGORY.getColumnId(),
                    REQUIREMENT_NATURE.getColumnId(),
                    DESCRIPTION.getColumnId(),
                    REQUIREMENT_VERSION_HAS_CHILDREN.getColumnId(),
                    REQUIREMENT_VERSION_HAS_PARENT.getColumnId(),
                    REQUIREMENT_VERSION_HAS_LINK_TYPE.getColumnId(),
                    REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT.getColumnId(),
                    LINKED_STANDARD_REQUIREMENT.getColumnId(),
                    CREATED_BY.getColumnId(),
                    CREATED_ON.getColumnId(),
                    LAST_MODIFIED_BY.getColumnId(),
                    LAST_MODIFIED_ON.getColumnId(),
                    VERSION_NUMBER.getColumnId(),
                    VERSION_COUNT.getColumnId(),
                    "#",
                    "folder",
                    "detail",
                    "delete",
                    "startExecution",
                    "execution");
    public static final List<String> COLUMNS_NOT_TRIGGERING_TEST_CASE_REQUEST =
            Arrays.asList(
                    PROJECT_NAME.getColumnId(),
                    ID.getColumnId(),
                    REFERENCE.getColumnId(),
                    NAME.getColumnId(),
                    STATUS.getColumnId(),
                    TEST_CASE_IMPORTANCE.getColumnId(),
                    TEST_CASE_NATURE.getColumnId(),
                    TEST_CASE_TYPE.getColumnId(),
                    TEST_CASE_KIND.getColumnId(),
                    DESCRIPTION.getColumnId(),
                    TEST_CASE_AUTOMATION_INDICATOR.getColumnId(),
                    TEST_CASE_AUTOMATION_PRIORITY.getColumnId(),
                    TRANSMITTED_ON.getColumnId(),
                    REQUEST_STATUS.getColumnId(),
                    TEST_CASE_HASAUTOSCRIPT.getColumnId(),
                    AUTOMATED_TEST_TECHNOLOGY.getColumnId(),
                    TEST_CASE_HAS_BOUND_SCM_REPOSITORY.getColumnId(),
                    TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE.getColumnId(),
                    CREATED_ON.getColumnId(),
                    CREATED_BY.getColumnId(),
                    LAST_MODIFIED_ON.getColumnId(),
                    LAST_MODIFIED_BY.getColumnId(),
                    "#",
                    "folder",
                    "detail",
                    "delete",
                    "startExecution",
                    "execution");

    public static final List<ColumnIds> COLUMN_IDS_LINKED_TO_MILESTONES =
            Arrays.asList(
                    ColumnIds.MILESTONES,
                    ColumnIds.MILESTONES_LABELS,
                    ColumnIds.MILESTONES_STATUS,
                    ColumnIds.MILESTONES_END_DATE);

    private final String columnId;
    private final String label;

    ColumnIds(String columnId, String label) {

        this.columnId = columnId;
        this.label = label;
    }

    public String getColumnId() {
        return columnId;
    }

    public String getLabel() {
        return label;
    }
}
