/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service;


import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.springframework.stereotype.Service;
import org.squashtest.tm.exception.DatabaseVersionMismatchException;
import org.squashtest.tm.service.configuration.ConfigurationService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Service
public class DatabaseVersionChecker {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseVersionChecker.class);

/*
   ##########################################################################################################
   # WARNING: THIS VARIABLE NEEDS TO BE UPDATED BEFORE EACH RELEASE TO REFLECT THE CURRENT DATABASE VERSION #
   ##########################################################################################################
*/
    public static final String DATABASE_SQUASH_VERSION = "10.0.0";

    @Inject
    ConfigurationService configurationService;

    @PostConstruct
    public void checkDatabaseVersion() {
        String databaseVersion= configurationService.findConfiguration("squashtest.tm.database.version");


        if (databaseVersion == null) {
            throw new DatabaseVersionMismatchException("Unable to find the database version.");
        }

        if (!DATABASE_SQUASH_VERSION.equals(databaseVersion)) {
            String message = String.format("The database version is not compatible. Required version : %s, current version : %s", DATABASE_SQUASH_VERSION, databaseVersion);
            throw new DatabaseVersionMismatchException(message);
        }

        LOGGER.info("The database version is compliant.");
    }
}
