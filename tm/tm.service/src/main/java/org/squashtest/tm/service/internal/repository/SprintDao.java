/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.Sprint;

public interface SprintDao extends EntityDao<Sprint> {
    // Do not remove, this method is used in plugin xsquash4jira
    List<Sprint> findByRemoteSynchronisationIdAndRemoteSprintIds(
            Long remoteSynchronisationId, List<Long> remoteSprintIds);

    // Do not remove, this method is used in plugin xsquash4jira
    /**
     * Find the list of sprints that no longer correspond to a remote sprint
     *
     * @param remoteSynchronisationId sync ID
     * @param existingRemoteSprintIds list of existing remote sprint IDs
     * @return squash sprints for which the corresponding remote sprint has been deleted
     */
    List<Long> findSquashSprintIdsHavingDeletedRemoteSprint(
            Long remoteSynchronisationId, List<Long> existingRemoteSprintIds);

    void deleteRemoteSynchronisationFromSprint(List<Long> remoteSyncIds);

    void deleteRemoteSynchronisationFromSprintId(long sprintId);

    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    void updateRemoteStateOfSynchronisedSprintsByIds(List<Long> sprintIds, String status);
}
