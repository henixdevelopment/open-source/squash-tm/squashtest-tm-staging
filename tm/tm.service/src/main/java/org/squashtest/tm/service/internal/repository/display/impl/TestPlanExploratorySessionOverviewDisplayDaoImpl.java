/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.countDistinct;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;

import java.util.Set;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverviewReviewStatus;
import org.squashtest.tm.service.internal.display.dto.execution.ExploratorySessionOverviewView;
import org.squashtest.tm.service.internal.repository.display.TestPlanExploratorySessionOverviewDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class TestPlanExploratorySessionOverviewDisplayDaoImpl
        implements TestPlanExploratorySessionOverviewDisplayDao {

    private final DSLContext dslContext;

    public TestPlanExploratorySessionOverviewDisplayDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public ExploratorySessionOverviewView findById(long id) {
        return dslContext
                .select(
                        EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.as(RequestAliasesConstants.ID),
                        EXPLORATORY_SESSION_OVERVIEW.NAME,
                        EXPLORATORY_SESSION_OVERVIEW.CHARTER,
                        EXPLORATORY_SESSION_OVERVIEW.SESSION_DURATION,
                        EXPLORATORY_SESSION_OVERVIEW.REFERENCE,
                        EXPLORATORY_SESSION_OVERVIEW.DUE_DATE,
                        EXPLORATORY_SESSION_OVERVIEW.SESSION_STATUS,
                        EXPLORATORY_SESSION_OVERVIEW.COMMENTS,
                        EXPLORATORY_SESSION_OVERVIEW.ATTACHMENT_LIST_ID,
                        PROJECT.PROJECT_ID,
                        TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID,
                        TEST_PLAN_ITEM.CREATED_BY,
                        TEST_PLAN_ITEM.CREATED_ON,
                        TEST_PLAN_ITEM.LAST_MODIFIED_BY,
                        TEST_PLAN_ITEM.LAST_MODIFIED_ON,
                        TEST_PLAN_ITEM.EXECUTION_STATUS,
                        countDistinct(SESSION_NOTE.NOTE_ID).as(RequestAliasesConstants.NB_NOTES),
                        TEST_PLAN_ITEM.TCLN_ID)
                .from(EXPLORATORY_SESSION_OVERVIEW)
                .join(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID))
                .leftJoin(EXECUTION)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .leftJoin(SESSION_NOTE)
                .on(SESSION_NOTE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .join(TEST_PLAN)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(TEST_PLAN.TEST_PLAN_ID))
                .join(CAMPAIGN_LIBRARY)
                .on(CAMPAIGN_LIBRARY.CL_ID.eq(TEST_PLAN.CL_ID))
                .join(PROJECT)
                .on(PROJECT.CL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(id))
                .groupBy(
                        EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID,
                        TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID,
                        PROJECT.PROJECT_ID)
                .fetchOneInto(ExploratorySessionOverviewView.class);
    }

    @Override
    public String inferReviewStatus(long overviewId) {
        Set<Boolean> executionReviewStatuses =
                dslContext
                        .selectDistinct(EXPLORATORY_EXECUTION.REVIEWED)
                        .from(EXPLORATORY_EXECUTION)
                        .innerJoin(EXECUTION)
                        .on(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                        .on(
                                EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(
                                        EXPLORATORY_EXECUTION.EXECUTION_ID))
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                        .fetchSet(EXPLORATORY_EXECUTION.REVIEWED);

        if (executionReviewStatuses.size() == 1 && executionReviewStatuses.contains(true)) {
            return ExploratorySessionOverviewReviewStatus.DONE.name();
        } else if ((executionReviewStatuses.size() == 1 && executionReviewStatuses.contains(false))
                || executionReviewStatuses.isEmpty()) {
            return ExploratorySessionOverviewReviewStatus.TO_DO.name();
        } else {
            return ExploratorySessionOverviewReviewStatus.RUNNING.name();
        }
    }

    @Override
    public NameAndReference findNameAndReferenceByExecutionId(long executionId) {
        return dslContext
                .select(EXPLORATORY_SESSION_OVERVIEW.NAME, EXPLORATORY_SESSION_OVERVIEW.REFERENCE)
                .from(EXPLORATORY_SESSION_OVERVIEW)
                .join(EXECUTION)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID))
                .where(EXECUTION.EXECUTION_ID.eq(executionId))
                .fetchOneInto(NameAndReference.class);
    }
}
