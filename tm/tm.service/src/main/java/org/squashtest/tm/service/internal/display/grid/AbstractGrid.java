/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid;

import static org.jooq.impl.DSL.count;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.OrderField;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectLimitAfterOffsetStep;
import org.jooq.SelectSeekStepN;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.filters.collector.DefaultFilterCollector;

public abstract class AbstractGrid {

    protected final Map<String, GridColumn> aliasToFieldDictionary;

    protected final Map<String, String> fieldToAliasDictionary;

    protected AbstractGrid() {
        this.fieldToAliasDictionary =
                this.getColumns().stream()
                        .collect(
                                Collectors.toMap(
                                        column -> column.getAliasedField().getName(), GridColumn::findRuntimeAlias));

        this.aliasToFieldDictionary =
                this.getColumns().stream()
                        .collect(Collectors.toMap(GridColumn::findRuntimeAlias, column -> column));
    }

    protected abstract List<GridColumn> getColumns();

    protected abstract Table<?> getTable();

    // return always true by default
    protected Condition craftInvariantFilter() {
        return DSL.val(1).eq(1);
    }

    protected abstract Field<?> getIdentifier();

    protected abstract Field<?> getProjectIdentifier();

    protected SortField<?> getDefaultOrder() {
        return this.getIdentifier().asc();
    }

    protected List<Field<?>> getFields() {
        return this.getColumns().stream().<Field<?>>map(GridColumn::getAliasedField).toList();
    }

    public GridResponse getRows(GridRequest request, DSLContext dslContext) {
        Long count = countRows(dslContext, request);
        Integer actualPage = GridResponse.getPageNumberWithinBounds(request, count);

        List<DataRow> dataRows = getDataRows(request, actualPage, dslContext);

        GridResponse gridResponse = new GridResponse();
        gridResponse.setCount(count);
        gridResponse.setDataRows(dataRows);
        gridResponse.setPage(actualPage);
        return gridResponse;
    }

    protected List<DataRow> getDataRows(
            GridRequest request, Integer actualPage, DSLContext dslContext) {
        Select<?> baseRequest = getBaseRequest(request, actualPage, dslContext);
        return baseRequest.fetch().stream().collect(getCollector());
    }

    private Select<?> getBaseRequest(GridRequest request, Integer actualPage, DSLContext dslContext) {
        int offset = 0;

        if (actualPage != null && request.getSize() != null) {
            offset = actualPage * request.getSize();
        }

        SelectLimitAfterOffsetStep<?> query = createQuery(request, dslContext).offset(offset);

        if (request.getSize() != null) {
            return query.limit(request.getSize());
        } else {
            return query;
        }
    }

    protected SelectSeekStepN<?> createQuery(GridRequest request, DSLContext dslContext) {
        return dslContext
                .select(getFields())
                .from(getTable())
                .where(craftInvariantFilter())
                .and(this.craftVariableFilters(request))
                .orderBy(getOrderClause(request));
    }

    protected List<OrderField<?>> getOrderClause(GridRequest request) {
        if (request.getSort().isEmpty()) {
            return Collections.singletonList(getDefaultOrder());
        } else {
            return generateOrderClause(request);
        }
    }

    protected List<OrderField<?>> generateOrderClause(GridRequest request) {
        return request.getSort().stream()
                .<OrderField<?>>map(
                        gridSort -> {
                            GridColumn gridColumn = this.aliasToFieldDictionary.get(gridSort.getProperty());
                            return gridColumn.generateOrderClause(gridSort.getDirection());
                        })
                .toList();
    }

    protected Long countRows(DSLContext dslContext, GridRequest request) {
        return dslContext
                .select(count())
                .from(getTable())
                .where(craftInvariantFilter())
                .and(this.craftVariableFilters(request))
                .fetchOne(0, Long.class);
    }

    protected Collector<Record, ?, List<DataRow>> getCollector() {
        return new AbstractGrid.DefaultCollector(
                this.getIdentifier(), this.getProjectIdentifier(), this.fieldToAliasDictionary);
    }

    protected Condition craftVariableFilters(GridRequest request) {
        if (request.getFilterValues().isEmpty()) {
            return DSL.val(1).eq(1);
        } else {
            return request.getFilterValues().stream()
                    .collect(
                            new DefaultFilterCollector(
                                    this.aliasToFieldDictionary, request.isSearchOnMultiColumns()));
        }
    }

    static class DefaultCollector implements Collector<Record, List<DataRow>, List<DataRow>> {

        private final Field<?> idField;

        private final Field<?> projectIdField;

        private final Map<String, String> columnNameMap;

        public DefaultCollector(
                Field<?> idField, Field<?> projectIdField, Map<String, String> columnNameMap) {
            this.idField = idField;
            this.projectIdField = projectIdField;
            this.columnNameMap = columnNameMap;
        }

        @Override
        public Supplier<List<DataRow>> supplier() {
            return ArrayList::new;
        }

        @Override
        public BiConsumer<List<DataRow>, Record> accumulator() {
            return (list, record) -> {
                DataRow dataRow = new DataRow();
                Map<String, Object> rawData = record.intoMap();
                Map<String, Object> data = new HashMap<>();

                // Using 'Collectors.toMap' won't work for entries with null values
                for (Map.Entry<String, Object> entry : rawData.entrySet()) {
                    data.put(columnNameMap.get(entry.getKey()), entry.getValue());
                }

                dataRow.setId(record.get(this.idField, String.class));

                if (this.projectIdField != null) {
                    dataRow.setProjectId((record.get(this.projectIdField, Long.class)));
                }

                dataRow.setData(data);
                list.add(dataRow);
            };
        }

        @Override
        public BinaryOperator<List<DataRow>> combiner() {
            return (x, y) -> {
                throw new UnsupportedOperationException();
            };
        }

        @Override
        public Function<List<DataRow>, List<DataRow>> finisher() {
            return Function.identity();
        }

        @Override
        public Set<Characteristics> characteristics() {
            Set<Characteristics> characteristics = new HashSet<>();
            characteristics.add(Characteristics.IDENTITY_FINISH);
            return characteristics;
        }
    }
}
