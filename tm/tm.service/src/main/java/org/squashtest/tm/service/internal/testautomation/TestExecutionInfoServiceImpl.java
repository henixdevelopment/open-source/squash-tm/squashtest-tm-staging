/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;

import java.util.List;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.internal.dto.executioncomparator.TestExecutionInfo;
import org.squashtest.tm.service.internal.dto.executioncomparator.TestExecutionInfoMapper;
import org.squashtest.tm.service.testautomation.TestExecutionInfoService;

@Service
public class TestExecutionInfoServiceImpl implements TestExecutionInfoService {

    private final DSLContext dslContext;

    public TestExecutionInfoServiceImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<TestExecutionInfo> compareExecutionsBySuites(List<String> suiteIds) {
        List<TestExecutionInfo> result =
                dslContext
                        .select(
                                AUTOMATED_SUITE.SUITE_ID,
                                AUTOMATED_SUITE.CREATED_ON,
                                EXECUTION.TCLN_ID,
                                EXECUTION.NAME,
                                EXECUTION.DATASET_LABEL,
                                EXECUTION.EXECUTION_STATUS)
                        .from(AUTOMATED_SUITE)
                        .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                        .on(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.eq(AUTOMATED_SUITE.SUITE_ID))
                        .leftJoin(EXECUTION)
                        .on(EXECUTION.EXECUTION_ID.eq(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID))
                        .where(AUTOMATED_SUITE.SUITE_ID.in(suiteIds))
                        .fetch(new TestExecutionInfoMapper());

        return TestExecutionInfoMapper.aggregate(result);
    }
}
