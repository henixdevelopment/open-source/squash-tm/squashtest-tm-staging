/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.UnknownConnectorKind;

@Component("squashtest.testautomation.connector-registry")
public class TestAutomationConnectorRegistry {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestAutomationConnectorRegistry.class);

    @Autowired(required = false)
    private Collection<TestAutomationConnector> connectors = Collections.emptyList();

    /** Registered providers mapped by connector kind. */
    private Map<TestAutomationServerKind, TestAutomationConnector> availableConnectors =
            new ConcurrentHashMap<>(5);

    public Collection<TestAutomationServerKind> listRegisteredConnectors() {
        return availableConnectors.keySet();
    }

    public TestAutomationConnector getConnectorForKind(TestAutomationServerKind kind) {
        TestAutomationConnector connector = availableConnectors.get(kind);

        if (connector == null) {
            UnknownConnectorKind ex =
                    new UnknownConnectorKind("TestAutomationConnector : unknown kind '" + kind + "'");
            ex.addArg(kind.name());
            throw ex;
        }
        return connector;
    }

    /**
     * Create a TestAutomationConnector suitable for the given TestAutomationServer. Since only a
     * TestAutomationServer is given, the connector is restricted to features related to the Server
     * (i.e. supported AuthenticationProtocols).
     *
     * @param taServer The TestAutomationServer to interact with.
     * @return The suitable TestAutomationConnector for the given TestAutomationServer.
     */
    public TestAutomationConnector createConnector(TestAutomationServer taServer) {
        TestAutomationServerKind kind = taServer.getKind();
        LOGGER.debug("Creating connector for Test Autoamtion Server of kind '{}'.", kind);
        TestAutomationConnector testAutomationConnector = availableConnectors.get(kind);
        if (testAutomationConnector == null) {
            throw new IllegalArgumentException(
                    "No registered TestAutomationConnector is of type '" + kind + "'.");
        }
        return testAutomationConnector;
    }

    /**
     * Registers a new kind of connector connector.
     *
     * @param provider
     */
    @PostConstruct
    public void registerConnector() {
        for (TestAutomationConnector connector : connectors) {
            TestAutomationServerKind kind = connector.getConnectorKind();

            if (kind == null) {
                throw new IllegalArgumentException("TestAutomationConnector : kind is undefined");
            }

            LOGGER.info("Registering connector for test automation platforms of kind '{}'", kind);

            availableConnectors.put(kind, connector);
        }
    }
}
