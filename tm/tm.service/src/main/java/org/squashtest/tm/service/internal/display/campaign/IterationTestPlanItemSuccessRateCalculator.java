/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.sum;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_TEST_PLAN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXEC;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SUCCESS_RATE;

import java.util.Map;
import java.util.Set;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.execution.ExecutionStatus;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IterationTestPlanItemSuccessRateCalculator extends AbstractSuccessRateCalculator {

    public IterationTestPlanItemSuccessRateCalculator(DSLContext dslContext) {
        super(dslContext);
    }

    @Override
    protected Map<Long, Float> createRateByDataRowIdMap(Set<Long> testPlanItemIds) {
        Field<Long> executionId =
                dslContext
                        .select(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID)
                        .from(ITEM_TEST_PLAN_EXECUTION)
                        .where(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .orderBy(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.desc())
                        .limit(1)
                        .asField(EXECUTION_ID);

        CommonTableExpression<Record2<Long, Long>> executionCTE =
                name(LAST_EXEC)
                        .fields(ITEM_TEST_PLAN_ID, EXECUTION_ID)
                        .as(
                                dslContext
                                        .select(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, executionId)
                                        .from(ITERATION_TEST_PLAN_ITEM)
                                        .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(testPlanItemIds)));

        Field<Float> successRate =
                (sum(when(EXECUTION_STEP.EXECUTION_STATUS.eq(ExecutionStatus.SUCCESS.name()), 1))
                                .cast(Float.class)
                                .mul(100))
                        .div(when(count().eq(0), 1).otherwise(count()))
                        .as(SUCCESS_RATE);

        Field<Long> itemTestPlanId = executionCTE.field(ITEM_TEST_PLAN_ID, Long.class);
        return dslContext
                .with(executionCTE)
                .select(itemTestPlanId, successRate)
                .from(executionCTE)
                .join(EXECUTION_EXECUTION_STEPS)
                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(executionCTE.field(EXECUTION_ID, Long.class)))
                .join(EXECUTION_STEP)
                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(EXECUTION_STEP.EXECUTION_STEP_ID))
                .groupBy(itemTestPlanId)
                .orderBy(itemTestPlanId)
                .fetchMap(itemTestPlanId, successRate);
    }
}
