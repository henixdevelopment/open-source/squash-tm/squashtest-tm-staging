/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.tree;

import static org.squashtest.tm.core.foundation.lang.PathUtils.splitPath;
import static org.squashtest.tm.service.internal.batchimport.TargetStatus.NOT_EXISTS;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.service.internal.batchimport.Existence;
import org.squashtest.tm.service.internal.batchimport.TargetStatus;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.ImportRequirementFinder;

public class ImportedRequirementTree {
    private static final String TARGET_NOT_LOADED = "Imported requirement not loaded";

    private final Map<RequirementTarget, ImportedRequirementNode> nodes = new HashMap<>();

    private final Map<ImportedRequirementNode, Set<ImportedRequirementNode>> missingNodes =
            new HashMap<>();

    public void updateVersionStatus(RequirementVersionTarget version, TargetStatus status) {
        ImportedRequirementNode requirement = getNode(version.getRequirement());

        if (requirement == null) {
            throw new IllegalStateException(TARGET_NOT_LOADED);
        }

        requirement.updateVersionStatus(version.getVersion(), status);
    }

    public void addVersion(RequirementVersionTarget version, TargetStatus status) {
        ImportedRequirementNode requirement = getNode(version.getRequirement());

        requirement.addVersion(version.getVersion(), status);
    }

    public void updateStatus(RequirementTarget requirementTarget, TargetStatus status) {
        ImportedRequirementNode node = getNode(requirementTarget);

        if (node == null) {
            throw new IllegalStateException(TARGET_NOT_LOADED);
        }

        node.updateStatus(status);
    }

    public boolean targetAlreadyLoaded(RequirementTarget target) {
        ImportedRequirementNode req = getNode(target);
        return req != null;
    }

    public boolean targetAlreadyLoaded(RequirementVersionTarget target) {
        ImportedRequirementNode req = getNode(target.getRequirement());
        if (req == null) {
            return false; // If requirement isn't loaded, the requirement version can't be loaded
        }
        return req.versionAlreadyLoaded(target.getVersion());
    }

    public TargetStatus getStatus(RequirementTarget target) {
        ImportedRequirementNode requirement = getNode(target);
        if (requirement != null) {
            return requirement.getStatus();
        } else {
            return NOT_EXISTS;
        }
    }

    public TargetStatus getStatus(RequirementVersionTarget target) {
        ImportedRequirementNode requirement = getNode(target.getRequirement());
        return requirement.getVersionStatus(target.getVersion());
    }

    private void addNode(ImportedRequirementNode node) {
        nodes.put(node.getTarget(), node);
    }

    /**
     * Set a RequirementVersionTarget status to not exists
     *
     * @param target
     */
    public void setNotExists(RequirementVersionTarget target) {
        ImportedRequirementNode req = getNode(target.getRequirement());
        if (req != null) {
            req.setNotExists(target.getVersion());
        }
    }

    public Long getNodeId(RequirementTarget requirement) {
        ImportedRequirementNode reqNode = getNode(requirement);
        if (reqNode != null && reqNode.getStatus().getStatus() == Existence.EXISTS) {
            return reqNode.getStatus().getId();
        }

        return null;
    }

    public Long getNodeId(RequirementVersionTarget version) {
        return getNodeId(version.getRequirement());
    }

    public void bindMilestone(RequirementVersionTarget target, String milestone) {
        ImportedRequirementNode req = getNode(target.getRequirement());
        req.bindMilestoneToVersion(target.getVersion(), milestone);
    }

    public void bindMilestone(RequirementVersionTarget target, List<String> milestones) {
        for (String milestone : milestones) {
            bindMilestone(target, milestone);
        }
    }

    public boolean isMilestoneUsedByOneVersion(RequirementVersionTarget target, String milestone) {
        ImportedRequirementNode req = getNode(target.getRequirement());
        return req.isMilestoneUsedByOneVersion(milestone);
    }

    public boolean isMilestoneLocked(RequirementVersionTarget target) {
        ImportedRequirementNode req = getNode(target.getRequirement());
        return req.isVersionMilestoneLocked(target.getVersion());
    }

    public void milestoneLock(RequirementVersionTarget target) {
        ImportedRequirementNode req = getNode(target.getRequirement());
        req.setVersionMilestoneLocked(target.getVersion());
    }

    public boolean isRequirementFolder(RequirementVersionTarget target) {
        return isRequirementFolder(target.getRequirement());
    }

    public boolean isRequirementFolder(RequirementTarget target) {
        ImportedRequirementNode req = getNode(target);
        if (req != null) {
            return req.isRequirementFolder() && req.getStatus().getStatus() != Existence.NOT_EXISTS;
        }
        return false;
    }

    public ImportedRequirementNode getNode(RequirementTarget target) {
        return nodes.get(target);
    }

    public boolean isHighLevelRequirement(RequirementTarget target) {
        ImportedRequirementNode node = getNode(target);

        if (node == null) {
            throw new IllegalStateException(TARGET_NOT_LOADED);
        }

        return node.isHighLevelRequirement();
    }

    public boolean isRequirementChild(RequirementTarget target) {
        ImportedRequirementNode node = getNode(target);

        if (node == null) {
            throw new IllegalStateException(TARGET_NOT_LOADED);
        }

        return node.isRequirementChild();
    }

    public void addNodes(
            Set<RequirementTarget> targets,
            ImportRequirementFinder finder,
            String project,
            Long requirementLibraryId) {
        ImportedRequirementNode root =
                ImportedRequirementNode.createRequirementLibraryNode(project, requirementLibraryId);
        addNode(root);

        for (RequirementTarget target : targets) {
            ImportedRequirementNode targetNode = getRequirementNode(target, finder);

            if (targetNode.exist()) {
                continue;
            }

            findFirstExistingParent(finder, root, targetNode);
        }
    }

    private void findFirstExistingParent(
            ImportRequirementFinder finder,
            ImportedRequirementNode root,
            ImportedRequirementNode targetNode) {
        String[] parts = splitPath(targetNode.getTarget().getPath());
        List<String> paths = PathUtils.scanPath(targetNode.getTarget().getPath());

        ImportedRequirementNode current = targetNode;

        for (int i = parts.length - 2; i >= 0; i--) {
            if (i == 0) {
                root.addContent(current);
                addMissingNode(root, current);
                return;
            }

            ImportedRequirementNode parent = findParent(current, paths.get(i), finder);
            parent.addContent(current);

            if (parent.exist()) {
                addMissingNode(parent, current);
                return;
            }

            current = parent;
        }
    }

    private ImportedRequirementNode getRequirementNode(
            RequirementTarget target, ImportRequirementFinder finder) {
        ImportedRequirementNode node = getNode(target);

        if (node != null) {
            return node;
        }

        node = finder.searchRequirementNode(target);

        if (node == null) {
            node =
                    ImportedRequirementNode.createRequirementNode(
                            target, new TargetStatus(Existence.NOT_EXISTS));
        }

        addNode(node);

        return node;
    }

    private void addMissingNode(ImportedRequirementNode parent, ImportedRequirementNode child) {
        missingNodes.computeIfAbsent(parent, k -> new HashSet<>()).add(child);
    }

    private ImportedRequirementNode findParent(
            ImportedRequirementNode child, String parentPath, ImportRequirementFinder finder) {
        // High level can only have folder as parent
        if (child.isHighLevelRequirement()) {
            return getFolderParent(parentPath, finder);
        }

        // Search if parent path is an imported requirement
        RequirementTarget target = finder.searchParentTarget(parentPath, child);

        if (target != null) {
            ImportedRequirementNode node = getRequirementNode(target, finder);

            child.fix();

            return node;
        }

        // Parent is folder
        if (child.isRequirementFolder()) {
            return getFolderParent(parentPath, finder);
        }

        // Search requirements
        ImportedRequirementNode requirementNode = findRequirementParent(child, parentPath, finder);

        if (requirementNode != null) {
            return requirementNode;
        }

        // Default
        return getFolderParent(parentPath, finder);
    }

    private static ImportedRequirementNode findRequirementParent(
            ImportedRequirementNode child, String parentPath, ImportRequirementFinder finder) {
        ImportedRequirementNode parent;

        // if the leaf is synchronized, we need to find synchronized requirement parent
        if (child.isSynchronized()) {
            parent =
                    finder.searchSynchronizedRequirementParent(
                            parentPath, child.getRemoteSynchronisationId());
        } else {
            parent = finder.searchBasicRequirementParent(parentPath);
        }

        return parent;
    }

    private ImportedRequirementNode getFolderParent(
            String parentPath, ImportRequirementFinder finder) {
        RequirementTarget target = new RequirementTarget(parentPath);

        ImportedRequirementNode folder = getNode(target);

        if (folder != null) {
            return folder;
        }

        folder = finder.searchFolderParent(target);

        if (folder == null) {
            folder =
                    ImportedRequirementNode.createRequirementFolderNode(
                            target, new TargetStatus(Existence.NOT_EXISTS));
        }

        addNode(folder);

        return folder;
    }

    // Add nodes to the tree without searching for parent nodes
    public void addNodes(Set<RequirementTarget> targets, ImportRequirementFinder finder) {

        for (RequirementTarget target : targets) {
            ImportedRequirementNode node = finder.searchRequirementNode(target);

            if (node == null) {
                node =
                        ImportedRequirementNode.createRequirementNode(
                                target, new TargetStatus(Existence.NOT_EXISTS));
            }

            addNode(node);
        }
    }

    public Map<ImportedRequirementNode, Set<ImportedRequirementNode>> collectMissingNode() {
        return missingNodes;
    }

    public void fixVersion(RequirementVersionTarget target) {
        ImportedRequirementNode node = getNode(target.getRequirement());

        int versionNumber = node.fixVersion();

        Integer deprecated = target.getVersion();

        if (deprecated != null && (deprecated <= 0 || deprecated > versionNumber)) {
            node.deleteVersion(deprecated);
        }

        target.setVersion(versionNumber);

        addVersion(target, new TargetStatus(Existence.NOT_EXISTS));
    }

    public boolean existPriorVersion(RequirementVersionTarget target) {
        ImportedRequirementNode node = getNode(target.getRequirement());

        return node.existPriorVersion(target.getVersion());
    }

    public Long getVersionId(RequirementTarget requirement, int version) {
        ImportedRequirementNode reqNode = getNode(requirement);
        if (reqNode != null && reqNode.getStatus().getStatus() == Existence.EXISTS) {
            return reqNode.getVersionId(version);
        }

        return null;
    }
}
