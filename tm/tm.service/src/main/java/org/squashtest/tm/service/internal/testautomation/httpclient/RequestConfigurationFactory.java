/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.httpclient;

import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import org.apache.http.client.config.RequestConfig;

/**
 * This component computes request configuration based on the endpointURI.
 *
 * @author edegenetais
 */
public class RequestConfigurationFactory {
    public RequestConfig getRequestConfig(URI endpoint, int timeoutSeconds) {
        try {
            if (InetAddress.getByName(endpoint.getHost()).isLoopbackAddress()) {
                return defaultConfigBuilder(timeoutSeconds)
                        .setLocalAddress(InetAddress.getByAddress(new byte[] {127, 0, 0, 1}))
                        .build();
            } else {
                return defaultConfigBuilder(timeoutSeconds).build();
            }
        } catch (UnknownHostException ex) {
            throw new ClientRuntimeException("Request configuration failure", ex, endpoint.toString());
        }
    }

    private RequestConfig.Builder defaultConfigBuilder(int timeoutSeconds) {
        final int timeoutMs = timeoutSeconds * 1000;
        return RequestConfig.copy(RequestConfig.DEFAULT)
                .setConnectionRequestTimeout(timeoutMs)
                .setConnectTimeout(timeoutMs)
                .setSocketTimeout(timeoutMs);
    }
}
