/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;

import java.util.List;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao;

@Repository
public class AutomatedSuiteDisplayDaoImpl implements AutomatedSuiteDisplayDao {

    private final DSLContext dslContext;

    public AutomatedSuiteDisplayDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<String> findReportUrlsByAutomatedSuiteId(String automatedSuiteId) {
        return dslContext
                .selectDistinct(AUTOMATED_EXECUTION_EXTENDER.RESULT_URL)
                .from(AUTOMATED_EXECUTION_EXTENDER)
                .innerJoin(AUTOMATED_SUITE)
                .on(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.eq(AUTOMATED_SUITE.SUITE_ID))
                .where(AUTOMATED_EXECUTION_EXTENDER.RESULT_URL.isNotNull())
                .and(AUTOMATED_SUITE.SUITE_ID.eq(automatedSuiteId))
                .fetch(AUTOMATED_EXECUTION_EXTENDER.RESULT_URL);
    }

    @Override
    public Long findAttachmentListIdByAutomatedSuite(String automatedSuiteId) {
        return dslContext
                .select(AUTOMATED_SUITE.ATTACHMENT_LIST_ID)
                .from(AUTOMATED_SUITE)
                .where(AUTOMATED_SUITE.SUITE_ID.eq(automatedSuiteId))
                .fetchOneInto(Long.class);
    }

    @Override
    public long countAutomatedSuiteByIterationId(Long iterationId) {
        return dslContext
                .select(AUTOMATED_SUITE.SUITE_ID)
                .from(AUTOMATED_SUITE)
                .where(AUTOMATED_SUITE.ITERATION_ID.eq(iterationId))
                .union(
                        dslContext
                                .select(AUTOMATED_SUITE.SUITE_ID)
                                .from(AUTOMATED_SUITE)
                                .join(TEST_SUITE)
                                .on(AUTOMATED_SUITE.TEST_SUITE_ID.eq(TEST_SUITE.ID))
                                .join(ITERATION_TEST_SUITE)
                                .on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                                .where(ITERATION_TEST_SUITE.ITERATION_ID.eq(iterationId)))
                .union(
                        dslContext
                                .select(AUTOMATED_SUITE.SUITE_ID)
                                .from(AUTOMATED_SUITE)
                                .join(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .join(EXECUTION)
                                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                .join(ITEM_TEST_PLAN_EXECUTION)
                                .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .join(ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .where(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(iterationId)))
                .fetch()
                .stream()
                .count();
    }

    @Override
    public long countAutomatedSuiteByTestSuiteId(Long testSuiteId) {
        return dslContext
                .select(AUTOMATED_SUITE.SUITE_ID)
                .from(AUTOMATED_SUITE)
                .innerJoin(TEST_SUITE)
                .on(AUTOMATED_SUITE.TEST_SUITE_ID.eq(TEST_SUITE.ID))
                .where(TEST_SUITE.ID.eq(testSuiteId))
                .union(
                        dslContext
                                .select(AUTOMATED_SUITE.SUITE_ID)
                                .from(AUTOMATED_SUITE)
                                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .innerJoin(EXECUTION)
                                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                                .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                                .on(
                                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                .innerJoin(ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(TEST_SUITE_TEST_PLAN_ITEM)
                                .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                                .innerJoin(TEST_SUITE)
                                .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
                                .where(TEST_SUITE.ID.eq(testSuiteId)))
                .fetch()
                .stream()
                .count();
    }
}
