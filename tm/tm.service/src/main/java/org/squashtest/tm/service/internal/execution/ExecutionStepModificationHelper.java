/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.CustomFieldVisitor;
import org.squashtest.tm.domain.customfield.MultiSelectField;
import org.squashtest.tm.domain.customfield.NumericField;
import org.squashtest.tm.domain.customfield.RichTextField;
import org.squashtest.tm.domain.customfield.SingleSelectField;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.denormalizedfield.PrivateDenormalizedFieldValueService;
import org.squashtest.tm.service.internal.repository.CustomFieldValueDao;
import org.squashtest.tm.service.internal.repository.ExecutionStepDao;

@Component
public class ExecutionStepModificationHelper {

    @Inject private DenormalizedFieldValueManager denormalizedFieldValueManager;

    @Inject private CustomFieldValueDao customFieldValueDao;

    @Inject private ExecutionStepDao executionStepDao;

    @Inject private PrivateDenormalizedFieldValueService privateDenormalizedFieldValueService;

    @Inject private ExecutionProcessingService executionProcessingService;

    public long doUpdateStep(List<ExecutionStep> toBeUpdated, Execution execution) {

        long firstModifiedIndex = -1;

        for (ExecutionStep execStep : toBeUpdated) {
            ActionTestStep step = (ActionTestStep) execStep.getReferencedTestStep();

            if (step == null) {
                execution.removeStep(execStep.getId());
                continue;
            }

            firstModifiedIndex =
                    firstModifiedIndex < 0 ? execution.getStepIndex(execStep.getId()) : firstModifiedIndex;

            final Dataset dataset = findStepDataset(execution, step);

            if (dataset != null) {
                execStep.fillParameterMap(dataset);
            }

            step.accept(execStep);
            executionProcessingService.changeExecutionStepStatus(execStep.getId(), ExecutionStatus.READY);

            privateDenormalizedFieldValueService.deleteAllDenormalizedFieldValues(execStep);
            privateDenormalizedFieldValueService.createAllDenormalizedFieldValues(step, execStep);

            for (Attachment actionStepAttach : step.getAllAttachments()) {
                if (!doesListContainAttachment(execStep.getAttachmentList(), actionStepAttach)) {
                    Attachment clone = actionStepAttach.shallowCopy();
                    execStep.getAttachmentList().addAttachment(clone);
                }
            }

            executionStepDao.persist(execStep);
        }

        return firstModifiedIndex;
    }

    private Dataset findStepDataset(Execution execution, ActionTestStep step) {
        if (isExecutionWithParameters(step)) {
            // [TM-544] Look for dataset inherited from called test case
            Long aStepTestCaseId = step.getTestCase().getId();
            Optional<CallTestStep> optionalCallTestStep =
                    execution.getReferencedTestCase().getCallSteps().stream()
                            .filter(
                                    callTestStep -> callTestStep.getCalledTestCase().getId().equals(aStepTestCaseId))
                            .findFirst();

            if (optionalCallTestStep.isPresent()
                    && !optionalCallTestStep.get().isDelegateParameterValues()) {
                return optionalCallTestStep.get().getCalledDataset();
            } else {
                return execution.getReferencedDataset();
            }
        }

        return null;
    }

    public List<ExecutionStep> findStepsToUpdate(Execution execution) {
        List<ExecutionStep> execSteps = execution.getSteps();
        List<ExecutionStep> toBeUpdated = new ArrayList<>();

        for (ExecutionStep eStep : execSteps) {
            ActionTestStep aStep = (ActionTestStep) eStep.getReferencedTestStep();
            // 6797 : Status of executionStep was lost if step was containing parameters.
            // eStep ==> parameters' value but aStep ==> ${param}, then eStep was adding to the
            // 'toBeUpdated' list.
            if (aStep != null) {
                if (isExecutionWithParameters(aStep)) {
                    // [TM-543][TM-544] Correction for issue 6797 didn't dealt with callTestStep with dataset
                    // coming from the call test case (and not the calling one)
                    // Correction below makes them not to be added to the 'toBeUpdated' list, except if the
                    // same test case is called more than one with different dataset.
                    // This situation is considered as normally never happening.
                    Long aStepTestCaseId = aStep.getTestCase().getId();
                    Optional<CallTestStep> optionalCallTestStep =
                            execution.getReferencedTestCase().getCallSteps().stream()
                                    .filter(
                                            callTestStep ->
                                                    callTestStep.getCalledTestCase().getId().equals(aStepTestCaseId))
                                    .findFirst();
                    handleStepParam(execution, toBeUpdated, eStep, aStep, optionalCallTestStep);
                } else {
                    testingStep(aStep, eStep, toBeUpdated);
                }
            }
        }

        return toBeUpdated;
    }

    private void handleStepParam(
            Execution execution,
            List<ExecutionStep> toBeUpdated,
            ExecutionStep eStep,
            ActionTestStep aStep,
            Optional<CallTestStep> optionalCallTestStep) {
        if (optionalCallTestStep.isPresent()
                && !optionalCallTestStep.get().isDelegateParameterValues()) {
            Dataset dataset = optionalCallTestStep.get().getCalledDataset();
            changeStepParamsByValue(eStep, aStep, dataset, toBeUpdated);
        } else {
            if (isExecutionWithDataset(execution)) {
                Dataset dataset = execution.getTestPlan().getReferencedDataset();
                changeStepParamsByValue(eStep, aStep, dataset, toBeUpdated);
            } else {
                testingStep(aStep, eStep, toBeUpdated);
            }
        }
    }

    private void testingStep(
            ActionTestStep aStep, ExecutionStep eStep, List<ExecutionStep> toBeUpdated) {
        if (!isStepEqual(eStep, aStep)) {
            toBeUpdated.add(eStep);
        }
    }

    private boolean isExecutionWithParameters(ActionTestStep aStep) {
        return aStep.findUsedParametersNames() != null;
    }

    private boolean isExecutionWithDataset(Execution execution) {
        if (execution.getTestPlan() != null) {
            return execution.getTestPlan().getReferencedDataset() != null;
        }
        return false;
    }

    private void changeStepParamsByValue(
            ExecutionStep eStep, ActionTestStep aStep, Dataset dataset, List<ExecutionStep> toBeUpdated) {
        String action = aStep.getAction();
        String expectedResult = aStep.getExpectedResult();
        Set<String> params = aStep.findUsedParametersNames();
        for (String param : params) {
            for (DatasetParamValue dpv : dataset.getParameterValues()) {
                changeStepParamByValue(aStep, param, dpv);
            }
        }
        if (!isStepEqual(eStep, aStep)) {
            toBeUpdated.add(eStep);
        }
        reinitiateActionTestStep(aStep, action, expectedResult);
    }

    private void changeStepParamByValue(ActionTestStep aStep, String param, DatasetParamValue dpv) {
        String paramValue = "";
        if (dpv.getParameter().getName().equals(param)) {
            paramValue = dpv.getParamValue();
            if (aStep.getExpectedResult().contains(param)) {
                aStep.setExpectedResult(aStep.getExpectedResult().replace("${" + param + "}", paramValue));
            }
            aStep.setAction(aStep.getAction().replace("${" + param + "}", paramValue));
        }
    }

    private void reinitiateActionTestStep(
            ActionTestStep aStep, String action, String expectedResult) {
        aStep.setAction(action);
        aStep.setExpectedResult(expectedResult);
    }

    private boolean isStepEqual(ExecutionStep eStep, ActionTestStep aStep) {
        return actionStepExist(aStep)
                && sameAction(eStep, aStep)
                && sameResult(eStep, aStep)
                && sameAttach(eStep, aStep)
                && sameCufs(eStep, aStep);
    }

    private boolean sameCufs(ExecutionStep eStep, ActionTestStep aStep) {

        List<DenormalizedFieldValue> denormalizedFieldValues =
                denormalizedFieldValueManager.findAllForEntity(eStep);

        List<CustomFieldValue> originalValues =
                customFieldValueDao.findAllCustomValues(aStep.getId(), BindableEntity.TEST_STEP);

        // different number of CUF
        if (originalValues.size() != denormalizedFieldValues.size()) {
            return false;
        }

        for (DenormalizedFieldValue denormVal : denormalizedFieldValues) {

            CustomFieldValue origVal = denormVal.getCustomFieldValue();
            if (origVal == null || hasChanged(denormVal, origVal)) {
                return false;
            }
        }

        return true;
    }

    private boolean hasChanged(
            final DenormalizedFieldValue denormVal, final CustomFieldValue origVal) {

        final boolean[] hasChanged = {false};

        origVal
                .getCustomField()
                .accept(
                        new CustomFieldVisitor() {

                            private void testValChange() {
                                if (valueHasChanged(denormVal, origVal)) {
                                    hasChanged[0] = true;
                                }
                            }

                            @Override
                            public void visit(MultiSelectField multiSelect) {
                                testValChange();
                            }

                            @Override
                            public void visit(RichTextField richField) {
                                testValChange();
                            }

                            @Override
                            public void visit(NumericField numericField) {
                                testValChange();
                            }

                            @Override
                            public void visit(CustomField standardValue) {
                                testValChange();
                            }

                            @Override
                            public void visit(SingleSelectField selectField) {
                                testValChange();
                            }
                        });

        return hasChanged[0];
    }

    private boolean valueHasChanged(DenormalizedFieldValue denormVal, CustomFieldValue origVal) {
        return !denormVal.getValue().equals(origVal.getValue());
    }

    private boolean actionStepExist(ActionTestStep aStep) {
        return aStep != null;
    }

    private boolean sameAction(ExecutionStep eStep, ActionTestStep aStep) {
        return eStep.getAction().equals(aStep.getAction());
    }

    private boolean sameResult(ExecutionStep eStep, ActionTestStep aStep) {
        return eStep.getExpectedResult().equals(aStep.getExpectedResult());
    }

    // Rather than testing a 1 to 1 match, we only check if there were newly added
    // attachments (attachments that exist on the action step but not on the exec step)
    private boolean sameAttach(ExecutionStep eStep, ActionTestStep aStep) {
        AttachmentList eStepAttach = eStep.getAttachmentList();
        Set<Attachment> aStepAttach = aStep.getAllAttachments();

        for (final Attachment aAttach : aStepAttach) {
            boolean exist = doesListContainAttachment(eStepAttach, aAttach);

            if (!exist) {
                return false;
            }
        }

        return true;
    }

    boolean doesListContainAttachment(AttachmentList attachmentList, Attachment attachment) {
        return CollectionUtils.exists(
                attachmentList.getAllAttachments(),
                item -> {
                    Attachment toCompare = (Attachment) item;
                    boolean sameName = toCompare.getName().equals(attachment.getName());
                    boolean sameSize = toCompare.getSize().equals(attachment.getSize());
                    return sameName && sameSize;
                });
    }
}
