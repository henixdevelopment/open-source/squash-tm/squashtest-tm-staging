/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE_BINDING;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BINDING_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EV_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INPUT_TYPE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NAME;

import java.util.Arrays;
import java.util.List;
import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

public class EnvironmentVariableGrid extends AbstractGrid {

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(EV_ID)),
                new GridColumn(DSL.field(NAME)),
                new GridColumn(DSL.field(INPUT_TYPE)),
                new GridColumn(DSL.isnull(DSL.field(BINDING_COUNT), 0).as(BINDING_COUNT)));
    }

    @Override
    protected Table<?> getTable() {
        return DSL.select(
                        ENVIRONMENT_VARIABLE.EV_ID.as(EV_ID),
                        ENVIRONMENT_VARIABLE.NAME.as(NAME),
                        ENVIRONMENT_VARIABLE.INPUT_TYPE.as(INPUT_TYPE),
                        DSL.count(ENVIRONMENT_VARIABLE_BINDING.EVB_ID).as(BINDING_COUNT))
                .from(ENVIRONMENT_VARIABLE)
                .leftJoin(ENVIRONMENT_VARIABLE_BINDING)
                .on(ENVIRONMENT_VARIABLE_BINDING.EV_ID.eq(ENVIRONMENT_VARIABLE.EV_ID))
                .groupBy(ENVIRONMENT_VARIABLE.EV_ID)
                .asTable();
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(EV_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return DSL.upper(DSL.field(NAME, String.class)).asc();
    }
}
