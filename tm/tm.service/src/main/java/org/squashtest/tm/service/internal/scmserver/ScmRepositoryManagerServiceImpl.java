/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.scmserver;

import static java.time.ZonedDateTime.now;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import com.google.common.net.PercentEscaper;
import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.core.scm.api.exception.ScmException;
import org.squashtest.tm.core.scm.api.exception.ScmNoCredentialsException;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.exception.WrongStringSizeException;
import org.squashtest.tm.exception.scm.NameAndBranchAlreadyInUseException;
import org.squashtest.tm.service.internal.dto.ScmRepositoryDto;
import org.squashtest.tm.service.internal.repository.ScmRepositoryDao;
import org.squashtest.tm.service.internal.repository.ScmServerDao;
import org.squashtest.tm.service.scmserver.ScmRepositoryFilesystemService;
import org.squashtest.tm.service.scmserver.ScmRepositoryManagerService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.spi.ScmConnector;

@Service
@Transactional
public class ScmRepositoryManagerServiceImpl
        implements ScmRepositoryManagerService, ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ScmRepositoryManagerServiceImpl.class);

    @Inject private ScmConnectorRegistry scmRegistry;
    @Inject private ScmServerDao scmServerDao;
    @Inject private ScmRepositoryDao scmRepositoryDao;
    @Inject private CredentialsProvider credentialsProvider;
    @Inject private ScmRepositoryFilesystemService scmRepositoryFileSystemService;
    @Inject private MessageSource i18nHelper;
    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject
    @Named("squashtest.tm.service.ThreadPoolTaskScheduler")
    private TaskScheduler taskScheduler;

    @Value("${squash.path.local-git-repositories-folder:../git-repositories}")
    private String localGitRepositoriesFolder;

    private String getMessage(String i18nKey) {
        Locale locale = LocaleContextHolder.getLocale();
        return i18nHelper.getMessage(i18nKey, null, locale);
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        taskScheduler.schedule(this::doCloneMissingLocalRepositories, Instant.now());
    }

    @Override
    public List<ScmRepository> findByScmServerOrderByPath(Long scmServerId) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        return scmRepositoryDao.findByScmServerIdOrderByRepositoryPathAsc(scmServerId);
    }

    @Override
    public List<ScmRepository> findClonedByScmServerOrderByName(long scmServerId) {
        return scmRepositoryDao.findClonedByScmServerOrderByName(scmServerId);
    }

    @Override
    public Page<ScmRepository> findPagedScmRepositoriesByScmServer(
            Long scmServerId, Pageable pageable) {
        return scmRepositoryDao.findByScmServerId(scmServerId, pageable);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public ScmRepository createNewScmRepository(
            long scmServerId,
            @NotNull String name,
            @NotNull String branch,
            String workingFolderPath,
            boolean cloneRepository)
            throws IOException {
        name = name.trim();
        branch = branch.trim();

        if (workingFolderPath != null) {
            workingFolderPath = workingFolderPath.trim();
        }

        checkNameAndBranchAlreadyInuse(scmServerId, name, branch);
        ScmServer scmServer = scmServerDao.getReferenceById(scmServerId);

        ScmRepository newScmRepository = new ScmRepository();
        newScmRepository.setName(name);
        newScmRepository.setWorkingBranch(branch);
        newScmRepository.setWorkingFolderPath(workingFolderPath);
        newScmRepository.setScmServer(scmServer);

        if (cloneRepository) {
            newScmRepository.setRepositoryPath(buildRepositoryPath(name, scmServer.getUrl()));
        } else {
            newScmRepository.setRepositoryPath("");
        }

        ScmRepository createdScmRepository = scmRepositoryDao.save(newScmRepository);

        if (cloneRepository) {
            initializeAndPrepareRepository(createdScmRepository);
        }
        return createdScmRepository;
    }

    private String buildRepositoryPath(String name, String serverUrl) {
        // Percent-encoded URL portion, limited to 200 characters
        final PercentEscaper percentEscaper = new PercentEscaper("", true);
        final String urlWithoutProtocol = serverUrl.replaceFirst(".*://", "");
        final String urlEncodedServerUrl = percentEscaper.escape(urlWithoutProtocol);
        final int urlPortionMaxLength = 200;
        final int urlPortionLength = Math.min(urlEncodedServerUrl.length(), urlPortionMaxLength);
        final String urlPortion = urlEncodedServerUrl.substring(0, urlPortionLength);

        // Percent-encoded name portion, limited to 50 characters
        final String urlEncodedName = percentEscaper.escape(name);
        final int namePortionMaxLength = 50;
        final int namePortionLength = Math.min(urlEncodedName.length(), namePortionMaxLength);
        final String namePortion = urlEncodedName.substring(0, namePortionLength);

        // ISO 8601 basic format timestamp
        final String timestamp = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss").format(now());

        return String.format(
                "%s/%s/%s_%s", localGitRepositoriesFolder, urlPortion, namePortion, timestamp);
    }

    /**
     * Given a ScmRepository, check that credentials exist for its ScmServer and are valid. Then try
     * to initialize the repository on file system and prepare it.
     *
     * @param scmRepository The ScmRepository to synchronize.
     */
    private void initializeAndPrepareRepository(ScmRepository scmRepository) throws IOException {
        Credentials credentials = checkAndReturnCredentials(scmRepository);

        ScmConnector connector = scmRegistry.createConnector(scmRepository);

        checkIfProtocolIsSupported(credentials, connector);

        connector.createRepository(credentials);

        connector.prepareRepository(credentials);
        // Create working folder if absent
        scmRepositoryFileSystemService.createWorkingFolderIfAbsent(scmRepository);
    }

    /**
     * Given a ScmRepository, check if the Credentials of its ScmServer are well defined and returns
     * it.
     *
     * @param scmRepository The ScmRepository to check
     * @return The Credentials if they are well defined
     * @throws ScmNoCredentialsException If no Credentials were defined for the ScmServer
     */
    private Credentials checkAndReturnCredentials(ScmRepository scmRepository) {
        ScmServer server = scmRepository.getScmServer();
        Optional<Credentials> maybeCredentials = credentialsProvider.getAppLevelCredentials(server);
        Supplier<ScmNoCredentialsException> throwIfNull =
                () -> {
                    throw new ScmNoCredentialsException(
                            String.format(
                                    getMessage("message.scmRepository.noCredentials"), scmRepository.getName()));
                };
        return maybeCredentials.orElseThrow(throwIfNull);
    }

    /**
     * Check if the given AuthenticationProtocol is supported by the ScmConnector.
     *
     * @param credentials The Credentials whose AuthentiacationProtocol is to check
     * @param connector The ScmConnector
     * @throws UnsupportedAuthenticationModeException If the protocol is not supported by the
     *     Connector
     */
    private void checkIfProtocolIsSupported(Credentials credentials, ScmConnector connector) {
        AuthenticationProtocol protocol = credentials.getImplementedProtocol();
        if (!connector.supports(protocol)) {
            throw new UnsupportedAuthenticationModeException(protocol.toString());
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public String updateBranch(long scmRepositoryId, String newBranch) throws IOException {
        checkFieldIsNotBlank(newBranch, "branch");
        checkFieldMaxSize(newBranch, "branch", 255);
        ScmRepository scmRepository = scmRepositoryDao.getReferenceById(scmRepositoryId);
        String formerBranch = scmRepository.getWorkingBranch();
        if (formerBranch.equals(newBranch)) {
            LOGGER.debug(
                    "Did not update the ScmRepository branch because the submitted branch is identical to the former one");
            return formerBranch;
        }
        checkNameAndBranchAlreadyInuse(
                scmRepository.getScmServer().getId(), scmRepository.getName(), newBranch);
        scmRepository.setWorkingBranch(newBranch);
        scmRepositoryDao.save(scmRepository);

        // prepare the local repository to switch branch
        prepareRepository(scmRepository);

        return newBranch;
    }

    /**
     * Given a ScmRepository, check that credentials exist for its ScmServer and are valid. Then try
     * prepare this repository.
     *
     * @param scmRepository The ScmRepository to synchronize.
     */
    private void prepareRepository(ScmRepository scmRepository) throws IOException {
        Credentials credentials = checkAndReturnCredentials(scmRepository);

        ScmConnector connector = scmRegistry.createConnector(scmRepository);

        checkIfProtocolIsSupported(credentials, connector);

        connector.prepareRepository(credentials);
        // Create working folder if absent
        scmRepositoryFileSystemService.createWorkingFolderIfAbsent(scmRepository);
    }

    private void checkNameAndBranchAlreadyInuse(
            long scmServerId, String repositoryName, String newWorkingBranch) {
        if (scmRepositoryDao.isRepositoryNameAndBranchAlreadyInUse(
                scmServerId, repositoryName, newWorkingBranch)) {
            throw new NameAndBranchAlreadyInUseException(
                    "The name of the ScmRepository "
                            + repositoryName
                            + " and branch "
                            + newWorkingBranch
                            + " are already in use for scm server id "
                            + scmServerId);
        }
    }

    private void checkFieldIsNotBlank(String fieldValue, String fieldName) {
        if (StringUtils.isEmpty(fieldValue.trim())) {
            throw new RequiredFieldException(fieldName);
        }
    }

    private void checkFieldMaxSize(String fieldValue, String fieldName, int maxSize) {
        if (fieldValue.length() > maxSize) {
            throw new WrongStringSizeException(fieldName, 0, maxSize);
        }
    }

    @Override
    public boolean isOneRepositoryBoundToProjectOrTestCase(Collection<Long> scmRepositoryIds) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        return scmRepositoryDao.isOneRepositoryBoundToProjectOrTestCase(scmRepositoryIds);
    }

    @Override
    public List<ScmRepositoryDto> getAllDeclaredScmRepositories(Locale locale) {
        List<ScmRepositoryDto> repositories =
                new ArrayList<>(scmRepositoryDao.getAllDeclaredScmRepositories());
        repositories.add(
                0, new ScmRepositoryDto(0L, i18nHelper.getMessage("label.None", null, locale)));
        return repositories;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteScmRepositories(Collection<Long> scmRepositoriesIds) {
        scmRepositoryDao.releaseScmRepositoriesFromTestCases(scmRepositoriesIds);
        scmRepositoryDao.releaseScmRepositoriesFromProjects(scmRepositoriesIds);
        deleteLocalClones(scmRepositoriesIds);
        scmRepositoryDao.deleteByIds(scmRepositoriesIds);
    }

    private void deleteLocalClones(Collection<Long> scmRepositoriesIds) {
        final List<ScmRepository> repositories = scmRepositoryDao.findAllById(scmRepositoriesIds);
        scmRepositoryFileSystemService.deleteLocalRepositories(repositories);
    }

    private void doCloneMissingLocalRepositories() {
        final List<ScmRepository> allRepositories = scmRepositoryDao.findAll();

        allRepositories.forEach(
                repository -> {
                    String repositoryPath = repository.getRepositoryPath();
                    if (repositoryPath != null
                            && !repositoryPath.isEmpty()
                            && !scmRepositoryFileSystemService.checkLocalRepositoryExists(repositoryPath)) {
                        try {
                            LOGGER.warn(
                                    "The local clone of the repository "
                                            + repository.getName()
                                            + " from the SCM server "
                                            + repository.getScmServer().getFriendlyName()
                                            + " is missing at the specified path "
                                            + repository.getBaseRepositoryFolder()
                                            + ". About to create the local clone.");
                            initializeAndPrepareRepository(repository);
                        } catch (IOException e) {
                            throw new ScmException(
                                    "Error while cloning the repository " + repository.getName(), e);
                        }
                    }
                });
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public void recloneScmRepository(long scmRepositoryId) {
        ScmRepository scmRepository = scmRepositoryDao.getReferenceById(scmRepositoryId);
        scmRepositoryFileSystemService.deleteLocalRepositories(List.of(scmRepository));

        taskScheduler.schedule(() -> doRecloneScmRepository(scmRepository), Instant.now());
    }

    private void doRecloneScmRepository(ScmRepository scmRepository) {
        try {
            initializeAndPrepareRepository(scmRepository);
        } catch (IOException e) {
            throw new ScmException("Error while cloning the repository " + scmRepository.getName(), e);
        }
    }
}
