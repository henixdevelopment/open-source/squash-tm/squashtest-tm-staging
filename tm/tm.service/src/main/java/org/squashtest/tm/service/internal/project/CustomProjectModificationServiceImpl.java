/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.project;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;
import static org.squashtest.tm.domain.project.Project.PROJECT_TYPE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_LIBRARY_OR_HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.TableField;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.bugtracker.BugtrackerProject;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTag;
import org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTagHolder;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.NoBugTrackerBindingException;
import org.squashtest.tm.exception.library.CannotDeleteProjectException;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.bugtracker.CustomBugTrackerModificationService;
import org.squashtest.tm.service.customfield.CustomFieldModelService;
import org.squashtest.tm.service.infolist.InfoListModelService;
import org.squashtest.tm.service.internal.display.dto.TemplateConfigurablePluginBindingDto;
import org.squashtest.tm.service.internal.dto.CustomFieldBindingModel;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.dto.json.JsonInfoList;
import org.squashtest.tm.service.internal.dto.json.JsonMilestone;
import org.squashtest.tm.service.internal.dto.json.JsonProject;
import org.squashtest.tm.service.internal.repository.BugtrackerProjectDao;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.ProjectTemplateDao;
import org.squashtest.tm.service.internal.repository.TemplateConfigurablePluginBindingDao;
import org.squashtest.tm.service.internal.servers.ManageableTokenAuthCredentials;
import org.squashtest.tm.service.milestone.MilestoneModelService;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.project.CustomProjectModificationService;
import org.squashtest.tm.service.project.GenericProjectCopyParameter;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginService;
import org.squashtest.tm.service.user.UserAccountService;

/**
 * @author mpagnon
 */
@Service("CustomProjectModificationService")
@Transactional
public class CustomProjectModificationServiceImpl implements CustomProjectModificationService {

    private final ProjectDeletionHandler projectDeletionHandler;
    private final ProjectTemplateDao projectTemplateDao;
    private final GenericProjectManagerService genericProjectManager;
    private final ProjectDao projectDao;
    private final PermissionEvaluationService permissionEvaluationService;
    private final GenericProjectDao genericProjectDao;
    private final UserAccountService userAccountService;
    private final DSLContext dsl;
    private final MilestoneModelService milestoneModelService;
    private final CustomFieldModelService customFieldModelService;
    private final InfoListModelService infoListModelService;
    private final TemplateConfigurablePluginBindingService templatePluginBindingService;
    private final TemplateConfigurablePluginService templateConfigurablePluginService;
    private final StoredCredentialsManager storedCredentialsManager;
    private final CustomProjectFinder customProjectFinder;
    private final BugtrackerProjectDao bugtrackerProjectDao;
    private final CustomBugTrackerModificationService customBugTrackerModificationService;
    private final TemplateConfigurablePluginBindingDao templateConfigurablePluginBindingDao;

    @PersistenceContext EntityManager entityManager;

    public CustomProjectModificationServiceImpl(
            ProjectDeletionHandler projectDeletionHandler,
            ProjectTemplateDao projectTemplateDao,
            GenericProjectManagerService genericProjectManager,
            ProjectDao projectDao,
            PermissionEvaluationService permissionEvaluationService,
            GenericProjectDao genericProjectDao,
            UserAccountService userAccountService,
            DSLContext dsl,
            MilestoneModelService milestoneModelService,
            CustomFieldModelService customFieldModelService,
            InfoListModelService infoListModelService,
            TemplateConfigurablePluginBindingService templatePluginBindingService,
            TemplateConfigurablePluginService templateConfigurablePluginService,
            StoredCredentialsManager storedCredentialsManager,
            CustomProjectFinder customProjectFinder,
            BugtrackerProjectDao bugtrackerProjectDao,
            CustomBugTrackerModificationService customBugTrackerModificationService,
            TemplateConfigurablePluginBindingDao templateConfigurablePluginBindingDao) {
        this.projectDeletionHandler = projectDeletionHandler;
        this.projectTemplateDao = projectTemplateDao;
        this.genericProjectManager = genericProjectManager;
        this.projectDao = projectDao;
        this.permissionEvaluationService = permissionEvaluationService;
        this.genericProjectDao = genericProjectDao;
        this.userAccountService = userAccountService;
        this.dsl = dsl;
        this.milestoneModelService = milestoneModelService;
        this.customFieldModelService = customFieldModelService;
        this.infoListModelService = infoListModelService;
        this.templatePluginBindingService = templatePluginBindingService;
        this.templateConfigurablePluginService = templateConfigurablePluginService;
        this.storedCredentialsManager = storedCredentialsManager;
        this.customProjectFinder = customProjectFinder;
        this.bugtrackerProjectDao = bugtrackerProjectDao;
        this.customBugTrackerModificationService = customBugTrackerModificationService;
        this.templateConfigurablePluginBindingDao = templateConfigurablePluginBindingDao;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteProject(long projectId) {
        if (hasProjectData(projectId)) {
            throw new CannotDeleteProjectException();
        }
        projectDeletionHandler.deleteProject(projectId);
    }

    @Override
    public boolean hasProjectData(long projectId) {
        try {
            projectDeletionHandler.checkProjectContainsOnlyFolders(projectId);
        } catch (CannotDeleteProjectException e) { // NOSONAR
            // : this exception is part of the nominal use case
            return true;
        }

        return false;
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void updateBugTrackerProjectNames(long projectId, List<String> projectBugTrackerNames) {
        GenericProject project = genericProjectDao.getProjectWithBugtrackerProjects(projectId);
        if (!project.isBoundToBugtracker()) {
            throw new NoBugTrackerBindingException(
                    "No bugtracker is linked to the project with id: " + projectId);
        }

        bugtrackerProjectDao.deleteAllByProjectId(projectId);
        addBugtrackerProjects(new LinkedHashSet<>(projectBugTrackerNames), project);

        customBugTrackerModificationService.refreshCacheForProject(projectId);
    }

    private static void addBugtrackerProjects(
            Set<String> projectBugTrackerNames, GenericProject project) {
        List<BugtrackerProject> newBugtrackerProjects = new ArrayList<>();
        int counter = 0;
        for (String name : projectBugTrackerNames) {
            newBugtrackerProjects.add(new BugtrackerProject(name, project, counter++));
        }
        project.addBugtrackerProjects(newBugtrackerProjects);
    }

    @Override
    public Project addProjectFromTemplate(
            Project newProject, long templateId, GenericProjectCopyParameter params)
            throws NameAlreadyInUseException {

        genericProjectManager.persist(newProject);

        ProjectTemplate projectTemplate = projectTemplateDao.getReferenceById(templateId);
        if (params.isKeepTemplateBinding()) {
            newProject.setTemplate(projectTemplate);
            makeParamsConsistent(params);
        }

        genericProjectManager.synchronizeGenericProject(newProject, projectTemplate, params);
        createPluginBindings(newProject, projectTemplate, params);
        genericProjectManager.createAttachmentFromDescription(newProject);
        return newProject;
    }

    private void createPluginBindings(
            Project newProject, ProjectTemplate projectTemplate, GenericProjectCopyParameter params) {
        final Long projectId = newProject.getId();
        final Long templateId = projectTemplate.getId();

        if (params.isKeepPluginsBinding()) {
            GenericProject sourceProject = genericProjectManager.findById(projectTemplate.getId());
            List<String> enabledPlugins =
                    sourceProject.getRequirementLibrary().getEnabledPlugins().stream()
                            .collect(Collectors.toList());
            List<TemplateConfigurablePluginBindingDto> alreadyBoundPlugins =
                    templateConfigurablePluginBindingDao.findAll(templateId, projectId, enabledPlugins);
            enabledPlugins.removeIf(
                    plugin ->
                            alreadyBoundPlugins.stream()
                                    .anyMatch(binding -> binding.getPluginId().equals(plugin)));
            List<TemplateConfigurablePlugin> plugins =
                    templateConfigurablePluginService.findByIds(enabledPlugins);
            templatePluginBindingService.createTemplateConfigurablePluginBindings(
                    projectTemplate, newProject, plugins);
        }
    }

    /* If binding with Template is kept, some parameters must be copied. */
    private void makeParamsConsistent(GenericProjectCopyParameter params) {
        params.setCopyCUF(true);
        params.setCopyInfolists(true);
        params.setCopyAllowTcModifFromExec(true);
        params.setCopyOptionalExecStatuses(true);
    }

    @Override
    public List<GenericProject> findAllICanManage() {
        List<GenericProject> projects = genericProjectDao.findAll();
        List<GenericProject> manageableProjects = new ArrayList<>();

        for (GenericProject project : projects) {
            if (permissionEvaluationService.hasRoleOrPermissionOnObject(
                    ROLE_ADMIN, Permissions.MANAGE_PROJECT.name(), project)) {
                manageableProjects.add(project);
            }
        }
        return manageableProjects;
    }

    @Override
    public List<String> findProjectNamesByIds(List<Long> projectIds) {
        return projectDao.findProjectNamesByIds(projectIds);
    }

    /**
     * Optimized implementation with SQL and no hibernate entities.
     *
     * @param userDto
     */
    @Override
    public List<Long> findAllReadableIds(UserDto userDto) {
        return findAllIdsByPermission(userDto, Permissions.READ.getMask());
    }

    @Override
    public CommonTableExpression<Record1<Long>> findAllReadableIdsCte(UserDto userDto) {
        return findAllIdsByPermissionCte(userDto, Permissions.READ.getMask());
    }

    @Override
    public boolean checkHasAtLeastOneReadableId(CommonTableExpression<Record1<Long>> projectIdsCte) {
        return projectDao.checkHasAtLeastOneReadableId(projectIdsCte);
    }

    @Override
    public List<Long> findReadableProjectIdsOnRequirementLibrary() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return findAllReadableIdsByLibraryClassName(
                currentUser, RequirementLibrary.class.getName(), PROJECT.RL_ID);
    }

    @Override
    public List<Long> findReadableProjectIdsOnTestCaseLibrary() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return findAllReadableIdsByLibraryClassName(
                currentUser, TestCaseLibrary.class.getName(), PROJECT.TCL_ID);
    }

    @Override
    public List<Long> findReadableProjectIdsOnCampaignLibrary() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return findAllReadableIdsByLibraryClassName(
                currentUser, CampaignLibrary.class.getName(), PROJECT.CL_ID);
    }

    @Override
    public List<Long> findReadableProjectIdsOnActionWordLibrary() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return findAllReadableIdsByLibraryClassName(
                currentUser, ActionWordLibrary.class.getName(), PROJECT.AWL_ID);
    }

    @Override
    public List<Long> findAllReadableIdsByLibraryClassName(
            UserDto userDto,
            String libraryClassName,
            TableField<ProjectRecord, Long> libraryColumnField) {
        if (userDto.isAdmin()) {
            return projectDao.findAllProjectIds();
        } else {
            return projectDao.findAllProjectIdsByPermissionMaskAndClassName(
                    userDto.getPartyIds(), Permissions.READ.getMask(), libraryClassName, libraryColumnField);
        }
    }

    @Override
    public List<Long> findAllManageableIds(UserDto userDto) {
        return findAllIdsByPermission(userDto, Permissions.MANAGE_PROJECT.getMask());
    }

    @Override
    public List<Long> findAllMilestoneManageableIds(UserDto userDto) {
        return findAllIdsByPermission(userDto, Permissions.MANAGE_MILESTONE.getMask());
    }

    @Override
    @PreAuthorize(READ_REQUIREMENT_LIBRARY_OR_HAS_ROLE_ADMIN)
    public Long findByRequirementLibraryId(long libraryId) {
        return projectDao.findByRequirementLibraryId(libraryId);
    }

    @Override
    public List<Long> findAllProjectIdsByEligibleTCPermission(UserDto userDto, int permission) {
        if (userDto.isAdmin()) {
            return projectDao.findAllProjectIds();
        }
        return projectDao.findAllProjectIdsByEligibleTCPermission(userDto.getPartyIds(), permission);
    }

    private List<Long> findAllIdsByPermission(UserDto userDto, int permission) {
        if (userDto.isAdmin()) {
            return projectDao.findAllProjectIds();
        } else {
            return projectDao.findAllProjectIdsByPermission(userDto.getPartyIds(), permission);
        }
    }

    private CommonTableExpression<Record1<Long>> findAllIdsByPermissionCte(
            UserDto userDto, int permission) {
        if (userDto.isAdmin()) {
            return projectDao.findAllProjectIdsCte();
        } else {
            return projectDao.findAllProjectIdsByPermissionCte(userDto.getPartyIds(), permission);
        }
    }

    @Override
    public List<Long> findAllExportableIdsOnGivenLibrary(
            UserDto userDto,
            String libraryClassname,
            TableField<ProjectRecord, Long> libraryColumnField) {
        if (userDto.isAdmin()) {
            return projectDao.findAllProjectIds();
        } else {
            return projectDao.findAllProjectIdsByPermissionMaskAndClassName(
                    userDto.getPartyIds(),
                    Permissions.EXPORT.getMask(),
                    libraryClassname,
                    libraryColumnField);
        }
    }

    /** Optimized implementation with SQL and no hibernate entities. */
    @Override
    public List<Long> findAllReadableIdsForAutomationWriter() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        if (currentUser.isAdmin()) {
            return projectDao.findAllProjectIds();
        } else {
            return projectDao.findAllProjectIdsForAutomationWriter(currentUser.getPartyIds());
        }
    }

    @Override
    public List<Long> findAllReadableIds() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return findAllReadableIds(currentUser);
    }

    @Override
    public List<Long> findAllManageableIds() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return findAllManageableIds(currentUser);
    }

    @Override
    public List<Long> findAllProjectIdsByEligibleTCPermission(int permission) {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return findAllProjectIdsByEligibleTCPermission(currentUser, permission);
    }

    @Override
    public List<Long> findAllProjectIdsOrderedByName(List<Long> projectIds) {
        return projectDao.findAllProjectIdsInListOrderedByName(projectIds);
    }

    @Override
    public boolean canReadAtLeastOneProject() {
        return !findAllReadableIds().isEmpty();
    }

    @Override
    public List<Project> findAllOrderedByName() {
        List<Long> readableProjectIds = customProjectFinder.findAllReadableIds();

        return readableProjectIds.isEmpty()
                ? new ArrayList<>()
                : projectDao.findByIdInOrderByName(readableProjectIds);
    }

    @Override
    public Collection<JsonProject> findAllProjects(
            List<Long> readableProjectIds, UserDto currentUser) {
        Map<Long, JsonProject> jsonProjects = doFindAllProjects(readableProjectIds);
        return jsonProjects.values();
    }

    @Override
    public Integer countProjectsAllowAutomationWorkflow() {
        return projectDao.countProjectsAllowAutomationWorkflow();
    }

    protected Map<Long, JsonProject> doFindAllProjects(List<Long> readableProjectIds) {
        // As projects are objects with complex relationship we pre fetch some of the relation to avoid
        // unnecessary joins or requests, and unnecessary conversion in DTO after fetch
        // We do that only on collaborators which should not be too numerous versus the number of
        // projects
        // good candidate for this pre fetch are infolists, custom fields (not bindings), milestones...
        Map<Long, JsonInfoList> infoListMap = infoListModelService.findUsedInfoList(readableProjectIds);

        Map<Long, JsonProject> jsonProjectMap = findJsonProjects(readableProjectIds, infoListMap);

        // Now we retrieve the bindings for projects, injecting cuf inside
        Map<Long, Map<String, List<CustomFieldBindingModel>>> customFieldsBindingsByProject =
                customFieldModelService.findCustomFieldsBindingsByProject(readableProjectIds);

        // We find the milestone bindings and provide projects with them
        Map<Long, List<JsonMilestone>> milestoneByProjectId =
                milestoneModelService.findMilestoneByProject(readableProjectIds);

        // We provide the projects with their bindings and milestones
        jsonProjectMap.forEach(
                (projectId, jsonProject) -> {
                    if (customFieldsBindingsByProject.containsKey(projectId)) {
                        Map<String, List<CustomFieldBindingModel>> bindingsByEntityType =
                                customFieldsBindingsByProject.get(projectId);
                        jsonProject.setCustomFieldBindings(bindingsByEntityType);
                    }

                    if (milestoneByProjectId.containsKey(projectId)) {
                        List<JsonMilestone> jsonMilestone = milestoneByProjectId.get(projectId);
                        jsonProject.setMilestones(new HashSet<>(jsonMilestone));
                    }
                });

        return jsonProjectMap;
    }

    private Map<Long, JsonProject> findJsonProjects(
            List<Long> readableProjectIds, Map<Long, JsonInfoList> infoListMap) {
        return dsl
                .select(
                        PROJECT.PROJECT_ID,
                        PROJECT.NAME,
                        PROJECT.REQ_CATEGORIES_LIST,
                        PROJECT.TC_NATURES_LIST,
                        PROJECT.TC_TYPES_LIST)
                .from(PROJECT)
                .where(PROJECT.PROJECT_ID.in(readableProjectIds))
                .and(PROJECT.PROJECT_TYPE.eq(PROJECT_TYPE))
                .orderBy(PROJECT.PROJECT_ID)
                .stream()
                .map(
                        r -> {
                            Long projectId = r.get(PROJECT.PROJECT_ID);
                            JsonProject jsonProject = new JsonProject(projectId, r.get(PROJECT.NAME));
                            jsonProject.setRequirementCategories(
                                    infoListMap.get(r.get(PROJECT.REQ_CATEGORIES_LIST)));
                            jsonProject.setTestCaseNatures(infoListMap.get(r.get(PROJECT.TC_NATURES_LIST)));
                            jsonProject.setTestCaseTypes(infoListMap.get(r.get(PROJECT.TC_TYPES_LIST)));
                            return jsonProject;
                        })
                .collect(
                        Collectors.toMap(
                                JsonProject::getId,
                                Function.identity(),
                                (u, v) -> {
                                    throw new IllegalStateException(String.format("Duplicate key %s", u));
                                },
                                LinkedHashMap::new));
    }

    @Override
    public void overrideEnvironmentTags(long projectId, List<String> tags) {
        final Optional<GenericProject> maybeProject = genericProjectDao.findById(projectId);

        if (maybeProject.isPresent()) {
            final GenericProject project = maybeProject.get();
            project.setEnvironmentTags(
                    tags.stream()
                            .map(tag -> new AutomationEnvironmentTag(tag, AutomationEnvironmentTagHolder.PROJECT))
                            .toList());
            project.setInheritsEnvironmentTags(false);
        } else {
            throwProjectNotFoundException(projectId);
        }
    }

    @Override
    public void clearEnvironmentTagOverrides(long projectId) {
        genericProjectManager.clearEnvironmentTagOverrides(projectId);
    }

    @Override
    public void setTestAutomationServerTokenOverride(long projectId, long serverId, String token) {
        assertProjectExists(projectId);
        assertTAServerExists(serverId);
        final ManageableTokenAuthCredentials tokenAuthCredentials =
                new ManageableTokenAuthCredentials(token);
        storedCredentialsManager.storeProjectCredentials(serverId, projectId, tokenAuthCredentials);

        clearEnvironmentTagOverrides(projectId);
    }

    @Override
    public void clearTestAutomationServerTokenOverride(long projectId, long serverId) {
        assertProjectExists(projectId);
        storedCredentialsManager.deleteProjectCredentials(serverId, projectId);

        clearEnvironmentTagOverrides(projectId);
    }

    private void assertProjectExists(long projectId) {
        final Optional<GenericProject> maybeProject = genericProjectDao.findById(projectId);

        if (maybeProject.isEmpty()) {
            throwProjectNotFoundException(projectId);
        }
    }

    private void throwProjectNotFoundException(long projectId) {
        throw new IllegalArgumentException(
                String.format("Project with id %d was not found", projectId));
    }

    private void assertTAServerExists(long projectId) {
        final TestAutomationServer maybeServer =
                entityManager.find(TestAutomationServer.class, projectId);

        if (maybeServer == null) {
            throw new IllegalArgumentException(
                    String.format("Test automation server with id %d was not found", projectId));
        }
    }
}
