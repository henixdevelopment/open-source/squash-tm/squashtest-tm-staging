/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.rowNumber;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Record3;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.attachment.ExternalContentCoordinates;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.FailureDetail;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentTagManagerService;
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentVariableManagerService;
import org.squashtest.tm.service.internal.campaign.CampaignNodeDeletionHandler;
import org.squashtest.tm.service.internal.campaign.LockedCampaignNodeDetectionService;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.deletion.jdbc.JdbcCampaignDeletionHandlerFactory;
import org.squashtest.tm.service.internal.deletion.jdbc.JdbcExecutionDeletionHandlerFactory;
import org.squashtest.tm.service.internal.deletion.jdbc.JdbcIterationDeletionHandlerFactory;
import org.squashtest.tm.service.internal.denormalizedfield.PrivateDenormalizedFieldValueService;
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao;
import org.squashtest.tm.service.internal.repository.AutomatedTestDao;
import org.squashtest.tm.service.internal.repository.CampaignDeletionDao;
import org.squashtest.tm.service.internal.repository.CampaignFolderDao;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.FailureDetailDao;
import org.squashtest.tm.service.internal.repository.FolderDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;

@Component("squashtest.tm.service.deletion.CampaignNodeDeletionHandler")
public class CampaignDeletionHandlerImpl
        extends AbstractNodeDeletionHandler<CampaignLibraryNode, CampaignFolder>
        implements CampaignNodeDeletionHandler {

    public static final int BIND_VARIABLES_LIMIT = 25000;

    @Inject private LockedCampaignNodeDetectionService lockedCampaignNodeDetectionService;

    @Inject private CampaignFolderDao folderDao;

    @Inject private CampaignDeletionDao deletionDao;

    @Inject private TestSuiteDao suiteDao;

    @Inject private IterationTestPlanDao iterationTestPlanDao;

    @Inject private ExecutionDao executionDao;

    @Inject private AutomatedTestDao autoTestDao;

    @Inject private AutomatedSuiteManagerService autoSuiteManagerService;

    @Inject private PrivateCustomFieldValueService customValueService;

    @Inject private PrivateDenormalizedFieldValueService denormalizedFieldValueService;

    @Inject
    private DenormalizedEnvironmentVariableManagerService
            denormalizedEnvironmentVariableManagerService;

    @Inject private DenormalizedEnvironmentTagManagerService denormalizedEnvironmentTagManagerService;

    @Inject private IterationTestPlanManagerService iterationTestPlanManagerService;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private CustomTestSuiteModificationService customTestSuiteModificationService;

    @Inject private JdbcIterationDeletionHandlerFactory jdbcIterationDeletionHandlerFactory;

    @Inject private JdbcCampaignDeletionHandlerFactory jdbcCampaignDeletionHandlerFactory;

    @Inject private JdbcExecutionDeletionHandlerFactory jdbcExecutionDeletionHandler;

    @Inject private DSLContext dslContext;

    @Inject private AutomatedExecutionExtenderDao automatedExecutionExtenderDao;

    @Inject private FailureDetailDao failureDetailDao;

    @PersistenceContext private EntityManager entityManager;

    @Override
    protected FolderDao<CampaignFolder, CampaignLibraryNode> getFolderDao() {
        return folderDao;
    }

    /* ************************** diagnostic section ******************************* */

    @Override
    protected List<SuppressionPreviewReport> diagnoseSuppression(List<Long> nodeIds) {
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        reportList.addAll(lockedCampaignNodeDetectionService.detectLockedCampaigns(nodeIds));
        reportList.addAll(lockedCampaignNodeDetectionService.detectLockedSprints(nodeIds));
        reportList.addAll(lockedCampaignNodeDetectionService.detectLockedByMilestone(nodeIds));
        reportList.addAll(lockedCampaignNodeDetectionService.detectLockedWithActiveMilestone(nodeIds));
        return reportList;
    }

    @Override
    public List<SuppressionPreviewReport> simulateIterationDeletion(List<Long> targetIds) {
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        reportList.addAll(lockedCampaignNodeDetectionService.detectLockedIteration(targetIds));
        reportList.addAll(
                lockedCampaignNodeDetectionService.detectLockedByMilestoneIteration(targetIds));
        return reportList;
    }

    @Override
    public List<SuppressionPreviewReport> simulateExecutionDeletion(Long execId) {
        // TODO : implement the specs when they are ready. Default is "nothing special".
        return Collections.emptyList();
    }

    @Override
    public List<SuppressionPreviewReport> simulateSuiteDeletion(List<Long> targetIds) {
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        reportList.addAll(lockedCampaignNodeDetectionService.detectLockedSuites(targetIds));
        reportList.addAll(
                lockedCampaignNodeDetectionService.detectLockedByMilestoneTestSuite(targetIds));
        reportList.addAll(
                lockedCampaignNodeDetectionService.detectTestPlanItemBoundMultipleTestSuite(targetIds));
        return reportList;
    }

    @Override
    protected List<Long> detectLockedNodes(List<Long> nodeIds) {
        return diagnoseSuppression(nodeIds).stream()
                .flatMap(report -> report.getLockedNodes().stream())
                .distinct()
                .toList();
    }

    @Override
    protected OperationReport batchDeleteNodes(List<Long> ids) {
        return jdbcCampaignDeletionHandlerFactory.build(ids).delete();
    }

    @Override
    protected OperationReport batchUnbindFromMilestone(List<Long> ids) {

        List<Long> remainingIds = deletionDao.findRemainingCampaignIds(ids);

        // some node should not be unbound.
        List<Long> lockedIds = deletionDao.findCampaignsWhichMilestonesForbidsDeletion(remainingIds);
        remainingIds.removeAll(lockedIds);

        OperationReport report = new OperationReport();
        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();

        activeMilestone.ifPresent(
                milestone -> deletionDao.unbindFromMilestone(remainingIds, milestone.getId()));

        report.addRemoved(remainingIds, "campaign");

        return report;
    }

    @Override
    public OperationReport deleteIterations(List<Long> targetIds) {
        List<Long> iterationsToDelete = new ArrayList<>(targetIds.size());
        List<Long> lockedIds =
                simulateIterationDeletion(targetIds).stream()
                        .flatMap(report -> report.getLockedNodes().stream())
                        .toList();
        iterationsToDelete.addAll(targetIds);
        iterationsToDelete.removeAll(lockedIds);

        doDeleteIterations(iterationsToDelete);
        OperationReport report = new OperationReport();
        report.addRemoved(iterationsToDelete, "iteration");
        return report;
    }

    private void doDeleteSuites(Collection<TestSuite> testSuites) {
        List<ExternalContentCoordinates> pairContentIDListIDS =
                testSuites.stream()
                        .flatMap(
                                testSuite -> {
                                    testSuite
                                            .getTestPlan()
                                            .forEach(testPlanItem -> testPlanItem.getTestSuites().clear());
                                    testSuite.getIteration().removeTestSuite(testSuite);
                                    return getExternalAttachmentContentCoordinatesOfObject(testSuite).stream();
                                })
                        .toList();

        List<Long> testSuiteIds = testSuites.stream().map(TestSuite::getId).toList();

        customValueService.deleteAllCustomFieldValues(BindableEntity.TEST_SUITE, testSuiteIds);
        List<Long> attachmentListIds = suiteDao.findTestSuiteAttachmentListIds(testSuiteIds);
        List<String> automatedSuitesIds = suiteDao.findTestSuitesAutomatedSuiteIds(testSuiteIds);
        autoSuiteManagerService.deleteAutomatedSuites(automatedSuitesIds);
        suiteDao.removeTestSuites(testSuiteIds);
        attachmentManager.removeAttachmentsAndLists(attachmentListIds);
        attachmentManager.deleteContents(pairContentIDListIDS);
    }

    @Override
    public void deleteExecution(Execution execution) {
        List<ExternalContentCoordinates> pairContentIDListIDSteps = deleteExecSteps(execution);
        List<ExternalContentCoordinates> pairContentIDListIDExec =
                getExternalAttachmentContentCoordinatesOfObject(execution);
        // Merge all lists of attachments
        if (!pairContentIDListIDExec.isEmpty()) {
            pairContentIDListIDSteps.addAll(pairContentIDListIDExec);
        }

        final IterationTestPlanItem iterationTestPlanItem = execution.getTestPlan();

        final Long projectId = execution.getProject().getId();

        deleteAutomatedExecutionExtenderAndDenormalizedEnvironments(execution, projectId);

        if (iterationTestPlanItem != null) {
            iterationTestPlanItem.removeExecution(execution);
        }

        denormalizedFieldValueService.deleteAllDenormalizedFieldValues(execution);
        customValueService.deleteAllCustomFieldValues(execution);

        if (iterationTestPlanItem != null) {
            customTestSuiteModificationService.updateExecutionStatus(
                    iterationTestPlanItem.getTestSuites());
        }

        deletionDao.removeEntity(execution); // cascade list, Attachment
        attachmentManager.deleteContents(pairContentIDListIDSteps);
    }

    private void deleteDenormalizedEnvironments(Execution execution, Long projectId) {
        if (isExecutedByAutomServer(projectId)) {
            denormalizedEnvironmentVariableManagerService
                    .removeDenormalizedEnvironmentVariablesOnExecutionDelete(
                            execution.getAutomatedExecutionExtender());
            denormalizedEnvironmentTagManagerService.removeDenormalizedTagsOnExecutionDelete(
                    execution.getAutomatedExecutionExtender());
            entityManager.flush();
        }
    }

    private boolean isExecutedByAutomServer(Long projectId) {
        TestAutomationServerKind serverKind =
                executionDao.getTestAutomationServerKindByProjectId(projectId);
        return Objects.nonNull(serverKind)
                && TestAutomationServerKind.squashOrchestrator.equals(serverKind);
    }

    @Override
    public void bulkDeleteExecutions(List<Long> executionIds) {
        List<List<Long>> executionIdPartitions = Lists.partition(executionIds, BIND_VARIABLES_LIMIT);

        Set<Long> testSuiteIds = new HashSet<>();

        executionIdPartitions.forEach(
                executionIdPartition -> {
                    testSuiteIds.addAll(suiteDao.findAllIdsByExecutionIds(executionIdPartition));

                    List<Long> itpiIds = executionDao.findItpiIdsByExecutionIds(executionIds);

                    jdbcExecutionDeletionHandler.build(executionIds).deleteExecutions();

                    batchUpdateItpiExecutionStatusAfterExecutionsDeletion(itpiIds);

                    entityManager.flush();
                });

        List<TestSuite> testSuites = suiteDao.findAllByIds(testSuiteIds);
        customTestSuiteModificationService.updateExecutionStatus(testSuites);
        entityManager.clear();
    }

    private void doDeleteIterations(List<Long> iterations) {
        if (!iterations.isEmpty()) {
            jdbcIterationDeletionHandlerFactory.build(iterations).deleteIterations();
        }
    }

    @Override
    public void deleteIterationTestPlanItem(IterationTestPlanItem item) {
        List<Execution> execs = new ArrayList<>(item.getExecutions());
        List<FailureDetail> failureDetails = new ArrayList<>(item.getFailureDetailList());
        deleteFailureDetails(failureDetails);
        item.getFailureDetailList().clear();
        deleteExecutions(execs);
        deletionDao.removeEntity(item);
    }

    @Override
    public void deleteSprintReqVersionTestPlanItem(TestPlanItem item) {
        List<Execution> execs = new ArrayList<>(item.getExecutions());
        deleteExecutions(execs);

        deletionDao.removeEntity(item);
    }

    /*
     *
     */
    @Override
    public void deleteExecutions(List<Execution> executions) {
        Collection<Execution> executionsCopy = new ArrayList<>(executions);
        for (Execution execution : executionsCopy) {
            deleteExecution(execution);
        }
    }

    @Override
    public void deleteFailureDetail(FailureDetail failureDetail) {
        List<AutomatedExecutionExtender> extenders = failureDetail.getAutomatedExecutions();
        for (AutomatedExecutionExtender extender : extenders) {
            extender.getFailureDetailList().remove(failureDetail);
            automatedExecutionExtenderDao.save(extender);
        }
        failureDetail.getAutomatedExecutions().clear();
        failureDetailDao.save(failureDetail);

        deletionDao.removeEntity(failureDetail);
    }

    @Override
    public void deleteFailureDetails(List<FailureDetail> failureDetails) {
        Collection<FailureDetail> failureDetailsCopy = new ArrayList<>(failureDetails);
        for (FailureDetail failureDetail : failureDetailsCopy) {
            deleteFailureDetail(failureDetail);
        }
    }

    /**
     * @param execution current execution
     * @return List<ExternalContentCoordinates id est the list of tuple ( AttachmentListId,
     *     AttachmentContentId ) corresponding to the ( path, fileName ) of the AttachmentContent when
     *     it is stored on FileSystemRepository
     */
    private List<ExternalContentCoordinates> deleteExecSteps(Execution execution) {

        /*
         * Even when asking the EntityManager to remove a step - thus assigning it a status DELETED -,
         * it can still abort its deletion when some random flush occurs :
         *
         * flushing the Execution (still in status MANAGED)
         * -> triggers cascade PERSIST on its steps
         * -> thus assign status MANAGED to the step that should have been deleted
         *
         * To prevent that to occur, first thing to do is to clear the step list.
         */

        Collection<ExecutionStep> steps = new ArrayList<>(execution.getSteps());
        execution.getSteps().clear();
        // saving path Content for FileSystem Repository
        List<ExternalContentCoordinates> pairContentIDListID;
        if (!steps.isEmpty()) {
            pairContentIDListID = attachmentManager.getListPairContentIDListIDForExecutionSteps(steps);
        } else {
            pairContentIDListID = new ArrayList<>();
        }

        // now we can delete them
        for (ExecutionStep step : steps) {
            denormalizedFieldValueService.deleteAllDenormalizedFieldValues(step);
            customValueService.deleteAllCustomFieldValues(step);
            deletionDao.removeEntity(step);
        }

        return pairContentIDListID;
    }

    private void deleteAutomatedExecutionExtenderAndDenormalizedEnvironments(
            Execution execution, Long projectId) {
        if (execution.getAutomatedExecutionExtender() == null) {
            return;
        }
        AutomatedExecutionExtender extender = execution.getAutomatedExecutionExtender();

        removeExtenderAssociationFromFailureDetails(extender);

        deleteDenormalizedEnvironments(execution, projectId);
        autoTestDao.removeIfUnused(extender.getAutomatedTest());
        deletionDao.removeEntity(extender);
        execution.setAutomatedExecutionExtender(null);
    }

    private void removeExtenderAssociationFromFailureDetails(AutomatedExecutionExtender extender) {
        List<FailureDetail> failureDetails = extender.getFailureDetailList();

        for (FailureDetail failureDetail : failureDetails) {
            failureDetail.getAutomatedExecutions().remove(extender);
            failureDetailDao.save(failureDetail);
        }
    }

    @Override
    public OperationReport deleteSuites(List<Long> suiteIds, boolean removeFromIter) {
        List<Long> suiteToDelete = new ArrayList<>(suiteIds.size());
        List<Long> lockedIds =
                simulateSuiteDeletion(suiteIds).stream()
                        .flatMap(report -> report.getLockedNodes().stream())
                        .toList();
        suiteToDelete.addAll(suiteIds);
        suiteToDelete.removeAll(lockedIds);

        List<TestSuite> suitesToDelete = suiteDao.findAllByIds(suiteToDelete);
        if (removeFromIter) {
            removeItpiFromIteration(suitesToDelete, suiteIds);
        }

        doDeleteSuites(suitesToDelete);

        OperationReport report = new OperationReport();

        if (!suitesToDelete.isEmpty()) {
            report.addRemoved(suiteIds, "test-suite");
        }

        return report;
    }

    @Override
    protected boolean isMilestoneMode() {
        return activeMilestoneHolder.getActiveMilestone().isPresent();
    }

    private void removeItpiFromIteration(List<TestSuite> suites, final List<Long> targetIds) {
        List<IterationTestPlanItem> listItpiToRemove =
                iterationTestPlanDao.findIterationTestPlanItemsToRemoveInDeleteTestSuite(suites, targetIds);
        if (listItpiToRemove != null) {
            Set<IterationTestPlanItem> itpiToRemove = new HashSet<>(listItpiToRemove);

            for (IterationTestPlanItem testPlanItem : itpiToRemove) {
                iterationTestPlanManagerService.removeTestPlanFromIteration(testPlanItem);
            }
        }
    }

    private void batchUpdateItpiExecutionStatusAfterExecutionsDeletion(
            List<Long> iterationTestPlanItemIds) {
        final String ITEM_TEST_PLAN_ID = "ITEM_TEST_PLAN_ID";
        final String EXECUTION_STATUS = "EXECUTION_STATUS";
        final String POSITION = "POSITION";

        CommonTableExpression<Record3<Long, String, Integer>> cte =
                name("cte")
                        .fields(ITEM_TEST_PLAN_ID, EXECUTION_STATUS, POSITION)
                        .as(
                                select(
                                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID,
                                                EXECUTION.EXECUTION_STATUS,
                                                rowNumber()
                                                        .over()
                                                        .partitionBy(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID)
                                                        .orderBy(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.desc()))
                                        .from(ITEM_TEST_PLAN_EXECUTION)
                                        .innerJoin(EXECUTION)
                                        .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                        .where(
                                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.in(iterationTestPlanItemIds)));

        Map<String, List<Long>> itemTestPlanIdsByExecutionStatus =
                dslContext
                        .with(cte)
                        .select(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                                when(cte.field(EXECUTION_STATUS).isNull(), ExecutionStatus.READY.name())
                                        .otherwise(cte.field(EXECUTION_STATUS, String.class))
                                        .as(EXECUTION_STATUS))
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .leftJoin(cte)
                        .on(
                                cte.field(ITEM_TEST_PLAN_ID, Long.class)
                                        .eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .where(
                                ITERATION_TEST_PLAN_ITEM
                                        .ITEM_TEST_PLAN_ID
                                        .in(iterationTestPlanItemIds)
                                        .and(
                                                cte.field(POSITION, Integer.class)
                                                        .eq(1)
                                                        .or(
                                                                cte.field(ITEM_TEST_PLAN_ID, Integer.class)
                                                                        .isNull()
                                                                        .and(
                                                                                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.notEqual(
                                                                                        ExecutionStatus.READY.name())))))
                        .fetchGroups(
                                field(EXECUTION_STATUS, String.class), ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);

        for (Map.Entry<String, List<Long>> entry : itemTestPlanIdsByExecutionStatus.entrySet()) {
            dslContext
                    .update(ITERATION_TEST_PLAN_ITEM)
                    .set(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS, entry.getKey())
                    .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(entry.getValue()))
                    .execute();
        }
    }
}
