/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.system;

import java.io.File;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.List;

/** Service for managing log files access and download. */
public interface LogFileDownloadService {
    /**
     * @return the current log file (the one that is currently being written to).
     */
    File getCurrentLogFile();

    /**
     * Get the names of all log files, excluding the current log file. The names are sorted in
     * ascending order. On a development environment, this method returns an empty list.
     *
     * @return the names of all log files
     */
    List<String> getAllPreviousLogFileNames();

    /**
     * Get the log file with the given name.
     *
     * @param fileName the name of the log file
     * @return the log file
     * @throws AccessDeniedException if the requested file is not accessible
     */
    File getPreviousLogFile(String fileName) throws IOException;

    File getLogFileWithPath(String filePath, String origin) throws IOException;
}
