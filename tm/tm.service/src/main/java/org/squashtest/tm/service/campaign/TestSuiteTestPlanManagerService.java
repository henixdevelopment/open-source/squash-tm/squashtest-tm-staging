/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.internal.dto.ExecutionSummaryDto;

/**
 * Service that aims at managing the test cases of a test suite (i.e. its test plan)
 *
 * @author François Gaillard
 */
public interface TestSuiteTestPlanManagerService {

    /**
     * Find a iteration using its id
     *
     * @param testSuiteId current test Suite Id
     */
    @UsedInPlugin("rest-api")
    TestSuite findTestSuite(long testSuiteId);

    void changeTestPlanPosition(long testSuiteId, int newIndex, List<Long> itemIds);

    /**
     * That method will attach several {@link IterationTestPlanItem} to several TestSuite. As usual,
     * they are identified using their Ids.
     *
     * <p>The implementation must also check that all these entities all belong to the same iteration
     * or throw an unchecked exception if not. TODO : define that exception.
     *
     * @param suiteIds current test Suite Id
     * @param itemTestPlanIds current item Test Plan Ids
     */
    void bindTestPlanToMultipleSuites(List<Long> suiteIds, List<Long> itemTestPlanIds);

    void unbindAllTestPlansFromTestPlanItem(List<Long> itemIds);

    CreatedTestPlanItems addTestCasesToIterationAndTestSuite(
            List<Long> testCaseIds, @Id long suiteId);

    CreatedTestPlanItems addTestCasesToIterationAndTestSuiteUnsecured(
            List<Long> testCaseIds, @Id long suiteId);

    void detachTestPlanFromTestSuite(List<Long> testPlanIds, @Id long suiteId);

    boolean detachTestPlanFromTestSuiteAndRemoveFromIteration(
            List<Long> testPlanIds, @Id long suiteId);

    List<User> findAssignableUserForTestPlan(long testSuiteId);

    Map<Long, List<ExecutionSummaryDto>> getLastExecutionStatuses(Collection<Long> itemTestPlanId);
}
