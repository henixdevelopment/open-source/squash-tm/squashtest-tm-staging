/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement;

import static org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader.Options.FETCH_REQUIREMENT_VERSION_COVERAGES;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.requirement.RequirementVersionLink;
import org.squashtest.tm.domain.requirement.RequirementVersionLinkType;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.CoverageImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.ExistLinkImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.LinkImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementFolderImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementVersionImportDto;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementFolderDao;
import org.squashtest.tm.service.internal.repository.RequirementLibraryDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionLinkDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionLinkTypeDao;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.internal.requirement.CategoryChainFixer;
import org.squashtest.tm.service.milestone.MilestoneMembershipManager;

@Service
@Transactional
public class RequirementImportServiceImpl implements RequirementImportService {

    private final RequirementLibraryDao requirementLibraryDao;
    private final RequirementFolderDao requirementFolderDao;
    private final RequirementDao requirementDao;
    private final EntityManager entityManager;
    private final PrivateCustomFieldValueService customFieldValueService;
    private final MilestoneMembershipManager milestoneService;
    private final TestCaseLoader testCaseLoader;
    private final RequirementVersionDao requirementVersionDao;
    private final RequirementVersionLinkTypeDao reqVersionLinkTypeDao;
    private final RequirementVersionLinkDao reqVersionLinkDao;

    public RequirementImportServiceImpl(
            RequirementLibraryDao requirementLibraryDao,
            RequirementFolderDao requirementFolderDao,
            RequirementDao requirementDao,
            EntityManager entityManager,
            PrivateCustomFieldValueService customFieldValueService,
            MilestoneMembershipManager milestoneService,
            TestCaseLoader testCaseLoader,
            RequirementVersionDao requirementVersionDao,
            RequirementVersionLinkTypeDao reqVersionLinkTypeDao,
            RequirementVersionLinkDao reqVersionLinkDao) {
        this.requirementLibraryDao = requirementLibraryDao;
        this.requirementFolderDao = requirementFolderDao;
        this.requirementDao = requirementDao;
        this.entityManager = entityManager;
        this.customFieldValueService = customFieldValueService;
        this.milestoneService = milestoneService;
        this.testCaseLoader = testCaseLoader;
        this.requirementVersionDao = requirementVersionDao;
        this.reqVersionLinkTypeDao = reqVersionLinkTypeDao;
        this.reqVersionLinkDao = reqVersionLinkDao;
    }

    @Override
    @PreventConcurrent(entityType = RequirementLibrary.class)
    public void addRequirementsToLibrary(
            @Id Long libraryId, Project project, List<RequirementImportDto> requirementsDto) {
        RequirementLibrary library = requirementLibraryDao.loadForNodeAddition(libraryId);

        for (RequirementImportDto dto : requirementsDto) {
            Requirement newRequirement = dto.getRequirement();

            library.addContent(newRequirement);
            replaceInfoListReferences(newRequirement);
            requirementDao.persist(newRequirement);
        }

        createRequirementCustomFieldValues(requirementsDto, project);
        initializeMilestoneBindings(requirementsDto);

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    @BatchPreventConcurrent(entityType = RequirementFolder.class)
    public void addRequirementsToFolders(
            @Ids List<Long> folderIds, Project project, List<Batch<RequirementImportDto>> batches) {
        Map<Long, RequirementFolder> requirementFolderById =
                requirementFolderDao.loadForNodeAddition(folderIds).stream()
                        .collect(Collectors.toMap(RequirementFolder::getId, Function.identity()));

        for (Batch<RequirementImportDto> batch : batches) {
            RequirementFolder folder = requirementFolderById.get(batch.getTargetId());

            for (RequirementImportDto dto : batch.getEntities()) {
                Requirement newRequirement = dto.getRequirement();

                folder.addContent(newRequirement);
                replaceInfoListReferences(newRequirement);
                requirementDao.persist(newRequirement);
            }
        }

        List<RequirementImportDto> requirementDtoList =
                batches.stream().flatMap(b -> b.getEntities().stream()).toList();

        createRequirementCustomFieldValues(requirementDtoList, project);
        initializeMilestoneBindings(requirementDtoList);

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    public void addRequirementsToRequirements(
            List<Long> requirementIds, Project project, List<Batch<RequirementImportDto>> batches) {
        Map<Long, Requirement> requirementById =
                requirementDao.loadForNodeAddition(requirementIds).stream()
                        .collect(Collectors.toMap(Requirement::getId, Function.identity()));

        for (Batch<RequirementImportDto> batch : batches) {
            Requirement parent = requirementById.get(batch.getTargetId());

            for (RequirementImportDto dto : batch.getEntities()) {
                Requirement newRequirement = dto.getRequirement();

                parent.addContent(newRequirement);
                replaceInfoListReferences(newRequirement);
                requirementDao.persist(newRequirement);
            }
        }

        List<RequirementImportDto> requirementDtoList =
                batches.stream().flatMap(b -> b.getEntities().stream()).toList();

        createRequirementCustomFieldValues(requirementDtoList, project);
        initializeMilestoneBindings(requirementDtoList);

        entityManager.flush();
        entityManager.clear();
    }

    private void replaceInfoListReferences(Requirement requirement) {
        CategoryChainFixer.fix(requirement);
    }

    @Override
    public void addFoldersToLibrary(
            Long libraryId, Project project, List<RequirementFolderImportDto> batch) {
        RequirementLibrary library = requirementLibraryDao.loadForNodeAddition(libraryId);

        List<RequirementFolder> folders =
                batch.stream().map(RequirementFolderImportDto::getFolder).toList();

        CategoryChainFixer fixer = new CategoryChainFixer();

        for (RequirementFolder folder : folders) {
            library.addContent(folder);
            fixer.fix(folder);
            requirementFolderDao.persist(folder);
        }

        batchFolderCustomFieldsAndMilestonesInit(batch, project);

        entityManager.flush();
        entityManager.clear();
    }

    private void batchFolderCustomFieldsAndMilestonesInit(
            List<RequirementFolderImportDto> foldersDto, Project project) {

        List<RequirementFolder> requirementFolders = new ArrayList<>();
        List<RequirementVersionImportDto> requirementVersions = new ArrayList<>();

        for (RequirementFolderImportDto folderDto : foldersDto) {
            requirementFolders.addAll(folderDto.getGeneratedFolders());
            requirementVersions.addAll(folderDto.getGeneratedVersions());
        }

        customFieldValueService.batchFolderCustomFieldValuesCreation(
                requirementFolders, BindableEntity.REQUIREMENT_FOLDER, project.getId());

        createVersionsCustomFieldValues(requirementVersions, project);

        Map<RequirementVersion, List<Long>> versionsMilestones =
                requirementVersions.stream()
                        .filter(entry -> !entry.milestoneIds().isEmpty())
                        .collect(
                                Collectors.toMap(
                                        RequirementVersionImportDto::requirementVersion,
                                        RequirementVersionImportDto::milestoneIds));

        milestoneService.bindHoldersToMilestones(versionsMilestones);
    }

    @Override
    public void addFoldersToFolders(
            List<Long> folderIds, Project project, List<Batch<RequirementFolderImportDto>> batches) {
        Map<Long, RequirementFolder> requirementFolderById =
                requirementFolderDao.loadForNodeAddition(folderIds).stream()
                        .collect(Collectors.toMap(RequirementFolder::getId, Function.identity()));

        List<RequirementFolderImportDto> foldersDto = new ArrayList<>();

        CategoryChainFixer fixer = new CategoryChainFixer();

        for (Batch<RequirementFolderImportDto> batch : batches) {
            RequirementFolder folder = requirementFolderById.get(batch.getTargetId());

            batch
                    .getEntities()
                    .forEach(
                            content -> {
                                RequirementFolder contentFolder = content.getFolder();
                                folder.addContent(contentFolder);
                                fixer.fix(contentFolder);
                                requirementFolderDao.persist(contentFolder);

                                foldersDto.add(content);
                            });
        }

        batchFolderCustomFieldsAndMilestonesInit(foldersDto, project);

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    public void addVersions(
            List<Long> requirementIds, Project project, List<Batch<RequirementVersionImportDto>> batch) {
        Map<Long, Requirement> requirementById =
                requirementDao.loadForVersionsAddition(requirementIds).stream()
                        .collect(Collectors.toMap(Requirement::getId, Function.identity()));

        Map<RequirementVersion, Map<Long, RawValue>> versionsCustomFields = new HashMap<>();
        Map<RequirementVersion, List<Long>> versionsMilestones = new HashMap<>();

        for (Batch<RequirementVersionImportDto> batchDto : batch) {
            Requirement parent = requirementById.get(batchDto.getTargetId());

            for (RequirementVersionImportDto dto : batchDto.getEntities()) {
                RequirementVersion version = dto.requirementVersion();
                parent.addExistingRequirementVersion(version);

                replaceInfoListReferences(parent);
                entityManager.persist(version);

                if (!dto.customFields().isEmpty()) {
                    versionsCustomFields.put(version, dto.customFields());
                }

                if (!dto.milestoneIds().isEmpty()) {
                    versionsMilestones.put(version, dto.milestoneIds());
                }
            }
        }

        customFieldValueService.initBatchCustomFieldValues(versionsCustomFields);
        milestoneService.bindHoldersToMilestones(versionsMilestones);
    }

    private void createRequirementCustomFieldValues(
            List<RequirementImportDto> requirementDtoList, Project project) {
        List<RequirementVersionImportDto> requirementVersionsDto =
                requirementDtoList.stream()
                        .flatMap(requirementDto -> requirementDto.getGeneratedVersions().stream())
                        .toList();

        createVersionsCustomFieldValues(requirementVersionsDto, project);
    }

    private void createVersionsCustomFieldValues(
            List<RequirementVersionImportDto> requirementVersionsDto, Project project) {

        List<RequirementVersion> requirementVersions =
                requirementVersionsDto.stream()
                        .map(RequirementVersionImportDto::requirementVersion)
                        .toList();

        createBatchCustomFieldValues(requirementVersions, project);

        Map<RequirementVersion, Map<Long, RawValue>> versionsCustomFields =
                requirementVersionsDto.stream()
                        .filter(entry -> !entry.customFields().isEmpty())
                        .collect(
                                Collectors.toMap(
                                        RequirementVersionImportDto::requirementVersion,
                                        RequirementVersionImportDto::customFields));

        customFieldValueService.initBatchCustomFieldValues(versionsCustomFields);
    }

    private void createBatchCustomFieldValues(
            List<RequirementVersion> requirementVersions, Project project) {
        customFieldValueService.createAllCustomFieldValues(requirementVersions, project);
    }

    private void initializeMilestoneBindings(List<RequirementImportDto> requirementDtoList) {
        Map<RequirementVersion, List<Long>> versionsMilestones =
                requirementDtoList.stream()
                        .flatMap(dto -> dto.getGeneratedVersions().stream())
                        .filter(entry -> !entry.milestoneIds().isEmpty())
                        .collect(
                                Collectors.toMap(
                                        RequirementVersionImportDto::requirementVersion,
                                        RequirementVersionImportDto::milestoneIds));

        milestoneService.bindHoldersToMilestones(versionsMilestones);
    }

    @Override
    public void createCoverages(List<Long> testCaseIds, List<Batch<CoverageImportDto>> batches) {
        Map<Long, TestCase> testCases =
                testCaseLoader.load(testCaseIds, EnumSet.of(FETCH_REQUIREMENT_VERSION_COVERAGES)).stream()
                        .collect(Collectors.toMap(TestCase::getId, Function.identity()));

        Set<Long> versionIds =
                batches.stream()
                        .flatMap(b -> b.getEntities().stream())
                        .map(CoverageImportDto::versionId)
                        .collect(Collectors.toSet());

        Map<Long, RequirementVersion> versions =
                requirementVersionDao.loadForCoverageAddition(versionIds).stream()
                        .collect(Collectors.toMap(RequirementVersion::getId, Function.identity()));

        for (Batch<CoverageImportDto> batch : batches) {
            TestCase testCase = testCases.get(batch.getTargetId());

            for (CoverageImportDto coverageImport : batch.getEntities()) {
                RequirementVersionCoverage coverage = coverageImport.getCoverage(testCase);
                RequirementVersion version = versions.get(coverageImport.versionId());

                coverage.setVerifiedRequirementVersion(version);

                if (!coverageImport.coverageExists()) {
                    coverage.setVerifyingTestCase(testCase);
                    entityManager.persist(coverage);
                }
            }
        }

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    public void createLinks(List<Batch<LinkImportDto>> batches) {

        Set<Long> versionIds =
                batches.stream()
                        .flatMap(batch -> batch.getEntities().stream())
                        .flatMap(
                                linkImport -> Stream.of(linkImport.sourceVersionId(), linkImport.destVersionId()))
                        .collect(Collectors.toSet());

        Set<String> roles =
                batches.stream()
                        .flatMap(batch -> batch.getEntities().stream())
                        .map(LinkImportDto::destRole)
                        .collect(Collectors.toSet());

        Map<Long, RequirementVersion> versions =
                requirementVersionDao.findAllById(versionIds).stream()
                        .collect(Collectors.toMap(RequirementVersion::getId, Function.identity()));

        Map<String, RequirementVersionLinkType> linkTypes = getLinkTypes(roles);

        for (var batch : batches) {
            RequirementVersion source = versions.get(batch.getTargetId());

            for (var linkImport : batch.getEntities()) {

                RequirementVersion dest = versions.get(linkImport.destVersionId());
                RequirementVersionLinkType type = linkTypes.get(linkImport.destRole());
                boolean outboundDirection = !type.getRole2Code().equals(linkImport.destRole());

                RequirementVersionLink outboundLink =
                        new RequirementVersionLink(source, dest, type, outboundDirection);
                entityManager.persist(outboundLink);

                RequirementVersionLink inboundLink = outboundLink.createSymmetricalRequirementVersionLink();
                entityManager.persist(inboundLink);
            }
        }

        entityManager.flush();
        entityManager.clear();
    }

    private Map<String, RequirementVersionLinkType> getLinkTypes(Set<String> roles) {
        Map<String, RequirementVersionLinkType> result = new HashMap<>();

        for (RequirementVersionLinkType link : reqVersionLinkTypeDao.findByRoleCodes(roles)) {
            result.put(link.getRole1Code(), link);
            result.put(link.getRole2Code(), link);
        }

        return result;
    }

    @Override
    public void updateLinks(List<ExistLinkImportDto> batch) {
        Set<Long> linkIds =
                batch.stream()
                        .flatMap(link -> Stream.of(link.inboundLinkId(), link.outboundLinkId()))
                        .collect(Collectors.toSet());

        Set<String> roles =
                batch.stream().map(ExistLinkImportDto::destRole).collect(Collectors.toSet());

        Map<Long, RequirementVersionLink> links =
                reqVersionLinkDao.findAllByIds(linkIds).stream()
                        .collect(Collectors.toMap(RequirementVersionLink::getId, Function.identity()));

        Map<String, RequirementVersionLinkType> linkTypes = getLinkTypes(roles);

        for (var link : batch) {
            RequirementVersionLink inboundLink = links.get(link.inboundLinkId());
            RequirementVersionLink outboundLink = links.get(link.outboundLinkId());
            RequirementVersionLinkType type = linkTypes.get(link.destRole());
            boolean outboundDirection = !type.getRole2Code().equals(link.destRole());

            outboundLink.setLinkType(type);
            outboundLink.setLinkDirection(outboundDirection);

            inboundLink.setLinkType(type);
            inboundLink.setLinkDirection(!outboundDirection);
        }

        entityManager.flush();
        entityManager.clear();
    }
}
