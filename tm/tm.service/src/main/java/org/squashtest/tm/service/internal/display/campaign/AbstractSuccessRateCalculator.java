/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SUCCESS_RATE;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

public abstract class AbstractSuccessRateCalculator {

    protected final DSLContext dslContext;

    protected AbstractSuccessRateCalculator(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    public void appendSuccessRate(GridResponse gridResponse) {
        appendSuccessRate(gridResponse.getDataRows());
    }

    private void appendSuccessRate(List<DataRow> dataRows) {
        Set<Long> dataRowIds = extractIds(dataRows);

        Map<Long, Float> rateById = createRateByDataRowIdMap(dataRowIds);

        Converter<String, String> converter =
                CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
        String successRateKey = converter.convert(SUCCESS_RATE);
        String executionModeKey = converter.convert(EXECUTION_MODE);
        String executionStatusKey = converter.convert(EXECUTION_STATUS);

        dataRows.forEach(
                dataRow -> {
                    long id = Long.parseLong(dataRow.getId());
                    if (TestCaseExecutionMode.AUTOMATED
                            .name()
                            .equals(dataRow.getData().get(executionModeKey))) {
                        boolean isAutomatedExecutionInSuccess =
                                ExecutionStatus.SUCCESS.name().equals(dataRow.getData().get(executionStatusKey));
                        float automatedExecutionSuccessRate = isAutomatedExecutionInSuccess ? 100 : 0;
                        dataRow.getData().put(successRateKey, automatedExecutionSuccessRate);
                    } else {
                        dataRow.getData().put(successRateKey, rateById.getOrDefault(id, 0f));
                    }
                });
    }

    private Set<Long> extractIds(List<DataRow> dataRows) {
        return dataRows.stream().map(DataRow::getId).map(Long::parseLong).collect(Collectors.toSet());
    }

    protected abstract Map<Long, Float> createRateByDataRowIdMap(Set<Long> dataRowIds);
}
