/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXPLORATORY_TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.HAS_DELEGATED_PARAMETER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KEYWORD_TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SCRIPTED_TEST_CASE_ID;

import java.util.List;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.display.dto.testcase.CalledTestCaseDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseFolderDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseLibraryDto;
import org.squashtest.tm.service.internal.repository.display.TestCaseDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class TestCaseDisplayDaoImpl implements TestCaseDisplayDao {

    private final DSLContext dsl;

    public TestCaseDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public TestCaseDto getTestCaseDtoById(Long testCaseId) {
        return dsl.select(
                        TEST_CASE_LIBRARY_NODE.TCLN_ID.as(RequestAliasesConstants.ID),
                        TEST_CASE_LIBRARY_NODE.NAME,
                        TEST_CASE_LIBRARY_NODE.DESCRIPTION,
                        TEST_CASE_LIBRARY_NODE.PROJECT_ID,
                        TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID,
                        TEST_CASE_LIBRARY_NODE.CREATED_BY,
                        TEST_CASE_LIBRARY_NODE.CREATED_ON,
                        TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY,
                        TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_ON,
                        TEST_CASE.REFERENCE,
                        TEST_CASE.IMPORTANCE,
                        TEST_CASE.TC_STATUS.as(RequestAliasesConstants.STATUS),
                        TEST_CASE.AUTOMATABLE,
                        TEST_CASE.IMPORTANCE_AUTO,
                        TEST_CASE.PREREQUISITE,
                        TEST_CASE.TC_NATURE.as(RequestAliasesConstants.NATURE),
                        TEST_CASE.TC_TYPE.as(RequestAliasesConstants.TYPE),
                        TEST_CASE.UUID,
                        TEST_CASE.EXECUTION_MODE,
                        SCRIPTED_TEST_CASE.TCLN_ID.as(SCRIPTED_TEST_CASE_ID),
                        SCRIPTED_TEST_CASE.SCRIPT,
                        KEYWORD_TEST_CASE.TCLN_ID.as(KEYWORD_TEST_CASE_ID),
                        EXPLORATORY_TEST_CASE.TCLN_ID.as(EXPLORATORY_TEST_CASE_ID),
                        EXPLORATORY_TEST_CASE.CHARTER,
                        EXPLORATORY_TEST_CASE.SESSION_DURATION,
                        TEST_CASE.AUTOMATED_TEST_REFERENCE,
                        TEST_CASE.AUTOMATED_TEST_TECHNOLOGY,
                        TEST_CASE.SCM_REPOSITORY_ID,
                        TEST_CASE.DRAFTED_BY_AI)
                .from(TEST_CASE_LIBRARY_NODE)
                .innerJoin(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(SCRIPTED_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                .leftJoin(KEYWORD_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                .leftJoin(EXPLORATORY_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(testCaseId))
                .fetchOne()
                .into(TestCaseDto.class);
    }

    @Override
    public TestCaseLibraryDto getTestCaseLibraryDtoById(Long id) {
        return dsl.select(
                        TEST_CASE_LIBRARY.TCL_ID.as(RequestAliasesConstants.ID),
                        TEST_CASE_LIBRARY.ATTACHMENT_LIST_ID,
                        PROJECT.PROJECT_ID,
                        PROJECT.DESCRIPTION,
                        PROJECT.NAME)
                .from(TEST_CASE_LIBRARY)
                .innerJoin(PROJECT)
                .on(PROJECT.TCL_ID.eq(TEST_CASE_LIBRARY.TCL_ID))
                .where(TEST_CASE_LIBRARY.TCL_ID.eq(id))
                .fetchOne()
                .into(TestCaseLibraryDto.class);
    }

    @Override
    public List<CalledTestCaseDto> getCallingTestCaseById(Long id) {
        return dsl.select(
                        PROJECT.NAME.as(RequestAliasesConstants.PROJECT_NAME),
                        TEST_CASE.REFERENCE,
                        TEST_CASE.TCLN_ID.as(RequestAliasesConstants.ID),
                        TEST_CASE_LIBRARY_NODE.NAME,
                        DATASET.NAME.as(RequestAliasesConstants.DATASET_NAME),
                        TEST_CASE_STEPS.STEP_ORDER,
                        CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES.as(HAS_DELEGATED_PARAMETER))
                .from(TEST_CASE_LIBRARY_NODE)
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(TEST_CASE_STEPS)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE_STEPS.TEST_CASE_ID))
                .innerJoin(CALL_TEST_STEP)
                .on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
                .leftJoin(DATASET)
                .on(CALL_TEST_STEP.CALLED_DATASET.eq(DATASET.DATASET_ID))
                .where(CALL_TEST_STEP.CALLED_TEST_CASE_ID.eq(id))
                .fetchInto(CalledTestCaseDto.class);
    }

    @Override
    public TestCaseFolderDto getTestCaseFolderDtoById(long testCaseFolderId) {
        return dsl.select(
                        TEST_CASE_LIBRARY_NODE.TCLN_ID.as(RequestAliasesConstants.ID),
                        TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID,
                        TEST_CASE_LIBRARY_NODE.DESCRIPTION,
                        TEST_CASE_LIBRARY_NODE.NAME,
                        TEST_CASE_LIBRARY_NODE.PROJECT_ID)
                .from(TEST_CASE_LIBRARY_NODE)
                .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(testCaseFolderId))
                .fetchOne()
                .into(TestCaseFolderDto.class);
    }

    @Override
    public Long getTestCaseIdByTestStepId(Long testStepId) {
        return dsl.select(TEST_CASE_LIBRARY_NODE.TCLN_ID)
                .from(TEST_CASE_LIBRARY_NODE)
                .innerJoin(TEST_CASE_STEPS)
                .on(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .where(TEST_CASE_STEPS.STEP_ID.eq(testStepId))
                .fetchOne(TEST_CASE_LIBRARY_NODE.TCLN_ID);
    }

    @Override
    public String getLastExecutionStatus(Long testCaseId, String executionMode) {
        Result<Record1<String>> resultStatus;

        if (TestCaseExecutionMode.EXPLORATORY.name().equals(executionMode)) {
            resultStatus =
                    dsl.select(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS)
                            .from(ITERATION_TEST_PLAN_ITEM)
                            .leftJoin(EXPLORATORY_SESSION_OVERVIEW)
                            .on(
                                    EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                            ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                            .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(testCaseId))
                            .and(
                                    ITERATION_TEST_PLAN_ITEM.LAST_MODIFIED_ON.eq(
                                            DSL.select(DSL.max(ITERATION_TEST_PLAN_ITEM.LAST_MODIFIED_ON))
                                                    .from(ITERATION_TEST_PLAN_ITEM)
                                                    .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(testCaseId))))
                            .fetch();
        } else {
            resultStatus =
                    dsl.select(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS)
                            .from(ITERATION_TEST_PLAN_ITEM)
                            .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(testCaseId))
                            .and(
                                    ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON.eq(
                                            DSL.select(DSL.max(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON))
                                                    .from(ITERATION_TEST_PLAN_ITEM)
                                                    .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(testCaseId))))
                            .fetch();
        }

        if (resultStatus.isEmpty()) {
            return "";
        } else {
            return resultStatus.get(0).value1();
        }
    }

    private Field<String> getTestCaseKindSelectField() {
        return DSL.when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.GHERKIN.toString())
                .when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.KEYWORD.toString())
                .when(EXPLORATORY_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.EXPLORATORY.toString())
                .otherwise(TestCaseKind.STANDARD.toString())
                .as(KIND);
    }

    @Override
    public String getTestCaseKindFromItpiId(Long itemId) {
        Result<Record1<String>> resultStatus =
                dsl.select(getTestCaseKindSelectField())
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .innerJoin(TEST_CASE)
                        .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                        .leftJoin(SCRIPTED_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                        .leftJoin(KEYWORD_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                        .leftJoin(EXPLORATORY_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                        .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(itemId))
                        .groupBy(
                                ITERATION_TEST_PLAN_ITEM.TCLN_ID,
                                SCRIPTED_TEST_CASE.TCLN_ID,
                                KEYWORD_TEST_CASE.TCLN_ID,
                                EXPLORATORY_TEST_CASE.TCLN_ID)
                        .fetch();

        return resultStatus.get(0).value1();
    }

    @Override
    public String getTestCaseKindFromTpiId(long testPlanItemId) {
        Result<Record1<String>> resultStatus =
                dsl.select(getTestCaseKindSelectField())
                        .from(TEST_PLAN_ITEM)
                        .innerJoin(TEST_CASE)
                        .on(TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                        .leftJoin(SCRIPTED_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                        .leftJoin(KEYWORD_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                        .leftJoin(EXPLORATORY_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                        .where(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(testPlanItemId))
                        .groupBy(
                                TEST_PLAN_ITEM.TCLN_ID,
                                SCRIPTED_TEST_CASE.TCLN_ID,
                                KEYWORD_TEST_CASE.TCLN_ID,
                                EXPLORATORY_TEST_CASE.TCLN_ID)
                        .fetch();

        return resultStatus.get(0).value1();
    }
}
