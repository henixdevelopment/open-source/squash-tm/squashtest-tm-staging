/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.List;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestPlanOwner;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.exception.execution.EmptyTestSuiteTestPlanException;
import org.squashtest.tm.exception.execution.TestPlanItemNotExecutableException;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

/**
 * Interface for service responsible to process execution of a list of {@link IterationTestPlanItem}
 *
 * @param <E> class of the entity owning the test plan. Use for injection differentiation.
 * @author aguilhem
 */
public interface TestPlanExecutionProcessingService<T extends TestPlanResume> {

    /**
     * Should start a new execution for the next executable test plan item of the given test plan
     * owner. Or should return the execution to resume or null if :
     *
     * <ul>
     *   <li>all terminated, or
     *   <li>no execution-step on executions, or
     *   <li>no execution and test-case deleted
     * </ul>
     *
     * @param testPlanOwnerId id of the {@link TestPlanOwner}
     * @param testPlanItemId id of the {@link IterationTestPlanItem}
     * @return the execution to resume/restart or null if there is none
     */
    T startResumeNextExecution(long testPlanOwnerId, long testPlanItemId);

    /**
     * Resumes execution of the {@link TestPlanOwner} of given ID limiting resumption to the partial
     * test plan defined by the given filters.
     *
     * @param testPlanOwnerId id of the {@link TestPlanOwner}
     * @param filters List of {@link GridFilterValue} defining partial test plan of the {@link
     *     TestPlanOwner}
     * @return
     */
    TestPlanResume resumeWithFilteredTestPlan(long testPlanOwnerId, List<GridFilterValue> filters);

    /**
     * Resumes execution of the given partial test plan.
     *
     * @param testPlanOwnerId id of the {@link TestPlanOwner}
     * @param testPlanItemId id of the {@link IterationTestPlanItem}
     * @param partialTestPlanItemIds ids of the {@link IterationTestPlanItem} in the partial test plan
     */
    TestPlanResume resumeNextExecutionOfFilteredTestPlan(
            long testPlanOwnerId, long testPlanItemId, List<Long> partialTestPlanItemIds);

    /**
     * returns the execution were to resume the test plan <br>
     * or throw a @link{@linkplain EmptyTestSuiteTestPlanException} or {@link
     * org.squashtest.tm.exception.execution.EmptyIterationTestPlanException} or {@linkplain
     * TestPlanItemNotExecutableException} if no execution is to be resumed because :
     *
     * <ul>
     *   <li>all terminated, or
     *   <li>no execution-step on executions, or
     *   <li>no execution and test-case deleted
     * </ul>
     *
     * <p>if there is no execution should start a new execution for the given test plan owner, ie
     * create an execution for the first test case of this owner's test plan
     *
     * @param testPlanOwnerId id of the {@link TestPlanOwner}
     * @return the {@linkplain Execution} where to resume or null
     * @throws EmptyTestSuiteTestPlanException thrown if {@link TestPlanOwner} is a {@link
     *     org.squashtest.tm.domain.campaign.TestSuite} with an empty test plan
     * @throws org.squashtest.tm.exception.execution.EmptyIterationTestPlanException thrown if {@link
     *     TestPlanOwner} is an {@link org.squashtest.tm.domain.campaign.Iteration} with an empty test
     *     plan
     * @throws {@link TestPlanItemNotExecutableException}
     */
    T startResume(long testPlanOwnerId);

    /**
     * DELETE all executions and relaunch test plan returns the execution were to resume the test plan
     * <br>
     * or throw a @link{@linkplain EmptyTestSuiteTestPlanException} or {@link
     * org.squashtest.tm.exception.execution.EmptyIterationTestPlanException} or {@linkplain
     * TestPlanItemNotExecutableException} if no execution is to be resumed because :
     *
     * <ul>
     *   <li>all terminated, or
     *   <li>no execution-step on executions, or
     *   <li>no execution and test-case deleted
     * </ul>
     *
     * <p>if there is no execution should start a new execution for the given test plan owner, ie
     * create an execution for the first test case of this owner's test plan
     *
     * @param testPlanOwnerId id of the {@link TestPlanOwner}
     * @return the {@linkplain Execution} where to resume or null
     * @throws EmptyTestSuiteTestPlanException thrown if {@link TestPlanOwner} is a {@link
     *     org.squashtest.tm.domain.campaign.TestSuite} with an empty test plan
     * @throws org.squashtest.tm.exception.execution.EmptyIterationTestPlanException thrown if {@link
     *     TestPlanOwner} is an {@link org.squashtest.tm.domain.campaign.Iteration} with an empty test
     *     plan
     * @throws {@link TestPlanItemNotExecutableException}
     */
    T relaunch(long testPlanOwnerId);

    /**
     * Same as {@link #relaunch(long)} but with a filtered test plan defined by the given filters.
     *
     * @param filters List of {@link GridFilterValue} to filter the test plan
     */
    TestPlanResume relaunchFilteredTestPlan(long testPlanOwnerId, List<GridFilterValue> filters);

    /**
     * Issue 7366 Method which tests if a {@link TestPlanOwner} has at least one deleted test case in
     * its test plan
     *
     * @param testPlanOwnerId id of the {@link TestPlanOwner}
     * @return true if test suite has at least one deleted test case in its test plan
     */
    boolean hasDeletedTestCaseInTestPlan(long testPlanOwnerId);
}
