/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static org.squashtest.tm.domain.EntityType.ITERATION;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.IterationTestPlanGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.TestSuiteTestPlanGrid;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testautomation.AutomatedSuitePreviewService;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview;
import org.squashtest.tm.service.user.UserAccountService;

@Service
public class AutomatedSuitePreviewServiceImpl implements AutomatedSuitePreviewService {

    private final DSLContext dslContext;
    private final AutomatedSuiteDao autoSuiteDao;
    private final UserAccountService userAccountService;
    private final PermissionEvaluationService permissionService;
    private final IterationDisplayDao iterationDisplayDao;
    private final TestSuiteDisplayDao testSuiteDisplayDao;

    public AutomatedSuitePreviewServiceImpl(
            DSLContext dslContext,
            AutomatedSuiteDao autoSuiteDao,
            UserAccountService userAccountService,
            PermissionEvaluationService permissionService,
            IterationDisplayDao iterationDisplayDao,
            TestSuiteDisplayDao testSuiteDisplayDao) {
        this.dslContext = dslContext;
        this.autoSuiteDao = autoSuiteDao;
        this.userAccountService = userAccountService;
        this.permissionService = permissionService;
        this.iterationDisplayDao = iterationDisplayDao;
        this.testSuiteDisplayDao = testSuiteDisplayDao;
    }

    @Override
    // security delegated to permission utils
    public AutomatedSuitePreview preview(AutomatedSuiteCreationSpecification specification) {

        // first, validate
        specification.validate();

        // check permission
        checkPermission(specification);

        EntityReference contextEntity = specification.getContext();
        List<Long> testPlanSubset = specification.getTestPlanSubsetIds();

        // if filters are specified and testPlanSubsetIds empty, fetch the TestPlanSubsetIds with the
        // filters
        if (!specification.getFilterValues().isEmpty() && testPlanSubset.isEmpty()) {
            GridRequest gridRequest = prepareNonPaginatedGridRequest(specification);
            Long contextId = contextEntity.getId();
            AbstractGrid testPlanGrid =
                    switch (contextEntity.getType()) {
                        case ITERATION ->
                                new IterationTestPlanGrid(contextId, null, iterationDisplayDao, permissionService);
                        case TEST_SUITE -> new TestSuiteTestPlanGrid(contextId, null, testSuiteDisplayDao);
                        default ->
                                throw new IllegalArgumentException(
                                        "Type " + contextEntity.getType() + " is not supported.");
                    };
            GridResponse gridResponse = testPlanGrid.getRows(gridRequest, this.dslContext);
            specification.setTestPlanSubsetIds(extractIdsFromGridResponse(gridResponse));
        }

        // fetch
        Long userId = getUserIfPermissionsOnIterationInsufficient(contextEntity);
        List<Couple<TestAutomationProject, Long>> projects =
                autoSuiteDao.findAllCalledByTestPlan(contextEntity, testPlanSubset, userId);

        boolean needManualServerSelection =
                projects.stream().anyMatch(couple -> couple.getA1().getServer().isManualSlaveSelection());

        // manual server selection is also required if at least one squashOrchestrator test is present
        if (!needManualServerSelection) {
            needManualServerSelection =
                    autoSuiteDao.itpiSelectionContainsSquashAutomTest(contextEntity, testPlanSubset);
        }

        // fetch List of SquashAutomProjectPreviews, note that test case list on project may be empty
        Map<Long, AutomatedSuitePreview.SquashAutomProjectPreview> squashAutomServersMap =
                autoSuiteDao.findAllSquashAutomProjectPreviews(contextEntity, testPlanSubset, userId);

        // filter out projects with no test-cases
        squashAutomServersMap = filterOutProjectsWithNoTestCases(squashAutomServersMap);

        // create the response
        AutomatedSuitePreview preview = new AutomatedSuitePreview();
        preview.setSpecification(specification);

        Collection<AutomatedSuitePreview.TestAutomationProjectPreview> projectPreview =
                projects.stream()
                        .map(
                                couple -> {
                                    TestAutomationProject taProject = couple.getA1();
                                    Long testCount = couple.getA2();
                                    return new AutomatedSuitePreview.TestAutomationProjectPreview(
                                            taProject.getId(),
                                            taProject.getLabel(),
                                            taProject.getServer().getName(),
                                            taProject.getSlaves(),
                                            testCount);
                                })
                        .toList();

        preview.setManualServerSelection(needManualServerSelection);
        preview.setProjects(projectPreview);
        preview.setSquashAutomProjects(squashAutomServersMap.values());
        return preview;
    }

    private GridRequest prepareNonPaginatedGridRequest(
            AutomatedSuiteCreationSpecification specification) {
        GridRequest gridRequest = new GridRequest();
        gridRequest.setFilterValues(specification.getFilterValues());
        return gridRequest.toNonPaginatedRequest();
    }

    private List<Long> extractIdsFromGridResponse(GridResponse gridResponse) {
        return gridResponse.getDataRows().stream().map(DataRow::getId).map(Long::valueOf).toList();
    }

    private Long getUserIfPermissionsOnIterationInsufficient(EntityReference contextEntity) {
        Long userId = null;
        if (contextEntity.getType().equals(EntityType.ITERATION)
                && !currentUserCanReadUnassigned(contextEntity.getId())) {
            userId = userAccountService.findCurrentUser().getId();
        }
        return userId;
    }

    private boolean currentUserCanReadUnassigned(Long iterationId) {
        return permissionService.hasRoleOrPermissionOnObject(
                Roles.ROLE_ADMIN,
                Permissions.READ_UNASSIGNED.name(),
                iterationId,
                Iteration.SIMPLE_CLASS_NAME);
    }

    private Map<Long, AutomatedSuitePreview.SquashAutomProjectPreview>
            filterOutProjectsWithNoTestCases(
                    Map<Long, AutomatedSuitePreview.SquashAutomProjectPreview> squashAutomServersMap) {
        return squashAutomServersMap.entrySet().stream()
                .filter(
                        longSquashAutomProjectPreviewEntry ->
                                !longSquashAutomProjectPreviewEntry.getValue().getTestCases().isEmpty())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    // assumes that the specification was validated first
    private void checkPermission(AutomatedSuiteCreationSpecification specification) {
        List<Long> singleId = new ArrayList<>();
        singleId.add(specification.getContext().getId());

        Class<?> clazz =
                (specification.getContext().getType() == ITERATION) ? Iteration.class : TestSuite.class;
        PermissionsUtils.checkPermission(
                permissionService, singleId, Permissions.EXECUTE.name(), clazz.getName());
    }

    @Override
    // security delegated to permission utils
    public List<String> findTestListPreview(
            AutomatedSuiteCreationSpecification specification, long automatedProjectId) {
        specification.validate();
        checkPermission(specification);
        Long userId = getUserIfPermissionsOnIterationInsufficient(specification.getContext());
        return autoSuiteDao.findTestPathForAutomatedSuiteAndProject(
                specification.getContext(),
                specification.getTestPlanSubsetIds(),
                automatedProjectId,
                userId);
    }
}
