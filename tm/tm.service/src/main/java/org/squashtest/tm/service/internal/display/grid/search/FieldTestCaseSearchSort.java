/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.search;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_TEST;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PARAMETER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.Level;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.service.internal.display.grid.GridSort;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

public class FieldTestCaseSearchSort<T> {
    private static final Pattern CUF_GRID_PROPERTY = Pattern.compile("cuf|(\\d+)");

    private final Field<T> aliasField;
    private final GridSort.SortDirection sortDirection;

    public FieldTestCaseSearchSort(Field<T> aliasField, GridSort.SortDirection sortDirection) {
        this.aliasField = aliasField;
        this.sortDirection = sortDirection;
    }

    public Field<T> getAliasField() {
        return aliasField;
    }

    public SortField<T> getSortField() {
        return getSortField(aliasField);
    }

    private static String getNormalizeFieldName(String camelCase) {
        Converter<String, String> converter =
                CaseFormat.LOWER_CAMEL.converterTo(CaseFormat.UPPER_UNDERSCORE);
        return converter.convert(camelCase);
    }

    public static FieldTestCaseSearchSort<?> getTestCaseSelectSearch(GridSort gridSort) {
        FieldTestCaseSearchSort<?> fieldTestCaseSearchSort;
        String name = getNormalizeFieldName(gridSort.getProperty());
        Matcher cufMatcher = CUF_GRID_PROPERTY.matcher(name);
        // if a cuf is found, we need to extract the cuf id, so the field cannot be static
        if (cufMatcher.find()) {
            Field<?> fieldCuf = getFieldCuf(Long.parseLong(cufMatcher.group(1)), name);
            fieldTestCaseSearchSort = new FieldTestCaseSearchSort<>(fieldCuf, gridSort.getDirection());
        } else {
            TestCaseSortField testCaseSortField =
                    Stream.of(TestCaseSortField.values())
                            .filter(testCaseSelectSearch -> testCaseSelectSearch.name().equalsIgnoreCase(name))
                            .findAny()
                            .orElseThrow(
                                    () -> new IllegalArgumentException("No such field with the property: " + name));
            fieldTestCaseSearchSort =
                    new FieldTestCaseSearchSort<>(testCaseSortField.getField(), gridSort.getDirection());
        }
        return fieldTestCaseSearchSort;
    }

    private SortField<T> getSortField(Field<T> field) {
        return sortDirection == GridSort.SortDirection.ASC ? field.asc() : field.desc();
    }

    // Fields generation
    private static Field<?> getFieldCuf(Long cufId, String alias) {
        return DSL.select(
                        when(
                                        CUSTOM_FIELD_VALUE.FIELD_TYPE.eq(String.valueOf(InputType.TAG)),
                                        groupConcat(CUSTOM_FIELD_VALUE_OPTION.LABEL).separator(", "))
                                .otherwise(CUSTOM_FIELD_VALUE.VALUE))
                .from(CUSTOM_FIELD_VALUE)
                .leftOuterJoin(CUSTOM_FIELD_VALUE_OPTION)
                .on(CUSTOM_FIELD_VALUE.CFV_ID.eq(CUSTOM_FIELD_VALUE_OPTION.CFV_ID))
                .where(CUSTOM_FIELD_VALUE.CF_ID.eq(cufId))
                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(BindableEntity.TEST_CASE.name()))
                .groupBy(CUSTOM_FIELD_VALUE.FIELD_TYPE, CUSTOM_FIELD_VALUE.VALUE)
                .asField(alias);
    }

    private enum TestCaseSortField {
        PROJECT_NAME(PROJECT.NAME),
        ID(TEST_CASE_LIBRARY_NODE.TCLN_ID),
        NAME(TEST_CASE_LIBRARY_NODE.NAME),
        REFERENCE(TEST_CASE.REFERENCE),
        STATUS(getFieldStatus()),
        IMPORTANCE(getFieldImportance()),
        NATURE(getFieldNature()),
        TYPE(getFieldType()),
        KIND(getFieldKind()),
        DESCRIPTION(TEST_CASE_LIBRARY_NODE.DESCRIPTION),
        AUTOMATABLE(getFieldAutomatable()),
        AUTOMATION_PRIORITY(AUTOMATION_REQUEST.AUTOMATION_PRIORITY),
        TRANSMITTED_ON(AUTOMATION_REQUEST.TRANSMITTED_ON),
        REQUEST_STATUS(getFieldRequestStatus()),
        HAS_AUTO_SCRIPT(getFieldHasAutoScript()),
        AUTOMATED_TEST_TECHNOLOGY(getFieldAutomatedTestTechnology()),
        HAS_BOUND_SCM_REPOSITORY(getFieldHasBoundScmRepository()),
        HAS_BOUND_AUTOMATED_TEST_REFERENCE(getFieldHasBoundAutomatedTestReference()),
        CREATED_ON(TEST_CASE_LIBRARY_NODE.CREATED_ON),
        CREATED_BY(TEST_CASE_LIBRARY_NODE.CREATED_BY),
        LAST_MODIFIED_ON(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_ON),
        LAST_MODIFIED_BY(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY),
        DRAFTED_BY_AI(TEST_CASE.DRAFTED_BY_AI),
        MILESTONE_LABELS(getFieldMilestoneLabel()),
        MILESTONE_STATUS(getFieldMilestoneStatus()),
        MILESTONE_END_DATE(getFieldMilestoneEndDate()),
        REQ_MILESTONE_LOCKED(getFieldMilestoneReqLocked()),
        ITERATION_LIST(getFieldIterationList()),
        TEST_SUITE_LIST(getFieldTestSuiteList()),
        CAMPAIGN_LIST(getFieldCampaignList()),
        TC_MILESTONE_LOCKED(getFieldTcMilestoneLocked()),

        // Count
        ATTACHMENTS(getFieldCountAttachement()),
        COVERAGES(getFieldCountCoverage()),
        STEPS(getFieldCountStep()),
        PARAM_COUNT(getFieldCountParameter()),
        DATASETS(getFieldCountDataset()),
        ITERATIONS(getFieldCountIteration()),
        EXECUTION_COUNT(getFieldCountExecution()),
        ISSUE_COUNT(getFieldCountIssue()),
        MILESTONES(getFieldCountMilestone()),
        ;

        private final Field<?> selectField;

        TestCaseSortField(Field<?> selectField) {
            this.selectField = selectField.as(this.name());
        }

        public Field<?> getField() {
            return selectField;
        }

        private static Field<String> getFieldNature() {
            return DSL.select(INFO_LIST_ITEM.CODE)
                    .from(INFO_LIST_ITEM)
                    .where(INFO_LIST_ITEM.ITEM_ID.eq(TEST_CASE.TC_NATURE))
                    .asField();
        }

        private static Field<String> getFieldType() {
            return DSL.select(INFO_LIST_ITEM.CODE)
                    .from(INFO_LIST_ITEM)
                    .where(INFO_LIST_ITEM.ITEM_ID.eq(TEST_CASE.TC_TYPE))
                    .asField();
        }

        private static Field<Integer> getFieldKind() {
            return DSL.case_()
                    .when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), 2)
                    .when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), 3)
                    .when(EXPLORATORY_TEST_CASE.TCLN_ID.isNotNull(), 4)
                    .when(TEST_CASE.TCLN_ID.isNotNull(), 1);
        }

        private static Field<Integer> getFieldHasAutoScript() {
            return DSL.case_().when(AUTOMATED_TEST.TEST_ID.isNotNull(), 2).otherwise(1);
        }

        private static Field<Integer> getFieldHasBoundScmRepository() {
            return DSL.case_().when(TEST_CASE.SCM_REPOSITORY_ID.isNotNull(), 2).otherwise(1);
        }

        private static Field<Integer> getFieldHasBoundAutomatedTestReference() {
            return DSL.case_().when(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull(), 2).otherwise(1);
        }

        private static Field<Integer> getFieldAutomatedTestTechnology() {
            return DSL.select(Tables.AUTOMATED_TEST_TECHNOLOGY.NAME)
                    .from(Tables.AUTOMATED_TEST_TECHNOLOGY)
                    .where(
                            Tables.AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID.eq(
                                    TEST_CASE.AUTOMATED_TEST_TECHNOLOGY))
                    .asField();
        }

        private static Field<?> getFieldMilestoneLabel() {
            Field<String> milestoneLabel =
                    DSL.select(MILESTONE.LABEL)
                            .from(MILESTONE_TEST_CASE)
                            .join(MILESTONE)
                            .on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                            .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                            .asField();
            return groupConcat(milestoneLabel)
                    .separator((", "))
                    .as(RequestAliasesConstants.MILESTONE_LABELS);
        }

        private static Field<?> getFieldMilestoneStatus() {
            Field<String> milestoneStatus =
                    DSL.select(MILESTONE.STATUS)
                            .from(MILESTONE_TEST_CASE)
                            .join(MILESTONE)
                            .on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                            .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                            .asField();
            return groupConcat(milestoneStatus)
                    .separator((", "))
                    .as(RequestAliasesConstants.MILESTONE_STATUS);
        }

        private static Field<?> getFieldMilestoneEndDate() {
            Field<String> milestoneEndDate =
                    DSL.select(MILESTONE.END_DATE)
                            .from(MILESTONE_TEST_CASE)
                            .join(MILESTONE)
                            .on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                            .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                            .asField();
            return groupConcat(milestoneEndDate)
                    .separator((", "))
                    .as(RequestAliasesConstants.MILESTONE_END_DATE);
        }

        private static Field<?> getFieldMilestoneReqLocked() {
            return DSL.select(countDistinct(MILESTONE.MILESTONE_ID))
                    .from(REQUIREMENT_VERSION_COVERAGE)
                    .join(MILESTONE_REQ_VERSION)
                    .on(
                            REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(
                                    MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                    .join(MILESTONE)
                    .on(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                    .where(
                            REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(
                                    TEST_CASE_LIBRARY_NODE.TCLN_ID))
                    .and(MILESTONE.STATUS.eq("LOCKED"))
                    .asField();
        }

        private static Field<?> getFieldIterationList() {
            return DSL.select(DSL.groupConcatDistinct(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                    .from(ITERATION_TEST_PLAN_ITEM)
                    .join(ITEM_TEST_PLAN_LIST)
                    .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                    .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                    .asField();
        }

        private static Field<?> getFieldTestSuiteList() {
            return DSL.select(DSL.groupConcatDistinct(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID))
                    .from(ITERATION_TEST_PLAN_ITEM)
                    .join(TEST_SUITE_TEST_PLAN_ITEM)
                    .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                    .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                    .asField();
        }

        private static Field<?> getFieldCampaignList() {
            return DSL.select(DSL.groupConcatDistinct(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID))
                    .from(CAMPAIGN_TEST_PLAN_ITEM)
                    .where(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                    .asField();
        }

        private static Field<?> getFieldTcMilestoneLocked() {
            return DSL.select(countDistinct(MILESTONE.MILESTONE_ID))
                    .from(MILESTONE_TEST_CASE)
                    .join(MILESTONE)
                    .on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                    .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                    .and(MILESTONE.STATUS.eq("LOCKED"))
                    .asField();
        }

        private static Field<Integer> getFieldCountAttachement() {
            return DSL.select(DSL.countDistinct(ATTACHMENT.ATTACHMENT_ID))
                    .from(ATTACHMENT)
                    .where(ATTACHMENT.ATTACHMENT_LIST_ID.eq(TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID))
                    .asField();
        }

        private static Field<Integer> getFieldCountCoverage() {
            return DSL.select(DSL.countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                    .from(REQUIREMENT_VERSION_COVERAGE)
                    .where(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                    .asField();
        }

        private static Field<Integer> getFieldCountStep() {
            return DSL.select(DSL.countDistinct(TEST_CASE_STEPS.STEP_ID))
                    .from(TEST_CASE_STEPS)
                    .where(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                    .asField();
        }

        private static Field<Integer> getFieldCountParameter() {
            return DSL.select(DSL.countDistinct(PARAMETER.PARAM_ID))
                    .from(PARAMETER)
                    .where(PARAMETER.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                    .asField();
        }

        private static Field<Integer> getFieldCountDataset() {
            return DSL.select(DSL.countDistinct(DATASET.DATASET_ID))
                    .from(DATASET)
                    .where(DATASET.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                    .asField();
        }

        private static Field<Integer> getFieldCountIteration() {
            return DSL.select(DSL.countDistinct(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                    .from(ITERATION_TEST_PLAN_ITEM)
                    .join(ITEM_TEST_PLAN_LIST)
                    .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                    .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                    .asField();
        }

        private static Field<Integer> getFieldCountExecution() {
            return DSL.select(DSL.countDistinct(EXECUTION.EXECUTION_ID))
                    .from(EXECUTION)
                    .where(EXECUTION.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                    .asField();
        }

        private static Field<Integer> getFieldCountIssue() {
            return DSL.select(DSL.countDistinct(ISSUE.REMOTE_ISSUE_ID))
                    .from(ISSUE)
                    .join(EXECUTION_ISSUES_CLOSURE)
                    .on(EXECUTION_ISSUES_CLOSURE.ISSUE_ID.eq(ISSUE.ISSUE_ID))
                    .join(EXECUTION)
                    .on(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                    .where(EXECUTION.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                    .asField();
        }

        private static Field<?> getFieldCountMilestone() {
            return DSL.select(DSL.countDistinct(MILESTONE_TEST_CASE.MILESTONE_ID))
                    .from(MILESTONE_TEST_CASE)
                    .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                    .asField();
        }

        private static Field<Integer> getFieldStatus() {
            Map<String, Integer> tcStatusMap =
                    Arrays.stream(TestCaseStatus.values())
                            .collect(
                                    Collectors.toMap(Enum::name, Level::getLevel, (a, b) -> a, LinkedHashMap::new));
            return DSL.case_(TEST_CASE.TC_STATUS).mapValues(tcStatusMap);
        }

        private static Field<Integer> getFieldImportance() {
            Map<String, Integer> tcImportanceMap =
                    Arrays.stream(TestCaseImportance.values())
                            .collect(
                                    Collectors.toMap(Enum::name, Level::getLevel, (a, b) -> a, LinkedHashMap::new));
            return DSL.case_(TEST_CASE.IMPORTANCE).mapValues(tcImportanceMap);
        }

        private static Field<Integer> getFieldAutomatable() {
            Map<String, Integer> tcAutomatableMap =
                    Arrays.stream(TestCaseAutomatable.values())
                            .collect(
                                    Collectors.toMap(Enum::name, Level::getLevel, (a, b) -> a, LinkedHashMap::new));
            return DSL.case_(TEST_CASE.AUTOMATABLE).mapValues(tcAutomatableMap);
        }

        private static Field<Integer> getFieldRequestStatus() {
            Map<String, Integer> automationRequestStatusMap =
                    Arrays.stream(AutomationRequestStatus.values())
                            .collect(
                                    Collectors.toMap(Enum::name, Level::getLevel, (a, b) -> a, LinkedHashMap::new));
            return DSL.case_(AUTOMATION_REQUEST.REQUEST_STATUS).mapValues(automationRequestStatusMap);
        }
    }
}
