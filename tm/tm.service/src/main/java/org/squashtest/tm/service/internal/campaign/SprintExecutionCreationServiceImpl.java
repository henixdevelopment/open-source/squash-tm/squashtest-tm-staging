/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.EXECUTE_TPI_OR_ROLE_ADMIN;

import java.util.Locale;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.ExecutionVisitor;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.KeywordExecution;
import org.squashtest.tm.domain.execution.ScriptedExecution;
import org.squashtest.tm.domain.testcase.ConsumerForScriptedTestCaseVisitor;
import org.squashtest.tm.domain.testcase.CreateExecutionFromTestCaseVisitor;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.exception.execution.ExecutionHasNoStepsException;
import org.squashtest.tm.exception.execution.TestPlanItemNotExecutableException;
import org.squashtest.tm.exception.execution.TestSuiteTestPlanHasDeletedTestCaseException;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.campaign.SprintExecutionCreationService;
import org.squashtest.tm.service.internal.campaign.scripted.ScriptedTestCaseExecutionHelper;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.denormalizedfield.PrivateDenormalizedFieldValueService;
import org.squashtest.tm.service.internal.repository.TestPlanItemDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestPlanExploratorySessionOverviewDisplayDao;
import org.squashtest.tm.service.testcase.TestCaseCyclicCallChecker;

@Service
@Transactional
public class SprintExecutionCreationServiceImpl implements SprintExecutionCreationService {

    private final MessageSource messageSource;

    private final AttachmentManagerService attachmentManagerService;

    private final TestCaseCyclicCallChecker testCaseCyclicCallChecker;

    private final PrivateCustomFieldValueService customFieldValueService;

    private final ScriptedTestCaseExecutionHelper scriptedTestCaseExecutionHelper;

    private final PrivateDenormalizedFieldValueService denormalizedFieldValueService;

    private final TestPlanItemDao testPlanItemDao;

    private final ProjectDisplayDao projectDisplayDao;

    private final TestPlanExploratorySessionOverviewDisplayDao
            testPlanExploratorySessionOverviewDisplayDao;

    private final SprintDisplayDao sprintDisplayDao;

    public SprintExecutionCreationServiceImpl(
            MessageSource messageSource,
            AttachmentManagerService attachmentManagerService,
            TestCaseCyclicCallChecker testCaseCyclicCallChecker,
            PrivateCustomFieldValueService customFieldValueService,
            ScriptedTestCaseExecutionHelper scriptedTestCaseExecutionHelper,
            PrivateDenormalizedFieldValueService denormalizedFieldValueService,
            TestPlanItemDao testPlanItemDao,
            ProjectDisplayDao projectDisplayDao,
            TestPlanExploratorySessionOverviewDisplayDao testPlanExploratorySessionOverviewDisplayDao,
            SprintDisplayDao sprintDisplayDao) {
        this.messageSource = messageSource;
        this.attachmentManagerService = attachmentManagerService;
        this.testCaseCyclicCallChecker = testCaseCyclicCallChecker;
        this.customFieldValueService = customFieldValueService;
        this.scriptedTestCaseExecutionHelper = scriptedTestCaseExecutionHelper;
        this.denormalizedFieldValueService = denormalizedFieldValueService;
        this.testPlanItemDao = testPlanItemDao;
        this.projectDisplayDao = projectDisplayDao;
        this.testPlanExploratorySessionOverviewDisplayDao =
                testPlanExploratorySessionOverviewDisplayDao;
        this.sprintDisplayDao = sprintDisplayDao;
    }

    @Override
    @PreAuthorize(EXECUTE_TPI_OR_ROLE_ADMIN)
    public Execution createExecution(long testPlanItemId) {
        checkParentSprintStatus(testPlanItemId);

        final TestPlanItem item =
                testPlanItemDao.findByIdWithReferencedTestCaseAndDataset(testPlanItemId);

        if (item.isTestCaseDeleted()) {
            throw new TestSuiteTestPlanHasDeletedTestCaseException();
        }

        final Locale locale =
                projectDisplayDao.findBddScriptLanguageByProjectId(item.getProject().getId()).getLocale();
        final Execution execution = createAndAddExecution(item, messageSource, locale);
        final boolean isExploratory =
                TestCaseExecutionMode.EXPLORATORY.equals(execution.getExecutionMode());

        if (!isExploratory && execution.getSteps().isEmpty()) {
            throw new ExecutionHasNoStepsException();
        }

        return execution;
    }

    private void checkParentSprintStatus(long testPlanItemId) {
        final SprintStatus sprintStatus =
                sprintDisplayDao.getSprintStatusByTestPlanItemId(testPlanItemId);
        if (sprintStatus == SprintStatus.CLOSED) {
            throw new SprintClosedException();
        }
    }

    private Execution createAndAddExecution(
            TestPlanItem item, MessageSource messageSource, Locale locale)
            throws TestPlanItemNotExecutableException {
        final Execution execution = createExec(item, messageSource, locale);
        item.addExecution(execution);
        operationsAfterAddingExec(execution);
        return execution;
    }

    private Execution createExec(TestPlanItem item, MessageSource messageSource, Locale locale) {
        final TestCase testCase = item.getReferencedTestCase();
        testCaseCyclicCallChecker.checkNoCyclicCall(testCase);
        return createExecution(item, messageSource, locale);
    }

    private Execution createExecution(TestPlanItem item, MessageSource messageSource, Locale locale) {
        final CreateExecutionFromTestCaseVisitor createExecutionVisitor =
                new CreateExecutionFromTestCaseVisitor(item.getReferencedDataset(), messageSource, locale);
        item.getReferencedTestCase().accept(createExecutionVisitor);

        final Execution createdExecution = createExecutionVisitor.getCreatedExecution();
        createdExecution.setSprintTestPlanItem(item);
        copyAttachments(createdExecution);
        return createdExecution;
    }

    private void copyAttachments(Execution execution) {
        attachmentManagerService.copyContentsOnExternalRepository(execution);

        for (ExecutionStep executionStep : execution.getSteps()) {
            attachmentManagerService.copyContentsOnExternalRepository(executionStep);
        }
    }

    private void operationsAfterAddingExec(Execution execution) {
        execution.accept(new ExecutionPostCreationVisitor());
    }

    private final class ExecutionPostCreationVisitor implements ExecutionVisitor {
        @Override
        public void visit(Execution execution) {
            createCustomFieldsForExecutionAndExecutionSteps(execution);
            createDenormalizedFieldsForExecutionAndExecutionSteps(execution);
            createExecutionAttachments(execution);
        }

        @Override
        public void visit(ScriptedExecution scriptedExecution) {
            createCustomAndDenormalizedFieldsForExecution(scriptedExecution);
            createExecutionStepsForScriptedTestCase(scriptedExecution);
        }

        @Override
        public void visit(KeywordExecution keywordExecution) {
            createCustomAndDenormalizedFieldsForExecution(keywordExecution);
        }

        @Override
        public void visit(ExploratoryExecution exploratoryExecution) {
            createCustomAndDenormalizedFieldsForExecution(exploratoryExecution);
            final TestPlanExploratorySessionOverviewDisplayDao.NameAndReference overviewNameAndRef =
                    testPlanExploratorySessionOverviewDisplayDao.findNameAndReferenceByExecutionId(
                            exploratoryExecution.getId());
            updateExploratoryExecutionInfoFromSessionOverview(exploratoryExecution, overviewNameAndRef);
        }

        private void updateExploratoryExecutionInfoFromSessionOverview(
                Execution execution,
                TestPlanExploratorySessionOverviewDisplayDao.NameAndReference overviewNameAndRef) {
            execution.setName(buildExecutionName(overviewNameAndRef));
            execution.setReference(overviewNameAndRef.reference());
        }

        private String buildExecutionName(
                TestPlanExploratorySessionOverviewDisplayDao.NameAndReference overviewNameAndRef) {
            if (overviewNameAndRef.reference().isEmpty()) {
                return overviewNameAndRef.name();
            } else {
                return overviewNameAndRef.reference() + " - " + overviewNameAndRef.name();
            }
        }

        private void createExecutionStepsForScriptedTestCase(ScriptedExecution scriptedExecution) {
            scriptedExecution
                    .getReferencedTestCase()
                    .accept(
                            new ConsumerForScriptedTestCaseVisitor(
                                    scriptedTestCase ->
                                            scriptedTestCaseExecutionHelper.createExecutionStepsForScriptedTestCase(
                                                    scriptedExecution)));
        }

        private void createCustomFieldsForExecutionAndExecutionSteps(Execution execution) {
            customFieldValueService.createAllCustomFieldValues(execution, execution.getProject());
            customFieldValueService.createAllCustomFieldValues(
                    execution.getSteps(), execution.getProject());
        }

        private void createCustomAndDenormalizedFieldsForExecution(Execution execution) {
            customFieldValueService.createAllCustomFieldValues(execution, execution.getProject());
            createDenormalizedFieldsForExecution(execution);
        }

        private void createDenormalizedFieldsForExecutionAndExecutionSteps(Execution execution) {
            createDenormalizedFieldsForExecution(execution);
            denormalizedFieldValueService.createAllDenormalizedFieldValuesForSteps(execution);
        }

        private void createDenormalizedFieldsForExecution(Execution execution) {
            final TestCase referencedTestCase = execution.getReferencedTestCase();
            denormalizedFieldValueService.createAllDenormalizedFieldValues(referencedTestCase, execution);
        }

        private void createExecutionAttachments(Execution execution) {
            final String description = execution.getDescription();

            if (description == null || description.isEmpty()) {
                return;
            }

            String newDescription =
                    attachmentManagerService.handleRichTextAttachments(
                            description, execution.getAttachmentList());

            if (!newDescription.equals(description)) {
                execution.setDescription(newDescription);
            }
        }
    }
}
