/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign.export;

import static org.jooq.impl.DSL.groupConcat;
import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;

import com.google.common.collect.Lists;
import com.google.common.math.IntMath;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.inject.Inject;
import org.apache.commons.collections.map.MultiValueMap;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.TableField;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.jooq.domain.tables.Milestone;
import org.squashtest.tm.jooq.domain.tables.records.CallTestStepRecord;
import org.squashtest.tm.jooq.domain.tables.records.CoreUserRecord;
import org.squashtest.tm.jooq.domain.tables.records.DatasetRecord;
import org.squashtest.tm.jooq.domain.tables.records.ExecutionExecutionStepsRecord;
import org.squashtest.tm.jooq.domain.tables.records.ExecutionIssuesClosureRecord;
import org.squashtest.tm.jooq.domain.tables.records.ExecutionRecord;
import org.squashtest.tm.jooq.domain.tables.records.ExecutionStepRecord;
import org.squashtest.tm.jooq.domain.tables.records.InfoListItemRecord;
import org.squashtest.tm.jooq.domain.tables.records.IssueRecord;
import org.squashtest.tm.jooq.domain.tables.records.ItemTestPlanExecutionRecord;
import org.squashtest.tm.jooq.domain.tables.records.IterationRecord;
import org.squashtest.tm.jooq.domain.tables.records.IterationTestPlanItemRecord;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.jooq.domain.tables.records.RequirementVersionCoverageRecord;
import org.squashtest.tm.jooq.domain.tables.records.TestCaseLibraryNodeRecord;
import org.squashtest.tm.jooq.domain.tables.records.TestCaseRecord;
import org.squashtest.tm.jooq.domain.tables.records.TestCaseStepsRecord;
import org.squashtest.tm.jooq.domain.tables.records.TestSuiteRecord;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.internal.dto.CampaignDto;
import org.squashtest.tm.service.internal.dto.CustomFieldDto;
import org.squashtest.tm.service.internal.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.dto.ExecutionDto;
import org.squashtest.tm.service.internal.dto.ITPIDto;
import org.squashtest.tm.service.internal.dto.IterationDto;
import org.squashtest.tm.service.internal.dto.TestCaseDto;

/**
 * Abstract class for CampaignExportCSVModel. Mainly use to store JOOQ field shortcut...
 *
 * @author aguilhem
 */
public abstract class AbstractCampaignExportCSVModel implements WritableCampaignCSVModel {

    static final TableField<IterationRecord, Long> ITERATION_ID = ITERATION.ITERATION_ID;

    static final TableField<IterationRecord, String> ITERATION_NAME = ITERATION.NAME;

    static final TableField<IterationRecord, Timestamp> ITERATION_SCHEDULED_END_DATE =
            ITERATION.SCHEDULED_END_DATE;

    static final TableField<IterationRecord, Timestamp> ITERATION_SCHEDULED_START_DATE =
            ITERATION.SCHEDULED_START_DATE;

    static final TableField<IterationRecord, Timestamp> ITERATION_ACTUAL_END_DATE =
            ITERATION.ACTUAL_END_DATE;

    static final TableField<IterationRecord, Timestamp> ITERATION_ACTUAL_START_DATE =
            ITERATION.ACTUAL_START_DATE;

    static final TableField<IterationTestPlanItemRecord, Long> ITPI_ID =
            ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID;

    static final TableField<IterationTestPlanItemRecord, String> ITPI_STATUS =
            ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS;

    static final TableField<CoreUserRecord, String> USER_LOGIN = CORE_USER.LOGIN;

    static final TableField<IterationTestPlanItemRecord, Timestamp> ITPI_LAST_EXECUTED_ON =
            ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON;

    static final TableField<ItemTestPlanExecutionRecord, Long> ITPI_EXECUTION =
            ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID;

    static final TableField<DatasetRecord, String> DATASET_NAME = DATASET.NAME;

    static final TableField<TestCaseRecord, Long> TC_ID = TEST_CASE.TCLN_ID;

    static final TableField<TestCaseRecord, String> TC_IMPORTANCE = TEST_CASE.IMPORTANCE;

    static final TableField<TestCaseRecord, String> TC_REFERENCE = TEST_CASE.REFERENCE;

    static final TableField<InfoListItemRecord, String> TC_TYPE =
            INFO_LIST_ITEM.as("info_list_1").CODE;

    static final TableField<InfoListItemRecord, String> TC_NATURE =
            INFO_LIST_ITEM.as("info_list_2").CODE;

    static final TableField<TestCaseRecord, String> TC_STATUS = TEST_CASE.TC_STATUS;

    static final TableField<TestCaseRecord, String> TC_PREREQUISITE = TEST_CASE.PREREQUISITE;

    static final TableField<TestCaseLibraryNodeRecord, String> TC_DESCRIPTION =
            TEST_CASE_LIBRARY_NODE.DESCRIPTION;

    static final TableField<RequirementVersionCoverageRecord, Long> TC_REQUIREMENT_VERIFIED =
            REQUIREMENT_VERSION_COVERAGE.as("tc_rvc").REQUIREMENT_VERSION_COVERAGE_ID;

    static final TableField<TestCaseLibraryNodeRecord, String> TC_NAME = TEST_CASE_LIBRARY_NODE.NAME;

    static final TableField<ProjectRecord, Long> PROJECT_ID = PROJECT.PROJECT_ID;

    static final TableField<ProjectRecord, String> PROJECT_NAME = PROJECT.NAME;

    static final TableField<ExecutionIssuesClosureRecord, Long> ITPI_ISSUE =
            EXECUTION_ISSUES_CLOSURE.as("exec_issue").ISSUE_ID;

    static final TableField<TestSuiteRecord, String> TSu_NAME = TEST_SUITE.NAME;

    static final Milestone TC_MILESTONE = MILESTONE.as("tc_milestone");

    static final Milestone IT_MILESTONE = MILESTONE.as("it_milestone");

    static final TableField<ExecutionRecord, Long> EXECUTION_ID = EXECUTION.EXECUTION_ID;

    static final TableField<ExecutionRecord, String> EXECUTION_MODE = EXECUTION.EXECUTION_MODE;

    static final TableField<ExecutionRecord, String> EXECUTION_STATUS = EXECUTION.EXECUTION_STATUS;

    static final TableField<ExecutionStepRecord, Long> EXECUTION_STEP_ID =
            EXECUTION_STEP.EXECUTION_STEP_ID;

    static final TableField<ExecutionStepRecord, String> EXECUTION_STEP_STATUS =
            EXECUTION_STEP.EXECUTION_STATUS;

    static final TableField<ExecutionStepRecord, String> ES_LAST_EXECUTED_BY =
            EXECUTION_STEP.LAST_EXECUTED_BY;

    static final TableField<ExecutionStepRecord, Timestamp> ES_LAST_EXECUTED_ON =
            EXECUTION_STEP.LAST_EXECUTED_ON;

    static final TableField<ExecutionStepRecord, String> ES_COMMENT = EXECUTION_STEP.COMMENT;

    static final TableField<ExecutionExecutionStepsRecord, Integer> ES_ORDER =
            EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER;

    static final TableField<ExecutionStepRecord, Long> ES_TS_ID = EXECUTION_STEP.TEST_STEP_ID;

    static final TableField<RequirementVersionCoverageRecord, Long> ES_REQUIREMENT_VERIFIED =
            REQUIREMENT_VERSION_COVERAGE.as("es_rvc").REQUIREMENT_VERSION_COVERAGE_ID;

    static final TableField<TestCaseStepsRecord, Integer> TS_ORDER = TEST_CASE_STEPS.STEP_ORDER;

    static final TableField<TestCaseStepsRecord, Long> TS_ID = TEST_CASE_STEPS.STEP_ID;

    static final TableField<CallTestStepRecord, Long> CTS_CALLED_TS =
            CALL_TEST_STEP.CALLED_TEST_CASE_ID;

    static final TableField<RequirementVersionCoverageRecord, Long> TS_REQUIREMENT_VERIFIED =
            REQUIREMENT_VERSION_COVERAGE.as("ts_rvc").REQUIREMENT_VERSION_COVERAGE_ID;

    static final TableField<IssueRecord, Long> ES_ISSUE = ISSUE.as("es_issue").ISSUE_ID;

    static final String TAG_LABELS = "tag_labels";

    static final int SQL_MAX_PARAMETERS_FOR_IN_CLAUSE = 65535;

    @Inject private FeatureManager featureManager;

    @Inject protected DSLContext dsl;

    char separator = ';';

    int nbColumns;

    boolean milestonesEnabled;

    Campaign campaign;
    CampaignDto campaignDto;

    List<CustomFieldDto> campCUFModel = new ArrayList<>();
    SortedSet<CustomFieldDto> iterCUFModel =
            new TreeSet<>(Comparator.comparing(CustomFieldDto::getId));
    SortedSet<CustomFieldDto> tcCUFModel = new TreeSet<>(Comparator.comparing(CustomFieldDto::getId));
    SortedSet<CustomFieldDto> execCUFModel =
            new TreeSet<>(Comparator.comparing(CustomFieldDto::getId));
    SortedSet<CustomFieldDto> esCUFModel = new TreeSet<>(Comparator.comparing(CustomFieldDto::getId));

    Map<Long, CustomFieldValueDto> campCUFValues = new HashMap<>();
    MultiValueMap iterCUFValues = new MultiValueMap(); // <Long, Collection<CustomFieldValueDto>>
    MultiValueMap tcCUFValues = new MultiValueMap(); // same here
    MultiValueMap execCUFValues = new MultiValueMap(); // same here
    MultiValueMap esCUFValues = new MultiValueMap(); // same here

    @Override
    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    @Override
    public void setSeparator(char separator) {
        this.separator = separator;
    }

    @Override
    public char getSeparator() {
        return separator;
    }

    @Override
    public void init() {
        campaignDto = createCampaignDto(campaign);
        initIterationsAndCustomFields();
        milestonesEnabled = featureManager.isEnabled(FeatureManager.Feature.MILESTONE);
    }

    private CampaignDto createCampaignDto(Campaign campaign) {
        return new CampaignDto(
                campaign.getId(),
                campaign.getScheduledStartDate(),
                campaign.getScheduledEndDate(),
                campaign.getActualStartDate(),
                campaign.getActualEndDate());
    }

    /** Populate campaignDto with all necessary entities dto depending on campaign export type. */
    abstract void initIterationsAndCustomFields();

    /**
     * Create Jooq request with all necessary field for campaign export.
     *
     * @return an iterator of the Jooq request result set
     */
    abstract Iterator<Record> getIterationJooqQueryIterator();

    /**
     * Find CUFs for a given entity type and all CUF values associated to these cufs for given entity
     * list
     *
     * @param entityType CUF entity type value. String representation of a {@link
     *     org.squashtest.tm.domain.customfield.BindableEntity}.
     * @param cufModel the {@link Collection} of {@link CustomFieldDto} to populate.
     * @param cufValues the {@link MultiValueMap} of {@link CustomFieldDto} to populate.
     * @param entityIdList the {@link Collection} of entity Id whom CUF value are desired
     */
    void populateCUFModelAndCufValues(
            String entityType,
            Collection<CustomFieldDto> cufModel,
            MultiValueMap cufValues,
            Collection<Long> entityIdList) {

        Field<String> tagLabels =
                dsl.select(groupConcat(CUSTOM_FIELD_VALUE_OPTION.LABEL).separator(" | "))
                        .from(CUSTOM_FIELD_VALUE_OPTION)
                        .where(CUSTOM_FIELD_VALUE_OPTION.CFV_ID.eq(CUSTOM_FIELD_VALUE.CFV_ID))
                        .asField(TAG_LABELS);

        if (entityIdList.size() >= SQL_MAX_PARAMETERS_FOR_IN_CLAUSE) {
            int partitionNumber = entityIdList.size() / SQL_MAX_PARAMETERS_FOR_IN_CLAUSE + 1;
            int partitionSize = IntMath.divide(entityIdList.size(), partitionNumber, RoundingMode.UP);
            List<List<Long>> partitions = Lists.partition(entityIdList.stream().toList(), partitionSize);
            partitions.forEach(
                    partition -> populateCUF(entityType, cufModel, cufValues, partition, tagLabels));
        } else {
            populateCUF(entityType, cufModel, cufValues, entityIdList, tagLabels);
        }
    }

    private void populateCUF(
            String entityType,
            Collection<CustomFieldDto> cufModel,
            MultiValueMap cufValues,
            Collection<Long> entityIdList,
            Field<String> tagLabels) {
        dsl.select(
                        CUSTOM_FIELD_VALUE.CFV_ID,
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID,
                        CUSTOM_FIELD_BINDING.CF_ID,
                        CUSTOM_FIELD_VALUE.VALUE,
                        CUSTOM_FIELD_VALUE.LARGE_VALUE,
                        tagLabels,
                        CUSTOM_FIELD.CODE,
                        CUSTOM_FIELD.INPUT_TYPE)
                .from(CUSTOM_FIELD_VALUE)
                .innerJoin(CUSTOM_FIELD_BINDING)
                .on(CUSTOM_FIELD_BINDING.CFB_ID.eq(CUSTOM_FIELD_VALUE.CFB_ID))
                .innerJoin(CUSTOM_FIELD)
                .on(CUSTOM_FIELD.CF_ID.eq(CUSTOM_FIELD_BINDING.CF_ID))
                .where(
                        (CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.in(entityIdList))
                                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(entityType)))
                .fetch()
                .forEach(
                        r -> {
                            CustomFieldDto newCFDto =
                                    new CustomFieldDto(
                                            r.get(CUSTOM_FIELD_BINDING.CF_ID),
                                            r.get(CUSTOM_FIELD.CODE),
                                            r.get(CUSTOM_FIELD.INPUT_TYPE));
                            cufModel.add(newCFDto);

                            CustomFieldValueDto newCFVDto = createCUFValueDto(r);
                            cufValues.put(r.get(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID), newCFVDto);
                        });
    }

    void populateCampCUFModelAndCampCUFValues() {
        Field<String> tagLabels =
                dsl.select(groupConcat(CUSTOM_FIELD_VALUE_OPTION.LABEL).separator(" | "))
                        .from(CUSTOM_FIELD_VALUE_OPTION)
                        .where(CUSTOM_FIELD_VALUE_OPTION.CFV_ID.eq(CUSTOM_FIELD_VALUE.CFV_ID))
                        .asField(TAG_LABELS);

        dsl.select(
                        CUSTOM_FIELD_VALUE.CFV_ID,
                        CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID,
                        CUSTOM_FIELD_BINDING.CF_ID,
                        CUSTOM_FIELD_VALUE.VALUE,
                        CUSTOM_FIELD_VALUE.LARGE_VALUE,
                        tagLabels,
                        CUSTOM_FIELD.CODE,
                        CUSTOM_FIELD.INPUT_TYPE)
                .from(CUSTOM_FIELD_VALUE)
                .innerJoin(CUSTOM_FIELD_BINDING)
                .on(CUSTOM_FIELD_BINDING.CFB_ID.eq(CUSTOM_FIELD_VALUE.CFB_ID))
                .innerJoin(CUSTOM_FIELD)
                .on(CUSTOM_FIELD.CF_ID.eq(CUSTOM_FIELD_BINDING.CF_ID))
                .where(
                        (CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.eq(campaign.getId()))
                                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq("CAMPAIGN")))
                .fetch()
                .forEach(
                        r -> {
                            CustomFieldDto newCFDto =
                                    new CustomFieldDto(
                                            r.get(CUSTOM_FIELD_BINDING.CF_ID),
                                            r.get(CUSTOM_FIELD.CODE),
                                            r.get(CUSTOM_FIELD.INPUT_TYPE));
                            campCUFModel.add(newCFDto);

                            CustomFieldValueDto newCFVDto = createCUFValueDto(r);
                            campCUFValues.put(r.get(CUSTOM_FIELD_VALUE.CF_ID), newCFVDto);
                        });
    }

    private CustomFieldValueDto createCUFValueDto(Record r) {

        Field<String> tagLabels =
                dsl.select(groupConcat(CUSTOM_FIELD_VALUE_OPTION.LABEL).separator(" | "))
                        .from(CUSTOM_FIELD_VALUE_OPTION)
                        .where(CUSTOM_FIELD_VALUE_OPTION.CFV_ID.eq(CUSTOM_FIELD_VALUE.CFV_ID))
                        .asField(TAG_LABELS);

        CustomFieldValueDto newCFVDto;
        if (r.get(CUSTOM_FIELD_VALUE.VALUE) != null) {
            newCFVDto =
                    new CustomFieldValueDto(
                            r.get(CUSTOM_FIELD_VALUE.CFV_ID),
                            r.get(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID),
                            r.get(CUSTOM_FIELD_VALUE.CF_ID),
                            adaptHtmlImageAttribute(r.get(CUSTOM_FIELD_VALUE.VALUE)));
        } else if (r.get(CUSTOM_FIELD_VALUE.LARGE_VALUE) != null) {
            newCFVDto =
                    new CustomFieldValueDto(
                            r.get(CUSTOM_FIELD_VALUE.CFV_ID),
                            r.get(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID),
                            r.get(CUSTOM_FIELD_VALUE.CF_ID),
                            adaptHtmlImageAttribute(r.get(CUSTOM_FIELD_VALUE.LARGE_VALUE)));
        } else if (r.get(tagLabels) != null) {
            newCFVDto =
                    new CustomFieldValueDto(
                            r.get(CUSTOM_FIELD_VALUE.CFV_ID),
                            r.get(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID),
                            r.get(CUSTOM_FIELD_VALUE.CF_ID),
                            adaptHtmlImageAttribute(r.get(tagLabels)));
        } else {
            newCFVDto =
                    new CustomFieldValueDto(
                            r.get(CUSTOM_FIELD_VALUE.CFV_ID),
                            r.get(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID),
                            r.get(CUSTOM_FIELD_VALUE.CF_ID));
        }

        return newCFVDto;
    }

    private String adaptHtmlImageAttribute(String value) {
        Document document = Jsoup.parse(value);
        boolean isHtml = !document.text().equals(value);
        if (isHtml) {
            Elements elements = document.select("img");
            if (!elements.isEmpty()) {
                elements.forEach(
                        element -> {
                            String imageSrc = element.attr("src");
                            if (!imageSrc.contains("data:image")) {
                                element.attr("alt", imageSrc);
                            }
                        });
                return document.body().html();
            }
        }
        return value;
    }

    void populateTestCase(Record r, TestCaseDto currentTestCase) {

        if (r.get(TC_MILESTONE.LABEL) != null) {
            currentTestCase.addMilestone(r.get(TC_MILESTONE.LABEL));
        }

        if (r.get(TC_REQUIREMENT_VERIFIED) != null) {
            currentTestCase.addRequirement(r.get(TC_REQUIREMENT_VERIFIED));
        }
    }

    ITPIDto createNewItpiDto(Record r) {
        ITPIDto newItpi =
                new ITPIDto(
                        r.get(ITPI_ID), r.get(ITPI_STATUS), r.get(USER_LOGIN), r.get(ITPI_LAST_EXECUTED_ON));

        populateItpi(r, newItpi);

        if (r.get(DATASET_NAME) != null) {
            newItpi.setDataset(r.get(DATASET_NAME));
        }

        return newItpi;
    }

    protected abstract void populateItpi(Record r, ITPIDto newItpi);

    protected String escapeDoubleQuote(String value) {
        if (value.contains("\"")) {
            return value.replace("\"", "\"\"");
        }
        return value;
    }

    protected IterationDto populateIterationInCampaignDto(Record r, IterationDto currentIteration) {
        if (campaignDto.getIteration(r.get(ITERATION_ID)) == null) {
            campaignDto.addIteration(
                    new IterationDto(
                            r.get(ITERATION_ID),
                            r.get(ITERATION_NAME),
                            r.get(ITERATION_SCHEDULED_START_DATE),
                            r.get(ITERATION_SCHEDULED_END_DATE),
                            r.get(ITERATION_ACTUAL_START_DATE),
                            r.get(ITERATION_ACTUAL_END_DATE)));
            currentIteration = campaignDto.getIteration(r.get(ITERATION_ID));
        }
        return currentIteration;
    }

    protected static void populateIterationMilestoneForCampaignDto(
            Record r, IterationDto currentIteration) {
        if (r.get(IT_MILESTONE.LABEL) != null) {
            currentIteration.addMilestone(r.get(IT_MILESTONE.LABEL));
        }
    }

    protected static void addIssueForExecution(Record r, ExecutionDto currentExecution) {
        if (r.get(ITPI_ISSUE) != null) {
            currentExecution.addIssue(r.get(ITPI_ISSUE));
        }
    }
}
