/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.partitionBy;
import static org.jooq.impl.DSL.select;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.CampaignIteration.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.tables.CampaignLibraryNode.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.Execution.EXECUTION;
import static org.squashtest.tm.jooq.domain.tables.ExecutionExecutionSteps.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.tables.ExecutionStep.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanExecution.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanList.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.tables.IterationTestPlanItem.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.Project.PROJECT;
import static org.squashtest.tm.jooq.domain.tables.TestAutomationServer.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_EXECUTION_STEPS_BY_EXECUTION_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_TEST_PLAN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ROW_NUMBER;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.hibernate.Hibernate;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.core.foundation.collection.Paging;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.internal.dto.AutomatedExecutionUpdateData;
import org.squashtest.tm.service.internal.dto.ExecutionSummaryDto;
import org.squashtest.tm.service.internal.repository.CustomExecutionDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;

@Repository
public class ExecutionDaoImpl implements CustomExecutionDao {

    private static final String EXECUTION_COUNT_STATUS = "Execution.countStatus";
    private static final String STATUS = "status";

    @PersistenceContext private EntityManager entityManager;

    @Inject private DSLContext dsl;

    @Override
    public Execution findAndInit(long executionId) {
        Execution execution = findById(executionId);
        Hibernate.initialize(execution.getReferencedTestCase());
        Hibernate.initialize(execution.getSteps());
        return execution;
    }

    private Execution findById(long executionId) {
        return entityManager.getReference(Execution.class, executionId);
    }

    @Override
    public ExecutionStatusReport getStatusReport(final long executionId) {

        ExecutionStatusReport report = new ExecutionStatusReport();

        for (ExecutionStatus status : ExecutionStatus.values()) {
            final ExecutionStatus fStatus = status;

            Long lResult =
                    (Long)
                            entityManager
                                    .createNamedQuery(EXECUTION_COUNT_STATUS)
                                    .setParameter("execId", executionId)
                                    .setParameter(STATUS, fStatus)
                                    .getSingleResult();

            report.set(status, lResult.intValue());
        }

        return report;
    }

    /*
     * same than for HibernateTestCaseDao#findStepsByIdFiltered :
     *
     * because we need to get the ordered list and we can't access the join table to sort them (again), we can't use the
     * Criteria API. So we're playing it old good java here.
     *
     * Note GRF : looks like service code to me
     */

    @Override
    public List<ExecutionStep> findStepsFiltered(final Long executionId, final Paging filter) {

        Execution execution = findById(executionId);
        int listSize = execution.getSteps().size();

        int startIndex = filter.getFirstItemIndex();
        int lastIndex = filter.getFirstItemIndex() + filter.getPageSize();

        // prevent IndexOutOfBoundException :
        if (startIndex >= listSize) {
            return new LinkedList<>(); // ie resultset is empty
        }
        if (lastIndex >= listSize) {
            lastIndex = listSize;
        }

        return execution.getSteps().subList(startIndex, lastIndex);
    }

    // ************** special execution status deactivation section ***************

    @Override
    public boolean projectUsesExecutionStatus(long projectId, ExecutionStatus executionStatus) {
        return hasExecStepWithStatus(projectId, executionStatus)
                || hasItemTestPlanWithStatus(projectId, executionStatus)
                || hasExecWithStatus(projectId, executionStatus);
    }

    private boolean hasExecStepWithStatus(long projectId, ExecutionStatus executionStatus) {
        return dsl.selectCount()
                        .from(EXECUTION_STEP)
                        .innerJoin(EXECUTION_EXECUTION_STEPS)
                        .on(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(EXECUTION_STEP.EXECUTION_STEP_ID))
                        .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                        .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_ID))
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                        .innerJoin(CAMPAIGN_ITERATION)
                        .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .where(EXECUTION_STEP.EXECUTION_STATUS.eq(executionStatus.name()))
                        .and(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(projectId))
                        .fetchOneInto(int.class)
                > 0;
    }

    private boolean hasExecWithStatus(long projectId, ExecutionStatus executionStatus) {
        return dsl.selectCount()
                        .from(org.squashtest.tm.jooq.domain.tables.Execution.EXECUTION)
                        .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                        .on(
                                ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(
                                        org.squashtest.tm.jooq.domain.tables.Execution.EXECUTION.EXECUTION_ID))
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                        .innerJoin(CAMPAIGN_ITERATION)
                        .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .where(
                                org.squashtest.tm.jooq.domain.tables.Execution.EXECUTION.EXECUTION_STATUS.eq(
                                        executionStatus.name()))
                        .and(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(projectId))
                        .fetchOneInto(int.class)
                > 0;
    }

    private boolean hasItemTestPlanWithStatus(long projectId, ExecutionStatus executionStatus) {
        return dsl.selectCount()
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .innerJoin(CAMPAIGN_ITERATION)
                        .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .where(ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.eq(executionStatus.name()))
                        .and(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(projectId))
                        .fetchOneInto(int.class)
                > 0;
    }

    @Override
    public TestAutomationServerKind getTestAutomationServerKindByProjectId(Long projectId) {
        return dsl.select(TEST_AUTOMATION_SERVER.KIND)
                .from(TEST_AUTOMATION_SERVER)
                .innerJoin(PROJECT)
                .on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchOneInto(TestAutomationServerKind.class);
    }

    @Override
    public Map<Long, List<ExecutionSummaryDto>> findIterationTestPlanItemLastExecStatuses(
            Collection<Long> itemTestPlanIds) {
        var maxOrderTable =
                select(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID,
                                DSL.max(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER).as("max_order"))
                        .from(ITEM_TEST_PLAN_EXECUTION)
                        .where(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.in(itemTestPlanIds))
                        .groupBy(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID)
                        .asTable("max_order_table");

        var partitionTable =
                select(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID,
                                EXECUTION.EXECUTION_ID,
                                (DSL.when(
                                                        EXECUTION.EXECUTION_STATUS.isNull(),
                                                        DSL.val(ExecutionStatus.READY, String.class))
                                                .otherwise(EXECUTION.EXECUTION_STATUS))
                                        .as(RequestAliasesConstants.EXECUTION_STATUS),
                                EXECUTION.LAST_EXECUTED_ON,
                                DSL.rowNumber()
                                        .over(
                                                partitionBy(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID)
                                                        .orderBy(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.desc()))
                                        .as(ROW_NUMBER))
                        .from(EXECUTION)
                        .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                        .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .innerJoin(maxOrderTable)
                        .on(
                                ITEM_TEST_PLAN_EXECUTION
                                        .ITEM_TEST_PLAN_ID
                                        .eq(maxOrderTable.field(ITEM_TEST_PLAN_ID, Long.class))
                                        .and(
                                                ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.ne(
                                                        maxOrderTable.field("max_order", Integer.class))))
                        .asTable();

        return dsl.selectFrom(partitionTable)
                .where(partitionTable.field(ROW_NUMBER, Integer.class).le(5))
                .fetchStream()
                .collect(
                        Collectors.groupingBy(
                                r -> r.get(ITEM_TEST_PLAN_ID, Long.class),
                                Collectors.mapping(
                                        r ->
                                                new ExecutionSummaryDto(
                                                        r.get(EXECUTION_ID, Long.class),
                                                        r.get(RequestAliasesConstants.EXECUTION_STATUS, String.class),
                                                        r.get(LAST_EXECUTED_ON, Date.class)),
                                        Collectors.toList())));
    }

    @Override
    public List<ExecutionSummaryDto> findSprintTestPlanItemLastExecStatuses(Long testPlanItemId) {
        final Integer maxOrder =
                dsl.select(DSL.max(EXECUTION.EXECUTION_ORDER))
                        .from(EXECUTION)
                        .where(EXECUTION.TEST_PLAN_ITEM_ID.eq(testPlanItemId))
                        .fetchOneInto(Integer.class);

        return dsl.select(
                        EXECUTION.EXECUTION_ID,
                        DSL.when(
                                        EXECUTION.EXECUTION_STATUS.isNull(),
                                        DSL.val(ExecutionStatus.READY, String.class))
                                .otherwise(EXECUTION.EXECUTION_STATUS)
                                .as("EXECUTION_STATUS"),
                        EXECUTION.LAST_EXECUTED_ON)
                .from(EXECUTION)
                .where(
                        EXECUTION
                                .TEST_PLAN_ITEM_ID
                                .eq(testPlanItemId)
                                .and(EXECUTION.EXECUTION_ORDER.notEqual(maxOrder)))
                .orderBy(EXECUTION.EXECUTION_ORDER.desc())
                .limit(5)
                .fetchInto(ExecutionSummaryDto.class);
    }

    @Override
    public AutomatedExecutionUpdateData findAutomatedExecutionUpdateData(
            Long itemId, String suiteId) {
        return dsl.select(
                        EXECUTION.EXECUTION_ID,
                        AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID,
                        EXECUTION.ATTACHMENT_LIST_ID)
                .from(EXECUTION)
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .where(
                        ITEM_TEST_PLAN_EXECUTION
                                .ITEM_TEST_PLAN_ID
                                .eq(itemId)
                                .and(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.eq(suiteId)))
                .fetchOneInto(AutomatedExecutionUpdateData.class);
    }

    @Override
    public List<Long> findItpiIdsByExecutionIds(List<Long> executionIds) {
        return dsl.selectDistinct(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID)
                .from(ITEM_TEST_PLAN_EXECUTION)
                .where(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.in(executionIds))
                .fetchInto(Long.class);
    }

    // ************** /special execution status deactivation section **************

    @Override
    public List<Long> findAllIdsByAutomatedSuiteIds(List<String> automatedSuiteIds) {
        Query fetchQuery = entityManager.createNamedQuery("Execution.findAllIdsByAutomatedSuiteIds");
        fetchQuery.setParameter("automatedSuiteIds", automatedSuiteIds);

        // Return executions ids
        return fetchQuery.getResultList();
    }

    @Override
    public List<ExecutionStep> findStepsForAllExecutions(Collection<Long> executionIds) {
        return new EntityGraphQueryBuilder<>(
                        entityManager, ExecutionStep.class, FIND_EXECUTION_STEPS_BY_EXECUTION_IDS)
                .executeDistinctList(Map.of(IDS, executionIds));
    }

    @Override
    public Execution loadWithSteps(long executionId) {
        return entityManager.createQuery("""
                        select e from Execution e
                        left join fetch e.referencedTestCase tc
                        left join fetch tc.steps
                        left join fetch e.steps step
                        left join fetch step.referencedTestStep
                        where e.id = :id""", Execution.class)
                .setParameter(ID, executionId)
                .getSingleResult();
    }

    @Override
    public List<Execution> findExecutionsByFailureDetailAndDateAfter(Long failureDetailId, Date createdOnDate) {
        return entityManager.createQuery("""
                    select e from Execution e
                    join e.automatedExecutionExtender ae
                    join ae.failureDetailList fd
                    where fd.id = :failureDetailId
                    and e.audit.createdOn>= :createdOnDate""", Execution.class)
                   .setParameter("failureDetailId", failureDetailId)
                   .setParameter("createdOnDate", createdOnDate)
                   .getResultList();
    }

}
