/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.environmentvariable;

import java.util.List;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;

public interface EnvironmentVariableBindingService {

    void createNewBindings(
            Long testAutomationServerId, EVBindableEntity entityType, List<Long> environmentVariableIds);

    void unbind(Long entityId, EVBindableEntity entityEntity, List<Long> evIds);

    void unbindByEnvironmentVariableId(Long evId);

    void unbindAllByEntityIdAndType(Long entityId, EVBindableEntity entityType);

    List<BoundEnvironmentVariableDto> getBoundEnvironmentVariablesByEntity(
            Long entityId, EVBindableEntity entityType);

    List<BoundEnvironmentVariableDto> getBoundEnvironmentVariablesByTestAutomationServerId(
            Long testAutomationServerId);

    void bindServerEnvironmentVariablesToProject(Long taServerId, Long projectId);

    List<BoundEnvironmentVariableDto> getBoundEnvironmentVariableFromProjectView(Long projectId);

    List<BoundEnvironmentVariableDto> getBoundEnvironmentVariableFromProjectViewByIteration(
            Long projectId, Long iterationId);
}
