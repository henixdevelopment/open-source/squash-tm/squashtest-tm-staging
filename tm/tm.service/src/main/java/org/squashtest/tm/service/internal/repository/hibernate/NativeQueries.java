/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

/**
 * thanks to the Hibernate support to pure scalar native queries, let's create another query
 * repository.
 *
 * @author bsiri
 */
@SuppressWarnings("squid:S1192")
public final class NativeQueries {

    public static final String TEST_CASE_FOLDER_SQL_FIND_PAIRED_CONTENT_FOR_FOLDERS =
            "select * from TCLN_RELATIONSHIP where ancestor_id in (:folderIds)";
    public static final String TEST_CASE_FOLDER_SQL_FIND_CONTENT_FOR_FOLDER =
            "select * from TCLN_RELATIONSHIP where ancestor_id in (:folderIds)";

    public static final String REQUIREMENT_FOLDER_SQL_FIND_PAIRED_CONTENT_FOR_FOLDERS =
            "select * from RLN_RELATIONSHIP where ancestor_id in (:folderIds)";
    public static final String REQUIREMENT_FOLDER_SQL_FIND_CONTENT_FOR_FOLDER =
            "select * from RLN_RELATIONSHIP where ancestor_id in (:folderIds)";

    public static final String CAMPAIGN_FOLDER_SQL_FIND_PAIRED_CONTENT_FOR_FOLDERS =
            "select * from CLN_RELATIONSHIP where ancestor_id in (:folderIds)";
    public static final String CAMPAIGN_FOLDER_SQL_FIND_CONTENT_FOR_FOLDER =
            "select * from CLN_RELATIONSHIP where ancestor_id in (:folderIds)";

    /* ***************************** deletion queries ************************************** */

    public static final String TESTCASELIBRARYNODE_SQL_FILTERFOLDERIDS =
            "select folder.tcln_id from TEST_CASE_FOLDER folder where folder.tcln_id in (:testcaseIds)";

    public static final String TESTCASE_SQL_UNBIND_MILESTONE =
            "delete from MILESTONE_TEST_CASE where MILESTONE_ID = :milestoneId and TEST_CASE_ID in (:testCaseIds)";
    public static final String TESTCASE_SQL_FINDNOTDELETED =
            "select TCLN_ID from TEST_CASE where TCLN_ID in (:allTestCaseIds)";

    public static final String TESTSTEP_SQL_REMOVEACTIONSTEPS =
            "delete from ACTION_TEST_STEP where test_step_id in (:testStepIds)";
    public static final String TESTSTEP_SQL_REMOVECALLSTEPS =
            "delete from CALL_TEST_STEP where test_step_id in (:testStepIds)";
    public static final String TESTSTEP_SQL_REMOVEACTIONWORDPARAMVALUES =
            "delete from ACTION_WORD_PARAMETER_VALUE where keyword_test_step_id in (:testStepIds)";
    public static final String TESTSTEP_SQL_REMOVEKEYWORDSTEPS =
            "delete from KEYWORD_TEST_STEP where test_step_id in (:testStepIds)";
    public static final String TESTSTEP_SQL_REMOVETESTSTEPS =
            "delete from TEST_STEP where test_step_id in (:testStepIds)";

    public static final String REQUIREMENTLIBRARYNODE_SQL_FILTERFOLDERIDS =
            "select folder.rln_id from REQUIREMENT_FOLDER folder where folder.rln_id in (:requirementIds)";

    public static final String REQUIREMENT_SQL_UNBIND_MILESTONE =
            "delete from MILESTONE_REQ_VERSION where MILESTONE_ID = :milestoneId and REQ_VERSION_ID in "
                    + "(select v.RES_ID from REQUIREMENT_VERSION v where v.REQUIREMENT_ID in (:requirementIds) )";
    public static final String REQUIREMENT_SQL_FINDNOTDELETED =
            "select RLN_ID from REQUIREMENT where RLN_ID in (:allRequirementIds)";

    public static final String CAMPAIGNLIBRARYNODE_SQL_FILTERFOLDERIDS =
            "select folder.cln_id from CAMPAIGN_FOLDER folder where folder.cln_id in (:campaignIds)";

    public static final String CAMPAIGN_SQL_UNBIND_MILESTONE =
            "delete from MILESTONE_CAMPAIGN where MILESTONE_ID = :milestoneId and CAMPAIGN_ID in (:campaignIds)";
    public static final String CAMPAIGN_SQL_FINDNOTDELETED =
            "select CLN_ID from CAMPAIGN where CLN_ID in (:allCampaignIds)";

    /*
     * ********************************************** consequences of test case deletion on campaign item test plans
     * *******************************************
     */

    /* *****************************LIBRARY_PLUGIN_PINDING******************************************* */

    public static final String DELETE_LIBRARY_PLUGING_PINDING_PROPERTY =
            "delete from LIBRARY_PLUGIN_BINDING_PROPERTY where PLUGIN_BINDING_ID = :libraryPluginBindingId";

    public static final String COUNT_ACTIVE_PLUGIN_IN_PROJECT =
            "Select count(*) from LIBRARY_PLUGIN_BINDING lpb INNER JOIN LIBRARY_PLUGIN_BINDING_PROPERTY as lpbp ON lpb.plugin_binding_id = lpbp.plugin_binding_id "
                    + " where library_id = :projectId and active = true";

    /* ************************************ /consequences of test case deletion on item test plans  ******************************************************* */

    public static final String TESTCASE_SQL_SETNULLCALLINGEXECUTIONS =
            "update EXECUTION set tcln_id = null where tcln_id in (:testCaseIds)";

    public static final String TESTCASE_SQL_SET_NULL_CALLING_EXECUTION_STEPS =
            "update EXECUTION_STEP set test_step_id = null where test_step_id in (:testStepIds)";

    public static final String TESTCASE_SQL_SET_NULL_AUTOMATION_REQUEST =
            "update TEST_CASE set AUTOMATION_REQUEST_ID = null where AUTOMATION_REQUEST_ID in (:automationRequestIds)";

    public static final String AUTOMATION_REQUEST_SQL_REMOVE_LIBRARY_CONTENT_FROMLIST =
            "delete from AUTOMATION_REQUEST_LIBRARY_CONTENT where content_id in (:automationRequestIds)";

    public static final String AUTOMATION_REQUEST_SQL_REMOVE =
            "delete from AUTOMATION_REQUEST where AUTOMATION_REQUEST_ID in (:automationRequestIds)";

    public static final String AUTOMATION_REQUEST_SQL_REMOVE_REMOTE_AUTOMATION_REQUEST_EXTENDER =
            "delete from REMOTE_AUTOMATION_REQUEST_EXTENDER where AUTOMATION_REQUEST_ID in (:automationRequestIds)";

    public static final String TESTCASE_SQL_REMOVEVERIFYINGTESTCASELIST =
            "delete from REQUIREMENT_VERSION_COVERAGE where verifying_test_case_id in (:testCaseIds)";

    public static final String TESTCASE_SQL_REMOVEVERIFYINGTESTSTEPLIST =
            "delete from VERIFYING_STEPS where TEST_STEP_ID in (:testStepIds)";

    public static final String TESTCASE_SQL_REMOVETESTSTEPFROMLIST =
            "delete from TEST_CASE_STEPS where step_id in (:testStepIds)";

    public static final String REQUIREMENT_SQL_REMOVEFROMVERIFIEDVERSIONSLISTS =
            " delete from REQUIREMENT_VERSION_COVERAGE "
                    + " where verified_req_version_id in (:versionIds)";

    public static final String REQUIREMENT_SQL_REMOVEFROMLINKEDVERSIONSLISTS =
            "delete from REQUIREMENT_VERSION_LINK"
                    + " where requirement_version_id in (:versionIds) or related_requirement_version_id in (:versionIds)";

    public static final String REQUIREMENT_SQL_REMOVEFROMVERIFIEDREQUIREMENTLISTS =
            " delete from REQUIREMENT_VERSION_COVERAGE "
                    + " where verified_req_version_id in ( "
                    + " select req_v.res_id from REQUIREMENT_VERSION req_v where req_v.requirement_id in (:requirementIds) "
                    + ")";

    public static final String REQUIREMENT_SQL_REMOVE_TEST_STEP_COVERAGE_BY_REQ_VERSION_IDS =
            "delete from VERIFYING_STEPS where REQUIREMENT_VERSION_COVERAGE_ID in (select REQUIREMENT_VERSION_COVERAGE_ID from REQUIREMENT_VERSION_COVERAGE where VERIFIED_REQ_VERSION_ID in (:versionIds))";

    public static final String REQUIREMENT_SQL_REMOVE_TEST_STEPS_BY_COVERAGE_IDS =
            "delete from VERIFYING_STEPS where requirement_version_coverage_id in :covIds";

    /* ********************************************* tree path queries ********************************************************************* */
    private static final String CLN_FIND_SORTED_PARENTS =
            " from CAMPAIGN_LIBRARY_NODE cln "
                    + "inner join CLN_RELATIONSHIP_CLOSURE clos "
                    + "on clos.ancestor_id = cln.cln_id "
                    + "where clos.descendant_id = :nodeId "
                    + "order by clos.depth desc";

    public static final String CLN_FIND_SORTED_PARENT_NAMES =
            "select cln.name " + CLN_FIND_SORTED_PARENTS;
    public static final String CLN_FIND_SORTED_PARENT_IDS =
            "select cln.cln_id " + CLN_FIND_SORTED_PARENTS;

    public static final String RLN_FIND_SORTED_PARENT_NAMES =
            "select rs.name from RESOURCE rs "
                    + "join REQUIREMENT_FOLDER rf "
                    + "on rs.res_id = rf.res_id "
                    + "join REQUIREMENT_LIBRARY_NODE rln "
                    + "on rf.rln_id = rln.rln_id "
                    + "inner join RLN_RELATIONSHIP_CLOSURE clos "
                    + "on clos.ancestor_id = rln.rln_id "
                    + "where clos.descendant_id = :nodeId "
                    + "order by clos.depth desc";

    public static final String RLN_FIND_SORTED_PARENT_IDS =
            "select rln.rln_id from REQUIREMENT_LIBRARY_NODE rln "
                    + "inner join RLN_RELATIONSHIP_CLOSURE clos "
                    + "on clos.ancestor_id = rln.rln_id "
                    + "where clos.descendant_id = :nodeId "
                    + "order by clos.depth desc";

    /* ********************************************* statistics ********************************************************************* */
    public static final String REQUIREMENT_SQL_BOUND_DESC_STATISTICS =
            "Select (Case When res.description != '' AND res.description is not null Then 1 Else 0 End) as hasDescription, count(res.res_id) "
                    + "From REQUIREMENT req "
                    + "Inner Join REQUIREMENT_VERSION reqVer on req.current_version_id = reqVer.res_id "
                    + "Inner Join RESOURCE res on reqVer.res_id = res.res_id "
                    + "Where req.rln_id in (:requirementIds) "
                    + "Group By hasDescription";

    public static final String REQUIREMENT_SQL_BOUND_DESC_STATISTICS_BY_VERSION_IDS =
            "Select (Case When res.description != '' AND res.description is not null Then 1 Else 0 End) as hasDescription, count(res.res_id) "
                    + "From RESOURCE res "
                    + "Where res.res_id in (:requirementVersionIds) "
                    + "Group By hasDescription";

    public static final String REQUIREMENT_SQL_COVERAGE_STATISTICS =
            "Select totalSelection.criticality, Coalesce(coveredSelection.coverCount, 0), totalSelection.totalCount "
                    + "From "
                    + "(Select coverageStats.criticality as criticality, count(sizeclass) as coverCount "
                    + "From (Select coverage.criticality as criticality, Case When count(coverage.tcId) = 0 then 0 Else 1 End as sizeclass "
                    + "From (Select reqVer.criticality as criticality, req.rln_id as reqId, reqVerCov.verified_req_version_id as tcId "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Join REQUIREMENT req on reqVer.res_id = req.current_version_id "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Where req.rln_id in (:requirementIds) "
                    + "Union "
                    + "Select reqVer.criticality as criticality, req.rln_id as reqId, reqVerCov.verified_req_version_id as tcId "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Join REQUIREMENT req on reqVer.res_id = req.current_version_id "
                    + "Join REQUIREMENT linkedLowLevelReq on req.rln_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE reqVerCov on reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Where req.rln_id in (:requirementIds)) as coverage "
                    + "Group By coverage.reqId, coverage.criticality) as coverageStats "
                    + "Where sizeclass = 1 "
                    + "Group By coverageStats.criticality) "
                    + "as coveredSelection "
                    + "Right Outer Join "
                    + "(Select reqVer2.criticality as criticality, count(reqVer2.res_id) as totalCount "
                    + "From REQUIREMENT_VERSION as reqVer2 "
                    + "Inner Join REQUIREMENT req2 on reqVer2.res_id = req2.current_version_id "
                    + "Where req2.rln_id in (:requirementIds) Group By reqVer2.criticality) "
                    + "as totalSelection "
                    + "On coveredSelection.criticality = totalSelection.criticality";

    public static final String REQUIREMENT_SQL_COVERAGE_STATISTICS_BY_VERSION_IDS =
            "Select totalSelection.criticality, Coalesce(coveredSelection.coverCount, 0), totalSelection.totalCount "
                    + "From "
                    + "(Select coverageStats.criticality as criticality, count(sizeclass) as coverCount "
                    + "From (Select coverage.criticality as criticality, Case When count(coverage.tcId) = 0 then 0 Else 1 End as sizeclass "
                    + "From (Select reqVer.criticality as criticality, reqVer.res_id as resId, reqVerCov.verified_req_version_id as tcId "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Where reqVer.res_id in (:requirementVersionIds) "
                    + "Union "
                    + "Select reqVer.criticality as criticality, reqVer.res_id as resId, reqVerCov.verified_req_version_id as tcId "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Join REQUIREMENT linkedLowLevelReq on reqVer.requirement_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE reqVerCov on reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Where reqVer.res_id in (:requirementVersionIds)) as coverage "
                    + "Group By coverage.resId, coverage.criticality) as coverageStats "
                    + "Where sizeclass = 1 "
                    + "Group By coverageStats.criticality) "
                    + "as coveredSelection "
                    + "Right Outer Join "
                    + "(Select reqVer2.criticality as criticality, count(reqVer2.res_id) as totalCount "
                    + "From REQUIREMENT_VERSION as reqVer2 "
                    + "Where reqVer2.res_id in (:requirementVersionIds) Group By reqVer2.criticality) "
                    + "as totalSelection "
                    + "On coveredSelection.criticality = totalSelection.criticality";

    public static final String REQUIREMENT_SQL_VALIDATION_STATISTICS_BASE_QUERY =
            " select aa.rln_id, "
                    + "               aa.criticality, "
                    + "               case max(case when bb.execution_status in ('FAILURE','BLOCKED','UNTESTABLE') then 3 "
                    + "                             when bb.execution_status in ('READY','RUNNING') or bb.execution_status is null then 2 "
                    + "                             when bb.execution_status in ('SUCCESS') then 1 "
                    + "                             else 0 "
                    + "                             end) over (partition by aa.rln_id) "
                    + "                    when 3 then 'FAILURE' "
                    + "                    when 2 then 'READY' "
                    + "                    when 1 then 'SUCCESS' "
                    + "                    when 0 then 'SETTLED' "
                    + "                    end status "
                    + "          from (select a.rln_id, a.high_level_requirement_id hl_rln_id, b.criticality, c.verifying_test_case_id, coalesce(d.dataset_id,0) dataset_id, max(coalesce(d.last_executed_on,'1970-01-01')) last_exec "
                    + "                  from REQUIREMENT a join REQUIREMENT_VERSION b on a.current_version_id=b.res_id "
                    + "                                left join HIGH_LEVEL_REQUIREMENT z on a.rln_id=z.rln_id "
                    + "                                left join REQUIREMENT_VERSION_COVERAGE c on b.res_id=c.verified_req_version_id "
                    + "                                left join ITERATION_TEST_PLAN_ITEM d on c.verifying_test_case_id=d.tcln_id "
                    + "                 where a.rln_id in (:requirementIds) and z.rln_id is null group by a.rln_id,2,3,c.verifying_test_case_id,dataset_id) aa "
                    + "                left join ITERATION_TEST_PLAN_ITEM bb on aa.verifying_test_case_id=bb.tcln_id "
                    + "                                                     and aa.dataset_id=coalesce(bb.dataset_id,0) "
                    + "                                                     and aa.last_exec=coalesce(bb.last_executed_on, '1970-01-01') "
                    + "        union "
                    + "        select aa.rln_id, "
                    + "               aa.criticality, "
                    + "               case max(case when bb.execution_status in ('FAILURE','BLOCKED','UNTESTABLE') then 3 "
                    + "                             when bb.execution_status in ('READY','RUNNING') or bb.execution_status is null then 2 "
                    + "                             when bb.execution_status in ('SUCCESS') then 1 "
                    + "                             else 0 "
                    + "                             end) over (partition by aa.rln_id) "
                    + "                    when 3 then 'FAILURE' "
                    + "                    when 2 then 'READY' "
                    + "                    when 1 then 'SUCCESS' "
                    + "                    when 0 then 'SETTLED' "
                    + "                    end status "
                    + "          from (select a.rln_id, b.criticality, c.verifying_test_case_id, coalesce(d.dataset_id,0) dataset_id, max(coalesce(d.last_executed_on,'1970-01-01')) last_exec "
                    + "                  from REQUIREMENT a join REQUIREMENT_VERSION b on a.current_version_id=b.res_id "
                    + "                                left join REQUIREMENT_VERSION_COVERAGE c on b.res_id=c.verified_req_version_id "
                    + "                                left join ITERATION_TEST_PLAN_ITEM d on c.verifying_test_case_id=d.tcln_id "
                    + "                                     join HIGH_LEVEL_REQUIREMENT e on a.rln_id=e.rln_id "
                    + "                 where a.rln_id in (:requirementIds) "
                    + "              group by 1,2,3,4 "
                    + "                union "
                    + "                select a.high_level_requirement_id rln_id, y.criticality, c.verifying_test_case_id, coalesce(d.dataset_id,0) dataset_id, max(coalesce(d.last_executed_on,'1970-01-01')) last_exec "
                    + "                  from REQUIREMENT z join REQUIREMENT a on a.high_level_requirement_id=z.rln_id "
                    + "                                     join REQUIREMENT_VERSION y on z.current_version_id=y.res_id "
                    + "                                     join REQUIREMENT_VERSION b on a.current_version_id=b.res_id "
                    + "                                left join REQUIREMENT_VERSION_COVERAGE c on b.res_id=c.verified_req_version_id "
                    + "                                left join ITERATION_TEST_PLAN_ITEM d on c.verifying_test_case_id=d.tcln_id "
                    + "                                     join HIGH_LEVEL_REQUIREMENT e on a.high_level_requirement_id=e.rln_id "
                    + "                                    where a.rln_id in (:requirementIds)  group by 1,2,3,4) aa "
                    + "                left join ITERATION_TEST_PLAN_ITEM bb on aa.verifying_test_case_id=bb.tcln_id "
                    + "                                                     and aa.dataset_id=coalesce(bb.dataset_id,0) "
                    + "                                                     and aa.last_exec=coalesce(bb.last_executed_on, '1970-01-01')";

    public static final String REQUIREMENT_SQL_VALIDATION_STATISTICS =
            "select a.criticality, a.status, count(*) "
                    + "  from ("
                    + REQUIREMENT_SQL_VALIDATION_STATISTICS_BASE_QUERY
                    + ") a "
                    + "group by  a.criticality, a.status;";

    public static final String REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION =
            "Select Distinct a.rln_id "
                    + "  from ("
                    + REQUIREMENT_SQL_VALIDATION_STATISTICS_BASE_QUERY
                    + ") a "
                    + "Where a.criticality = (:criticality) "
                    + "And a.status In (:validationStatus);";

    public static final String REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_ID_BASE_QUERY =
            "select aa.res_id, "
                    + "               aa.criticality, "
                    + "               case max(case when bb.execution_status in ('FAILURE','BLOCKED','UNTESTABLE') then 3 "
                    + "                             when bb.execution_status in ('READY','RUNNING') or bb.execution_status is null then 2 "
                    + "                             when bb.execution_status in ('SUCCESS') then 1 "
                    + "                             else 0 "
                    + "                             end) over (partition by aa.res_id) "
                    + "                    when 3 then 'FAILURE' "
                    + "                    when 2 then 'READY' "
                    + "                    when 1 then 'SUCCESS' "
                    + "                    when 0 then 'SETTLED' "
                    + "                    end status "
                    + "          from (select a.rln_id, a.high_level_requirement_id hl_rln_id, b.res_id, b.criticality, c.verifying_test_case_id, coalesce(d.dataset_id,0) dataset_id, max(coalesce(d.last_executed_on,'1970-01-01')) last_exec "
                    + "                  from REQUIREMENT a join REQUIREMENT_VERSION b on a.current_version_id=b.res_id "
                    + "                                left join HIGH_LEVEL_REQUIREMENT z on a.rln_id=z.rln_id "
                    + "                                left join REQUIREMENT_VERSION_COVERAGE c on b.res_id=c.verified_req_version_id "
                    + "                                left join ITERATION_TEST_PLAN_ITEM d on c.verifying_test_case_id=d.tcln_id "
                    + "                 where a.current_version_id in (:requirementVersionIds) and z.rln_id is null group by 1,2,3,4,5,6) aa "
                    + "                left join ITERATION_TEST_PLAN_ITEM bb on aa.verifying_test_case_id=bb.tcln_id "
                    + "                                                     and aa.dataset_id=coalesce(bb.dataset_id,0) "
                    + "                                                     and aa.last_exec=coalesce(bb.last_executed_on, '1970-01-01') "
                    + "        union "
                    + "        select aa.res_id, "
                    + "               aa.criticality, "
                    + "               case max(case when bb.execution_status in ('FAILURE','BLOCKED','UNTESTABLE') then 3 "
                    + "                             when bb.execution_status in ('READY','RUNNING') or bb.execution_status is null then 2 "
                    + "                             when bb.execution_status in ('SUCCESS') then 1 "
                    + "                             else 0 "
                    + "                             end) over (partition by aa.rln_id) "
                    + "                    when 3 then 'FAILURE' "
                    + "                    when 2 then 'READY' "
                    + "                    when 1 then 'SUCCESS' "
                    + "                    when 0 then 'SETTLED' "
                    + "                    end status "
                    + "          from (select b.res_id, a.rln_id, b.criticality, c.verifying_test_case_id, coalesce(d.dataset_id,0) dataset_id, max(coalesce(d.last_executed_on,'1970-01-01')) last_exec "
                    + "                  from REQUIREMENT a join REQUIREMENT_VERSION b on a.current_version_id=b.res_id "
                    + "                                left join REQUIREMENT_VERSION_COVERAGE c on b.res_id=c.verified_req_version_id "
                    + "                                left join ITERATION_TEST_PLAN_ITEM d on c.verifying_test_case_id=d.tcln_id "
                    + "                                     join HIGH_LEVEL_REQUIREMENT e on a.rln_id=e.rln_id "
                    + "                 where b.res_id in (:requirementVersionIds) "
                    + "              group by 1,2,3,4,5 "
                    + "                union "
                    + "                select y.res_id, a.high_level_requirement_id rln_id, y.criticality, c.verifying_test_case_id, coalesce(d.dataset_id,0) dataset_id, max(coalesce(d.last_executed_on,'1970-01-01')) last_exec "
                    + "                  from REQUIREMENT z join REQUIREMENT a on a.high_level_requirement_id=z.rln_id "
                    + "                                     join REQUIREMENT_VERSION y on z.current_version_id=y.res_id "
                    + "                                     join REQUIREMENT_VERSION b on a.current_version_id=b.res_id "
                    + "                                left join REQUIREMENT_VERSION_COVERAGE c on b.res_id=c.verified_req_version_id "
                    + "                                left join ITERATION_TEST_PLAN_ITEM d on c.verifying_test_case_id=d.tcln_id "
                    + "                                     join HIGH_LEVEL_REQUIREMENT e on a.high_level_requirement_id=e.rln_id "
                    + "                                    where y.res_id in (:requirementVersionIds)  group by 1,2,3,4,5) aa "
                    + "                left join ITERATION_TEST_PLAN_ITEM bb on aa.verifying_test_case_id=bb.tcln_id "
                    + "                                                     and aa.dataset_id=coalesce(bb.dataset_id,0)  "
                    + "                                                     and aa.last_exec=coalesce(bb.last_executed_on, '1970-01-01')";

    public static final String REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_IDS =
            "select a.criticality, a.status, count(*) "
                    + "from ("
                    + REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_ID_BASE_QUERY
                    + ") a "
                    + "group by  a.criticality, a.status;";

    public static final String REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION_BY_VERSION_IDS =
            "Select Distinct a.res_id "
                    + "from ("
                    + REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_ID_BASE_QUERY
                    + ") a "
                    + "Where a.criticality = (:criticality) "
                    + "And a.status In (:validationStatus);";

    /*
     * This query cannot be expressed in hql because the CASE construct doesn't
     * support multiple WHEN.
     *
     * See definition of sct.sizeclass in the CASE WHEN construct.
     */
    public static final String REQUIREMENT_SQL_BOUND_TCS_STATISTICS =
            "Select coverageStats.sizeclass, count(coverageStats.sizeclass) as count "
                    + "From "
                    + "(Select case "
                    + "When count(distinct coverage.tcId) = 0 then 0 "
                    + "When count(distinct coverage.tcId) = 1 then 1 "
                    + "Else 2 "
                    + "End as sizeclass "
                    + "From (Select req.rln_id as reqId, cov.verifying_test_case_id as tcId "
                    + "From REQUIREMENT req "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE cov on req.current_version_id = cov.verified_req_version_id "
                    + "Where req.rln_id in (:requirementIds) "
                    + "Union "
                    + "Select linkedLowLevelReq.high_level_requirement_id as reqId, cov.verifying_test_case_id as tcId "
                    + "From REQUIREMENT linkedLowLevelReq "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE cov on linkedLowLevelReq.current_version_id = cov.verified_req_version_id "
                    + "Where linkedLowLevelReq.high_level_requirement_id in (:requirementIds)) as coverage "
                    + "Group By coverage.reqId) as coverageStats "
                    + "Group By coverageStats.sizeclass";

    public static final String REQUIREMENT_SQL_BOUND_TCS_STATISTICS_BY_VERSION_IDS =
            "Select coverageStats.sizeclass, count(coverageStats.sizeclass) as count "
                    + "From "
                    + "(Select case "
                    + "When count(distinct coverage.tcId) = 0 then 0 "
                    + "When count(distinct coverage.tcId) = 1 then 1 "
                    + "Else 2 "
                    + "End as sizeclass "
                    + "From (Select reqVer.res_id as resId, cov.verifying_test_case_id as tcId "
                    + "From REQUIREMENT_VERSION reqVer "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE cov on reqVer.res_id = cov.verified_req_version_id "
                    + "Where reqVer.res_id in (:requirementVersionIds) "
                    + "Union "
                    + "Select reqVer.res_id as resId, cov.verifying_test_case_id as tcId "
                    + "From REQUIREMENT_VERSION reqVer "
                    + "Left Join REQUIREMENT linkedLowLevelReq on reqVer.requirement_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Left Outer Join REQUIREMENT_VERSION_COVERAGE cov on linkedLowLevelReq.current_version_id = cov.verified_req_version_id "
                    + "Where reqVer.res_id in (:requirementVersionIds)) as coverage "
                    + "Group By coverage.resId) as coverageStats "
                    + "Group By coverageStats.sizeclass";

    // [ISSUE-182] Put back chart which was removed in Squash 4.1
    public static final String LEGACY_REQUIREMENT_SQL_VALIDATION_STATISTICS =
            "Select Selection1.criticality, Selection1.status, count(*) "
                    + "From "
                    + "(Select Distinct req.rln_id as requirement, reqVer.criticality as criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, itpi.execution_status as status, itpi.last_executed_on as execDate "
                    + "From REQUIREMENT as req "
                    + "Inner Join REQUIREMENT_VERSION as reqVer on req.current_version_id = reqVer.res_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where req.rln_id in (:requirementIds) "
                    + "Union "
                    + "Select Distinct req.rln_id as requirement, reqVer.criticality as criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, itpi.execution_status as status, itpi.last_executed_on as execDate "
                    + "From REQUIREMENT as req "
                    + "Inner Join REQUIREMENT_VERSION as reqVer on req.current_version_id = reqVer.res_id "
                    + "Left Join REQUIREMENT as linkedLowLevelReq on req.rln_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where req.rln_id in (:requirementIds)) as Selection1 "
                    + "Inner Join "
                    + "(Select req.rln_id as requirement, reqVer.criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, max(itpi.last_executed_on) as lastDate "
                    + "From REQUIREMENT as req "
                    + "Inner Join REQUIREMENT_VERSION as reqVer on req.current_version_id = reqVer.res_id "
                    + "Left Join REQUIREMENT as linkedLowLevelReq on req.rln_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "OR reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET as dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Inner Join "
                    + "(Select Max(stat.requirement) as requirement, stat.criticality as criticality, stat.testCase as testCase "
                    + "From (Select req.rln_id as requirement, reqVer.criticality as criticality, tc.tcln_id as testCase "
                    + "From REQUIREMENT req "
                    + "Inner Join REQUIREMENT_VERSION as reqVer On req.current_version_id = reqVer.res_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov On reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc On tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Where req.rln_id in (:requirementIds) "
                    + "Union "
                    + "Select req.rln_id as requirement, reqVer.criticality as criticality, tc.tcln_id as testCase "
                    + "From REQUIREMENT req "
                    + "Inner Join REQUIREMENT_VERSION as reqVer On req.current_version_id = reqVer.res_id "
                    + "Left Join REQUIREMENT as linkedLowLevelReq on req.rln_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov On reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Inner Join TEST_CASE as tc On tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Where req.rln_id in (:requirementIds)) as stat "
                    + "Group By criticality, testCase) as NoDuplicateTCByCritSelection "
                    + "On NoDuplicateTCByCritSelection.requirement = req.rln_id "
                    + "And NoDuplicateTCByCritSelection.criticality = reqVer.criticality "
                    + "And NoDuplicateTCByCritSelection.testCase = tc.tcln_id "
                    + "Where req.rln_id in (:requirementIds) "
                    + "Group By req.rln_id, reqVer.criticality, tc.tcln_id, dataset.dataset_id) as LastExecutionSelection "
                    + "On Selection1.requirement = LastExecutionSelection.requirement And Selection1.testCase = LastExecutionSelection.testCase "
                    + "And (Selection1.execDate = LastExecutionSelection.lastDate Or (Selection1.execDate is Null And LastExecutionSelection.lastDate Is Null)) "
                    + "And (Selection1.dataset = LastExecutionSelection.dataset Or (Selection1.dataset is Null And LastExecutionSelection.dataset Is Null)) "
                    + "Group By Selection1.criticality, Selection1.status";

    public static final String LEGACY_REQUIREMENT_SQL_VALIDATION_STATISTICS_BY_VERSION_IDS =
            "Select Selection1.criticality, Selection1.status, count(*) "
                    + "From "
                    + "(Select Distinct reqVer.res_id as requirementVersion, reqVer.criticality as criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, itpi.execution_status as status, itpi.last_executed_on as execDate "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where reqVer.res_id in (:requirementVersionIds) "
                    + "Union "
                    + "Select Distinct reqVer.res_id as requirementVersion, reqVer.criticality as criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, itpi.execution_status as status, itpi.last_executed_on as execDate "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Inner Join REQUIREMENT as req On reqVer.requirement_id = req.rln_id "
                    + "Left Join REQUIREMENT as linkedLowLevelReq on req.rln_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where reqVer.res_id in (:requirementVersionIds)) as Selection1 "
                    + "Inner Join "
                    + "(Select reqVer.res_id as requirementVersion, reqVer.criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, max(itpi.last_executed_on) as lastDate "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Left join REQUIREMENT linkedLowLevelReq ON reqVer.requirement_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "OR reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET as dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Inner Join "
                    + "(Select Max(stat.requirementVersion) as requirementVersion, stat.criticality as criticality, stat.testCase as testCase "
                    + "From (Select reqVer.res_id as requirementVersion, reqVer.criticality as criticality, tc.tcln_id as testCase "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov On reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc On tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Where reqVer.res_id in (:requirementVersionIds) "
                    + "Union "
                    + "Select reqVer.res_id as requirementVersion, reqVer.criticality as criticality, tc.tcln_id as testCase "
                    + "From REQUIREMENT_VERSION as reqVer "
                    + "Inner Join REQUIREMENT req On req.rln_id = reqVer.requirement_id "
                    + "Left Join REQUIREMENT as linkedLowLevelReq on req.rln_id = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov On reqVerCov.verified_req_version_id = linkedLowLevelReq.current_version_id "
                    + "Inner Join TEST_CASE as tc On tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Where reqVer.res_id in (:requirementVersionIds)) as stat "
                    + "Group By criticality, testCase) as NoDuplicateTCByCritSelection "
                    + "On NoDuplicateTCByCritSelection.requirementVersion = reqVer.res_id "
                    + "And NoDuplicateTCByCritSelection.criticality = reqVer.criticality "
                    + "And NoDuplicateTCByCritSelection.testCase = tc.tcln_id "
                    + "Where reqVer.res_id in (:requirementVersionIds) "
                    + "Group By reqVer.res_id, reqVer.criticality, tc.tcln_id, dataset.dataset_id) as LastExecutionSelection "
                    + "On Selection1.requirementVersion = LastExecutionSelection.requirementVersion And Selection1.testCase = LastExecutionSelection.testCase "
                    + "And (Selection1.execDate = LastExecutionSelection.lastDate Or (Selection1.execDate is Null And LastExecutionSelection.lastDate Is Null)) "
                    + "And (Selection1.dataset = LastExecutionSelection.dataset Or (Selection1.dataset is Null And LastExecutionSelection.dataset Is Null)) "
                    + "Group By Selection1.criticality, Selection1.status";
    public static final String LEGACY_REQUIREMENT_SQL_REQUIREMENTS_IDS_FROM_VALIDATION =
            "Select Distinct Selection1.requirement "
                    + "From ( "
                    + "Select Distinct req.rln_id as requirement, reqVer.criticality as criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, Coalesce(itpi.execution_status, 'NOT_FOUND' ) as status, itpi.last_executed_on as execDate "
                    + "From REQUIREMENT as req "
                    + "Inner Join REQUIREMENT_VERSION as reqVer on req.current_version_id = reqVer.res_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where req.rln_id In (:requirementIds) "
                    + "Union "
                    + "Select Distinct highReq.rln_id as requirement, highReqVer.criticality as criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, Coalesce(itpi.execution_status, 'NOT_FOUND' ) as status, itpi.last_executed_on as execDate "
                    + "From REQUIREMENT as highReq "
                    + "Inner Join REQUIREMENT_VERSION highReqVer on highReq.current_version_id = highReqVer.RES_ID "
                    + "inner join HIGH_LEVEL_REQUIREMENT hlr on highReq.RLN_ID = hlr.RLN_ID "
                    + "left join REQUIREMENT linkedLowLevelReq on hlr.RLN_ID = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION as reqVer on linkedLowLevelReq.current_version_id = reqVer.res_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where highReq.RLN_ID In (:requirementIds) "
                    + ") as Selection1 "
                    + "Inner Join "
                    + "(Select LastExecutionSubSelection.requirement as requirement, LastExecutionSubSelection.criticality as criticality, LastExecutionSubSelection.testCase as testCase, LastExecutionSubSelection.dataset as dataset, max(LastExecutionSubSelection.lastDate) as lastDate "
                    + "From "
                    + "(Select req.rln_id as requirement, reqVer.criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, max(itpi.last_executed_on) as lastDate "
                    + "From REQUIREMENT as req "
                    + "Inner Join REQUIREMENT_VERSION as reqVer on req.current_version_id = reqVer.res_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET as dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where req.rln_id In (:requirementIds) "
                    + "Group By req.rln_id, reqVer.criticality, tc.tcln_id, dataset.dataset_id "
                    + "Union "
                    + "Select highReq.rln_id as requirement, highReqVer.criticality, tc.tcln_id as testCase, dataset.dataset_id as dataset, max(itpi.last_executed_on) as lastDate "
                    + "From REQUIREMENT as highReq "
                    + "Inner Join REQUIREMENT_VERSION highReqVer on highReq.current_version_id = highReqVer.RES_ID "
                    + "inner join HIGH_LEVEL_REQUIREMENT hlr on highReq.RLN_ID = hlr.RLN_ID "
                    + "left join REQUIREMENT linkedLowLevelReq on hlr.RLN_ID = linkedLowLevelReq.high_level_requirement_id "
                    + "Inner Join REQUIREMENT_VERSION as reqVer on linkedLowLevelReq.current_version_id = reqVer.res_id "
                    + "Inner Join REQUIREMENT_VERSION_COVERAGE as reqVerCov on reqVerCov.verified_req_version_id = reqVer.res_id "
                    + "Inner Join TEST_CASE as tc on tc.tcln_id = reqVerCov.verifying_test_case_id "
                    + "Left Outer Join ITERATION_TEST_PLAN_ITEM itpi on itpi.tcln_id = tc.tcln_id "
                    + "Left Outer Join DATASET as dataset on dataset.dataset_id = itpi.dataset_id "
                    + "Where highReq.rln_id In (:requirementIds) "
                    + "Group By highReq.rln_id, highReqVer.criticality, tc.tcln_id, dataset.dataset_id "
                    + ") as LastExecutionSubSelection "
                    + "Group By LastExecutionSubSelection.requirement, LastExecutionSubSelection.criticality, LastExecutionSubSelection.testCase, LastExecutionSubSelection.dataset "
                    + ") as LastExecutionSelection "
                    + "On Selection1.requirement = LastExecutionSelection.requirement And Selection1.testCase = LastExecutionSelection.testCase "
                    + "And (Selection1.execDate = LastExecutionSelection.lastDate Or (Selection1.execDate is Null And LastExecutionSelection.lastDate Is Null)) "
                    + "And (Selection1.dataset = LastExecutionSelection.dataset Or (Selection1.dataset is Null And LastExecutionSelection.dataset Is Null)) "
                    + "Where Selection1.criticality = (:criticality) "
                    + "And Selection1.status In (:validationStatus)";

    private NativeQueries() {
        super();
    }
}
