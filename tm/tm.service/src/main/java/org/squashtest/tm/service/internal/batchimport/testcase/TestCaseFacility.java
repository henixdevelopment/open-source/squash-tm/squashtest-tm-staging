/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase;

import static org.squashtest.tm.core.foundation.lang.PathUtils.unescapePathPartSlashes;
import static org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig.BATCH_20;
import static org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig.BATCH_30;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.IsScriptedTestCaseVisitor;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.importer.ImportStatus;
import org.squashtest.tm.service.importer.LogEntry;
import org.squashtest.tm.service.importer.Target;
import org.squashtest.tm.service.infolist.InfoListItemFinderService;
import org.squashtest.tm.service.internal.batchimport.AbstractEntityFacilitySupport;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.batchimport.Batches;
import org.squashtest.tm.service.internal.batchimport.CallStepParamsInfo;
import org.squashtest.tm.service.internal.batchimport.DatasetParametersValueImportData;
import org.squashtest.tm.service.internal.batchimport.Existence;
import org.squashtest.tm.service.internal.batchimport.FacilityImpl;
import org.squashtest.tm.service.internal.batchimport.FacilityImplHelper;
import org.squashtest.tm.service.internal.batchimport.FacilityUtils;
import org.squashtest.tm.service.internal.batchimport.LogTrain;
import org.squashtest.tm.service.internal.batchimport.Messages;
import org.squashtest.tm.service.internal.batchimport.TargetStatus;
import org.squashtest.tm.service.internal.batchimport.instruction.ActionStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.CallStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetParamValueInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.Instruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.ActionStepImportData;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.CallStepImportData;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseFolderImportData;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseImportData;
import org.squashtest.tm.service.internal.batchimport.testcase.tree.FolderNode;
import org.squashtest.tm.service.internal.batchimport.testcase.tree.FolderTree;
import org.squashtest.tm.service.internal.batchimport.testcase.tree.MissingNode;
import org.squashtest.tm.service.internal.importer.ExcelRowReaderUtils;
import org.squashtest.tm.service.internal.library.LibraryUtils;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.DatasetParamValueDao;
import org.squashtest.tm.service.internal.repository.ParameterDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.testcase.CallStepManagerService;
import org.squashtest.tm.service.testcase.DatasetModificationService;
import org.squashtest.tm.service.testcase.ParameterModificationService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;

/**
 * @author Gregory Fouquet
 * @since 1.14.0 04/05/16
 */
@Component
@Scope("prototype")
public class TestCaseFacility extends AbstractEntityFacilitySupport {
    private static final Logger LOGGER = LoggerFactory.getLogger(FacilityImpl.class);
    private final FacilityImplHelper helper = new FacilityImplHelper(this);

    @Inject private InfoListItemFinderService listItemFinderService;
    @Inject private TestCaseLibraryNavigationService navigationService;
    @Inject private TestCaseModificationService testcaseModificationService;
    @Inject private ProjectDao projectDao;

    @Inject private CallStepManagerService callstepService;
    @Inject private ParameterModificationService parameterService;
    @Inject private DatasetModificationService datasetService;

    @Inject private DatasetDao datasetDao;
    @Inject private DatasetParamValueDao paramvalueDao;
    @Inject private ParameterDao paramDao;

    @Inject private TestCaseImportService testCaseImportService;

    @PersistenceContext private EntityManager entityManager;

    public void createTestCases(List<TestCaseInstruction> instructions, Project project) {
        List<TestCaseImportData> testCaseInFolders = new ArrayList<>();

        Batch<TestCaseImportData> libraryTestCases = new Batch<>(project.getTestCaseLibrary().getId());

        for (TestCaseInstruction instr : instructions) {
            TestCaseImportData importTestCase = initTestCaseImportData(instr);

            TestCaseTarget target = instr.getTarget();
            if (target.isRootTestCase()) {
                libraryTestCases.addEntity(importTestCase);
            } else {
                testCaseInFolders.add(importTestCase);
            }
        }

        createTestCasesLinkToLibrary(project, libraryTestCases);

        createTestCasesLinkToFolders(project, testCaseInFolders);
    }

    private TestCaseImportData initTestCaseImportData(TestCaseInstruction instr) {
        TestCase testCase = instr.getTestCase();
        TestCaseTarget target = instr.getTarget();
        testCase.setName(unescapePathPartSlashes(target.getName()));

        Map<String, String> cufValues = instr.getCustomFields();

        helper.fillNullWithDefaults(testCase);
        helper.truncate(testCase, cufValues);

        Map<Long, RawValue> acceptableCufs = toAcceptableCufs(cufValues);

        List<Long> milestoneIds = boundMilestonesIds(instr);

        TestCaseImportData importTestCase = new TestCaseImportData(testCase, target);

        if (!acceptableCufs.isEmpty()) {
            importTestCase.setCustomFields(acceptableCufs);
        }

        if (!milestoneIds.isEmpty()) {
            importTestCase.setMilestoneIds(milestoneIds);
        }

        return importTestCase;
    }

    private void createTestCasesLinkToLibrary(Project project, Batch<TestCaseImportData> batch) {
        List<TestCaseImportData> importedTestCases = batch.getEntities();

        if (importedTestCases.isEmpty()) {
            return;
        }

        Long libraryId = batch.getTargetId();

        List<String> contentNames = navigationService.findContentNamesByLibraryId(libraryId);
        fixConflictNames(contentNames, importedTestCases);

        testCaseImportService.addTestCasesToLibrary(libraryId, batch, project);

        importedTestCases.forEach(this::validatingTestCaseCreation);
    }

    private static void fixConflictNames(
            List<String> contentNames, List<TestCaseImportData> importData) {
        if (importData != null && !importData.isEmpty()) {
            importData.stream()
                    .map(TestCaseImportData::getTestCase)
                    .forEach(
                            testCase -> {
                                String newName =
                                        LibraryUtils.generateNonClashingName(
                                                testCase.getName(), contentNames, Sizes.NAME_MAX);
                                if (!newName.equals(testCase.getName())) {
                                    testCase.setName(newName);
                                }
                                contentNames.add(newName);
                            });
        }
    }

    private void createTestCasesLinkToFolders(
            Project project, List<TestCaseImportData> folderTestCases) {

        Batches<TestCaseImportData> batches =
                collectExistingNodesAndCreateMissing(project.getName(), folderTestCases);

        if (batches == null) {
            return;
        }

        fixConflictNamesInFolders(batches);

        for (List<Batch<TestCaseImportData>> batchList : batches.partition(BATCH_30)) {
            List<Long> currentFolderIds = batchList.stream().map(Batch::getTargetId).toList();

            testCaseImportService.addTestCasesToFolders(currentFolderIds, project, batchList);
            batchList.stream()
                    .flatMap(b -> b.getEntities().stream())
                    .forEach(this::validatingTestCaseCreation);
        }
    }

    private void validatingTestCaseCreation(TestCaseImportData testCaseData) {
        validator.getModel().setExists(testCaseData.getTarget(), testCaseData.getTestCase().getId());
    }

    private void fixConflictNamesInFolders(Batches<TestCaseImportData> batches) {
        Map<Long, List<String>> folderContentNames =
                navigationService.findContentNamesByFolderIds(batches.getTargetIds());

        for (var entry : folderContentNames.entrySet()) {
            List<TestCaseImportData> importData = batches.getEntitiesByTargetId(entry.getKey());
            fixConflictNames(entry.getValue(), importData);
        }
    }

    private Batches<TestCaseImportData> collectExistingNodesAndCreateMissing(
            String projectName, List<TestCaseImportData> folderTestCases) {
        Map<String, List<TestCaseImportData>> importDataByFolderPath =
                folderTestCases.stream()
                        .collect(Collectors.groupingBy(instruction -> instruction.getTarget().getFolder()));

        if (importDataByFolderPath.isEmpty()) {
            return null;
        }

        FolderTree tree = new FolderTree(projectName, importDataByFolderPath);

        TreeMap<String, Long> projectPathIdTree = navigationService.buildTestCasePathsTree(projectName);

        Long testCaseLibraryId = projectDao.findTestCaseLibraryIdByName(projectName);

        List<MissingNode> missingNodes = tree.collectMissingNodes(projectPathIdTree, testCaseLibraryId);

        createMissingNodes(testCaseLibraryId, missingNodes);

        return tree.collectExistingNodes(projectPathIdTree);
    }

    private void createMissingNodes(Long testCaseLibraryId, List<MissingNode> missingNodes) {
        if (missingNodes.isEmpty()) {
            return;
        }

        Batch<TestCaseFolderImportData> libraryFolders = new Batch<>(testCaseLibraryId);
        Batches<TestCaseFolderImportData> folderBatch = new Batches<>();

        for (MissingNode missingNode : missingNodes) {
            FolderNode node = missingNode.node();
            TestCaseFolderImportData folderImportData =
                    new TestCaseFolderImportData(
                            node.convertToSquashFolder(), node.collectAllTestCaseImportData());

            if (missingNode.isLibraryContent()) {
                libraryFolders.addEntity(folderImportData);
            } else {
                folderBatch.addBatch(missingNode.parentId(), folderImportData);
            }
        }

        if (!libraryFolders.getEntities().isEmpty()) {
            testCaseImportService.addFoldersToLibrary(testCaseLibraryId, libraryFolders);
            libraryFolders.getEntities().stream()
                    .flatMap(b -> b.testCaseDataInNodes().stream())
                    .forEach(this::validatingTestCaseCreation);
        }

        for (List<Batch<TestCaseFolderImportData>> batch : folderBatch.partition(BATCH_20)) {
            List<Long> targetIds = batch.stream().map(Batch::getTargetId).toList();
            testCaseImportService.addFoldersToFolders(targetIds, batch);
            batch.stream()
                    .flatMap(b -> b.getEntities().stream())
                    .flatMap(b -> b.testCaseDataInNodes().stream())
                    .forEach(this::validatingTestCaseCreation);
        }
    }

    /**
     * May be called either by the create test case scenario, or in an update scenario (business says
     * that updating a test case that dont exist implies to create it first).
     */
    private void createTCRoutine(LogTrain train, TestCaseInstruction instruction) {
        TestCase testCase = instruction.getTestCase();
        TestCaseTarget target = instruction.getTarget();

        try {
            Project project = projectDao.findByName(target.getProject());
            createTestCases(Collections.singletonList(instruction), project);
            validator.getModel().setExists(target, testCase.getId());

            LOGGER.debug(EXCEL_ERR_PREFIX + "Created Test Case \t'" + target + "'");

        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            validator.getModel().setNotExists(target);
            LOGGER.error(EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_WHILE_IMPORTING + target + " : ", ex);
        }
    }

    private void fixNatureAndType(TestCaseTarget target, TestCase testCase) {

        // at this point of the process the target is assumed to be safe for
        // use,
        // no need to defensively check that the project exists and such
        TargetStatus projectStatus = validator.getModel().getProjectStatus(target.getProject());

        InfoListItem nature = testCase.getNature();
        if (nature != null
                && !listItemFinderService.isNatureConsistent(projectStatus.getId(), nature.getCode())) {
            testCase.setNature(listItemFinderService.findDefaultTestCaseNature(projectStatus.getId()));
        }

        InfoListItem type = testCase.getType();
        if (type != null
                && !listItemFinderService.isTypeConsistent(projectStatus.getId(), type.getCode())) {
            testCase.setType(listItemFinderService.findDefaultTestCaseType(projectStatus.getId()));
        }
    }

    /**
     * @param instr instruction read from import file, pointing to a TRANSIENT test case template
     * @param persistentSource the PERSISTENT test case
     */
    private void rebindMilestones(TestCaseInstruction instr, TestCase persistentSource) {
        if (!instr.getMilestones().isEmpty()) {
            List<Milestone> ms = milestoneHelper.findBindable(instr.getMilestones());
            persistentSource.getMilestones().clear();
            persistentSource.bindAllMilsetones(ms);
        }
        // feat 5169 if milestone cell is empty in xls import file, unbind all milestones
        else {
            persistentSource.getMilestones().clear();
        }
    }

    public LogTrain updateTestCase(TestCaseInstruction instr) {
        TestCaseTarget target = instr.getTarget();
        TestCase testCase = instr.getTestCase();
        Map<String, String> cufValues = instr.getCustomFields();

        TargetStatus status = validator.getModel().getStatus(target);

        LogTrain train = validator.updateTestCase(instr);

        if (train.hasCriticalErrors()) {
            return train;
        }

        if (status.getStatus() == Existence.NOT_EXISTS) {
            createTCRoutine(train, instr);
        } else {
            try {

                helper.truncate(testCase, cufValues);
                fixNatureAndType(target, testCase);

                doUpdateTestcase(instr);

                LOGGER.debug(EXCEL_ERR_PREFIX + "Updated Test Case \t'" + target + "'");

            } catch (Exception ex) {
                train.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                                .build());
                LOGGER.error(EXCEL_ERR_PREFIX + "unexpected error while updating " + target + " : ", ex);
            }
        }

        return train;
    }

    private void doUpdateTestcase(TestCaseInstruction instr) {
        TestCaseTarget target = instr.getTarget();
        TestCase testCase = instr.getTestCase();
        Map<String, String> cufValues = instr.getCustomFields();

        TestCase orig = validator.getModel().get(target);
        Long origId = orig.getId();

        // update the custom field values

        doUpdateCustomFields(cufValues, orig);

        if (validator.areMilestoneValid(instr)) {
            rebindMilestones(instr, orig);
        }

        /*
         * Issue #6968 : Because renaming / changing the ref of a test case triggers an immediate reindexation
         * of the related ITPIs (and the test case itself by the way). In the process the session attached to
         * the persistent collection of the milestones is killed, not sure why, thus triggering the
         * lazy exception.
         *
         *  The hack to prevent this is to make sure the indexation happens last, which here can be done
         *  by updating the core attributes to the last position.
         */
        // update the test case core attributes last

        doUpdateTestCaseCoreAttributes(testCase, orig);
        doUpdateTestCaseScriptIfScripted(testCase, orig);

        // move the test case if its index says it has to move
        Integer order = target.getOrder();

        if (order != null && order > -1 && order < navigationService.countSiblingsOfNode(origId)) {
            final Long[] origIds = new Long[] {origId};
            final ClipboardPayload clipboardPayload =
                    ClipboardPayload.withWhiteListIgnored(Collections.singletonList(origId));

            if (target.isRootTestCase()) {
                Long libraryId =
                        validator.getModel().getProjectStatus(target.getProject()).getTestCaseLibraryId();
                navigationService.moveNodesToLibrary(libraryId, origIds, order, clipboardPayload);
            } else {
                Long folderId = navigationService.findNodeIdByPath(target.getFolder());
                navigationService.moveNodesToFolder(folderId, origIds, order, clipboardPayload);
            }
        }
    }

    private void doUpdateTestCaseScriptIfScripted(TestCase testCase, TestCase orig) {
        boolean isTestCaseScripted = isTestCaseScripted(testCase);
        boolean isOrigScripted = isTestCaseScripted(orig);

        if (isTestCaseScripted && isOrigScripted) {
            String newScript = ((ScriptedTestCase) testCase).getScript();
            if (StringUtils.isNoneBlank(newScript)) {
                ((ScriptedTestCase) orig).setScript(newScript);
            }
        }
    }

    private boolean isTestCaseScripted(TestCase testCase) {
        IsScriptedTestCaseVisitor testCaseVisitor = new IsScriptedTestCaseVisitor();
        testCase.accept(testCaseVisitor);
        return testCaseVisitor.isScripted();
    }

    private void doUpdateTestCaseCoreAttributes(TestCase testCase, TestCase orig) {
        Long origId = orig.getId();
        final boolean newImportanceAuto =
                testCase.isImportanceAuto() != null && testCase.isImportanceAuto();

        updateName(orig, testCase.getName(), origId);
        updateReference(orig, testCase.getReference(), origId);
        updateDescription(orig, testCase.getDescription(), origId);
        updatePrerequisite(orig, testCase.getPrerequisite(), origId);
        updateImportance(testCase, orig, newImportanceAuto, origId);
        updateInfoListNature(orig, testCase.getNature(), origId);
        updateInfoListType(orig, testCase.getType(), origId);
        updateStatus(orig, testCase.getStatus(), origId);

        // WARNING : automation request is not created in import yet, so don't need to update for now

        final boolean importanceChanged =
                orig.isImportanceAuto() != null && orig.isImportanceAuto().equals(newImportanceAuto);

        // If we go "auto", we call the service method even if the value did not change so that we're
        // sure the importance value will be set accordingly.
        if (importanceChanged || newImportanceAuto) {
            testcaseModificationService.changeImportanceAuto(origId, newImportanceAuto);
        }
    }

    private void updateStatus(TestCase orig, TestCaseStatus newStatus, Long origId) {
        if (newStatus != null && orig.getStatus() != newStatus) {
            testcaseModificationService.changeStatus(origId, newStatus);
        }
    }

    private void updateName(TestCase orig, String newName, Long origId) {
        if (!StringUtils.isBlank(newName) && !newName.equals(orig.getName())) {
            testcaseModificationService.rename(origId, newName);
        }
    }

    private void updateReference(TestCase orig, String newRef, Long origId) {
        if (!StringUtils.isBlank(newRef) && !newRef.equals(orig.getReference())) {
            testcaseModificationService.changeReference(origId, newRef);
        }
    }

    private void updateDescription(TestCase orig, String newDesc, Long origId) {
        if (!StringUtils.isBlank(newDesc) && !newDesc.equals(orig.getDescription())) {
            testcaseModificationService.changeDescription(
                    origId, ExcelRowReaderUtils.escapeHTMLInsideTags(newDesc));
        }
    }

    private void updatePrerequisite(TestCase orig, String newPrereq, Long origId) {
        if (!StringUtils.isBlank(newPrereq) && !newPrereq.equals(orig.getPrerequisite())) {
            testcaseModificationService.changePrerequisite(
                    origId, ExcelRowReaderUtils.escapeHTMLInsideTags(newPrereq));
        }
    }

    private void updateImportance(
            TestCase testCase, TestCase orig, boolean newImportanceAuto, Long origId) {
        TestCaseImportance newImp = testCase.getImportance();
        if (!newImportanceAuto && newImp != null && orig.getImportance() != newImp) {
            testcaseModificationService.changeImportance(origId, newImp);
        }
    }

    private void updateInfoListNature(TestCase orig, InfoListItem newNat, Long origId) {
        if (newNat != null && !newNat.references(orig.getNature())) {
            testcaseModificationService.changeNature(origId, newNat.getCode());
        }
    }

    private void updateInfoListType(TestCase orig, InfoListItem newType, Long origId) {
        if (newType != null && !newType.references(orig.getType())) {
            testcaseModificationService.changeType(origId, newType.getCode());
        }
    }

    public LogTrain deleteTestCase(TestCaseTarget target) {

        LogTrain train = validator.deleteTestCase(target);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {

            doDeleteTestCase(target);
            validator.getModel().setDeleted(target);

            LOGGER.debug(EXCEL_ERR_PREFIX + "Deleted Test Case \t'" + target + "'");

        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(EXCEL_ERR_PREFIX + "unexpected error while deleting " + target + " : ", ex);
        }

        return train;
    }

    private void doDeleteTestCase(TestCaseTarget target) {
        TestCase tc = validator.getModel().get(target);
        navigationService.deleteNodes(Collections.singletonList(tc.getId()));
    }

    public LogTrain updateActionStep(
            TestStepTarget target, ActionTestStep testStep, Map<String, String> cufValues) {

        LogTrain train = validator.updateActionStep(target, testStep, cufValues);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {
            helper.truncate(cufValues);
            doUpdateActionStep(target, testStep, cufValues);

            LOGGER.debug(EXCEL_ERR_PREFIX + "Updated Action Step \t'" + target + "'");
        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(EXCEL_ERR_PREFIX + "unexpected error while updating step " + target + " : ", ex);
        }

        return train;
    }

    private void doUpdateActionStep(
            TestStepTarget target, ActionTestStep testStep, Map<String, String> cufValues) {

        // update the step
        ActionTestStep orig = (ActionTestStep) validator.getModel().getStep(target);

        String newAction = testStep.getAction();
        if (!StringUtils.isBlank(newAction) && !newAction.equals(orig.getAction())) {
            orig.setAction(ExcelRowReaderUtils.escapeHTMLInsideTags(newAction));
        }

        String newResult = testStep.getExpectedResult();
        if (!StringUtils.isBlank(newResult) && !newResult.equals(orig.getExpectedResult())) {
            orig.setExpectedResult(ExcelRowReaderUtils.escapeHTMLInsideTags(newResult));
        }

        // the custom field values now
        doUpdateCustomFields(cufValues, orig);
    }

    public LogTrain updateCallStep(
            TestStepTarget target,
            CallTestStep testStep,
            TestCaseTarget calledTestCase,
            CallStepParamsInfo paramInfo,
            ActionTestStep actionStepBackup) {

        LogTrain train =
                validator.updateCallStep(target, testStep, calledTestCase, paramInfo, actionStepBackup);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {
            doUpdateCallStep(target, calledTestCase, paramInfo);
            validator.getModel().updateCallStepTarget(target, calledTestCase, paramInfo);

            LOGGER.debug(
                    EXCEL_ERR_PREFIX + "Created Call Step \t'" + target + "' -> '" + calledTestCase + "'");
        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(EXCEL_ERR_PREFIX + "unexpected error while updating step " + target + " : ", ex);
        }

        return train;
    }

    private void doUpdateCallStep(
            TestStepTarget target, TestCaseTarget calledTestCase, CallStepParamsInfo paramInfo) {

        // update the step
        TestStep actualStep = validator.getModel().getStep(target);
        TestCase newCalled = validator.getModel().get(calledTestCase);
        callstepService.checkForCyclicStepCallBeforePaste(
                actualStep.getTestCase().getId(), newCalled.getId());
        ((CallTestStep) actualStep).setCalledTestCase(newCalled);

        // update the parameter assignation
        changeParameterAssignation(actualStep.getId(), calledTestCase, paramInfo);
    }

    private void changeParameterAssignation(
            Long stepId, TestCaseTarget tc, CallStepParamsInfo paramInfo) {
        Long dsId = null;
        ParameterAssignationMode mode = paramInfo.getParamMode();

        if (paramInfo.getParamMode() == ParameterAssignationMode.CALLED_DATASET) {

            Long tcid = validator.getModel().getId(tc);
            String dsname = helper.truncate(paramInfo.getCalledDatasetName());
            Dataset ds = datasetDao.findByTestCaseIdAndName(tcid, dsname);

            // if the dataset exists we can actually bind the step to it.
            // otherwise we fallback to the default mode (nothing).
            // This later case has been dutifully reported by the
            // validator facility of course.
            if (ds != null) {
                dsId = ds.getId();
            } else {
                mode = ParameterAssignationMode.NOTHING;
            }
        }
        callstepService.setParameterAssignationMode(stepId, mode, dsId);
    }

    public LogTrain deleteTestStep(TestStepTarget target) {

        LogTrain train = validator.deleteTestStep(target);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {
            doDeleteTestStep(target);
            validator.getModel().remove(target);

            LOGGER.debug(EXCEL_ERR_PREFIX + "Deleted Step \t'" + target + "'");

        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(EXCEL_ERR_PREFIX + "unexpected error while deleting step " + target + " : ", ex);
        }

        return train;
    }

    private void doDeleteTestStep(TestStepTarget target) {
        TestCase tc = validator.getModel().get(target.getTestCase());
        testcaseModificationService.removeStepFromTestCaseByIndex(tc.getId(), target.getIndex());
    }

    public LogTrain updateParameter(ParameterTarget target, Parameter param) {

        LogTrain train = validator.updateParameter(target, param);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {
            doUpdateParameter(target, param);
            validator.getModel().addParameter(target); // create the parameter if didn't exist already.
            // Double-insertion proof.

            LOGGER.debug(EXCEL_ERR_PREFIX + "Updated Parameter \t'" + target + "'");
        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(
                    EXCEL_ERR_PREFIX + "unexpected error while updating parameter " + target + " : ", ex);
        }

        return train;
    }

    private void doUpdateParameter(ParameterTarget target, Parameter param) {
        if (!validator.getModel().doesParameterExists(target)) {
            Long testcaseId = validator.getModel().getId(target.getOwner());
            helper.fillNullWithDefaults(param);
            helper.truncate(param);
            parameterService.addNewParameterToTestCase(param, testcaseId);
        } else {
            String description = param.getDescription();
            if (description != null) {
                findParameter(target).setDescription(description);
            }
        }
    }

    public LogTrain deleteParameter(ParameterTarget target) {

        LogTrain train = validator.deleteParameter(target);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {
            doDeleteParameter(target);
            validator.getModel().removeParameter(target);

            LOGGER.debug(EXCEL_ERR_PREFIX + "Deleted Parameter \t'" + target + "'");
        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(
                    EXCEL_ERR_PREFIX + "unexpected error while deleting parameter " + target + " : ", ex);
        }

        return train;
    }

    private void doDeleteParameter(ParameterTarget target) {
        Long testcaseId = validator.getModel().getId(target.getOwner());
        List<Parameter> allparams = parameterService.findAllParameters(testcaseId);

        Parameter param = null;
        for (Parameter p : allparams) {
            if (p.getName().equals(target.getName())) {
                param = p;
                break;
            }
        }

        parameterService.remove(param);
    }

    public LogTrain failsafeUpdateParameterValue(
            DatasetTarget dataset, ParameterTarget param, String value, boolean isUpdate) {

        LogTrain train = validator.failsafeUpdateParameterValue(dataset, param, value, isUpdate);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {
            doFailsafeUpdateParameterValue(dataset, param, value);

            validator.getModel().addDataset(dataset);

            LOGGER.debug(
                    EXCEL_ERR_PREFIX
                            + "Updated Param Value for param \t'"
                            + param
                            + "' in dataset '"
                            + dataset
                            + "'");
        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(dataset)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(
                    EXCEL_ERR_PREFIX
                            + "unexpected error while setting parameter "
                            + param
                            + " in dataset "
                            + dataset
                            + " : ",
                    ex);
        }

        return train;
    }

    private void doFailsafeUpdateParameterValue(
            DatasetTarget dataset, ParameterTarget param, String value) {
        DatasetParamValue dpv = findParamValue(dataset, param);
        String trValue = helper.truncate(value);
        dpv.setParamValue(trValue);
    }

    private DatasetParamValue findParamValue(DatasetTarget dataset, ParameterTarget param) {

        Dataset dbDs = findOrCreateDataset(dataset);
        Parameter dsParam = findParameter(param);

        for (DatasetParamValue dpv : dbDs.getParameterValues()) {
            if (dpv.getParameter().equals(dsParam)) {
                return dpv;
            }
        }

        // else we have to create it. Note that the services do not provide any
        // facility for that
        // so we have to do it from scratch here. Tsss, lazy conception again.
        DatasetParamValue dpv = new DatasetParamValue(dsParam, dbDs);
        paramvalueDao.save(dpv);
        dbDs.addParameterValue(dpv);

        return dpv;
    }

    private Parameter findParameter(ParameterTarget param) {
        Long testcaseId = validator.getModel().getId(param.getOwner());

        Parameter found = paramDao.findOwnParameterByNameAndTestCase(param.getName(), testcaseId);

        if (found != null) {
            return found;
        } else {
            throw new NoSuchElementException("parameter " + param + " could not be found");
        }
    }

    public LogTrain deleteDataset(DatasetTarget dataset) {

        LogTrain train = validator.deleteDataset(dataset);

        if (train.hasCriticalErrors()) {
            return train;
        }

        try {
            doDeleteDataset(dataset);

            validator.getModel().removeDataset(dataset);

            LOGGER.debug(EXCEL_ERR_PREFIX + "Deleted Dataset '" + dataset + "'");

        } catch (Exception ex) {
            train.addEntry(
                    LogEntry.failure()
                            .forTarget(dataset)
                            .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                            .build());
            LOGGER.error(
                    EXCEL_ERR_PREFIX + "unexpected error while deleting dataset " + dataset + " : ", ex);
        }

        return train;
    }

    private void doDeleteDataset(DatasetTarget dataset) {
        Dataset ds = findOrCreateDataset(dataset);
        TestCase tc = ds.getTestCase();
        tc.removeDataset(ds);
        datasetService.remove(ds);
    }

    /**
     * @return the found Dataset or a new one (non null value)
     */
    private Dataset findOrCreateDataset(DatasetTarget dataset) {
        Long tcid = validator.getModel().getId(dataset.getTestCase());

        String truncated = helper.truncate(dataset.getName());
        Dataset found = datasetDao.findByTestCaseIdAndName(tcid, truncated);

        if (found != null) {
            return found;
        } else {
            Dataset newds = new Dataset();
            newds.setName(dataset.getName());
            helper.fillNullWithDefaults(newds);
            helper.truncate(newds);
            datasetService.persist(newds, tcid);

            LOGGER.debug(EXCEL_ERR_PREFIX + "Created Dataset \t'" + dataset + "'");

            return newds;
        }
    }

    // --------------------- Batch operations ---------------------

    public void createParameters(List<ParameterInstruction> instructions) {
        Batches<Parameter> batchToCreate = new Batches<>();
        Batches<ParameterInstruction> batchToUpdate = new Batches<>();

        instructions.forEach(
                instr -> {
                    Parameter parameter = instr.getParameter();
                    ParameterTarget target = instr.getTarget();
                    Long testcaseId = validator.getModel().getId(target.getOwner());

                    if (validator.getModel().doesParameterExists(target)) {
                        batchToUpdate.addBatch(testcaseId, instr);
                    } else {
                        helper.fillNullWithDefaults(parameter);
                        helper.truncate(parameter);
                        batchToCreate.addBatch(testcaseId, parameter);
                    }
                });

        addMissingParameters(batchToCreate);
        updateExistingParameters(batchToUpdate);

        instructions.forEach(
                instruction -> {
                    validator.getModel().addParameter(instruction.getTarget());
                    LOGGER.debug(EXCEL_ERR_PREFIX + "Created Parameter \t'" + instruction.getTarget() + "'");
                });
    }

    private void addMissingParameters(Batches<Parameter> batch) {
        if (batch.isEmpty()) {
            return;
        }

        for (List<Batch<Parameter>> batchList : batch.partition(BATCH_30)) {
            parameterService.batchParameterAddition(batchList);
        }
    }

    private void updateExistingParameters(Batches<ParameterInstruction> batch) {
        if (batch.isEmpty()) {
            return;
        }

        for (List<Batch<ParameterInstruction>> batchList : batch.partition(BATCH_20)) {
            parameterService.updateImportParameters(batchList);
        }
    }

    public void createDatasets(List<DatasetInstruction> instructions) {
        Map<Long, List<DatasetTarget>> targetsByTestCaseIds =
                instructions.stream()
                        .map(Instruction::getTarget)
                        .collect(
                                Collectors.groupingBy(
                                        target -> validator.getModel().getId(target.getTestCase()),
                                        Collectors.toList()));

        Map<Long, List<String>> datasetsNamesByTestCaseId =
                datasetDao.findOwnDatasetNamesByTestCaseIds(targetsByTestCaseIds.keySet());

        Batches<Dataset> batch =
                collectMissingDatasets(targetsByTestCaseIds, datasetsNamesByTestCaseId);

        for (List<Batch<Dataset>> batchList : batch.partition(BATCH_20)) {
            List<Long> testCaseIds = batchList.stream().map(Batch::getTargetId).toList();
            datasetService.persistBatch(testCaseIds, batchList);
        }
    }

    protected void addUnexpectedError(LogTrain train, Target target, Throwable ex) {
        train.addEntry(
                LogEntry.failure()
                        .forTarget(target)
                        .withMessage(Messages.ERROR_UNEXPECTED_ERROR, ex.getClass().getName())
                        .build());
    }

    private Batches<Dataset> collectMissingDatasets(
            Map<Long, List<DatasetTarget>> targetsByTestCaseIds,
            Map<Long, List<String>> datasetsNamesByTestCaseId) {
        Batches<Dataset> batch = new Batches<>();

        for (var entry : targetsByTestCaseIds.entrySet()) {
            List<DatasetTarget> targets = entry.getValue();
            Long testCaseId = entry.getKey();
            List<String> collectDatasetNames = new ArrayList<>();

            List<String> existingDatasets =
                    datasetsNamesByTestCaseId.getOrDefault(testCaseId, Collections.emptyList());

            for (DatasetTarget datasetTarget : targets) {
                String truncatedName = helper.truncate(datasetTarget.getName());
                if (!existingDatasets.contains(truncatedName)
                        && !collectDatasetNames.contains(truncatedName)) {
                    Dataset dataset = new Dataset();
                    dataset.setName(datasetTarget.getName());
                    helper.fillNullWithDefaults(dataset);
                    helper.truncate(dataset);
                    batch.addBatch(testCaseId, dataset);
                    collectDatasetNames.add(truncatedName);
                }
            }
        }

        return batch;
    }

    public void addActionSteps(List<ActionStepInstruction> instructions, Project project) {
        Batches<ActionStepImportData> batches = batchGroupingActionSteps(instructions);

        createBatchActionTestSteps(project, batches);
    }

    private void createBatchActionTestSteps(Project project, Batches<ActionStepImportData> batches) {
        if (batches.isEmpty()) {
            return;
        }

        for (var batchList : batches.partition(BATCH_30)) {
            List<Long> testCaseIds = batchList.stream().map(Batch::getTargetId).toList();
            testcaseModificationService.addImportActionSteps(project, testCaseIds, batchList);
            batchList.stream()
                    .flatMap(b -> b.getEntities().stream())
                    .map(ActionStepImportData::getTarget)
                    .forEach(target -> validator.getModel().addActionStep(target));
        }
    }

    private Batches<ActionStepImportData> batchGroupingActionSteps(
            List<ActionStepInstruction> instructions) {
        Batches<ActionStepImportData> batches = new Batches<>();

        for (ActionStepInstruction instruction : instructions) {
            ActionTestStep testStep = instruction.getTestStep();
            helper.fillNullWithDefaults(testStep);

            Map<String, String> customFields = instruction.getCustomFields();
            helper.truncate(customFields);
            Map<Long, RawValue> acceptableCustomFields = toAcceptableCufs(customFields);

            TestStepTarget target = instruction.getTarget();
            Long testCaseId = validator.getModel().getId(target.getTestCase());

            ActionStepImportData testStepImportData =
                    new ActionStepImportData(testStep, target, target.getIndex());
            testStepImportData.addCustomFields(acceptableCustomFields);

            batches.addBatch(testCaseId, testStepImportData);
        }

        return batches;
    }

    public void addCallSteps(List<CallStepInstruction> instructions, Project project) {
        validator.addCallSteps(instructions, project);

        Batches<ActionStepImportData> batchesActionStep = new Batches<>();
        Batches<CallStepImportData> batchesCallStep = new Batches<>();

        instructions.stream()
                .filter(i -> !i.hasCriticalErrors())
                .forEach(i -> callStepInstructionDistribution(i, batchesActionStep, batchesCallStep));

        try {
            createBatchActionTestSteps(project, batchesActionStep);
            createBatchCallTestSteps(project, batchesCallStep);
        } catch (Exception ex) {
            instructions.stream()
                    .filter(i -> !i.hasCriticalErrors())
                    .forEach(
                            i ->
                                    i.addLogEntry(
                                            ImportStatus.FAILURE,
                                            Messages.ERROR_UNEXPECTED_ERROR,
                                            null,
                                            ex.getClass().getName()));

            LOGGER.error(
                    EXCEL_ERR_PREFIX + UNEXPECTED_ERROR_DURING_CREATION, "call steps", project.getName(), ex);
        }
    }

    private void createBatchCallTestSteps(Project project, Batches<CallStepImportData> batches) {
        if (batches.isEmpty()) {
            return;
        }

        for (var batchList : batches.partition(BATCH_30)) {
            List<Long> testCaseIds = batchList.stream().map(Batch::getTargetId).toList();
            callstepService.addImportCallSteps(project, testCaseIds, batchList);
            batchList.stream()
                    .flatMap(b -> b.getEntities().stream())
                    .forEach(
                            data ->
                                    validator
                                            .getModel()
                                            .addCallStep(
                                                    data.getTarget(),
                                                    data.getCalledTestCaseTarget(),
                                                    data.getParameterMode()));
        }
    }

    private void callStepInstructionDistribution(
            CallStepInstruction instruction,
            Batches<ActionStepImportData> actionStepBatches,
            Batches<CallStepImportData> callBatches) {
        TestStepTarget target = instruction.getTarget();

        Long testCaseId = validator.getModel().getId(target.getTestCase());

        String mustImportCallAsActionStepErrorI18n =
                FacilityUtils.mustImportCallAsActionStep(instruction.getLogTrain());

        if (mustImportCallAsActionStepErrorI18n != null) {
            ActionTestStep testStep = instruction.getActionStepBackup();
            helper.fillNullWithDefaults(testStep);

            ActionStepImportData testStepImportData =
                    new ActionStepImportData(testStep, target, target.getIndex());

            actionStepBatches.addBatch(testCaseId, testStepImportData);
        } else {
            Long calledTestCaseId = validator.getModel().getId(instruction.getCalledTC());
            CallStepParamsInfo paramsInfo = instruction.getDatasetInfo();

            CallStepImportData callStepImportData =
                    new CallStepImportData(
                            calledTestCaseId,
                            instruction.getCalledTC(),
                            paramsInfo.getParamMode(),
                            helper.truncate(paramsInfo.getCalledDatasetName()),
                            target);

            callBatches.addBatch(testCaseId, callStepImportData);
        }
    }

    public void addDatasetParametersValues(List<DatasetParamValueInstruction> instructions) {

        Map<DatasetTarget, List<DatasetParamValueInstruction>> instructionsByDataset =
                instructions.stream()
                        .collect(Collectors.groupingBy(Instruction::getTarget, Collectors.toList()));

        Batches<DatasetParametersValueImportData> batches =
                collectBatchDatasetParameterValues(instructionsByDataset);

        for (List<Batch<DatasetParametersValueImportData>> batchList : batches.partition(BATCH_30)) {
            List<Long> testCaseIds = batchList.stream().map(Batch::getTargetId).toList();

            // This operation should be in the DatasetModificationService.
            Batches<Dataset> datasetsToCreate =
                    batchDatasetParametersValuesAddition(batchList, testCaseIds);

            for (List<Batch<Dataset>> datasetBatchList : datasetsToCreate.partition(BATCH_30)) {
                List<Long> testCaseDatasetIds = batchList.stream().map(Batch::getTargetId).toList();
                datasetService.persistBatch(testCaseDatasetIds, datasetBatchList);
            }

            entityManager.flush();
            entityManager.clear();
        }
    }

    private Batches<DatasetParametersValueImportData> collectBatchDatasetParameterValues(
            Map<DatasetTarget, List<DatasetParamValueInstruction>> instructionsByDataset) {
        Batches<DatasetParametersValueImportData> batches = new Batches<>();

        for (var entry : instructionsByDataset.entrySet()) {
            DatasetTarget target = entry.getKey();
            Long testCaseId = validator.getModel().getId(target.getTestCase());
            DatasetParametersValueImportData data =
                    new DatasetParametersValueImportData(target.getName());
            for (var instruction : entry.getValue()) {
                data.addParameterTargetValue(
                        instruction.getParameterTarget().getName(),
                        helper.truncate(instruction.getDatasetValue().getValue()));
            }
            batches.addBatch(testCaseId, data);
        }

        return batches;
    }

    private Batches<Dataset> batchDatasetParametersValuesAddition(
            List<Batch<DatasetParametersValueImportData>> batchList, List<Long> testCaseIds) {
        Map<Long, List<Parameter>> parametersByTestCaseId =
                paramDao.findParametersByTestCaseIds(testCaseIds);
        Map<Long, List<Dataset>> datasetsByTestCaseId =
                datasetDao.findOwnDatasetsByTestCaseIds(testCaseIds);

        Batches<Dataset> datasetsToCreate = new Batches<>();

        for (Batch<DatasetParametersValueImportData> batch : batchList) {
            Map<String, Dataset> datasetNames =
                    datasetsByTestCaseId.getOrDefault(batch.getTargetId(), Collections.emptyList()).stream()
                            .collect(Collectors.toMap(Dataset::getName, Function.identity()));

            Map<String, Parameter> parameterByName =
                    parametersByTestCaseId.getOrDefault(batch.getTargetId(), Collections.emptyList()).stream()
                            .collect(Collectors.toMap(Parameter::getName, Function.identity()));

            for (DatasetParametersValueImportData parameterData : batch.getEntities()) {
                String datasetName = parameterData.getDataset();
                Dataset dataset = datasetNames.get(parameterData.getDataset());

                if (dataset == null) {
                    dataset = createNewDataset(datasetName, parameterData, parameterByName);
                    datasetsToCreate.addBatch(batch.getTargetId(), dataset);
                } else {
                    updateExistingDataset(dataset, parameterData, parameterByName);
                }
            }
        }

        return datasetsToCreate;
    }

    private void updateExistingDataset(
            Dataset dataset,
            DatasetParametersValueImportData parameterData,
            Map<String, Parameter> parameterByName) {
        parameterData
                .getValuesParameters()
                .forEach(
                        (parameterName, value) -> {
                            Parameter parameter = parameterByName.get(parameterName);
                            checkNotNullParameter(parameterName, parameter);
                            DatasetParamValue dpv = retrieveParameter(dataset, parameter);
                            if (dpv == null) {
                                dpv = new DatasetParamValue(parameter, dataset);
                                paramvalueDao.save(dpv);
                                dataset.addParameterValue(dpv);
                            }
                            dpv.setParamValue(value);
                        });
    }

    private static void checkNotNullParameter(String parameterName, Parameter parameter) {
        if (parameter == null) {
            throw new NoSuchElementException("Parameter " + parameterName + " could not be found");
        }
    }

    private Dataset createNewDataset(
            String datasetName,
            DatasetParametersValueImportData parameterData,
            Map<String, Parameter> parameterByName) {
        Dataset dataset = new Dataset();
        dataset.setName(datasetName);
        helper.fillNullWithDefaults(dataset);
        helper.truncate(dataset);

        parameterData
                .getValuesParameters()
                .forEach(
                        (parameterName, value) -> {
                            Parameter parameter = parameterByName.get(parameterName);
                            checkNotNullParameter(parameterName, parameter);
                            DatasetParamValue dpv = new DatasetParamValue(parameter, dataset);
                            dataset.addParameterValue(dpv);
                            dpv.setParamValue(value);
                        });

        return dataset;
    }

    private DatasetParamValue retrieveParameter(Dataset dataset, Parameter parameter) {
        for (DatasetParamValue dpv : dataset.getParameterValues()) {
            if (dpv.getParameter().equals(parameter)) {
                return dpv;
            }
        }

        return null;
    }
}
