/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.val;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.campaign.Campaign.FULL_NAME_SEPARATOR;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DISABLED_EXECUTION_STATUS;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.jooq.tools.StringUtils;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignFolderDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignLibraryDto;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationPlanningDto;
import org.squashtest.tm.service.internal.repository.display.CampaignDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class CampaignDisplayDaoImpl implements CampaignDisplayDao {
    private DSLContext dsl;

    public CampaignDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public CampaignLibraryDto getCampaignLibraryDtoById(Long id) {
        return dsl.select(
                        CAMPAIGN_LIBRARY.CL_ID.as(RequestAliasesConstants.ID),
                        CAMPAIGN_LIBRARY.ATTACHMENT_LIST_ID,
                        PROJECT.PROJECT_ID,
                        PROJECT.DESCRIPTION,
                        PROJECT.NAME)
                .from(CAMPAIGN_LIBRARY)
                .innerJoin(PROJECT)
                .on(PROJECT.TCL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                .where(CAMPAIGN_LIBRARY.CL_ID.eq(id))
                .fetchOne()
                .into(CampaignLibraryDto.class);
    }

    @Override
    public CampaignFolderDto getCampaignFolderDtoById(long campaignFolderId) {
        return dsl.select(
                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID),
                        CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID,
                        CAMPAIGN_LIBRARY_NODE.DESCRIPTION,
                        CAMPAIGN_LIBRARY_NODE.NAME,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID)
                .from(CAMPAIGN_LIBRARY_NODE)
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(campaignFolderId))
                .fetchOne()
                .into(CampaignFolderDto.class);
    }

    @Override
    public CampaignDto getCampaignDtoById(long campaignId) {
        return dsl.select(
                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID),
                        CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID,
                        CAMPAIGN_LIBRARY_NODE.DESCRIPTION,
                        CAMPAIGN_LIBRARY_NODE.NAME,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
                        CAMPAIGN_LIBRARY_NODE.CREATED_BY,
                        CAMPAIGN_LIBRARY_NODE.CREATED_ON,
                        CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_BY,
                        CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_ON,
                        CAMPAIGN.REFERENCE,
                        CAMPAIGN.CAMPAIGN_STATUS,
                        CAMPAIGN.ACTUAL_END_AUTO,
                        CAMPAIGN.ACTUAL_END_DATE,
                        CAMPAIGN.ACTUAL_START_AUTO,
                        CAMPAIGN.ACTUAL_START_DATE,
                        CAMPAIGN.SCHEDULED_START_DATE,
                        CAMPAIGN.SCHEDULED_END_DATE,
                        DSL.field(DSL.count(CAMPAIGN_TEST_PLAN_ITEM.DATASET_ID).gt(0)).as("HAS_DATASETS"))
                .from(CAMPAIGN_LIBRARY_NODE)
                .innerJoin(CAMPAIGN)
                .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .leftJoin(CAMPAIGN_TEST_PLAN_ITEM)
                .on(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(campaignId))
                .groupBy(CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN.CLN_ID)
                .fetchOne()
                .into(CampaignDto.class);
    }

    @Override
    public List<IterationPlanningDto> findIterationPlanningByCampaign(Long campaignId) {
        return dsl.select(
                        ITERATION.ITERATION_ID.as(RequestAliasesConstants.ID),
                        ITERATION.SCHEDULED_START_DATE,
                        ITERATION.SCHEDULED_END_DATE,
                        ITERATION.NAME)
                .from(ITERATION)
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .where(CAMPAIGN.CLN_ID.eq(campaignId))
                .fetchInto(IterationPlanningDto.class);
    }

    @Override
    public int getNbTestPlanItem(Long campaignId, String login) {
        SelectConditionStep<Record1<Integer>> allTestPlanItemsFromCampaign =
                dsl.selectCount()
                        .from(CAMPAIGN)
                        .innerJoin(CAMPAIGN_TEST_PLAN_ITEM)
                        .on(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                        .leftJoin(CORE_USER)
                        .on(CORE_USER.PARTY_ID.eq(CAMPAIGN_TEST_PLAN_ITEM.USER_ID))
                        .where(CAMPAIGN.CLN_ID.eq(campaignId));
        if (login != null) {
            allTestPlanItemsFromCampaign = allTestPlanItemsFromCampaign.and(CORE_USER.LOGIN.eq(login));
        }

        Integer count = allTestPlanItemsFromCampaign.groupBy(CAMPAIGN.CLN_ID).fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public List<String> retrieveFullNameByCampaignLibraryNodeIds(
            List<Long> campaignLibraryNodeIds, List<Long> projectIds) {
        Field<String> fullName =
                when(
                                CAMPAIGN.REFERENCE.isNull().or(CAMPAIGN.REFERENCE.eq(StringUtils.EMPTY)),
                                CAMPAIGN_LIBRARY_NODE.NAME)
                        .otherwise(
                                concat(CAMPAIGN.REFERENCE, val(FULL_NAME_SEPARATOR), CAMPAIGN_LIBRARY_NODE.NAME));

        return dsl.select(fullName)
                .from(CAMPAIGN_LIBRARY_NODE)
                .leftJoin(CAMPAIGN)
                .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.in(campaignLibraryNodeIds))
                .and(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(fullName)
                .fetch(fullName);
    }

    @Override
    public List<Long> findDistinctProjectIdsByCampaignLibraryIds(Set<Long> campaignLibraryIds) {
        return dsl.selectDistinct(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .where(PROJECT.CL_ID.in(campaignLibraryIds))
                .fetchInto(Long.class);
    }

    @Override
    public List<Long> findDistinctProjectIdsByCampaignLibraryNodeIds(Set<Long> campaignNodeIds) {
        return dsl.selectDistinct(CAMPAIGN_LIBRARY_NODE.PROJECT_ID)
                .from(CAMPAIGN_LIBRARY_NODE)
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.in(campaignNodeIds))
                .fetchInto(Long.class);
    }

    @Override
    public List<String> findAllDisabledExecutionStatusByProjectIds(Set<Long> projectIds) {

        return dsl.select(DISABLED_EXECUTION_STATUS.EXECUTION_STATUS)
                .from(PROJECT)
                .leftJoin(CAMPAIGN_LIBRARY)
                .on(PROJECT.CL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                .leftJoin(DISABLED_EXECUTION_STATUS)
                .on(CAMPAIGN_LIBRARY.CL_ID.eq(DISABLED_EXECUTION_STATUS.CL_ID))
                .where(PROJECT.PROJECT_ID.in(projectIds))
                .and(DISABLED_EXECUTION_STATUS.EXECUTION_STATUS.isNotNull())
                .fetchInto(String.class);
    }

    @Override
    public List<NamedReference> findNamedReferences(List<Long> ids) {
        return dsl.select(
                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID), CAMPAIGN_LIBRARY_NODE.NAME)
                .from(CAMPAIGN_LIBRARY_NODE)
                .innerJoin(CAMPAIGN)
                .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.in(ids))
                .fetchInto(NamedReference.class);
    }

    @Override
    public Map<Long, List<Long>> findExecutionIdsByCampaignIds(List<Long> campaignIds) {
        return dsl.select(CAMPAIGN_ITERATION.CAMPAIGN_ID, ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID)
                .from(CAMPAIGN_ITERATION)
                .leftJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .where(CAMPAIGN_ITERATION.CAMPAIGN_ID.in(campaignIds))
                .and(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.isNotNull())
                .fetchGroups(CAMPAIGN_ITERATION.CAMPAIGN_ID, ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID);
    }

    @Override
    public List<Long> findContainedCampaignIds(List<Long> campaignLibraryNodeIds) {
        return dsl.select(CAMPAIGN.CLN_ID)
                .from(CAMPAIGN)
                .leftJoin(CLN_RELATIONSHIP_CLOSURE)
                .on(CAMPAIGN.CLN_ID.eq(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .where(
                        CLN_RELATIONSHIP_CLOSURE
                                .ANCESTOR_ID
                                .in(campaignLibraryNodeIds)
                                .or(CAMPAIGN.CLN_ID.in(campaignLibraryNodeIds)))
                .fetchInto(Long.class);
    }
}
