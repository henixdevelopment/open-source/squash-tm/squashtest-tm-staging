/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.user;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;

/**
 * Will handle CRUD on Squash user accounts, groups, permissions and the like. For all operations
 * about user authentication, the said operation will be delegated to the
 * UserAuthenticationManagerService.
 *
 * <p>Security should ensure that :
 *
 * <p>- access to user informations (both reading and writing) are opened to the said user
 * ROLE_ADMIN authority only, - the rest requires ROLE_ADMIN authority only.
 *
 * @author bsiri
 */
public interface UserAdministrationService extends UserManagerService {

    // TODO use a project finder where this method is used
    List<Project> findAllProjects();

    Map<String, String> findPostLoginInformation();

    /**
     * Will add user to teams members lists.<br>
     * access restricted to admins
     *
     * @param userId : the id of the concerned {@link User}
     * @param teamIds : ids of the {@link Team}s to add user to.
     * @return the set of all non associated {@link Team}s with custom profile and without ultimate
     *     license
     */
    Set<String> associateToTeams(long userId, List<Long> teamIds);

    String getDefaultAuthenticatedRedirectUrlForUserAuthority();
}
