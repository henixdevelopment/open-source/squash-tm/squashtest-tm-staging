/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import static org.squashtest.tm.service.security.Authorizations.WRITE_TC_OR_ROLE_ADMIN;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.IsScriptedTestCaseVisitor;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.DatasetParamValueDao;
import org.squashtest.tm.service.internal.repository.ParameterDao;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.SecurityCheckableObject;
import org.squashtest.tm.service.testcase.DatasetModificationService;

@Transactional
@Service("squashtest.tm.service.DatasetModificationService")
public class DatasetModificationServiceImpl implements DatasetModificationService {

    private static final String WRITE = "WRITE";

    @Inject private DatasetDao datasetDao;

    @Inject private ParameterDao parameterDao;

    @Inject private DatasetParamValueDao datasetParamValueDao;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject private TestCaseLoader testCaseLoader;

    @PersistenceContext private EntityManager entityManager;

    @Override
    public Dataset findById(long datasetId) {
        return datasetDao.getReferenceById(datasetId);
    }

    @PreAuthorize(WRITE_TC_OR_ROLE_ADMIN)
    @Override
    @CheckBlockingMilestone(entityType = TestCase.class)
    public Dataset persist(Dataset dataset, @Id long testCaseId) {
        Dataset sameName = datasetDao.findByTestCaseIdAndName(testCaseId, dataset.getName());

        if (sameName != null) {
            throw new DuplicateNameException(dataset.getName(), dataset.getName());
        } else {
            TestCase testCase =
                    testCaseLoader.load(testCaseId, EnumSet.of(TestCaseLoader.Options.FETCH_DATASETS));

            doPersistDataset(testCase, dataset);
        }

        return dataset;
    }

    @Override
    public void persistBatch(List<Long> testCaseIds, List<Batch<Dataset>> batchList) {
        Map<Long, TestCase> testCases =
                testCaseLoader.load(testCaseIds, EnumSet.of(TestCaseLoader.Options.FETCH_DATASETS)).stream()
                        .collect(Collectors.toMap(TestCaseLibraryNode::getId, Function.identity()));

        Map<Long, List<Parameter>> parametersByTestCaseId =
                parameterDao.findAllParametersByTestCaseIds(testCaseIds);

        for (Batch<Dataset> batch : batchList) {
            TestCase testCase = testCases.get(batch.getTargetId());
            List<Dataset> datasets = batch.getEntities();
            List<Parameter> parameters = parametersByTestCaseId.get(batch.getTargetId());

            for (Dataset dataset : datasets) {
                IsScriptedTestCaseVisitor testCaseVisitor = new IsScriptedTestCaseVisitor();
                if (testCaseVisitor.isScripted()) {
                    throw new IllegalArgumentException("Cannot add dataset in a scripted test case.");
                }
                testCase.accept(testCaseVisitor);

                dataset.setTestCase(testCase);
                testCase.addDataset(dataset);

                if (!parameters.isEmpty()) {
                    updateDatasetParameters(dataset, parameters);
                }
            }
        }

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    public Dataset persistUnsecured(Dataset dataset, TestCase testCase) {
        Dataset sameName = datasetDao.findByTestCaseIdAndName(testCase.getId(), dataset.getName());

        if (sameName != null) {
            throw new DuplicateNameException(dataset.getName(), dataset.getName());
        } else {
            doPersistDataset(testCase, dataset);
        }
        return dataset;
    }

    private void doPersistDataset(TestCase testCase, Dataset dataset) {
        IsScriptedTestCaseVisitor testCaseVisitor = new IsScriptedTestCaseVisitor();
        if (testCaseVisitor.isScripted()) {
            throw new IllegalArgumentException("Cannot add dataset in a scripted test case.");
        }
        testCase.accept(testCaseVisitor);

        dataset.setTestCase(testCase);
        testCase.addDataset(dataset);

        Collection<Parameter> parameters = parameterDao.findAllParametersByTestCase(testCase.getId());
        updateDatasetParameters(dataset, parameters);
    }

    @Override
    public Collection<Dataset> findAllForTestCase(long testCaseId) {
        return datasetDao.findAllByTestCaseId(testCaseId);
    }

    @Override
    public void remove(Dataset dataset) {
        TestCase testCase = dataset.getTestCase();
        PermissionsUtils.checkPermission(
                permissionEvaluationService, new SecurityCheckableObject(testCase, WRITE));
        datasetDao.removeDatasetFromTestPlanItems(dataset.getId());
        datasetDao.delete(dataset);
    }

    @Override
    @CheckBlockingMilestone(entityType = Dataset.class)
    public void removeById(@Id long datasetId) {
        Optional<Dataset> optDataset = this.datasetDao.findById(datasetId);
        if (optDataset.isPresent()) {
            Dataset dataset = optDataset.get();
            TestCase testCase = dataset.getTestCase();
            PermissionsUtils.checkPermission(
                    permissionEvaluationService, new SecurityCheckableObject(testCase, WRITE));
            remove(dataset);
        }
    }

    @Override
    public void removeAllByTestCaseIds(List<Long> testCaseIds) {
        datasetDao.removeDatasetsFromTestPlanItems(testCaseIds);
        datasetDao.removeDatasetInBatchByTestCaseIds(testCaseIds);
    }

    @Override
    @CheckBlockingMilestone(entityType = Dataset.class)
    public void changeName(@Id long datasetId, String newName) {

        Dataset dataset = datasetDao.getReferenceById(datasetId);
        TestCase testCase = dataset.getTestCase();
        PermissionsUtils.checkPermission(
                permissionEvaluationService, new SecurityCheckableObject(testCase, WRITE));

        Dataset sameName = datasetDao.findByTestCaseIdAndName(dataset.getTestCase().getId(), newName);
        if (sameName != null && !sameName.getId().equals(dataset.getId())) {
            throw new DuplicateNameException(dataset.getName(), newName);
        } else {
            dataset.setName(newName);
        }
    }

    @Override
    @CheckBlockingMilestone(entityType = DatasetParamValue.class)
    public void changeParamValue(@Id long datasetParamValueId, String value) {
        DatasetParamValue paramValue = datasetParamValueDao.getReferenceById(datasetParamValueId);
        TestCase testCase = paramValue.getDataset().getTestCase();
        PermissionsUtils.checkPermission(
                permissionEvaluationService, new SecurityCheckableObject(testCase, WRITE));
        paramValue.setParamValue(value);
    }

    public List<Dataset> getAllDatasetByTestCase(long testCaseId) {
        return this.datasetDao.findOwnDatasetsByTestCase(testCaseId);
    }

    @Override
    public DatasetParamValue findDatasetParamValue(Dataset dataset, Parameter parameter) {
        DatasetParamValue result = null;
        Set<DatasetParamValue> datasetParamValues = dataset.getParameterValues();
        for (DatasetParamValue datasetParamValue : datasetParamValues) {
            if (datasetParamValue.getParameter().equals(parameter)) {
                result = datasetParamValue;
            }
        }
        return result;
    }

    private DatasetParamValue findOrAddParameter(Dataset dataset, Parameter parameter) {
        DatasetParamValue datasetParamValue = findDatasetParamValue(dataset, parameter);
        if (datasetParamValue == null) {
            datasetParamValue = new DatasetParamValue(parameter, dataset);
        }
        return datasetParamValue;
    }

    private void updateDatasetParameters(Dataset dataset, Collection<Parameter> parameters) {

        for (Parameter parameter : parameters) {
            DatasetParamValue datasetParamValue = findOrAddParameter(dataset, parameter);
            dataset.addParameterValue(datasetParamValue);
        }
    }

    @Override
    public void cascadeDatasetsUpdate(long testCaseId) {

        Collection<Dataset> allDataset = datasetDao.findOwnDatasetsByTestCase(testCaseId);
        allDataset.addAll(datasetDao.findAllDelegateDatasets(testCaseId));

        Collection<Parameter> params = parameterDao.findAllParametersByTestCase(testCaseId);

        for (Dataset dataset : allDataset) {
            this.updateDatasetParameters(dataset, params);
        }
    }

    @Override
    public void cascadeDatasetsUpdate(Collection<Long> testCaseIds) {
        if (testCaseIds.isEmpty()) {
            return;
        }

        Map<Long, List<Dataset>> allDatasetsByTestCaseId =
                datasetDao.findAllDatasetsByTestCaseIds(testCaseIds);

        Map<Long, List<Parameter>> allParametersByTestCaseId =
                parameterDao.findAllParametersByTestCaseIds(testCaseIds);

        for (var entry : allDatasetsByTestCaseId.entrySet()) {
            Optional.ofNullable(allParametersByTestCaseId.get(entry.getKey()))
                    .ifPresent(
                            paramList ->
                                    entry.getValue().forEach(dataset -> updateDatasetParameters(dataset, paramList)));
        }
    }
}
