/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.orchestrator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrchestratorConfVersions {

    private String name;
    private List<NameAndVersion> internalComponents;
    private List<NameAndVersion> baseImages;

    public OrchestratorConfVersions() {}

    public OrchestratorConfVersions(
            String name, List<NameAndVersion> internalComponents, List<NameAndVersion> baseImages) {
        this.name = name;
        this.internalComponents = internalComponents;
        this.baseImages = baseImages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("internalComponents")
    public List<NameAndVersion> getInternalComponents() {
        return internalComponents;
    }

    @JsonProperty("internal-components")
    public void setInternalComponents(List<NameAndVersion> internalComponents) {
        this.internalComponents = internalComponents;
    }

    @JsonProperty("baseImages")
    public List<NameAndVersion> getBaseImages() {
        return baseImages;
    }

    @JsonProperty("base-images")
    public void setBaseImages(List<NameAndVersion> baseImages) {
        this.baseImages = baseImages;
    }
}
