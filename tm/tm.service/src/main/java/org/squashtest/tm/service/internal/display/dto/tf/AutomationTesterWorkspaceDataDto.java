/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.tf;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.display.dto.UserView;

public class AutomationTesterWorkspaceDataDto {

    private int nbGlobalAutomReq;
    private int nbReadyForTransmissionAutomReq;
    private int nbToBeValidatedAutomReq;
    private List<UserView> usersWhoModifiedTestCasesGlobalView = new ArrayList<>();
    private List<UserView> usersWhoModifiedTestCasesReadyView = new ArrayList<>();
    private List<UserView> usersWhoModifiedTestCasesValidateView = new ArrayList<>();

    public int getNbGlobalAutomReq() {
        return nbGlobalAutomReq;
    }

    public void setNbGlobalAutomReq(int nbGlobalAutomReq) {
        this.nbGlobalAutomReq = nbGlobalAutomReq;
    }

    public int getNbReadyForTransmissionAutomReq() {
        return nbReadyForTransmissionAutomReq;
    }

    public void setNbReadyForTransmissionAutomReq(int nbReadyForTransmissionAutomReq) {
        this.nbReadyForTransmissionAutomReq = nbReadyForTransmissionAutomReq;
    }

    public int getNbToBeValidatedAutomReq() {
        return nbToBeValidatedAutomReq;
    }

    public void setNbToBeValidatedAutomReq(int nbToBeValidatedAutomReq) {
        this.nbToBeValidatedAutomReq = nbToBeValidatedAutomReq;
    }

    public List<UserView> getUsersWhoModifiedTestCasesGlobalView() {
        return usersWhoModifiedTestCasesGlobalView;
    }

    public void setUsersWhoModifiedTestCasesGlobalView(
            List<UserView> usersWhoModifiedTestCasesGlobalView) {
        this.usersWhoModifiedTestCasesGlobalView = usersWhoModifiedTestCasesGlobalView;
    }

    public List<UserView> getUsersWhoModifiedTestCasesReadyView() {
        return usersWhoModifiedTestCasesReadyView;
    }

    public void setUsersWhoModifiedTestCasesReadyView(
            List<UserView> usersWhoModifiedTestCasesReadyView) {
        this.usersWhoModifiedTestCasesReadyView = usersWhoModifiedTestCasesReadyView;
    }

    public List<UserView> getUsersWhoModifiedTestCasesValidateView() {
        return usersWhoModifiedTestCasesValidateView;
    }

    public void setUsersWhoModifiedTestCasesValidateView(
            List<UserView> usersWhoModifiedTestCasesValidateView) {
        this.usersWhoModifiedTestCasesValidateView = usersWhoModifiedTestCasesValidateView;
    }
}
