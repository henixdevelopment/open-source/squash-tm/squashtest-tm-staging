/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.excel;

import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.service.annotation.CacheScope;
import org.squashtest.tm.service.importer.EntityType;
import org.squashtest.tm.service.importer.ImportLog;
import org.squashtest.tm.service.internal.batchimport.Facility;
import org.squashtest.tm.service.internal.batchimport.FacilityImpl;
import org.squashtest.tm.service.internal.batchimport.instruction.Instruction;
import org.squashtest.tm.service.internal.batchimport.LogTrain;
import org.squashtest.tm.service.internal.batchimport.SimulationFacility;
import org.squashtest.tm.service.internal.batchimport.instruction.container.InstructionContainer;
import org.squashtest.tm.service.internal.batchimport.column.ExcelWorkbookParser;
import org.squashtest.tm.service.internal.batchimport.instruction.container.Importer;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public abstract class ExcelBatchImporter {

    @Inject
    private Provider<SimulationFacility> simulatorProvider;

    @Inject
    private Provider<FacilityImpl> facilityImplProvider;

    @Inject
    private EntityManager entityManager;

    @Inject
    private ProjectDao projectDao;

	/*
		1/ The logger is set by constructor, check the subclasses.

		2/ It has protected scope so that subclasses can access it.

		3/ I need this logger to be accessible from the subclasses and I
		wasn't sure about having one single static property for receiving
		two loggers instances (one for the TestCase importer and the other
		one for Requirement importer). So I made the loggers non-static.
		I still use the static naming convention though.

	*/
	protected final Logger LOGGER; // NOSONAR see comment above


	protected ExcelBatchImporter(Logger logger) {
		this.LOGGER = logger;
	}


	public ImportLog simulateImport(File excelFile) {
		LOGGER.debug("beginning import simulation");

		ExcelWorkbookParser parser = getExcelWorkbookParser(excelFile);

		LogTrain unknownHeaders = parser.logUnknownHeaders();

        Importer containers = buildOrderedInstructionContainers(parser);

        LOGGER.trace("running import simulation");
        ImportLog importLog = run(containers, simulatorProvider);

        importLog.appendLogTrain(unknownHeaders);

        LOGGER.trace("done");
        return importLog;

    }

    private ExcelWorkbookParser getExcelWorkbookParser(File excelFile) {
        LOGGER.trace("parsing excel file");
        ExcelWorkbookParser parser = ExcelWorkbookParser.createParser(excelFile);
        parser.parse().releaseResources();
        LOGGER.trace("parsing done");
        return parser;
    }

    @CacheScope
    public ImportLog performImport(File excelFile) {
        LOGGER.debug("beginning import");

        ExcelWorkbookParser parser = getExcelWorkbookParser(excelFile);

        LogTrain unknownHeaders = parser.logUnknownHeaders();

        Importer containers = buildOrderedInstructionContainers(parser);

        LOGGER.trace("running import");
        ImportLog importLog = run(containers, facilityImplProvider);

        importLog.appendLogTrain(unknownHeaders);

        LOGGER.trace("done");
        return importLog;

    }

	/*
	 *
	 * Feat 3695 :
	 *
	 * an additional step is required now that DATASET and PArameter values are processed separately : we still need to
	 * merge their logs.
	 *
	 */
    private <T extends Facility> ImportLog run(Importer importer, Provider<T> facility) {
        HibernateConfig.enableBatch(entityManager, HibernateConfig.BATCH_30);

        return importer.performImport(facility, projectDao);
    }

    public Importer buildOrderedInstructionContainers(ExcelWorkbookParser parser) {

        return buildImporter(parser);
    }

    private Importer buildImporter(ExcelWorkbookParser parser) {
        Importer importer = new Importer();

        for (EntityType entity : getEntityType()) {
            addInstructionsByEntity(parser, entity, importer);
        }

        return importer;
    }

    protected <T extends Instruction<?>> void addInstructionsToImporter(
        List<T> instructions,
        Importer importer,
        Function<List<T>, InstructionContainer<T>> containerCreator) {

        Map<String, List<T>> instructionsMap = new HashMap<>();

        for (T instruction : instructions) {

            if (!instruction.canImport()) {
                importer.addErrorInstruction(instruction);
                continue;
            }

            instructionsMap.computeIfAbsent(PathUtils.extractUnescapedProjectName(instruction.getSourcePath()), k -> new ArrayList<>()).add(instruction);
        }

        instructionsMap.forEach((project, container) -> importer.addInstructionContainer(project, containerCreator.apply(container)));
    }

	public abstract List<EntityType> getEntityType();

	public abstract void addInstructionsByEntity(ExcelWorkbookParser parser, EntityType entityType, Importer importer);

}
