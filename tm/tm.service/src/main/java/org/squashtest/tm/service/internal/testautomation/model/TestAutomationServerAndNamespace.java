/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model;

import static java.util.Objects.requireNonNull;

import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;

/**
 * Class which holds a {@link TestAutomationServer} and a namespace. It is used to group tests which
 * will be executed against the same server and namespace. Every test grouped by an occurrence of
 * this class will be launched with the same workflow in the orchestrator.
 */
public class TestAutomationServerAndNamespace {

    private final TestAutomationServer testAutomationServer;
    private final String namespace;

    public TestAutomationServerAndNamespace(
            TestAutomationServer testAutomationServer, String namespace) {
        this.testAutomationServer = requireNonNull(testAutomationServer);
        if (StringUtils.isBlank(namespace)) {
            this.namespace = "default";
        } else {
            this.namespace = namespace;
        }
    }

    public TestAutomationServer getTestAutomationServer() {
        return testAutomationServer;
    }

    public String getNamespace() {
        return namespace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestAutomationServerAndNamespace that = (TestAutomationServerAndNamespace) o;
        return testAutomationServer.equals(that.testAutomationServer)
                && namespace.equals(that.namespace);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testAutomationServer, namespace);
    }
}
