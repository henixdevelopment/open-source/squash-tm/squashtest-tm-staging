/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_CAMPAIGN_LIBRARY_OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_CAMPAIGN_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_CAMPFOLDER_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.CampaignFolderKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.CampaignKnownIssueFinder;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignFolderDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignLibraryDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignMultiSelectionDto;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationPlanningDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.CampaignTestPlanGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.TestPlanGridHelpers;
import org.squashtest.tm.service.internal.library.EntityPathHeaderService;
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CampaignDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateCampaignDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;
import org.squashtest.tm.service.user.PartyPreferenceService;

@Service
@Transactional(readOnly = true)
public class CampaignDisplayServiceImpl implements CampaignDisplayService {

    public static final String DEACTIVATED_USER_I18N_KEY =
            "administration-workspace.users.manage.deactivated";

    private static final String CAN_READ_CAMPAIGN =
            "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'READ')"
                    + OR_HAS_ROLE_ADMIN;

    private final AttachmentDisplayDao attachmentDisplayDao;
    private final AvailableDatasetAppender availableDatasetAppender;
    private final CampaignDisplayDao campaignDisplayDao;
    private final CampaignFolderKnownIssueFinder campaignFolderKnownIssueFinder;
    private final CampaignKnownIssueFinder campaignKnownIssueFinder;
    private final CampaignTestPlanManagerService campaignTestPlanManagerService;
    private final CustomFieldValueDisplayDao customFieldValueDisplayDao;
    private final CustomReportDashboardService customReportDashboardService;
    private final DSLContext dslContext;
    private final EntityPathHeaderService entityPathHeaderService;
    private final HibernateCampaignDao hibernateCampaignDao;
    private final IterationDisplayDao iterationDisplayDao;
    private final MilestoneDisplayDao milestoneDisplayDao;
    private final PartyPreferenceService partyPreferenceService;
    private final PermissionEvaluationService permissionEvaluationService;
    private final EntityPathHeaderDao entityPathHeaderDao;
    private final ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper;
    private final UserContextService userContextService;
    private final EntityManager entityManager;
    private final GridConfigurationService gridConfigurationService;
    private final MessageSource messageSource;
    private final CustomItpiLastExecutionFilterDao itpiLastExecutionFilterDao;

    public CampaignDisplayServiceImpl(
            CampaignDisplayDao campaignDisplayDao,
            IterationDisplayDao iterationDisplayDao,
            AttachmentDisplayDao attachmentDisplayDao,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            MilestoneDisplayDao milestoneDisplayDao,
            HibernateCampaignDao hibernateCampaignDao,
            DSLContext dslContext,
            CampaignKnownIssueFinder campaignKnownIssueFinder,
            CampaignTestPlanManagerService campaignTestPlanManagerService,
            CustomReportDashboardService customReportDashboardService,
            PartyPreferenceService partyPreferenceService,
            ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper,
            AvailableDatasetAppender availableDatasetAppender,
            UserContextService userContextService,
            PermissionEvaluationService permissionEvaluationService,
            EntityPathHeaderDao entityPathHeaderDao,
            CampaignFolderKnownIssueFinder campaignFolderKnownIssueFinder,
            EntityPathHeaderService entityPathHeaderService,
            GridConfigurationService gridConfigurationService,
            EntityManager entityManager,
            MessageSource messageSource,
            CustomItpiLastExecutionFilterDao itpiLastExecutionFilterDao) {

        this.attachmentDisplayDao = attachmentDisplayDao;
        this.availableDatasetAppender = availableDatasetAppender;
        this.campaignDisplayDao = campaignDisplayDao;
        this.campaignFolderKnownIssueFinder = campaignFolderKnownIssueFinder;
        this.campaignKnownIssueFinder = campaignKnownIssueFinder;
        this.campaignTestPlanManagerService = campaignTestPlanManagerService;
        this.customFieldValueDisplayDao = customFieldValueDisplayDao;
        this.customReportDashboardService = customReportDashboardService;
        this.dslContext = dslContext;
        this.entityPathHeaderService = entityPathHeaderService;
        this.hibernateCampaignDao = hibernateCampaignDao;
        this.iterationDisplayDao = iterationDisplayDao;
        this.milestoneDisplayDao = milestoneDisplayDao;
        this.partyPreferenceService = partyPreferenceService;
        this.permissionEvaluationService = permissionEvaluationService;
        this.entityPathHeaderDao = entityPathHeaderDao;
        this.readUnassignedTestPlanHelper = readUnassignedTestPlanHelper;
        this.userContextService = userContextService;
        this.gridConfigurationService = gridConfigurationService;
        this.entityManager = entityManager;
        this.messageSource = messageSource;
        this.itpiLastExecutionFilterDao = itpiLastExecutionFilterDao;
    }

    @Override
    @PreAuthorize(READ_CAMPAIGN_LIBRARY_OR_HAS_ROLE_ADMIN)
    public CampaignLibraryDto getCampaignLibraryView(long libraryId) {
        CampaignLibraryDto libraryDto = this.campaignDisplayDao.getCampaignLibraryDtoById(libraryId);
        libraryDto.setAttachmentList(
                attachmentDisplayDao.findAttachmentListById(libraryDto.getAttachmentListId()));

        PartyPreference preference =
                partyPreferenceService.findPreferenceForCurrentUser(
                        CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
        if (preference != null) {
            Long dashboardId = Long.valueOf(preference.getPreferenceValue());
            libraryDto.setFavoriteDashboardId(dashboardId);
        }
        libraryDto.setNbIssues(campaignFolderKnownIssueFinder.countKnownIssues(libraryId));
        libraryDto.setCanShowFavoriteDashboard(
                customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN));
        libraryDto.setShouldShowFavoriteDashboard(
                customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN));

        return libraryDto;
    }

    @Override
    @PreAuthorize(READ_CAMPFOLDER_OR_ROLE_ADMIN)
    public CampaignFolderDto getCampaignFolderView(long campFolderId) {
        CampaignFolderDto campaignFolderDto =
                this.campaignDisplayDao.getCampaignFolderDtoById(campFolderId);
        campaignFolderDto.setAttachmentList(
                attachmentDisplayDao.findAttachmentListById(campaignFolderDto.getAttachmentListId()));
        campaignFolderDto.setCustomFieldValues(
                customFieldValueDisplayDao.findCustomFieldValues(
                        BindableEntity.CAMPAIGN_FOLDER, campFolderId));
        campaignFolderDto.setNbIssues(campaignFolderKnownIssueFinder.countKnownIssues(campFolderId));
        campaignFolderDto.setCanShowFavoriteDashboard(
                customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN));
        campaignFolderDto.setShouldShowFavoriteDashboard(
                customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN));
        PartyPreference preference =
                partyPreferenceService.findPreferenceForCurrentUser(
                        CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
        if (preference != null) {
            Long dashboardId = Long.valueOf(preference.getPreferenceValue());
            campaignFolderDto.setFavoriteDashboardId(dashboardId);
        }
        campaignFolderDto.setPath(entityPathHeaderService.buildCLNPathHeader(campFolderId));

        return campaignFolderDto;
    }

    @Override
    @PreAuthorize(READ_CAMPAIGN_OR_ROLE_ADMIN)
    public CampaignDto getCampaignView(long campaignId) {
        CampaignDto campaignDto = this.campaignDisplayDao.getCampaignDtoById(campaignId);
        campaignDto.setMilestones(milestoneDisplayDao.getMilestonesByCampaignId(campaignId));
        campaignDto.setAttachmentList(
                attachmentDisplayDao.findAttachmentListById(campaignDto.getAttachmentListId()));
        List<Long> itpiIdsInTCLastExecScope =
                itpiLastExecutionFilterDao.gatherLatestItpiIdsForTCInScopeForCampaign(
                        Collections.singletonList(campaignId));
        campaignDto.setTestPlanStatistics(
                hibernateCampaignDao.findCampaignStatisticsForTCLastExecutionScope(
                        campaignId, itpiIdsInTCLastExecScope));
        campaignDto.setCustomFieldValues(
                customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.CAMPAIGN, campaignId));
        campaignDto.setNbIssues(campaignKnownIssueFinder.countKnownIssues(campaignId));
        appendUsers(campaignDto);
        campaignDto.setShouldShowFavoriteDashboard(
                customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN));
        campaignDto.setCanShowFavoriteDashboard(
                customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN));
        PartyPreference preference =
                partyPreferenceService.findPreferenceForCurrentUser(
                        CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
        if (preference != null) {
            Long dashboardId = Long.valueOf(preference.getPreferenceValue());
            campaignDto.setFavoriteDashboardId(dashboardId);
        }
        appendNbTestPlanItems(campaignDto);
        campaignDto.setPath(entityPathHeaderService.buildCLNPathHeader(campaignId));

        return campaignDto;
    }

    private void appendUsers(CampaignDto campaignDto) {
        List<User> users =
                this.campaignTestPlanManagerService.findAssignableUserForTestPlan(campaignDto.getId());
        campaignDto.setUsers(UserView.fromEntities(users));
    }

    private void appendNbTestPlanItems(CampaignDto campaignDto) {
        String login = this.userContextService.getUsername();
        int count;

        if (currentUserCanReadUnassigned(campaignDto.getId())) {
            count = this.campaignDisplayDao.getNbTestPlanItem(campaignDto.getId(), null);
        } else {
            count = this.campaignDisplayDao.getNbTestPlanItem(campaignDto.getId(), login);
        }
        campaignDto.setNbTestPlanItems(count);
    }

    private boolean currentUserCanReadUnassigned(Long campaignId) {
        return this.permissionEvaluationService.hasRoleOrPermissionOnObject(
                Roles.ROLE_ADMIN,
                Permissions.READ_UNASSIGNED.name(),
                campaignId,
                Campaign.SIMPLE_CLASS_NAME);
    }

    @Override
    public List<IterationPlanningDto> findIterationsPlanningByCampaign(Long campaignId) {
        return campaignDisplayDao.findIterationPlanningByCampaign(campaignId);
    }

    @Override
    @PreAuthorize(CAN_READ_CAMPAIGN)
    public GridResponse findTestPlan(Long campaignId, GridRequest gridRequest) {
        String userLoginToRestrictTo =
                this.readUnassignedTestPlanHelper.getUserToRestrictTo(
                        campaignId, Campaign.SIMPLE_CLASS_NAME);
        CampaignTestPlanGrid testPlanGrid = new CampaignTestPlanGrid(campaignId, userLoginToRestrictTo);
        Campaign campaign = entityManager.find(Campaign.class, campaignId);
        Long projectId = campaign.getProject().getId();
        GridResponse gridResponse = testPlanGrid.getRows(gridRequest, dslContext);

        appendDataRows(gridResponse);

        availableDatasetAppender.appendAvailableDatasets(gridResponse);
        gridResponse.setActiveColumnIds(
                gridConfigurationService.findActiveColumnIdsForUserWithProjectId(
                        gridRequest.getGridId(), projectId));
        return gridResponse;
    }

    private void appendDataRows(GridResponse gridResponse) {
        Set<Long> testCaseIds =
                gridResponse.getDataRows().stream()
                        .map(row -> (Long) row.getData().get(RequestAliasesConstants.toCamelCase(TEST_CASE_ID)))
                        .collect(Collectors.toSet());

        Map<Long, String> testCasePathById =
                entityPathHeaderDao.buildTestCasePathByIds(testCaseIds, " / ");

        for (DataRow row : gridResponse.getDataRows()) {
            TestPlanGridHelpers.addTestCasePath(row, testCasePathById);
            TestPlanGridHelpers.formatDeactivatedUserNameInRowData(row, messageSource);
        }
    }

    @Override
    public List<String> retrieveFullNameByCampaignLibraryNodeIds(
            List<Long> campaignLibraryNodeIds, List<Long> projectIds) {
        return campaignDisplayDao.retrieveFullNameByCampaignLibraryNodeIds(
                campaignLibraryNodeIds, projectIds);
    }

    @Override
    public CampaignMultiSelectionDto getCampaignMultiView(NodeReferences nodeReferences) {
        CampaignMultiSelectionDto campaignMultiSelectionDto = new CampaignMultiSelectionDto();
        campaignMultiSelectionDto.setCanShowFavoriteDashboard(
                customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN));
        campaignMultiSelectionDto.setShouldShowFavoriteDashboard(
                customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN));

        findAndSetMultiSelectionDisabledExecutionStatus(nodeReferences, campaignMultiSelectionDto);

        PartyPreference partyPreference =
                partyPreferenceService.findPreferenceForCurrentUser(
                        CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
        if (partyPreference != null) {
            Long dashboardId = Long.valueOf(partyPreference.getPreferenceValue());
            campaignMultiSelectionDto.setFavoriteDashboardId(dashboardId);
        }

        return campaignMultiSelectionDto;
    }

    // scope is invalid if selection includes test suites or mixes iterations with any other entity
    // type
    @Override
    public boolean isMultiSelectionScopeValid(NodeReferences nodeReferences) {
        int nbTestSuitesInScope = nodeReferences.extractNonLibrariesByType(NodeType.TEST_SUITE).size();

        if (nbTestSuitesInScope > 0) {
            return false;
        }

        int nbIterationsInScope = nodeReferences.extractNonLibrariesByType(NodeType.ITERATION).size();

        return nbIterationsInScope == 0
                || nbIterationsInScope == nodeReferences.extractNonLibraries().size();
    }

    private void findAndSetMultiSelectionDisabledExecutionStatus(
            NodeReferences nodeReferences, CampaignMultiSelectionDto campaignMultiSelectionDto) {
        List<Long> projectIds = new ArrayList<>();
        List<String> finalDisabledExecutionStatuses = new ArrayList<>();

        Set<Long> nodeIds = nodeReferences.extractNonLibraryIds();
        boolean isIterationScope =
                nodeReferences.extractNonLibrariesByType(NodeType.ITERATION).size() == nodeIds.size();

        if (isIterationScope) {
            projectIds.addAll(iterationDisplayDao.findDistinctProjectIdsByIterationIds(nodeIds));
        } else {
            Set<Long> libraryIds = nodeReferences.extractLibraryIds();
            projectIds.addAll(campaignDisplayDao.findDistinctProjectIdsByCampaignLibraryIds(libraryIds));
            projectIds.addAll(campaignDisplayDao.findDistinctProjectIdsByCampaignLibraryNodeIds(nodeIds));
        }

        int nbOfProjectsInPerimeter = projectIds.size();
        List<String> disabledExecutionStatuses =
                campaignDisplayDao.findAllDisabledExecutionStatusByProjectIds(new HashSet<>(projectIds));

        if (!disabledExecutionStatuses.isEmpty() && nbOfProjectsInPerimeter > 1) {
            filterDisabledStatusesForAllProjects(
                    finalDisabledExecutionStatuses, disabledExecutionStatuses, nbOfProjectsInPerimeter);
        } else if (nbOfProjectsInPerimeter == 1) {
            finalDisabledExecutionStatuses = disabledExecutionStatuses;
        }

        campaignMultiSelectionDto.setDisabledExecutionStatus(finalDisabledExecutionStatuses);
    }

    // Returns only execution statuses that are disabled for all the selected projects.
    // If even one project has activated the status then the display should not be blocked.
    private List<String> filterDisabledStatusesForAllProjects(
            List<String> finalDisabledExecutionStatuses,
            List<String> disabledExecutionStatuses,
            int nbOfProjects) {
        ListIterator<String> iterator =
                disabledExecutionStatuses.stream().distinct().toList().listIterator();

        while (iterator.hasNext()) {
            String executionStatus = iterator.next();
            // count the occurrences of each status in the list
            int nbStatusOccurrences =
                    disabledExecutionStatuses.stream()
                            .filter(disabledStatus -> disabledStatus.equals(executionStatus))
                            .toList()
                            .size();

            // if the number of occurrences matches the number of projects,
            // we can assume that the status has been disabled for all the projects in the selection
            if (nbStatusOccurrences == nbOfProjects) {
                finalDisabledExecutionStatuses.add(executionStatus);
            }
        }

        return finalDisabledExecutionStatuses;
    }
}
