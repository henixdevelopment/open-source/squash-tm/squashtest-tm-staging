/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.requirements;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.requirements.RequirementPathFinderService;
import org.squashtest.tm.service.internal.library.HibernatePathService;

@Service
@Transactional(readOnly = true)
public class RequirementPathFinderServiceImpl implements RequirementPathFinderService {

    @PersistenceContext private EntityManager em;

    @Override
    public String buildRequirementLinkPath(Long requirementId, String requirementProjectName) {

        if (requirementId != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(requirementProjectName);
            String pathFromFolder = getPathFromFolder(requirementId);
            String pathFromParents = getPathFromParentsRequirements(requirementId);
            sb.append(pathFromFolder);
            sb.append(pathFromParents);
            return HibernatePathService.substituteSpecialPathSeparator(sb.toString());
        } else {
            return null;
        }
    }

    private String getPathFromParentsRequirements(Long requirementId) {
        String result =
                requirementVersionQuery("requirement.findReqParentPathForToolTip", requirementId, "");
        if (!result.isEmpty()) {
            return " " + HibernatePathService.PATH_SEPARATOR + " " + result;
        } else {
            return "";
        }
    }

    private String getPathFromFolder(Long requirementId) {
        String result =
                requirementVersionQuery("requirement.findReqFolderPathForToolTip", requirementId, "");
        if (!result.isEmpty()) {
            return " " + HibernatePathService.PATH_SEPARATOR + " " + result;
        } else {
            return "";
        }
    }

    @SuppressWarnings("unchecked")
    private <R> R requirementVersionQuery(String queryName, Long requirementId, R defaultValue) {
        Session session = getStatelessSession();
        Query q = session.getNamedQuery(queryName);
        q.setParameter("requirementId", requirementId);
        R result = (R) q.uniqueResult();
        return result != null ? result : defaultValue;
    }

    private Session getStatelessSession() {
        Session s = em.unwrap(Session.class);
        s.setHibernateFlushMode(FlushMode.MANUAL);
        return s;
    }
}
