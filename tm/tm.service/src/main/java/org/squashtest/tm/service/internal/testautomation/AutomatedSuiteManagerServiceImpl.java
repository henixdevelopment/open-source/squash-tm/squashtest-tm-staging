/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.squashtest.tm.domain.EntityType.ITERATION;
import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanExecution.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.service.security.Authorizations.EXECUTE_ITERATION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.EXECUTE_TS_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.attachment.ExternalContentCoordinates;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolder;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentTagManagerService;
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentVariableManagerService;
import org.squashtest.tm.service.internal.campaign.CampaignNodeDeletionHandler;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.denormalizedfield.PrivateDenormalizedFieldValueService;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.internal.repository.AutomatedTestDao;
import org.squashtest.tm.service.internal.repository.CustomTestAutomationServerDao;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;
import org.squashtest.tm.service.testautomation.AutomationDeletionCount;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs;
import org.squashtest.tm.service.testautomation.model.EnvironmentVariableValue;
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.service.testautomation.supervision.AutomatedExecutionViewUtils;
import org.squashtest.tm.service.testautomation.supervision.model.AutomatedSuiteOverview;

@Transactional
@Service("squashtest.tm.service.AutomatedSuiteManagementService")
public class AutomatedSuiteManagerServiceImpl implements AutomatedSuiteManagerService {

    private static final String EXECUTE = "EXECUTE";

    private static final Logger LOGGER =
            LoggerFactory.getLogger(AutomatedSuiteManagerServiceImpl.class);

    public static final int BIND_VARIABLES_LIMIT = 500;

    @Inject private AutomatedSuiteDao autoSuiteDao;

    @Inject private IterationDao iterationDao;

    @Inject private TestSuiteDao testSuiteDao;

    @Inject private IterationTestPlanDao testPlanDao;

    @Inject private ExecutionDao executionDao;

    @Inject private PrivateDenormalizedFieldValueService denormalizedFieldValueService;

    @Inject private TestAutomationConnectorRegistry connectorRegistry;

    @Inject private PermissionEvaluationService permissionService;

    @Inject private CampaignNodeDeletionHandler deletionHandler;

    @Inject private PrivateCustomFieldValueService customFieldValuesService;

    @Inject private ProjectDao projectDao;

    @Inject private CustomTestAutomationServerDao testAutomationServerDao;

    @Inject private GenericProjectDao genericProjectDao;

    @Inject private AutomatedTestDao autoTestDao;

    @Inject private CredentialsProvider credentialsProvider;

    @Inject private AttachmentManagerService attachmentManagerService;

    @PersistenceContext private EntityManager entityManager;

    @Inject private DSLContext dslContext;

    @Inject private UltimateLicenseAvailabilityService ultimateLicenseService;

    @Inject
    private DenormalizedEnvironmentVariableManagerService
            denormalizedEnvironmentVariableManagerService;

    @Inject private DenormalizedEnvironmentTagManagerService denormalizedEnvironmentTagManagerService;

    @Inject private CustomTestSuiteModificationService customTestSuiteModificationService;

    @Inject private AutomatedSuiteStartService automatedSuiteStartService;

    @Override
    public AutomatedSuite findById(String id) {
        return autoSuiteDao.findById(id);
    }

    @Override
    public boolean stopWorkflows(String suiteId, List<String> workflows) {
        List<Boolean> errors = new ArrayList<>();
        workflows.forEach(
                workflow -> {
                    LOGGER.info("Stopping workflow {} for suite {}", workflow, suiteId);
                    AutomatedSuiteWorkflow automatedSuiteWorkflow =
                            autoSuiteDao.getAutomatedSuiteWorkflowByWorkflowId(workflow);
                    if (automatedSuiteWorkflow == null) {
                        LOGGER.info("No running workflow {} for suite {}", workflow, suiteId);
                        return;
                    }

                    stopWorkflow(suiteId, workflow, automatedSuiteWorkflow, errors);
                });

        return errors.contains(true);
    }

    private void stopWorkflow(
            String suiteId,
            String workflow,
            AutomatedSuiteWorkflow automatedSuiteWorkflow,
            List<Boolean> errors) {
        Long projectId = automatedSuiteWorkflow.getProjectId();
        TestAutomationServer server = genericProjectDao.findTestAutomationServer(projectId);
        if (server == null) {
            LOGGER.error("No test automation server found for project with id: {}", projectId);
            errors.add(true);
            return;
        }
        TestAutomationConnector connector = connectorRegistry.getConnectorForKind(server.getKind());
        TokenAuthCredentials credentials =
                credentialsProvider.getProjectLevelCredentials(server.getId(), projectId).orElse(null);
        try {
            OrchestratorResponse<Void> response =
                    connector.killWorkflow(server, projectId, credentials, workflow);
            if (response.isReachable()) {
                AutomatedSuite suite = entityManager.find(AutomatedSuite.class, suiteId);
                suite.getWorkflows().removeIf(work -> work.getWorkflowId().equals(workflow));
                computeNewTestSuiteStatus(suite);
            } else {
                LOGGER.error(
                        "Orchestrator unreachable. Cannot stop workflow {} for suite {}", workflow, suiteId);
                errors.add(true);
            }
        } catch (TestAutomationException ex) {
            LOGGER.error("An error occurred while stopping workflow {} for suite {}", workflow, suiteId);
            LOGGER.trace("Error stacktrace: ", ex);
            errors.add(true);
        }
    }

    private void computeNewTestSuiteStatus(AutomatedSuite suite) {
        TestSuite testSuite = suite.getTestSuite();
        if (testSuite != null) {
            customTestSuiteModificationService.updateExecutionStatus(List.of(testSuite));
        }
    }

    private void processAutomatedSuitesDeletion(List<String> automatedSuiteIds) {
        List<Long> executionIds = executionDao.findAllIdsByAutomatedSuiteIds(automatedSuiteIds);
        if (!executionIds.isEmpty()) {
            deletionHandler.bulkDeleteExecutions(executionIds);
        }

        List<Long> attachmentListIds = autoSuiteDao.findAttachmentListIdsByIds(automatedSuiteIds);
        List<ExternalContentCoordinates> externalContentCoordinates =
                attachmentManagerService.getListPairContentIDListIDForAutomatedSuiteIds(automatedSuiteIds);

        autoSuiteDao.deleteAutomatedSuiteWorkflowsBySuiteIds(automatedSuiteIds);
        autoSuiteDao.deleteAllByIds(automatedSuiteIds);
        autoTestDao.pruneOrphans();

        attachmentManagerService.removeAttachmentsAndLists(attachmentListIds);
        attachmentManagerService.deleteContents(externalContentCoordinates);
    }

    @Override
    public void deleteAutomatedSuites(List<String> automatedSuiteIds) {
        processAutomatedSuitesDeletion(automatedSuiteIds);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public AutomationDeletionCount countOldAutomatedSuitesAndExecutions() {
        return autoSuiteDao.countOldAutomatedSuitesAndExecutions();
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public AutomationDeletionCount countOldAutomatedSuitesAndExecutionsForProject(Long projectId) {
        return autoSuiteDao.countOldAutomatedSuitesAndExecutionsByProjectId(projectId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void cleanOldSuites() {
        cleanAutomatedSuites(autoSuiteDao.getOldAutomatedSuiteIds());
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void cleanOldSuitesForProject(Long projectId) {
        cleanAutomatedSuites(autoSuiteDao.getOldAutomatedSuiteIdsByProjectId(projectId));
    }

    private void cleanAutomatedSuites(List<String> oldAutomatedSuiteIds) {
        if (oldAutomatedSuiteIds.isEmpty()) {
            return;
        }
        List<List<String>> automatedSuiteIdPartitions =
                Lists.partition(oldAutomatedSuiteIds, BIND_VARIABLES_LIMIT);
        automatedSuiteIdPartitions.forEach(
                automatedSuiteIdPartition -> {
                    processAutomatedSuitesDeletion(automatedSuiteIdPartition);
                    entityManager.flush();
                    entityManager.clear();
                });
    }

    @Override
    @BatchPreventConcurrent(entityType = IterationTestPlanItem.class)
    public AutomatedSuiteOverview createAndExecute(
            AutomatedSuiteCreationSpecification specification, @Ids List<Long> itemTestPlanIds) {
        if (ultimateLicenseService.isAvailable()) {
            setSquashAutomOrchestratorAdditionalConfiguration(specification);
        }

        LOGGER.debug("Start creating executions: {}", new Date());
        AutomatedSuiteWithSquashAutomAutomatedITPIs suiteWithAutomItpis =
                createFromSpecification(specification, itemTestPlanIds);
        LOGGER.debug("End creating executions: {}", new Date());

        LOGGER.debug("Start sending executions: {}", new Date());
        automatedSuiteStartService.start(
                suiteWithAutomItpis,
                specification.getExecutionConfigurations(),
                specification.getSquashAutomExecutionConfigurations());
        LOGGER.debug("End sending executions: {}", new Date());

        LOGGER.debug("Start creating automated suite overview: {}", new Date());
        AutomatedSuiteOverview automatedSuiteOverview =
                AutomatedExecutionViewUtils.buildFirstAutomatedSuiteOverview(suiteWithAutomItpis);
        LOGGER.debug("End creating automated suite overview: {}", new Date());

        setMessages(suiteWithAutomItpis, automatedSuiteOverview);
        return automatedSuiteOverview;
    }

    private static void setMessages(
            AutomatedSuiteWithSquashAutomAutomatedITPIs suiteWithAutomItpis,
            AutomatedSuiteOverview automatedSuiteOverview) {
        if (suiteWithAutomItpis.getErrorMessage() != null) {
            automatedSuiteOverview.setErrorMessage(suiteWithAutomItpis.getErrorMessage());
        } else if (suiteWithAutomItpis.getWorkflowsUUIDs() != null
                && !suiteWithAutomItpis.getWorkflowsUUIDs().isEmpty()) {
            automatedSuiteOverview.setWorkflowsUUIDs(suiteWithAutomItpis.getWorkflowsUUIDs());
        }
    }

    @Override
    @PreAuthorize(EXECUTE_ITERATION_OR_ROLE_ADMIN)
    @BatchPreventConcurrent(entityType = IterationTestPlanItem.class)
    public AutomatedSuite createFromIterationTestPlan(long iterationId, @Ids List<Long> testPlanIds) {
        Long projectId = iterationDao.getProjectId(iterationId);

        Iteration iteration = iterationDao.findById(iterationId);

        List<Long> squashTFAutomatedItems = extractSquashTFAutomatedItems(testPlanIds);

        String newSuiteId = createSuiteAndClearSession(iteration);

        createSquashTFExecutions(squashTFAutomatedItems, newSuiteId, projectId);

        return entityManager.find(AutomatedSuite.class, newSuiteId);
    }

    /**
     * @see AutomatedSuiteManagerService#createFromTestSuiteTestPlan(long, List)
     */
    @Override
    @PreAuthorize(EXECUTE_TS_OR_ROLE_ADMIN)
    @BatchPreventConcurrent(entityType = IterationTestPlanItem.class)
    public AutomatedSuite createFromTestSuiteTestPlan(long testSuiteId, @Ids List<Long> testPlanIds) {
        Long projectId = testSuiteDao.getProjectId(testSuiteId);

        List<Long> squashTFAutomatedItems = extractSquashTFAutomatedItems(testPlanIds);

        TestSuite testSuite = testSuiteDao.findById(testSuiteId).orElseThrow();

        String suiteId = createSuiteAndClearSession(testSuite);

        createSquashTFExecutions(squashTFAutomatedItems, suiteId, projectId);

        return entityManager.find(AutomatedSuite.class, suiteId);
    }

    /**
     * @see AutomatedSuiteManagerService#createFromItemsAndIteration(java.util.List, long)
     */
    @Override
    @BatchPreventConcurrent(entityType = IterationTestPlanItem.class)
    public AutomatedSuite createFromItemsAndIteration(@Ids List<Long> testPlanIds, long iterationId) {
        PermissionsUtils.checkPermission(
                permissionService, testPlanIds, EXECUTE, IterationTestPlanItem.class.getName());

        List<Long> items = testPlanDao.findAllByIdsOrderedByIterationTestPlan(testPlanIds);

        Long projectId = iterationDao.getProjectId(iterationId);

        return createSuite(items, projectId);
    }

    private AutomatedSuite createSuite(List<Long> items, Long projectId) {
        List<Long> squashTFAutomatedItems = extractSquashTFAutomatedItems(items);
        String newSuiteId = createSuiteAndClearSession();

        createSquashTFExecutions(squashTFAutomatedItems, newSuiteId, projectId);

        return entityManager.find(AutomatedSuite.class, newSuiteId);
    }

    // ******************* public methods for executions with Squash TF or Squash AUTOM for automated
    // suite and executions creation ***************************

    // security delegated to permission utils
    private AutomatedSuiteWithSquashAutomAutomatedITPIs createFromSpecification(
            AutomatedSuiteCreationSpecification specification, List<Long> itemTestPlanIds) {
        specification.validate();
        checkPermission(specification);
        Long contextId = specification.getContext().getId();

        Collection<SquashAutomExecutionConfiguration> orchestratorConfigurations =
                specification.getSquashAutomExecutionConfigurations();
        EntityType entityType = specification.getContext().getType();

        return createAutomatedSuite(contextId, orchestratorConfigurations, entityType, itemTestPlanIds);
    }

    private AutomatedSuiteWithSquashAutomAutomatedITPIs createAutomatedSuite(
            Long entityId,
            Collection<SquashAutomExecutionConfiguration> orchestratorConfigurations,
            EntityType entityType,
            List<Long> itemIds) {

        Long projectId = findProjectId(entityType, entityId);

        List<Long> squashTFAutomatedItems = extractSquashTFAutomatedItems(itemIds);

        List<Long> squashOrchestratorAutomatedItems = extractSquashOrchestratorAutomatedItems(itemIds);

        String suiteId = createSuiteAndClearSession(entityId, entityType);

        Map<Long, Long> orchestratorItemExecutionMap =
                createAutomatedExecutions(
                        suiteId,
                        squashTFAutomatedItems,
                        squashOrchestratorAutomatedItems,
                        orchestratorConfigurations,
                        projectId);

        return createAutomatedSuiteWithSquashAutomAutomatedITPIs(suiteId, orchestratorItemExecutionMap);
    }

    private Map<Long, Long> createAutomatedExecutions(
            String suiteId,
            List<Long> squashTFAutomatedItems,
            List<Long> squashOrchestratorAutomatedItems,
            Collection<SquashAutomExecutionConfiguration> orchestratorConfigurations,
            Long projectId) {

        createSquashTFExecutions(squashTFAutomatedItems, suiteId, projectId);

        return createSquashOrchestratorExecutions(
                squashOrchestratorAutomatedItems, suiteId, orchestratorConfigurations, projectId);
    }

    private Long findProjectId(EntityType entityType, Long entityId) {
        if (ITERATION.equals(entityType)) {
            return iterationDao.getProjectId(entityId);
        } else {
            return testSuiteDao.getProjectId(entityId);
        }
    }

    private String createSuiteAndClearSession(Long entityId, EntityType entityType) {
        if (ITERATION.equals(entityType)) {
            return createSuiteAndClearSession(iterationDao.findById(entityId));
        } else {
            return createSuiteAndClearSession(testSuiteDao.findById(entityId).orElseThrow());
        }
    }

    private AutomatedSuiteWithSquashAutomAutomatedITPIs
            createAutomatedSuiteWithSquashAutomAutomatedITPIs(
                    String automatedSuiteId, Map<Long, Long> orchestratorItemExecutionMap) {

        AutomatedSuite suite = entityManager.find(AutomatedSuite.class, automatedSuiteId);

        List<IterationTestPlanItem> items =
                testPlanDao.fetchWithServerByIds(orchestratorItemExecutionMap.keySet());

        AutomatedSuiteWithSquashAutomAutomatedITPIs automatedSuiteWithSquashAutomAutomatedITPIs =
                new AutomatedSuiteWithSquashAutomAutomatedITPIs(suite, items);

        automatedSuiteWithSquashAutomAutomatedITPIs.setItemExecutionMap(orchestratorItemExecutionMap);

        return automatedSuiteWithSquashAutomAutomatedITPIs;
    }

    // ******************* private permission check methods ***************************

    // assumes that the specification was validated first
    private void checkPermission(AutomatedSuiteCreationSpecification specification) {
        List<Long> singleId = new ArrayList<>();
        singleId.add(specification.getContext().getId());

        Class<?> clazz =
                (specification.getContext().getType() == ITERATION) ? Iteration.class : TestSuite.class;
        PermissionsUtils.checkPermission(permissionService, singleId, EXECUTE, clazz.getName());
    }

    @Override
    // security delegated to start(AutomatedSuite, Collection)
    public void start(AutomatedSuite suite) {
        automatedSuiteStartService.start(suite, new ArrayList<>());
    }

    // ******************* create suite private methods ***************************

    private void createSquashTFExecutions(List<Long> itemIds, String newSuiteId, Long projectId) {

        if (itemIds.isEmpty()) {
            return;
        }

        HibernateConfig.enableBatch(entityManager, 20);

        List<List<Long>> partitionedIds = Lists.partition(itemIds, 20);

        Project project = projectDao.fetchForAutomatedExecutionCreation(projectId);

        for (List<Long> ids : partitionedIds) {
            List<Execution> executions = new ArrayList<>();

            Map<Long, Long> itemExecutionMap =
                    createOneBatchOfSquashTFExecution(ids, newSuiteId, executions);

            entityManager.flush();
            entityManager.clear();

            insertBatchItemTestPlanExecutionLink(itemExecutionMap);

            addExternalFields(executions, project);
        }
    }

    private void addExternalFields(List<Execution> executions, Project project) {
        customFieldValuesService.createAllCustomFieldValues(executions, project);
        List<ExecutionStep> steps =
                executions.stream().flatMap(exec -> exec.getSteps().stream()).toList();
        customFieldValuesService.createAllCustomFieldValues(steps, project);

        Map<Long, List<DenormalizedFieldHolder>> map =
                executions.stream()
                        .map(DenormalizedFieldHolder.class::cast)
                        .collect(
                                Collectors.groupingBy(
                                        holder -> ((Execution) holder).getReferencedTestCase().getId()));

        denormalizedFieldValueService.createBatchDenormalizedFieldValues(map, BindableEntity.TEST_CASE);
        denormalizedFieldValueService.createAllDenormalizedFieldValuesForSteps(executions);

        entityManager.flush();
        entityManager.clear();
    }

    private Map<Long, Long> createSquashOrchestratorExecutions(
            List<Long> itemIds,
            String newSuiteId,
            Collection<SquashAutomExecutionConfiguration> configurations,
            Long projectId) {

        if (itemIds.isEmpty()) {
            return Collections.emptyMap();
        }

        HibernateConfig.enableBatch(entityManager, 20);

        List<List<Long>> partitionedIds = Lists.partition(itemIds, 20);

        Map<Long, Long> itemExecutionMap = new HashMap<>();

        Project project = projectDao.fetchForAutomatedExecutionCreation(projectId);

        for (List<Long> ids : partitionedIds) {
            itemExecutionMap.putAll(
                    createOneBatchOfSquashOrchestratorExecutions(ids, newSuiteId, configurations, project));
        }

        return itemExecutionMap;
    }

    private Map<Long, Long> createOneBatchOfSquashOrchestratorExecutions(
            List<Long> ids,
            String newSuiteId,
            Collection<SquashAutomExecutionConfiguration> configurations,
            Project project) {
        AutomatedSuite automatedSuite = entityManager.find(AutomatedSuite.class, newSuiteId);

        List<IterationTestPlanItem> items = testPlanDao.fetchForAutomatedExecutionCreation(ids);

        Map<Long, Long> itemExecutionMap = new HashMap<>();

        List<Execution> createdExecution = new ArrayList<>();

        items.forEach(
                item ->
                        createAutomatedExecution(
                                configurations, item, itemExecutionMap, automatedSuite, createdExecution));

        entityManager.flush();
        entityManager.clear();

        insertBatchItemTestPlanExecutionLink(itemExecutionMap);

        addExternalFields(createdExecution, project);

        return itemExecutionMap;
    }

    // Todo BMS refactor all of automated execution creation in AutomatedExecutionCreationService
    private void insertBatchItemTestPlanExecutionLink(Map<Long, Long> itemExecutionMap) {
        Map<Long, Integer> nextItemExecutionOrderMap =
                testPlanDao.getNextTestPlanExecutionOrders(itemExecutionMap.keySet());

        List<Query> inserts = new ArrayList<>();

        itemExecutionMap.forEach(
                (itemId, executionId) -> {
                    Integer executionOrder = nextItemExecutionOrderMap.get(itemId);
                    if (executionOrder != null) {
                        inserts.add(
                                dslContext
                                        .insertInto(ITEM_TEST_PLAN_EXECUTION)
                                        .set(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID, itemId)
                                        .set(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID, executionId)
                                        .set(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER, executionOrder));
                    }
                });

        dslContext.batch(inserts).execute();
    }

    private void createAutomatedExecution(
            Collection<SquashAutomExecutionConfiguration> configurations,
            IterationTestPlanItem item,
            Map<Long, Long> itemExecutionMap,
            AutomatedSuite automatedSuite,
            List<Execution> createdExecutions) {
        if (!item.isAutomated()) {
            return;
        }

        Execution execution = item.createExecution(null, null);
        AutomatedExecutionExtender extender = new AutomatedExecutionExtender();
        extender.setExecution(execution);
        execution.setAutomatedExecutionExtender(extender);
        saveAutomatedExecution(item, execution);
        itemExecutionMap.put(item.getId(), execution.getId());
        automatedSuite.addExtender(extender);
        createdExecutions.add(execution);

        TestCase testCase = item.getReferencedTestCase();

        SquashAutomExecutionConfiguration configuration =
                configurations.stream()
                        .filter(
                                conf ->
                                        conf.getProjectId().equals(item.getReferencedTestCase().getProject().getId()))
                        .findAny()
                        .orElseThrow();

        AutomatedTestTechnology technology = testCase.getAutomatedTestTechnology();
        if (nonNull(technology)) {
            extender.setTestTechnology(technology.getName());
        }

        createDenormalizedEnvironmentTagsForAutomatedExecution(
                configuration.getEnvironmentTags(), technology, extender);
        createDenormalizedEnvironmentVariablesForAutomatedExecution(
                configuration.getEnvironmentVariables(), extender);
    }

    private void saveAutomatedExecution(IterationTestPlanItem item, Execution execution) {
        executionDao.save(execution);
        item.notifyAutomatedExecutionAddition(execution);
    }

    private String createSuiteAndClearSession() {
        AutomatedSuite newSuite = autoSuiteDao.createNewSuite();
        entityManager.flush();
        String newSuiteId = newSuite.getId();
        entityManager.clear();
        return newSuiteId;
    }

    /** Create an automated suite and link it to the given Iteration. */
    private String createSuiteAndClearSession(Iteration iteration) {
        AutomatedSuite newSuite = autoSuiteDao.createNewSuite(iteration);
        entityManager.flush();
        String newSuiteId = newSuite.getId();
        entityManager.clear();
        return newSuiteId;
    }

    /** Create an automated suite and link it to the given TestSuite */
    private String createSuiteAndClearSession(TestSuite testSuite) {
        AutomatedSuite newSuite = autoSuiteDao.createNewSuite(testSuite);
        entityManager.flush();
        String newSuiteId = newSuite.getId();
        entityManager.clear();
        return newSuiteId;
    }

    private Map<Long, Long> createOneBatchOfSquashTFExecution(
            List<Long> ids, String newSuiteId, List<Execution> executions) {
        AutomatedSuite automatedSuite = entityManager.find(AutomatedSuite.class, newSuiteId);
        List<IterationTestPlanItem> items = testPlanDao.fetchForAutomatedExecutionCreation(ids);

        Map<Long, Long> itemExecutionMap = new HashMap<>();
        for (IterationTestPlanItem item : items) {
            if (item.isAutomated()) {
                Execution execution = item.createAutomatedExecution();
                saveAutomatedExecution(item, execution);
                automatedSuite.addExtender(execution.getAutomatedExecutionExtender());
                executions.add(execution);
                itemExecutionMap.put(item.getId(), execution.getId());
            }
        }

        return itemExecutionMap;
    }

    private void createDenormalizedEnvironmentVariablesForAutomatedExecution(
            Map<String, EnvironmentVariableValue> environmentVariables,
            AutomatedExecutionExtender extender) {
        if (nonNull(environmentVariables) && !environmentVariables.isEmpty()) {
            Map<String, String> variables = new HashMap<>();
            environmentVariables.forEach((key, value) -> variables.put(key, value.getValue()));
            denormalizedEnvironmentVariableManagerService
                    .createAllDenormalizedEnvironmentVariablesForAutomatedExecution(variables, extender);
        }
    }

    private void createDenormalizedEnvironmentTagsForAutomatedExecution(
            List<String> environmentTags,
            AutomatedTestTechnology testTechnology,
            AutomatedExecutionExtender extender) {
        if (isNull(environmentTags)) {
            return;
        }

        List<String> tags = new ArrayList<>(environmentTags);
        String technologyKey = testTechnology.getActionProviderKey();

        if (nonNull(technologyKey) && !technologyKey.isEmpty()) {
            String testTechnologyPrefix = extractTechnologyPrefix(technologyKey);
            if (environmentTags.contains(testTechnologyPrefix)) {
                tags.remove(testTechnologyPrefix);
            }
        }
        denormalizedEnvironmentTagManagerService
                .createAllDenormalizedEnvironmentTagsForAutomatedExecution(tags, extender);
    }

    /** Extract prefix from a technology key. i.e. cucumber5/execute@v1 -> cucumber5 */
    private String extractTechnologyPrefix(String technologyKey) {
        return technologyKey.split("/")[0];
    }

    @Override
    public void pruneAutomatedSuites(List<String> automatedSuiteIds, boolean complete) {
        Map<Long, Long> map =
                autoSuiteDao.getAutomatedExecutionIdsWithAttachmentListIds(automatedSuiteIds, complete);

        List<Long> executionIds = map.keySet().stream().toList();
        List<ExternalContentCoordinates> externalContentCoordinates =
                attachmentManagerService.getListPairContentIDListIDForExecutionIds(executionIds);

        List<Long> attachmentLists = map.values().stream().toList();
        attachmentManagerService.removeAttachmentsAndContents(
                attachmentLists, externalContentCoordinates);
    }

    @Override
    public void pruneAttachments(Long projectId, boolean complete) {
        pruneAutomatedSuites(autoSuiteDao.getOldAutomatedSuiteIdsByProjectId(projectId), complete);
    }

    // ******************* execute suite with itpi (Squash Autom) private methods
    // **************************

    private List<Long> extractSquashOrchestratorAutomatedItems(List<Long> items) {
        return testPlanDao.filterSquashOrchestratorItemIds(items);
    }

    private List<Long> extractSquashTFAutomatedItems(List<Long> itemIds) {
        return testPlanDao.filterSquashTfItemIds(itemIds);
    }

    private void setSquashAutomOrchestratorAdditionalConfiguration(
            AutomatedSuiteCreationSpecification specification) {
        for (SquashAutomExecutionConfiguration additionalConfiguration :
                specification.getSquashAutomExecutionConfigurations()) {
            additionalConfiguration.setAdditionalConfiguration(
                    testAutomationServerDao.getAdditionalConfigurationByProjectId(
                            additionalConfiguration.getProjectId()));
        }
    }
}
