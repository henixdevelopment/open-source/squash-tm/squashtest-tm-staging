/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable;
import org.squashtest.tm.service.annotation.EmptyCollectionGuard;

public interface EnvironmentVariableDao extends JpaRepository<EnvironmentVariable, Long> {

    EnvironmentVariable findByName(String name);

    @Query("select ev from EnvironmentVariable ev where ev.name in :environmentVariableNames")
    List<EnvironmentVariable> findAllByName(
            @Param("environmentVariableNames") Set<String> environmentVariableNames);

    @Query(
            "select ssev from SingleSelectEnvironmentVariable ssev where ssev.id = :environmentVariableId")
    SingleSelectEnvironmentVariable findSingleSelectEnvironmentVariableById(
            @Param("environmentVariableId") Long environmentVariableId);

    @EmptyCollectionGuard
    List<EnvironmentVariable> findAllByIdIn(List<Long> ids);
}
