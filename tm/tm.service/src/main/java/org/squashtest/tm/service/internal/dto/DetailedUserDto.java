/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto;

public class DetailedUserDto extends UserDto {
    private String firstName;
    private String lastName;
    private boolean hasAnyReadPermission; // at least one READ permission
    private boolean isProjectManager; // at least one MANAGE_PROJECT permission
    private boolean isMilestoneManager; // at least one MANAGE_MILESTONE permission
    private boolean isClearanceManager; // at least one MANAGE_PROJECT_CLEARANCE permission
    private boolean isFunctionalTester; // at least one WRITE_AS_FUNCTIONAL permission
    private boolean isAutomationProgrammer; // at least one WRITE_AS_AUTOMATION permission

    /** True if CORE_USER.CAN_DELETE_FROM_FRONT is true AND Premium plugin is present. */
    private boolean canDeleteFromFront;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isCanDeleteFromFront() {
        return canDeleteFromFront;
    }

    public void setCanDeleteFromFront(boolean canDeleteFromFront) {
        this.canDeleteFromFront = canDeleteFromFront;
    }

    public boolean isProjectManager() {
        return isProjectManager;
    }

    public void setProjectManager(boolean projectManager) {
        isProjectManager = projectManager;
    }

    public boolean isMilestoneManager() {
        return isMilestoneManager;
    }

    public void setMilestoneManager(boolean milestoneManager) {
        isMilestoneManager = milestoneManager;
    }

    public boolean isClearanceManager() {
        return isClearanceManager;
    }

    public void setClearanceManager(boolean clearanceManager) {
        isClearanceManager = clearanceManager;
    }

    public boolean isFunctionalTester() {
        return isFunctionalTester;
    }

    public void setFunctionalTester(boolean functionalTester) {
        isFunctionalTester = functionalTester;
    }

    public boolean isAutomationProgrammer() {
        return isAutomationProgrammer;
    }

    public void setAutomationProgrammer(boolean automationProgrammer) {
        isAutomationProgrammer = automationProgrammer;
    }

    public boolean isHasAnyReadPermission() {
        return hasAnyReadPermission;
    }

    public void setHasAnyReadPermission(boolean hasAnyReadPermission) {
        this.hasAnyReadPermission = hasAnyReadPermission;
    }
}
