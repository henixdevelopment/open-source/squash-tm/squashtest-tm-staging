/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.selectCount;
import static org.jooq.impl.DSL.sum;
import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_REPORT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.ResultQuery;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.springframework.security.acls.domain.BasePermission;
import org.squashtest.tm.api.plugin.PluginType;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.project.LibraryPluginBinding;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.internal.batchimport.ProjectLibrariesIds;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.ExecutionEnvironmentCountProjectInfoDto;
import org.squashtest.tm.service.internal.repository.CustomProjectDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

public class ProjectDaoImpl extends HibernateEntityDao<Project> implements CustomProjectDao {

    private static final String PROJECT_ID = "projectId";
    private static final String PROJECT_NAME = "projectName";

    @Inject private DSLContext dsl;

    @PersistenceContext private EntityManager em;

    @Override
    public long countNonFoldersInProject(long projectId) {
        SelectConditionStep<Record1<Integer>> requirementCount =
                selectCount()
                        .from(REQUIREMENT)
                        .innerJoin(REQUIREMENT_LIBRARY_NODE)
                        .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                        .where(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(projectId));

        SelectConditionStep<Record1<Integer>> testCaseCount =
                selectCount()
                        .from(TEST_CASE)
                        .innerJoin(TEST_CASE_LIBRARY_NODE)
                        .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .where(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(projectId));

        SelectConditionStep<Record1<Integer>> campaignCount =
                selectCount()
                        .from(CAMPAIGN)
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(projectId));

        SelectConditionStep<Record1<Integer>> sprintCount =
                selectCount()
                        .from(SPRINT)
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(SPRINT.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(projectId));

        SelectConditionStep<Record1<Integer>> sprintGroupCount =
                selectCount()
                        .from(SPRINT_GROUP)
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(SPRINT_GROUP.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(projectId));

        SelectConditionStep<Record1<Integer>> customReportCount =
                selectCount()
                        .from(CUSTOM_REPORT_LIBRARY_NODE)
                        .innerJoin(PROJECT)
                        .on(CUSTOM_REPORT_LIBRARY_NODE.CRL_ID.eq(PROJECT.CRL_ID))
                        .where(
                                PROJECT
                                        .PROJECT_ID
                                        .eq(projectId)
                                        .and(CUSTOM_REPORT_LIBRARY_NODE.ENTITY_TYPE.notIn("LIBRARY", "FOLDER")));

        return dsl.select(
                        sum(field(requirementCount))
                                .add(field(testCaseCount))
                                .add(field(campaignCount))
                                .add(field(sprintCount))
                                .add(field(sprintGroupCount))
                                .add(field(customReportCount)))
                .fetchOneInto(Integer.class);
    }

    @Override
    public long countNonFolderInActionWord(long projectId) {
        return executeEntityNamedQuery("project.countNonFolderInActionWord", idParameter(projectId));
    }

    private SetQueryParametersCallback idParameter(final long id) {
        return new SetIdParameter(ParameterNames.PROJECT_ID, id);
    }

    @Override
    public List<String> findUsersWhoCreatedTestCases(List<Long> projectIds) {
        if (projectIds.isEmpty()) {
            return Collections.emptyList();
        }

        // Making it with subrequest allow faster request when having lots of data
        // With testing dataset, the time to fetch users who created/modified come from 1.5sec to 120ms
        // on my computer
        Table<Record1<String>> distinctTclnCreatedBy =
                dsl.selectDistinct(TEST_CASE_LIBRARY_NODE.CREATED_BY)
                        .from(TEST_CASE_LIBRARY_NODE)
                        .asTable("DISTINCT_TCLN_CREATED_BY");

        Field<String> createdBy = distinctTclnCreatedBy.field("CREATED_BY", String.class);
        return dsl.selectDistinct(createdBy)
                .from(distinctTclnCreatedBy)
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.CREATED_BY.eq(createdBy))
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .where(TEST_CASE_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(createdBy)
                .fetch(createdBy, String.class);
    }

    @Override
    public List<String> findUsersWhoModifiedTestCases(List<Long> projectIds) {
        if (projectIds.isEmpty()) {
            return Collections.emptyList();
        }
        Table<Record1<String>> distinctTclnLastModifiedBy =
                dsl.selectDistinct(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
                        .from(TEST_CASE_LIBRARY_NODE)
                        .where(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY.isNotNull())
                        .asTable("DISTINCT_TCLN_MODIFIED_BY");

        Field<String> lastModifiedBy =
                distinctTclnLastModifiedBy.field("LAST_MODIFIED_BY", String.class);

        return dsl.selectDistinct(lastModifiedBy)
                .from(distinctTclnLastModifiedBy)
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY.eq(lastModifiedBy))
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .where(TEST_CASE_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(lastModifiedBy)
                .fetch(lastModifiedBy, String.class);
    }

    @Override
    public List<String> findUsersWhoCreatedRequirementVersions(List<Long> projectIds) {
        if (projectIds.isEmpty()) {
            return Collections.emptyList();
        }
        Table<Record1<String>> distinctRlnCreatedBy =
                dsl.selectDistinct(REQUIREMENT_LIBRARY_NODE.CREATED_BY)
                        .from(REQUIREMENT_LIBRARY_NODE)
                        .asTable("DISTINCT_RLN_CREATED_BY");

        Field<String> createdBy = distinctRlnCreatedBy.field("CREATED_BY", String.class);

        return dsl.selectDistinct(createdBy)
                .from(distinctRlnCreatedBy)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(REQUIREMENT_LIBRARY_NODE.CREATED_BY.eq(createdBy))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .where(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(createdBy)
                .fetch(createdBy, String.class);
    }

    @Override
    public List<String> findUsersWhoModifiedRequirementVersions(List<Long> projectIds) {
        if (projectIds.isEmpty()) {
            return Collections.emptyList();
        }
        Table<Record1<String>> distinctResourceLastModifiedBy =
                dsl.selectDistinct(RESOURCE.LAST_MODIFIED_BY)
                        .from(RESOURCE)
                        .where(RESOURCE.LAST_MODIFIED_BY.isNotNull())
                        .asTable("DISTINCT_RESOURCE_MODIFIED_BY");

        Field<String> lastModifiedBy =
                distinctResourceLastModifiedBy.field("LAST_MODIFIED_BY", String.class);

        return dsl.selectDistinct(lastModifiedBy)
                .from(distinctResourceLastModifiedBy)
                .innerJoin(RESOURCE)
                .on(RESOURCE.LAST_MODIFIED_BY.eq(lastModifiedBy))
                .innerJoin(REQUIREMENT_VERSION)
                .on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                .where(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(lastModifiedBy)
                .fetch(lastModifiedBy, String.class);
    }

    @Override
    public List<String> findUsersWhoExecutedItpi(List<Long> projectIds) {
        return dsl.selectDistinct(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY)
                .from(ITERATION_TEST_PLAN_ITEM)
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(ITERATION)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .and(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY.isNotNull())
                .orderBy(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY)
                .fetch(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY, String.class);
    }

    @Override
    public List<String> findUsersAssignedToItpi(List<Long> projectIds) {
        return dsl.selectDistinct(CORE_USER.LOGIN)
                .from(ITERATION_TEST_PLAN_ITEM)
                .innerJoin(CORE_USER)
                .on(ITERATION_TEST_PLAN_ITEM.USER_ID.eq(CORE_USER.PARTY_ID))
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(ITERATION)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(CORE_USER.LOGIN)
                .fetch(CORE_USER.LOGIN, String.class);
    }

    @Override
    public List<String> findProjectNamesByIds(List<Long> projectIds) {
        return dsl.select(PROJECT.NAME)
                .from(PROJECT)
                .where(PROJECT.PROJECT_ID.in(projectIds))
                .orderBy(PROJECT.NAME)
                .fetch(PROJECT.NAME);
    }

    @Override
    @UsedInPlugin("Xsquash4Jira")
    public Long findIdByName(String name) {
        return dsl.select(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .where(PROJECT.NAME.eq(name))
                .fetchOne(PROJECT.PROJECT_ID, Long.class);
    }

    @Override
    public List<Long> findAllProjectIds() {
        return findAllProjectIdsQuery().fetch(PROJECT.PROJECT_ID, Long.class);
    }

    @Override
    public CommonTableExpression<Record1<Long>> findAllProjectIdsCte() {
        return getProjectCTE(findAllProjectIdsQuery());
    }

    @Override
    public boolean checkHasAtLeastOneReadableId(CommonTableExpression<Record1<Long>> projectIdsCte) {
        return dsl.fetchExists(dsl.with(projectIdsCte).selectFrom(projectIdsCte));
    }

    private <T> CommonTableExpression<Record1<T>> getProjectCTE(ResultQuery<Record1<T>> query) {
        return DSL.name(RequestAliasesConstants.PROJECT_CTE)
                .fields(RequestAliasesConstants.PROJECT_ID_CTE)
                .as(query);
    }

    private ResultQuery<Record1<Long>> findAllProjectIdsQuery() {
        return dsl.select(PROJECT.PROJECT_ID).from(PROJECT).where(PROJECT.PROJECT_TYPE.eq("P"));
    }

    @Override
    public List<Long> findAllProjectIdsInListOrderedByName(List<Long> projectIds) {
        return dsl.select(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .where(PROJECT.PROJECT_TYPE.eq("P"))
                .and(PROJECT.PROJECT_ID.in(projectIds))
                .orderBy(PROJECT.NAME)
                .fetch(PROJECT.PROJECT_ID, Long.class);
    }

    @Override
    public List<Long> findAllProjectIdsByPermission(List<Long> partyIds, int permission) {
        return findAllProjectIdsByPermissionMaskAndClassName(
                partyIds, permission, Project.class.getName(), PROJECT.PROJECT_ID);
    }

    @Override
    public CommonTableExpression<Record1<Long>> findAllProjectIdsByPermissionCte(
            List<Long> partyIds, int permission) {
        return getProjectCTE(
                findAllProjectIdsByPermissionMaskAndClassNameQuery(
                        partyIds, permission, Project.class.getName(), PROJECT.PROJECT_ID));
    }

    @Override
    public List<Long> findAllProjectIdsByEligibleTCPermission(List<Long> partyIds, int permission) {

        List<Long> tclIdsByPermission =
                findAllProjectIdsByPermissionMaskAndClassName(
                        partyIds, permission, TestCaseLibrary.CLASS_NAME, PROJECT.TCL_ID);

        return dsl.selectDistinct(TEST_CASE_LIBRARY_NODE.PROJECT_ID)
                .from(TEST_CASE_LIBRARY_NODE)
                .leftJoin(TEST_CASE_LIBRARY_CONTENT)
                .on(TEST_CASE_LIBRARY_CONTENT.CONTENT_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(TCLN_RELATIONSHIP)
                .on(TCLN_RELATIONSHIP.DESCENDANT_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(MILESTONE_TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
                .leftJoin(MILESTONE)
                .on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                .leftJoin(SCRIPTED_TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                .leftJoin(EXPLORATORY_TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                .where(
                        MILESTONE
                                .STATUS
                                .notEqual(MilestoneStatus.LOCKED.toString())
                                .or(MILESTONE.STATUS.isNull())
                                .and(SCRIPTED_TEST_CASE.TCLN_ID.isNull())
                                .and(EXPLORATORY_TEST_CASE.TCLN_ID.isNull())
                                .and(TEST_CASE_LIBRARY_CONTENT.LIBRARY_ID.in(tclIdsByPermission)))
                .fetch(TEST_CASE_LIBRARY_NODE.PROJECT_ID, Long.class);
    }

    @Override
    public List<Long> findAllProjectAndTemplateIds() {
        return dsl.select(PROJECT.PROJECT_ID).from(PROJECT).fetch(PROJECT.PROJECT_ID, Long.class);
    }

    @Override
    public List<Long> findAllProjectIdsOrderedByName(List<Long> partyIds) {
        String className = Project.class.getName();
        SelectConditionStep<Record1<Long>> selectGroupsWithGivenPermissionOnProjects =
                buildSelectGroupsWithGivenPermissionOnProjectsQuery(
                        BasePermission.READ.getMask(), className);

        Table<Record2<Long, String>> selectProjectIdAndNameAccordingToPartyPermissions =
                dsl.selectDistinct(
                                ACL_OBJECT_IDENTITY.IDENTITY.as(PROJECT_ID), PROJECT.NAME.as(PROJECT_NAME))
                        .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                        .innerJoin(ACL_OBJECT_IDENTITY)
                        .on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
                        .innerJoin(ACL_CLASS)
                        .on(ACL_CLASS.ID.eq(ACL_OBJECT_IDENTITY.CLASS_ID))
                        .innerJoin(PROJECT)
                        .on(PROJECT.PROJECT_ID.eq(ACL_OBJECT_IDENTITY.IDENTITY))
                        .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds))
                        .and(ACL_CLASS.CLASSNAME.eq(className))
                        .and(
                                ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.in(
                                        selectGroupsWithGivenPermissionOnProjects))
                        .asTable();

        return dsl.select(selectProjectIdAndNameAccordingToPartyPermissions.field(PROJECT_ID))
                .from(selectProjectIdAndNameAccordingToPartyPermissions)
                .orderBy(selectProjectIdAndNameAccordingToPartyPermissions.field(PROJECT_NAME))
                .fetch(selectProjectIdAndNameAccordingToPartyPermissions.field(PROJECT_ID), Long.class);
    }

    @Override
    public List<Long> findAllProjectIdsByPermissionMaskAndClassName(
            List<Long> partyIds,
            int permissionMask,
            String className,
            TableField<ProjectRecord, Long> libraryColumnField) {
        return findAllProjectIdsByPermissionMaskAndClassNameQuery(
                        partyIds, permissionMask, className, libraryColumnField)
                .fetchInto(Long.class);
    }

    private ResultQuery<Record1<Long>> findAllProjectIdsByPermissionMaskAndClassNameQuery(
            List<Long> partyIds,
            int permissionMask,
            String className,
            TableField<ProjectRecord, Long> libraryColumnField) {
        SelectConditionStep<Record1<Long>> selectGroupsWithGivenPermissionOnProjects =
                buildSelectGroupsWithGivenPermissionOnProjectsQuery(permissionMask, className);

        return dsl.selectDistinct(PROJECT.PROJECT_ID)
                .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .innerJoin(ACL_OBJECT_IDENTITY)
                .on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
                .innerJoin(ACL_CLASS)
                .on(ACL_CLASS.ID.eq(ACL_OBJECT_IDENTITY.CLASS_ID))
                .innerJoin(PROJECT)
                .on(ACL_OBJECT_IDENTITY.IDENTITY.eq(libraryColumnField))
                .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds))
                .and(ACL_CLASS.CLASSNAME.eq(className))
                .and(
                        ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.in(
                                selectGroupsWithGivenPermissionOnProjects));
    }

    @Override
    public List<Long> findAllProjectIdsForAutomationWriter(List<Long> partyIds) {
        return dsl.selectDistinct(ACL_OBJECT_IDENTITY.IDENTITY)
                .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .join(ACL_OBJECT_IDENTITY)
                .on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
                .join(ACL_CLASS)
                .on(ACL_CLASS.ID.eq(ACL_OBJECT_IDENTITY.CLASS_ID))
                .join(ACL_GROUP_PERMISSION)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                .where(
                        ACL_RESPONSIBILITY_SCOPE_ENTRY
                                .PARTY_ID
                                .in(partyIds)
                                .and(ACL_CLASS.CLASSNAME.eq(Project.class.getName()))
                                .and(
                                        ACL_GROUP_PERMISSION.PERMISSION_MASK.eq(
                                                Permissions.WRITE_AS_AUTOMATION.getMask())))
                .fetch(ACL_OBJECT_IDENTITY.IDENTITY, Long.class);
    }

    @Override
    public Integer countProjectsAllowAutomationWorkflow() {
        return dsl.selectCount()
                .from(PROJECT)
                .where(PROJECT.ALLOW_AUTOMATION_WORKFLOW.eq(true))
                .fetchOne()
                .value1();
    }

    @Override
    public LibraryPluginBinding findPluginForProject(Long projectId, PluginType pluginType) {
        LibraryPluginBinding lpb;
        javax.persistence.Query query = entityManager.createNamedQuery("Project.findPluginForProject");
        query.setParameter(PROJECT_ID, projectId);
        query.setParameter("pluginType", pluginType);
        try {
            lpb = (LibraryPluginBinding) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
        return lpb;
    }

    @Override
    public void removeLibraryPluginBindingProperty(Long libraryPluginBindingId) {
        javax.persistence.Query query =
                entityManager.createNativeQuery(NativeQueries.DELETE_LIBRARY_PLUGING_PINDING_PROPERTY);
        query.setParameter("libraryPluginBindingId", libraryPluginBindingId);
        query.executeUpdate();
    }

    @Override
    public BigInteger countActivePluginInProject(long projectId) {
        Query query = em.createNativeQuery(NativeQueries.COUNT_ACTIVE_PLUGIN_IN_PROJECT);
        query.setParameter(PROJECT_ID, projectId);
        return (BigInteger) query.getSingleResult();
    }

    @Override
    public List<Long> findAllManagedProjectIds(List<Long> partyIds) {
        return dsl.selectDistinct(ACL_OBJECT_IDENTITY.IDENTITY)
                .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .join(ACL_OBJECT_IDENTITY)
                .on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
                .join(ACL_CLASS)
                .on(ACL_CLASS.ID.eq(ACL_OBJECT_IDENTITY.CLASS_ID))
                .join(ACL_GROUP_PERMISSION)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                .where(
                        ACL_RESPONSIBILITY_SCOPE_ENTRY
                                .PARTY_ID
                                .in(partyIds)
                                .and(ACL_CLASS.CLASSNAME.eq(Project.class.getName()))
                                .and(ACL_GROUP_PERMISSION.PERMISSION_MASK.eq(Permissions.MANAGE_PROJECT.getMask()))
                                .and(ACL_GROUP_PERMISSION.CLASS_ID.eq(ACL_CLASS.ID)))
                .fetch(ACL_OBJECT_IDENTITY.IDENTITY, Long.class);
    }

    @Override
    public Project fetchForAutomatedExecutionCreation(long projectId) {
        Query query = em.createNamedQuery("Project.fetchForAutomatedExecutionCreation");
        query.setParameter(PROJECT_ID, projectId);
        return (Project) query.getSingleResult();
    }

    @Override
    public String findAutomationWorkflowTypeByProjectId(Long projectId) {
        return dsl.select(PROJECT.AUTOMATION_WORKFLOW_TYPE)
                .from(PROJECT)
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchOneInto(String.class);
    }

    @Override
    public List<ExecutionEnvironmentCountProjectInfoDto> findProjectsAndServersByProjectIds(
            Set<Long> projectIds) {
        return dsl.select(
                        PROJECT.PROJECT_ID, PROJECT.NAME, THIRD_PARTY_SERVER.SERVER_ID, THIRD_PARTY_SERVER.NAME)
                .from(PROJECT)
                .innerJoin(THIRD_PARTY_SERVER)
                .on(THIRD_PARTY_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
                .where(PROJECT.PROJECT_ID.in(projectIds))
                .fetchInto(ExecutionEnvironmentCountProjectInfoDto.class);
    }

    @Override
    public Project fetchByIterationIdForAutomatedExecutionCreation(long iterationId) {
        return em.createQuery(
                        "select project from Iteration it join it.campaign.project project where it.id = :id",
                        Project.class)
                .setParameter(ID, iterationId)
                .getSingleResult();
    }

    @Override
    public Project fetchByTestSuiteForAutomatedExecutionCreation(long testSuiteId) {
        return em.createQuery(
                        "select project from TestSuite suite join suite.iteration.campaign.project project where suite.id = :id",
                        Project.class)
                .setParameter(ID, testSuiteId)
                .getSingleResult();
    }

    @Override
    public List<Long> findTestCaseLibraryNodesIds(Long projectId) {
        return dsl.select(TEST_CASE_LIBRARY_CONTENT.CONTENT_ID)
                .from(PROJECT)
                .innerJoin(TEST_CASE_LIBRARY_CONTENT)
                .on(PROJECT.TCL_ID.eq(TEST_CASE_LIBRARY_CONTENT.LIBRARY_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchInto(Long.class);
    }

    @Override
    public List<Long> findRequirementLibraryNodesIds(Long projectId) {
        return dsl.select(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID)
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_CONTENT)
                .on(PROJECT.RL_ID.eq(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchInto(Long.class);
    }

    @Override
    public List<Long> findCampaignLibraryNodesIds(Long projectId) {
        return dsl.select(CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID)
                .from(PROJECT)
                .innerJoin(CAMPAIGN_LIBRARY_CONTENT)
                .on(PROJECT.CL_ID.eq(CAMPAIGN_LIBRARY_CONTENT.LIBRARY_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchInto(Long.class);
    }

    /*******************
     * PRIVATE METHODS *
     *******************/

    private SelectConditionStep<Record1<Long>> buildSelectGroupsWithGivenPermissionOnProjectsQuery(
            int permissionMask, String classname) {
        return dsl.select(ACL_GROUP.ID)
                .from(ACL_GROUP)
                .innerJoin(ACL_GROUP_PERMISSION)
                .on(ACL_GROUP_PERMISSION.ACL_GROUP_ID.eq(ACL_GROUP.ID))
                .innerJoin(ACL_CLASS)
                .on(ACL_CLASS.ID.eq(ACL_GROUP_PERMISSION.CLASS_ID))
                .where(ACL_GROUP_PERMISSION.PERMISSION_MASK.eq(permissionMask))
                .and(ACL_CLASS.CLASSNAME.eq(classname));
    }

    @Override
    public List<ProjectLibrariesIds> getLibrariesIdsByNames(List<String> projectNames) {
        return dsl.select(
                        PROJECT.PROJECT_ID,
                        PROJECT.NAME,
                        PROJECT.TCL_ID.as("testCaseLibraryId"),
                        PROJECT.RL_ID.as("requirementLibraryId"))
                .from(PROJECT)
                .where(PROJECT.NAME.in(projectNames))
                .fetchInto(ProjectLibrariesIds.class);
    }

    @Override
    public Long findByRequirementLibraryId(long requirementLibraryId) {
        return dsl.select(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .where(PROJECT.RL_ID.eq(requirementLibraryId))
                .fetchOne(PROJECT.PROJECT_ID, Long.class);
    }
}
