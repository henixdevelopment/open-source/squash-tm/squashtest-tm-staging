/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.users.ApiToken;

public interface ApiTokenDao extends JpaRepository<ApiToken, Long> {

    Optional<ApiToken> findByUuid(String uuid);

    void deleteAllByUserId(long userId);

    void deleteByUuid(String tokenUuid);

    // This method is currently used for testing purposes only
    List<ApiToken> findAllByUserId(long userId);

    // This method is used in a scheduler, and needs a new transaction on its own.
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void deleteAllByNameContains(String namePart);

    List<ApiToken> findAllByNameContains(String namePart);

    /**
     * This method should not be used with a userId different from the currentUser's one, except for
     * the case when an administrator needs to access the tokens of a user from the group Test
     * automation server.
     */
    Page<ApiToken> findAllByUserId(long userId, Pageable pageable);
}
