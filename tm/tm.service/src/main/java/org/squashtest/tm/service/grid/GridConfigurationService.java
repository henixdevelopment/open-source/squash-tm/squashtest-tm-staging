/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.grid;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Row;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel;

public interface GridConfigurationService {
    void addOrUpdatePreferenceForCurrentUser(String gridId, List<String> columnIds);

    void addOrUpdateGridColumnConfig(User user, String gridId, List<String> columnIds);

    void resetGridColumnConfig(String gridId);

    List<String> findActiveColumnIdsByGridColumnDisplayReferenceId(Long referenceId);

    List<String> findActiveColumnIdsForUser(String gridId);

    void addOrUpdatePreferenceForCurrentUserWithProjectId(
            String gridId, List<String> activeColumnIds, Long projectId);

    List<String> findActiveColumnIdsForUserWithProjectId(String gridId, Long projectId);

    void resetGridColumnConfigWithProjectId(String gridId, String projectId);

    void deleteColumnConfigForProject(Long projectId);

    Long getCufIdFromCufColumnName(String columnName);

    String reformatSimpleDateForExport(Date date);

    String reformatMultipleDateForExport(String concatenatedDates);

    String reformatLabelForExport(String concatenatedLabel);

    String nullSafeValue(ExportModel.CustomField customField);

    ExportModel.CustomField findCufById(List<ExportModel.CustomField> cufs, Long id, Integer index);

    void registerCuf(
            Row headerRow, int cIdx, String cufColumnName, Map<Long, Integer> cufColumnsByCode);
}
