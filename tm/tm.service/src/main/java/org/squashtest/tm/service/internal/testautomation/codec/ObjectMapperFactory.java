/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.codec;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.opentestfactory.messages.OTFMessage;
import org.opentestfactory.messages.Status;
import org.squashtest.tm.service.internal.testautomation.model.messages.ChannelStatus;
import org.squashtest.tm.service.internal.testautomation.model.messages.VersionStatus;
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowHandle;
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowsStatus;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;

public final class ObjectMapperFactory {
    private ObjectMapperFactory() {}

    public static ObjectMapper getJsonObjectMapper() {
        final JsonFactory jsonFactory = new JsonFactory();
        return getSetupObjectMapper(jsonFactory);
    }

    private static ObjectMapper getSetupObjectMapper(final JsonFactory jsonFactory) {
        final SimpleModule module = new SimpleModule();
        module.addDeserializer(AutomatedExecutionEnvironment.class, new ChannelDeserializer());

        final ObjectMapper objectMapper = new ObjectMapper(jsonFactory);
        objectMapper.registerModule(module);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper
                .addMixIn(OTFMessage.class, SquashTFMessageMixin.class)
                .addMixIn(Status.class, StatusMixin.class)
                .addMixIn(WorkflowHandle.class, WorkflowHandleMixin.class)
                .addMixIn(ChannelStatus.class, ChannelStatusMixin.class)
                .addMixIn(VersionStatus.class, VersionStatusMixin.class)
                .addMixIn(WorkflowsStatus.class, WorkflowsStatusMixin.class);
        return objectMapper;
    }
}
