/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution;

import static org.squashtest.tm.service.security.Authorizations.EXECUTE_EXECUTION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_EXECUTION_OR_ROLE_ADMIN;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.ExploratoryExecutionEvent;
import org.squashtest.tm.domain.execution.ExploratoryExecutionEventType;
import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState;
import org.squashtest.tm.domain.execution.LatestExploratoryExecutionEvent;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.exception.exploratoryexecution.IllegalActionForPausedExploratoryExecutionException;
import org.squashtest.tm.exception.exploratoryexecution.IllegalActionForStoppedExploratoryExecutionException;
import org.squashtest.tm.exception.exploratoryexecution.IllegalExploratoryExecutionEventSequenceException;
import org.squashtest.tm.exception.exploratoryexecution.IllegalExploratoryExecutionStateTransitionException;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.execution.ExploratoryExecutionService;
import org.squashtest.tm.service.internal.repository.ExploratoryExecutionDao;
import org.squashtest.tm.service.internal.repository.ExploratoryExecutionEventDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.security.UserContextService;

@Transactional
@Service
public class ExploratoryExecutionServiceImpl implements ExploratoryExecutionService {

    private final ExploratoryExecutionEventDao exploratoryExecutionEventDao;
    private final UserDao userDao;
    private final UserContextService userContextService;
    private final SprintDisplayDao sprintDisplayDao;
    private final ExploratoryExecutionDao exploratoryExecutionDao;

    @PersistenceContext private EntityManager entityManager;

    public ExploratoryExecutionServiceImpl(
            ExploratoryExecutionEventDao exploratoryExecutionEventDao,
            UserDao userDao,
            UserContextService userContextService,
            SprintDisplayDao sprintDisplayDao,
            ExploratoryExecutionDao exploratoryExecutionDao) {
        this.exploratoryExecutionEventDao = exploratoryExecutionEventDao;
        this.userDao = userDao;
        this.userContextService = userContextService;
        this.sprintDisplayDao = sprintDisplayDao;
        this.exploratoryExecutionDao = exploratoryExecutionDao;
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public void startOrResumeExploratoryExecution(@Id long executionId, String author) {
        checkSprintStatusAllowModification(executionId);

        final ExploratoryExecutionRunningState currentState =
                findExploratoryExecutionRunningState(executionId);

        if (ExploratoryExecutionRunningState.NEVER_STARTED.equals(currentState)) {
            startSession(executionId, author, currentState);
        } else {
            resumeSession(executionId, author, currentState);
        }
    }

    private void startSession(
            long executionId, String author, ExploratoryExecutionRunningState currentState) {
        if (!ExploratoryExecutionRunningState.NEVER_STARTED.equals(currentState)) {
            throw new IllegalExploratoryExecutionStateTransitionException.AlreadyRunning(
                    ExploratoryExecutionEventType.START);
        }

        createEvent(executionId, author, ExploratoryExecutionEventType.START);
        updateExecutionMetadata(executionId);
    }

    private void resumeSession(
            long executionId, String author, ExploratoryExecutionRunningState currentState) {
        if (ExploratoryExecutionRunningState.NEVER_STARTED.equals(currentState)
                || ExploratoryExecutionRunningState.RUNNING.equals(currentState)) {
            throw new IllegalExploratoryExecutionStateTransitionException.AlreadyRunning(
                    ExploratoryExecutionEventType.RESUME);
        }

        createEvent(executionId, author, ExploratoryExecutionEventType.RESUME);
        updateExecutionMetadata(executionId);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    public void pauseExploratoryExecution(@Id long executionId, String author) {
        checkSprintStatusAllowModification(executionId);

        final ExploratoryExecutionRunningState currentState =
                findExploratoryExecutionRunningState(executionId);

        if (!ExploratoryExecutionRunningState.RUNNING.equals(currentState)) {
            throw new IllegalExploratoryExecutionStateTransitionException.AlreadyPaused();
        }

        createEvent(executionId, author, ExploratoryExecutionEventType.PAUSE);
        updateExecutionMetadata(executionId);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    public void stopExploratoryExecution(@Id long executionId, String author) {
        checkSprintStatusAllowModification(executionId);

        final ExploratoryExecutionRunningState currentState =
                findExploratoryExecutionRunningState(executionId);

        if (ExploratoryExecutionRunningState.NEVER_STARTED.equals(currentState)
                || ExploratoryExecutionRunningState.STOPPED.equals(currentState)) {
            throw new IllegalExploratoryExecutionStateTransitionException.AlreadyStopped();
        }

        createEvent(executionId, author, ExploratoryExecutionEventType.STOP);
        updateExecutionMetadata(executionId);
    }

    private void createEvent(
            long executionId, String author, ExploratoryExecutionEventType eventType) {
        final ExploratoryExecution exploratoryExecution =
                entityManager.find(ExploratoryExecution.class, executionId);

        exploratoryExecutionEventDao.save(
                new ExploratoryExecutionEvent(exploratoryExecution, new Date(), author, eventType));
    }

    public void updateExecutionMetadata(long executionId) {
        final Date lastExecutedOn = new Date();
        final String lastExecutedBy = userContextService.getUsername();

        final ExploratoryExecution exploratoryExecution =
                entityManager.find(ExploratoryExecution.class, executionId);
        exploratoryExecution.setLastExecutedOn(lastExecutedOn);
        exploratoryExecution.setLastExecutedBy(lastExecutedBy);
        exploratoryExecution.setExecutionStatus(ExecutionStatus.RUNNING);

        // Forward to the ITPI
        exploratoryExecution.updateTestPlanItemLastExecution(userDao.findUserByLogin(lastExecutedBy));
    }

    @Override
    @PreAuthorize(READ_EXECUTION_OR_ROLE_ADMIN)
    public ExploratoryExecutionRunningState findExploratoryExecutionRunningState(
            @Id long executionId) {
        final List<ExploratoryExecutionEvent> events =
                exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(executionId);

        if (events.isEmpty()) {
            return ExploratoryExecutionRunningState.NEVER_STARTED;
        }

        return switch (events.get(0).getEventType()) {
            case STOP -> ExploratoryExecutionRunningState.STOPPED;
            case PAUSE -> ExploratoryExecutionRunningState.PAUSED;
            case START, RESUME -> ExploratoryExecutionRunningState.RUNNING;
        };
    }

    @Override
    @PreAuthorize(READ_EXECUTION_OR_ROLE_ADMIN)
    public LatestExploratoryExecutionEvent fetchLatestExploratoryExecutionEvent(
            @Id Long executionId) {
        final List<ExploratoryExecutionEvent> events =
                exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDate(executionId);
        if (events.isEmpty()) {
            return null;
        }

        Date latestEventTimestamp = events.get(events.size() - 1).getDate();
        return new LatestExploratoryExecutionEvent(latestEventTimestamp, getTotalTimeElapsed(events));
    }

    @Override
    public void checkSessionIsNotPaused(ExploratoryExecutionRunningState currentState) {
        boolean isPaused = ExploratoryExecutionRunningState.PAUSED.equals(currentState);

        if (isPaused) {
            throw new IllegalActionForPausedExploratoryExecutionException();
        }
    }

    @Override
    public void checkSessionIsNotStopped(ExploratoryExecutionRunningState currentState) {
        boolean isStopped = ExploratoryExecutionRunningState.STOPPED.equals(currentState);

        if (isStopped) {
            throw new IllegalActionForStoppedExploratoryExecutionException();
        }
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public void assignUser(@Id Long executionId, Long userId) {
        checkSprintStatusAllowModification(executionId);

        ExploratoryExecution exploratoryExecution =
                entityManager.find(ExploratoryExecution.class, executionId);
        Optional<User> user = userDao.findById(userId);
        if (user.isPresent()) {
            exploratoryExecution.setAssigneeUser(user.get());
        } else {
            exploratoryExecution.setAssigneeUser(null);
        }
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public void updateTaskDivision(@Id Long executionId, String taskDivision) {
        checkSprintStatusAllowModification(executionId);

        ExploratoryExecution exploratoryExecution =
                entityManager.find(ExploratoryExecution.class, executionId);
        if (taskDivision.isEmpty()) {
            exploratoryExecution.setTaskDivision(null);
        } else {
            exploratoryExecution.setTaskDivision(taskDivision);
        }
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public void updateReviewStatus(@Id Long executionId, boolean reviewed) {
        checkSprintStatusAllowModification(executionId);

        ExploratoryExecution exploratoryExecution =
                entityManager.find(ExploratoryExecution.class, executionId);
        exploratoryExecution.setReviewed(reviewed);
    }

    @Override
    @PreAuthorize(READ_EXECUTION_OR_ROLE_ADMIN)
    public boolean isExecutionRunning(@Id Long executionId) {
        return ExploratoryExecutionRunningState.RUNNING.equals(
                findExploratoryExecutionRunningState(executionId));
    }

    @Override
    public void pauseRunningExploratoryExecutions(Long sprintId) {
        exploratoryExecutionDao.findAllExploratoryExecutionsBySprintId(sprintId).stream()
                .filter(
                        execId ->
                                findExploratoryExecutionRunningState(execId)
                                        .equals(ExploratoryExecutionRunningState.RUNNING))
                .forEach(
                        execId -> {
                            createEvent(
                                    execId, UserContextHolder.getUsername(), ExploratoryExecutionEventType.PAUSE);
                            updateExecutionMetadata(execId);
                        });
    }

    private int getTotalTimeElapsed(List<ExploratoryExecutionEvent> events) {
        int totalTimeElapsed = 0;

        for (int i = 1; i < events.size(); i++) {
            ExploratoryExecutionEvent currentEvent = events.get(i);

            if (isPauseOrStop(currentEvent)) {
                ExploratoryExecutionEvent previousEvent = events.get(i - 1);

                if (isPause(previousEvent)) {
                    // We can have a STOP after a PAUSE. We don't need to add the time difference between
                    // those two
                    continue;
                } else if (!isStartOrResume(previousEvent)) {
                    // We should never have a STOP after another STOP event
                    throw new IllegalExploratoryExecutionEventSequenceException();
                }

                long difference = currentEvent.getDate().getTime() - previousEvent.getDate().getTime();
                totalTimeElapsed += difference / 1000;
            }
        }
        return totalTimeElapsed;
    }

    private boolean isPauseOrStop(ExploratoryExecutionEvent event) {
        return ExploratoryExecutionEventType.PAUSE.equals(event.getEventType())
                || ExploratoryExecutionEventType.STOP.equals(event.getEventType());
    }

    private boolean isPause(ExploratoryExecutionEvent event) {
        return ExploratoryExecutionEventType.PAUSE.equals(event.getEventType());
    }

    private boolean isStartOrResume(ExploratoryExecutionEvent event) {
        return ExploratoryExecutionEventType.START.equals(event.getEventType())
                || ExploratoryExecutionEventType.RESUME.equals(event.getEventType());
    }

    private void checkSprintStatusAllowModification(long executionId) {
        SprintStatus sprintStatus = sprintDisplayDao.getSprintStatusByExecutionId(executionId);

        if (SprintStatus.CLOSED.equals(sprintStatus)) {
            throw new SprintClosedException();
        }
    }
}
