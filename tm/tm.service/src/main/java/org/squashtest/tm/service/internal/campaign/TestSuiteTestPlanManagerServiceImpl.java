/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.ObjectIdentityRetrievalStrategy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.CheckBlockingMilestones;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.audit.AuditModificationService;
import org.squashtest.tm.service.campaign.CreatedTestPlanItems;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.internal.dto.ExecutionSummaryDto;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;

@Service("squashtest.tm.service.TestSuiteTestPlanManagerService")
@Transactional
public class TestSuiteTestPlanManagerServiceImpl implements TestSuiteTestPlanManagerService {

    private static final String HAS_LINK_PERMISSION_ID =
            "hasPermission(#suiteId, 'org.squashtest.tm.domain.campaign.TestSuite', 'LINK') ";

    @Inject private IterationTestPlanManagerService delegateIterationTestPlanManagerService;

    @Inject private TestSuiteDao testSuiteDao;

    @Inject private IterationTestPlanDao itemTestPlanDao;

    @Inject private CustomTestSuiteModificationService customTestSuiteModificationService;

    @Inject private AuditModificationService auditModificationService;

    @Inject
    @Qualifier("squashtest.core.security.ObjectIdentityRetrievalStrategy")
    private ObjectIdentityRetrievalStrategy objIdRetrievalStrategy;

    @Inject private ObjectAclService aclService;

    @Inject private UserDao userDao;

    @Inject private PermissionEvaluationService permissionService;

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize(
            "hasPermission(#testSuiteId, 'org.squashtest.tm.domain.campaign.TestSuite', 'READ') "
                    + OR_HAS_ROLE_ADMIN)
    public TestSuite findTestSuite(long testSuiteId) {
        return testSuiteDao.getReferenceById(testSuiteId);
    }

    @Override
    @CheckBlockingMilestones(entityType = TestSuite.class)
    public void bindTestPlanToMultipleSuites(@Ids List<Long> suiteIds, List<Long> itemTestPlanIds) {
        PermissionsUtils.checkPermission(
                permissionService, suiteIds, Permissions.LINK.name(), TestSuite.class.getName());

        for (Long id : suiteIds) {
            bindTestPlan(id, itemTestPlanIds);
        }
    }

    private void bindTestPlan(long suiteId, List<Long> itemTestPlanIds) {
        TestSuite suite = testSuiteDao.getReferenceById(suiteId);
        suite.bindTestPlanItemsById(itemTestPlanIds);
        customTestSuiteModificationService.updateExecutionStatus(List.of(suite));
        auditModificationService.updateAuditable((AuditableMixin) suite);
        auditModificationService.updateAuditable((AuditableMixin) suite.getIteration());
    }

    /**
     * That method will attach several {@link IterationTestPlanItem} to the given TestSuite. They are
     * identified using their Objects.
     *
     * <p>These entities all belong to the same iteration since they have previously been attached to
     * it.
     *
     * @param testSuite current test Suites
     * @param itemTestPlans current item Test Plans
     */
    private void bindTestPlanObj(TestSuite testSuite, List<IterationTestPlanItem> itemTestPlans) {
        testSuite.bindTestPlanItems(itemTestPlans);
        auditModificationService.updateAuditable((AuditableMixin) testSuite);
        auditModificationService.updateAuditable((AuditableMixin) testSuite.getIteration());
    }

    private void unbindTestPlanObj(TestSuite testSuite, List<IterationTestPlanItem> itemTestPlans) {
        testSuite.unBindTestPlan(itemTestPlans);
        customTestSuiteModificationService.updateExecutionStatus(List.of(testSuite));
        auditModificationService.updateAuditable((AuditableMixin) testSuite);
        auditModificationService.updateAuditable((AuditableMixin) testSuite.getIteration());
    }

    private void unbindTestPlanToMultipleSuites(List<Long> unboundTestSuiteIds, List<Long> itpIds) {
        List<TestSuite> unboundTestSuites = testSuiteDao.findAllById(unboundTestSuiteIds);
        List<IterationTestPlanItem> iterationTestPlanItems = itemTestPlanDao.findAllByIdIn(itpIds);
        for (TestSuite suite : unboundTestSuites) {
            unbindTestPlanObj(suite, iterationTestPlanItems);
        }
    }

    @Override
    public void unbindAllTestPlansFromTestPlanItem(List<Long> itemIds) {
        List<IterationTestPlanItem> items = itemTestPlanDao.findAllByIdIn(itemIds);

        PermissionsUtils.checkPermission(permissionService, items, Permissions.LINK.name());

        Set<Long> affectedTestSuites = new HashSet<>();
        items.forEach(
                item ->
                        affectedTestSuites.addAll(
                                item.getTestSuites().stream().map(TestSuite::getId).collect(Collectors.toSet())));

        unbindTestPlanToMultipleSuites(new ArrayList<>(affectedTestSuites), itemIds);
    }

    @Override
    @PreAuthorize(HAS_LINK_PERMISSION_ID + OR_HAS_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = TestSuite.class)
    public void changeTestPlanPosition(@Id long suiteId, int newIndex, List<Long> itemIds) {

        TestSuite suite = testSuiteDao.getReferenceById(suiteId);

        List<IterationTestPlanItem> items = testSuiteDao.findTestPlanPartition(suiteId, itemIds);

        suite.reorderTestPlan(newIndex, items);
    }

    @Override
    @PreAuthorize(HAS_LINK_PERMISSION_ID + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestSuite.class, paramName = "suiteId")
    @CheckBlockingMilestone(entityType = TestSuite.class)
    public CreatedTestPlanItems addTestCasesToIterationAndTestSuite(
            List<Long> testCaseIds, @Id long suiteId) {
        return addTestCasesToIterationAndTestSuiteUnsecured(testCaseIds, suiteId);
    }

    @Override
    public CreatedTestPlanItems addTestCasesToIterationAndTestSuiteUnsecured(
            List<Long> testCaseIds, long suiteId) {
        TestSuite testSuite = testSuiteDao.getReferenceById(suiteId);

        Iteration iteration = testSuite.getIteration();

        CreatedTestPlanItems listTestPlanItemsToAffectToTestSuite =
                delegateIterationTestPlanManagerService.addTestPlanItemsToIteration(testCaseIds, iteration);

        List<IterationTestPlanItem> itemTestPlans =
                delegateIterationTestPlanManagerService.findTestPlanItems(
                        listTestPlanItemsToAffectToTestSuite.getItemTestPlanIds());

        bindTestPlanObj(testSuite, itemTestPlans);
        customTestSuiteModificationService.updateExecutionStatus(List.of(testSuite));
        auditModificationService.updateAuditable((AuditableMixin) testSuite);
        return listTestPlanItemsToAffectToTestSuite;
    }

    @Override
    @PreAuthorize(HAS_LINK_PERMISSION_ID + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestSuite.class, paramName = "suiteId")
    @CheckBlockingMilestone(entityType = TestSuite.class)
    public void detachTestPlanFromTestSuite(List<Long> testPlanIds, @Id long suiteId) {

        TestSuite testSuite = testSuiteDao.getReferenceById(suiteId);
        List<IterationTestPlanItem> listTestPlanItems = new ArrayList<>();

        for (long testPlanId : testPlanIds) {
            IterationTestPlanItem iterTestPlanItem = itemTestPlanDao.getReferenceById(testPlanId);
            listTestPlanItems.add(iterTestPlanItem);
        }

        unbindTestPlanObj(testSuite, listTestPlanItems);
        customTestSuiteModificationService.updateExecutionStatus(List.of(testSuite));
    }

    @Override
    @Transactional
    @PreAuthorize(HAS_LINK_PERMISSION_ID + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestSuite.class, paramName = "suiteId")
    @CheckBlockingMilestone(entityType = TestSuite.class)
    public boolean detachTestPlanFromTestSuiteAndRemoveFromIteration(
            List<Long> testPlanIds, @Id long suiteId) {
        TestSuite testSuite = testSuiteDao.getReferenceById(suiteId);

        Iteration iteration = testSuite.getIteration();

        auditModificationService.updateAuditable((AuditableMixin) testSuite);

        return delegateIterationTestPlanManagerService.removeTestPlansFromIterationObj(
                testPlanIds, iteration);
    }

    @Override
    public List<User> findAssignableUserForTestPlan(long testSuiteId) {
        TestSuite testSuite = testSuiteDao.findById(testSuiteId).get();

        List<ObjectIdentity> entityRefs = new ArrayList<>();

        ObjectIdentity oid = objIdRetrievalStrategy.getObjectIdentity(testSuite);
        entityRefs.add(oid);

        List<String> loginList = aclService.findUsersWithExecutePermission(entityRefs);

        return userDao.findUsersByLoginList(loginList);
    }

    @Override
    public Map<Long, List<ExecutionSummaryDto>> getLastExecutionStatuses(
            Collection<Long> itemTestPlanIds) {
        return delegateIterationTestPlanManagerService.getLastExecutionStatuses(itemTestPlanIds);
    }
}
