/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.servers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.servers.AuthenticationPolicy;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.servers.OAuth2Credentials;
import org.squashtest.tm.domain.servers.ThirdPartyServer;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.exception.bugtracker.CannotObtainOauth2TokensException;
import org.squashtest.tm.exception.bugtracker.InvalidOauth2RequestException;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.servers.UserCredentialsCache;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.Optional;



/**
 * @see CredentialsProvider
 */
@Service("squashtest.tm.service.CredentialsProvider")
public class CredentialsProviderImpl implements CredentialsProvider {

	private static final Logger LOGGER = LoggerFactory.getLogger(CredentialsProviderImpl.class);
	private static final String CREDENTIALS_FOUND = "CredentialsProviderImpl : credentials found";
	private static final String CREDENTIALS_NOT_FOUND = "CredentialsProviderImpl : credentials not found";
	private static final String CREDENTIALS_FOUND_IN_DB = "CredentialsProviderImpl : found in database";

	@Inject
	private StoredCredentialsManager storedCredentialsManager;

	@Inject
	private FeatureManager featureManager;

	@Inject
	private Provider<OAuth2ConsumerService> oAuth2ConsumerServiceProvider;

	private final ThreadLocal<UserCredentialsCache> threadedCache = new ThreadLocal<>();

	@Override
	public String currentUser() {
		UserCredentialsCache userCache = getCache();
		return userCache.getUser();
	}

	@Override
	public boolean hasCredentials(ThirdPartyServer server) {
		return getCurrentUserCredentials(server).isPresent();
	}

	@Override
	public boolean hasAppLevelCredentials(ThirdPartyServer server) {
		return getAppLevelCredentials(server).isPresent();
	}

	@Override
	public Optional<Credentials> getCurrentUserCredentials(ThirdPartyServer server) {
		LOGGER.debug("CredentialsProviderImpl : looking for credentials for server '{}' for current user", server.getName());

		Credentials credentials = getCredentialsFromCache(server);

		if (credentials == null){
			credentials = getCurrentUserCredentialsFromStore(server);
		}

		Optional<Credentials> option;
		if (credentials != null){
			credentials = refreshOauth2TokensIfNeeded(server.getId(), credentials, currentUser());
			LOGGER.debug(CREDENTIALS_FOUND);
			option = Optional.of(credentials);
		}
		else{
			LOGGER.debug(CREDENTIALS_NOT_FOUND);
			option = Optional.empty();
		}

		return option;

	}

	@Override
	public Optional<Credentials> getUserCredentials(ThirdPartyServer server, String username) {
		LOGGER.debug("CredentialsProviderImpl : looking for credentials for server '{}' for user '{}'", server.getName(), username);
		Credentials credentials = getUserCredentialsFromStore(server, username);

		Optional<Credentials> option;
		if (credentials != null){
			credentials = refreshOauth2TokensIfNeeded(server.getId(), credentials, username);
			LOGGER.debug(CREDENTIALS_FOUND);
			option = Optional.of(credentials);
		}
		else{
			LOGGER.debug(CREDENTIALS_NOT_FOUND);
			option = Optional.empty();
		}

		return option;

	}

	@Override
	public Optional<Credentials> getAppLevelCredentials(ThirdPartyServer server) {

		LOGGER.debug("CredentialsProviderImpl : looking for app-level credentials for server '{}'", server.getName());

		Credentials credentials = getAppLevelCredentialsFromStore(server);

		Optional<Credentials> option;
		if (credentials != null){
			credentials = refreshOauth2TokensIfNeeded(server.getId(), credentials,null);
			LOGGER.debug(CREDENTIALS_FOUND);
			option = Optional.of(credentials);
		}
		else{
			LOGGER.debug(CREDENTIALS_NOT_FOUND);
			option = Optional.empty();
		}

		return option;
	}

	private Credentials refreshOauth2TokensIfNeeded(Long serverId, Credentials creds, String username){
		if(creds.getImplementedProtocol() == AuthenticationProtocol.OAUTH_2) {
			OAuth2Credentials oauth2Creds = (OAuth2Credentials) creds;

			Long dateNow = System.currentTimeMillis();
			Long tokenExpirationDate = oauth2Creds.getExpirationDate();

			if(dateNow > tokenExpirationDate){
				try {
					return oAuth2ConsumerServiceProvider.get().refreshOauth2Token(serverId, oauth2Creds, username);
				} catch (CannotObtainOauth2TokensException | InvalidOauth2RequestException e) {
					LOGGER.error ("Could not refresh Oauth2 token", e);
				}
			}
		}

		return creds;
	}

	private Credentials getCredentialsFromCache(ThirdPartyServer server){
		UserCredentialsCache userCache = getCache();
		Credentials credentials = null;
		if (userCache.hasCredentials(server)){
			credentials = userCache.getCredentials(server);
			LOGGER.trace("CredentialsProviderImpl : found in cache");
		}
		return credentials;
	}

	private Credentials getCurrentUserCredentialsFromStore(ThirdPartyServer server){
		Credentials result = null;
		ManageableCredentials managed = storedCredentialsManager.findUserCredentials(server.getId(), currentUser());
		if (managed != null){
			LOGGER.trace(CREDENTIALS_FOUND_IN_DB);
			result = managed.build(storedCredentialsManager, server, getCache().getUser());
		}
		return result;
	}

	private Credentials getUserCredentialsFromStore(ThirdPartyServer server, String username){
		Credentials result = null;
		ManageableCredentials managed = storedCredentialsManager.findUserCredentials(server.getId(), username);
		if (managed != null){
			LOGGER.trace(CREDENTIALS_FOUND_IN_DB);
			result = managed.build(storedCredentialsManager, server, getCache().getUser());
		}
		return result;
	}

	private Credentials getAppLevelCredentialsFromStore(ThirdPartyServer server){
		Credentials result = null;
		ManageableCredentials managed = storedCredentialsManager.unsecuredFindAppLevelCredentials(server.getId());
		if (managed != null){
			LOGGER.trace(CREDENTIALS_FOUND_IN_DB);
			result = managed.build(storedCredentialsManager, server, null);
		}
		return result;
	}

	// ************** cache management ******************


	@Override
	public void cacheCredentials(ThirdPartyServer server, Credentials credentials) {
		if (server.getAuthenticationPolicy() != AuthenticationPolicy.APP_LEVEL){
			LOGGER.debug("CredentialsProviderImpl : caching credentials for server '{}'", server.getName());
			UserCredentialsCache userCache = getCache();
			userCache.cacheIfAllowed(server, credentials);
		}
		else{
			LOGGER.debug("CredentialsProviderImpl : refused to cache application-level credentials");
		}
	}

	@Override
	public void uncacheCredentials(ThirdPartyServer server) {
		UserCredentialsCache userCache = getCache();
		userCache.uncache(server);
	}

	@Override
	public void restoreCache(UserCredentialsCache credentials) {
		if (credentials == null) {
			throw new NullArgumentException("Cannot store null credentials");
		}
		LOGGER.debug("CredentialsProviderImpl : restoring credentials cache for user '{}'", credentials.getUser());
		this.threadedCache.set(credentials);
	}

	@Override
	public void unloadCache() {
		UserCredentialsCache cache = threadedCache.get();
		if (cache != null) {
			LOGGER.debug("CredentialsProviderImpl : unloading credentials cache for user '{}'", cache.getUser() );
		}
		threadedCache.remove();
	}

    @Override
    public Optional<TokenAuthCredentials> getProjectLevelCredentials(Long serverId, Long projectId) {
        return Optional.ofNullable(storedCredentialsManager.findProjectCredentials(serverId, projectId))
            .filter(TokenAuthCredentials.class::isInstance)
            .map(TokenAuthCredentials.class::cast);
    }

    @Override
	public UserCredentialsCache getCache(){
		UserCredentialsCache userCache = threadedCache.get();

		if (userCache == null){
			LOGGER.trace("CredentialsProviderImpl : current user has no credentials cache (yet).");
			userCache = createDefaultOrDie();
			threadedCache.set(userCache);
		}

		return userCache;
	}

    /*
            Creates an empty instance of UserCredentialsCache, but a user context (ie a security context) is required for that.
            If none is available an exception will be thrown because this provider only deals with user credentials
            (not Squash-TM own credentials), indicating a programmatic error.
         */
	private UserCredentialsCache createDefaultOrDie(){
		LOGGER.debug("CredentialsProviderImpl : attempting to create a default cache");
		String username = UserContextHolder.getUsername();
		if (StringUtils.isBlank(username)){
			throw new IllegalStateException(
				"CredentialsProviderImpl : attempted to get the credentials cache for current user but " +
					"none were found. This is a programming error, which means that either there is no user context, or " +
					"that the thread was initiated in an illegal way (the credentials cache were not loaded from the session)");
		}
		return new UserCredentialsCache(username, featureManager);
	}
}
