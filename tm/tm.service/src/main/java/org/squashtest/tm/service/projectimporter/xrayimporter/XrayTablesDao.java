/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.xrayimporter;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.ObjLongConsumer;
import java.util.stream.Stream;
import org.jooq.Query;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CalledTestXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.CustomFieldTable;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.ItemTable;

public interface XrayTablesDao {

    void createTables(ItemTable itemTable, CustomFieldTable customFieldTable);

    void createItems(List<ItemXrayDto> itemXrayDtos);

    void dropTablesIfExist();

    void executeBatchQueries(List<Query> queries);

    // Update name of the item if it is a duplicate
    void addQueryUpdateItemName(Long itemId, String newName, List<Query> queries);

    void updateCustomFieldPivotId(Long customFieldId, String pivotIdValue);

    boolean isEmptyTestCaseFolder(XrayField.Type type);

    boolean isEmptyItemTable(XrayField.Type... typeValue);

    boolean isEmptyCalledTestCase();

    Stream<Record1<Long>> selectItemTable(XrayField.Type... typeValue);

    void selectTest(Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue);

    Stream<Record1<String>> selectTypeRepositoryFolder(XrayField.Type typeValue);

    void selectCalledTestCasesToAssignDataset(
            String testCaseXrayKey, ObjLongConsumer<String> consumStepCalledTestParameters);

    void selectCalledTestCases(Consumer<List<CalledTestXrayDto>> calledTestXrayDtosConsumer);

    void selectTestPlan(Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue);

    void selectOrphanTestExecution(Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue);

    void selectTestExecutionForIteration(
            Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue);

    Stream<Record3<Long, String, String>> selectDuplicateNameGroupBy(
            XrayField.CustomFieldKey customFieldKey, XrayField.Type... itemTypes);

    Stream<Record2<Long, String>> selectDuplicateName(XrayField.Type... typeValue);

    Stream<Record2<Long, String>> selectDuplicateNameOrphanTestExecution(XrayField.Type... typeValue);

    ItemTable getItemTable();

    CustomFieldTable getCustomFieldTable();
}
