/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.testplanretriever;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Provider;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;
import org.squashtest.tm.service.internal.testautomation.TaParametersBuilder;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.testautomation.testplanretriever.AllCustomFieldValuesForExec;
import org.squashtest.tm.service.testautomation.testplanretriever.CustomFieldValuesForExec;

@Service
@Transactional
public class AutomatedTestBuilderService<C extends CustomFieldValuesForExec> {

    private final UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService;
    protected Provider<TaParametersBuilder> paramBuilder;
    protected CustomFieldValueFinderService customFieldValueFinder;

    @Inject
    protected AutomatedTestBuilderService(
            Provider<TaParametersBuilder> paramBuilder,
            CustomFieldValueFinderService customFieldValueFinder,
            UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService) {
        this.paramBuilder = paramBuilder;
        this.customFieldValueFinder = customFieldValueFinder;
        this.ultimateLicenseAvailabilityService = ultimateLicenseAvailabilityService;
    }

    public Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>
            createTestWithParams(
                    IterationTestPlanItem item,
                    C customFieldValuesForExec,
                    Credentials scrCredentials,
                    Long itemExecutionId,
                    String testCasePath) {
        if (ultimateLicenseAvailabilityService.isAvailable()) {
            return createTestWithParamsUltimate(
                    item,
                    (AllCustomFieldValuesForExec) customFieldValuesForExec,
                    scrCredentials,
                    itemExecutionId,
                    testCasePath);
        } else {
            return createTestWithParamsComunity(
                    item, customFieldValuesForExec, scrCredentials, itemExecutionId, testCasePath);
        }
    }

    private Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>
            createTestWithParamsComunity(
                    IterationTestPlanItem item,
                    CustomFieldValuesForExec customFieldValuesForExec,
                    Credentials scrCredentials,
                    Long itemExecutionId,
                    String testCasePath) {
        TestCase testCase = item.getReferencedTestCase();
        Dataset dataset = item.getReferencedDataset();
        Collection<CustomFieldValue> tcFields =
                customFieldValuesForExec.getValueForTestCase(testCase.getId());
        Map<String, Object> params =
                paramBuilder
                        .get()
                        .testCase()
                        .addEntity(testCase)
                        .addCustomFields(tcFields)
                        .dataset()
                        .addEntity(dataset)
                        .scmRepository()
                        .addEntity(testCase.getScmRepository())
                        .scmRepositoryCredentials()
                        .addEntity(scrCredentials)
                        .build();
        params.put("TC_EXECUTION_ID", itemExecutionId);
        Map<String, Object> metadata = buildTestCaseMetadata(testCase, testCasePath);
        return new ImmutableTriple<>(item, params, metadata);
    }

    private Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>
            createTestWithParamsUltimate(
                    IterationTestPlanItem item,
                    AllCustomFieldValuesForExec customFieldValuesForExec,
                    Credentials scrCredentials,
                    Long itemExecutionId,
                    String testCasePath) {
        TestCase testCase = item.getReferencedTestCase();
        Dataset dataset = item.getReferencedDataset();
        Collection<CustomFieldValue> tcFields =
                customFieldValuesForExec.getValueForTestCase(testCase.getId());
        Collection<CustomFieldValue> iterFields =
                customFieldValuesForExec.getValueForIteration(item.getIteration().getId());
        Collection<CustomFieldValue> campFields =
                customFieldValuesForExec.getValueForCampaign(item.getCampaign().getId());
        Collection<CustomFieldValue> testSuiteFields =
                item.getTestSuites().stream()
                        .map(TestSuite::getId)
                        .map(customFieldValuesForExec::getValueForTestSuite)
                        .flatMap(Collection::stream)
                        .toList();

        Map<String, Object> params =
                paramBuilder
                        .get()
                        .testCase()
                        .addEntity(testCase)
                        .addCustomFields(tcFields)
                        .iteration()
                        .addCustomFields(iterFields)
                        .campaign()
                        .addCustomFields(campFields)
                        .testSuite()
                        .addCustomFields(testSuiteFields)
                        .dataset()
                        .addEntity(dataset)
                        .scmRepository()
                        .addEntity(testCase.getScmRepository())
                        .scmRepositoryCredentials()
                        .addEntity(scrCredentials)
                        .build();
        params.put("TC_EXECUTION_ID", itemExecutionId);
        Map<String, Object> metadata = buildTestCaseMetadata(testCase, testCasePath);
        return new ImmutableTriple<>(item, params, metadata);
    }

    public CustomFieldValuesForExec fetchCustomFieldValues(Collection<IterationTestPlanItem> items) {
        Map<Long, List<CustomFieldValue>> testCaseCfv = fetchTestCaseCfv(items);
        if (ultimateLicenseAvailabilityService.isAvailable()) {
            Map<Long, List<CustomFieldValue>> iterationCfv = fetchIterationCfv(items);
            Map<Long, List<CustomFieldValue>> campaignCfv = fetchCampaignCfv(items);
            Map<Long, List<CustomFieldValue>> testSuiteCfv = fetchTestSuiteCfv(items);
            return new AllCustomFieldValuesForExec(testCaseCfv, iterationCfv, campaignCfv, testSuiteCfv);
        }
        return new CustomFieldValuesForExec(testCaseCfv);
    }

    protected Map<Long, List<CustomFieldValue>> fetchTestCaseCfv(
            Collection<IterationTestPlanItem> items) {
        List<TestCase> testCases = new ArrayList<>();
        for (IterationTestPlanItem item : items) {
            testCases.add(item.getReferencedTestCase());
        }
        return customFieldValueFinder.findAllCustomFieldValues(testCases).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchIterationCfv(
            Collection<IterationTestPlanItem> items) {
        List<Iteration> iterations = new ArrayList<>();
        for (IterationTestPlanItem item : items) {
            iterations.add(item.getIteration());
        }
        return customFieldValueFinder.findAllCustomFieldValues(iterations).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchCampaignCfv(
            Collection<IterationTestPlanItem> items) {
        List<Campaign> campaigns = new ArrayList<>();
        for (IterationTestPlanItem item : items) {
            campaigns.add(item.getCampaign());
        }
        return customFieldValueFinder.findAllCustomFieldValues(campaigns).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchTestSuiteCfv(
            Collection<IterationTestPlanItem> items) {
        List<TestSuite> testSuites = new ArrayList<>();
        for (IterationTestPlanItem item : items) {
            testSuites.addAll(item.getTestSuites());
        }
        return customFieldValueFinder.findAllCustomFieldValues(testSuites).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    protected Map<String, Object> buildTestCaseMetadata(TestCase testCase, String testCasePath) {
        Map<String, Object> metadata = new HashMap<>();
        metadata.put("name", testCase.getName());
        metadata.put("reference", testCase.getAutomatedTestReference());
        metadata.put("uuid", testCase.getUuid());
        metadata.put("nature", testCase.getNature().getCode());
        metadata.put("importance", testCase.getImportance().name());
        metadata.put("type", testCase.getType().getCode());
        metadata.put("technology", testCase.getAutomatedTestTechnology().getName());
        metadata.put("path", getPathAsArray(testCasePath));
        return metadata;
    }

    protected Object getPathAsArray(String testCasePath) {
        return new ArrayList<>(Arrays.asList(testCasePath.split(" > ")));
    }
}
