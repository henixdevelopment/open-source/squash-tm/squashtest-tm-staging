/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.jooq.BatchBindStep;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SelectConditionStep;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.jooq.domain.tables.records.CampaignTestPlanItemRecord;
import org.squashtest.tm.jooq.domain.tables.records.ItemTestPlanListRecord;
import org.squashtest.tm.jooq.domain.tables.records.TestSuiteTestPlanItemRecord;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.TestCaseDeletionDao;

/*
 * we'll perform a lot of operation using SQL because Hibernate whine at bulk-delete on polymorphic entities.
 *
 * See bugs : HHH-4183, HHH-1361, HHH-1657
 *
 */

@Repository
public class HibernateTestCaseDeletionDao extends AbstractHibernateDeletionDao
        implements TestCaseDeletionDao {

    private static final String TEST_CASES_IDS = "testCaseIds";
    private static final String AUTOMATION_REQUEST_IDS = "automationRequestIds";
    private static final String TEST_STEP_IDS = "testStepIds";
    private static final String FOLDER_IDS = "folderIds";
    private final DSLContext dslContext;

    public HibernateTestCaseDeletionDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public void removeEntities(final List<Long> entityIds) {

        if (entityIds.isEmpty()) {
            return;
        }

        Session session = getSession();
        session.setJdbcBatchSize(10000);

        removeEntityFromParentLibraryIfExists(entityIds, session);
        removeEntityFromParentFolderIfExists(entityIds, session);

        removeTestCases(entityIds);
        removeTestCaseLibraryNodes(entityIds);

        session.setJdbcBatchSize(null);
        session.flush();
        session.clear();
    }

    private void removeTestCaseLibraryNodes(List<Long> nodeIds) {
        List<Long> attachmentListIds =
                dslContext
                        .select(ATTACHMENT_LIST.ATTACHMENT_LIST_ID)
                        .from(ATTACHMENT_LIST)
                        .join(TEST_CASE_LIBRARY_NODE)
                        .on(TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID.eq(ATTACHMENT_LIST.ATTACHMENT_LIST_ID))
                        .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.in(nodeIds))
                        .fetchInto(Long.class);

        dslContext
                .deleteFrom(TCLN_RELATIONSHIP_CLOSURE)
                .where(TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.in(nodeIds))
                .execute();
        dslContext
                .deleteFrom(TEST_CASE_LIBRARY_NODE)
                .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.in(nodeIds))
                .execute();
        dslContext
                .deleteFrom(ATTACHMENT)
                .where(ATTACHMENT.ATTACHMENT_LIST_ID.in(attachmentListIds))
                .execute();
        dslContext
                .deleteFrom(ATTACHMENT_LIST)
                .where(ATTACHMENT_LIST.ATTACHMENT_LIST_ID.in(attachmentListIds))
                .execute();
    }

    private void removeTestCases(List<Long> nodeIds) {
        dslContext
                .deleteFrom(MILESTONE_TEST_CASE)
                .where(MILESTONE_TEST_CASE.TEST_CASE_ID.in(nodeIds))
                .execute();
        dslContext.deleteFrom(KEYWORD_TEST_CASE).where(KEYWORD_TEST_CASE.TCLN_ID.in(nodeIds)).execute();
        dslContext
                .deleteFrom(EXPLORATORY_TEST_CASE)
                .where(EXPLORATORY_TEST_CASE.TCLN_ID.in(nodeIds))
                .execute();
        dslContext
                .deleteFrom(SCRIPTED_TEST_CASE)
                .where(SCRIPTED_TEST_CASE.TCLN_ID.in(nodeIds))
                .execute();
        dslContext.deleteFrom(TEST_CASE).where(TEST_CASE.TCLN_ID.in(nodeIds)).execute();
        dslContext.deleteFrom(TEST_CASE_FOLDER).where(TEST_CASE_FOLDER.TCLN_ID.in(nodeIds)).execute();
    }

    private void removeEntityFromParentLibraryIfExists(List<Long> nodeIds, Session session) {
        Map<Long, List<TestCaseLibraryNode>> nodesToDeleteByLibraryId =
                session
                        .createQuery(
                                "select lib.id, lcontent from TestCaseLibrary lib join lib.rootContent lcontent where lcontent.id in :ids",
                                Object[].class)
                        .setParameter(ParameterNames.IDS, nodeIds)
                        .getResultList()
                        .stream()
                        .collect(
                                Collectors.groupingBy(
                                        result -> (Long) result[0],
                                        Collectors.mapping(
                                                result -> (TestCaseLibraryNode) result[1], Collectors.toList())));

        if (nodesToDeleteByLibraryId.isEmpty()) {
            return;
        }

        List<TestCaseLibrary> libraries =
                session
                        .createQuery(
                                "select lib from TestCaseLibrary lib join fetch lib.project join fetch lib.rootContent where lib.id in :ids",
                                TestCaseLibrary.class)
                        .setParameter(ParameterNames.IDS, nodesToDeleteByLibraryId.keySet())
                        .getResultList();

        for (TestCaseLibrary library : libraries) {
            List<TestCaseLibraryNode> nodes = nodesToDeleteByLibraryId.get(library.getId());
            nodes.forEach(library::removeContent);
        }

        session.flush();
        session.clear();
    }

    private void removeEntityFromParentFolderIfExists(List<Long> nodeIds, Session session) {
        Map<Long, List<TestCaseLibraryNode>> nodesToDeleteByFolderId =
                session
                        .createQuery(
                                "select fold.id, fcontent from TestCaseFolder as fold join fold.content fcontent where fcontent.id in :ids",
                                Object[].class)
                        .setParameter(ParameterNames.IDS, nodeIds)
                        .getResultList()
                        .stream()
                        .collect(
                                Collectors.groupingBy(
                                        result -> (Long) result[0],
                                        Collectors.mapping(
                                                result -> (TestCaseLibraryNode) result[1], Collectors.toList())));

        if (nodesToDeleteByFolderId.isEmpty()) {
            return;
        }

        List<TestCaseFolder> folders =
                session
                        .createQuery(
                                "select folder from TestCaseFolder folder join fetch folder.content where folder.id in :ids",
                                TestCaseFolder.class)
                        .setParameter(ParameterNames.IDS, nodesToDeleteByFolderId.keySet())
                        .getResultList();

        for (TestCaseFolder folder : folders) {
            List<TestCaseLibraryNode> nodes = nodesToDeleteByFolderId.get(folder.getId());
            nodes.forEach(folder::removeContent);
        }

        session.flush();
        session.clear();
    }

    @Override
    public void removeAutomationRequestLibraryContent(List<Long> automationRequestIds) {
        if (!automationRequestIds.isEmpty()) {
            executeUpdateSQLQuery(
                    NativeQueries.TESTCASE_SQL_SET_NULL_AUTOMATION_REQUEST,
                    AUTOMATION_REQUEST_IDS,
                    automationRequestIds);
            executeDeleteSQLQuery(
                    NativeQueries.AUTOMATION_REQUEST_SQL_REMOVE_LIBRARY_CONTENT_FROMLIST,
                    AUTOMATION_REQUEST_IDS,
                    automationRequestIds);
            executeDeleteSQLQuery(
                    NativeQueries.AUTOMATION_REQUEST_SQL_REMOVE_REMOTE_AUTOMATION_REQUEST_EXTENDER,
                    AUTOMATION_REQUEST_IDS,
                    automationRequestIds);
            executeDeleteSQLQuery(
                    NativeQueries.AUTOMATION_REQUEST_SQL_REMOVE,
                    AUTOMATION_REQUEST_IDS,
                    automationRequestIds);
        }
    }

    @Override
    public void removeAllSteps(List<Long> testStepIds) {
        if (!testStepIds.isEmpty()) {
            executeDeleteSQLQuery(
                    NativeQueries.TESTCASE_SQL_REMOVETESTSTEPFROMLIST, TEST_STEP_IDS, testStepIds);

            executeDeleteSQLQuery(
                    NativeQueries.TESTSTEP_SQL_REMOVEACTIONSTEPS, TEST_STEP_IDS, testStepIds);
            executeDeleteSQLQuery(NativeQueries.TESTSTEP_SQL_REMOVECALLSTEPS, TEST_STEP_IDS, testStepIds);
            // SQUASH-1018 - constraint violation
            executeDeleteSQLQuery(
                    NativeQueries.TESTSTEP_SQL_REMOVEACTIONWORDPARAMVALUES, TEST_STEP_IDS, testStepIds);
            executeDeleteSQLQuery(
                    NativeQueries.TESTSTEP_SQL_REMOVEKEYWORDSTEPS, TEST_STEP_IDS, testStepIds);
            executeDeleteSQLQuery(NativeQueries.TESTSTEP_SQL_REMOVETESTSTEPS, TEST_STEP_IDS, testStepIds);
        }
    }

    @Override
    public List<Long> findTestSteps(List<Long> testCaseIds) {
        if (!testCaseIds.isEmpty()) {
            return executeSelectNamedQuery("testCase.findAllSteps", TEST_CASES_IDS, testCaseIds);
        }
        return Collections.emptyList();
    }

    @Override
    public List<Long> findTestCaseAttachmentListIds(List<Long> testCaseIds) {
        if (!testCaseIds.isEmpty()) {
            return executeSelectNamedQuery(
                    "testCase.findAllAttachmentLists", TEST_CASES_IDS, testCaseIds);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Long> findTestCaseFolderAttachmentListIds(List<Long> folderIds) {
        if (!folderIds.isEmpty()) {
            return executeSelectNamedQuery(
                    "testCaseFolder.findAllAttachmentLists", FOLDER_IDS, folderIds);
        }
        return Collections.emptyList();
    }

    @Override
    public List<Long> findTestStepAttachmentListIds(List<Long> testStepIds) {
        if (!testStepIds.isEmpty()) {
            return executeSelectNamedQuery("testStep.findAllAttachmentLists", TEST_STEP_IDS, testStepIds);
        }
        return Collections.emptyList();
    }

    /*
     * Cleanup in Campaign test plans prior to test case deletion.
     *
     * Because we have unique key constraints and we need to batch update CTPIs to update their order, we'll proceed
     * like so :
     * - Fetch CTPIs that need to be reordered
     * - Remove all affected CTPIs (those that are effectively deleted and those that need reordering)
     * - Re-insert the CTPIs that need reordering with their new position.
     *
     * Note: You'll see awkward temporary tables (tempCTPI, tempITPI,...) here and there. These are needed
     * for MariaDB which disallow updates where the updated table appears in a sub-select unless we give it
     * an alias.
     */
    @Override
    public void removeCampaignTestPlanInboundReferences(List<Long> testCasesToRemove) {
        if (testCasesToRemove.isEmpty()) {
            return;
        }

        Map<Long, Result<CampaignTestPlanItemRecord>> itemsToReorderByCampaignId =
                fetchCampaignItemsToReorder(testCasesToRemove);
        deleteAffectedCampaignItems(testCasesToRemove);
        batchInsertReorderedCampaignItems(itemsToReorderByCampaignId);
    }

    private Map<Long, Result<CampaignTestPlanItemRecord>> fetchCampaignItemsToReorder(
            List<Long> testCasesToRemove) {

        List<Long> affectedCampaignIds =
                dslContext
                        .select(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID)
                        .from(CAMPAIGN_TEST_PLAN_ITEM)
                        .where(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.in(testCasesToRemove))
                        .fetchInto(Long.class);

        List<Long> itemIdsToRemove =
                dslContext
                        .select(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID)
                        .from(CAMPAIGN_TEST_PLAN_ITEM)
                        .where(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.in(testCasesToRemove))
                        .fetchInto(Long.class);

        if (!affectedCampaignIds.isEmpty()) {
            SelectConditionStep<CampaignTestPlanItemRecord> query =
                    dslContext
                            .selectFrom(CAMPAIGN_TEST_PLAN_ITEM)
                            .where(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.in(affectedCampaignIds));
            if (!itemIdsToRemove.isEmpty()) {
                query = query.and(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.notIn(itemIdsToRemove));
            }

            return query
                    .orderBy(CAMPAIGN_TEST_PLAN_ITEM.TEST_PLAN_ORDER)
                    .fetchGroups(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID);
        } else {
            return new HashMap<>();
        }
    }

    private void deleteAffectedCampaignItems(List<Long> testCasesToRemove) {

        List<Long> affectedCampaignIds =
                dslContext
                        .select(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID)
                        .from(CAMPAIGN_TEST_PLAN_ITEM)
                        .where(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.in(testCasesToRemove))
                        .fetchInto(Long.class);

        if (!affectedCampaignIds.isEmpty()) {
            executeWithinHibernateSession(
                    dslContext
                            .delete(CAMPAIGN_TEST_PLAN_ITEM)
                            .where(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.in(affectedCampaignIds)));
        }
    }

    private void batchInsertReorderedCampaignItems(
            Map<Long, Result<CampaignTestPlanItemRecord>> groupedItemsToReorder) {
        groupedItemsToReorder
                .values()
                .forEach(
                        records ->
                                IntStream.range(0, records.size())
                                        .forEach(
                                                position -> {
                                                    CampaignTestPlanItemRecord record = records.get(position);
                                                    record.setTestPlanOrder(position);
                                                    record.changed(
                                                            true); // We need to set this flag to avoid 'null' columns...
                                                }));

        dslContext
                .batchInsert(groupedItemsToReorder.values().stream().flatMap(Collection::stream).toList())
                .execute();
    }

    /*
     * The process to update an iteration test plan due to test cases removal is similar to the one used for campaign
     * test plans (see note for removeCampaignTestPlanInboundReferences). We need to reorder Iteration TPIs and Test
     * Suite TPIs that are affected by the test cases deletion. We'll do this in-memory by first fetching items that
     * need reordering, deleting all affected items, and reinsert items with a new order.
     */
    @Override
    public void removeOrSetIterationTestPlanInboundReferencesToNull(List<Long> testCasesToRemove) {

        if (testCasesToRemove.isEmpty()) {
            return;
        }

        Set<Long> iterationTestPlanItemsWithoutExecution =
                findIterationTestPlanItemsWithoutExecution(testCasesToRemove);

        if (!iterationTestPlanItemsWithoutExecution.isEmpty()) {
            Set<Long> iterationIds = findIterationsToReorder(iterationTestPlanItemsWithoutExecution);
            Set<Long> testSuiteIds = findTestSuiteToReorder(iterationTestPlanItemsWithoutExecution);
            deleteSessionOverview(iterationTestPlanItemsWithoutExecution);
            deleteItemTestPlanList(iterationTestPlanItemsWithoutExecution);
            deleteTestSuiteTestPlanItem(iterationTestPlanItemsWithoutExecution);
            deleteIterationTestPlanItems(iterationTestPlanItemsWithoutExecution);
            reorderIterations(iterationIds);
            reorderTestSuites(testSuiteIds);
        }

        // now that items without execution has been deleted and iteration/test suite reordered
        // we just have to set TCLN_ID, and DATASET_ID to null into remaining ITPIs
        nullifyReferencedTestCase(testCasesToRemove);
    }

    private void reorderTestSuites(Set<Long> testSuiteIds) {
        if (!testSuiteIds.isEmpty()) {
            doReorderTestSuites(testSuiteIds);
        }
    }

    private void doReorderTestSuites(Set<Long> testSuiteIds) {
        // 1 fetch the remaining items in affected test suite correctly ordered
        // grouped by test suite so we can easily iterate over items in each collection
        Collection<Result<TestSuiteTestPlanItemRecord>> itemGroups =
                fetchTestSuiteTestPlanItemsToReorder(testSuiteIds);

        // 2 delete all TEST_SUITE_TEST_PLAN_ITEM for affected test suites
        deleteRemainingTestSuiteTestPlanItem(testSuiteIds);

        // 3 Items were fetched in good order, we just need to Insert back into
        // TEST_SUITE_TEST_PLAN_ITEM
        //  without any problems (null, negatives values, holes...) in ORDER column.
        //  as the table has no primary key column we cannot use jooq UpdatableRecord, hence the manual
        // batchInsert
        batchReorderTestSuiteTestPlanItems(itemGroups);
    }

    private void batchReorderTestSuiteTestPlanItems(
            Collection<Result<TestSuiteTestPlanItemRecord>> itemGroups) {
        if (!itemGroups.isEmpty()) {
            doBatchReorderTestSuiteTestPlanItems(itemGroups);
        }
    }

    private void doBatchReorderTestSuiteTestPlanItems(
            Collection<Result<TestSuiteTestPlanItemRecord>> itemGroups) {
        BatchBindStep batch =
                dslContext.batch(
                        dslContext
                                .insertInto(
                                        TEST_SUITE_TEST_PLAN_ITEM,
                                        TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID,
                                        TEST_SUITE_TEST_PLAN_ITEM.TPI_ID,
                                        TEST_SUITE_TEST_PLAN_ITEM.TEST_PLAN_ORDER)
                                .values((Long) null, null, null));

        itemGroups.forEach(
                itemGroup ->
                        IntStream.range(0, itemGroup.size())
                                .forEach(
                                        position -> {
                                            TestSuiteTestPlanItemRecord record = itemGroup.get(position);
                                            batch.bind(record.getSuiteId(), record.getTpiId(), position);
                                        }));

        batch.execute();
    }

    private void deleteRemainingTestSuiteTestPlanItem(Set<Long> testSuiteIds) {
        org.jooq.Query deleteQuery =
                dslContext
                        .deleteFrom(TEST_SUITE_TEST_PLAN_ITEM)
                        .where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.in(testSuiteIds));
        executeWithinHibernateSession(deleteQuery);
    }

    private Collection<Result<TestSuiteTestPlanItemRecord>> fetchTestSuiteTestPlanItemsToReorder(
            Set<Long> testSuiteIds) {
        return dslContext
                .selectFrom(TEST_SUITE_TEST_PLAN_ITEM)
                .where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.in(testSuiteIds))
                .orderBy(TEST_SUITE_TEST_PLAN_ITEM.TEST_PLAN_ORDER)
                .fetchGroups(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID)
                .values();
    }

    private void reorderIterations(Set<Long> iterationIds) {
        if (!iterationIds.isEmpty()) {
            doReorderIterations(iterationIds);
        }
    }

    private void doReorderIterations(Set<Long> iterationIds) {
        // 1 fetch the remaining items in affected iterations correctly ordered
        // grouped by iteration so we can easily iterate over items in each collection
        Collection<Result<ItemTestPlanListRecord>> itemGroups =
                fetchIterationTestPlanItemsToReorder(iterationIds);
        // 2 delete all ITEM_TEST_PLAN_LIST for affected iterations
        deleteRemainingItemTestPlanList(iterationIds);
        // 3 Items were fetched in good order, we just need to Insert back into ITEM_TEST_PLAN_LIST
        //  without any problems (null, negatives values, holes...) in ORDER column.
        //  as the table has no primary key column we cannot use jooq UpdatableRecord, hence the manual
        // batchInsert
        batchReorderIterationTestPlanItems(itemGroups);
    }

    private void batchReorderIterationTestPlanItems(
            Collection<Result<ItemTestPlanListRecord>> itemGroups) {
        if (!itemGroups.isEmpty()) {
            doBatchReorderIterationTestPlanItems(itemGroups);
        }
    }

    private void doBatchReorderIterationTestPlanItems(
            Collection<Result<ItemTestPlanListRecord>> itemGroups) {
        BatchBindStep batch =
                dslContext.batch(
                        dslContext
                                .insertInto(
                                        ITEM_TEST_PLAN_LIST,
                                        ITEM_TEST_PLAN_LIST.ITERATION_ID,
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID,
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ORDER)
                                .values((Long) null, null, null));
        // see jooq documentation to see why this form is mandatory. Please check that there will be at
        // least one bind
        // else when executing the batch if no bind is done, it will result in bad statement : insert
        // into... values(null, null, null)

        itemGroups.forEach(
                itemGroup ->
                        IntStream.range(0, itemGroup.size())
                                .forEach(
                                        position -> {
                                            ItemTestPlanListRecord record = itemGroup.get(position);
                                            batch.bind(record.getIterationId(), record.getItemTestPlanId(), position);
                                        }));
        batch.execute();
    }

    private void deleteRemainingItemTestPlanList(Set<Long> iterationIds) {
        if (!iterationIds.isEmpty()) {
            org.jooq.Query deleteQuery =
                    dslContext
                            .deleteFrom(ITEM_TEST_PLAN_LIST)
                            .where(ITEM_TEST_PLAN_LIST.ITERATION_ID.in(iterationIds));
            executeWithinHibernateSession(deleteQuery);
        }
    }

    private void deleteIterationTestPlanItems(Set<Long> iterationTestPlanItemIds) {
        if (!iterationTestPlanItemIds.isEmpty()) {
            org.jooq.Query deleteQuery =
                    dslContext
                            .deleteFrom(ITERATION_TEST_PLAN_ITEM)
                            .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(iterationTestPlanItemIds));
            executeWithinHibernateSession(deleteQuery);
        }
    }

    private void deleteTestSuiteTestPlanItem(Set<Long> iterationTestPlanItemIds) {
        org.jooq.Query deleteQuery =
                dslContext
                        .deleteFrom(TEST_SUITE_TEST_PLAN_ITEM)
                        .where(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.in(iterationTestPlanItemIds));
        executeWithinHibernateSession(deleteQuery);
    }

    private void deleteItemTestPlanList(Set<Long> iterationTestPlanItemIds) {
        org.jooq.Query deleteQuery =
                dslContext
                        .deleteFrom(ITEM_TEST_PLAN_LIST)
                        .where(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.in(iterationTestPlanItemIds));
        executeWithinHibernateSession(deleteQuery);
    }

    private void deleteSessionOverview(Set<Long> iterationTestPlanItemIds) {
        org.jooq.Query deleteQuery =
                dslContext
                        .deleteFrom(EXPLORATORY_SESSION_OVERVIEW)
                        .where(EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.in(iterationTestPlanItemIds));
        executeWithinHibernateSession(deleteQuery);
    }

    private Set<Long> findTestSuiteToReorder(Set<Long> iterationTestPlanItemsWithoutExecution) {
        return dslContext
                .selectDistinct(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID)
                .from(TEST_SUITE_TEST_PLAN_ITEM)
                .where(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.in(iterationTestPlanItemsWithoutExecution))
                .fetchSet(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID);
    }

    private Set<Long> findIterationsToReorder(Set<Long> iterationTestPlanItemsWithoutExecution) {
        return dslContext
                .selectDistinct(ITEM_TEST_PLAN_LIST.ITERATION_ID)
                .from(ITEM_TEST_PLAN_LIST)
                .where(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.in(iterationTestPlanItemsWithoutExecution))
                .fetchSet(ITEM_TEST_PLAN_LIST.ITERATION_ID);
    }

    private Set<Long> findIterationTestPlanItemsWithoutExecution(List<Long> testCaseIds) {
        return dslContext
                .select(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
                .from(ITERATION_TEST_PLAN_ITEM)
                .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .where(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.isNull())
                .and(ITERATION_TEST_PLAN_ITEM.TCLN_ID.in(testCaseIds))
                .fetchSet(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);
    }

    private Collection<Result<ItemTestPlanListRecord>> fetchIterationTestPlanItemsToReorder(
            Set<Long> iterationIds) {
        return dslContext
                .selectFrom(ITEM_TEST_PLAN_LIST)
                .where(ITEM_TEST_PLAN_LIST.ITERATION_ID.in(iterationIds))
                .orderBy(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ORDER)
                .fetchGroups(ITEM_TEST_PLAN_LIST.ITERATION_ID)
                .values();
    }

    private void nullifyReferencedTestCase(List<Long> testCasesToRemove) {
        org.jooq.Query nullifyQuery =
                dslContext
                        .update(ITERATION_TEST_PLAN_ITEM)
                        .set(ITERATION_TEST_PLAN_ITEM.TCLN_ID, (Long) null)
                        .set(ITERATION_TEST_PLAN_ITEM.DATASET_ID, (Long) null)
                        .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.in(testCasesToRemove));

        // We don't use executeWithinHibernateSession because the type information for the 'null'
        // parameter
        // would be lost, causing a SQL error
        NativeQuery<?> nativeQuery = getSession().createNativeQuery(nullifyQuery.getSQL());
        List<Object> bindValues = nullifyQuery.getBindValues();

        nativeQuery.setParameter(1, null, LongType.INSTANCE);
        nativeQuery.setParameter(2, null, LongType.INSTANCE);

        for (int i = 2; i < bindValues.size(); i++) {
            nativeQuery.setParameter(i + 1, bindValues.get(i));
        }

        nativeQuery.executeUpdate();
    }

    private void executeWithinHibernateSession(org.jooq.Query jooqQuery) {
        NativeQuery<?> nativeQuery = getSession().createNativeQuery(jooqQuery.getSQL());
        List<Object> bindValues = jooqQuery.getBindValues();

        for (int i = 0; i < bindValues.size(); i++) {
            nativeQuery.setParameter(i + 1, bindValues.get(i));
        }

        nativeQuery.executeUpdate();
    }

    @Override
    public void setExecStepInboundReferencesToNull(List<Long> testStepIds) {
        if (!testStepIds.isEmpty()) {
            NativeQuery query =
                    getSession()
                            .createNativeQuery(NativeQueries.TESTCASE_SQL_SET_NULL_CALLING_EXECUTION_STEPS);
            query.setParameterList(TEST_STEP_IDS, testStepIds, LongType.INSTANCE);
            query.executeUpdate();
        }
    }

    @Override
    public void setExecutionInboundReferencesToNull(List<Long> testCaseIds) {
        if (!testCaseIds.isEmpty()) {
            NativeQuery query =
                    getSession().createNativeQuery(NativeQueries.TESTCASE_SQL_SETNULLCALLINGEXECUTIONS);
            query.setParameterList(TEST_CASES_IDS, testCaseIds, LongType.INSTANCE);
            query.executeUpdate();
        }
    }

    @Override
    public void removeFromVerifyingTestCaseLists(List<Long> testCaseIds) {
        if (!testCaseIds.isEmpty()) {
            NativeQuery query =
                    getSession().createNativeQuery(NativeQueries.TESTCASE_SQL_REMOVEVERIFYINGTESTCASELIST);
            query.setParameterList(TEST_CASES_IDS, testCaseIds, LongType.INSTANCE);
            query.executeUpdate();
        }
    }

    @Override
    public void removeFromVerifyingTestStepsList(List<Long> testStepIds) {
        if (!testStepIds.isEmpty()) {
            NativeQuery query =
                    getSession().createNativeQuery(NativeQueries.TESTCASE_SQL_REMOVEVERIFYINGTESTSTEPLIST);
            query.setParameterList(TEST_STEP_IDS, testStepIds, LongType.INSTANCE);
            query.executeUpdate();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long>[] separateFolderFromTestCaseIds(List<Long> originalIds) {
        List<Long> folderIds = new ArrayList<>();
        List<Long> testcaseIds = new ArrayList<>();

        List<BigInteger> filtredFolderIds =
                executeSelectSQLQuery(
                        NativeQueries.TESTCASELIBRARYNODE_SQL_FILTERFOLDERIDS, "testcaseIds", originalIds);

        for (Long oId : originalIds) {
            if (filtredFolderIds.contains(BigInteger.valueOf(oId))) {
                folderIds.add(oId);
            } else {
                testcaseIds.add(oId);
            }
        }

        List<Long>[] result = new List[2];
        result[0] = folderIds;
        result[1] = testcaseIds;

        return result;
    }

    @Override
    public List<Long> findRemainingTestCaseIds(List<Long> originalIds) {
        List<BigInteger> rawids =
                executeSelectSQLQuery(
                        NativeQueries.TESTCASE_SQL_FINDNOTDELETED, "allTestCaseIds", originalIds);
        List<Long> tcIds = new ArrayList<>(rawids.size());
        for (BigInteger rid : rawids) {
            tcIds.add(rid.longValue());
        }
        return tcIds;
    }

    @Override
    public void unbindFromMilestone(List<Long> testCaseIds, Long milestoneId) {

        if (!testCaseIds.isEmpty()) {
            NativeQuery query =
                    getSession().createNativeQuery(NativeQueries.TESTCASE_SQL_UNBIND_MILESTONE);
            query.setParameterList(TEST_CASES_IDS, testCaseIds, LongType.INSTANCE);
            query.setParameter("milestoneId", milestoneId);
            query.executeUpdate();
        }
    }

    @Override
    public List<Long> findTestCasesWhichMilestonesForbidsDeletion(List<Long> originalId) {
        if (!originalId.isEmpty()) {
            MilestoneStatus[] blockingStatuses =
                    new MilestoneStatus[MilestoneStatus.MILESTONE_BLOCKING_STATUSES.size()];
            MilestoneStatus.MILESTONE_BLOCKING_STATUSES.toArray(blockingStatuses);
            Query query =
                    getSession().getNamedQuery("testCase.findTestCasesWhichMilestonesForbidsDeletion");
            query.setParameterList(TEST_CASES_IDS, originalId, LongType.INSTANCE);
            query.setParameterList("lockedStatuses", blockingStatuses);
            return query.list();
        } else {
            return new ArrayList<>();
        }
    }
}
