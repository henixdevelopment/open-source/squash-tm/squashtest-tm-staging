/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static java.util.Collections.emptyList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.inject.Provider;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.internal.testautomation.model.IterationTestPlanItemWithCustomFields;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs;
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;
import org.squashtest.tm.service.testautomation.model.SuiteExecutionConfiguration;
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException;
import org.squashtest.tm.service.testautomation.spi.ServerConnectionFailed;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.service.testautomation.spi.UnknownConnectorKind;

/**
 * Service to start the execution of an automated suite. It's an internal service, not meant to be
 * exposed to the outside world. It's only used by AutomatedSuiteManagerServiceImpl.
 */
@Service
public class AutomatedSuiteStartService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AutomatedSuiteStartService.class);

    private static final String START_COLLECTING_AUTOMATED_EXECUTIONS_FOR =
            "-- START COLLECTING AUTOMATED EXECUTIONS FOR ";
    private static final String END_COLLECTING_AUTOMATED_EXECUTIONS_FOR =
            "-- END COLLECTING AUTOMATED EXECUTIONS FOR ";
    private static final String START_SENDING_AUTOMATED_EXECUTIONS_FOR =
            "-- START SENDING AUTOMATED EXECUTIONS FOR ";
    private static final String END_SENDING_AUTOMATED_EXECUTIONS_FOR =
            "-- END SENDING AUTOMATED EXECUTIONS FOR ";
    private static final String TEST_AUTOMATION_UNKNOWN_CONNECTOR =
            "Test Automation : unknown connector :";
    private static final String TEST_AUTOMATION_ERROR_OCCURRED =
            "Test Automation : an error occurred :";

    private final PermissionEvaluationService permissionEvaluationService;
    private final TestAutomationConnectorRegistry connectorRegistry;
    private final AutomatedSuiteDao automatedSuiteDao;
    private final Provider<TaParametersBuilder> parametersBuilderProvider;
    private final CustomFieldValueFinderService customFieldValueFinder;
    private final CredentialsProvider credentialsProvider;

    public AutomatedSuiteStartService(
            PermissionEvaluationService permissionEvaluationService,
            TestAutomationConnectorRegistry connectorRegistry,
            AutomatedSuiteDao automatedSuiteDao,
            Provider<TaParametersBuilder> parametersBuilderProvider,
            CustomFieldValueFinderService customFieldValueFinder,
            CredentialsProvider credentialsProvider) {
        this.permissionEvaluationService = permissionEvaluationService;
        this.connectorRegistry = connectorRegistry;
        this.automatedSuiteDao = automatedSuiteDao;
        this.parametersBuilderProvider = parametersBuilderProvider;
        this.customFieldValueFinder = customFieldValueFinder;
        this.credentialsProvider = credentialsProvider;
    }

    // Left package-private on purpose: only used by AutomatedSuiteManagerServiceImpl
    void start(AutomatedSuite suite, Collection<SuiteExecutionConfiguration> configuration) {
        List<AutomatedExecutionExtender> executionExtenders = getOptimizedExecutionsExtenders(suite);
        String suiteId = suite.getId();
        LOGGER.debug("- START CHECKING EXECUTIONS PERMISSIONS {}", new Date());
        PermissionsUtils.checkPermission(
                permissionEvaluationService, executionExtenders, Permissions.EXECUTE.name());
        LOGGER.debug("- END CHECKING EXECUTIONS PERMISSIONS {}", new Date());
        LOGGER.debug("- START SORTING EXECUTIONS {}", new Date());
        ExtenderSorter sorter = new ExtenderSorter(suite, configuration);
        LOGGER.debug("- END SORTING EXECUTIONS {}", new Date());

        LOGGER.debug("- START COLLECTING AND SENDING ALL AUTOMATED EXECUTIONS {}", new Date());
        Iteration iteration = getIteration(suite, executionExtenders);

        while (sorter.hasNext()) {
            collectAndSendExecutions(sorter, suiteId, iteration);
        }
        LOGGER.debug("- END COLLECTING AND SENDING ALL EXECUTIONS {}", new Date());
    }

    // Left package-private on purpose: only used by AutomatedSuiteManagerServiceImpl
    void start(
            AutomatedSuiteWithSquashAutomAutomatedITPIs suite,
            Collection<SuiteExecutionConfiguration> jenkinsConfigurations,
            Collection<SquashAutomExecutionConfiguration> squashAutomConfigurations) {

        AutomatedSuite automatedSuite = suite.getSuite();
        String suiteId = automatedSuite.getId();
        List<AutomatedExecutionExtender> executionExtenders =
                getOptimizedExecutionsExtenders(automatedSuite);
        List<IterationTestPlanItem> itpis = suite.getSquashAutomAutomatedItems();
        LOGGER.debug("- START CHECKING EXECUTIONS PERMISSIONS {}", new Date());
        PermissionsUtils.checkPermission(
                permissionEvaluationService, executionExtenders, Permissions.EXECUTE.name());
        LOGGER.debug("- END CHECKING EXECUTIONS PERMISSIONS {}", new Date());
        LOGGER.debug("- START CHECKING ITPIS PERMISSIONS {}", new Date());
        PermissionsUtils.checkPermission(
                permissionEvaluationService, itpis, Permissions.EXECUTE.name());
        LOGGER.debug("- END CHECKING ITPIS PERMISSIONS {}", new Date());
        LOGGER.debug("- START SORTING EXECUTIONS {}", new Date());
        ExtenderSorter extenderSorter = new ExtenderSorter(automatedSuite, jenkinsConfigurations);
        LOGGER.debug("- END SORTING EXECUTIONS {}", new Date());
        LOGGER.debug("- START SORTING ITPIS {}", new Date());
        ITPISorter itpiSorter = new ITPISorter(itpis);
        LOGGER.debug("- END SORTING ITPIS {}", new Date());

        LOGGER.debug("- START COLLECTING AND SENDING ALL AUTOMATED EXECUTIONS {}", new Date());
        Iteration iteration = getIteration(automatedSuite, executionExtenders);

        while (extenderSorter.hasNext()) {
            collectAndSendExecutions(extenderSorter, suiteId, iteration);
        }

        LOGGER.debug("- END COLLECTING AND SENDING ALL EXECUTIONS {}", new Date());

        LOGGER.debug("- START COLLECTING AND SENDING ALL AUTOMATED ITPIS {}", new Date());
        collectAndSendItems(suite, squashAutomConfigurations, itpiSorter, suiteId, automatedSuite);
        LOGGER.debug("- END COLLECTING AND SENDING ALL ITPIS {}", new Date());
    }

    private static Iteration getIteration(
            AutomatedSuite automatedSuite, List<AutomatedExecutionExtender> extenders) {
        if (automatedSuite.getIteration() != null) {
            return automatedSuite.getIteration();
        }

        if (automatedSuite.getTestSuite() != null) {
            return automatedSuite.getTestSuite().getIteration();
        }

        return extenders.get(0).getExecution().getTestPlan().getIteration();
    }

    private void collectAndSendItems(
            AutomatedSuiteWithSquashAutomAutomatedITPIs suite,
            Collection<SquashAutomExecutionConfiguration> squashAutomConfigurations,
            ITPISorter itpiSorter,
            String suiteId,
            AutomatedSuite automatedSuite) {
        while (itpiSorter.hasNext()) {
            Map.Entry<TestAutomationServerKind, Collection<IterationTestPlanItem>> itemsByKind =
                    itpiSorter.getNextEntry();

            try {
                TestAutomationConnector connector =
                        connectorRegistry.getConnectorForKind(itemsByKind.getKey());

                LOGGER.debug(
                        START_COLLECTING_AUTOMATED_EXECUTIONS_FOR + itemsByKind.getKey() + " " + new Date());
                Collection<IterationTestPlanItemWithCustomFields> tests =
                        collectItpis(itemsByKind.getValue(), suite.getItemExecutionMap());
                LOGGER.debug(
                        END_COLLECTING_AUTOMATED_EXECUTIONS_FOR + itemsByKind.getKey() + " " + new Date());

                LOGGER.debug(
                        START_SENDING_AUTOMATED_EXECUTIONS_FOR + itemsByKind.getKey() + " " + new Date());
                sendAutomatedExecutions(
                        suite, squashAutomConfigurations, suiteId, automatedSuite, connector, tests);
                LOGGER.debug(
                        END_SENDING_AUTOMATED_EXECUTIONS_FOR + itemsByKind.getKey() + " " + new Date());
            } catch (ServerConnectionFailed | InvalidSquashOrchestratorConfigurationException ex) {
                String errorMessage = ex.getMessage();
                LOGGER.error(errorMessage, ex);
                suite.setErrorMessage(errorMessage);
                updateAutomSuiteStatusToBlockedAndExecutionsStatusToError(suite.getSuite());
            } catch (UnknownConnectorKind ex) {
                LOGGER.error(TEST_AUTOMATION_UNKNOWN_CONNECTOR, ex);
                updateAutomSuiteStatusToBlockedAndExecutionsStatusToError(suite.getSuite());
            } catch (TestAutomationException ex) {
                LOGGER.error(TEST_AUTOMATION_ERROR_OCCURRED, ex);
                updateAutomSuiteStatusToBlockedAndExecutionsStatusToError(suite.getSuite());
            } catch (UnsupportedOperationException ex) {
                LOGGER.debug("Catch Unsupported Operation Exception", ex);
            }
        }
    }

    private static void sendAutomatedExecutions(
            AutomatedSuiteWithSquashAutomAutomatedITPIs suite,
            Collection<SquashAutomExecutionConfiguration> squashAutomConfigurations,
            String suiteId,
            AutomatedSuite automatedSuite,
            TestAutomationConnector connector,
            Collection<IterationTestPlanItemWithCustomFields> tests) {
        List<AutomatedSuiteWorkflow> workflows =
                connector.executeParameterizedTestsBasedOnITPICollection(
                        tests, suiteId, squashAutomConfigurations);

        if (workflows != null) {
            suite.setWorkflowsUUIDs(
                    workflows.stream().map(AutomatedSuiteWorkflow::getWorkflowId).toList());
            automatedSuite.setWorkflows(workflows);
        }
    }

    private List<AutomatedExecutionExtender> getOptimizedExecutionsExtenders(AutomatedSuite suite) {
        LOGGER.debug("- START FETCHING OPTIMIZED EXECUTIONS {}", new Date());
        List<AutomatedExecutionExtender> executionExtenders =
                automatedSuiteDao.findAndFetchForAutomatedExecutionCreation(suite.getId());
        LOGGER.debug("- FETCHED " + executionExtenders.size());
        LOGGER.debug("- END FETCHING OPTIMIZED EXECUTIONS {}", new Date());
        return executionExtenders;
    }

    private Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> collectAutomatedExecs(
            Collection<AutomatedExecutionExtender> extenders, Iteration iteration) {

        Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests =
                new ArrayList<>(extenders.size());

        CustomFieldValuesForExec customFieldValuesForExec = fetchCustomFieldValues(extenders);
        for (AutomatedExecutionExtender extender : extenders) {
            tests.add(createAutomatedExecAndParams(extender, customFieldValuesForExec, iteration));
        }
        return tests;
    }

    private CustomFieldValuesForExec fetchCustomFieldValues(
            Collection<AutomatedExecutionExtender> extenders) {
        Map<Long, List<CustomFieldValue>> testCaseCfv = fetchTestCaseCfv(extenders);
        Map<Long, List<CustomFieldValue>> iterationCfv = fetchIterationCfv(extenders);
        Map<Long, List<CustomFieldValue>> campaignCfv = fetchCampaignCfv(extenders);
        Map<Long, List<CustomFieldValue>> testSuiteCfv = fetchTestSuiteCfv(extenders);
        return new CustomFieldValuesForExec(testCaseCfv, iterationCfv, campaignCfv, testSuiteCfv);
    }

    private Map<Long, List<CustomFieldValue>> fetchTestCaseCfv(
            Collection<AutomatedExecutionExtender> extenders) {
        Set<TestCase> testCases =
                extenders.stream()
                        .map(extender -> extender.getExecution().getReferencedTestCase())
                        .collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(testCases).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchIterationCfv(
            Collection<AutomatedExecutionExtender> extenders) {
        Set<Iteration> iterations =
                extenders.stream()
                        .map(extender -> extender.getIterationTestPlanItem().getIteration())
                        .collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(iterations).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchCampaignCfv(
            Collection<AutomatedExecutionExtender> extenders) {
        Set<Campaign> campaigns =
                extenders.stream()
                        .map(extender -> extender.getIterationTestPlanItem().getIteration().getCampaign())
                        .collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(campaigns).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchTestSuiteCfv(
            Collection<AutomatedExecutionExtender> extenders) {
        Set<TestSuite> testSuites =
                extenders.stream()
                        .map(extender -> extender.getIterationTestPlanItem().getTestSuites())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(testSuites).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private void collectAndSendExecutions(
            ExtenderSorter sorter, String suiteId, Iteration iteration) {
        Map.Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>> extendersByKind =
                sorter.getNextEntry();

        TestAutomationConnector connector;

        try {
            connector = connectorRegistry.getConnectorForKind(extendersByKind.getKey());
            doCollectAndSendExecutions(extendersByKind, connector, suiteId, iteration);
        } catch (UnknownConnectorKind ex) {
            LOGGER.error(TEST_AUTOMATION_UNKNOWN_CONNECTOR, ex);
            notifyExecutionError(extendersByKind.getValue(), ex.getMessage());
        } catch (TestAutomationException ex) {
            LOGGER.error(TEST_AUTOMATION_ERROR_OCCURRED, ex);
            notifyExecutionError(extendersByKind.getValue(), ex.getMessage());
        }
    }

    private void doCollectAndSendExecutions(
            Map.Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>> extendersByKind,
            TestAutomationConnector connector,
            String suiteId,
            Iteration iteration) {
        LOGGER.debug(
                START_COLLECTING_AUTOMATED_EXECUTIONS_FOR + extendersByKind.getKey() + " " + new Date());
        Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests =
                collectAutomatedExecs(extendersByKind.getValue(), iteration);
        LOGGER.debug(
                END_COLLECTING_AUTOMATED_EXECUTIONS_FOR + extendersByKind.getKey() + " " + new Date());
        LOGGER.debug(
                START_SENDING_AUTOMATED_EXECUTIONS_FOR + extendersByKind.getKey() + " " + new Date());
        connector.executeParameterizedTests(tests, suiteId);
        LOGGER.debug(
                END_SENDING_AUTOMATED_EXECUTIONS_FOR + extendersByKind.getKey() + " " + new Date());
    }

    private Collection<IterationTestPlanItemWithCustomFields> collectItpis(
            Collection<IterationTestPlanItem> itpis, Map<Long, Long> itemExecutionMap) {

        Collection<IterationTestPlanItemWithCustomFields> tests = new ArrayList<>(itpis.size());

        CustomFieldValuesForExec customFieldValuesForItpi = fetchCustomFieldValuesForITPIs(itpis);

        Map<Long, Credentials> scmServerCredentialsMap = getScmServerCredentialsMap(itpis);

        for (IterationTestPlanItem item : itpis) {
            tests.add(
                    createItemAndParams(
                            item,
                            customFieldValuesForItpi,
                            itemExecutionMap.get(item.getId()),
                            scmServerCredentialsMap));
        }
        return tests;
    }

    private Map<Long, Credentials> getScmServerCredentialsMap(
            Collection<IterationTestPlanItem> itpis) {

        Map<Long, ScmServer> scmServerMap =
                itpis.stream()
                        .filter(item -> item.getReferencedTestCase().getScmRepository() != null)
                        .map(item -> item.getReferencedTestCase().getScmRepository().getScmServer())
                        .collect(
                                Collectors.toMap(
                                        ScmServer::getId,
                                        server -> server,
                                        (existing, replacement) -> existing,
                                        LinkedHashMap::new));

        Map<Long, Credentials> scmServerCredentialsMap = new HashMap<>();

        scmServerMap
                .values()
                .forEach(
                        server -> {
                            Optional<Credentials> maybeCredentials =
                                    credentialsProvider.getAppLevelCredentials(server);
                            scmServerCredentialsMap.put(server.getId(), maybeCredentials.orElse(null));
                        });

        return scmServerCredentialsMap;
    }

    private CustomFieldValuesForExec fetchCustomFieldValuesForITPIs(
            Collection<IterationTestPlanItem> items) {
        Map<Long, List<CustomFieldValue>> testCaseCfv = fetchTestCaseCfvForITPIs(items);
        Map<Long, List<CustomFieldValue>> iterationCfv = fetchIterationCfvForITPIs(items);
        Map<Long, List<CustomFieldValue>> campaignCfv = fetchCampaignCfvForITPIs(items);
        Map<Long, List<CustomFieldValue>> testSuiteCfv = fetchTestSuiteCfvForITPIs(items);
        return new CustomFieldValuesForExec(testCaseCfv, iterationCfv, campaignCfv, testSuiteCfv);
    }

    private Map<Long, List<CustomFieldValue>> fetchTestCaseCfvForITPIs(
            Collection<IterationTestPlanItem> items) {
        Set<TestCase> testCases =
                items.stream()
                        .map(IterationTestPlanItem::getReferencedTestCase)
                        .collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(testCases).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchIterationCfvForITPIs(
            Collection<IterationTestPlanItem> items) {
        Set<Iteration> iterations =
                items.stream().map(IterationTestPlanItem::getIteration).collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(iterations).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchCampaignCfvForITPIs(
            Collection<IterationTestPlanItem> items) {
        Set<Campaign> campaigns =
                items.stream().map(item -> item.getIteration().getCampaign()).collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(campaigns).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private Map<Long, List<CustomFieldValue>> fetchTestSuiteCfvForITPIs(
            Collection<IterationTestPlanItem> extenders) {
        Set<TestSuite> testSuites =
                extenders.stream()
                        .map(IterationTestPlanItem::getTestSuites)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());
        return customFieldValueFinder.findAllCustomFieldValues(testSuites).stream()
                .collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
    }

    private IterationTestPlanItemWithCustomFields createItemAndParams(
            IterationTestPlanItem item,
            CustomFieldValuesForExec customFieldValuesForExec,
            Long itemExecutionId,
            Map<Long, Credentials> scmServerCredentialsMap) {

        Collection<CustomFieldValue> tcFields =
                customFieldValuesForExec.getValueForTestcase(item.getReferencedTestCase().getId());
        Collection<CustomFieldValue> iterFields =
                customFieldValuesForExec.getValueForIteration(item.getIteration().getId());
        Collection<CustomFieldValue> campFields =
                customFieldValuesForExec.getValueForCampaign(item.getCampaign().getId());
        Collection<CustomFieldValue> testSuiteFields =
                item.getTestSuites().stream()
                        .map(TestSuite::getId)
                        .map(customFieldValuesForExec::getValueForTestSuite)
                        .flatMap(Collection::stream)
                        .toList();

        Credentials credentials = null;

        ScmRepository repository = item.getReferencedTestCase().getScmRepository();

        if (repository != null) {
            if (repository.getScmServer().isCredentialsNotShared()) {
                LOGGER.info(
                        "Do not share Git credentials with the execution environment for the SCM server : {}. Please make sure you have correctly configured Git credentials in the execution environment.",
                        repository.getScmServer().getFriendlyName());
            } else {
                credentials = scmServerCredentialsMap.get(repository.getScmServer().getId());
            }
        }

        Map<String, Object> params =
                parametersBuilderProvider
                        .get()
                        .testCase()
                        .addEntity(item.getReferencedTestCase())
                        .addCustomFields(tcFields)
                        .iteration()
                        .addCustomFields(iterFields)
                        .campaign()
                        .addCustomFields(campFields)
                        .testSuite()
                        .addCustomFields(testSuiteFields)
                        .dataset()
                        .addEntity(item.getReferencedDataset())
                        .scmRepository()
                        .addEntity(repository)
                        .scmRepositoryCredentials()
                        .addEntity(credentials)
                        .build();

        params.put("TC_EXECUTION_ID", itemExecutionId);
        return new IterationTestPlanItemWithCustomFields(item, params);
    }

    private void updateAutomSuiteStatusToBlockedAndExecutionsStatusToError(AutomatedSuite suite) {
        suite.setExecutionStatus(ExecutionStatus.BLOCKED);
        suite
                .getExecutionExtenders()
                .forEach(extender -> extender.getExecution().setExecutionStatus(ExecutionStatus.ERROR));
    }

    private Couple<AutomatedExecutionExtender, Map<String, Object>> createAutomatedExecAndParams(
            AutomatedExecutionExtender extender,
            CustomFieldValuesForExec customFieldValuesForExec,
            Iteration iteration) {
        Execution execution = extender.getExecution();

        Collection<CustomFieldValue> tcFields =
                customFieldValuesForExec.getValueForTestcase(execution.getReferencedTestCase().getId());
        Collection<CustomFieldValue> iterFields =
                customFieldValuesForExec.getValueForIteration(iteration.getId());
        Collection<CustomFieldValue> campFields =
                customFieldValuesForExec.getValueForCampaign(iteration.getCampaign().getId());
        Collection<CustomFieldValue> testSuiteFields =
                execution.getTestPlan().getTestSuites().stream()
                        .map(TestSuite::getId)
                        .map(customFieldValuesForExec::getValueForTestSuite)
                        .flatMap(Collection::stream)
                        .toList();

        Map<String, Object> params =
                parametersBuilderProvider
                        .get()
                        .testCase()
                        .addEntity(execution.getReferencedTestCase())
                        .addCustomFields(tcFields)
                        .iteration()
                        .addCustomFields(iterFields)
                        .campaign()
                        .addCustomFields(campFields)
                        .testSuite()
                        .addCustomFields(testSuiteFields)
                        .dataset()
                        .addEntity(execution.getTestPlan().getReferencedDataset())
                        .build();

        return new Couple<>(extender, params);
    }

    private void notifyExecutionError(
            Collection<AutomatedExecutionExtender> failedExecExtenders, String message) {
        for (AutomatedExecutionExtender extender : failedExecExtenders) {
            extender.setExecutionStatus(ExecutionStatus.ERROR);
            extender.setResultSummary(HtmlUtils.htmlEscape(message));
        }
    }

    private static class ExtenderSorter {
        private final Map<Long, SuiteExecutionConfiguration> configurationByProject;

        private final Map<TestAutomationServerKind, Collection<AutomatedExecutionExtender>>
                extendersByKind;

        private Iterator<Map.Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>>>
                iterator = null;

        public ExtenderSorter(
                AutomatedSuite suite, Collection<SuiteExecutionConfiguration> configuration) {

            configurationByProject = new HashMap<>(configuration.size());

            for (SuiteExecutionConfiguration conf : configuration) {
                configurationByProject.put(conf.getProjectId(), conf);
            }

            // rem : previous impl relied on a HashMap, which broke the tests on java 8. as I have no damn
            // clue about
            // the desired order, let's retort to keys natural order using a TreeMap
            extendersByKind = new TreeMap<>();

            for (AutomatedExecutionExtender extender : suite.getExecutionExtenders()) {
                if (extender.getAutomatedTest() != null) {
                    TestAutomationServerKind serverKind =
                            extender.getAutomatedTest().getProject().getServer().getKind();

                    register(extender, serverKind);
                }
            }

            iterator = extendersByKind.entrySet().iterator();
        }

        public boolean hasNext() {
            return iterator.hasNext();
        }

        public Map.Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>>
                getNextEntry() {
            return iterator.next();
        }

        private void register(
                AutomatedExecutionExtender extender, TestAutomationServerKind serverKind) {

            SuiteExecutionConfiguration conf =
                    configurationByProject.get(extender.getAutomatedProject().getId());

            if (conf != null) {
                extender.setNodeName(conf.getNode());
            }

            extendersByKind.computeIfAbsent(serverKind, k -> new LinkedList<>()).add(extender);
        }
    }

    private static class ITPISorter {

        private final Map<TestAutomationServerKind, Collection<IterationTestPlanItem>> itemsByKind;

        private Iterator<Map.Entry<TestAutomationServerKind, Collection<IterationTestPlanItem>>>
                iterator = null;

        public ITPISorter(List<IterationTestPlanItem> items) {
            // rem : previous impl relied on a HashMap, which broke the tests on java 8. as I have no damn
            // clue about
            // the desired order, let's retort to keys natural order using a TreeMap
            itemsByKind = new TreeMap<>();

            for (IterationTestPlanItem item : items) {
                TestAutomationServerKind serverKind =
                        item.getReferencedTestCase().getProject().getTestAutomationServer().getKind();

                register(item, serverKind);
            }

            iterator = itemsByKind.entrySet().iterator();
        }

        public boolean hasNext() {
            return iterator.hasNext();
        }

        public Map.Entry<TestAutomationServerKind, Collection<IterationTestPlanItem>> getNextEntry() {
            return iterator.next();
        }

        private void register(IterationTestPlanItem extender, TestAutomationServerKind serverKind) {
            itemsByKind.computeIfAbsent(serverKind, k -> new LinkedList<>()).add(extender);
        }
    }

    private record CustomFieldValuesForExec(
            Map<Long, List<CustomFieldValue>> testCaseCfv,
            Map<Long, List<CustomFieldValue>> iterationCfv,
            Map<Long, List<CustomFieldValue>> campaignCfv,
            Map<Long, List<CustomFieldValue>> suiteCfv) {
        public List<CustomFieldValue> getValueForTestcase(Long testCaseId) {
            return testCaseCfv.getOrDefault(testCaseId, emptyList());
        }

        public List<CustomFieldValue> getValueForIteration(Long iterationId) {
            return iterationCfv.getOrDefault(iterationId, emptyList());
        }

        public List<CustomFieldValue> getValueForCampaign(Long campaignId) {
            return campaignCfv.getOrDefault(campaignId, emptyList());
        }

        public List<CustomFieldValue> getValueForTestSuite(Long suiteId) {
            return suiteCfv.getOrDefault(suiteId, emptyList());
        }
    }
}
