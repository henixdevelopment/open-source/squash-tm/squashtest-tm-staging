/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import static org.jooq.impl.DSL.select;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.deletion.jdbc.delegate.AbstractDelegateDeleteQuery.extractFieldTableNameAsParam;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.TableOnConditionStep;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.service.internal.api.repository.HibernateSessionClearing;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

/**
 * Delete iterations using fast and optimized sql, allowing to avoid the whole 'hibernate web' used
 * as domain. With a proper domain and some simple database cascade this class shouldn't exist...
 */
public class JdbcIterationDeletionHandler extends AbstractExecutionDeletionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcIterationDeletionHandler.class);

    private final Set<Long> iterationIds;

    public JdbcIterationDeletionHandler(
            Collection<Long> iterationIds,
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper,
            String operationId) {
        super(dslContext, entityManager, attachmentRepository, reorderHelper, operationId);
        this.iterationIds = new HashSet<>(iterationIds);
    }

    @Override
    protected TableOnConditionStep<Record> joinToExecution() {
        return joinToIterationTestPlanItem()
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(
                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .innerJoin(Tables.EXECUTION)
                .on(Tables.EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID));
    }

    private TableOnConditionStep<Record> joinToTestSuite() {
        return ITERATION
                .innerJoin(ITERATION_TEST_SUITE)
                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(TEST_SUITE)
                .on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID));
    }

    private TableOnConditionStep<Record> joinToIterationTestPlanItem() {
        return ITERATION
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID));
    }

    @Override
    protected Condition getPredicate() {
        return ITERATION.ITERATION_ID.in(iterationIds);
    }

    @HibernateSessionClearing
    public void deleteIterations() {
        logStartProcess();
        clearPersistenceContext();
        storeEntitiesToDeleteIntoWorkingTable();
        performDeletions();
        reorderCampaigns();
        cleanWorkingTable();
        logEndProcess();
    }

    private void logEndProcess() {
        LOGGER.info(
                String.format(
                        "Deleted Iterations %s. Time elapsed %s",
                        iterationIds, startDate.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
    }

    private void performDeletions() {
        deleteItemTestPlanExecutions();
        performExecutionDeletions();
        deleteFailureDetails();
        deleteTestSuites();
        deleteIterationTestPlanItem();
        deleteIterationEntities();
        deleteCustomFieldValues();
        deleteAttachmentLists();
        deleteAttachmentContents();
    }

    private void reorderCampaigns() {
        // Here we fetch the ids, but the list should be rather small, because it's the campaign top
        // object.
        List<Long> campaignIds = workingTables.selectIds(CAMPAIGN.CLN_ID);
        reorderHelper.reorder(
                campaignIds,
                CAMPAIGN_ITERATION,
                CAMPAIGN_ITERATION.CAMPAIGN_ID,
                CAMPAIGN_ITERATION.ITERATION_ID,
                CAMPAIGN_ITERATION.ITERATION_ORDER);

        LOGGER.debug("Reordered campaign content. Time on this stage {} ms", timeDiff());
        updateTime();
    }

    private void deleteIterationEntities() {
        workingTables.delete(ITERATION.ITERATION_ID, AUTOMATED_SUITE.ITERATION_ID);
        workingTables.delete(ITERATION.ITERATION_ID, CAMPAIGN_ITERATION.ITERATION_ID);
        workingTables.delete(ITERATION.ITERATION_ID, ITERATION.ITERATION_ID);
        logDelete(ITERATION);
    }

    private void deleteFailureDetails() {
        workingTables.delete(
                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, FAILURE_DETAIL.ITEM_TEST_PLAN_ID);
        logDelete(FAILURE_DETAIL);
    }

    private void deleteIterationTestPlanItem() {
        workingTables.delete(ITERATION.ITERATION_ID, ITEM_TEST_PLAN_LIST.ITERATION_ID);
        workingTables.delete(
                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID);
        workingTables.delete(
                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);
        logDelete(ITERATION_TEST_PLAN_ITEM);
    }

    private void deleteTestSuites() {
        workingTables.delete(ITERATION.ITERATION_ID, ITERATION_TEST_SUITE.ITERATION_ID);
        workingTables.delete(TEST_SUITE.ID, TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID);
        workingTables.delete(TEST_SUITE.ID, AUTOMATED_SUITE.TEST_SUITE_ID);
        workingTables.delete(TEST_SUITE.ID, TEST_SUITE.ID);
        logDelete(TEST_SUITE);
    }

    private void deleteItemTestPlanExecutions() {
        workingTables.delete(
                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID);
    }

    private void logStartProcess() {
        LOGGER.debug(
                String.format(
                        "Init deletion process of iterations %s. Operation:  %s", iterationIds, operationId));
    }

    private void storeEntitiesToDeleteIntoWorkingTable() {
        storeExecutionsToDeleteIntoWorkingTable();
        addCampaign();
        addIteration();
        addTestSuite();
        addFailureDetails();
        addIterationTestPlanItem();
        addCustomFieldValues();
        addAttachmentList();
        logReferenceEntitiesComplete();
    }

    private void addAttachmentList() {
        workingTables.addEntity(
                ATTACHMENT_LIST.ATTACHMENT_LIST_ID,
                () ->
                        selectExecutionAttachmentLists()
                                .union(makeSelectAttachmentList(TEST_SUITE.ID, TEST_SUITE.ATTACHMENT_LIST_ID))
                                .union(
                                        makeSelectAttachmentList(ITERATION.ITERATION_ID, ITERATION.ATTACHMENT_LIST_ID))
                                // For an unknown reason a dev broke a 12 year and 150+ table convention
                                // and choose to use a string as primary key, in place of a good old
                                // Long/BigInteger...
                                // No way to easily fix that for now but maybe one day we will have time for this...
                                .union(
                                        select(
                                                        AUTOMATED_SUITE.ATTACHMENT_LIST_ID,
                                                        extractFieldTableNameAsParam(ATTACHMENT_LIST.ATTACHMENT_LIST_ID),
                                                        operationIdParam)
                                                .from(AUTOMATED_SUITE)
                                                .where(AUTOMATED_SUITE.ITERATION_ID.in(iterationIds))));
    }

    private void addCampaign() {
        workingTables.addEntity(
                CAMPAIGN.CLN_ID,
                () ->
                        makeSelectClause(CAMPAIGN.CLN_ID)
                                .from(CAMPAIGN)
                                .innerJoin(CAMPAIGN_ITERATION)
                                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                                .innerJoin(ITERATION)
                                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                                .where(getPredicate()));
    }

    private void addIteration() {
        workingTables.addEntity(
                ITERATION.ITERATION_ID,
                () -> makeSelectClause(ITERATION.ITERATION_ID).from(ITERATION).where(getPredicate()));
    }

    private void addCustomFieldValues() {
        workingTables.addEntity(
                CUSTOM_FIELD_VALUE.CFV_ID,
                () ->
                        selectExecutionCustomFieldValues()
                                .union(makeSelectCustomFieldValues(TEST_SUITE.ID, BindableEntity.TEST_SUITE))
                                .union(
                                        makeSelectCustomFieldValues(ITERATION.ITERATION_ID, BindableEntity.ITERATION)));
    }

    private void addIterationTestPlanItem() {
        workingTables.addEntity(
                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                () ->
                        makeSelectClause(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
                                .from(joinToIterationTestPlanItem())
                                .where(getPredicate()));
    }

    private void addFailureDetails() {
        workingTables.addEntity(
                FAILURE_DETAIL.ITEM_TEST_PLAN_ID,
                () ->
                        makeSelectClause(FAILURE_DETAIL.ITEM_TEST_PLAN_ID)
                                .from(joinToFailureDetail())
                                .where(getPredicate()));
    }

    private TableOnConditionStep<Record> joinToFailureDetail() {
        return ITERATION
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(FAILURE_DETAIL)
                .on(FAILURE_DETAIL.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID));
    }

    private void addTestSuite() {
        workingTables.addEntity(
                TEST_SUITE.ID,
                () -> makeSelectClause(TEST_SUITE.ID).from(joinToTestSuite()).where(getPredicate()));
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
