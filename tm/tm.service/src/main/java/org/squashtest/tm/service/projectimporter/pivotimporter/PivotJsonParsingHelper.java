/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.SquashCustomFieldInfo;

public final class PivotJsonParsingHelper {

    private PivotJsonParsingHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> getArrayStringValues(JsonParser jsonParser) throws IOException {
        List<String> values = new ArrayList<>();
        while (isNotTheEndOfParsedArray(jsonParser)) {
            values.add(jsonParser.getValueAsString());
        }

        return values;
    }

    public static boolean isStartingToParseNewObject(JsonParser jsonParser) {
        return JsonToken.START_OBJECT.equals(jsonParser.getCurrentToken());
    }

    public static boolean isNotTheEndOfParsedObject(JsonParser jsonParser) throws IOException {
        return jsonParser.nextToken() != JsonToken.END_OBJECT;
    }

    public static boolean isStartingToParseNewArray(JsonParser jsonParser) {
        return JsonToken.START_ARRAY.equals(jsonParser.getCurrentToken());
    }

    public static boolean isNotTheEndOfParsedArray(JsonParser jsonParser) throws IOException {
        return jsonParser.nextToken() != JsonToken.END_ARRAY;
    }

    public static Map<Long, RawValue> getCustomFieldValues(
            JsonParser jsonParser, Map<String, SquashCustomFieldInfo> customFieldIdsMap)
            throws IOException {
        Map<Long, RawValue> customFieldsValues = new HashMap<>();
        while (isNotTheEndOfParsedArray(jsonParser)) {
            if (isStartingToParseNewObject(jsonParser)) {
                parseCustomFieldValue(jsonParser, customFieldIdsMap, customFieldsValues);
            }
        }
        return customFieldsValues;
    }

    private static void parseCustomFieldValue(
            JsonParser jsonParser,
            Map<String, SquashCustomFieldInfo> customFieldIdsMap,
            Map<Long, RawValue> customFieldsValues)
            throws IOException {
        String id = null;
        String value = null;

        while (isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            if (JsonImportField.ID.equals(fieldName)) {
                id = jsonParser.getText();
            } else if (JsonImportField.VALUE.equals(fieldName)) {
                value = jsonParser.getValueAsString();
            }
        }
        SquashCustomFieldInfo customFieldInfo = customFieldIdsMap.get(id);
        if (InputType.TAG.equals(customFieldInfo.inputType())) {
            List<String> tagValues =
                    Objects.nonNull(value) ? Arrays.stream(value.split("\\|")).toList() : new ArrayList<>();
            customFieldsValues.put(customFieldInfo.id(), new RawValue(tagValues));
        } else {
            customFieldsValues.put(customFieldInfo.id(), new RawValue(value));
        }
    }
}
