/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.execution;

import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState;
import org.squashtest.tm.domain.execution.LatestExploratoryExecutionEvent;

public interface ExploratoryExecutionService {

    void startOrResumeExploratoryExecution(long executionId, String author);

    void pauseExploratoryExecution(long executionId, String author);

    void stopExploratoryExecution(long executionId, String author);

    ExploratoryExecutionRunningState findExploratoryExecutionRunningState(long executionId);

    LatestExploratoryExecutionEvent fetchLatestExploratoryExecutionEvent(Long executionId);

    void checkSessionIsNotPaused(ExploratoryExecutionRunningState currentState);

    void checkSessionIsNotStopped(ExploratoryExecutionRunningState currentState);

    void assignUser(Long executionId, Long userId);

    void updateTaskDivision(Long executionId, String taskDivision);

    /**
     * Updates the status and the last execution info for an execution as well as its test plan item.
     *
     * @param executionId The exploratory execution ID
     */
    void updateExecutionMetadata(long executionId);

    void updateReviewStatus(Long executionId, boolean reviewed);

    boolean isExecutionRunning(Long executionId);

    void pauseRunningExploratoryExecutions(Long sprintId);
}
