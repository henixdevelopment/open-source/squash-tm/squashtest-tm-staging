/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.customreport;

import static org.squashtest.tm.service.security.Authorizations.READ_CUSTOM_REPORT_LIBRARY_NODE_OR_ROLE_ADMIN;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.customreport.CustomReportDisplayService;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.internal.display.dto.customreports.ChartDefinitionDto;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportFolderDto;
import org.squashtest.tm.service.internal.repository.display.CustomReportDisplayDao;

@Service
@Transactional(readOnly = true)
public class CustomReportDisplayServiceImpl implements CustomReportDisplayService {

    private final CustomReportDisplayDao customReportDisplayDao;

    public CustomReportDisplayServiceImpl(CustomReportDisplayDao customReportDisplayDao) {
        this.customReportDisplayDao = customReportDisplayDao;
    }

    @Override
    public ChartDefinitionDto getChartDefinitionDto(Long customReportLibraryNodeId) {
        return customReportDisplayDao.findChartDefinitionByNodeId(customReportLibraryNodeId);
    }

    @Override
    @PreAuthorize(READ_CUSTOM_REPORT_LIBRARY_NODE_OR_ROLE_ADMIN)
    public LibraryDto getCustomReportLibraryView(long treeNodeId) {
        return customReportDisplayDao.getCustomReportLibraryDtoByNodeId(treeNodeId);
    }

    @Override
    public CustomReportFolderDto getCustomReportFolderView(long customReportFolderId) {
        return this.customReportDisplayDao.getCustomReportFolderDtoById(customReportFolderId);
    }
}
