/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.scmserver;

import java.util.Collection;
import java.util.List;
import org.squashtest.tm.domain.scm.ScmServer;

public interface ScmServerManagerService {

    /**
     * Create a new ScmServer with its attributes.
     *
     * @param newScmServer The ScmServer with its attributes to create.
     * @return The ScmServer newly created
     */
    ScmServer createNewScmServer(ScmServer newScmServer);

    /**
     * Update the name of the ScmServer with the given Id with the given name.
     *
     * @param scmServerId The Id of the ScmServer which name is to update.
     * @param newName The new name to give the ScmServer.
     * @return The new name of the ScmServer.
     */
    String updateName(long scmServerId, String newName);

    /**
     * Update the Url of the ScmServer with the given Id with the given Url.
     *
     * @param scmServerId The Id of the ScmServer which Url is to update.
     * @param url The new Url to give to the scmServer
     * @return The new Url of the ScmServer.
     */
    String updateUrl(long scmServerId, String url);

    /**
     * Update the committer mail of the ScmServer with the given Id with the given committerMail.
     *
     * @param scmServerId The Id of the ScmServer which committer mail is to update.
     * @param committerMail The new committer mail to give to the ScmServer
     * @return The new committer mail of the ScmServer.
     */
    String updateCommitterMail(long scmServerId, String committerMail);

    /**
     * Get the boolean credentialsNotShared of the ScmServer with the given Id.
     *
     * @param scmServerId The Id of the ScmServer to get credentialsNotShared.
     * @return The credentialsNotShared of the ScmServer.
     */
    boolean getCredentialsNotShared(long scmServerId);

    /**
     * Update the boolean credentialsNotShared of the ScmServer with the given Id with the given
     * credentialsNotShared.
     *
     * @param scmServerId The Id of the ScmServer which credentialsNotShared is to update.
     * @param credentialsNotShared The new credentialsNotShared to give to the ScmServer
     * @return The new credentialsNotShared of the ScmServer.
     */
    boolean updateCredentialsNotShared(long scmServerId, boolean credentialsNotShared);

    /**
     * Delete the ScmServers with the given Ids. Also deletes the ScmRepositories contained in these
     * ScmServers.
     *
     * @param scmServerIds The Ids of the ScmServers to delete.
     */
    void deleteScmServers(Collection<Long> scmServerIds);

    /**
     * @param scmServerIds scm server IDs
     * @return true if one of these SCM Server possesses bindings to test cases or projects through
     *     one of its repositories
     */
    boolean areServerRepositoriesBoundToProjectOrTestCases(List<Long> scmServerIds);

    void updateDescription(long scmServerId, String description);
}
