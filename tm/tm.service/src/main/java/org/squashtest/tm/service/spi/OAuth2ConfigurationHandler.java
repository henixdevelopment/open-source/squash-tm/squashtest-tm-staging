/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.spi;

import org.squashtest.csp.core.bugtracker.spi.DefaultOAuth2FormValues;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.OAuth2Credentials;

public interface OAuth2ConfigurationHandler {

    String getOauth2AuthenticationUrl(BugTracker bugTracker, GenericServerOAuth2Conf conf);

    void appendScope(String bugTrackerUrl, StringBuilder sb, GenericServerOAuth2Conf conf);

    String getOauth2RequestTokenUrl(Long serverId, String code, GenericServerOAuth2Conf conf);

    String getRefreshTokenUrl(GenericServerOAuth2Conf conf, OAuth2Credentials creds);

    DefaultOAuth2FormValues getDefaultValueForOauth2Form(String jiraUrl);
}
