/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.testcase.exploratory.session;

import static org.squashtest.tm.service.security.Authorizations.READ_TC_OR_ROLE_ADMIN;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.display.testcase.exploratory.session.TestCaseExploratorySessionsDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.testcase.TestCaseSessionsGrid;
import org.squashtest.tm.service.internal.repository.display.ExploratorySessionOverviewDisplayDao;

@Service
public class TestCaseExploratorySessionsDisplayServiceImpl
        implements TestCaseExploratorySessionsDisplayService {

    private final DSLContext dslContext;
    private final ExploratorySessionOverviewDisplayDao exploratorySessionOverviewDisplayDao;

    public TestCaseExploratorySessionsDisplayServiceImpl(
            DSLContext dslContext,
            ExploratorySessionOverviewDisplayDao exploratorySessionOverviewDisplayDao) {
        this.dslContext = dslContext;
        this.exploratorySessionOverviewDisplayDao = exploratorySessionOverviewDisplayDao;
    }

    @PreAuthorize(READ_TC_OR_ROLE_ADMIN)
    @Override
    public GridResponse findByTestCaseId(Long testCaseId, GridRequest gridRequest) {
        return new TestCaseSessionsGrid(testCaseId).getRows(gridRequest, dslContext);
    }

    @PreAuthorize(READ_TC_OR_ROLE_ADMIN)
    @Override
    public Integer countExploratorySessionOverviewByTestCaseId(Long testCaseId) {
        return exploratorySessionOverviewDisplayDao.countSessionOverviewsByTestCaseId(testCaseId);
    }
}
