/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.decode;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.groupConcatDistinct;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration.CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_TEST_TECHNOLOGY;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PARAMETER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ATTACHMENTS_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.COVERAGES_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ISSUE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.SelectConditionStep;
import org.jooq.SelectSelectStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.TestCaseResearchDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class TestCaseResearchDisplayDaoImpl implements TestCaseResearchDisplayDao {

    private static final String ID_ALIAS = "ID";
    private static final String ITEMS_ALIAS = "ITERATIONS";
    private static final String MILESTONES_ALIAS = "MILESTONES";
    private static final String NATURE_ALIAS = "NATURE";
    private static final String REQ_MILESTONE_LOCKED = "REQ_MILESTONE_LOCKED";
    private static final String STATUS_ALIAS = "STATUS";
    private static final String STEPS_ALIAS = "STEPS";
    private static final String TC_MILESTONE_LOCKED = "TC_MILESTONE_LOCKED";
    private static final String TYPE_ALIAS = "TYPE";
    private static final String ITERATION_LIST = "ITERATION_LIST";
    private static final String TEST_SUITE_LIST = "TEST_SUITE_LIST";
    private static final String CAMPAIGN_LIST = "CAMPAIGN_LIST";
    private static final String DATASETS_ALIAS = "DATASETS";
    private static final String PARAMETER_ALIAS = "PARAM_COUNT";
    private static final String CALL_STEP_COUNT_ALIAS = "CALL_STEP_COUNT";
    private static final String TEST_CASE_ENTITY_TYPE = "TEST_CASE";
    private static final String TEST_CASE_SEARCH = "test-case-search";

    private final DSLContext jooq;

    public TestCaseResearchDisplayDaoImpl(DSLContext jooq) {
        this.jooq = jooq;
    }

    @Override
    public GridResponse getRows(List<Long> testCaseIds, GridRequest gridRequest) {

        GridResponse gridResponse = new GridResponse();
        List<SelectConditionStep<?>> countQueries = getCountFields();
        SelectSelectStep<Record> selectBaseColumns = jooq.select(getFields());
        SelectSelectStep<Record> selectAllColumns;
        List<String> simplifiedColumnDisplayGridIds = gridRequest.getSimplifiedColumnDisplayGridIds();

        if (!simplifiedColumnDisplayGridIds.contains(TEST_CASE_SEARCH)) {
            selectAllColumns =
                    selectBaseColumns.select(
                            countQueries.get(0).asField(MILESTONES_ALIAS),
                            countQueries.get(1).asField(COVERAGES_ALIAS),
                            countQueries.get(2).asField(STEPS_ALIAS),
                            countQueries.get(3).asField(ITEMS_ALIAS),
                            countQueries.get(4).asField(ATTACHMENTS_ALIAS),
                            countQueries.get(5).asField(DATASETS_ALIAS),
                            countQueries.get(6).asField(PARAMETER_ALIAS),
                            countQueries.get(7).asField(CALL_STEP_COUNT_ALIAS),
                            countQueries.get(8).asField(EXECUTION_COUNT),
                            countQueries.get(9).asField(ISSUE_COUNT));
        } else {
            selectAllColumns =
                    selectBaseColumns.select(
                            DSL.value((Integer) null).as(MILESTONES_ALIAS),
                            DSL.value((Integer) null).as(COVERAGES_ALIAS),
                            DSL.value((Integer) null).as(STEPS_ALIAS),
                            DSL.value((Integer) null).as(ITEMS_ALIAS),
                            DSL.value((Integer) null).as(ATTACHMENTS_ALIAS),
                            DSL.value((Boolean) null).as(DATASETS_ALIAS),
                            DSL.value((Integer) null).as(PARAMETER_ALIAS),
                            DSL.value((Integer) null).as(CALL_STEP_COUNT_ALIAS),
                            DSL.value((Integer) null).as(EXECUTION_COUNT),
                            DSL.value((Boolean) null).as(ISSUE_COUNT));
        }
        getCufFields(selectAllColumns, simplifiedColumnDisplayGridIds);
        selectAllColumns
                .from(getTable())
                .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.in(testCaseIds))
                .groupBy(
                        TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ID_ALIAS),
                        TEST_CASE.TCLN_ID,
                        PROJECT.PROJECT_ID,
                        AUTOMATION_REQUEST.AUTOMATION_PRIORITY,
                        AUTOMATION_REQUEST.TRANSMITTED_ON,
                        AUTOMATION_REQUEST.REQUEST_STATUS,
                        SCRIPTED_TEST_CASE.TCLN_ID,
                        KEYWORD_TEST_CASE.TCLN_ID,
                        EXPLORATORY_TEST_CASE.TCLN_ID,
                        SCRIPTED_TEST_CASE.SCRIPT,
                        AUTOMATED_TEST_TECHNOLOGY.NAME)
                .stream()
                .forEach(
                        record -> {
                            DataRow dataRow = new DataRow();
                            dataRow.setId(record.get(TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ID_ALIAS)).toString());
                            dataRow.setProjectId(record.get(TEST_CASE_LIBRARY_NODE.PROJECT_ID));
                            Map<String, Object> rawData = record.intoMap();
                            Map<String, Object> data = new HashMap<>();

                            // Using 'Collectors.toMap' won't work for entries with null values
                            for (Map.Entry<String, Object> entry : rawData.entrySet()) {
                                data.put(convertField(entry.getKey()), entry.getValue());
                            }
                            dataRow.setData(data);
                            gridResponse.addDataRow(dataRow);
                        });
        return gridResponse;
    }

    private List<Field<?>> getFields() {
        return Arrays.asList(
                TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ID_ALIAS),
                TEST_CASE_LIBRARY_NODE.NAME,
                TEST_CASE_LIBRARY_NODE.PROJECT_ID,
                TEST_CASE_LIBRARY_NODE.CREATED_BY,
                TEST_CASE_LIBRARY_NODE.CREATED_ON,
                TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY,
                TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_ON,
                decode(TEST_CASE_LIBRARY_NODE.DESCRIPTION, "", false, true)
                        .as(TEST_CASE_LIBRARY_NODE.DESCRIPTION),
                TEST_CASE.REFERENCE,
                TEST_CASE.IMPORTANCE,
                TEST_CASE.IMPORTANCE_AUTO,
                TEST_CASE.TC_NATURE.as(NATURE_ALIAS),
                TEST_CASE.TC_TYPE.as(TYPE_ALIAS),
                TEST_CASE.TC_STATUS.as(STATUS_ALIAS),
                when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.GHERKIN.name())
                        .when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.KEYWORD.name())
                        .when(EXPLORATORY_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.EXPLORATORY.name())
                        .otherwise(TestCaseKind.STANDARD.name())
                        .as(RequestAliasesConstants.TC_KIND),
                TEST_CASE.AUTOMATABLE,
                TEST_CASE.DRAFTED_BY_AI,
                AUTOMATION_REQUEST.AUTOMATION_PRIORITY,
                AUTOMATION_REQUEST.TRANSMITTED_ON,
                AUTOMATION_REQUEST.REQUEST_STATUS,
                decode(TEST_CASE.TA_TEST, null, false, true).as(RequestAliasesConstants.HAS_AUTO_SCRIPT),
                AUTOMATED_TEST_TECHNOLOGY.NAME.as(RequestAliasesConstants.AUTOMATED_TEST_TECHNOLOGY),
                decode(TEST_CASE.SCM_REPOSITORY_ID, null, false, true)
                        .as(RequestAliasesConstants.HAS_BOUND_SCM_REPOSITORY),
                decode(TEST_CASE.AUTOMATED_TEST_REFERENCE, null, false, true)
                        .as(RequestAliasesConstants.HAS_BOUND_AUTOMATED_TEST_REFERENCE),
                PROJECT.NAME.as(PROJECT_NAME),
                groupConcat(MILESTONE.LABEL)
                        .orderBy(MILESTONE.LABEL)
                        .separator((", "))
                        .as(RequestAliasesConstants.MILESTONE_LABELS),
                groupConcat(MILESTONE.STATUS)
                        .orderBy(MILESTONE.LABEL)
                        .separator((", "))
                        .as(RequestAliasesConstants.MILESTONE_STATUS),
                groupConcat(MILESTONE.END_DATE)
                        .orderBy(MILESTONE.LABEL)
                        .separator((", "))
                        .as(RequestAliasesConstants.MILESTONE_END_DATE),
                DSL.select(countDistinct(MILESTONE.MILESTONE_ID))
                        .from(REQUIREMENT_VERSION_COVERAGE)
                        .join(MILESTONE_REQ_VERSION)
                        .on(
                                REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(
                                        MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                        .join(MILESTONE)
                        .on(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                        .where(
                                REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(
                                        TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .and(MILESTONE.STATUS.eq("LOCKED"))
                        .asField(REQ_MILESTONE_LOCKED),
                DSL.select(groupConcatDistinct(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .join(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .asField(ITERATION_LIST),
                DSL.select(groupConcatDistinct(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID))
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .join(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .asField(TEST_SUITE_LIST),
                DSL.select(groupConcatDistinct(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID))
                        .from(CAMPAIGN_TEST_PLAN_ITEM)
                        .where(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .asField(CAMPAIGN_LIST),
                DSL.select(countDistinct(MILESTONE.MILESTONE_ID))
                        .from(MILESTONE_TEST_CASE)
                        .join(MILESTONE)
                        .on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                        .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .and(MILESTONE.STATUS.eq("LOCKED"))
                        .asField(TC_MILESTONE_LOCKED));
    }

    private List<SelectConditionStep<?>> getCountFields() {
        List<SelectConditionStep<?>> countQueries = new ArrayList<>();

        countQueries.add(
                DSL.selectCount()
                        .from(MILESTONE_TEST_CASE)
                        .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.selectCount()
                        .from(REQUIREMENT_VERSION_COVERAGE)
                        .where(
                                REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(
                                        TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.selectCount()
                        .from(TEST_CASE_STEPS)
                        .where(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.select(countDistinct(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .join(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.selectCount()
                        .from(ATTACHMENT)
                        .where(ATTACHMENT.ATTACHMENT_LIST_ID.eq(TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID)));
        countQueries.add(
                DSL.selectCount()
                        .from(DATASET)
                        .where(DATASET.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.selectCount()
                        .from(PARAMETER)
                        .where(PARAMETER.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.selectCount()
                        .from(TEST_CASE_STEPS)
                        .join(CALL_TEST_STEP)
                        .on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
                        .where(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.selectCount()
                        .from(EXECUTION)
                        .where(EXECUTION.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));
        countQueries.add(
                DSL.select(countDistinct(ISSUE.REMOTE_ISSUE_ID))
                        .from(ISSUE)
                        .join(EXECUTION_ISSUES_CLOSURE)
                        .on(EXECUTION_ISSUES_CLOSURE.ISSUE_ID.eq(ISSUE.ISSUE_ID))
                        .join(EXECUTION)
                        .on(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .where(EXECUTION.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID)));

        return countQueries;
    }

    private Table<?> getTable() {
        return TEST_CASE_LIBRARY_NODE
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftOuterJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .leftOuterJoin(SCRIPTED_TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                .leftOuterJoin(KEYWORD_TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                .leftOuterJoin(AUTOMATED_TEST_TECHNOLOGY)
                .on(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.eq(AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID))
                .leftOuterJoin(EXPLORATORY_TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                .leftJoin(MILESTONE_TEST_CASE)
                .on(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(MILESTONE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID));
    }

    private String convertField(String fieldName) {
        if (fieldName.startsWith(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS)) {
            return fieldName;
        } else {
            Converter<String, String> converter =
                    CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
            return converter.convert(fieldName);
        }
    }

    /**
     * If simplifiedColumnDisplayGridIds does not contain "test-case-search", adds to the
     * selectBaseColumns a number of columns equal to the number of custom fields bound to test cases.
     * These extra columns extract for each test case the value of the cuf. If
     * simplifiedColumnDisplayGridIds contains "test-case-search", adds the same columns but these
     * columns will remain empty (for performance issues).
     *
     * @param selectBaseColumns
     * @param simplifiedColumnDisplayGridIds
     */
    private void getCufFields(
            SelectSelectStep<Record> selectBaseColumns, List<String> simplifiedColumnDisplayGridIds) {
        List<Long> cufIds =
                jooq.selectDistinct(CUSTOM_FIELD.CF_ID)
                        .from(CUSTOM_FIELD)
                        .innerJoin(CUSTOM_FIELD_BINDING)
                        .on(CUSTOM_FIELD.CF_ID.eq(CUSTOM_FIELD_BINDING.CF_ID))
                        .where(CUSTOM_FIELD_BINDING.BOUND_ENTITY.eq(TEST_CASE_ENTITY_TYPE))
                        .fetchInto(Long.class);
        if (!simplifiedColumnDisplayGridIds.contains(TEST_CASE_SEARCH)) {
            for (Long cufId : cufIds) {
                selectBaseColumns.select(
                        DSL.select(
                                        when(
                                                        CUSTOM_FIELD_VALUE.FIELD_TYPE.eq(String.valueOf(InputType.TAG)),
                                                        groupConcat(CUSTOM_FIELD_VALUE_OPTION.LABEL).separator(", "))
                                                .otherwise(CUSTOM_FIELD_VALUE.VALUE))
                                .from(CUSTOM_FIELD_VALUE)
                                .leftOuterJoin(CUSTOM_FIELD_VALUE_OPTION)
                                .on(CUSTOM_FIELD_VALUE.CFV_ID.eq(CUSTOM_FIELD_VALUE_OPTION.CFV_ID))
                                .where(CUSTOM_FIELD_VALUE.CF_ID.eq(cufId))
                                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                                .and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(TEST_CASE_ENTITY_TYPE))
                                .groupBy(CUSTOM_FIELD_VALUE.FIELD_TYPE, CUSTOM_FIELD_VALUE.VALUE)
                                .asField(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS + cufId));
            }
        } else {
            for (Long cufId : cufIds) {
                selectBaseColumns.select(DSL.value((String) null).as("CUF_" + cufId));
            }
        }
    }
}
