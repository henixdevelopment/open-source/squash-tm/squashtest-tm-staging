/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.column;

import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.COVERAGE_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.DATASETS_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.DATASET_PARAM_VALUES_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.LINKED_LOW_LEVEL_REQS_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.PARAMETERS_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.REQUIREMENT_LINKS_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.REQUIREMENT_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.STEPS_SHEET;
import static org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet.TEST_CASES_SHEET;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Value;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.exception.SheetCorruptedException;
import org.squashtest.tm.service.batchimport.excel.TemplateMismatchException;
import org.squashtest.tm.service.importer.LogEntry;
import org.squashtest.tm.service.importer.Target;
import org.squashtest.tm.service.internal.batchimport.LogTrain;
import org.squashtest.tm.service.internal.batchimport.Messages;
import org.squashtest.tm.service.internal.batchimport.column.requirement.LinkedLowLevelRequirementInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.requirement.LinkedLowLevelRequirementsSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementLinkInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementLinksSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageTarget;
import org.squashtest.tm.service.internal.batchimport.column.testcase.DatasetInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.testcase.DatasetParamValueInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.testcase.DatasetParamValuesSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.DatasetSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.ParameterInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.testcase.ParameterSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.StepInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.testcase.StepSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseInstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetParamValueInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.Instruction;
import org.squashtest.tm.service.internal.batchimport.instruction.LinkedLowLevelRequirementInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementLinkInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.StepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.LinkedLowLevelRequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementLinkTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;

/**
 * Parses an excel import workbook and creates instructions.
 *
 * <p>Usage :
 *
 * <pre>
 * {
 * 	&#064;code
 * 	ExcelWorkbookParser parser = ExcelWorkbookParser.createParser(xlsxFile);
 * 	parser.parse().releaseResources();
 * 	List&lt;Instructions&gt; instructions = parser.getInstructions();
 * }
 * </pre>
 *
 * @author Gregory Fouquet
 */
public class ExcelWorkbookParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelWorkbookParser.class);

    private static final String RAWTYPES = "rawtypes";
    private static final String UNCHECKED = "unchecked";

    @Inject
    @Value("${uploadfilter.upload.import.maxLinesPerSheetForExcelImport:100}")
    private int maxLines;

    private Workbook workbook;
    private final WorkbookMetaData wmd;

    private final Map<TemplateWorksheet, List<Instruction<?>>> instructionsByWorksheet =
            new EnumMap<>(TemplateWorksheet.class);
    private final Map<TemplateWorksheet, Factory<?>> instructionBuilderFactoryByWorksheet =
            new EnumMap<>(TemplateWorksheet.class);

    /**
     * Should be used by ExcelWorkbookParserBuilder only.
     *
     * @param workbook
     * @param wmd
     */
    ExcelWorkbookParser(@NotNull Workbook workbook, @NotNull WorkbookMetaData wmd) {
        super();
        this.workbook = workbook;
        this.wmd = wmd;

        instructionsByWorksheet.put(TEST_CASES_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(STEPS_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(PARAMETERS_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(DATASETS_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(DATASET_PARAM_VALUES_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(REQUIREMENT_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(COVERAGE_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(REQUIREMENT_LINKS_SHEET, new ArrayList<>());
        instructionsByWorksheet.put(LINKED_LOW_LEVEL_REQS_SHEET, new ArrayList<>());

        instructionBuilderFactoryByWorksheet.put(
                REQUIREMENT_SHEET,
                new Factory<RequirementSheetColumn>() {

                    @Override
                    public AbstractInstructionBuilder<?, ?> create(WorksheetDef<RequirementSheetColumn> wd) {
                        return new RequirementInstructionBuilder(wd);
                    }
                });

        instructionBuilderFactoryByWorksheet.put(
                TEST_CASES_SHEET,
                new Factory<TestCaseSheetColumn>() {
                    @Override
                    public AbstractInstructionBuilder<?, ?> create(WorksheetDef<TestCaseSheetColumn> wd) {
                        return new TestCaseInstructionBuilder(wd);
                    }
                });
        instructionBuilderFactoryByWorksheet.put(
                STEPS_SHEET,
                new Factory<StepSheetColumn>() {
                    @Override
                    public AbstractInstructionBuilder<?, ?> create(WorksheetDef<StepSheetColumn> wd) {
                        return new StepInstructionBuilder(wd);
                    }
                });
        instructionBuilderFactoryByWorksheet.put(
                PARAMETERS_SHEET,
                new Factory<ParameterSheetColumn>() {
                    @Override
                    public AbstractInstructionBuilder<?, ?> create(WorksheetDef<ParameterSheetColumn> wd) {
                        return new ParameterInstructionBuilder(wd);
                    }
                });
        instructionBuilderFactoryByWorksheet.put(
                DATASETS_SHEET,
                new Factory<DatasetSheetColumn>() {
                    @Override
                    public AbstractInstructionBuilder<?, ?> create(WorksheetDef<DatasetSheetColumn> wd) {
                        return new DatasetInstructionBuilder(wd);
                    }
                });
        instructionBuilderFactoryByWorksheet.put(
                DATASET_PARAM_VALUES_SHEET,
                new Factory<DatasetParamValuesSheetColumn>() {
                    @Override
                    public AbstractInstructionBuilder<?, ?> create(
                            WorksheetDef<DatasetParamValuesSheetColumn> wd) {
                        return new DatasetParamValueInstructionBuilder(wd);
                    }
                });

        instructionBuilderFactoryByWorksheet.put(
                COVERAGE_SHEET,
                new Factory<CoverageSheetColumn>() {

                    @Override
                    public AbstractInstructionBuilder<?, ?> create(WorksheetDef<CoverageSheetColumn> wd) {
                        return new CoverageInstructionBuilder(wd);
                    }
                });

        instructionBuilderFactoryByWorksheet.put(
                REQUIREMENT_LINKS_SHEET,
                new Factory<RequirementLinksSheetColumn>() {
                    @Override
                    public AbstractInstructionBuilder<?, ?> create(
                            WorksheetDef<RequirementLinksSheetColumn> wd) {
                        return new RequirementLinkInstructionBuilder(wd);
                    }
                });

        instructionBuilderFactoryByWorksheet.put(
                LINKED_LOW_LEVEL_REQS_SHEET,
                new Factory<LinkedLowLevelRequirementsSheetColumn>() {
                    @Override
                    public AbstractInstructionBuilder<?, ?> create(
                            WorksheetDef<LinkedLowLevelRequirementsSheetColumn> wd) {
                        return new LinkedLowLevelRequirementInstructionBuilder(wd);
                    }
                });
    }

    public LogTrain logUnknownHeaders() {

        LogTrain logs = new LogTrain();

        for (WorksheetDef<?> wd : wmd.getWorksheetDefs()) {
            Collection<UnknownColumnDef> unknowns = wd.getUnknownColumns();
            for (UnknownColumnDef unknown : unknowns) {
                LogEntry.Builder builder =
                        LogEntry.failure()
                                .atLine(0)
                                .forTarget(createDummyTarget(wd))
                                .withMessage(Messages.ERROR_UNKNOWN_COLUMN_HEADER, unknown.getHeader())
                                .withImpact(Messages.IMPACT_COLUMN_IGNORED, (Object[]) null);
                logs.addEntry(builder.build());
            }
        }

        return logs;
    }

    // quick and dirty. LogEntries need a target because ImportLog sorts the entries by the EntityType
    // of the target of the log entry.
    private Target createDummyTarget(WorksheetDef<?> def) {
        return switch (def.getWorksheetType()) {
            case TEST_CASES_SHEET -> new TestCaseTarget();
            case STEPS_SHEET -> new TestStepTarget();
            case DATASET_PARAM_VALUES_SHEET, DATASETS_SHEET -> new DatasetTarget();
            case PARAMETERS_SHEET -> new ParameterTarget();
            case REQUIREMENT_SHEET -> new RequirementVersionTarget();
            case COVERAGE_SHEET -> new CoverageTarget();
            case REQUIREMENT_LINKS_SHEET -> new RequirementLinkTarget();
            case LINKED_LOW_LEVEL_REQS_SHEET -> new LinkedLowLevelRequirementTarget();
            default ->
                    throw new IllegalArgumentException(
                            "sheet '"
                                    + def.getSheetName()
                                    + "' is unknown and contains errors in its column headers");
        };
    }

    /**
     * Parses the file and creates instructions accordingly.
     *
     * @return this
     */
    public ExcelWorkbookParser parse() {
        LOGGER.info("Parsing test-cases excel workbook {}", workbook);

        if (workbook == null) {
            throw new IllegalStateException(
                    "No workbook available for parsing. Maybe you released this parser's resources by mistake.");
        }

        for (WorksheetDef<?> wd : wmd.getWorksheetDefs()) {
            processWorksheet(wd);
        }

        LOGGER.debug("Done parsing test-cases workbook");

        return this;
    }

    @SuppressWarnings({RAWTYPES, UNCHECKED})
    private void processWorksheet(WorksheetDef<?> worksheetDef) {
        LOGGER.debug("Processing worksheet {}", worksheetDef.getWorksheetType());

        Sheet sheet = workbook.getSheet(worksheetDef.getSheetName());

        AbstractInstructionBuilder<?, ?> instructionBuilder =
                instructionBuilderFactoryByWorksheet
                        .get(worksheetDef.getWorksheetType())
                        .create((WorksheetDef) worksheetDef); // useless (WorksheetDef) cast
        // required for compiler not to whine

        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            LOGGER.trace("Creating instruction for row {}", i);
            Row row = sheet.getRow(i);
            if (!isEmpty(row)) {
                Instruction instruction = instructionBuilder.build(row);
                instructionsByWorksheet.get(worksheetDef.getWorksheetType()).add(instruction);
            }
        }
    }

    /**
     * Releases resources held by this parser. The result of parsing is still available but the {@link
     * #parse()} method should no longer be called.
     *
     * @return this
     */
    public ExcelWorkbookParser releaseResources() {
        // as per POI doc : workbook resources are released upon GC
        workbook = null;
        return this;
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<TestCaseInstruction> getTestCaseInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        TEST_CASES_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<StepInstruction> getTestStepInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        STEPS_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<ParameterInstruction> getParameterInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        PARAMETERS_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<DatasetInstruction> getDatasetInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        DATASETS_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<DatasetParamValueInstruction> getDatasetParamValuesInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        DATASET_PARAM_VALUES_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<RequirementVersionInstruction> getRequirementVersionInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        REQUIREMENT_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<CoverageInstruction> getCoverageInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        COVERAGE_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<RequirementLinkInstruction> getRequirementLinkInstructions() {
        return (List)
                instructionsByWorksheet.get(
                        REQUIREMENT_LINKS_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    @SuppressWarnings({UNCHECKED, RAWTYPES})
    public List<LinkedLowLevelRequirementInstruction> getLinkedLowLevelRequirementInstruction() {
        return (List)
                instructionsByWorksheet.get(
                        LINKED_LOW_LEVEL_REQS_SHEET); // useless (List) cast required for compiler not to
        // whine
    }

    public boolean isEmpty(Row row) {
        boolean isEmpty = true;

        if (row != null) {
            Iterator<Cell> iterator = row.cellIterator();

            while (iterator.hasNext()) {
                Cell c = iterator.next();
                if (!StringUtils.isBlank(c.toString())) {
                    isEmpty = false;
                    break;
                }
            }
        }

        return isEmpty;
    }

    /**
     * Can create an {@link AbstractInstructionBuilder} for a given {@link WorksheetDef<C>}
     *
     * @param <C> a TemplateColumn
     */
    private static interface Factory<C extends Enum<C> & TemplateColumn> {
        AbstractInstructionBuilder<?, ?> create(WorksheetDef<C> wd);
    }

    /**
     * Factory method which should be used to create a parser.
     *
     * @param xls
     * @return
     * @throws SheetCorruptedException when the excel file is unreadable
     * @throws TemplateMismatchException when the workbook does not match the template in an
     *     unrecoverable way.
     */
    public static final ExcelWorkbookParser createParser(File xls)
            throws SheetCorruptedException, TemplateMismatchException {
        return new ExcelWorkbookParserBuilder(xls).build();
    }
}
