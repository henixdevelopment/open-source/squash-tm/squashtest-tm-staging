/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintDto;

public interface SprintDisplayDao {

    SprintDto getSprintDtoById(long sprintId);

    SprintStatus getSprintStatusByTestPlanItemId(long testPlanItemId);

    SprintStatus getSprintStatusByExecutionId(long executionId);

    SprintStatus getSprintStatusByExecutionStepId(Long executionStepId);

    Map<Long, List<Long>> findExecutionIdsBySprintIds(List<Long> sprintIds);

    /**
     * Given a list of campaign library node ids, returns the corresponding sprint named references if
     * they are sprints. Child sprint nodes are not returned (e.g. if a CLN ID corresponds to a
     * SprintGroup, the child sprints are not returned unless their ID is in the list).
     *
     * @param clnIds - list of campaign library node ids
     * @return list of named references corresponding to sprints
     */
    List<NamedReference> findNamedReferences(List<Long> clnIds);

    int countTestPlanItems(long sprintId, String userToRestrictTo);

    SprintStatus getSprintStatusBySprintReqVersionId(long sprintReqVersionId);

    SprintStatus getSprintStatusBySessionOverviewId(long overviewId);
}
