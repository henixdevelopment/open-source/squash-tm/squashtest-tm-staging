/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class CustomFieldXrayDto {
    private Long id;
    private Long itemId;
    private String key;
    private String name;
    private String value;
    private Integer stepIndex;
    private String stepAction;
    private String stepData;
    private String stepExpectedResult;
    private String stepCalledTestKey;
    private String stepCalledTestParameters;
    private Integer datasetRow;
    private String datasetName;
    private String datasetValue;
    private String pivotId;
    private String issueLinkName;
    private String issueLinkKey;
    private String issueLinkRelationship;

    public CustomFieldXrayDto() {
        // Needed for Jooq Mapping
    }

    public CustomFieldXrayDto(
            CustomFieldXrayDto customFieldDto, AtomicLong itemId, AtomicLong cufId) {
        this.key = customFieldDto.key;
        this.name = customFieldDto.name;
        this.itemId = itemId.get();
        this.id = cufId.incrementAndGet();
    }

    // Called Test Parameter
    public void setToStringStepCalledTestParameters(Map<String, String> stepCalledTestParameters) {
        if (Objects.nonNull(stepCalledTestParameters) && !stepCalledTestParameters.isEmpty()) {
            this.stepCalledTestParameters = String.valueOf(stepCalledTestParameters);
        }
    }

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public String getStepAction() {
        return stepAction;
    }

    public void setStepAction(String stepAction) {
        this.stepAction = stepAction;
    }

    public String getStepData() {
        return stepData;
    }

    public void setStepData(String stepData) {
        this.stepData = stepData;
    }

    public String getStepExpectedResult() {
        return stepExpectedResult;
    }

    public void setStepExpectedResult(String stepExpectedResult) {
        this.stepExpectedResult = stepExpectedResult;
    }

    public String getStepCalledTestKey() {
        return stepCalledTestKey;
    }

    public void setStepCalledTestKey(String stepCalledTestKey) {
        this.stepCalledTestKey = stepCalledTestKey;
    }

    public String getStepCalledTestParameters() {
        return stepCalledTestParameters;
    }

    public void setStepCalledTestParameters(String stepCalledTestParameters) {
        this.stepCalledTestParameters = stepCalledTestParameters;
    }

    public Integer getDatasetRow() {
        return datasetRow;
    }

    public void setDatasetRow(Integer datasetRow) {
        this.datasetRow = datasetRow;
    }

    public String getDatasetName() {
        return datasetName;
    }

    public void setDatasetName(String datasetName) {
        this.datasetName = datasetName;
    }

    public String getDatasetValue() {
        return datasetValue;
    }

    public void setDatasetValue(String datasetValue) {
        this.datasetValue = datasetValue;
    }

    public String getPivotId() {
        return pivotId;
    }

    public void setPivotId(String pivotId) {
        this.pivotId = pivotId;
    }

    public String getIssueLinkName() {
        return issueLinkName;
    }

    public void setIssueLinkName(String issueLinkName) {
        this.issueLinkName = issueLinkName;
    }

    public String getIssueLinkKey() {
        return issueLinkKey;
    }

    public void setIssueLinkKey(String issueLinkKey) {
        this.issueLinkKey = issueLinkKey;
    }

    public String getIssueLinkRelationship() {
        return issueLinkRelationship;
    }

    public void setIssueLinkRelationship(String issueLinkRelationship) {
        this.issueLinkRelationship = issueLinkRelationship;
    }
}
