/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter.parsers;

import com.fasterxml.jackson.core.JsonParser;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.library.NewFolderDto;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.service.internal.dto.projectimporter.FolderToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.projectimporter.pivotimporter.PivotFormatLoggerHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.FolderParser;

@Service("FolderParser")
public class FolderParserImpl implements FolderParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(FolderParserImpl.class);

    @Override
    public FolderToImport parseFolder(
            JsonParser jsonParser,
            JsonImportFile jsonFile,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        FolderToImport folderToImport = new FolderToImport();
        NewFolderDto newFolderDto = new NewFolderDto();

        try {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> folderToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.NAME -> newFolderDto.setName(jsonParser.getText());
                    case JsonImportField.DESCRIPTION -> newFolderDto.setDescription(jsonParser.getText());
                    case JsonImportField.CUSTOM_FIELDS ->
                            newFolderDto.setCustomFields(
                                    PivotJsonParsingHelper.getCustomFieldValues(
                                            jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                    case JsonImportField.PARENT_TYPE ->
                            folderToImport.setParentType(EntityType.valueOf(jsonParser.getText()));
                    case JsonImportField.PARENT_ID -> folderToImport.setParentId(jsonParser.getText());
                    case JsonImportField.ATTACHMENTS ->
                            folderToImport.setAttachments(AttachmentParserHelper.parseAttachments(jsonParser));
                    default -> {
                        // continue parsing
                    }
                }
                PivotFormatLoggerHelper.logParsingSuccessForEntity(
                        LOGGER,
                        PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, false),
                        newFolderDto.getName(),
                        folderToImport.getInternalId(),
                        pivotFormatImport);
            }

            folderToImport.setFolder(newFolderDto);
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.getFolderEntityKindNameFromJsonFile(jsonFile, false),
                    folderToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }
        return folderToImport;
    }
}
