/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

/**
 * Parameter names commonly used in queries
 *
 * @author mpagnon
 */
public final class ParameterNames {
    public static final String ATTACHMENT_LIST_ID = "attachmentListId";
    public static final String CALLED_ID = "called_id";
    public static final String CALLED_TABLE = "CALLED_TABLE";
    public static final String CALLER_ID = "caller_id";
    public static final String CALLING_TABLE = "CALLING_TABLE";
    public static final String CAMPAIGN_ID = "campaignId";
    public static final String CTE = "cte";
    public static final String DATASET_ID = "datasetId";
    public static final String ENTITIES = "entities";
    public static final String ENTITY = "entity";
    public static final String ENTITY_IDS = "entityIds";
    public static final String FOLDER_IDS = "folderIds";
    public static final String ID = "id";
    public static final String IDS = "ids";
    public static final String ITERATION_ID = "iterationId";
    public static final String KEYS = "keys";
    public static final String LIBRARY_NODE_ID = "libraryNodeId";
    public static final String NAME = "name";
    public static final String NODE_ID = "nodeId";
    public static final String NODE_IDS = "nodeIds";
    public static final String POSITION = "position";
    public static final String PROJECT = "project";
    public static final String PROJECT_ID = "projectId";
    public static final String PROJECT_IDS = "projectIds";
    public static final String REMOTE_IDS = "remoteIds";
    public static final String REMOTE_SYNCHRONISATION_ID = "remoteSynchronisationId";
    public static final String SERVER_ID = "serverId";
    public static final String SOURCE_ID = "source_id";
    public static final String SPRINTS = "sprints";
    public static final String SPRINT_ID = "sprintId";
    public static final String TEST_CASE_ID = "testCaseId";
    public static final String TEST_CASE_IDS = "testCaseIds";
    public static final String VERSION_ID = "versionId";

    private ParameterNames() {
        super();
    }
}
