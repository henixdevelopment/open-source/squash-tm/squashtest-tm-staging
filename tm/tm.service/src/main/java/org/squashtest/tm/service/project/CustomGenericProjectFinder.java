/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.project;

import java.util.Collection;
import java.util.List;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.project.AdministrableProject;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.users.PartyProjectPermissionsBean;
import org.squashtest.tm.service.internal.display.dto.party.UnboundPartiesResponse;

/**
 * Holder for non dynamically generated find methods for both Project and ProjectTemplate
 *
 * @author mpagnon
 */
public interface CustomGenericProjectFinder {

    AdministrableProject findAdministrableProjectById(long projectId);

    @UsedInPlugin("rest-api")
    List<PartyProjectPermissionsBean> findPartyPermissionsBeansByProject(long projectId);

    @UsedInPlugin("rest-api")
    List<AclGroup> findAllPossiblePermission();

    UnboundPartiesResponse findPartyWithoutPermissionByProject(long projectId);

    /**
     * Will return a list of TestAutomationProject (jobNames only) available for the server bound to
     * the given project. The returned list will not contain already bound ta-projects.
     *
     * @param projectId : the id of the {@link GenericProject} we want the available ta-projects for
     * @return : the list of {@link TestAutomationProject} available and not already bound to the
     *     tm-project
     */
    Collection<TestAutomationProject> findAllAvailableTaProjects(long projectId);

    /**
     * Will return a list of TestAutomationProject (jobNames only) available on the server bound to
     * the given project and authorized for the Jenkins account defined for the current user. The
     * returned list will not contain already bound ta-projects.
     *
     * @param projectId The Id of the {@link GenericProject} we want the available ta-projects for.
     * @return The list of {@link TestAutomationProject} available on the server, authorized for
     *     current user's Jenkins account and not already bound to the tm-project.
     */
    Collection<TestAutomationProject> findAllAvailableTaProjectsWithUserLevelCredentials(
            long projectId);

    List<String> getProjectEnvironmentTags(long projectId);

    boolean isInheritsEnvironmentTags(long projectId);
}
