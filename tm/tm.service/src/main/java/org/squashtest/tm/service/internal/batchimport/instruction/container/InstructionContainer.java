/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.instruction.container;

import static org.squashtest.tm.service.importer.ImportMode.CREATE;
import static org.squashtest.tm.service.importer.ImportMode.DELETE;
import static org.squashtest.tm.service.importer.ImportMode.UPDATE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.importer.ImportMode;
import org.squashtest.tm.service.importer.Target;
import org.squashtest.tm.service.internal.batchimport.Facility;
import org.squashtest.tm.service.internal.batchimport.instruction.Instruction;

public abstract class InstructionContainer<T extends Instruction<? extends Target>> {

    private final List<T> createInstructions;
    private final List<T> updateInstructions;
    private final List<T> deleteInstructions;

    protected InstructionContainer(List<T> instructions) {
        Map<ImportMode, List<T>> instructionsByMode =
                instructions.stream().collect(Collectors.groupingBy(T::getMode, Collectors.toList()));

        this.createInstructions = instructionsByMode.getOrDefault(CREATE, Collections.emptyList());
        this.updateInstructions = instructionsByMode.getOrDefault(UPDATE, Collections.emptyList());
        this.deleteInstructions = instructionsByMode.getOrDefault(DELETE, Collections.emptyList());
    }

    public List<T> getCreateInstructions() {
        return createInstructions;
    }

    public List<T> getUpdateInstructions() {
        return updateInstructions;
    }

    public List<T> getDeleteInstructions() {
        return deleteInstructions;
    }

    public List<T> executeInstructions(Facility facility, Project project) {

        if (!createInstructions.isEmpty()) {
            executeCreate(facility, project);
        }

        if (!updateInstructions.isEmpty()) {
            executeUpdate(facility, project);
        }

        if (!deleteInstructions.isEmpty()) {
            executeDelete(facility);
        }

        return getAllInstructions();
    }

    public List<T> getAllInstructions() {

        List<T> instructions = new ArrayList<>();
        instructions.addAll(createInstructions);
        instructions.addAll(updateInstructions);
        instructions.addAll(deleteInstructions);

        return instructions;
    }

    protected void executeUpdate(Facility facility, Project project) {
        executeInstructions(getUpdateInstructions(), facility);
    }

    protected void executeDelete(Facility facility) {
        executeInstructions(getDeleteInstructions(), facility);
    }

    protected void executeCreate(Facility facility, Project project) {
        executeInstructions(getCreateInstructions(), facility);
    }

    private void executeInstructions(List<T> instructions, Facility facility) {
        instructions.forEach(instruction -> instruction.execute(facility));
    }
}
