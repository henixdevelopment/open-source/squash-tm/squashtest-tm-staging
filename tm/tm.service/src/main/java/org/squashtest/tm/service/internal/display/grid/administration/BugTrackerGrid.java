/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import static org.jooq.impl.DSL.coalesce;
import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.max;
import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER;
import static org.squashtest.tm.jooq.domain.Tables.LIBRARY_PLUGIN_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.LIBRARY_PLUGIN_BINDING_PROPERTY;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SERVER_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SYNCHRONISATION_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.URL;

import java.util.Arrays;
import java.util.List;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

public class BugTrackerGrid extends AbstractGrid {

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(SERVER_ID)),
                new GridColumn(DSL.field(NAME)),
                new GridColumn(DSL.field(KIND)),
                new GridColumn(DSL.field(URL)),
                new GridColumn(DSL.field(SYNCHRONISATION_COUNT).as(SYNCHRONISATION_COUNT)));
    }

    @Override
    protected Table<?> getTable() {
        SelectHavingStep<Record2<Long, Integer>> externalSynchronisationCount =
                getExternalSynchronisationCount();

        return DSL.select(
                        THIRD_PARTY_SERVER.SERVER_ID.as(SERVER_ID),
                        THIRD_PARTY_SERVER.NAME.as(NAME),
                        BUGTRACKER.BUGTRACKER_ID,
                        BUGTRACKER.KIND.as(KIND),
                        THIRD_PARTY_SERVER.URL.as(URL),
                        coalesce(
                                        max(externalSynchronisationCount.field(SYNCHRONISATION_COUNT)),
                                        count(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID),
                                        0)
                                .as(SYNCHRONISATION_COUNT))
                .from(THIRD_PARTY_SERVER)
                .join(BUGTRACKER)
                .on(BUGTRACKER.BUGTRACKER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .leftJoin(REMOTE_SYNCHRONISATION)
                .on(REMOTE_SYNCHRONISATION.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .leftJoin(externalSynchronisationCount)
                .on(
                        THIRD_PARTY_SERVER.SERVER_ID.eq(
                                externalSynchronisationCount.field(SERVER_ID, Long.class)))
                .groupBy(THIRD_PARTY_SERVER.SERVER_ID, BUGTRACKER.BUGTRACKER_ID)
                .asTable();
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(SERVER_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return DSL.upper(DSL.field(NAME, String.class)).asc();
    }

    private SelectHavingStep<Record2<Long, Integer>> getExternalSynchronisationCount() {
        final List<String> pluginIds =
                Arrays.asList("squash.tm.plugin.redminereq", "squash.tm.plugin.jirareq");

        return DSL.select(
                        LIBRARY_PLUGIN_BINDING_PROPERTY.PLUGIN_BINDING_VALUE.cast(Long.class).as(SERVER_ID),
                        count(LIBRARY_PLUGIN_BINDING.PLUGIN_BINDING_ID).as(SYNCHRONISATION_COUNT))
                .from(LIBRARY_PLUGIN_BINDING)
                .innerJoin(LIBRARY_PLUGIN_BINDING_PROPERTY)
                .on(
                        LIBRARY_PLUGIN_BINDING_PROPERTY.PLUGIN_BINDING_ID.eq(
                                LIBRARY_PLUGIN_BINDING.PLUGIN_BINDING_ID))
                .where(
                        LIBRARY_PLUGIN_BINDING
                                .PLUGIN_ID
                                .in(pluginIds)
                                .and(LIBRARY_PLUGIN_BINDING_PROPERTY.PLUGIN_BINDING_KEY.eq("serverId")))
                .groupBy(DSL.field(SERVER_ID));
    }
}
