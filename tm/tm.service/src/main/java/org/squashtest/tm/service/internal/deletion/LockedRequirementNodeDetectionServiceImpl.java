/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.deletion.MultipleReqVersionWithActiveMilestonesReport;
import org.squashtest.tm.service.deletion.SingleMilestonesReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.campaign.LockedRequirementNodeDetectionService;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementDeletionDao;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Service
public class LockedRequirementNodeDetectionServiceImpl extends AbstractLockedNodeDetectionService
        implements LockedRequirementNodeDetectionService {
    private static final String REQUIREMENTS_TYPE = "requirements";

    private final RequirementDeletionDao deletionDao;
    private final RequirementDao requirementDao;

    @Autowired
    public LockedRequirementNodeDetectionServiceImpl(
            ActiveMilestoneHolder activeMilestoneHolder,
            RequirementDeletionDao deletionDao,
            RequirementDao requirementDao) {
        super(activeMilestoneHolder);
        this.deletionDao = deletionDao;
        this.requirementDao = requirementDao;
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedByMilestone(List<Long> nodeIds) {
        return super.detectLockedByMilestone(nodeIds, REQUIREMENTS_TYPE);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedWithActiveMilestone(List<Long> nodeIds) {
        return super.detectLockedWithActiveMilestone(nodeIds, REQUIREMENTS_TYPE);
    }

    @Override
    protected void addMultipleMilestoneBindingToReportList(
            List<Long> nodeIds,
            List<Long> boundNodes,
            List<SuppressionPreviewReport> reportList,
            String reportType) {
        if (!boundNodes.isEmpty()) {
            SuppressionPreviewReport suppressionPreviewReport =
                    getReportMultipleMilestoneBinding(nodeIds, boundNodes, reportType);
            // deletion : no node bound to more than one milestone shall be deleted
            suppressionPreviewReport.addAllLockedNodes(boundNodes);
            reportList.add(suppressionPreviewReport);
        } else {
            reportList.add(new SingleMilestonesReport(reportType));
        }
    }

    @Override
    protected void addAdditionalReportWithActiveMilestoneToReportList(
            List<Long> nodeIds,
            Long activeMilestoneId,
            List<SuppressionPreviewReport> reportList,
            String reportType) {
        List<Long> nonBoundNodes = requirementDao.findNonBoundRequirement(nodeIds, activeMilestoneId);
        addNonBoundNodesWithActiveMilestoneToReportList(nonBoundNodes, reportList, reportType);
        // requirement that can be deleted must have only one version
        List<Long> reqHavingManyVersions = requirementDao.filterRequirementHavingManyVersions(nodeIds);
        if (!reqHavingManyVersions.isEmpty()) {
            SuppressionPreviewReport suppressionPreviewReport =
                    new MultipleReqVersionWithActiveMilestonesReport(reportType);
            suppressionPreviewReport.addAllLockedNodes(reqHavingManyVersions);
            reportList.add(suppressionPreviewReport);
        }
    }

    @Override
    protected List<Long> findNodesWhichMilestonesForbidsDeletion(List<Long> nodeIds) {
        return deletionDao.filterRequirementsIdsWhichMilestonesForbidsDeletion(nodeIds);
    }

    @Override
    protected List<Long>[] getIdsSeparateFolderFromNodeIds(List<Long> nodeIds) {
        return deletionDao.separateFolderFromRequirementIds(nodeIds);
    }

    @Override
    protected List<Long> findNodeIdsHavingMultipleMilestones(List<Long> nodeIds) {
        return deletionDao.filterRequirementHavingMultipleMilestones(nodeIds);
    }
}
