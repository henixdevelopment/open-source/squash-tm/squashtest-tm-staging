/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.domain.synchronisation.SynchronisationStatus;

public class RemoteSynchronisationDto {
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteSynchronisationDto.class);

    private long id;
    private String name;
    private boolean isEnabled;
    private long serverId;
    private String serverName;
    private long projectId;
    private String projectName;
    private String perimeter;
    private String remoteSelectType;
    private String remoteSelectValue;
    private SynchronisationStatus status;
    private Date lastSyncDate;
    private Date lastSuccessfulSyncDate;
    private String syncErrorLogFilePath;
    private Integer synchronizedRequirementsCount;
    private Integer unprocessedRequirementsCount;
    private Integer synchronizedSprintTicketsCount;
    private Integer unprocessedSprintTicketsCount;

    public RemoteSynchronisationDto(RemoteSynchronisation remoteSync) {
        this.id = remoteSync.getId();
        this.name = remoteSync.getName();
        this.isEnabled = remoteSync.isSynchronisationEnable();
        this.serverId = remoteSync.getServer().getId();
        this.serverName = remoteSync.getServer().getName();
        this.projectId = remoteSync.getProject().getId();
        this.projectName = remoteSync.getProject().getName();
        this.perimeter = getStringOption(remoteSync, "perimeter");
        this.remoteSelectType = remoteSync.getSelectType();
        this.remoteSelectValue = remoteSync.getSelectValue();
        this.status = remoteSync.getSynchronisationStatus();
        this.lastSyncDate = remoteSync.getLastSyncDate();
        this.lastSuccessfulSyncDate = remoteSync.getLastSuccessfulSyncDate();
        this.synchronizedRequirementsCount =
                getIntegerOption(remoteSync, "synchronizedRequirementsCount");
        this.unprocessedRequirementsCount =
                getIntegerOption(remoteSync, "unprocessedRequirementsCount");
        this.synchronizedSprintTicketsCount =
                getIntegerOption(remoteSync, "synchronizedSprintTicketsCount");
        this.unprocessedSprintTicketsCount =
                getIntegerOption(remoteSync, "unprocessedSprintTicketsCount");
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean enabled) {
        this.isEnabled = enabled;
    }

    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(String perimeter) {
        this.perimeter = perimeter;
    }

    public String getRemoteSelectType() {
        return remoteSelectType;
    }

    public void setRemoteSelectType(String remoteSelectType) {
        this.remoteSelectType = remoteSelectType;
    }

    public String getRemoteSelectValue() {
        return remoteSelectValue;
    }

    public void setRemoteSelectValue(String remoteSelectValue) {
        this.remoteSelectValue = remoteSelectValue;
    }

    public SynchronisationStatus getStatus() {
        return status;
    }

    public void setStatus(SynchronisationStatus status) {
        this.status = status;
    }

    public Date getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Date lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public Date getLastSuccessfulSyncDate() {
        return lastSuccessfulSyncDate;
    }

    public void setLastSuccessfulSyncDate(Date lastSuccessfulSyncDate) {
        this.lastSuccessfulSyncDate = lastSuccessfulSyncDate;
    }

    public Integer getSynchronizedRequirementsCount() {
        return synchronizedRequirementsCount;
    }

    public Integer getUnprocessedRequirementsCount() {
        return unprocessedRequirementsCount;
    }

    public Integer getSynchronizedSprintTicketsCount() {
        return synchronizedSprintTicketsCount;
    }

    public Integer getUnprocessedSprintTicketsCount() {
        return unprocessedSprintTicketsCount;
    }

    private Integer getIntegerOption(RemoteSynchronisation remoteSynchronisation, String optionKey) {
        JsonNode synchronisationOption = getSynchronisationOption(remoteSynchronisation, optionKey);
        return synchronisationOption != null ? synchronisationOption.asInt() : null;
    }

    private String getStringOption(RemoteSynchronisation remoteSynchronisation, String optionKey) {
        JsonNode synchronisationOption = getSynchronisationOption(remoteSynchronisation, optionKey);
        return synchronisationOption != null ? synchronisationOption.asText() : null;
    }

    private JsonNode getSynchronisationOption(
            RemoteSynchronisation remoteSynchronisation, String optionKey) {
        try {
            if (remoteSynchronisation.getOptions() != null) {
                JsonNode optionsNode = new ObjectMapper().readTree((remoteSynchronisation.getOptions()));
                return optionsNode.get(optionKey);
            } else {
                return null;
            }
        } catch (IOException ioException) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(
                        String.format(
                                "Could not deserialize perimeter for synchronisation %d",
                                remoteSynchronisation.getId()));
            }
            throw new IllegalArgumentException(ioException);
        }
    }

    public String getSyncErrorLogFilePath() {
        return syncErrorLogFilePath;
    }

    public void setSyncErrorLogFilePath(String syncErrorLogFilePath) {
        this.syncErrorLogFilePath = syncErrorLogFilePath;
    }
}
