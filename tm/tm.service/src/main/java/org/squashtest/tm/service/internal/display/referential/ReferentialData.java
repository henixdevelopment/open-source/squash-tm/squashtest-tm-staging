/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.referential;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.display.dto.AutomatedTestTechnologyDto;
import org.squashtest.tm.service.internal.display.dto.InfoListDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionLinkTypeDto;
import org.squashtest.tm.service.internal.display.dto.ScmServerDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerDto;

public class ReferentialData extends AdminReferentialData {

    private List<ProjectDto> projects = new ArrayList<>();
    private List<Long> filteredProjectIds = new ArrayList<>();
    private boolean projectFilterStatus = false;
    private List<InfoListDto> infoLists = new ArrayList<>();
    private List<MilestoneDto> milestones = new ArrayList<>();
    private List<TestAutomationServerDto> automationServers = new ArrayList<>();
    private List<RequirementVersionLinkTypeDto> requirementVersionLinkTypes = new ArrayList<>();
    // Plugins that provide a wizard in a workspace (e.g campaign assistants)
    private List<WorkspaceWizardDto> workspaceWizards = new ArrayList<>();
    private List<AutomatedTestTechnologyDto> automatedTestTechnologies = new ArrayList<>();
    private List<ScmServerDto> scmServers = new ArrayList<>();
    private List<ProjectPermissionDto> projectPermissions = new ArrayList<>();

    public List<ProjectDto> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectDto> projects) {
        this.projects = projects;
    }

    public List<Long> getFilteredProjectIds() {
        return filteredProjectIds;
    }

    public void setFilteredProjectIds(List<Long> filteredProjectIds) {
        this.filteredProjectIds = filteredProjectIds;
    }

    public boolean isProjectFilterStatus() {
        return projectFilterStatus;
    }

    public void setProjectFilterStatus(boolean projectFilterStatus) {
        this.projectFilterStatus = projectFilterStatus;
    }

    public List<InfoListDto> getInfoLists() {
        return infoLists;
    }

    public void setInfoLists(List<InfoListDto> infoLists) {
        this.infoLists = infoLists;
    }

    public List<MilestoneDto> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<MilestoneDto> milestones) {
        this.milestones = milestones;
    }

    public List<TestAutomationServerDto> getAutomationServers() {
        return automationServers;
    }

    public void setAutomationServers(List<TestAutomationServerDto> automationServers) {
        this.automationServers = automationServers;
    }

    public List<RequirementVersionLinkTypeDto> getRequirementVersionLinkTypes() {
        return requirementVersionLinkTypes;
    }

    public void setRequirementVersionLinkTypes(
            List<RequirementVersionLinkTypeDto> requirementVersionLinkTypes) {
        this.requirementVersionLinkTypes = requirementVersionLinkTypes;
    }

    public List<WorkspaceWizardDto> getWorkspaceWizards() {
        return workspaceWizards;
    }

    public void setWorkspaceWizards(List<WorkspaceWizardDto> workspaceWizards) {
        this.workspaceWizards = workspaceWizards;
    }

    public List<AutomatedTestTechnologyDto> getAutomatedTestTechnologies() {
        return automatedTestTechnologies;
    }

    public void setAutomatedTestTechnologies(
            List<AutomatedTestTechnologyDto> automatedTestTechnologies) {
        this.automatedTestTechnologies = automatedTestTechnologies;
    }

    public List<ScmServerDto> getScmServers() {
        return scmServers;
    }

    public void setScmServers(List<ScmServerDto> scmServers) {
        this.scmServers = scmServers;
    }

    public List<ProjectPermissionDto> getProjectPermissions() {
        return projectPermissions;
    }

    public void setProjectPermissions(List<ProjectPermissionDto> projectPermissions) {
        this.projectPermissions = projectPermissions;
    }
}
