/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.license;

import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.license.UltimateLicenseAvailability;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;

@Service
@Transactional
public class UltimateLicenseAvailabilityServiceImpl implements UltimateLicenseAvailabilityService {

    private final UltimateLicenseAvailability ultimateLicenseAvailability;

    @Autowired
    public UltimateLicenseAvailabilityServiceImpl(
            Optional<UltimateLicenseAvailability> ultimateLicenseAvailability) {
        this.ultimateLicenseAvailability = ultimateLicenseAvailability.orElse(null);
    }

    @Override
    public boolean isAvailable() {
        return Objects.nonNull(ultimateLicenseAvailability)
                && ultimateLicenseAvailability.isAvailable();
    }

    @Override
    public void checkIfAvailable() {
        if (Objects.nonNull(ultimateLicenseAvailability)) {
            ultimateLicenseAvailability.checkIfAvailable();
        } else {
            throw new AccessDeniedException("No ultimate license available");
        }
    }
}
