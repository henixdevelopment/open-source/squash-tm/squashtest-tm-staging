/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import static org.squashtest.tm.domain.EntityType.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.service.security.Authorizations.CREATE_REQFOLDER_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.CREATE_REQLIBRARY_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.CREATE_REQUIREMENT_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_LIBRARY_NODE_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_OR_ROLE_ADMIN;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.library.NewFolderDto;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.ExportRequirementData;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.NewRequirementVersionDto;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.requirement.RequirementLibraryNodeVisitor;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.exception.library.NameAlreadyExistsAtDestinationException;
import org.squashtest.tm.exception.sync.PathAlreadyInUseException;
import org.squashtest.tm.exception.sync.PathValidationDomainException;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.CheckBlockingMilestones;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.annotation.PreventConcurrents;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.importer.ImportLog;
import org.squashtest.tm.service.importer.XlsImportLimitationHandler;
import org.squashtest.tm.service.infolist.InfoListItemFinderService;
import org.squashtest.tm.service.internal.batchexport.RequirementExcelExporter;
import org.squashtest.tm.service.internal.batchexport.RequirementExportDao;
import org.squashtest.tm.service.internal.batchexport.SearchRequirementExcelExporter;
import org.squashtest.tm.service.internal.batchexport.SearchSimpleRequirementExcelExporter;
import org.squashtest.tm.service.internal.batchexport.models.RequirementExportModel;
import org.squashtest.tm.service.internal.batchimport.requirement.RequirementExcelBatchImporter;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.library.AbstractLibraryNavigationService;
import org.squashtest.tm.service.internal.library.NodeDeletionHandler;
import org.squashtest.tm.service.internal.library.PasteStrategy;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementFolderDao;
import org.squashtest.tm.service.internal.repository.RequirementLibraryDao;
import org.squashtest.tm.service.internal.repository.RequirementLibraryNodeDao;
import org.squashtest.tm.service.internal.repository.display.HighLevelRequirementDisplayDao;
import org.squashtest.tm.service.internal.requirement.coercers.RLNAndParentIdsCoercerForArray;
import org.squashtest.tm.service.internal.requirement.coercers.RLNAndParentIdsCoercerForList;
import org.squashtest.tm.service.internal.requirement.coercers.RequirementLibraryIdsCoercerForArray;
import org.squashtest.tm.service.internal.requirement.coercers.RequirementLibraryIdsCoercerForList;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.milestone.MilestoneMembershipManager;
import org.squashtest.tm.service.project.ProjectFilterModificationService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.remotesynchronisation.RemoteSynchronisationService;
import org.squashtest.tm.service.requirement.RequirementLibraryFinderService;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.requirement.RequirementStatisticsService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.SecurityCheckableObject;
import org.squashtest.tm.service.statistics.requirement.RequirementStatisticsBundle;
import org.squashtest.tm.service.user.UserAccountService;

@SuppressWarnings("rawtypes")
@Service("squashtest.tm.service.RequirementLibraryNavigationService")
@Transactional
public class RequirementLibraryNavigationServiceImpl
        extends AbstractLibraryNavigationService<
                RequirementLibrary, RequirementFolder, RequirementLibraryNode>
        implements RequirementLibraryNavigationService, RequirementLibraryFinderService {
    public static final String SYNCHRONISATION_PATH = "synchronisationPath";
    private static final String REQUIREMENT_ID = "requirementId";
    private static final String SOURCE_NODES_IDS = "sourceNodesIds";
    private static final String DESTINATION_ID = "destinationId";
    private static final String TARGET_ID = "targetId";
    private static final String EXPORT = "EXPORT";
    private static final String NODE_IDS = "nodeIds";
    private static final String SIMPLE = "simple";

    @Inject private RequirementLibraryDao requirementLibraryDao;
    @Inject private RequirementFolderDao requirementFolderDao;

    @Inject private RequirementLibraryNodeDao requirementLibraryNodeDao;

    @Inject private RequirementDao requirementDao;
    @Inject private RequirementNodeDeletionHandler deletionHandler;
    @Inject private ProjectFilterModificationService projectFilterModificationService;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToRequirementFolderStrategy")
    private Provider<PasteStrategy<RequirementFolder, RequirementLibraryNode>>
            pasteToRequirementFolderStrategyProvider;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToRequirementLibraryStrategy")
    private Provider<PasteStrategy<RequirementLibrary, RequirementLibraryNode>>
            pasteToRequirementLibraryStrategyProvider;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToRequirementStrategy")
    private Provider<PasteStrategy<Requirement, Requirement>> pasteToRequirementStrategyProvider;

    @Inject private RequirementStatisticsService statisticsService;

    @Inject private MilestoneMembershipManager milestoneService;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private InfoListItemFinderService infoListItemService;

    @Inject private RequirementExportDao requirementExportDao;

    @Inject
    @Named(value = "requirementExcelExporter")
    private Provider<RequirementExcelExporter> exporterProvider;

    @Inject private Provider<SearchRequirementExcelExporter> searchExporterProvider;

    @Inject private Provider<SearchSimpleRequirementExcelExporter> searchSimpleExporterProvider;

    @Inject private RequirementExcelBatchImporter batchImporter;

    @Inject private ProjectDao projectDao;

    @Inject private CustomFieldBindingFinderService customFieldBindingFinderService;

    @Inject private PrivateCustomFieldValueService customFieldValueService;

    @Inject private RemoteSynchronisationService remoteSynchronisationService;

    @Inject private RequirementFactory requirementFactory;

    @Inject private HighLevelRequirementDisplayDao highLevelRequirementDisplayDao;

    @Inject private UserAccountService userAccountService;

    @Inject private ProjectFinder projectFinder;

    @Inject private XlsImportLimitationHandler xlsImportLimitationHandler;

    @Override
    protected NodeDeletionHandler<RequirementLibraryNode, RequirementFolder> getDeletionHandler() {
        return deletionHandler;
    }

    @Override
    @PostAuthorize("hasPermission(returnObject,'READ') " + OR_HAS_ROLE_ADMIN)
    public Requirement findRequirement(long reqId) {
        return requirementDao.findById(reqId);
    }

    @Override
    protected RequirementLibraryDao getLibraryDao() {
        return requirementLibraryDao;
    }

    @Override
    protected RequirementFolderDao getFolderDao() {
        return requirementFolderDao;
    }

    @Override
    protected RequirementLibraryNodeDao getLibraryNodeDao() {
        return requirementLibraryNodeDao;
    }

    @Override
    protected PasteStrategy<RequirementFolder, RequirementLibraryNode> getPasteToFolderStrategy() {
        return pasteToRequirementFolderStrategyProvider.get();
    }

    @Override
    protected PasteStrategy<RequirementLibrary, RequirementLibraryNode> getPasteToLibraryStrategy() {
        return pasteToRequirementLibraryStrategyProvider.get();
    }

    private PasteStrategy<Requirement, Requirement> getPasteToRequirementStrategy() {
        return pasteToRequirementStrategyProvider.get();
    }

    @Override
    @UsedInPlugin("Xsquash4GitLab & Xsquash4Jira")
    public String getPathAsString(long entityId) {
        // get
        RequirementLibraryNode node = getLibraryNodeDao().findById(entityId);

        // check
        checkPermission(new SecurityCheckableObject(node, "READ"));

        // proceed
        List<String> names = getLibraryNodeDao().getParentsName(entityId);
        String projectName = node.getProject().getName();

        if (projectName.contains("/")) {
            projectName = projectName.replace("/", "\\/");
        }

        return "/" + projectName + "/" + formatPath(names);
    }

    private String formatPath(List<String> names) {
        StringBuilder builder = new StringBuilder();
        for (String name : names) {
            builder.append("/").append(name);
        }
        return builder.toString();
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.requirement.RequirementLibrary' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibrary.class)
    public RequirementFolder addFolderToLibrary(@Id long destinationId, RequirementFolder newFolder) {

        RequirementLibrary container = getLibraryDao().findById(destinationId);
        container.addContent(newFolder);

        // fix the nature and type for the possible nested test cases inside that folder
        replaceAllInfoListReferences(newFolder);

        // now proceed
        getFolderDao().persist(newFolder);

        // and then create the custom field values, as a better fix for [Issue 2061]
        createAllCustomFieldValues(newFolder);
        createAttachmentsFromLibraryNode(newFolder, newFolder);
        generateCuf(newFolder);
        return newFolder;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.requirement.RequirementLibrary' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibrary.class)
    public RequirementFolder addFolderToLibrary(@Id long destinationId, NewFolderDto folderDto) {
        RequirementFolder newFolder = (RequirementFolder) folderDto.toFolder(REQUIREMENT_FOLDER);
        return addFolderToLibrary(destinationId, newFolder, folderDto.getCustomFields());
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.requirement.RequirementFolder' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibraryNode.class)
    public RequirementFolder addFolderToFolder(@Id long destinationId, RequirementFolder newFolder) {

        RequirementFolder container = getFolderDao().findById(destinationId);
        container.addContent(newFolder);

        // fix the nature and type for the possible nested test cases inside that folder
        replaceAllInfoListReferences(newFolder);

        // now proceed
        getFolderDao().persist(newFolder);

        // and then create the custom field values, as a better fix for [Issue 2061]
        createAllCustomFieldValues(newFolder);
        generateCuf(newFolder);
        createAttachmentsFromLibraryNode(newFolder, newFolder);
        return newFolder;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.requirement.RequirementFolder' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibraryNode.class)
    public RequirementFolder addFolderToFolder(@Id long destinationId, NewFolderDto folderDto) {
        RequirementFolder newFolder = (RequirementFolder) folderDto.toFolder(REQUIREMENT_FOLDER);
        return addFolderToFolder(destinationId, newFolder, folderDto.getCustomFields());
    }

    private void generateCuf(RequirementFolder newFolder) {
        List<CustomFieldBinding> projectsBindings =
                customFieldBindingFinderService.findCustomFieldsForProjectAndEntity(
                        newFolder.getProject().getId(), BindableEntity.REQUIREMENT_FOLDER);
        for (CustomFieldBinding binding : projectsBindings) {
            customFieldValueService.cascadeCustomFieldValuesCreationNotCreatedFolderYet(
                    binding, newFolder);
        }
    }

    @Override
    @PreAuthorize(CREATE_REQLIBRARY_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibrary.class)
    public Requirement addRequirementToRequirementLibrary(
            @Id long libraryId, @NotNull NewRequirementVersionDto newVersion, List<Long> milestoneIds) {
        return addRequirementToRequirementLibraryUnsecured(libraryId, newVersion, milestoneIds);
    }

    @Override
    public Requirement addRequirementToRequirementLibraryUnsecured(
            @Id long libraryId, @NotNull NewRequirementVersionDto newVersion, List<Long> milestoneIds) {
        RequirementLibrary library = requirementLibraryDao.findById(libraryId);

        Requirement newReq = createRequirement(newVersion);

        library.addContent(newReq);

        replaceAllInfoListReferences(newReq);

        requirementDao.persist(newReq);
        createCustomFieldValues(newReq.getCurrentVersion());

        customFieldValueService.initCustomFieldValues(
                newReq.getCurrentVersion(), newVersion.getCustomFields());
        createAttachmentsFromLibraryNode(newReq, newReq.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(
                newReq.getCurrentVersion().getId(), milestoneIds);

        return newReq;
    }

    @Override
    @PreAuthorize(CREATE_REQLIBRARY_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibrary.class)
    public Requirement addRequirementToRequirementLibrary(
            @Id long libraryId, @NotNull Requirement requirement, List<Long> milestoneIds) {
        RequirementLibrary library = requirementLibraryDao.findById(libraryId);

        if (!library.isContentNameAvailable(requirement.getName())) {
            throw new DuplicateNameException(requirement.getName(), requirement.getName());
        }

        library.addContent(requirement);
        replaceAllInfoListReferences(requirement);
        requirementDao.persist(requirement);
        createCustomFieldValues(requirement.getCurrentVersion());
        createAttachmentsFromLibraryNode(requirement, requirement.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(
                requirement.getCurrentVersion().getId(), milestoneIds);
        return requirement;
    }

    private Requirement createRequirement(NewRequirementVersionDto newVersionData) {
        return requirementFactory.createRequirement(newVersionData);
    }

    @Override
    @PreAuthorize(CREATE_REQFOLDER_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibraryNode.class)
    public Requirement addRequirementToRequirementFolder(
            @Id long folderId, @NotNull NewRequirementVersionDto firstVersion, List<Long> milestoneIds) {
        return addRequirementToRequirementFolderUnsecured(folderId, firstVersion, milestoneIds);
    }

    @Override
    public Requirement addRequirementToRequirementFolderUnsecured(
            @Id long folderId, @NotNull NewRequirementVersionDto firstVersion, List<Long> milestoneIds) {
        RequirementFolder folder = requirementFolderDao.findByIdWithContent(folderId);

        Requirement newReq = createRequirement(firstVersion);

        folder.addContent(newReq);
        replaceAllInfoListReferences(newReq);
        requirementDao.persist(newReq);
        RequirementVersion currentVersion = newReq.getCurrentVersion();
        createCustomFieldValues(currentVersion);
        customFieldValueService.initCustomFieldValues(currentVersion, firstVersion.getCustomFields());
        createAttachmentsFromLibraryNode(newReq, newReq.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(currentVersion.getId(), milestoneIds);

        return newReq;
    }

    @Override
    @PreAuthorize(CREATE_REQFOLDER_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibraryNode.class)
    public Requirement addRequirementToRequirementFolder(
            @Id long folderId, @NotNull Requirement requirement, List<Long> milestoneIds) {
        RequirementFolder folder = requirementFolderDao.findByIdWithContent(folderId);

        if (!folder.isContentNameAvailable(requirement.getName())) {
            throw new DuplicateNameException(requirement.getName(), requirement.getName());
        }

        folder.addContent(requirement);
        replaceAllInfoListReferences(requirement);
        requirementDao.persist(requirement);
        createCustomFieldValues(requirement.getCurrentVersion());
        createAttachmentsFromLibraryNode(requirement, requirement.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(
                requirement.getCurrentVersion().getId(), milestoneIds);

        return requirement;
    }

    @Override
    @PreAuthorize(CREATE_REQUIREMENT_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibraryNode.class)
    @CheckBlockingMilestone(entityType = Requirement.class)
    public Requirement addRequirementToRequirement(
            @Id long requirementId,
            @NotNull NewRequirementVersionDto newRequirement,
            List<Long> milestoneIds) {
        return addRequirementToRequirementUnsecured(requirementId, newRequirement, milestoneIds);
    }

    @Override
    public Requirement addRequirementToRequirementUnsecured(
            @Id long requirementId,
            @NotNull NewRequirementVersionDto newRequirement,
            List<Long> milestoneIds) {

        Requirement parent = requirementDao.findByIdWithChildren(requirementId);
        Requirement child = createRequirement(newRequirement);

        parent.addContent(child);
        replaceAllInfoListReferences(child);
        requirementDao.persist(child);

        createCustomFieldValues(child.getCurrentVersion());
        customFieldValueService.initCustomFieldValues(
                child.getCurrentVersion(), newRequirement.getCustomFields());
        createAttachmentsFromLibraryNode(child, child.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(
                child.getCurrentVersion().getId(), milestoneIds);

        return child;
    }

    @Override
    @PreAuthorize(CREATE_REQUIREMENT_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibraryNode.class)
    @CheckBlockingMilestone(entityType = Requirement.class)
    public Requirement addRequirementToHighLevelRequirement(
            @Id long requirementId,
            @NotNull NewRequirementVersionDto newRequirement,
            List<Long> milestoneIds) {

        HighLevelRequirement parent = (HighLevelRequirement) requirementDao.findById(requirementId);
        Requirement child = createRequirement(newRequirement);

        parent.addContent(child);
        replaceAllInfoListReferences(child);
        requirementDao.persist(child);

        createCustomFieldValues(child.getCurrentVersion());
        customFieldValueService.initCustomFieldValues(
                child.getCurrentVersion(), newRequirement.getCustomFields());
        createAttachmentsFromLibraryNode(child, child.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(
                child.getCurrentVersion().getId(), milestoneIds);

        return child;
    }

    @Override
    @PreAuthorize(CREATE_REQUIREMENT_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = RequirementLibraryNode.class)
    public Requirement addRequirementToRequirement(
            @Id long requirementId, @NotNull Requirement newRequirement, List<Long> milestoneIds) {

        Requirement parent = requirementDao.findByIdWithChildren(requirementId);

        parent.addContent(newRequirement);
        replaceAllInfoListReferences(newRequirement);
        requirementDao.persist(newRequirement);
        createCustomFieldValues(newRequirement.getCurrentVersion());
        createAttachmentsFromLibraryNode(newRequirement, newRequirement.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(
                newRequirement.getCurrentVersion().getId(), milestoneIds);

        return newRequirement;
    }

    private Requirement addRequirementToHighLevelRequirement(
            long requirementId, Requirement newRequirement, List<Long> milestoneIds) {

        HighLevelRequirement parent = (HighLevelRequirement) requirementDao.findById(requirementId);

        parent.addContent(newRequirement);
        replaceAllInfoListReferences(newRequirement);
        requirementDao.persist(newRequirement);
        createCustomFieldValues(newRequirement.getCurrentVersion());
        createAttachmentsFromLibraryNode(newRequirement, newRequirement.getCurrentVersion());
        milestoneService.bindRequirementVersionToMilestones(
                newRequirement.getCurrentVersion().getId(), milestoneIds);

        return newRequirement;
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibraryNode.class, paramName = REQUIREMENT_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = SOURCE_NODES_IDS,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = SOURCE_NODES_IDS,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    @CheckBlockingMilestone(entityType = Requirement.class)
    public List<Requirement> copyNodesToRequirement(
            @Id(REQUIREMENT_ID) long requirementId,
            @Ids(SOURCE_NODES_IDS) Long[] sourceNodesIds,
            ClipboardPayload clipboardPayload) {
        PasteStrategy<Requirement, Requirement> pasteStrategy = getPasteToRequirementStrategy();
        makeCopierStrategy(pasteStrategy);
        return pasteStrategy.pasteNodes(requirementId, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibraryNode.class, paramName = REQUIREMENT_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = NODE_IDS,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = NODE_IDS,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    @CheckBlockingMilestone(entityType = Requirement.class)
    public void moveNodesToRequirement(
            @Id(REQUIREMENT_ID) long requirementId,
            @Ids(NODE_IDS) Long[] nodeIds,
            ClipboardPayload clipboardPayload) {
        if (nodeIds.length == 0) {
            return;
        }
        try {
            PasteStrategy<Requirement, Requirement> pasteStrategy = getPasteToRequirementStrategy();
            makeMoverStrategy(pasteStrategy);
            pasteStrategy.pasteNodes(requirementId, clipboardPayload);
        } catch (NullArgumentException | DuplicateNameException dne) {
            throw new NameAlreadyExistsAtDestinationException(dne);
        }
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibraryNode.class, paramName = REQUIREMENT_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = NODE_IDS,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = NODE_IDS,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    @CheckBlockingMilestone(entityType = Requirement.class)
    public void moveNodesToRequirement(
            @Id(REQUIREMENT_ID) long requirementId, @Ids(NODE_IDS) Long[] nodeIds) {
        moveNodesToRequirement(
                requirementId, nodeIds, ClipboardPayload.withWhiteListIgnored(Arrays.asList(nodeIds)));
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibraryNode.class, paramName = REQUIREMENT_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = NODE_IDS,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = NODE_IDS,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    @CheckBlockingMilestone(entityType = Requirement.class)
    public void moveNodesToRequirement(
            @Id(REQUIREMENT_ID) long requirementId,
            @Ids(NODE_IDS) Long[] nodeIds,
            int position,
            ClipboardPayload clipboardPayload) {
        if (nodeIds.length == 0) {
            return;
        }
        try {
            PasteStrategy<Requirement, Requirement> pasteStrategy = getPasteToRequirementStrategy();
            makeMoverStrategy(pasteStrategy);
            pasteStrategy.pasteNodes(requirementId, clipboardPayload, position);
        } catch (NullArgumentException | DuplicateNameException dne) {
            throw new NameAlreadyExistsAtDestinationException(dne);
        }
    }

    @Override
    public List<ExportRequirementData> findRequirementsToExportFromLibrary(List<Long> libraryIds) {
        PermissionsUtils.checkPermission(
                permissionService, libraryIds, EXPORT, RequirementLibrary.class.getName());
        return requirementDao.findRequirementToExportFromLibrary(libraryIds);
    }

    @Override
    public List<ExportRequirementData> findRequirementsToExportFromNodes(List<Long> nodesIds) {
        PermissionsUtils.checkPermission(
                permissionService, nodesIds, EXPORT, RequirementLibraryNode.class.getName());
        return requirementDao.findRequirementToExportFromNodes(nodesIds);
    }

    @Override
    @PreAuthorize(READ_REQUIREMENT_OR_ROLE_ADMIN)
    public List<Requirement> findChildrenRequirements(long requirementId) {
        return requirementDao.findChildrenRequirements(requirementId);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = SOURCE_NODES_IDS,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = SOURCE_NODES_IDS,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    public void copyNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(SOURCE_NODES_IDS) Long[] sourceNodesIds,
            ClipboardPayload clipboardPayload) {
        super.copyNodesToFolder(destinationId, sourceNodesIds, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = TARGET_ID,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    public void copyNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload) {
        super.copyNodesToLibrary(destinationId, targetId, clipboardPayload);
    }

    // ******************** more private code *******************

    private void replaceAllInfoListReferences(RequirementFolder folder) {
        new CategoryChainFixer().fix(folder);
    }

    private void replaceAllInfoListReferences(Requirement requirement) {
        CategoryChainFixer.fix(requirement);
    }

    private void createAllCustomFieldValues(RequirementFolder folder) {
        new CustomFieldValuesFixer().fix(folder);
    }

    private class CustomFieldValuesFixer implements RequirementLibraryNodeVisitor {

        private void fix(RequirementFolder folder) {
            for (RequirementLibraryNode node : folder.getContent()) {
                node.accept(this);
            }
        }

        private void fix(Requirement req) {
            req.accept(this);
        }

        @Override
        public void visit(Requirement requirement) {
            createCustomFieldValues(requirement.getCurrentVersion());
            for (Requirement req : requirement.getContent()) {
                fix(req);
            }
        }

        @Override
        public void visit(RequirementFolder folder) {
            fix(folder);
        }
    }

    @Override
    public File exportRequirementAsExcel(
            List<Long> libraryIds,
            List<Long> nodeIds,
            boolean keepRteFormat,
            MessageSource messageSource) {
        return exportRequirementAsExcel(libraryIds, nodeIds, false, keepRteFormat, messageSource);
    }

    @Override
    public File exportRequirementAsExcel(
            List<Long> libraryIds,
            List<Long> nodeIds,
            boolean addLinkedLowLevelReq,
            boolean keepRteFormat,
            MessageSource messageSource) {

        // 2.1 Find the list of all req ids that belongs to library and node selection.
        Set<Long> reqIds = new HashSet<>();
        Collection<Long> allIds = findRequirementIdsFromSelection(libraryIds, nodeIds);
        allIds = securityFilterIds(allIds, Requirement.class.getName(), EXPORT);
        reqIds.addAll(allIds);

        // 2.2 Add linked low level requirements, if option is selected, filtered by exportable projects
        // for current user
        if (addLinkedLowLevelReq) {
            List<Long> exportableProjectIds = findExportableProjectIds();
            reqIds.addAll(
                    highLevelRequirementDisplayDao.findStandardRequirementsByRequirementIdsAndProjectIds(
                            new ArrayList<>(reqIds), exportableProjectIds));
        }

        // 3. For each req, find all versions
        List<Long> reqVersionIds = requirementDao.findIdsVersionsForAll(new ArrayList<>(reqIds));

        // 4. Get exportModel from database
        RequirementExportModel exportModel =
                requirementExportDao.findAllRequirementModel(reqVersionIds);

        // 5. Instantiate a fresh exporter, append model to excel file and return
        RequirementExcelExporter exporter = exporterProvider.get();
        exporter.appendToWorkbook(exportModel, keepRteFormat);
        return exporter.print();
    }

    @Override
    public File searchExportRequirementAsExcel(
            List<Long> nodeIds,
            boolean keepRteFormat,
            boolean addLinkedLowLevelReq,
            MessageSource messageSource,
            String type,
            Boolean simplifiedColumnDisplay) {
        Collection<Long> reqVersionIds =
                securityFilterIds(nodeIds, RequirementVersion.class.getName(), EXPORT);
        RequirementExportModel exportModel;
        File file;

        if (addLinkedLowLevelReq) {
            List<Long> exportableProjectIds = findExportableProjectIds();
            reqVersionIds.addAll(
                    highLevelRequirementDisplayDao
                            .findLinkedLowLevelReqVersionIdsByReqVersionIdsAndProjectIds(
                                    new ArrayList<>(reqVersionIds), exportableProjectIds));
        }

        if (type.equals(SIMPLE)) {
            exportModel =
                    requirementExportDao.populateRequirementExportModelFromJooq(
                            new ArrayList<>(reqVersionIds));
            SearchSimpleRequirementExcelExporter exporter = searchSimpleExporterProvider.get();
            exporter.createHeaders(simplifiedColumnDisplay);
            exporter.getMessageSource(messageSource);
            exporter.appendToWorkbook(exportModel, keepRteFormat, simplifiedColumnDisplay);
            file = exporter.print();
        } else {
            exportModel = requirementExportDao.findAllRequirementModel(new ArrayList<>(reqVersionIds));
            SearchRequirementExcelExporter exporter = searchExporterProvider.get();
            exporter.appendToWorkbook(exportModel, keepRteFormat);
            file = exporter.print();
        }

        return file;
    }

    @Override
    public ImportLog simulateImportExcelRequirement(File xls) {
        xlsImportLimitationHandler.checkMaxNumberOfRequirementsInsideXlsFile(xls);
        return batchImporter.simulateImport(xls);
    }

    @Override
    public ImportLog importExcelRequirement(File xls) {
        try {
            xlsImportLimitationHandler.incrementImportProcessCounter();
            xlsImportLimitationHandler.checkMaxNumberOfRequirementsInsideXlsFile(xls);
            xlsImportLimitationHandler.checkIfImportSlotIsAvailable();
            return batchImporter.performImport(xls);
        } finally {
            xlsImportLimitationHandler.decrementImportProcessCounter();
        }
    }

    @Override
    public List<Long> findNodeIdsByPath(List<String> paths) {
        return requirementLibraryNodeDao.findNodeIdsByPath(paths);
    }

    @Override
    public Long findNodeIdByPath(String path) {
        return StringUtils.isBlank(path) ? null : requirementLibraryNodeDao.findNodeIdByPath(path);
    }

    @Override
    public Long findNodeIdByRemoteKey(String remoteKey, String projectName) {
        return requirementDao.findNodeIdByRemoteKey(remoteKey, projectName);
    }

    @Override
    public Long findNodeIdByRemoteKeyAndSynchronisationId(String remoteKey, Long remoteSyncId) {
        return requirementDao.findNodeIdByRemoteKeyAndRemoteSyncId(remoteKey, remoteSyncId);
    }

    @Override
    public List<Long> findNodeIdsByRemoteKeys(List<String> remoteKeys, String projectName) {
        Map<String, Long> idsByKeys = requirementDao.findNodeIdsByRemoteKeys(remoteKeys, projectName);

        List<Long> res = new ArrayList<>(remoteKeys.size());

        for (String key : remoteKeys) {
            Long id = idsByKeys.get(key);
            res.add(id);
        }

        return res;
    }

    @Override
    public Collection<Long> findRequirementIdsFromSelection(
            Collection<Long> libraryIds, Collection<Long> nodeIds) {
        return findRequirementIdsFromSelection(libraryIds, nodeIds, false);
    }

    @Override
    public Collection<Long> findRequirementIdsFromSelection(
            Collection<Long> libraryIds, Collection<Long> nodeIds, boolean isExtendedHighLvlReqScope) {

        /*
         *  first, let's check the permissions on those root nodes
         *  By transitivity, if the user can read them then it will
         *  be allowed to read the requirements below
         */
        Collection<Long> readLibIds =
                securityFilterIds(libraryIds, RequirementLibrary.class.getName(), "READ");
        Collection<Long> readNodeIds =
                securityFilterIds(nodeIds, RequirementLibraryNode.class.getName(), "READ");

        // now we can collect the requirements
        Set<Long> reqIds = new HashSet<>();

        if (!readLibIds.isEmpty()) {
            reqIds.addAll(
                    highLevelRequirementDisplayDao.findRequirementIdsByLibraryIds(
                            readLibIds, isExtendedHighLvlReqScope));
        }
        if (!readNodeIds.isEmpty()) {
            reqIds.addAll(
                    highLevelRequirementDisplayDao.findRequirementIdsByNodeIds(
                            readNodeIds, isExtendedHighLvlReqScope));
        }

        // return
        return reqIds;
    }

    @Override
    public Long mkdirs(String folderpath) {
        List<String> paths = PathUtils.scanPath(folderpath);
        String[] splits = PathUtils.splitPath(folderpath);
        Project project = projectDao.findByName(PathUtils.unescapePathPartSlashes(splits[0]));

        if (splits.length < 2) {
            throw new IllegalArgumentException(
                    "Folder path for mkdir must contains at least a valid /projectName/folder");
        }

        if (project == null) {
            throw new IllegalArgumentException("Folder path for mkdir must concern an existing project");
        }

        List<Long> ids = findNodeIdsByPath(paths);
        RequirementFolder folderTree;

        int position = ids.indexOf(null);

        switch (position) {
            case -1: // no null value so all node exists, returning ids of the last folder
                return ids.get(ids.size() - 1);
            case 0: // no member of the path exists, we must create the hierachy under the Requirement
                // librairy
                folderTree = makeFolderTree(project, 1, splits);
                addFolderToLibrary(project.getRequirementLibrary().getId(), folderTree);
                break;
            default: // Something already exists... requirement or folder ?
                return createReqOrFolderIfSomethingAlreadyExists(splits, project, ids, position);
        }

        // now get the last folder on path and return id
        RequirementFolder lastfolder = folderTree;

        while (lastfolder.hasContent()) {
            lastfolder = (RequirementFolder) lastfolder.getContent().get(0);
        }

        return lastfolder.getId();
    }

    private Long createReqOrFolderIfSomethingAlreadyExists(
            String[] splits, Project project, List<Long> ids, int position) {
        Requirement requirement = findRequirement(ids.get(position - 1));
        if (requirement == null) {
            return createFolderTree(project, position, ids.get(position - 1), splits);
        } else {
            return createRequirementTree(
                    project, position, ids.get(position - 1), splits, requirement.isHighLevel());
        }
    }

    private List<Long> findExportableProjectIds() {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        return projectFinder.findAllExportableIdsOnGivenLibrary(
                currentUser, RequirementLibrary.class.getName(), PROJECT.RL_ID);
    }

    private Long createRequirementTree(
            Project project, int position, Long idBaseRequirement, String[] splits, boolean isHighLevel) {
        Requirement requirementTree = makeRequirementTree(project, position + 1, splits);
        List<Long> emptyIds = Collections.emptyList();
        if (isHighLevel) {
            addRequirementToHighLevelRequirement(idBaseRequirement, requirementTree, emptyIds);
        } else {
            addRequirementToRequirement(idBaseRequirement, requirementTree, emptyIds);
        }

        // now get the last requirement on path and return his id

        Requirement lastRequirement = requirementTree;

        while (lastRequirement.hasContent()) {
            lastRequirement = lastRequirement.getContent().get(0);
        }

        return lastRequirement.getId();
    }

    private Long createFolderTree(Project project, int position, Long idBaseFolder, String[] splits) {
        RequirementFolder folderTree = makeFolderTree(project, position + 1, splits);
        addFolderToFolder(idBaseFolder, folderTree);

        // now get the last folder on path and return his id

        RequirementFolder lastfolder = folderTree;

        while (lastfolder.hasContent()) {
            lastfolder = (RequirementFolder) lastfolder.getContent().get(0);
        }

        return lastfolder.getId();
    }

    private RequirementFolder makeFolderTree(Project project, int startIndex, String[] names) {
        RequirementFolder baseFolder = null;
        RequirementFolder childFolder;
        RequirementFolder parentFolder = null;

        for (int i = startIndex; i < names.length; i++) {
            childFolder = new RequirementFolder();
            childFolder.setName(PathUtils.unescapePathPartSlashes(names[i]));
            childFolder.setDescription("");
            childFolder.notifyAssociatedWithProject(project);
            if (baseFolder
                    == null) { // if we have no folder yet, we are creating the base, which will be also the
                // first parent
                baseFolder = childFolder;
            } else {
                parentFolder.addContent(childFolder);
            }
            parentFolder = childFolder;
        }

        return baseFolder;
    }

    private Requirement makeRequirementTree(Project project, int startIndex, String[] names) {
        Requirement baseRequirement = null;
        Requirement childRequirement;
        Requirement parentRequirement = null;

        for (int i = startIndex; i < names.length; i++) {
            childRequirement = new Requirement(new RequirementVersion());
            childRequirement.setName(PathUtils.unescapePathPartSlashes(names[i]));
            childRequirement.setDescription("");
            childRequirement.setCategory(
                    infoListItemService.findDefaultRequirementCategory(project.getId()));
            childRequirement.notifyAssociatedWithProject(project);
            if (baseRequirement
                    == null) { // if we have no folder yet, we are creating the base, which will be also the
                // first parent
                baseRequirement = childRequirement;
            } else {
                parentRequirement.addContent(childRequirement);
            }
            parentRequirement = childRequirement;
        }

        return baseRequirement;
    }

    @Override
    public void changeCurrentVersionNumber(Requirement requirement, Integer noVersion) {
        // if target noVersion = actual noVersion, nothing to change, return
        if (requirement.getCurrentVersion().getVersionNumber() == noVersion) {
            return;
        }
        if (requirement.findRequirementVersion(noVersion) == null) {
            RequirementVersion lastCreatedReqVersion = requirement.getCurrentVersion();
            lastCreatedReqVersion.setVersionNumber(noVersion);
            requirement.setCurrentVersion(requirement.findLastNonObsoleteVersionAfterImport());
        } else {
            throw new IllegalArgumentException(
                    "RequirementVersion with version number "
                            + noVersion
                            + " already exist in this Requirement, id : "
                            + requirement.getId());
        }
    }

    @Override
    public void initCUFvalues(
            RequirementVersion reqVersion, Map<Long, RawValue> initialCustomFieldValues) {
        customFieldValueService.initCustomFieldValues(reqVersion, initialCustomFieldValues);
    }

    @Override
    public RequirementLibraryNode findRequirementLibraryNodeById(Long id) {
        return requirementLibraryNodeDao.findById(id);
    }

    @Override
    @PreAuthorize(READ_REQUIREMENT_LIBRARY_NODE_OR_ROLE_ADMIN)
    public List<String> findNamesInNodeStartingWith(long rlnId, String nameStart) {
        return requirementFolderDao.findNamesInNodeStartingWith(rlnId, nameStart);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.requirement.RequirementLibrary', 'READ')"
                    + OR_HAS_ROLE_ADMIN)
    public List<String> findNamesInLibraryStartingWith(long libraryId, String nameStart) {
        return requirementFolderDao.findNamesInLibraryStartingWith(libraryId, nameStart);
    }

    // ##################### PREVENT CONCURRENCY OVERRIDES ##########################

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = TARGET_ID,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToFolder(destinationId, targetId, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = TARGET_ID,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            int position,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToFolder(destinationId, targetId, position, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = TARGET_ID,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToLibrary(destinationId, targetId, clipboardPayload);
    }

    @Override
    public void moveNodesToLibrary(long destinationId, Long[] targetId) {
        super.moveNodesToLibrary(
                destinationId, targetId, ClipboardPayload.withWhiteListIgnored(Arrays.asList(targetId)));
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = RequirementLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = RLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = TARGET_ID,
                        coercer = RequirementLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            int position,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToLibrary(destinationId, targetId, position, clipboardPayload);
    }

    @Override
    public void moveNodesToLibrary(long destinationId, Long[] targetId, int position) {
        super.moveNodesToLibrary(
                destinationId,
                targetId,
                position,
                ClipboardPayload.withWhiteListIgnored(Arrays.asList(targetId)));
    }

    @Override
    @PreventConcurrents(
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = RequirementLibraryNode.class,
                        paramName = "targetIds",
                        coercer = RLNAndParentIdsCoercerForList.class),
                @BatchPreventConcurrent(
                        entityType = RequirementLibrary.class,
                        paramName = "targetIds",
                        coercer = RequirementLibraryIdsCoercerForList.class)
            })
    @CheckBlockingMilestones(entityType = Requirement.class)
    public OperationReport deleteNodes(@Ids("targetIds") List<Long> targetIds) {
        return super.deleteNodes(targetIds);
    }

    // ##################### PREVENT CONCURENCY OVERRIDES ##########################

    @Override
    public RequirementStatisticsBundle getStatisticsForSelection(
            Collection<Long> libraryIds, Collection<Long> nodeIds) {

        return getStatisticsForSelection(libraryIds, nodeIds, false);
    }

    @Override
    public RequirementStatisticsBundle getStatisticsForSelection(
            Collection<Long> libraryIds, Collection<Long> nodeIds, boolean isExtendedHighLvlReqScope) {

        Collection<Long> reqIds =
                findRequirementIdsFromSelection(libraryIds, nodeIds, isExtendedHighLvlReqScope);

        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        if (activeMilestone.isPresent()) {
            return statisticsService.gatherRequirementStatisticsBundleForActiveMilestone(
                    reqIds, activeMilestone.get());
        }

        return statisticsService.gatherRequirementStatisticsBundle(reqIds);
    }

    @Override
    public List<Long> findAllRequirementIdsInMilestone(Milestone activeMilestone) {
        if (activeMilestone != null) {
            List<Long> milestoneIds = new ArrayList<>();
            milestoneIds.add(activeMilestone.getId());
            return requirementDao.findAllRequirementIdsFromMilestones(milestoneIds);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Requirement findRequirement(Long nodeId) {
        return requirementDao.findById(nodeId);
    }

    @Override
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    public void validatePathForSync(String projectName, String synchronisationPath)
            throws RequiredFieldException, PathValidationDomainException, PathAlreadyInUseException {
        if (StringUtils.isBlank(synchronisationPath)) {
            throw new RequiredFieldException("path");
        }

        if (!PathUtils.isPathSyntaxValid(synchronisationPath)) {
            throw new PathValidationDomainException(SYNCHRONISATION_PATH);
        }

        remoteSynchronisationService.checkPathAvailability(projectName, synchronisationPath);

        final String fullSynchronisationPath =
                PathUtils.appendPathToProjectName(projectName, synchronisationPath);
        final Long id = findNodeIdByPath(fullSynchronisationPath);

        if (id != null) {
            throw new PathAlreadyInUseException(SYNCHRONISATION_PATH);
        }
    }

    @Override
    public List<String> findContentNamesByLibraryId(Long libraryId) {
        return requirementLibraryDao.findContentNamesByLibraryId(libraryId);
    }

    @Override
    public Map<Long, List<String>> findContentNamesByNodeIds(Collection<Long> folderIds) {
        if (folderIds.isEmpty()) {
            return Collections.emptyMap();
        }

        return requirementLibraryNodeDao.findContentNames(folderIds);
    }
}
