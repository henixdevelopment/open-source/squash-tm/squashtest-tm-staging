/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.milestone.MilestoneStatus.MILESTONE_BLOCKING_STATUSES;
import static org.squashtest.tm.domain.project.AutomationWorkflowType.NONE;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.jenkins;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.squashOrchestrator;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;
import static org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus.AUTOMATED;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_TO_BLOCKING_MILESTONE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_LATEST_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LATEST_EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_LABELS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MAX_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MIN_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_PLAN_ITEM_ID;

import java.sql.Timestamp;
import java.util.Objects;
import org.apache.commons.lang3.NotImplementedException;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.SelectHavingStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;

public abstract class AbstractTestPlanGrid<R extends Record> extends AbstractGrid {
    protected static final String MAX_EXECUTION_ORDER = "MAX_EXECUTION_ORDER";

    protected String userLoginToRestrictTo;

    protected AbstractTestPlanGrid(String userLoginToRestrictTo) {
        this.userLoginToRestrictTo = userLoginToRestrictTo;
    }

    protected abstract Table<R> getItemTable();

    protected abstract TableField<R, Long> getItemIdColumn();

    protected abstract TableField<R, Long> getAssigneeIdColumn();

    protected abstract TableField<R, Long> getTestCaseIdColumn();

    protected SelectOnConditionStep<Record5<Long, Long, String, String, Timestamp>>
            findItpiWithLatestExecution() {
        // ITEM_ID | MAX_EXECUTION_ORDER
        SelectHavingStep<?> findLastExecutionByOrder = findLastExecutionByOrder();

        return DSL.select(
                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.as(ITEM_ID),
                        ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.as(LATEST_EXECUTION_ID),
                        EXECUTION.EXECUTION_MODE.as(LAST_EXECUTION_MODE),
                        EXECUTION.EXECUTION_STATUS.as(EXECUTION_LATEST_STATUS),
                        EXECUTION.LAST_EXECUTED_ON.as(EXECUTION_LAST_EXECUTED_ON))
                .from(ITEM_TEST_PLAN_EXECUTION)
                .innerJoin(findLastExecutionByOrder)
                .on(
                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                findLastExecutionByOrder.field(ITEM_ID, Long.class)))
                .and(
                        ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.eq(
                                findLastExecutionByOrder.field(MAX_EXECUTION_ORDER, Integer.class)))
                .innerJoin(EXECUTION)
                .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID));
    }

    protected SelectHavingStep<Record5<Long, Long, String, String, Timestamp>>
            selectFromLatestExecutionForTestPlanItem() {
        SelectHavingStep<Record1<Timestamp>> selectLatestExecutedOn =
                DSL.select(DSL.max(EXECUTION.LAST_EXECUTED_ON))
                        .from(EXECUTION)
                        .where(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID));

        return DSL.select(
                        EXECUTION.TEST_PLAN_ITEM_ID.as(TEST_PLAN_ITEM_ID),
                        EXECUTION.EXECUTION_ID.as(LATEST_EXECUTION_ID),
                        EXECUTION.EXECUTION_MODE.as(LAST_EXECUTION_MODE),
                        EXECUTION.EXECUTION_STATUS.as(EXECUTION_LATEST_STATUS),
                        EXECUTION.LAST_EXECUTED_ON.as(EXECUTION_LAST_EXECUTED_ON))
                .from(EXECUTION)
                .innerJoin(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXECUTION.TEST_PLAN_ITEM_ID))
                .where(EXECUTION.LAST_EXECUTED_ON.eq(selectLatestExecutedOn));
    }

    protected SelectHavingStep<Record2<Long, Integer>> findLastExecutionByOrder() {
        throw new NotImplementedException("This method should be implemented in subclasses");
    }

    protected SelectHavingStep<Record> getUser() {
        // ITEM_ID | USER
        return DSL.select(
                        getItemIdColumn().as(ITEM_ID),
                        CORE_USER.LOGIN.as(ASSIGNEE_LOGIN),
                        TestPlanGridHelpers.formatAssigneeFullName())
                .from(getItemTable())
                .innerJoin(CORE_USER)
                .on(getAssigneeIdColumn().eq(CORE_USER.PARTY_ID));
    }

    protected SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
        // ITEM_ID | MILESTONE_LABELS | MILESTONE_MIN_DATE | MILESTONE_MAX_DATE
        return DSL.select(
                        getItemIdColumn().as(ITEM_ID),
                        DSL.min(MILESTONE.END_DATE).as(MILESTONE_MIN_DATE),
                        DSL.max(MILESTONE.END_DATE).as(MILESTONE_MAX_DATE),
                        DSL.listAgg(MILESTONE.LABEL, ", ")
                                .withinGroupOrderBy(MILESTONE.END_DATE.asc())
                                .as(MILESTONE_LABELS))
                .from(getItemTable())
                .innerJoin(MILESTONE_TEST_CASE)
                .on(getTestCaseIdColumn().eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
                .innerJoin(MILESTONE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
                .groupBy(getItemIdColumn(), MILESTONE_TEST_CASE.TEST_CASE_ID);
    }

    /* Get ITPI ExecutionMode from TestCase.  */
    protected SelectHavingStep<Record2<Long, String>> getAutomationFields() {
        return DSL.select(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ITEM_ID),
                        when(
                                        TEST_CASE.EXECUTION_MODE.eq(TestCaseExecutionMode.EXPLORATORY.name()),
                                        TestCaseExecutionMode.EXPLORATORY.name())
                                .otherwise(
                                        when(
                                                        (PROJECT
                                                                        .AUTOMATION_WORKFLOW_TYPE
                                                                        .eq(NONE.name())
                                                                        .or(
                                                                                PROJECT
                                                                                        .AUTOMATION_WORKFLOW_TYPE
                                                                                        .ne(NONE.name())
                                                                                        .and(TEST_CASE.AUTOMATABLE.eq(Y.name()))
                                                                                        .and(
                                                                                                AUTOMATION_REQUEST.REQUEST_STATUS.eq(
                                                                                                        AUTOMATED.name()))))
                                                                // if Jenkins server, TestCase only has to be linked to an
                                                                // automation test
                                                                .and(
                                                                        (TEST_AUTOMATION_SERVER
                                                                                        .KIND
                                                                                        .eq(jenkins.name())
                                                                                        .and(TEST_CASE.TA_TEST.isNotNull()))
                                                                                // if Squash Autom server, then the 3 automation attributes
                                                                                // must exist
                                                                                .or(
                                                                                        TEST_AUTOMATION_SERVER
                                                                                                .KIND
                                                                                                .eq(squashOrchestrator.name())
                                                                                                .and(
                                                                                                        TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
                                                                                                .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
                                                                                                .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull()))),
                                                        TestCaseExecutionMode.AUTOMATED.name())
                                                .otherwise(TestCaseExecutionMode.MANUAL.name()))
                                .as(INFERRED_EXECUTION_MODE))
                .from(ITERATION_TEST_PLAN_ITEM)
                .innerJoin(TEST_CASE)
                .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .leftJoin(TEST_AUTOMATION_SERVER)
                .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID));
    }

    protected SelectHavingStep<Record2<Long, String>> getTestCaseKind() {
        return DSL.select(
                        getItemIdColumn().as(ITEM_ID),
                        DSL.when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.GHERKIN.toString())
                                .when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.KEYWORD.toString())
                                .when(
                                        EXPLORATORY_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.EXPLORATORY.toString())
                                .otherwise(TestCaseKind.STANDARD.toString())
                                .as(KIND))
                .from(getItemTable())
                .innerJoin(TEST_CASE)
                .on(getTestCaseIdColumn().eq(TEST_CASE.TCLN_ID))
                .leftJoin(SCRIPTED_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                .leftJoin(KEYWORD_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                .leftJoin(EXPLORATORY_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                .groupBy(
                        getItemIdColumn(),
                        SCRIPTED_TEST_CASE.TCLN_ID,
                        KEYWORD_TEST_CASE.TCLN_ID,
                        EXPLORATORY_TEST_CASE.TCLN_ID);
    }

    protected Table<Record> getFilteredTableOnAssigneeIfNotExploratory(Table<Record> table) {
        if (userLoginToRestrictTo != null) {
            final Field<String> assigneeLoginField =
                    Objects.requireNonNull(
                            table.field(ASSIGNEE_LOGIN, String.class), "Assignee login field should not be null");

            return table
                    .where(assigneeLoginField.eq(userLoginToRestrictTo).or(this.isExploratoryRow()))
                    .asTable();
        } else {
            return table;
        }
    }

    // This condition gets overridden in CampaignTestPlanGrid as there are no overviews for campaigns
    protected Condition isExploratoryRow() {
        return EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.isNotNull();
    }

    protected SelectHavingStep<Record2<Long, Boolean>> getBoundToBlockingMilestone() {
        // ITEM_ID | BOUND_TO_BLOCKING_MILESTONE
        return DSL.select(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ITEM_ID),
                        DSL.field(countDistinct(MILESTONE.MILESTONE_ID).gt(0)).as(BOUND_TO_BLOCKING_MILESTONE))
                .from(ITERATION_TEST_PLAN_ITEM)
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                .innerJoin(MILESTONE_CAMPAIGN)
                .on(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
                .innerJoin(MILESTONE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_CAMPAIGN.MILESTONE_ID))
                .where(MILESTONE.STATUS.in(MILESTONE_BLOCKING_STATUSES))
                .groupBy(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);
    }
}
