/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.project;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_CLEARANCE_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.plugin.ConfigurablePlugin;
import org.squashtest.tm.api.plugin.PluginType;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.api.wizard.WorkspaceWizard;
import org.squashtest.tm.api.workspace.WorkspaceType;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.actionword.ActionWordTreeDefinition;
import org.squashtest.tm.domain.aiserver.AiServer;
import org.squashtest.tm.domain.bdd.BddImplementationTechnology;
import org.squashtest.tm.domain.bdd.BddScriptLanguage;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;
import org.squashtest.tm.domain.infolist.InfoList;
import org.squashtest.tm.domain.library.PluginReferencer;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.AdministrableProject;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.LibraryPluginBinding;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestLibrary;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.PartyProjectPermissionsBean;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.UnknownEntityException;
import org.squashtest.tm.exception.artificialintelligence.server.AiServerWasDeletedException;
import org.squashtest.tm.exception.project.LockedParameterException;
import org.squashtest.tm.exception.testautomation.DuplicateTMLabelException;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.bugtracker.CustomBugTrackerModificationService;
import org.squashtest.tm.service.customfield.CustomFieldBindingModificationService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.infolist.InfoListFinderService;
import org.squashtest.tm.service.internal.display.dto.party.UnboundPartiesResponse;
import org.squashtest.tm.service.internal.display.dto.party.UnboundParty;
import org.squashtest.tm.service.internal.repository.ActionWordLibraryNodeDao;
import org.squashtest.tm.service.internal.repository.AutomationRequestDao;
import org.squashtest.tm.service.internal.repository.BugTrackerDao;
import org.squashtest.tm.service.internal.repository.CustomReportLibraryNodeDao;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.InfoListDao;
import org.squashtest.tm.service.internal.repository.PartyDao;
import org.squashtest.tm.service.internal.repository.ProfileDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.ProjectTemplateDao;
import org.squashtest.tm.service.internal.repository.RemoteAutomationRequestExtenderDao;
import org.squashtest.tm.service.internal.repository.RemoteSynchronisationDao;
import org.squashtest.tm.service.internal.repository.RequirementFolderSyncExtenderDao;
import org.squashtest.tm.service.internal.repository.RequirementSyncExtenderDao;
import org.squashtest.tm.service.internal.repository.SprintDao;
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao;
import org.squashtest.tm.service.internal.repository.SprintRequirementSyncExtenderDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateRequirementDao;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateSprintGroupDao;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.project.CustomGenericProjectFinder;
import org.squashtest.tm.service.project.CustomGenericProjectManager;
import org.squashtest.tm.service.project.GenericProjectCopyParameter;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.security.ObjectIdentityService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginService;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.service.testautomation.TestAutomationServerManagerService;
import org.squashtest.tm.service.testcase.CustomTestCaseModificationService;

@Service("CustomGenericProjectManager")
@Transactional
public class CustomGenericProjectManagerImpl implements CustomGenericProjectManager {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(CustomGenericProjectManagerImpl.class);

    @Inject private GenericProjectDao genericProjectDao;
    @Inject private ProjectDao projectDao;
    @Inject private ProjectTemplateDao templateDao;
    @Inject private BugTrackerDao bugTrackerDao;
    @PersistenceContext private EntityManager em;
    @Inject private RemoteSynchronisationDao remoteSynchronisationDao;
    @Inject private PartyDao partyDao;
    @Inject private ExecutionDao executionDao;
    @Inject private ObjectIdentityService objectIdentityService;
    @Inject private Provider<GenericToAdministrableProject> genericToAdministrableConvertor;
    @Inject private ProjectsPermissionManagementService permissionsManager;
    @Inject private PermissionEvaluationService permissionEvaluationService;
    @Inject private ProjectDeletionHandler projectDeletionHandler;
    @Inject private ExecutionProcessingService execProcessing;
    @Inject private TestAutomationServerManagerService taServerService;
    @Inject private TestAutomationProjectManagerService taProjectService;
    @Inject private InfoListFinderService infoListService;
    @Inject private CustomFieldBindingModificationService customFieldBindingModificationService;
    @Inject private CustomReportLibraryNodeDao customReportLibraryNodeDao;
    @Inject private ActionWordLibraryNodeDao actionWordLibraryNodeDao;
    @Inject private TestCaseDao testCaseDao;
    @Inject private CustomTestCaseModificationService customTestCaseModificationService;
    @Inject private RequirementFolderSyncExtenderDao requirementFolderSyncExtenderDao;
    @Inject private RequirementSyncExtenderDao requirementSyncExtenderDao;
    @Inject private SprintRequirementSyncExtenderDao sprintReqSyncExtenderDao;
    @Inject private SprintDao sprintDao;
    @Inject private HibernateRequirementDao hibernateRequirementDao;
    @Inject private HibernateSprintGroupDao hibernateSprintGroupDao;
    @Inject private SprintReqVersionDao sprintReqVersionDao;
    @Inject private AutomationRequestDao automationRequestDao;
    @Inject private GenericProjectManagerService projectManager;
    @Inject private RemoteAutomationRequestExtenderDao remoteAutomationRequestExtenderDao;
    @Inject private TemplateConfigurablePluginService templateConfigurablePluginService;
    @Inject private TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService;
    @Inject private InfoListDao infoListDao;
    @Inject private TestCaseLoader testCaseLoader;
    @Inject private CustomBugTrackerModificationService customBugTrackerModificationService;
    @Inject private AttachmentManagerService attachmentManagerService;
    @Inject private EnvironmentVariableBindingService environmentVariableBindingService;
    @Inject private UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService;
    @Inject private ProfileDao profileDao;

    @Autowired(required = false)
    Collection<WorkspaceWizard> plugins = Collections.emptyList();

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public GenericProject persist(GenericProject project) {

        // plug-in the default info lists
        assignDefaultInfolistToProject(project);

        if (genericProjectDao.countByName(project.getName()) > 0) {
            throw new NameAlreadyInUseException(project.getClass().getSimpleName(), project.getName());
        }

        CampaignLibrary cl = new CampaignLibrary();
        project.setCampaignLibrary(cl);
        em.persist(cl);

        RequirementLibrary rl = new RequirementLibrary();
        project.setRequirementLibrary(rl);
        em.persist(rl);

        TestCaseLibrary tcl = new TestCaseLibrary();
        project.setTestCaseLibrary(tcl);
        em.persist(tcl);

        CustomReportLibrary crl = new CustomReportLibrary();
        project.setCustomReportLibrary(crl);
        em.persist(crl);

        // add the tree node for the CustomReportLibrary as for custom report workspace library
        // object and their representation in tree are distinct entities
        CustomReportLibraryNode crlNode =
                new CustomReportLibraryNode(
                        CustomReportTreeDefinition.LIBRARY, crl.getId(), project.getName(), crl);
        crlNode.setEntity(crl);
        em.persist(crlNode);

        AutomationRequestLibrary arl = new AutomationRequestLibrary();
        project.setAutomationRequestLibrary(arl);
        em.persist(arl);

        ActionWordLibrary awl = new ActionWordLibrary();
        project.setActionWordLibrary(awl);
        em.persist(awl);

        // add tree node for the ActionWordLibrary
        ActionWordLibraryNode awlNode =
                new ActionWordLibraryNode(
                        ActionWordTreeDefinition.LIBRARY, awl.getId(), project.getName(), awl);
        awlNode.setEntity(awl);
        em.persist(awlNode);

        // now persist it
        em.persist(project);
        em.flush(); // otherwise ids not available

        objectIdentityService.addObjectIdentity(project.getId(), project.getClass());
        objectIdentityService.addObjectIdentity(tcl.getId(), tcl.getClass());
        objectIdentityService.addObjectIdentity(rl.getId(), rl.getClass());
        objectIdentityService.addObjectIdentity(cl.getId(), cl.getClass());
        objectIdentityService.addObjectIdentity(crl.getId(), crl.getClass());
        objectIdentityService.addObjectIdentity(arl.getId(), arl.getClass());
        objectIdentityService.addObjectIdentity(awl.getId(), awl.getClass());

        return project;
    }

    private void assignDefaultInfolistToProject(GenericProject project) {
        InfoList defaultCategories = infoListService.findByCode("DEF_REQ_CAT");
        project.setRequirementCategories(defaultCategories);

        InfoList defaultNatures = infoListService.findByCode("DEF_TC_NAT");
        project.setTestCaseNatures(defaultNatures);

        InfoList defaultTypes = infoListService.findByCode("DEF_TC_TYP");
        project.setTestCaseTypes(defaultTypes);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void coerceProjectIntoTemplate(long projectId) {
        projectDeletionHandler.checkProjectContainsOnlyFolders(projectId);
        projectDeletionHandler.deleteAllLibrariesContent(projectId);

        Project project = projectDao.getReferenceById(projectId);

        projectDeletionHandler.removeProjectFromFilters(project);
        templateConfigurablePluginBindingService.removeAllForGenericProject(project.getId());
        deleteAllSync(projectId);

        ProjectTemplate template = genericProjectDao.coerceProjectIntoTemplate(projectId);

        objectIdentityService.addObjectIdentity(projectId, ProjectTemplate.class);
        permissionsManager.copyAssignedUsersFromProjectToTemplate(template, projectId);
        permissionsManager.removeAllPermissionsFromProject(projectId);
        objectIdentityService.removeObjectIdentity(projectId, Project.class);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void associateToTemplate(
            long projectId, long templateId, List<String> boundTemplatePlugins) {
        Project project = projectDao.getReferenceById(projectId);
        ProjectTemplate template = templateDao.getReferenceById(templateId);
        project.setTemplate(template);
        synchronizeProjectFromTemplate(project, template, boundTemplatePlugins);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void disassociateFromTemplate(long projectId) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        genericProject.setTemplate(null);
        templateConfigurablePluginBindingService.removeAllForGenericProject(projectId);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public AdministrableProject findAdministrableProjectById(long projectId) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        return genericToAdministrableConvertor.get().convertToAdministrableProject(genericProject);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_CLEARANCE_OR_ROLE_ADMIN)
    public void addNewPermissionToProject(List<Long> userIds, long projectId, String permission) {
        if (!ultimateLicenseAvailabilityService.isAvailable() && !AclGroup.isSystem(permission)) {
            throw new IllegalArgumentException(
                    String.format("%s %s %s", "Profile", permission, "is not a system profile."));
        }

        userIds.forEach(partyId -> addNewPermissionToProject(partyId, projectId, permission));
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_CLEARANCE_OR_ROLE_ADMIN)
    public void addNewPermissionToProject(List<Long> userIds, long projectId, long profileId) {
        if (!ultimateLicenseAvailabilityService.isAvailable()) {
            Optional<AclGroup> optionalProfile = profileDao.findById(profileId);
            if (optionalProfile.isPresent() && !optionalProfile.get().isSystem()) {
                throw new IllegalArgumentException(
                        String.format(
                                "%s %s %s",
                                "Profile", optionalProfile.get().getQualifiedName(), "is not a system profile."));
            }
        }
        userIds.forEach(partyId -> addNewPermissionToProject(partyId, projectId, profileId));
    }

    private void addNewPermissionToProject(long userId, long projectId, String permission) {
        permissionsManager.addNewPermissionToProject(userId, projectId, permission);
    }

    private void addNewPermissionToProject(long userId, long projectId, long profileId) {
        permissionsManager.addNewPermissionToProject(userId, projectId, profileId);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_CLEARANCE_OR_ROLE_ADMIN)
    public void removeProjectPermission(List<Long> userIds, long projectId) {
        userIds.forEach(userId -> removeProjectPermission(userId, projectId));
    }

    private void removeProjectPermission(long userId, long projectId) {
        permissionsManager.removeProjectPermission(userId, projectId);
    }

    @Override
    public List<PartyProjectPermissionsBean> findPartyPermissionsBeansByProject(long projectId) {
        return permissionsManager.findPartyPermissionsBeanByProject(projectId);
    }

    @Override
    public List<AclGroup> findAllPossiblePermission() {
        return permissionsManager.findAllPossiblePermission();
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_CLEARANCE_OR_ROLE_ADMIN)
    public UnboundPartiesResponse findPartyWithoutPermissionByProject(long projectId) {
        List<Party> partyList = permissionsManager.findPartyWithoutPermissionByProject(projectId);

        List<UnboundParty> users = new ArrayList<>();
        List<UnboundParty> teams = new ArrayList<>();

        for (Party p : partyList) {
            final boolean isUser = User.class.isAssignableFrom(p.getClass());
            final Long id = p.getId();
            final String label = HTMLCleanupUtils.cleanAndUnescapeHTML(p.getName());
            final UnboundParty unboundParty = new UnboundParty(id, label);

            if (isUser) {
                users.add(unboundParty);
            } else {
                teams.add(unboundParty);
            }
        }

        return new UnboundPartiesResponse(users, teams);
    }

    @Override
    public Party findPartyById(long partyId) {
        return partyDao.getReferenceById(partyId);
    }

    // ********************************** Test automation section
    // *************************************

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void bindTestAutomationServer(long projectId, Long serverId) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);

        // Remove all TA projects
        taProjectService.deleteAllForTMProject(projectId);

        // Remove all project environment tags and use server tags inheritance
        clearEnvironmentTagOverrides(projectId);

        TestAutomationServer taServer = null;
        if (serverId != null) {
            taServer = taServerService.findById(serverId);
            bindServerEnvironmentVariablesToProject(projectId, serverId);
        }

        genericProject.setTestAutomationServer(taServer);
    }

    private void bindServerEnvironmentVariablesToProject(Long projectId, Long taServerId) {
        environmentVariableBindingService.bindServerEnvironmentVariablesToProject(
                taServerId, projectId);
    }

    @Override
    public void bindTestAutomationProject(long projectId, TestAutomationProject taProject) {

        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        bindTestAutomationProject(taProject, genericProject);
    }

    private void bindTestAutomationProject(
            TestAutomationProject taProject, GenericProject genericProject) {
        TestAutomationServer server = genericProject.getTestAutomationServer();
        taProject.setServer(server);
        taProject.setTmProject(genericProject);

        taProjectService.persist(taProject);

        genericProject.bindTestAutomationProject(taProject);

        if (taProject.isCanRunGherkin()) {
            taProjectService.setUniqueBddProject(taProject.getId());
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void bindTestAutomationProjects(
            long projectId, Collection<TestAutomationProject> taProjects) {
        checkTAProjectNames(taProjects, projectId);
        for (TestAutomationProject p : taProjects) {
            bindTestAutomationProject(projectId, p);
        }
    }

    private void checkTAProjectNames(Collection<TestAutomationProject> taProjects, long projectId) {
        List<String> taProjectNames = genericProjectDao.findBoundTestAutomationProjectLabels(projectId);

        for (TestAutomationProject taProject : taProjects) {
            checkTAProjecTName(taProject, taProjectNames);
        }
    }

    private void checkTAProjecTName(TestAutomationProject taProject, List<String> projectNames) {
        if (projectNames.contains(taProject.getLabel())) {
            throw new DuplicateTMLabelException(taProject.getLabel());
        }
    }

    /**
     * @see CustomGenericProjectFinder#findAllAvailableTaProjects(long)
     */
    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public Collection<TestAutomationProject> findAllAvailableTaProjects(long projectId) {
        TestAutomationServer server = genericProjectDao.findTestAutomationServer(projectId);
        if (server == null) {
            return Collections.emptyList();
        }
        Collection<TestAutomationProject> availableTaProjects =
                taProjectService.listProjectsOnServer(server);
        return filterAlreadyBoundProjects(projectId, availableTaProjects);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public Collection<TestAutomationProject> findAllAvailableTaProjectsWithUserLevelCredentials(
            long projectId) {
        TestAutomationServer server = genericProjectDao.findTestAutomationServer(projectId);
        if (server == null) {
            return Collections.emptyList();
        }
        Collection<TestAutomationProject> availableTaProjects =
                taProjectService.listProjectsOnServerForCurrentUser(server);
        return filterAlreadyBoundProjects(projectId, availableTaProjects);
    }

    @Override
    public List<String> getProjectEnvironmentTags(long genericProjectId) {
        return genericProjectDao.getEnvironmentTags(genericProjectId);
    }

    @Override
    public boolean isInheritsEnvironmentTags(long genericProjectId) {
        return genericProjectDao.isInheritsEnvironmentTags(genericProjectId);
    }

    private Collection<TestAutomationProject> filterAlreadyBoundProjects(
            long projectId, Collection<TestAutomationProject> availableTaProjects) {
        Collection<String> alreadyBoundProjectsJobNames =
                genericProjectDao.findBoundTestAutomationProjectJobNames(projectId);

        Iterator<TestAutomationProject> it = availableTaProjects.iterator();
        while (it.hasNext()) {
            TestAutomationProject taProject = it.next();
            if (alreadyBoundProjectsJobNames.contains(taProject.getJobName())) {
                it.remove();
            }
        }
        return availableTaProjects;
    }

    /* ----- Scm Repository Section----- */

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void bindScmRepository(long projectId, long scmRepositoryId) {
        genericProjectDao.bindScmRepository(projectId, scmRepositoryId);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void unbindScmRepository(long projectId) {
        genericProjectDao.unbindScmRepository(projectId);
    }

    // ********************************** bugtracker section
    // *************************************

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeBugTracker(long projectId, Long newBugtrackerId) {
        GenericProject project = genericProjectDao.getProjectWithBugtrackerProjects(projectId);
        BugTracker newBugtracker =
                bugTrackerDao
                        .findById(newBugtrackerId)
                        .orElseThrow(() -> new UnknownEntityException(newBugtrackerId, BugTracker.class));
        changeBugTracker(project, newBugtracker);
    }

    private void changeBugTracker(GenericProject project, BugTracker newBugtracker) {
        LOGGER.debug(
                "Change bugTracker for project {} bugtracker id: {}",
                project.getId(),
                newBugtracker.getId());
        if (!project.isBoundToBugtracker()) {
            project.bindBugtracker(newBugtracker);
        } else {
            Long newBugtrackerId = newBugtracker.getId();
            Long oldBugtrackerId = project.getBugTracker().getId();
            if (!newBugtrackerId.equals(oldBugtrackerId)) {
                project.bindBugtracker(newBugtracker);
            }
        }

        // Update reporting cache
        customBugTrackerModificationService.refreshCacheForProject(project.getId());
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void removeBugTracker(long projectId) {
        LOGGER.debug("Remove bugTracker projects for project id {}", projectId);
        GenericProject project = genericProjectDao.getProjectWithBugtrackerProjects(projectId);
        project.unbindBugtracker();
    }

    // **************************** plugin section
    // **********************************

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void enablePlugin(long projectId, @NotNull ConfigurablePlugin plugin) {
        plugin
                .getWorkspaces()
                .forEach(
                        workspace ->
                                enablePluginForWorkspace(
                                        projectId, workspace, plugin.getId(), plugin.getPluginType()));
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void enablePluginForWorkspace(
            long projectId, WorkspaceType workspace, String pluginId, PluginType pluginType) {
        final PluginReferencer<?> library = findLibrary(projectId, workspace);
        final LibraryPluginBinding binding = library.getPluginBinding(pluginId);

        if (binding != null) {
            binding.setActive(true);
            enableAllRemoteSynchronisations(projectId, pluginId);
        } else {
            library.enablePlugin(pluginId);
            final LibraryPluginBinding newBinding = library.getPluginBinding(pluginId);

            if (pluginType != null) {
                newBinding.setPluginType(pluginType);
            }
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void disablePluginAndRemoveConfiguration(
            long projectId, List<WorkspaceType> workspaces, String pluginId) {
        for (WorkspaceType workspace : workspaces) {
            PluginReferencer<?> library = findLibrary(projectId, workspace);
            library.disablePlugin(pluginId);
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void removeSynchronisations(long projectId, String pluginId) {
        List<RemoteSynchronisation> listRemoteSync =
                remoteSynchronisationDao.findByProjectIdAndKind(projectId, pluginId);
        List<Long> syncIds = listRemoteSync.stream().map(RemoteSynchronisation::getId).toList();

        if (!syncIds.isEmpty()) {
            removeSynchronisationsByIds(syncIds);
        }

        listRemoteSync.forEach(remoteSync -> remoteSynchronisationDao.delete(remoteSync));
    }

    @Override
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    public void removeSynchronisationsByIds(List<Long> syncIds) {
        checkPermissionOnSyncIds(syncIds);
        hibernateRequirementDao.setNativeManagementModeForSynchronizedRequirements(syncIds);
        requirementFolderSyncExtenderDao.deleteByRemoteSynchronisationId(syncIds);
        requirementSyncExtenderDao.deleteByRemoteSynchronisationId(syncIds);
        removeSprintSynchronisationBySyncIds(syncIds);
        remoteSynchronisationDao.deleteAllById(syncIds);
    }

    @Override
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    public void removeSprintSynchronisationBySyncIds(List<Long> syncIds) {
        checkPermissionOnSyncIds(syncIds);
        sprintReqVersionDao.setAllSprintReqVersionToNativeByRemoteSyncIds(syncIds);
        hibernateSprintGroupDao.setRemoteSyncIdsToNull(syncIds);
        sprintDao.deleteRemoteSynchronisationFromSprint(syncIds);
        sprintReqSyncExtenderDao.deleteByRemoteSynchronisationIds(syncIds);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void disablePluginAndKeepConfiguration(
            long projectId, List<WorkspaceType> workspaces, String pluginId) {
        for (WorkspaceType workspace : workspaces) {
            PluginReferencer<?> library = findLibrary(projectId, workspace);
            LibraryPluginBinding binding = library.getPluginBinding(pluginId);

            if (binding != null) {
                binding.setActive(false);
            }
            // remoteSynchronisation

        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void disableSynchronisations(long projectId, String pluginId) {
        List<RemoteSynchronisation> listRemoteSync =
                remoteSynchronisationDao.findByProjectIdAndKind(projectId, pluginId);
        listRemoteSync.forEach(remoteSync -> remoteSync.setSynchronisationEnable(false));
    }

    @Override
    public boolean hasProjectRemoteSynchronisation(long projectId) {
        return !remoteSynchronisationDao.findByProjectId(projectId).isEmpty();
    }

    @Override
    public void deleteAllSync(long projectId) {
        List<RemoteSynchronisation> rmList = remoteSynchronisationDao.findByProjectId(projectId);
        List<Long> ids = rmList.stream().map(RemoteSynchronisation::getId).toList();

        if (!ids.isEmpty()) {
            hibernateRequirementDao.setNativeManagementModeForSynchronizedRequirements(ids);

            // deleteRequierement folder sync extender
            requirementFolderSyncExtenderDao.deleteByRemoteSynchronisationId(ids);
            // delete sync extender
            requirementSyncExtenderDao.deleteByRemoteSynchronisationId(ids);
            // delete sprint req sync extender
            sprintReqSyncExtenderDao.deleteByRemoteSynchronisationIds(ids);
            // delete sync from sprint
            sprintDao.deleteRemoteSynchronisationFromSprint(ids);
            // delete sync
            remoteSynchronisationDao.deleteByProjectId(projectId);
        }
    }

    @Override
    public void deleteAllRemoteAutomationRequestExtenders(long projectId) {
        List<AutomationRequest> automationRequests = automationRequestDao.findByProjectId(projectId);
        List<Long> automationRequestIds =
                automationRequests.stream().map(AutomationRequest::getId).toList();

        // delete remote automation request extenders
        if (!automationRequestIds.isEmpty()) {
            remoteAutomationRequestExtenderDao.deleteByAutomationRequestIds(automationRequestIds);
        }
    }

    @Override
    public Map<String, String> getPluginConfiguration(
            long projectId, WorkspaceType workspace, String pluginId) {
        return doGetPluginConfiguration(projectId, workspace, pluginId);
    }

    @Override
    public Map<String, String> getPluginConfigurationWithoutCheck(
            long projectId, WorkspaceType workspace, String pluginId) {
        return doGetPluginConfiguration(projectId, workspace, pluginId);
    }

    private Map<String, String> doGetPluginConfiguration(
            long projectId, WorkspaceType workspace, String pluginId) {
        PluginReferencer<?> library = findLibrary(projectId, workspace);
        LibraryPluginBinding binding = library.getPluginBinding(pluginId);
        if (binding != null) {
            return binding.getProperties();
        } else {
            return new HashMap<>();
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void setPluginConfiguration(
            long projectId, WorkspaceType workspace, String pluginId, Map<String, String> configuration) {

        PluginReferencer<?> library = findLibrary(projectId, workspace);
        if (!library.isPluginEnabled(pluginId)) {
            library.enablePlugin(pluginId);
        }

        LibraryPluginBinding binding = library.getPluginBinding(pluginId);
        binding.setProperties(configuration);

        if (genericProjectDao.isProjectTemplate(projectId)) {
            synchronizeBoundPluginConfigurations(projectId, pluginId);
        }
    }

    @Override
    public void synchronizeBoundPluginConfigurations(long templateId, String pluginId) {
        templateConfigurablePluginBindingService
                .findAllByTemplateIdAndPluginId(templateId, pluginId)
                .forEach(
                        binding -> {
                            final Long projectId = binding.getProjectId();
                            templateConfigurablePluginService
                                    .findById(pluginId)
                                    .ifPresent(
                                            plugin -> plugin.synchroniseTemplateConfiguration(templateId, projectId));
                        });
    }

    @Override
    public void enableAllRemoteSynchronisations(long projectId, String pluginId) {
        List<RemoteSynchronisation> listRemoteSync =
                remoteSynchronisationDao.findByProjectIdAndKind(projectId, pluginId);
        listRemoteSync.forEach(remoteSync -> remoteSync.setSynchronisationEnable(true));
    }

    // ************************** status configuration section
    // ****************************

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void enableExecutionStatus(long projectId, ExecutionStatus executionStatus) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        // Parameter is locked if it is a bound Project
        if (genericProject.isBoundToTemplate()) {
            throw new LockedParameterException();
        }
        doEnableExecutionStatus(genericProject, executionStatus);
    }

    @Override
    public void doEnableExecutionStatus(
            GenericProject genericProject, ExecutionStatus executionStatus) {
        genericProject.getCampaignLibrary().enableStatus(executionStatus);
        if (genericProjectDao.isProjectTemplate(genericProject.getId())) {
            /* TODO: Optimize with a request. */
            Collection<Project> boundProjects = projectDao.findAllBoundToTemplate(genericProject.getId());
            for (Project boundProject : boundProjects) {
                boundProject.getCampaignLibrary().enableStatus(executionStatus);
            }
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void disableExecutionStatus(long projectId, ExecutionStatus executionStatus) {
        GenericProject project = genericProjectDao.getReferenceById(projectId);
        // Parameter is locked if the Project is bound to a Template
        if (project.isBoundToTemplate()) {
            throw new LockedParameterException();
        }
        doDisableExecutionStatus(project, executionStatus);
    }

    @Override
    public void doDisableExecutionStatus(
            GenericProject genericProject, ExecutionStatus executionStatus) {
        genericProject.getCampaignLibrary().disableStatus(executionStatus);
        /* If the GenericProject is a Template, propagate modification to bound Projects. */
        if (genericProjectDao.isProjectTemplate(genericProject.getId())) {
            Collection<Project> boundProjects = projectDao.findAllBoundToTemplate(genericProject.getId());
            for (Project boundProject : boundProjects) {
                boundProject.getCampaignLibrary().disableStatus(executionStatus);
            }
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public Set<ExecutionStatus> enabledExecutionStatuses(long projectId) {
        return enabledExecutionStatusesUnsecured(projectId);
    }

    private Set<ExecutionStatus> enabledExecutionStatusesUnsecured(long projectId) {
        GenericProject project = genericProjectDao.getReferenceById(projectId);

        Set<ExecutionStatus> statuses = new HashSet<>();
        statuses.addAll(Arrays.asList(ExecutionStatus.values()));

        Set<ExecutionStatus> disabledStatuses = project.getCampaignLibrary().getDisabledStatuses();

        statuses.removeAll(disabledStatuses);
        statuses.removeAll(ExecutionStatus.TA_STATUSES_ONLY);

        return statuses;
    }

    @Override
    public Set<ExecutionStatus> disabledExecutionStatuses(long projectId) {
        GenericProject project = genericProjectDao.getReferenceById(projectId);
        return project.getCampaignLibrary().getDisabledStatuses();
    }

    @Override
    public void replaceExecutionStepStatus(
            long projectId, ExecutionStatus source, ExecutionStatus target) {

        // save the ids of executions having steps with the source status
        List<Long> modifiedExecutionIds =
                executionDao.findExecutionIdsHavingStepStatus(projectId, source);

        // now modify the step statuses
        executionDao.replaceExecutionStepStatus(projectId, source, target);

        // now update the execution status
        for (Long id : modifiedExecutionIds) {
            ExecutionStatusReport report = execProcessing.getExecutionStatusReport(id);
            execProcessing.setExecutionStatus(id, report);
        }

        // finally update the item test plans
        executionDao.replaceTestPlanStatus(projectId, source, target);
    }

    @Override
    public boolean projectUsesExecutionStatus(long projectId, ExecutionStatus executionStatus) {

        return executionDao.projectUsesExecutionStatus(projectId, executionStatus);
    }

    // **************** private stuffs **************

    private PluginReferencer<?> findLibrary(long projectId, WorkspaceType workspace) {
        GenericProject project = genericProjectDao.getReferenceById(projectId);

        return switch (workspace) {
            case TEST_CASE_WORKSPACE -> project.getTestCaseLibrary();
            case REQUIREMENT_WORKSPACE -> project.getRequirementLibrary();
            case CAMPAIGN_WORKSPACE -> project.getCampaignLibrary();
            default ->
                    throw new IllegalArgumentException(
                            "WorkspaceType " + workspace + " is unknown and is not covered");
        };
    }

    /**
     * @see CustomGenericProjectManager#changeName(long, String)
     */
    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeName(long projectId, String newName) {
        GenericProject project = genericProjectDao.getReferenceById(projectId);

        if (StringUtils.equals(project.getName(), newName)) {
            return;
        }

        boolean projectNameIsSameWithDifferentCase =
                StringUtils.equals(project.getName().toLowerCase(), newName.toLowerCase());
        if (!projectNameIsSameWithDifferentCase && genericProjectDao.countByName(newName) > 0) {
            throw new NameAlreadyInUseException(project.getClass().getSimpleName(), newName);
        }
        CustomReportLibrary crl = project.getCustomReportLibrary();
        CustomReportLibraryNode node = customReportLibraryNodeDao.findNodeFromEntity(crl);
        node.setName(newName);
        ActionWordLibrary awl = project.getActionWordLibrary();
        ActionWordLibraryNode actionWordLibraryNode = actionWordLibraryNodeDao.findNodeFromEntity(awl);
        actionWordLibraryNode.setName(newName);

        project.setName(newName);
    }

    private void copyMilestone(GenericProject target, GenericProject source) {

        List<Milestone> milestones = getOnlyBindableMilestones(source.getMilestones());

        target.bindMilestones(milestones);

        for (Milestone milestone : milestones) {
            milestone.addProjectToPerimeter(target);
        }
    }

    private List<Milestone> getOnlyBindableMilestones(List<Milestone> milestones) {
        List<Milestone> bindableMilestones = new ArrayList<>();
        for (Milestone m : milestones) {
            if (m.getStatus().isBindableToProject()) {
                bindableMilestones.add(m);
            }
        }
        return bindableMilestones;
    }

    private void copyTestAutomationSettings(GenericProject target, GenericProject source) {

        target.setTestAutomationServer(source.getTestAutomationServer());

        for (TestAutomationProject automationProject : source.getTestAutomationProjects()) {
            TestAutomationProject taCopy = automationProject.createCopy();
            bindTestAutomationProject(target.getId(), taCopy);
        }
    }

    private void copyImplementationTechnologyAndScriptLanguage(
            GenericProject target, GenericProject source) {
        target.setBddImplementationTechnology(source.getBddImplementationTechnology());
        target.setBddScriptLanguage(source.getBddScriptLanguage());
    }

    private void copyAutomationWorkflowSettings(GenericProject target, GenericProject source) {
        target.setAllowAutomationWorkflow(source.isAllowAutomationWorkflow());
        target.setAutomationWorkflowType(source.getAutomationWorkflowType());
    }

    private void copyBugtrackerSettings(GenericProject target, GenericProject source) {
        if (source.isBoundToBugtracker()) {
            changeBugTracker(target, source.getBugTracker());
        }
    }

    private void copyAiServerSettings(GenericProject target, GenericProject source) {
        target.setAiServer(source.getAiServer());
    }

    private void copyCustomFieldsSettings(GenericProject target, GenericProject source) {
        customFieldBindingModificationService.copyCustomFieldsSettingsFromTemplate(target, source);
    }

    private void copyAssignedUsers(GenericProject target, GenericProject source) {
        permissionsManager.copyAssignedUsers(target, source);
    }

    private void copyInfolists(GenericProject target, GenericProject source) {
        target.setRequirementCategories(source.getRequirementCategories());
        target.setTestCaseNatures(source.getTestCaseNatures());
        target.setTestCaseTypes(source.getTestCaseTypes());
    }

    private void copyExecutionStatuses(GenericProject target, GenericProject source) {

        Set<ExecutionStatus> enabledStatuses = enabledExecutionStatusesUnsecured(source.getId());
        Set<ExecutionStatus> disabledStatuses = disabledExecutionStatuses(source.getId());

        for (ExecutionStatus execStatusToEnable : enabledStatuses) {
            doEnableExecutionStatus(target, execStatusToEnable);
        }
        for (ExecutionStatus execStatusToDisable : disabledStatuses) {
            doDisableExecutionStatus(target, execStatusToDisable);
        }
    }

    private void copyPluginsActivation(GenericProject target, GenericProject source) {
        PluginType pluginType;
        for (String pluginId : source.getRequirementLibrary().getEnabledPlugins()) {
            target.getRequirementLibrary().enablePlugin(pluginId);
        }
        for (String pluginId : source.getTestCaseLibrary().getEnabledPlugins()) {
            target.getTestCaseLibrary().enablePlugin(pluginId);
            LibraryPluginBinding lpb = source.getTestCaseLibrary().getPluginBinding(pluginId);
            if (lpb != null) {
                pluginType = lpb.getPluginType();
                if (pluginType != null) {
                    target.getTestCaseLibrary().getPluginBinding(pluginId).setPluginType(pluginType);
                }
            }
        }
        for (String pluginId : source.getCampaignLibrary().getEnabledPlugins()) {
            target.getCampaignLibrary().enablePlugin(pluginId);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public GenericProject synchronizeGenericProject(
            GenericProject target, GenericProject source, GenericProjectCopyParameter params) {

        if (params.isCopyPermissions()) {
            copyAssignedUsers(target, source);
        }
        if (params.isCopyCUF()) {
            copyCustomFieldsSettings(target, source);
        }
        if (params.isCopyBugtrackerBinding()) {
            copyBugtrackerSettings(target, source);
        }
        if (params.isCopyAiServerBinding()) {
            copyAiServerSettings(target, source);
        }
        if (params.isCopyAutomatedProjects()) {
            copyImplementationTechnologyAndScriptLanguage(target, source);
            copyTestAutomationSettings(target, source);
            copyAutomationWorkflowSettings(target, source);
        }
        if (params.isCopyInfolists()) {
            copyInfolists(target, source);
        }
        if (params.isCopyMilestone()) {
            copyMilestone(target, source);
        }
        if (params.isCopyAllowTcModifFromExec()) {
            target.setAllowTcModifDuringExec(source.allowTcModifDuringExec());
        }
        if (params.isCopyOptionalExecStatuses()) {
            copyExecutionStatuses(target, source);
        }
        if (params.isCopyPluginsActivation()) {
            copyPluginsActivation(target, source);
        }

        if (params.isCopyPluginsConfiguration()) {
            copyPluginConfiguration(target, source);
        }

        return target;
    }

    private void copyPluginConfiguration(GenericProject target, GenericProject source) {

        GenericProject sourceProject = projectManager.findById(source.getId());
        List<String> enabledPluginIds =
                sourceProject.getRequirementLibrary().getEnabledPlugins().stream().toList();
        List<TemplateConfigurablePlugin> configurablePlugins =
                templateConfigurablePluginService.findByIds(enabledPluginIds);
        configurablePlugins.forEach(
                plugin -> plugin.synchroniseTemplateConfiguration(source.getId(), target.getId()));
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public GenericProject synchronizeProjectFromTemplate(
            Project target, ProjectTemplate source, List<String> boundTemplatePlugins) {
        copyCustomFieldsSettings(target, source);
        copyInfolists(target, source);
        infoListDao.setDefaultNatureForProject(
                target.getId(), source.getTestCaseNatures().getDefaultItem());
        infoListDao.setDefaultTypeForProject(
                target.getId(), source.getTestCaseTypes().getDefaultItem());
        infoListDao.setDefaultCategoryForProject(
                target.getId(), source.getRequirementCategories().getDefaultItem());
        target.setAllowTcModifDuringExec(source.allowTcModifDuringExec());
        copyExecutionStatuses(target, source);
        copyPluginsActivation(target, source);
        copyImplementationTechnologyAndScriptLanguage(target, source);
        copyAutomationWorkflowSettings(target, source);
        copyServersSettings(target, source);

        boundTemplatePlugins.stream()
                .map(templateConfigurablePluginService::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(plugin -> addTemplatePluginConfigurationBinding(plugin, target, source));

        return target;
    }

    private void copyServersSettings(Project target, ProjectTemplate source) {
        if (target.getTestAutomationServer() == null) {
            copyTestAutomationSettings(target, source);
        }
        if (!target.isBoundToBugtracker()) {
            copyBugtrackerSettings(target, source);
        }
        if (target.getAiServer() == null) {
            copyAiServerSettings(target, source);
        }
    }

    private void addTemplatePluginConfigurationBinding(
            TemplateConfigurablePlugin plugin, Project target, ProjectTemplate source) {
        final Long templateId = source.getId();
        final Long projectId = target.getId();
        final String pluginId = plugin.getId();

        if (!templateConfigurablePluginBindingService.hasBinding(templateId, projectId, pluginId)) {
            templateConfigurablePluginBindingService.createTemplateConfigurablePluginBinding(
                    source, target, plugin);

            if (pluginHasConfiguration(plugin, templateId)) {
                plugin.synchroniseTemplateConfiguration(templateId, projectId);
            }
        }
    }

    @Override
    public boolean pluginHasConfigurationOrSynchronisations(
            ConfigurablePlugin plugin, long projectId) {
        boolean hasRemoteSynchro = hasProjectRemoteSynchronisation(projectId);
        return pluginHasConfiguration(plugin, projectId) || hasRemoteSynchro;
    }

    @Override
    public boolean pluginHasConfiguration(ConfigurablePlugin plugin, long projectId) {
        WorkspaceType workspaceType = plugin.getConfiguringWorkspace();
        Map<String, String> pluginConfiguration =
                getPluginConfiguration(projectId, workspaceType, plugin.getId());
        return !pluginConfiguration.isEmpty();
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeAllowTcModifDuringExec(long projectId, boolean active) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        if (!genericProject.isBoundToTemplate()) {
            genericProject.setAllowTcModifDuringExec(active);
            /* If project is a Template, propagate on all the bound projects. */
            if (ProjectHelper.isTemplate(genericProject)) {
                templateDao.propagateAllowTcModifDuringExec(projectId, active);
            }
        } else {
            throw new LockedParameterException();
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public boolean checkIfTcGherkinHaveTaScript(Long projectId) {
        boolean check = false;
        Integer number = testCaseDao.countScriptedTestCaseAssociatedToTAScriptByProject(projectId);
        if (number > 0) {
            check = true;
        }
        return check;
    }

    private void changeAutomationWorkflow(
            long projectId, boolean active, boolean isChangingRemoteWorkflowToNativeSimplified) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);

        genericProject.setAllowAutomationWorkflow(active);

        if (active) {
            List<Long> tcIds = testCaseDao.findAllTestCaseAssociatedToTAScriptByProject(projectId);
            createAutomationRequestForTc(tcIds);
            // check that we are changing from a remote workflow to a native simplified workflow
            if (isChangingRemoteWorkflowToNativeSimplified) {
                this.convertRemoteStatusToSquashAutomRequestStatus(genericProject);
            }
        }
        /* If project is a Template, propagate on all the bound projects. */
        if (ProjectHelper.isTemplate(genericProject)) {
            templateDao.propagateAllowAutomationWorkflow(projectId, active);
        }
    }

    /**
     * For all automatable test cases in a generic project, check if automation request status is the
     * configured final* status. If it is, set automation request status to automated, else set it to
     * transmitted.
     *
     * @param genericProject squash project
     */
    private void convertRemoteStatusToSquashAutomRequestStatus(GenericProject genericProject) {
        String finalRemoteStatus = this.getRemoteFinalStatus(genericProject.getId(), true);
        Map<Long, String> tcRemoteStatusMap =
                testCaseDao.findAllAutomatableTestCasesByProjectId(genericProject.getId());
        List<TestCase> testCases =
                testCaseLoader.load(
                        tcRemoteStatusMap.keySet(),
                        EnumSet.of(TestCaseLoader.Options.FETCH_AUTOMATION_REQUEST));

        testCases.forEach(
                tc -> {
                    String tcRemoteStatus = tcRemoteStatusMap.get(tc.getId());
                    boolean isFinalStatus =
                            tcRemoteStatus != null && tcRemoteStatus.equalsIgnoreCase(finalRemoteStatus);

                    if (!isFinalStatus) {
                        tc.getAutomationRequest().setRequestStatus(AutomationRequestStatus.TRANSMITTED);
                    } else {
                        tc.getAutomationRequest().setRequestStatus(AutomationRequestStatus.AUTOMATED);
                    }
                });
    }

    private String getRemoteFinalStatus(Long projectId, boolean wasRemoteWorkflow) {
        final LibraryPluginBinding lpb =
                projectDao.findPluginForProject(projectId, PluginType.AUTOMATION);

        if (lpb != null && wasRemoteWorkflow) {
            final Map<String, String> pluginConfiguration =
                    projectManager.getPluginConfigurationWithoutCheck(
                            projectId, WorkspaceType.TEST_CASE_WORKSPACE, lpb.getPluginId());
            return pluginConfiguration == null ? null : pluginConfiguration.get("finalState");
        }
        return null;
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeAutomationWorkflow(long projectId, String automationWorkflow) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);

        boolean isChangingRemoteWorkflowToNativeSimplified =
                (AutomationWorkflowType.REMOTE_WORKFLOW == genericProject.getAutomationWorkflowType()
                        && AutomationWorkflowType.NATIVE_SIMPLIFIED
                                .name()
                                .equalsIgnoreCase(automationWorkflow));

        genericProject.setAutomationWorkflowType(AutomationWorkflowType.valueOf(automationWorkflow));

        // Since allowAutomationWorkflow still exists, we have to update it consequently
        boolean active = !"NONE".equals(automationWorkflow);
        changeAutomationWorkflow(projectId, active, isChangingRemoteWorkflowToNativeSimplified);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeUseTreeStructureInScmRepo(long projectId, boolean activated) {
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        genericProject.setUseTreeStructureInScmRepo(activated);
    }

    private void createAutomationRequestForTc(List<Long> tcIds) {

        List<TestCase> testCases = testCaseLoader.load(tcIds);
        for (int x = 0; x < testCases.size(); x++) {
            TestCase testCase = testCases.get(x);
            testCase.setAutomatable(TestCaseAutomatable.Y);
            customTestCaseModificationService.createRequestForTestCase(
                    testCase, AutomationRequestStatus.AUTOMATED);
            if (x % 20 == 0) {
                em.flush();
                em.clear();
            }
        }
    }

    @Override
    public boolean isProjectUsingWorkflow(long projectId) {
        boolean isProjectUsingWorkflow = false;
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        String workflowType = genericProject.getAutomationWorkflowType().getI18nKey();
        if (workflowType.equals(AutomationWorkflowType.REMOTE_WORKFLOW.getI18nKey())) {
            // check if the plugin exists
            for (WorkspaceWizard plugin : plugins) {
                if (PluginType.AUTOMATION.equals(plugin.getPluginType())) {
                    isProjectUsingWorkflow = true;
                    break;
                }
            }
        }
        return isProjectUsingWorkflow;
    }

    @Override
    public boolean isProjectTemplate(long projectId) {
        return genericProjectDao.isProjectTemplate(projectId);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeBddImplTechnology(long projectId, String bddImplTechnology) {
        BddImplementationTechnology newBddImplTechnology =
                BddImplementationTechnology.valueOf(bddImplTechnology);
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        genericProject.setBddImplementationTechnology(newBddImplTechnology);
        if (BddImplementationTechnology.ROBOT.equals(newBddImplTechnology)) {
            genericProject.setBddScriptLanguage(BddScriptLanguage.ENGLISH);
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeBddScriptLanguage(long projectId, String bddScriptLanguage) {
        BddScriptLanguage newBddScriptLanguage = BddScriptLanguage.valueOf(bddScriptLanguage);
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        BddImplementationTechnology currentBddImplTechnology =
                genericProject.getBddImplementationTechnology();
        if (BddImplementationTechnology.ROBOT.equals(currentBddImplTechnology)
                && !BddScriptLanguage.ENGLISH.equals(newBddScriptLanguage)) {
            throw new IllegalArgumentException(
                    "No language other than English can be set for a Robot project.");
        }
        genericProject.setBddScriptLanguage(newBddScriptLanguage);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public Integer changeAutomatedSuitesLifetime(long projectId, String rawLifetime) {
        Integer lifetime;
        try {
            if (StringUtils.isBlank(rawLifetime)) {
                lifetime = null;
            } else {
                lifetime = Integer.parseInt(rawLifetime);
                if (lifetime < 0) {
                    throw new IllegalArgumentException();
                }
            }
        } catch (IllegalArgumentException ex) {
            throw new WrongLifetimeFormatException(ex);
        }
        GenericProject genericProject = genericProjectDao.getReferenceById(projectId);
        genericProject.setAutomatedSuitesLifetime(lifetime);
        return lifetime;
    }

    @Override
    public void clearEnvironmentTagOverrides(long projectId) {
        final Optional<GenericProject> maybeProject = genericProjectDao.findById(projectId);

        if (maybeProject.isPresent()) {
            final GenericProject project = maybeProject.get();
            project.setEnvironmentTags(Collections.emptyList());
            project.setInheritsEnvironmentTags(true);
        } else {
            throw new IllegalArgumentException(
                    String.format("Project with id %d was not found", projectId));
        }
    }

    @Override
    public void createAttachmentFromDescription(GenericProject project) {
        String description = project.getDescription();

        if (description == null || description.isEmpty()) {
            return;
        }

        String newDescription =
                attachmentManagerService.handleRichTextAttachments(
                        description, project.getAttachmentList());

        if (!description.equals(newDescription)) {
            project.setDescription(newDescription);
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeAiServer(long projectId, long aiServerId) {
        GenericProject project = em.find(GenericProject.class, projectId);
        AiServer aiServer = em.find(AiServer.class, aiServerId);
        if (aiServer == null) {
            throw new AiServerWasDeletedException();
        } else {
            project.setAiServer(aiServer);
        }
    }

    @Override
    public void unbindAiServers(List<Long> aiServerIds) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        genericProjectDao.unbindAiServers(aiServerIds);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void unbindAiServer(long projectId) {
        genericProjectDao.unbindAiServer(projectId);
    }

    @Override
    public void unbindBugTrackers(List<Long> bugTrackerIds) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        genericProjectDao.unbindBugTrackers(bugTrackerIds);
    }

    private void checkPermissionOnSyncIds(List<Long> syncIds) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                syncIds,
                Permissions.MANAGE_PROJECT.name(),
                RemoteSynchronisation.class.getName());
    }
}
