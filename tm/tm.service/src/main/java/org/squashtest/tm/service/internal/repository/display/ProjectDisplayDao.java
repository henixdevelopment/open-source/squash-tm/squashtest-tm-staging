/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jooq.CommonTableExpression;
import org.jooq.Record1;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.bdd.BddScriptLanguage;
import org.squashtest.tm.service.internal.display.dto.PivotFormatImportDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;

public interface ProjectDisplayDao {

    List<ProjectDto> getActiveProjectsByIds(List<Long> projectIds);

    List<ProjectDto> getActiveProjectsByIds(CommonTableExpression<Record1<Long>> projectCte);

    Map<Long, List<String>> getDisabledExecutionStatus(Set<Long> projectIds);

    Map<Long, List<String>> getDisabledExecutionStatus(
            CommonTableExpression<Record1<Long>> projectIdsCte);

    ProjectViewDto getProjectOrTemplateById(Long projectId);

    List<Long> getProjectsLinkedToTemplate(Long templateId);

    void appendActivatedPlugins(List<ProjectDto> projects);

    Long getTaServerIdByProjectId(Long projectId);

    List<NamedReference> findProjectsByTemplateId(Long templateId);

    List<NamedReference> findAllReferences();

    BddScriptLanguage findBddScriptLanguageByProjectId(long projectId);

    Map<Long, Boolean> allowAutomationWorkflowByProjectId(List<Long> projectIds);

    List<String> fetchBugtrackerProjectNames(long projectId);

    List<PivotFormatImportDto> getExistingImports(Long projectId);
}
