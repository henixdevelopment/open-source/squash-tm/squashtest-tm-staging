/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.SprintRequirementSyncExtender;
import org.squashtest.tm.domain.requirement.RemoteRequirementPerimeterStatus;

public interface SprintRequirementSyncExtenderDao
        extends JpaRepository<SprintRequirementSyncExtender, Long> {

    @Query("""
        delete SprintRequirementSyncExtender extender
        where extender.sprintReqVersion.id
        in (select srv.id
            from SprintReqVersion srv
            where srv.sprint.id = :sprintId)
    """)
    @Modifying
    void deleteAllSprintReqSyncExtendersBySprintId(@Param("sprintId") Long sprintId);

    @Query(
            "delete SprintRequirementSyncExtender extender where extender.remoteSynchronisation.id in (:ids)")
    @Modifying
    void deleteByRemoteSynchronisationIds(@Param("ids") List<Long> ids);

    @Query(
            "select extender from SprintRequirementSyncExtender extender "
                    + "left join fetch extender.sprintReqVersion reqVersion "
                    + "where reqVersion.sprint.id = :sprintId "
                    + "and extender.remoteSynchronisation.id = :remoteSyncId "
                    + "and extender.remoteReqId = :remoteReqId ")
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    SprintRequirementSyncExtender findBySprintIdRemoteSyncIdAndRemoteReqId(
            @Param("sprintId") long sprintId,
            @Param("remoteSyncId") long remoteSyncId,
            @Param("remoteReqId") String remoteReqId);

    @Query(
            "update SprintRequirementSyncExtender ex set ex.remotePerimeterStatus=:status "
                    + "where ex.sprintReqVersion.id in (:sprintReqVersions)")
    @Modifying
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    void updatePerimeterStatusForSprintReqVersions(
            @Param("sprintReqVersions") List<Long> sprintReqVersions,
            @Param("status") RemoteRequirementPerimeterStatus status);

    @Query(
            "update SprintRequirementSyncExtender ex set ex.remotePerimeterStatus=:status "
                    + "where ex.remoteSynchronisation.id = :remoteSyncId")
    @Modifying
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    void updatePerimeterStatusByRemoteSyncId(
            @Param("remoteSyncId") Long remoteSyncId,
            @Param("status") RemoteRequirementPerimeterStatus status);

    @Query(
            "update SprintRequirementSyncExtender ex "
                    + "set ex.remotePerimeterStatus=:status "
                    + "where ex.remoteReqId in (:remoteReqIds) and ex.sprintReqVersion in (select srv from SprintReqVersion srv where srv.sprint.id = (:sprintId))")
    @Modifying
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    void updatePerimeterStatus(
            @Param("remoteReqIds") List<String> remoteReqIds,
            @Param("sprintId") Long sprintId,
            @Param("status") RemoteRequirementPerimeterStatus status);

    @Query("""
        select extender.remoteReqId from SprintRequirementSyncExtender extender
        inner join extender.sprintReqVersion reqVersion
        where reqVersion.sprint.id in (:sprintIds)
    """)
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    List<String> findRemoteKeysBySprintIds(@Param("sprintIds") List<Long> sprintIds);
}
