/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.requirement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HighLevelRequirementVersionDto extends AbstractRequirementVersionDto {
    private List<LinkedLowLevelRequirementDto> lowLevelRequirements = new ArrayList<>();
    private List<RequirementVersionLinkDto> requirementVersionLinks = new ArrayList<>();

    public List<RequirementVersionLinkDto> getRequirementVersionLinks() {
        return requirementVersionLinks;
    }

    public void setRequirementVersionLinks(List<RequirementVersionLinkDto> requirementVersionLinks) {
        this.requirementVersionLinks = requirementVersionLinks;
    }

    public List<LinkedLowLevelRequirementDto> getLowLevelRequirements() {
        return lowLevelRequirements;
    }

    public void setLowLevelRequirements(List<LinkedLowLevelRequirementDto> lowLevelRequirements) {
        this.lowLevelRequirements = lowLevelRequirements;
    }

    @Override
    public boolean isHighLevelRequirement() {
        return true;
    }

    public static class LinkedLowLevelRequirementDto {
        private String projectName;
        private String reference;
        private String name;
        private String milestoneLabels;
        private Date milestoneMinDate;
        private Date milestoneMaxDate;
        private int versionNumber;
        private Long requirementId;
        private Long requirementVersionId;
        private boolean isChildOfRequirement = false;
        private String requirementStatus;
        private String criticality;
        private String path;
        private int nbVerifyingTestCases;

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMilestoneLabels() {
            return milestoneLabels;
        }

        public void setMilestoneLabels(String milestoneLabels) {
            this.milestoneLabels = milestoneLabels;
        }

        public Date getMilestoneMinDate() {
            return milestoneMinDate;
        }

        public void setMilestoneMinDate(Date milestoneMinDate) {
            this.milestoneMinDate = milestoneMinDate;
        }

        public Date getMilestoneMaxDate() {
            return milestoneMaxDate;
        }

        public void setMilestoneMaxDate(Date milestoneMaxDate) {
            this.milestoneMaxDate = milestoneMaxDate;
        }

        public int getVersionNumber() {
            return versionNumber;
        }

        public void setVersionNumber(int versionNumber) {
            this.versionNumber = versionNumber;
        }

        public Long getRequirementId() {
            return requirementId;
        }

        public void setRequirementId(Long requirementId) {
            this.requirementId = requirementId;
        }

        public Long getRequirementVersionId() {
            return requirementVersionId;
        }

        public void setRequirementVersionId(Long requirementVersionId) {
            this.requirementVersionId = requirementVersionId;
        }

        public boolean isChildOfRequirement() {
            return isChildOfRequirement;
        }

        public void setChildOfRequirement(boolean childOfRequirement) {
            isChildOfRequirement = childOfRequirement;
        }

        public String getRequirementStatus() {
            return requirementStatus;
        }

        public void setRequirementStatus(String requirementStatus) {
            this.requirementStatus = requirementStatus;
        }

        public String getCriticality() {
            return criticality;
        }

        public void setCriticality(String criticality) {
            this.criticality = criticality;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getNbVerifyingTestCases() {
            return nbVerifyingTestCases;
        }

        public void setNbVerifyingTestCases(int nbVerifyingTestCases) {
            this.nbVerifyingTestCases = nbVerifyingTestCases;
        }
    }
}
