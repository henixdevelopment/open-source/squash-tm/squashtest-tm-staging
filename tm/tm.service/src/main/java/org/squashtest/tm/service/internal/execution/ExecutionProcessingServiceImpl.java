/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution;

import static org.squashtest.tm.service.security.Authorizations.EXECUTE_EXECSTEP_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.exception.execution.ExecutionHasNoRunnableStepException;
import org.squashtest.tm.exception.execution.ExecutionHasNoStepsException;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.execution.ExecutionModificationService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.ExecutionStepDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.security.UserContextService;

@Service("squashtest.tm.service.ExecutionProcessingService")
@Transactional
public class ExecutionProcessingServiceImpl implements ExecutionProcessingService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ExecutionProcessingServiceImpl.class);

    @Inject private UserContextService userContextService;

    @Inject private ExecutionDao executionDao;

    @Inject private ExecutionStepDao executionStepDao;

    @Inject private ExecutionModificationService execModService;

    @Inject private IterationTestPlanManagerService testPlanService;

    @Inject private CustomTestSuiteModificationService customTestSuiteModificationService;

    @Inject private UserDao userDao;

    @Inject private SprintDisplayDao sprintDisplayDao;

    @Override
    public ExecutionStep findExecutionStep(Long executionStepId) {
        return executionStepDao.findById(executionStepId);
    }

    @Override
    public boolean wasNeverRun(Long executionId) {
        return executionDao.countSteps(executionId)
                        - executionDao.countStatus(executionId, ExecutionStatus.READY)
                == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public ExecutionStep findRunnableExecutionStep(long executionId)
            throws ExecutionHasNoStepsException {
        Execution execution = executionDao.getReferenceById(executionId);

        ExecutionStep step;
        try {
            step = execution.findFirstRunnableStep();
        } catch (ExecutionHasNoRunnableStepException e) { // NOSONAR
            // : this exception is part of the nominal use case
            step = execution.getLastStep();
        }

        return step;
    }

    @Override
    @Transactional(readOnly = true)
    public ExecutionStep findStepAt(long executionId, int executionStepIndex) {
        Execution execution = executionDao.findAndInit(executionId);

        return execution.getSteps().get(executionStepIndex);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECSTEP_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = ExecutionStep.class)
    public void changeExecutionStepStatus(@Id Long executionStepId, ExecutionStatus status) {

        ExecutionStep step = executionStepDao.findById(executionStepId);
        ExecutionStatus formerStatus = step.getExecutionStatus();
        Set<ExecutionStatus> disabledStatusList = step.getCampaignLibrary().getDisabledStatuses();

        if (!disabledStatusList.contains(status)) {
            step.setExecutionStatus(status);
            /* update execution data for step and update execution and item test plan status and execution data*/
            forwardAndUpdateStatus(step, formerStatus);
        } else {
            throw new RuntimeException(
                    "Unable to set this status: "
                            + status.getCanonicalStatus().name()
                            + ". It's a optional status, check your project settings");
        }
    }

    @Override
    @CheckBlockingMilestone(entityType = ExecutionStep.class)
    public void setExecutionStepComment(@Id Long executionStepId, String comment) {
        ExecutionStep step = executionStepDao.findById(executionStepId);
        checkParentSprintStatus(step.getExecution().getId());
        step.setComment(comment);
    }

    @Override
    public Execution findExecution(Long executionId) {
        return execModService.findAndInitExecution(executionId);
    }

    @Override
    public List<ExecutionStep> getExecutionSteps(Long executionId) {
        return execModService.findExecutionSteps(executionId);
    }

    @Override
    public int findExecutionStepRank(Long executionStepId) {
        ExecutionStep step = executionStepDao.findById(executionStepId);
        return step.getExecutionStepOrder();
    }

    @Override
    public int findTotalNumberSteps(Long executionId) {
        Execution execution = executionDao.findAndInit(executionId);
        return execution.getSteps().size();
    }

    @Override
    @CheckBlockingMilestone(entityType = Execution.class)
    public void setExecutionStatus(@Id Long executionId, ExecutionStatus status) {
        checkParentSprintStatus(executionId);
        Execution execution = executionDao.getReferenceById(executionId);
        execution.setExecutionStatus(status);
    }

    @Override
    public ExecutionStatusReport getExecutionStatusReport(Long executionId) {
        return executionDao.getStatusReport(executionId);
    }

    /***
     * Method which update :<br>
     * * execution and item test plan status * execution data for the step, execution and item test plan
     *
     * @param executionStep
     * @param formerStepStatus
     */
    private void forwardAndUpdateStatus(
            ExecutionStep executionStep, ExecutionStatus formerStepStatus) {
        checkParentSprintStatus(executionStep.getExecution().getId());

        // update step execution data
        updateStepExecutionData(executionStep);

        Execution execution = executionStepDao.findParentExecution(executionStep.getId());

        ExecutionStatus formerExecutionStatus = execution.getExecutionStatus();
        ExecutionStatus newStepStatus = executionStep.getExecutionStatus();

        // let's see if we can autocompute with only 3 these statuses
        ExecutionStatus newExecutionStatus =
                newStepStatus.deduceNewStatus(formerExecutionStatus, formerStepStatus);

        if (newExecutionStatus == null) { // means we couldn't autocompute with only 3 statuses
            ExecutionStatusReport report = executionDao.getStatusReport(execution.getId());
            newExecutionStatus = ExecutionStatus.computeNewStatus(report);
        }

        execution.setExecutionStatus(newExecutionStatus);
        updateExecutionMetadata(execution);

        final IterationTestPlanItem iterationTestPlanItem = execution.getTestPlan();

        if (iterationTestPlanItem != null) {
            customTestSuiteModificationService.updateExecutionStatus(
                    iterationTestPlanItem.getTestSuites());
        }
    }

    /***
     * Update the execution step lastExecutionBy and On values depending on the status
     *
     * @param executionStep
     *            the step to update
     */
    @Override
    public void updateStepExecutionData(ExecutionStep executionStep) {
        // check the execution step status
        if (executionStep.getExecutionStatus().compareTo(ExecutionStatus.READY) == 0) {
            // if the item test plan status is READY, we reset the data
            executionStep.setLastExecutedBy(null);
            executionStep.setLastExecutedOn(null);
        } else {
            // we update the step execution data
            executionStep.setLastExecutedBy(userContextService.getUsername());
            executionStep.setLastExecutedOn(new Date());
        }
    }

    /***
     * Asks an execution to update it's metadata (lastExecutionOn, lastExecutedBy)
     * according to regular execution business rules.
     *
     * @param execution
     *            the execution to update
     */
    private void updateExecutionMetadata(Execution execution) {
        LOGGER.debug("update the executed by/on for given execution and it's test plan.");

        // Get the date and user of the most recent step which status is not at READY
        ExecutionStep mostRecentStep = getMostRecentExecutionStep(execution);
        if (mostRecentStep != null) {
            execution.setLastExecutedBy(mostRecentStep.getLastExecutedBy());
            execution.setLastExecutedOn(mostRecentStep.getLastExecutedOn());
        } else {
            execution.setLastExecutedBy(null);
            execution.setLastExecutedOn(null);
        }

        // forward to the test plan
        final IterationTestPlanItem itpi = execution.getTestPlan();

        if (itpi != null) {
            testPlanService.updateMetadata(itpi);
        } else {
            final TestPlanItem testPlanItem = execution.getSprintTestPlanItem();
            final Execution latestExecution = testPlanItem.getLatestExecution();

            if (latestExecution != null && latestExecution.getLastExecutedBy() != null) {
                final User assignee = userDao.findUserByLogin(latestExecution.getLastExecutedBy());
                testPlanItem.updateLastExecutionAndAssignee(latestExecution.getLastExecutedOn(), assignee);
            }
        }
    }

    @PreAuthorize(
            "hasPermission(#extender, 'EXECUTE') or hasRole('ROLE_TA_API_CLIENT')" + OR_HAS_ROLE_ADMIN)
    @Override
    public void updateExecutionMetadata(AutomatedExecutionExtender extender) {

        Execution execution = extender.getExecution();

        execution.setLastExecutedOn(new Date());
        execution.setLastExecutedBy(userContextService.getUsername());

        // forward to the test plan
        IterationTestPlanItem testPlan = execution.getTestPlan();
        testPlanService.updateMetadata(testPlan);
    }

    /***
     * Method which gets the most recent execution step which status is not at READY
     *
     * @param givenExecution
     *            the execution from which we get the steps
     * @return the most recent Execution Step which is not "READY"
     */
    private ExecutionStep getMostRecentExecutionStep(Execution givenExecution) {
        // Start at the fist one
        ExecutionStep mostRecentExecutionStep = givenExecution.getSteps().get(0);
        List<ExecutionStep> stepList = givenExecution.getSteps();
        for (ExecutionStep executionStep : stepList) {
            // first the status
            if (executionStep.getExecutionStatus().compareTo(ExecutionStatus.READY) != 0) {
                // first the most recent execution step has no execution date
                if (mostRecentExecutionStep.getLastExecutedOn() == null) {
                    mostRecentExecutionStep = executionStep;
                }
                // we compare the date and update the value if the step date is greater
                else if (executionStep.getLastExecutedOn() != null
                        && mostRecentExecutionStep
                                        .getLastExecutedOn()
                                        .compareTo(executionStep.getLastExecutedOn())
                                < 0) {
                    mostRecentExecutionStep = executionStep;
                }
            }
        }
        return mostRecentExecutionStep;
    }

    @Override
    public void setExecutionStatus(Long executionId, ExecutionStatusReport report) {
        checkParentSprintStatus(executionId);

        Execution execution = executionDao.findAndInit(executionId);

        ExecutionStatus newStatus = ExecutionStatus.computeNewStatus(report);

        execution.setExecutionStatus(newStatus);
    }

    private void checkParentSprintStatus(Long executionId) {
        final SprintStatus sprintStatus = sprintDisplayDao.getSprintStatusByExecutionId(executionId);

        if (SprintStatus.CLOSED.equals(sprintStatus)) {
            throw new SprintClosedException();
        }
    }
}
