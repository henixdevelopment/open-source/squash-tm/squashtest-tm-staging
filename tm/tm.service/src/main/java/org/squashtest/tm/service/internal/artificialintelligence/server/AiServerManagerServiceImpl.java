/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.artificialintelligence.server;

import static org.squashtest.tm.jooq.domain.Tables.AI_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.Date;
import java.util.List;
import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.aiserver.AiServer;
import org.squashtest.tm.domain.servers.AuthenticationPolicy;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.artificialintelligence.server.MalformedJsonPathException;
import org.squashtest.tm.service.annotation.IsUltimateLicenseAvailable;
import org.squashtest.tm.service.artificialintelligence.server.AiServerManagerService;
import org.squashtest.tm.service.internal.repository.AiServerDao;
import org.squashtest.tm.service.jsonpathextractor.JsonPathExtractor;
import org.squashtest.tm.service.project.CustomGenericProjectManager;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional
public class AiServerManagerServiceImpl implements AiServerManagerService {

    private final AiServerDao aiServerDao;
    private final DSLContext dsl;
    private final CustomGenericProjectManager customGenericProjectManager;
    private final UserAccountService userAccountService;
    private final JsonPathExtractor jsonPathExtractor;

    public AiServerManagerServiceImpl(
            AiServerDao aiServerDao,
            DSLContext dsl,
            CustomGenericProjectManager customGenericProjectManager,
            UserAccountService userAccountService,
            JsonPathExtractor jsonPathExtractor) {
        this.aiServerDao = aiServerDao;
        this.dsl = dsl;
        this.customGenericProjectManager = customGenericProjectManager;
        this.userAccountService = userAccountService;
        this.jsonPathExtractor = jsonPathExtractor;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void persist(AiServer server) {
        String serverName = server.getName();
        if (nameInUse(serverName)) {
            throw new NameAlreadyInUseException(AiServer.class.getSimpleName(), serverName);
        }
        server.setAuthenticationPolicy(AuthenticationPolicy.APP_LEVEL);
        server.setAuthenticationProtocol(AuthenticationProtocol.TOKEN_AUTH);
        aiServerDao.save(server);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void deleteAiServers(List<Long> aiServerIds) {
        customGenericProjectManager.unbindAiServers(aiServerIds);
        aiServerDao.deleteAllById(aiServerIds);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void updateName(long aiServerId, String name) {
        if (nameInUse(name)) {
            throw new NameAlreadyInUseException(AiServer.class.getSimpleName(), name);
        }
        AiServer server = aiServerDao.getReferenceById(aiServerId);
        server.setName(name);
    }

    @Override
    @IsUltimateLicenseAvailable
    public boolean nameInUse(String serverName) {
        Long id =
                dsl.select(THIRD_PARTY_SERVER.SERVER_ID)
                        .from(THIRD_PARTY_SERVER)
                        .join(AI_SERVER)
                        .on(THIRD_PARTY_SERVER.SERVER_ID.eq(AI_SERVER.SERVER_ID))
                        .where(THIRD_PARTY_SERVER.NAME.eq(serverName))
                        .fetchOneInto(Long.class);

        return id != null;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void updateDescription(long aiServerId, String description) {
        AiServer server = aiServerDao.getReferenceById(aiServerId);
        server.setDescription(description);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void updateUrl(long aiServerId, String url) {
        AiServer server = aiServerDao.getReferenceById(aiServerId);
        server.setUrl(url);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void updatePayloadTemplate(long aiServerId, String payload) {
        AiServer server = aiServerDao.getReferenceById(aiServerId);
        if (payload.isEmpty()) {
            server.setPayloadTemplate(null);
        } else {
            server.setPayloadTemplate(payload);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void forceAuditAfterCredentialsUpdate(long aiServerId) {
        AiServer server = aiServerDao.getReferenceById(aiServerId);
        server.setLastModifiedOn(new Date());
        server.setLastModifiedBy(userAccountService.findCurrentUserDto().getUsername());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void updateJsonPath(long aiServerId, String jsonPath) {
        if (jsonPathExtractor.isPathValid(jsonPath) || jsonPath.isEmpty()) {
            AiServer server = aiServerDao.getReferenceById(aiServerId);
            if (jsonPath.isEmpty()) {
                server.setJsonPath(null);
            } else {
                server.setJsonPath(jsonPath);
            }
        } else {
            throw new MalformedJsonPathException();
        }
    }
}
