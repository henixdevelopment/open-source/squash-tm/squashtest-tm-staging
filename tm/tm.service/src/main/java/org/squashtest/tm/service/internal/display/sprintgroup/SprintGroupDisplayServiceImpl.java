/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.sprintgroup;

import static org.squashtest.tm.service.security.Authorizations.READ_SPRINT_GROUP_OR_ADMIN;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.sprintgroup.SprintGroupDisplayService;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.sprintgroup.SprintGroupDto;
import org.squashtest.tm.service.internal.library.EntityPathHeaderService;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintGroupDisplayDao;

@Service
@Transactional(readOnly = true)
public class SprintGroupDisplayServiceImpl implements SprintGroupDisplayService {
    private final SprintGroupDisplayDao sprintGroupDisplayDao;
    private final AttachmentDisplayDao attachmentDisplayDao;
    private final EntityPathHeaderService entityPathHeaderService;

    public SprintGroupDisplayServiceImpl(
            SprintGroupDisplayDao sprintGroupDisplayDao,
            AttachmentDisplayDao attachmentDisplayDao,
            EntityPathHeaderService entityPathHeaderService) {
        this.sprintGroupDisplayDao = sprintGroupDisplayDao;
        this.attachmentDisplayDao = attachmentDisplayDao;
        this.entityPathHeaderService = entityPathHeaderService;
    }

    @Override
    @PreAuthorize(READ_SPRINT_GROUP_OR_ADMIN)
    public SprintGroupDto getSprintGroupView(long sprintGroupId) {
        SprintGroupDto sprintGroupDto = sprintGroupDisplayDao.getSprintGroupDtoById(sprintGroupId);
        AttachmentListDto attachmentList =
                attachmentDisplayDao.findAttachmentListById(sprintGroupDto.getAttachmentListId());
        sprintGroupDto.setAttachmentList(attachmentList);
        String path = entityPathHeaderService.buildCLNPathHeader(sprintGroupId);
        sprintGroupDto.setPath(path);

        return sprintGroupDto;
    }
}
