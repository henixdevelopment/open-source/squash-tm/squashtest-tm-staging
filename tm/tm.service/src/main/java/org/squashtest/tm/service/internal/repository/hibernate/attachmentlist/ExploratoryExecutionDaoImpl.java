/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.attachmentlist;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;

import java.util.List;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.repository.ExploratoryExecutionDao;

@Repository
public class ExploratoryExecutionDaoImpl implements ExploratoryExecutionDao {
    private final DSLContext dslContext;

    public ExploratoryExecutionDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<Long> findAllExploratoryExecutionsBySprintId(Long sprintId) {
        return dslContext
                .select(EXPLORATORY_EXECUTION.EXECUTION_ID)
                .from(EXPLORATORY_EXECUTION)
                .innerJoin(EXECUTION)
                .on(EXECUTION.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                .innerJoin(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(EXECUTION.TEST_PLAN_ITEM_ID))
                .innerJoin(SPRINT_REQ_VERSION)
                .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .innerJoin(SPRINT)
                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                .where(SPRINT.CLN_ID.eq(sprintId))
                .fetchInto(Long.class);
    }

    @Override
    public Long findExecutionIdBySessionNoteId(long sessionNoteId) {
        return dslContext
                .select(EXPLORATORY_EXECUTION.EXECUTION_ID)
                .from(EXPLORATORY_EXECUTION)
                .innerJoin(SESSION_NOTE)
                .on(SESSION_NOTE.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
                .where(SESSION_NOTE.NOTE_ID.eq(sessionNoteId))
                .fetchOneInto(Long.class);
    }
}
