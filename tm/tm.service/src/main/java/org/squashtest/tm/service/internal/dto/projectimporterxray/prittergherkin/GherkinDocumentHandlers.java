/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.prittergherkin;

import gherkin.ast.Background;
import gherkin.ast.Comment;
import gherkin.ast.DataTable;
import gherkin.ast.DocString;
import gherkin.ast.Examples;
import gherkin.ast.Feature;
import gherkin.ast.Scenario;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
import gherkin.ast.Tag;

public interface GherkinDocumentHandlers<A extends GherkinAccumulator> {
    A handleBackground(Background background, A gherkinAccumulator);

    A handleDataTable(DataTable dataTable, A gherkinAccumulator);

    A handleDocString(DocString docString, A gherkinAccumulator);

    A handleExamples(Examples examples, A gherkinAccumulator);

    A handleFeature(Feature feature, A gherkinAccumulator);

    A handleScenario(Scenario scenario, A gherkinAccumulator);

    A handleScenarioOutline(ScenarioOutline scenarioOutline, A gherkinAccumulator);

    A handleStep(Step step, A gherkinAccumulator);

    A handleTableCell(TableCell tableCell, A gherkinAccumulator);

    A handleTableRow(TableRow tableRow, A gherkinAccumulator);

    A handleTag(Tag tag, A gherkinAccumulator);

    A handleComment(Comment comment, A gherkinAccumulator);

    void appendNewLine(A gherkinDocumentPrinterResult);
}
