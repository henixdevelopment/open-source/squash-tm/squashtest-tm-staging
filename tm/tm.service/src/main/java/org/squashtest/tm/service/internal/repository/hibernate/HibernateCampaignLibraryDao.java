/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_CAMPAIGN_LIBRARY_CONTENT_BY_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;

import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.service.internal.repository.CampaignLibraryDao;

@Repository
public class HibernateCampaignLibraryDao
        extends HibernateLibraryDao<CampaignLibrary, CampaignLibraryNode>
        implements CampaignLibraryDao {

    @Override
    public CampaignLibrary loadContainerForPaste(long id) {
        return entityManager
                .createQuery(FIND_CAMPAIGN_LIBRARY_CONTENT_BY_ID, CampaignLibrary.class)
                .setParameter(ID, id)
                .getSingleResult();
    }

    @Override
    public CampaignLibrary loadForNodeAddition(Long libraryId) {
        return entityManager
                .createQuery(FIND_CAMPAIGN_LIBRARY_CONTENT_BY_ID, CampaignLibrary.class)
                .setParameter(ID, libraryId)
                .getSingleResult();
    }
}
