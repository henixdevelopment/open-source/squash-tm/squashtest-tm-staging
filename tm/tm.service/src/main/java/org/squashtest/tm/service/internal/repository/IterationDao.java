/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import org.apache.commons.collections.MultiMap;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseExecutionStatus;
import org.squashtest.tm.domain.testcase.TestStep;

public interface IterationDao extends EntityDao<Iteration> {

    List<Iteration> findAllIterationContainingTestCase(long testCaseId);

    List<NamedReference> findAllTestSuitesAsNamedReferences(long iterationId);

    void persistIterationAndTestPlan(Iteration iteration);

    /**
     * For Feat 4434. Compute a Map<ExecutionStatus,Integer> by following the chain : {@link
     * RequirementVersionCoverage} -> {@link TestCase} -> {@link IterationTestPlanItem}.<br>
     * Filtered by the perimeter : the {@link List} of {@link Iteration}. This method WILL NOT take
     * account of results induced by {@link RequirementVersion} linked to {@link TestStep}. They will
     * be treated as if they were linked only to their {@link TestCase}
     */
    List<TestCaseExecutionStatus> findExecStatusForIterationsAndTestCases(
            List<Long> testCasesIds, List<Long> iterationsIds);

    List<Long> findVerifiedTcIdsInIterations(List<Long> tcIds, List<Long> iterationsIds);

    List<Long> findVerifiedTcIdsInIterationsWithExecution(List<Long> tcIds, List<Long> iterationsIds);

    MultiMap findVerifiedITPI(List<Long> tcIds, List<Long> iterationsIds);

    Iteration findByUUID(String targetUUID);

    Long getProjectId(long iterationId);

    List<Long> findTestPlanIds(@Param("id") long iterationId);

    Iteration loadForExecutionResume(long iterationId);

    boolean hasDeletedTestCaseInTestPlan(long iterationId);
}
