/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.inline;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ATTACHMENT_LIST_ID;

import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.requirement.QRequirementVersion;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testcase.QTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.jooq.domain.tables.ActionTestStep;
import org.squashtest.tm.jooq.domain.tables.TestCaseLibraryNode;
import org.squashtest.tm.jooq.domain.tables.TestCaseSteps;
import org.squashtest.tm.service.internal.repository.AttachmentListDao;
import org.squashtest.tm.service.internal.repository.hibernate.attachmentlist.querybuilder.AttachmentListQueryBuilder;

@Repository
public class HibernateAttachmentListDao implements AttachmentListDao {

    private static final Map<String, Class<?>> ENTITY_CLASS_MAP =
            Map.of(
                    EntityType.PROJECT.toString(),
                    GenericProject.class,
                    EntityType.REQUIREMENT_VERSION.toString(),
                    RequirementVersion.class,
                    EntityType.TEST_CASE.toString(),
                    TestCase.class,
                    EntityType.CAMPAIGN.toString(),
                    Campaign.class,
                    EntityType.ITERATION.toString(),
                    org.squashtest.tm.domain.campaign.Iteration.class,
                    EntityType.TEST_SUITE.toString(),
                    org.squashtest.tm.domain.campaign.TestSuite.class,
                    EntityType.EXECUTION.toString(),
                    org.squashtest.tm.domain.execution.Execution.class,
                    EntityType.SESSION_NOTE.toString(),
                    org.squashtest.tm.domain.execution.SessionNote.class,
                    EntityType.EXECUTION_STEP.toString(),
                    org.squashtest.tm.domain.execution.ExecutionStep.class);

    private static final Map<String, Class<?>> EXTENDED_ENTITY_CLASS_MAP;

    static {
        EXTENDED_ENTITY_CLASS_MAP = new HashMap<>(ENTITY_CLASS_MAP);
        EXTENDED_ENTITY_CLASS_MAP.put(
                EntityType.REQUIREMENT_LIBRARY.toString(), RequirementLibrary.class);
        EXTENDED_ENTITY_CLASS_MAP.put(
                EntityType.REQUIREMENT_FOLDER.toString(), RequirementFolder.class);
        EXTENDED_ENTITY_CLASS_MAP.put(EntityType.CAMPAIGN_LIBRARY.toString(), CampaignLibrary.class);
        EXTENDED_ENTITY_CLASS_MAP.put(EntityType.CAMPAIGN_FOLDER.toString(), CampaignFolder.class);
        EXTENDED_ENTITY_CLASS_MAP.put(EntityType.TEST_CASE_LIBRARY.toString(), TestCaseLibrary.class);
        EXTENDED_ENTITY_CLASS_MAP.put(EntityType.TEST_CASE_FOLDER.toString(), TestCaseFolder.class);
        EXTENDED_ENTITY_CLASS_MAP.put(
                EntityType.ACTION_WORD_LIBRARY.toString(), ActionWordLibrary.class);
        EXTENDED_ENTITY_CLASS_MAP.put(EntityType.SPRINT.toString(), Sprint.class);
        EXTENDED_ENTITY_CLASS_MAP.put(
                EntityType.EXPLORATORY_SESSION_OVERVIEW.toString(), ExploratorySessionOverview.class);
        EXTENDED_ENTITY_CLASS_MAP.put(EntityType.SPRINT_GROUP.toString(), SprintGroup.class);
    }

    private static final String ENTITY_TYPE = "entity_type";
    private static final String ENTITY_ID = "entity_id";

    @PersistenceContext private EntityManager entityManager;

    @Inject private DSLContext dsl;

    @Override
    public AttachmentList getOne(Long id) {
        return entityManager.getReference(AttachmentList.class, id);
    }

    @Override
    public TestCase findAssociatedTestCaseIfExists(Long attachmentListId) {
        final QTestCase testCase = QTestCase.testCase;
        return new JPAQueryFactory(entityManager)
                .selectFrom(testCase)
                .where(testCase.attachmentList.id.eq(attachmentListId))
                .fetchOne();
    }

    @Override
    public RequirementVersion findAssociatedRequirementVersionIfExists(Long attachmentListId) {
        final QRequirementVersion req = QRequirementVersion.requirementVersion;

        return new JPAQueryFactory(entityManager)
                .selectFrom(req)
                .where(req.attachmentList.id.eq(attachmentListId))
                .fetchOne();
    }

    @Override
    public Long findAttachmentListIdByAttachmentId(Long attachmentId) {
        return dsl.select(ATTACHMENT.ATTACHMENT_LIST_ID)
                .from(ATTACHMENT)
                .where(ATTACHMENT.ATTACHMENT_ID.eq(attachmentId))
                .fetchOne(ATTACHMENT.ATTACHMENT_LIST_ID);
    }

    @Override
    public AuditableMixin findAuditableAssociatedEntityIfExists(
            Long attachmentListId, EntityType holderType) {
        Class<?> holderClass = holderType.getEntityClass();

        if (!AuditableMixin.class.isAssignableFrom(holderClass)) {
            return null;
        }

        String query = new AttachmentListQueryBuilder().getHolderEntityQuery(holderType);

        return entityManager
                .createQuery(query, AuditableMixin.class)
                .setParameter(ATTACHMENT_LIST_ID, attachmentListId)
                .getSingleResult();
    }

    @Override
    public AttachmentHolder findAttachmentHolder(Long attachmentListId) {
        Record2<String, Long> jooqRecord = getAssociatedEntityTypeAndId(attachmentListId);

        if (jooqRecord != null) {
            String entityType = jooqRecord.get(ENTITY_TYPE, String.class);
            long entityId = jooqRecord.get(ENTITY_ID, Long.class);

            Class<?> entityClass = EXTENDED_ENTITY_CLASS_MAP.get(entityType);
            if (entityClass != null) {
                return (AttachmentHolder) entityManager.find(entityClass, entityId);
            }
        } else {
            // Because automated suite id is string ...
            String automatedSuiteId = getAssociatedAutomatedSuite(attachmentListId);
            if (automatedSuiteId != null) {
                return entityManager.find(AutomatedSuite.class, automatedSuiteId);
            }
        }
        throw new IllegalArgumentException(
                "No entity class holder found for attachment list id : " + attachmentListId);
    }

    @Override
    public AttachmentHolder findAttachmentHolder(Long attachmentListId, EntityType entityType) {
        Class<?> holderClass = entityType.getEntityClass();

        if (!AttachmentHolder.class.isAssignableFrom(holderClass)) {
            throw new IllegalArgumentException(
                    "Entity type " + entityType + " is not an attachment holder");
        }

        String query = new AttachmentListQueryBuilder().getHolderEntityQuery(entityType);

        return entityManager
                .createQuery(query, AttachmentHolder.class)
                .setParameter(ATTACHMENT_LIST_ID, attachmentListId)
                .getSingleResult();
    }

    private Record2<String, Long> getAssociatedEntityTypeAndId(Long attachmentListId) {
        return dsl.select(
                        field(
                                        when(TEST_CASE_FOLDER.TCLN_ID.isNull(), inline(EntityType.TEST_CASE.toString()))
                                                .otherwise(inline(EntityType.TEST_CASE_FOLDER.toString())))
                                .as(ENTITY_TYPE),
                        TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ENTITY_ID))
                .from(TEST_CASE_LIBRARY_NODE)
                .leftJoin(TEST_CASE_FOLDER)
                .on(TEST_CASE_FOLDER.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .where(TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID.eq(attachmentListId))
                .union(
                        dsl.select(
                                        inline(EntityType.TEST_CASE.toString()).as(ENTITY_TYPE),
                                        TestCaseLibraryNode.TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ENTITY_ID))
                                .from(TestCaseLibraryNode.TEST_CASE_LIBRARY_NODE)
                                .innerJoin(TestCaseSteps.TEST_CASE_STEPS)
                                .on(
                                        TestCaseSteps.TEST_CASE_STEPS.TEST_CASE_ID.eq(
                                                TestCaseLibraryNode.TEST_CASE_LIBRARY_NODE.TCLN_ID))
                                .innerJoin(ActionTestStep.ACTION_TEST_STEP)
                                .on(
                                        ActionTestStep.ACTION_TEST_STEP.TEST_STEP_ID.eq(
                                                TestCaseSteps.TEST_CASE_STEPS.STEP_ID))
                                .where(ActionTestStep.ACTION_TEST_STEP.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        field(
                                                        when(
                                                                        CAMPAIGN_FOLDER.CLN_ID.isNotNull(),
                                                                        inline(EntityType.CAMPAIGN_FOLDER.toString()))
                                                                .when(
                                                                        SPRINT.CLN_ID.isNotNull(), inline(EntityType.SPRINT.toString()))
                                                                .when(
                                                                        SPRINT_GROUP.CLN_ID.isNotNull(),
                                                                        inline(EntityType.SPRINT_GROUP.toString()))
                                                                .otherwise(inline(EntityType.CAMPAIGN.toString())))
                                                .as(ENTITY_TYPE),
                                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(ENTITY_ID))
                                .from(CAMPAIGN_LIBRARY_NODE)
                                .leftJoin(CAMPAIGN_FOLDER)
                                .on(CAMPAIGN_FOLDER.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                .leftJoin(SPRINT)
                                .on(SPRINT.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                .leftJoin(SPRINT_GROUP)
                                .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(SPRINT_GROUP.CLN_ID))
                                .where(CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        field(
                                                        when(
                                                                        REQUIREMENT_FOLDER.RLN_ID.isNull(),
                                                                        inline(EntityType.REQUIREMENT_VERSION.toString()))
                                                                .otherwise(inline(EntityType.REQUIREMENT_FOLDER.toString())))
                                                .as(ENTITY_TYPE),
                                        field(
                                                        when(REQUIREMENT_FOLDER.RLN_ID.isNull(), RESOURCE.RES_ID)
                                                                .otherwise(REQUIREMENT_FOLDER.RLN_ID))
                                                .as(ENTITY_ID))
                                .from(RESOURCE)
                                .leftJoin(REQUIREMENT_FOLDER)
                                .on(REQUIREMENT_FOLDER.RES_ID.eq(RESOURCE.RES_ID))
                                .where(RESOURCE.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.ITERATION.toString()).as(ENTITY_TYPE),
                                        ITERATION.ITERATION_ID.as(ENTITY_ID))
                                .from(ITERATION)
                                .where(ITERATION.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.TEST_SUITE.toString()).as(ENTITY_TYPE),
                                        TEST_SUITE.ID.as(ENTITY_ID))
                                .from(TEST_SUITE)
                                .where(TEST_SUITE.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.PROJECT.toString()).as(ENTITY_TYPE),
                                        PROJECT.PROJECT_ID.as(ENTITY_ID))
                                .from(PROJECT)
                                .where(PROJECT.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.EXECUTION_STEP.toString()).as(ENTITY_TYPE),
                                        EXECUTION_STEP.EXECUTION_STEP_ID.as(ENTITY_ID))
                                .from(EXECUTION_STEP)
                                .where(EXECUTION_STEP.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.REQUIREMENT_LIBRARY.toString()).as(ENTITY_TYPE),
                                        REQUIREMENT_LIBRARY.RL_ID.as(ENTITY_ID))
                                .from(REQUIREMENT_LIBRARY)
                                .where(REQUIREMENT_LIBRARY.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.TEST_CASE_LIBRARY.toString()).as(ENTITY_TYPE),
                                        TEST_CASE_LIBRARY.TCL_ID.as(ENTITY_ID))
                                .from(TEST_CASE_LIBRARY)
                                .where(TEST_CASE_LIBRARY.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.CAMPAIGN_LIBRARY.toString()).as(ENTITY_TYPE),
                                        CAMPAIGN_LIBRARY.CL_ID.as(ENTITY_ID))
                                .from(CAMPAIGN_LIBRARY)
                                .where(CAMPAIGN_LIBRARY.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.EXPLORATORY_SESSION_OVERVIEW.toString()).as(ENTITY_TYPE),
                                        EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.as(ENTITY_ID))
                                .from(EXPLORATORY_SESSION_OVERVIEW)
                                .where(EXPLORATORY_SESSION_OVERVIEW.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.SESSION_NOTE.toString()).as(ENTITY_TYPE),
                                        SESSION_NOTE.NOTE_ID.as(ENTITY_ID))
                                .from(SESSION_NOTE)
                                .where(SESSION_NOTE.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .union(
                        dsl.select(
                                        inline(EntityType.EXECUTION.toString()).as(ENTITY_TYPE),
                                        EXECUTION.EXECUTION_ID.as(ENTITY_ID))
                                .from(EXECUTION)
                                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                                .leftJoin(AUTOMATED_SUITE)
                                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .where(
                                        EXECUTION
                                                .ATTACHMENT_LIST_ID
                                                .eq(attachmentListId)
                                                .or(AUTOMATED_SUITE.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                                .limit(1))
                .union(
                        dsl.select(
                                        inline(EntityType.ACTION_WORD_LIBRARY.toString()).as(ENTITY_TYPE),
                                        ACTION_WORD_LIBRARY.AWL_ID.as(ENTITY_ID))
                                .from(ACTION_WORD_LIBRARY)
                                .where(ACTION_WORD_LIBRARY.ATTACHMENT_LIST_ID.eq(attachmentListId)))
                .fetchOne();
    }

    private String getAssociatedAutomatedSuite(Long attachmentListId) {
        return dsl.select(AUTOMATED_SUITE.SUITE_ID)
                .from(AUTOMATED_SUITE)
                .where(AUTOMATED_SUITE.ATTACHMENT_LIST_ID.eq(attachmentListId))
                .fetchOneInto(String.class);
    }
}
