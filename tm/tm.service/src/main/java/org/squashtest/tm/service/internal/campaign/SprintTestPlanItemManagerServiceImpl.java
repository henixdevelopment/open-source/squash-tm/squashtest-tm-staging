/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.EXECUTE_TPI_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.WRITE_TPI_OR_ROLE_ADMIN;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.ObjectIdentityRetrievalStrategy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintReqVersion;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.exception.campaign.SprintNotLinkableException;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.campaign.SprintTestPlanItemManagerService;
import org.squashtest.tm.service.execution.ExecutionFinder;
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintTestPlanItemDisplayDao;
import org.squashtest.tm.service.security.Authorizations;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;

@Service
@Transactional
public class SprintTestPlanItemManagerServiceImpl implements SprintTestPlanItemManagerService {

    private final CampaignNodeDeletionHandler deletionHandler;
    private final PermissionEvaluationService permissionEvaluationService;
    private final ExecutionFinder executionFinder;
    private final UserDao userDao;
    private final SprintTestPlanItemDisplayDao sprintTestPlanItemDisplayDao;
    private final ObjectIdentityRetrievalStrategy objectIdentityRetrievalStrategy;
    private final ObjectAclService objectAclService;
    private final SprintReqVersionDao sprintReqVersionDao;
    private final SprintDisplayDao sprintDisplayDao;

    @PersistenceContext private EntityManager entityManager;

    public SprintTestPlanItemManagerServiceImpl(
            CampaignNodeDeletionHandler deletionHandler,
            PermissionEvaluationService permissionEvaluationService,
            ExecutionFinder executionFinder,
            UserDao userDao,
            SprintTestPlanItemDisplayDao sprintTestPlanItemDisplayDao,
            @Qualifier("squashtest.core.security.ObjectIdentityRetrievalStrategy")
                    ObjectIdentityRetrievalStrategy objectIdentityRetrievalStrategy,
            ObjectAclService objectAclService,
            SprintReqVersionDao sprintReqVersionDao,
            SprintDisplayDao sprintDisplayDao) {
        this.deletionHandler = deletionHandler;
        this.permissionEvaluationService = permissionEvaluationService;
        this.executionFinder = executionFinder;
        this.userDao = userDao;
        this.sprintTestPlanItemDisplayDao = sprintTestPlanItemDisplayDao;
        this.objectIdentityRetrievalStrategy = objectIdentityRetrievalStrategy;
        this.objectAclService = objectAclService;
        this.sprintReqVersionDao = sprintReqVersionDao;
        this.sprintDisplayDao = sprintDisplayDao;
    }

    @Override
    @PreAuthorize(EXECUTE_TPI_OR_ROLE_ADMIN)
    public void changeDataset(long testPlanItemId, Long datasetId) {
        final TestPlanItem item = findTestPlanItemAndCheckSprintStatus(testPlanItemId);

        if (datasetId == null) {
            item.setReferencedDataset(null);
        } else {
            final Dataset dataset = entityManager.find(Dataset.class, datasetId);
            item.setReferencedDataset(dataset);
        }
    }

    @Override
    public void deleteExecutionsAndRemoveFromTestPlanItem(
            List<Long> executionIds, long testPlanItemId) {
        checkParentSprintStatus(testPlanItemId);
        checkDeleteExecutionPermission(executionIds);

        final List<Execution> executions = executionFinder.findAllById(executionIds);
        deletionHandler.deleteExecutions(executions);

        final TestPlanItem testPlanItem = entityManager.find(TestPlanItem.class, testPlanItemId);
        testPlanItem.removeExecutionsById(executionIds);
    }

    private void checkParentSprintStatus(long testPlanItemId) {
        SprintStatus sprintStatus = sprintDisplayDao.getSprintStatusByTestPlanItemId(testPlanItemId);
        if (SprintStatus.CLOSED.equals(sprintStatus)) {
            throw new SprintClosedException();
        }
    }

    private void checkDeleteExecutionPermission(List<Long> executionIds) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                executionIds,
                Permissions.DELETE_EXECUTION.name(),
                Execution.class.getName());
    }

    @Override
    public void applyFastPass(List<Long> testPlanItemIds, String statusName) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                testPlanItemIds,
                Permissions.EXECUTE.name(),
                TestPlanItem.class.getName());

        final List<SprintReqVersion> sprintReqVersions =
                sprintReqVersionDao.findWithSprintByTestPlanItemIds(testPlanItemIds);
        final Map<SprintReqVersion, List<TestPlanItem>> testPlanItemsBySprintReqVersion =
                sprintReqVersions.stream()
                        .collect(
                                Collectors.toMap(
                                        sprintReqVersion -> sprintReqVersion,
                                        sprintReqVersion ->
                                                sprintReqVersion.getTestPlan().getTestPlanItems().stream()
                                                        .filter(item -> testPlanItemIds.contains(item.getId()))
                                                        .toList()));

        final ExecutionStatus status = ExecutionStatus.valueOf(statusName);
        final String login = UserContextHolder.getUsername();
        final User user = userDao.findUserByLogin(login);

        if (testPlanItemsBySprintReqVersion.entrySet().stream()
                .allMatch(entry -> SprintStatus.CLOSED.equals(entry.getKey().getSprint().getStatus()))) {
            throw new SprintClosedException();
        } else {
            testPlanItemsBySprintReqVersion.forEach(
                    (sprintReqVersion, testPlanItems) -> {
                        if (!SprintStatus.CLOSED.equals(sprintReqVersion.getSprint().getStatus())) {
                            testPlanItems.forEach(testPlanItem -> testPlanItem.applyFastPass(status, user));
                        }
                    });
        }
    }

    @Override
    @PreAuthorize(Authorizations.READ_TPI_OR_ROLE_ADMIN)
    public List<User> findAssignableUsersByTestPlanItemId(Long testPlanItemId) {
        final Long campaignLibraryId =
                sprintTestPlanItemDisplayDao.findCampaignLibraryIdByTestPlanItemId(testPlanItemId);
        return findAssignableUsersByCampaignLibraryId(campaignLibraryId);
    }

    @Override
    @PreAuthorize(Authorizations.READ_SESSION_OVERVIEW_OR_ROLE_ADMIN)
    public List<User> findAssignableUsersByExploratorySessionOverviewId(Long sessionOverviewId) {
        final Long campaignLibraryId =
                sprintTestPlanItemDisplayDao.findCampaignLibraryIdByExploratorySessionOverviewId(
                        sessionOverviewId);
        return findAssignableUsersByCampaignLibraryId(campaignLibraryId);
    }

    @Override
    public List<User> findAssignableUsersByCampaignLibraryId(Long libraryId) {
        final CampaignLibrary campaignLibrary = entityManager.find(CampaignLibrary.class, libraryId);
        final ObjectIdentity oid = objectIdentityRetrievalStrategy.getObjectIdentity(campaignLibrary);
        final List<String> loginList = objectAclService.findUsersWithExecutePermission(List.of(oid));
        return userDao.findUsersByLoginList(loginList);
    }

    @Override
    @PreAuthorize(WRITE_TPI_OR_ROLE_ADMIN)
    public void assignUserToTestPlanItem(Long testPlanItemId, long userId) {
        final TestPlanItem item = findTestPlanItemAndCheckSprintStatus(testPlanItemId);
        final User user = entityManager.find(User.class, userId);
        item.setAssignee(user);
    }

    private TestPlanItem findTestPlanItemAndCheckSprintStatus(Long testPlanItemId) {
        final SprintReqVersion sprintReqVersion =
                sprintReqVersionDao.findWithSprintByTestPlanItemId(testPlanItemId);
        final TestPlanItem item =
                sprintReqVersion.getTestPlan().getTestPlanItems().stream()
                        .filter(i -> i.getId().equals(testPlanItemId))
                        .findFirst()
                        .orElseThrow();
        final Sprint sprint = sprintReqVersion.getSprint();

        if (SprintStatus.CLOSED.equals(sprint.getStatus())) {
            throw new SprintClosedException();
        }
        return item;
    }

    @Override
    public void deleteSprintReqVersionTestPlanItems(
            Long sprintReqVersionId, List<Long> testPlanItemIds) {
        final SprintReqVersion sprintReqVersion =
                sprintReqVersionDao.findByIdWithTestPlanAndSprint(sprintReqVersionId);

        if (SprintStatus.CLOSED.equals(sprintReqVersion.getSprint().getStatus())) {
            throw new SprintNotLinkableException();
        }

        Set<TestPlanItem> itemsToRemove =
                sprintReqVersion.getTestPlan().getTestPlanItems().stream()
                        .filter(item -> testPlanItemIds.contains(item.getId()))
                        .collect(Collectors.toSet());

        itemsToRemove.forEach(item -> checkPermissionsAndRemoveItem(sprintReqVersion, item));

        sprintReqVersion.updateLastModificationWithCurrentUser();
    }

    private void checkPermissionsAndRemoveItem(SprintReqVersion sprintReqVersion, TestPlanItem item) {
        if (item.getExecutions().isEmpty()) {
            if (permissionEvaluationService.hasRoleOrPermissionOnObject(
                    Roles.ROLE_ADMIN, Permissions.DELETE.name(), item)) {
                doRemoveTestPlanItemFromSprintReqVersion(sprintReqVersion, item);
            }
        } else {
            if (permissionEvaluationService.hasRoleOrPermissionOnObject(
                    Roles.ROLE_ADMIN, Permissions.EXTENDED_DELETE.name(), item)) {
                doRemoveTestPlanItemFromSprintReqVersion(sprintReqVersion, item);
            }
        }
    }

    private void doRemoveTestPlanItemFromSprintReqVersion(
            SprintReqVersion sprintReqVersion, TestPlanItem item) {
        sprintReqVersion.getTestPlan().removeTestPlanItem(item);
        deletionHandler.deleteSprintReqVersionTestPlanItem(item);
    }

    @Override
    public void moveSprintReqVersionTestPlanItems(
            long sprintReqVersionId, List<Long> testPlanItemIds, int position) {
        final SprintReqVersion sprintReqVersion =
                sprintReqVersionDao.findByIdWithTestPlanAndSprint(sprintReqVersionId);

        if (SprintStatus.CLOSED.equals(sprintReqVersion.getSprint().getStatus())) {
            throw new SprintNotLinkableException();
        }

        final List<TestPlanItem> testPlanItems =
                sprintReqVersion.getTestPlan().getTestPlanItems().stream()
                        .filter(item -> testPlanItemIds.contains(item.getId()))
                        .toList();
        sprintReqVersion.getTestPlan().moveItems(testPlanItems, position);
    }
}
