/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import org.squashtest.tm.domain.campaign.CampaignStatus;
import org.squashtest.tm.domain.campaign.IterationStatus;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.testcase.TestCaseStatus;

public enum XrayStatus {
    RE_OPENED(
            "reopened",
            RequirementStatus.WORK_IN_PROGRESS,
            TestCaseStatus.WORK_IN_PROGRESS,
            CampaignStatus.PLANNED,
            IterationStatus.PLANNED),
    OPEN(
            "open",
            RequirementStatus.WORK_IN_PROGRESS,
            TestCaseStatus.WORK_IN_PROGRESS,
            CampaignStatus.PLANNED,
            IterationStatus.PLANNED),
    IN_PROGRESS(
            "in progress",
            RequirementStatus.WORK_IN_PROGRESS,
            TestCaseStatus.WORK_IN_PROGRESS,
            CampaignStatus.IN_PROGRESS,
            IterationStatus.IN_PROGRESS),
    RESOLVED(
            "resolved",
            RequirementStatus.UNDER_REVIEW,
            TestCaseStatus.UNDER_REVIEW,
            CampaignStatus.FINISHED,
            IterationStatus.FINISHED),
    CLOSED(
            "closed",
            RequirementStatus.APPROVED,
            TestCaseStatus.APPROVED,
            CampaignStatus.ARCHIVED,
            IterationStatus.ARCHIVED),
    DEFAULT(
            "",
            RequirementStatus.WORK_IN_PROGRESS,
            TestCaseStatus.defaultValue(),
            CampaignStatus.defaultValue(),
            IterationStatus.defaultValue());

    private final String xrayField;
    private final RequirementStatus requirementStatus;
    private final TestCaseStatus testCaseStatus;
    private final CampaignStatus campaignStatus;
    private final IterationStatus iterationStatus;

    XrayStatus(
            String xrayField,
            RequirementStatus requirementStatus,
            TestCaseStatus testCaseStatus,
            CampaignStatus campaignStatus,
            IterationStatus iterationStatus) {
        this.xrayField = xrayField;
        this.requirementStatus = requirementStatus;
        this.testCaseStatus = testCaseStatus;
        this.campaignStatus = campaignStatus;
        this.iterationStatus = iterationStatus;
    }

    public String getXrayField() {
        return xrayField;
    }

    public RequirementStatus getRequirementStatus() {
        return requirementStatus;
    }

    public TestCaseStatus getTestCaseStatus() {
        return testCaseStatus;
    }

    public CampaignStatus getCampaignStatus() {
        return campaignStatus;
    }

    public IterationStatus getIterationStatus() {
        return iterationStatus;
    }

    public static String getXrayFieldMapping() {
        return Arrays.stream(XrayStatus.values())
                .filter(xrayStatus -> xrayStatus != DEFAULT)
                .map(XrayStatus::getXrayField)
                .collect(Collectors.joining(", "));
    }

    public static XrayStatus getStatus(String status) {
        if (Objects.isNull(status)) {
            return DEFAULT;
        }
        return Arrays.stream(XrayStatus.values())
                .filter(
                        xrayStatus -> xrayStatus.getXrayField().toLowerCase().contains(status.toLowerCase()))
                .findFirst()
                .orElse(DEFAULT);
    }
}
