/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.artificialintelligence.testcasegeneration;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.exception.artificialintelligence.server.AiServerJsonResponseParsingException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.AiServerBadPayloadTemplateException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.AiServerInitializeRemoteActionException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.AiServerMalformedUrlException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.AiServerNotConfiguredException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.AiServerResponseInoperableException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.AiServerWrongTokenException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.AiServerWrongUrlException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.RemoteAiServerActionException;
import org.squashtest.tm.exception.artificialintelligence.testcasegeneration.UnextractableAiServerResponseException;
import org.squashtest.tm.service.annotation.IsUltimateLicenseAvailable;
import org.squashtest.tm.service.artificialintelligence.testcasegeneration.AiTestCaseGenerationService;
import org.squashtest.tm.service.internal.display.dto.AiServerDto;
import org.squashtest.tm.service.internal.dto.ActionTestStepFromAiDto;
import org.squashtest.tm.service.internal.dto.TestCaseFromAiDto;
import org.squashtest.tm.service.internal.dto.TestCasesCreationFromAiDto;
import org.squashtest.tm.service.internal.repository.AiTestCaseGenerationDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;

@Service
@Transactional
public class AiTestCaseGenerationServiceImpl implements AiTestCaseGenerationService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(AiTestCaseGenerationServiceImpl.class);
    private static final String NO_RESPONSE_FROM_SERVER =
            "sqtm-core.exception.artificial-intelligence.generate-test-case.cannot-get-response";
    private static final String NO_RESPONSE_MESSAGE_FROM_SERVER =
            "sqtm-core.exception.artificial-intelligence.generate-test-case.cannot-get-response-message";

    private final EntityManager entityManager;
    private final RequirementVersionDao requirementVersionDao;
    private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;
    private final AiTestCaseGenerationDao aiTestCaseGenerationDao;
    private final ActiveMilestoneHolder activeMilestoneHolder;
    private final StoredCredentialsManager credentialsManager;

    public AiTestCaseGenerationServiceImpl(
            EntityManager entityManager,
            RequirementVersionDao requirementVersionDao,
            TestCaseLibraryNavigationService testCaseLibraryNavigationService,
            AiTestCaseGenerationDao aiTestCaseGenerationDao,
            ActiveMilestoneHolder activeMilestoneHolder,
            StoredCredentialsManager credentialsManager) {
        this.entityManager = entityManager;
        this.requirementVersionDao = requirementVersionDao;
        this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
        this.aiTestCaseGenerationDao = aiTestCaseGenerationDao;
        this.activeMilestoneHolder = activeMilestoneHolder;
        this.credentialsManager = credentialsManager;
    }

    @Override
    @IsUltimateLicenseAvailable
    public Long findTargetTestCaseLibraryId(Long projectId) {
        return aiTestCaseGenerationDao.findTargetTestCaseLibraryId(projectId);
    }

    @Override
    @IsUltimateLicenseAvailable
    public String generateTestCaseFromRequirementUsingAiUserSide(
            long projectIdLinkedToRequirement, String requirement) {
        AiServerDto aiServerDto =
                aiTestCaseGenerationDao.findAiServerDtoByProjectId(projectIdLinkedToRequirement);
        if (aiServerDto.getPayloadTemplate() == null || aiServerDto.getJsonPath() == null) {
            throw new AiServerNotConfiguredException();
        }
        String parsedRequirement = HTMLCleanupUtils.htmlToLayoutAwareText(requirement, true);

        return generateTestCaseFromRequirementUsingAi(aiServerDto, parsedRequirement);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public String testAiServerConfiguration(long serverId, String requirement) {
        AiServerDto aiServerDto = aiTestCaseGenerationDao.findAiServerDtoByAiServerId(serverId);
        return generateTestCaseFromRequirementUsingAi(aiServerDto, requirement);
    }

    public String generateTestCaseFromRequirementUsingAi(
            AiServerDto aiServerDto, String requirementDescription) {

        TokenAuthCredentials credentials =
                (TokenAuthCredentials)
                        credentialsManager.unsecuredFindAppLevelCredentials(aiServerDto.getId());
        HttpURLConnection connection = prepareConnection(credentials, aiServerDto.getUrl());

        String jsonPayload =
                getPayloadAsJsonString(
                        JSONObject.escape(requirementDescription).replace("\"", "\\\""),
                        aiServerDto.getPayloadTemplate());

        StringBuilder response = buildAndSendRequest(jsonPayload, connection);
        int responseCode;
        try {
            responseCode = connection.getResponseCode();
        } catch (IOException e) {
            LOGGER.error("Cannot get response code from server", e);
            throw new RemoteAiServerActionException(e.getMessage(), response.toString());
        }

        if (responseCode == HttpURLConnection.HTTP_OK) {
            String extractedServerResponse =
                    extractServerResponse(aiServerDto.getJsonPath(), response.toString());
            return cleanServerResponse(response.toString(), extractedServerResponse);

        } else {
            LOGGER.error("Expected code 200. Response received is: {}", response);
            throw new RemoteAiServerActionException(String.valueOf(responseCode), response.toString());
        }
    }

    public String cleanServerResponse(String originalResponse, String extractedServerResponse) {
        Pattern patternMarkDown = Pattern.compile("```json\\s*\\n([^`]*)```", Pattern.DOTALL);
        Matcher matcherMarkDown = patternMarkDown.matcher(extractedServerResponse);
        if (matcherMarkDown.find()) {
            extractedServerResponse = matcherMarkDown.group(1);
        }
        String testCasesPattern = "\\{\\s*\"testCases\"\\s*:\\s*\\[.*]\\s*}";
        Pattern patternTestCases = Pattern.compile(testCasesPattern, Pattern.DOTALL);
        Matcher matcherForTestCases = patternTestCases.matcher(extractedServerResponse);
        if (matcherForTestCases.find()) {
            return matcherForTestCases.group();
        } else {
            throw new AiServerResponseInoperableException(originalResponse);
        }
    }

    /*
    This code is only used for the artificial intelligence feature for the moment.
    If needed, refactor this by creating a utility class (ExtractResponseAtJsonPathUtils for example)
     */
    public String extractServerResponse(String jsonPath, String originalServerResponse) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            JsonNode responseRoot = objectMapper.readTree(originalServerResponse);
            String transformedJsonPath = transformToJsonPointer(jsonPath);
            JsonNode result = responseRoot.at(transformedJsonPath);

            if (!result.isMissingNode()) {
                return result.asText();
            } else {
                throw new UnextractableAiServerResponseException(originalServerResponse);
            }

        } catch (JsonProcessingException e) {
            LOGGER.error("Cannot parse the response into a JSON tree structure", e);
            throw new AiServerJsonResponseParsingException(originalServerResponse);
        }
    }

    public String transformToJsonPointer(String jsonPath) {
        // If the path starts with a [, there is no need to add the / at the beginning since all
        // brackets are already replaced with /
        String firstChar = "[".equals(jsonPath.substring(0, 1)) ? "" : "/";
        return firstChar + jsonPath.replace(".", "/").replace("[", "/").replace("]", "");
    }

    private static HttpURLConnection prepareConnection(
            TokenAuthCredentials credentials, String apiUrl) {
        URL url;
        try {
            url = new URL(apiUrl);
        } catch (MalformedURLException e) {
            LOGGER.error("The URl is not correctly formed.", e);
            throw new AiServerMalformedUrlException();
        }
        return getHttpUrlConnection(credentials, url);
    }

    private static HttpURLConnection getHttpUrlConnection(TokenAuthCredentials credentials, URL url) {
        HttpURLConnection connection;

        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            LOGGER.error("Cannot open https connection.", e);
            throw new AiServerInitializeRemoteActionException();
        }

        try {
            connection.setRequestMethod("POST");
        } catch (IOException e) {
            LOGGER.error("Cannot set the POST method for the request.", e);
            throw new AiServerInitializeRemoteActionException();
        }

        connection.setDoOutput(true);

        if (credentials != null) {
            connection.setRequestProperty(HttpHeaders.AUTHORIZATION, "Bearer " + credentials.getToken());
        }
        connection.setRequestProperty(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        connection.setRequestProperty(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
        connection.setRequestProperty(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        return connection;
    }

    /*
    This code is only used for the artificial intelligence feature for the moment.
    If needed, refactor this by creating a utility class (MustacheUtils for example)
    */
    public String getPayloadAsJsonString(String requirementDescription, String payloadTemplate) {
        Map<String, String> scope = Collections.singletonMap("requirement", requirementDescription);

        MustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache mustache = mustacheFactory.compile(new StringReader(payloadTemplate), "template");

        StringWriter writer = new StringWriter();
        mustache.execute(writer, scope);

        return writer.toString();
    }

    private static StringBuilder buildAndSendRequest(
            String jsonPayload, HttpURLConnection connection) {
        byte[] input = jsonPayload.getBytes(StandardCharsets.UTF_8);
        connection.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(input.length));

        try (OutputStream os = connection.getOutputStream()) {
            os.write(input, 0, input.length);
        } catch (IOException e) {
            LOGGER.error("Cannot send the request to the remote server", e);
            throw new AiServerInitializeRemoteActionException();
        }

        int responseCode = getResponseCode(connection);
        String responseMessage = getResponseMessage(connection, responseCode);

        StringBuilder response = new StringBuilder();

        if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
            handleErrorResponseFromServer(connection, response, responseCode, responseMessage);
        } else {
            handleServerResponse(connection, response, responseCode, responseMessage);
        }

        return response;
    }

    private static int getResponseCode(HttpURLConnection connection) {
        int responseCode;
        try {
            responseCode = connection.getResponseCode();
        } catch (IOException e) {
            LOGGER.error("Error getting response code from the server", e);
            throw new RemoteAiServerActionException(NO_RESPONSE_FROM_SERVER, null);
        }
        return responseCode;
    }

    private static String getResponseMessage(HttpURLConnection connection, int responseCode) {
        String responseMessage;
        try {
            responseMessage = connection.getResponseMessage();
        } catch (IOException e) {
            LOGGER.error("Error getting response message from the server", e);
            throw new RemoteAiServerActionException(
                    NO_RESPONSE_MESSAGE_FROM_SERVER, String.valueOf(responseCode));
        }
        return responseMessage;
    }

    private static void handleErrorResponseFromServer(
            HttpURLConnection connection,
            StringBuilder response,
            int responseCode,
            String responseMessage) {
        try (BufferedReader errorReader =
                new BufferedReader(
                        new InputStreamReader(connection.getErrorStream(), StandardCharsets.UTF_8))) {
            String errorLine;
            while ((errorLine = errorReader.readLine()) != null) {
                response.append(errorLine);
            }
        } catch (IOException e) {
            LOGGER.error("Error reading the error response from the server", e);
            throw new RemoteAiServerActionException(
                    responseCode + " " + responseMessage, response.toString());
        }

        LOGGER.error(
                "Error response from the server. HTTP Status Code: {}. Error message: {}",
                responseCode,
                responseMessage);
        switch (responseCode) {
            case HttpURLConnection.HTTP_UNAUTHORIZED ->
                    throw new AiServerWrongTokenException(
                            responseCode + " " + responseMessage, response.toString());
            case HttpURLConnection.HTTP_NOT_FOUND ->
                    throw new AiServerWrongUrlException(
                            responseCode + " " + responseMessage, response.toString());
            case HttpURLConnection.HTTP_BAD_REQUEST ->
                    throw new AiServerBadPayloadTemplateException(
                            responseCode + " " + responseMessage, response.toString());
            default ->
                    throw new RemoteAiServerActionException(
                            responseCode + " " + responseMessage, response.toString());
        }
    }

    private static void handleServerResponse(
            HttpURLConnection connection,
            StringBuilder response,
            int responseCode,
            String responseMessage) {
        try (BufferedReader reader =
                new BufferedReader(
                        new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            LOGGER.error("Remote server cannot finalize the request", e);
            throw new RemoteAiServerActionException(
                    responseCode + " " + responseMessage, response.toString());
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#targetLibraryId, 'org.squashtest.tm.domain.testcase.TestCaseLibrary' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void persistTestCaseFromAi(
            long targetLibraryId, Long requirementVersionId, TestCasesCreationFromAiDto testCasesData) {
        EntityReference destinationEntityReference =
                EntityReference.fromNodeId(testCasesData.getDestinationFolderId());

        List<TestCase> generatedTestCases =
                generateTestCases(testCasesData, destinationEntityReference);

        addTestCasesToNode(destinationEntityReference, generatedTestCases);

        RequirementVersion requirementVersion =
                requirementVersionDao.loadForCoverageAddition(requirementVersionId);

        for (TestCase testCase : generatedTestCases) {
            var coverage = new RequirementVersionCoverage(requirementVersion, testCase);
            entityManager.persist(coverage);
        }
    }

    private void addTestCasesToNode(
            EntityReference destinationEntityReference, List<TestCase> generatedTestCases) {
        List<Long> milestoneIds = findMilestoneIds();

        Long parentId = destinationEntityReference.getId();

        switch (destinationEntityReference.getType()) {
            case TEST_CASE_LIBRARY ->
                    testCaseLibraryNavigationService.addTestCasesToLibrary(
                            parentId, generatedTestCases, milestoneIds);
            case TEST_CASE_FOLDER ->
                    testCaseLibraryNavigationService.addTestCasesToFolder(
                            parentId, generatedTestCases, milestoneIds);
            default ->
                    throw new IllegalArgumentException(
                            "This entity type is not handled " + destinationEntityReference.getType());
        }
    }

    private List<TestCase> generateTestCases(
            TestCasesCreationFromAiDto testCasesData, EntityReference destinationEntityReference) {

        List<String> contentNames = new ArrayList<>(getContentNames(destinationEntityReference));

        return testCasesData.getTestCasesFromAi().stream()
                .map(testCaseFromAiDto -> getNewTestCase(testCaseFromAiDto, contentNames))
                .toList();
    }

    private List<String> getContentNames(EntityReference destinationEntityReference) {
        Long parentId = destinationEntityReference.getId();
        return switch (destinationEntityReference.getType()) {
            case TEST_CASE_LIBRARY ->
                    testCaseLibraryNavigationService.findContentNamesByLibraryId(parentId);
            case TEST_CASE_FOLDER ->
                    testCaseLibraryNavigationService
                            .findContentNamesByFolderIds(Collections.singletonList(parentId))
                            .getOrDefault(parentId, new ArrayList<>());
            default ->
                    throw new IllegalArgumentException(
                            "This entity type is not handled " + destinationEntityReference.getType());
        };
    }

    private TestCase getNewTestCase(
            TestCaseFromAiDto testCaseFromAiDto, List<String> parentContentNames) {
        TestCase newTestCase = new TestCase();
        fixTestCaseDuplicationNames(newTestCase, testCaseFromAiDto.getName(), parentContentNames);
        newTestCase.setDescription(testCaseFromAiDto.getDescription());
        newTestCase.setPrerequisite(testCaseFromAiDto.getPrerequisites());
        for (ActionTestStepFromAiDto actionStep : testCaseFromAiDto.getSteps()) {
            ActionTestStep newStep = new ActionTestStep();
            newStep.setAction(actionStep.getAction());
            newStep.setExpectedResult(actionStep.getExpectedResult());
            newTestCase.addStep(actionStep.getIndex(), newStep);
        }
        newTestCase.setDraftedByAi(true);

        return newTestCase;
    }

    private static void fixTestCaseDuplicationNames(
            TestCase testCase, String name, List<String> parentContentNames) {
        int i = 0;
        String uniqueName = name;

        while (parentContentNames.contains(uniqueName)) {
            i++;
            uniqueName = name + '(' + i + ')';
        }

        testCase.setName(uniqueName);
        parentContentNames.add(uniqueName);
    }

    private List<Long> findMilestoneIds() {
        List<Long> milestoneIds = new ArrayList<>();
        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        activeMilestone.ifPresent(milestone -> milestoneIds.add(milestone.getId()));
        return milestoneIds;
    }
}
