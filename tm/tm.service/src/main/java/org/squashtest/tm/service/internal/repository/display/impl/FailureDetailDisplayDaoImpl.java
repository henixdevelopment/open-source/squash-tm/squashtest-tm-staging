/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.internal.repository.display.FailureDetailDisplayDao;

@Repository
@Transactional(readOnly = true)
public class FailureDetailDisplayDaoImpl implements FailureDetailDisplayDao {

    private final DSLContext dslContext;

    public FailureDetailDisplayDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public Long findProjectIdByFailureDetailId(Long failureDetailId) {
        return dslContext
                .select(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(FAILURE_DETAIL)
                .on(FAILURE_DETAIL.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .where(FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(failureDetailId))
                .groupBy(PROJECT.PROJECT_ID)
                .fetchOneInto(Long.class);
    }
}
