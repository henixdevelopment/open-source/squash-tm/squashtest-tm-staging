/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.sprint;

import java.util.List;
import org.squashtest.tm.service.internal.display.dto.sprint.AvailableTestPlanItemDto;
import org.squashtest.tm.service.internal.display.dto.sprint.AvailableTestPlanItemIdsDto;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintDto;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionDto;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionView;

public interface SprintDisplayService {

    SprintDto getSprintView(long sprintId);

    List<SprintReqVersionDto> findSprintReqVersionDtosBySprintId(long sprintId);

    List<SprintReqVersionDto> findSprintReqVersionDtosForDenormalization(long sprintId);

    SprintReqVersionView findSprintReqVersionViewById(long sprintReqVersionId);

    /**
     * Returns items that can be added to the test plan of the sprint requirement version based on the
     * requirement coverage.
     *
     * @param sprintReqVersionId - the id of the sprint requirement version
     * @return list of available test plan items
     */
    List<AvailableTestPlanItemDto> findAvailableTestPlanItems(long sprintReqVersionId);

    /**
     * Returns items containing ids of testPlans and possibly dataset that can be added to the test
     * plan of the sprint requirement version based on the requirement coverage.
     *
     * @param sprintReqVersionId - the id of the sprint requirement version
     * @return list of available test plan item ids
     */
    List<AvailableTestPlanItemIdsDto> findAvailableTestPlanItemIds(long sprintReqVersionId);
}
