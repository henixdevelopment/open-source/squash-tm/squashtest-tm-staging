/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.internal.dto.WorkflowDto;

@JsonIgnoreProperties(ignoreUnknown = true)
public record WorkflowData(
        String apiVersion, String kind, Metadata metadata, Map<String, WorkflowStatus> status) {

    private static final String PHASE_KEY = "phase";

    public WorkflowDto toWorkflowDto() {
        return new WorkflowDto(
                metadata.workflowId(),
                metadata().name(),
                metadata.namespace(),
                metadata.creationTimestamp(),
                convertStatus(status.get(PHASE_KEY)),
                metadata.completionTimestamp());
    }

    private String convertStatus(WorkflowStatus workflowStatus) {
        return switch (workflowStatus) {
            case DONE -> ExecutionStatus.SUCCESS.name();
            case FAILED -> ExecutionStatus.FAILURE.name();
            case RUNNING -> ExecutionStatus.RUNNING.name();
            case PENDING -> ExecutionStatus.READY.name();
        };
    }

    public enum WorkflowStatus {
        DONE,
        FAILED,
        RUNNING,
        PENDING
    }
}
