/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase.dto;

import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;

public class CallStepImportData {

    private final Long calledTestCaseId;

    private final TestCaseTarget calledTestCaseTarget;

    private final String calledDatasetName;

    private final TestStepTarget target;

    private final Integer index;

    private ParameterAssignationMode parameterMode;

    private CallTestStep callTestStep;

    private Dataset dataset;

    public CallStepImportData(
            Long calledTestCaseId,
            TestCaseTarget calledTestCaseTarget,
            ParameterAssignationMode parameterMode,
            String datasetName,
            TestStepTarget target) {
        this.calledTestCaseId = calledTestCaseId;
        this.calledTestCaseTarget = calledTestCaseTarget;
        this.parameterMode = parameterMode;
        this.calledDatasetName = datasetName;
        this.target = target;
        this.index = target.getIndex();
    }

    public void setCallTestStep(CallTestStep callTestStep) {
        this.callTestStep = callTestStep;
    }

    public void setParameterMode(ParameterAssignationMode parameterMode) {
        this.parameterMode = parameterMode;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public Long getCalledTestCaseId() {
        return calledTestCaseId;
    }

    public ParameterAssignationMode getParameterMode() {
        return parameterMode;
    }

    public String getCalledDatasetName() {
        return calledDatasetName;
    }

    public TestStepTarget getTarget() {
        return target;
    }

    public Integer getIndex() {
        return index;
    }

    public CallTestStep getCallTestStep() {
        return callTestStep;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public TestCaseTarget getCalledTestCaseTarget() {
        return calledTestCaseTarget;
    }
}
