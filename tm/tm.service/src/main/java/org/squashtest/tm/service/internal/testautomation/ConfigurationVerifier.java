/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Objects;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException;
import org.squashtest.tm.service.testautomation.spi.OutdatedSquashOrchestratorException;

public final class ConfigurationVerifier {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationVerifier.class);
    private static final String AUTHORIZATION = "Authorization";
    private static final String GET_METHOD = "GET";
    private static final String POST_METHOD = "POST";
    private static final String DELETE_METHOD = "DELETE";
    private static final String BEARER = "BEARER ";
    private static final String SQUASH_ORCHESTRATOR_ALLINONE_VERSION_DATE_FORMAT = "yyyy-MM";
    private static final String SQUASH_ORCHESTRATOR_MINIMUM_VERSION = "4.14";
    private static final String SQUASH_ORCHESTRATOR_MINIMUM_ALLINONE_REQUIRED_VERSION = "2024-10";
    private static final String SQUASH_ORCHESTRATOR_RELEASED_ALLINONE_VERSION_PREFIX = "20";
    private static final String SQUASH_ORCHESTRATOR_UNREALASED_WARNING_MESSAGE =
            "You are using an unreleased version of Squash Orchestrator, which may cause issues.";
    private static final String SQUASH_ORCHESTRATOR_OUTDATED_EXCEPTION_MESSAGE_KEY =
            "testautomation.exceptions.outdated-squash-orchestrator";
    private static final String SQUASH_VERSION_REGEX = "\\.";

    private ConfigurationVerifier() {
        super();
    }

    public static void verify(
            String token, TestAutomationServer testAutomationServer, MessageSource messageSource) {

        checkTokenValidity(token, messageSource);
        checkReceptionistValidity(testAutomationServer.getUrl(), token, messageSource);
        checkObserverUrlValidity(
                getUrl(testAutomationServer.getUrl(), testAutomationServer.getObserverUrl()),
                token,
                messageSource);
        checkEventBusUrlValidity(
                getUrl(testAutomationServer.getUrl(), testAutomationServer.getEventBusUrl()),
                token,
                messageSource);
        checkKillswitchUrlValidity(
                getUrl(testAutomationServer.getUrl(), testAutomationServer.getKillswitchUrl()),
                token,
                messageSource);
    }

    public static void checkSquashOrchestratorVersionValidity(
            String otfAllinoneVersion, String squashTMVersion, MessageSource messageSource) {

        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern(SQUASH_ORCHESTRATOR_ALLINONE_VERSION_DATE_FORMAT);
        YearMonth referenceDate =
                YearMonth.parse(SQUASH_ORCHESTRATOR_MINIMUM_ALLINONE_REQUIRED_VERSION, formatter);

        if (!otfAllinoneVersion.startsWith(SQUASH_ORCHESTRATOR_RELEASED_ALLINONE_VERSION_PREFIX)) {
            LOGGER.warn(SQUASH_ORCHESTRATOR_UNREALASED_WARNING_MESSAGE);
            return;
        }

        try {
            YearMonth squashOrchestratorVersionDate =
                    YearMonth.parse(otfAllinoneVersion.split(SQUASH_VERSION_REGEX)[0], formatter);
            if (squashOrchestratorVersionDate.isBefore(referenceDate)) {
                String[] squashTMVersionSplit = squashTMVersion.split(SQUASH_VERSION_REGEX);
                Object[] args =
                        new Object[] {
                            squashTMVersionSplit[0] + "." + squashTMVersionSplit[1],
                            SQUASH_ORCHESTRATOR_MINIMUM_VERSION,
                            SQUASH_ORCHESTRATOR_MINIMUM_ALLINONE_REQUIRED_VERSION
                        };
                throw new OutdatedSquashOrchestratorException(
                        messageSource.getMessage(
                                SQUASH_ORCHESTRATOR_OUTDATED_EXCEPTION_MESSAGE_KEY,
                                args,
                                LocaleContextHolder.getLocale()));
            }
        } catch (DateTimeParseException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private static void checkTokenValidity(String token, MessageSource messageSource) {
        if (Objects.nonNull(token) && !token.isEmpty()) {
            try {
                SignedJWT jwt = SignedJWT.parse(token);
                JWTClaimsSet claimSet = jwt.getJWTClaimsSet();
                if (claimSet.getExpirationTime() != null
                        && claimSet.getExpirationTime().before(new Date())) {
                    Object[] args = new Object[] {claimSet.getExpirationTime()};
                    throw new InvalidSquashOrchestratorConfigurationException(
                            messageSource.getMessage(
                                    "testautomation.exceptions.token.expired",
                                    args,
                                    "The token has expired.",
                                    LocaleContextHolder.getLocale()));
                }
            } catch (ParseException e) {
                throw new InvalidSquashOrchestratorConfigurationException(
                        messageSource.getMessage(
                                "testautomation.exceptions.token.invalid",
                                null,
                                "The token is invalid",
                                LocaleContextHolder.getLocale()));
            }
        }
    }

    private static void checkUrlValidity(
            String url,
            TestableService service,
            String method,
            String token,
            MessageSource messageSource) {

        String serviceName = service.name().toLowerCase();

        try {
            URL targetURL = new URL(url + service.endPoint);
            HttpURLConnection connection = (HttpURLConnection) targetURL.openConnection();
            connection.setRequestMethod(method);
            connection.setRequestProperty(AUTHORIZATION, BEARER + token);
            int responseCode = connection.getResponseCode();

            if (responseCode == 401 || responseCode == 403) {
                throw new InvalidSquashOrchestratorConfigurationException(
                        messageSource.getMessage(
                                "testautomation.exceptions.token.noaccess", null, LocaleContextHolder.getLocale()));
            } else if (responseCode != service.expectedStatusCode) {
                throw new InvalidSquashOrchestratorConfigurationException(
                        messageSource.getMessage(
                                "testautomation.exceptions.configuration.url.invalid",
                                new Object[] {serviceName, url, responseCode},
                                LocaleContextHolder.getLocale()));
            }
        } catch (IOException e) {
            LOGGER.error("This service is unreachable: {}", serviceName, e);
            throw new InvalidSquashOrchestratorConfigurationException(
                    messageSource.getMessage(
                            "testautomation.exceptions.configuration.url.timeout",
                            new Object[] {serviceName, url},
                            LocaleContextHolder.getLocale()));
        }
    }

    private static void checkReceptionistValidity(
            String receptionistUrl, String token, MessageSource messageSource) {
        checkUrlValidity(
                receptionistUrl, TestableService.RECEPTIONIST, POST_METHOD, token, messageSource);
    }

    private static void checkObserverUrlValidity(
            String observerUrl, String token, MessageSource messageSource) {
        checkUrlValidity(observerUrl, TestableService.OBSERVER, GET_METHOD, token, messageSource);
    }

    private static void checkEventBusUrlValidity(
            String eventBusUrl, String token, MessageSource messageSource) {
        checkUrlValidity(eventBusUrl, TestableService.EVENT_BUS, GET_METHOD, token, messageSource);
    }

    private static void checkKillswitchUrlValidity(
            String killswitchUrl, String token, MessageSource messageSource) {
        checkUrlValidity(
                killswitchUrl, TestableService.KILLSWITCH, DELETE_METHOD, token, messageSource);
    }

    private static String getUrl(String receptionistUrl, String url) {
        return Objects.nonNull(url) && !url.isEmpty() ? url : receptionistUrl;
    }

    public enum TestableService {
        RECEPTIONIST("/workflows", 422),
        OBSERVER("/channels", 200),
        EVENT_BUS("/subscriptions", 200),
        KILLSWITCH("/workflows/health-check", 422);

        public final String endPoint;
        public final int expectedStatusCode;

        TestableService(String endPoint, int expectedStatusCode) {
            this.endPoint = endPoint;
            this.expectedStatusCode = expectedStatusCode;
        }

        public String getEndPoint() {
            return endPoint;
        }

        public int getExpectedStatusCode() {
            return expectedStatusCode;
        }
    }
}
