/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model.messages;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.opentestfactory.dto.v1.JobDTO;
import org.opentestfactory.messages.AbstractStep;
import org.opentestfactory.messages.Category;
import org.opentestfactory.messages.OTFMessage;
import org.squashtest.squash.automation.tm.testplan.library.application.ParamActionStep;
import org.squashtest.squash.automation.tm.testplan.library.application.TestCodeCheckoutStep;
import org.squashtest.squash.automation.tm.testplan.library.application.TestExecutionProviderStep;
import org.squashtest.squash.automation.tm.testplan.library.model.Credentials;
import org.squashtest.squash.automation.tm.testplan.library.model.Project;
import org.squashtest.squash.automation.tm.testplan.library.model.TestExecution;
import org.squashtest.squash.automation.tm.testplan.library.model.TestPlan;
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext;

public class Workflow extends OTFMessage {
    public static final String NAME_METADATA_KEY = "name";

    private Map<String, JobDTO> jobs;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> variables = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> resources = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Object> hooks = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> defaults = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> strategy = new HashMap<>();

    public Workflow(
            String apiVersion,
            String name,
            TestPlan testPlan,
            TestPlanContext testPlanContext,
            String additionalConfiguration) {
        super(apiVersion);
        addMetadata("name", name);
        addMetadata("namespace", testPlan.getNamespace());
        addMetadata("managedTests", new HashMap<>());
        addOriginProjectsMetaData(testPlan);
        Map managedTests = (Map) this.getMetadata().get("managedTests");
        managedTests.put("testPlan", getTestPlanMetadata(testPlanContext));
        managedTests.put("testCases", new HashMap<>());
        Map<String, JobDTO> jobMap = new HashMap<>(testPlan.getProjects().size());
        for (Project project : testPlan.getProjects()) {
            JobDTO job = buildJob(project);
            jobMap.put("squashTMJob-" + jobMap.size(), job);
            Map<String, Map<String, Object>> testCases =
                    (Map<String, Map<String, Object>>) managedTests.get("testCases");
            project
                    .getTests()
                    .forEach(
                            test ->
                                    testCases.put(test.getMetadata("exec_step_id").toString(), test.getMetadata()));
        }
        this.jobs = jobMap;
        if (additionalConfiguration != null && !"".equals(additionalConfiguration)) {
            AdditionalConfiguration addConf = new AdditionalConfiguration(additionalConfiguration);
            mergeWithAdditionalConfiguration(addConf);
        }
    }

    public Workflow(String apiVersion) {
        super(apiVersion);
    }

    private void addOriginProjectsMetaData(TestPlan testPlan) {
        List<String> projectIds =
                testPlan.getConfigurations().stream()
                        .map(conf -> conf.getTmProjectId().toString())
                        .toList();

        addMetadata(
                "annotations", Collections.singletonMap("squashtest.org/originating-projects", projectIds));
    }

    private void mergeWithAdditionalConfiguration(AdditionalConfiguration additionalConfiguration) {
        this.setDefaults(additionalConfiguration.getDefaults());
        this.setHooks(additionalConfiguration.getHooks());
        this.setMetadata(
                Stream.concat(
                                this.getMetadata().entrySet().stream(),
                                additionalConfiguration.getMetadata().entrySet().stream())
                        .collect(
                                Collectors.toMap(
                                        Map.Entry::getKey,
                                        Map.Entry::getValue,
                                        (baseMetadata, additionalMetadata) -> baseMetadata)));
        this.setResources(additionalConfiguration.getResources());
        this.setVariables(additionalConfiguration.getVariables());
        this.setStrategy(additionalConfiguration.getStrategy());
    }

    private Map<String, Object> getTestPlanMetadata(TestPlanContext testPlanContext) {
        Map<String, Object> results = new HashMap<>();
        results.put("uuid", testPlanContext.getEntityUuid());
        results.put("type", testPlanContext.getEntityTypeAsString());
        results.put("path", testPlanContext.getEntityPathAsList());
        return results;
    }

    public String name() {
        return (String) getMetadata().get(NAME_METADATA_KEY);
    }

    private JobDTO buildJob(Project project) {
        final ArrayList<AbstractStep> steps = new ArrayList<>();
        final Credentials testRepositoryCredentials = project.testRepositoryCredentials;
        steps.add(
                new TestCodeCheckoutStep(project.testRepository, testRepositoryCredentials, "checkOut"));
        for (TestExecution exec : project.getTests()) {
            if (!exec.getParams().isEmpty()) {
                ParamActionStep paramActionStep =
                        new ParamActionStep((project.toolCategory), exec.getParams(), "paramStepName");
                steps.add(paramActionStep);
                exec.addMetadata("param_step_id", paramActionStep.getStepId());
            }
            steps.add(
                    new TestExecutionProviderStep(
                            exec.getDefinition(), project.toolCategory, exec.getStepId(), "execStepName"));
        }
        List<String> runsOn = getRunsOn(project);
        if (Objects.nonNull(project.getEnvironmentVariables())
                && !project.getEnvironmentVariables().isEmpty()) {
            steps.forEach(step -> step.setVariables(project.getEnvironmentVariables()));
            return new JobDTO(runsOn, steps, project.getEnvironmentVariables());
        }
        return new JobDTO(runsOn, steps);
    }

    public Map<String, JobDTO> getJobs() {
        return Collections.unmodifiableMap(jobs);
    }

    private List<String> getRunsOn(Project project) {
        List<String> environmentTags = new ArrayList<>(project.getEnvironmentTags());
        String technologyTag = new Category(project.toolCategory).prefix();
        if (!environmentTags.contains(technologyTag)) {
            environmentTags.add(technologyTag);
        }
        return environmentTags;
    }

    public Map<String, Object> getVariables() {
        return !variables.isEmpty() ? Collections.unmodifiableMap(variables) : null;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    public Map<String, Object> getResources() {
        return !resources.isEmpty() ? Collections.unmodifiableMap(resources) : null;
    }

    public void setResources(Map<String, Object> resources) {
        this.resources = resources;
    }

    public List<Object> getHooks() {
        return !hooks.isEmpty() ? hooks : null;
    }

    public void setHooks(List<Object> hooks) {
        this.hooks = hooks;
    }

    public Map<String, Object> getDefaults() {
        return !defaults.isEmpty() ? Collections.unmodifiableMap(defaults) : null;
    }

    public void setDefaults(Map<String, Object> defaults) {
        this.defaults = defaults;
    }

    public Map<String, Object> getStrategy() {
        return !strategy.isEmpty() ? Collections.unmodifiableMap(strategy) : null;
    }

    public void setStrategy(Map<String, Object> strategy) {
        this.strategy = strategy;
    }
}
