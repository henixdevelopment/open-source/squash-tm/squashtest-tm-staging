/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.feature;

import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.AUTOCONNECT_ON_CONNECTION_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.CASE_INSENSITIVE_ACTIONS_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.CASE_INSENSITIVE_LOGIN_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.MILESTONE_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.SEARCH_ACTIVATION;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.STACK_TRACE_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.UNSAFE_ATTACHMENT_PREVIEW_FEATURE_ENABLED;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import javax.inject.Inject;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.milestone.MilestoneManagerService;

/**
 * @author Gregory Fouquet
 */
@Service(value = "featureManager")
@Transactional
public class FeatureManagerImpl implements FeatureManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureManagerImpl.class);

    @Inject @Lazy private ConfigurationService configuration;

    @Inject @Lazy private MilestoneManagerService milestoneManager;

    /**
     * @see
     *     org.squashtest.tm.service.feature.FeatureManager#isEnabled(org.squashtest.tm.service.feature.FeatureManager.Feature)
     */
    @Override
    public boolean isEnabled(Feature feature) {
        LOGGER.trace("Polling feature {}", feature);

        return switch (feature) {
            case MILESTONE -> configuration.getBoolean(MILESTONE_FEATURE_ENABLED);
            case CASE_INSENSITIVE_LOGIN ->
                    configuration.getBoolean(CASE_INSENSITIVE_LOGIN_FEATURE_ENABLED);
            case CASE_INSENSITIVE_ACTIONS ->
                    configuration.getBoolean(CASE_INSENSITIVE_ACTIONS_FEATURE_ENABLED);
            case STACK_TRACE -> configuration.getBoolean(STACK_TRACE_FEATURE_ENABLED);
            case AUTOCONNECT_ON_CONNECTION -> configuration.getBoolean(AUTOCONNECT_ON_CONNECTION_ENABLED);
            case SEARCH_ACTIVATION -> configuration.getBoolean(SEARCH_ACTIVATION);
            case UNSAFE_ATTACHMENT_PREVIEW ->
                    configuration.getBoolean(UNSAFE_ATTACHMENT_PREVIEW_FEATURE_ENABLED);
            default ->
                    throw new IllegalArgumentException(
                            "I don't know feature '" + feature + "'. I am unable to tell if it's enabled or not");
        };
    }

    /**
     * @see
     *     org.squashtest.tm.service.feature.FeatureManager#setEnabled(org.squashtest.tm.service.feature.FeatureManager.Feature,
     *     boolean)
     */
    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void setEnabled(Feature feature, boolean enabled) {
        LOGGER.trace("Setting feature {} to {}", feature, enabled);

        switch (feature) {
            case MILESTONE -> setMilestoneFeatureEnabled(enabled);
            case CASE_INSENSITIVE_LOGIN -> setCaseInsensitiveLoginFeatureEnabled(enabled);
            case CASE_INSENSITIVE_ACTIONS -> setCaseInsensitiveActionsFeatureEnabled(enabled);
            case STACK_TRACE -> setStackTraceFeatureEnabled(enabled);
            case AUTOCONNECT_ON_CONNECTION -> setAutoconnectOnConnectionEnabled(enabled);
            case SEARCH_ACTIVATION -> setSearchActivationFeatureEnabled(enabled);
            case UNSAFE_ATTACHMENT_PREVIEW -> setUnsafeAttachmentPreviewEnabled(enabled);
            default -> throw new IllegalArgumentException("Unknown feature '%s'.".formatted(feature));
        }
    }

    private void setCaseInsensitiveLoginFeatureEnabled(boolean enabled) {
        // TODO check if possible
        configuration.set(CASE_INSENSITIVE_LOGIN_FEATURE_ENABLED, enabled);
    }

    private void setCaseInsensitiveActionsFeatureEnabled(boolean enabled) {
        configuration.set(CASE_INSENSITIVE_ACTIONS_FEATURE_ENABLED, enabled);
    }

    private void setStackTraceFeatureEnabled(boolean enabled) {
        // TODO check if possible
        configuration.set(STACK_TRACE_FEATURE_ENABLED, enabled);
    }

    private void setAutoconnectOnConnectionEnabled(boolean enabled) {
        configuration.set(AUTOCONNECT_ON_CONNECTION_ENABLED, enabled);
    }

    private void setMilestoneFeatureEnabled(boolean enabled) {
        configuration.set(MILESTONE_FEATURE_ENABLED, enabled);
        if (enabled) {
            milestoneManager.enableFeature();
        } else {
            milestoneManager.disableFeature();
        }
    }

    private void setSearchActivationFeatureEnabled(boolean enabled) {
        configuration.set(SEARCH_ACTIVATION, enabled);
    }

    private void setUnsafeAttachmentPreviewEnabled(boolean enabled) {
        configuration.set(UNSAFE_ATTACHMENT_PREVIEW_FEATURE_ENABLED, enabled);
    }
}
