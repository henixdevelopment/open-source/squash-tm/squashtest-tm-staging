/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.Comparator;

public final class DatasetModel {
    public static final Comparator<DatasetModel> COMPARATOR =
            new Comparator<DatasetModel>() {
                @Override
                public int compare(DatasetModel o1, DatasetModel o2) {
                    int comp1 = o1.getTcOwnerPath().compareTo(o2.getTcOwnerPath());
                    int comp2 = o1.getName().compareTo(o2.getName());
                    int comp3 = o1.getParamName().compareTo(o2.getParamName());
                    return comp1 != 0 ? comp1 : comp2 != 0 ? comp2 : comp3;
                }
            };

    private String tcOwnerPath;
    private long ownerId;
    private long id;
    private String name;
    private String paramOwnerPath;
    private long paramOwnerId;
    private String paramName;
    private String paramValue;

    public DatasetModel(
            long ownerId, long id, String name, long paramOwnerId, String paramName, String paramValue) {
        super();
        this.ownerId = ownerId;
        this.id = id;
        this.name = name;
        this.paramOwnerId = paramOwnerId;
        this.paramName = paramName;
        this.paramValue = paramValue;
    }

    public String getTcOwnerPath() {
        return tcOwnerPath;
    }

    public void setTcOwnerPath(String tcOwnerPath) {
        this.tcOwnerPath = tcOwnerPath;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParamOwnerPath() {
        return paramOwnerPath;
    }

    public void setParamOwnerPath(String paramOwnerPath) {
        this.paramOwnerPath = paramOwnerPath;
    }

    public long getParamOwnerId() {
        return paramOwnerId;
    }

    public void setParamOwnerId(long paramOwnerId) {
        this.paramOwnerId = paramOwnerId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
