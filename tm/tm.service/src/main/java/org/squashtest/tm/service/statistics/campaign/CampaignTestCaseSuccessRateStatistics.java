/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.campaign;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.squashtest.tm.domain.LevelComparator;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatus.Conclusiveness;
import org.squashtest.tm.domain.execution.ExecutionStatus.Termination;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.statistics.CountOnEnum;

public class CampaignTestCaseSuccessRateStatistics {

    private final LinkedHashMap<TestCaseImportance, CountOnEnum<ExecutionStatus>> statistics;
    private final LinkedHashMap<TestCaseImportance, CountOnEnum<ExecutionStatus>> conclusivenessStats;

    public CampaignTestCaseSuccessRateStatistics() {
        statistics =
                EnumSet.allOf(TestCaseImportance.class).stream()
                        .sorted(LevelComparator.getInstance())
                        .collect(
                                Collectors.toMap(
                                        Function.identity(),
                                        e -> new CountOnEnum<>(ExecutionStatus.class),
                                        (e1, e2) -> e1,
                                        LinkedHashMap::new));

        conclusivenessStats =
                EnumSet.allOf(TestCaseImportance.class).stream()
                        .sorted(LevelComparator.getInstance())
                        .collect(
                                Collectors.toMap(
                                        Function.identity(),
                                        e -> new CountOnEnum<>(ExecutionStatus.TERMINAL_STATUSES),
                                        (e1, e2) -> e1,
                                        LinkedHashMap::new));
    }

    public void add(TestCaseImportance testCaseImportance, ExecutionStatus status, Long number) {
        this.statistics.get(testCaseImportance).add(status, number);
        this.conclusivenessStats.get(testCaseImportance).add(status, number);
    }

    public LinkedHashMap<TestCaseImportance, LinkedHashMap<Conclusiveness, Integer>>
            getConclusiveness() {
        LinkedHashMap<TestCaseImportance, LinkedHashMap<Conclusiveness, Integer>> output =
                new LinkedHashMap<>();
        this.conclusivenessStats.forEach(
                (testCaseImportance, executionStatusCountOnEnum) ->
                        output.put(
                                testCaseImportance,
                                executionStatusCountOnEnum.getStatistics(
                                        Conclusiveness::fromExecutionStatus, Conclusiveness.class)));
        return output;
    }

    public LinkedHashMap<TestCaseImportance, LinkedHashMap<Termination, Integer>> getTermination() {
        LinkedHashMap<TestCaseImportance, LinkedHashMap<Termination, Integer>> output =
                new LinkedHashMap<>();
        this.statistics.forEach(
                (testCaseImportance, executionStatusCountOnEnum) ->
                        output.put(
                                testCaseImportance,
                                executionStatusCountOnEnum.getStatistics(
                                        Termination::fromExecutionStatus, Termination.class)));
        return output;
    }

    /*
     * Utility method to convert directly from tuples...
     * Take care of types inside your tuples.
     */
    public static CampaignTestCaseSuccessRateStatistics fromTuples(List<Object[]> tuples) {
        CampaignTestCaseSuccessRateStatistics result = new CampaignTestCaseSuccessRateStatistics();
        for (Object[] tuple : tuples) {
            result.add((TestCaseImportance) tuple[0], (ExecutionStatus) tuple[1], (Long) tuple[2]);
        }
        return result;
    }
}
