/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution;

import static org.squashtest.tm.service.security.Authorizations.EXECUTE_EXECSTEP_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.EXECUTE_EXECUTION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.Paging;
import org.squashtest.tm.core.foundation.collection.PagingBackedPagedCollectionHolder;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.execution.SessionNoteKind;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.exception.execution.ExecutionHasNoStepsException;
import org.squashtest.tm.exception.requirement.MilestoneForbidModificationException;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.execution.ExecutionModificationService;
import org.squashtest.tm.service.execution.ExploratoryExecutionService;
import org.squashtest.tm.service.internal.campaign.CampaignNodeDeletionHandler;
import org.squashtest.tm.service.internal.display.dto.execution.SessionNoteDto;
import org.squashtest.tm.service.internal.dto.ExecutionSummaryDto;
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.ExecutionStepDao;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.SessionNoteDao;
import org.squashtest.tm.service.internal.repository.display.SessionNoteDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

@Service("squashtest.tm.service.ExecutionModificationService")
@Transactional
public class ExecutionModificationServiceImpl implements ExecutionModificationService {

    @Inject private ExecutionDao executionDao;

    @Inject private ExecutionStepDao executionStepDao;

    @Inject private CampaignNodeDeletionHandler deletionHandler;

    @Inject private ExecutionStepModificationHelper executionStepModifHelper;

    @Inject private MilestoneDao milestoneDao;

    @Inject private SessionNoteDao sessionNoteDao;

    @Inject private SessionNoteDisplayDao sessionNoteDisplayDao;

    @Inject private ExploratoryExecutionService exploratoryExecutionService;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject private AttachmentManagerService attachmentManagerService;

    @Inject private SprintDisplayDao sprintDisplayDao;

    @Inject private AutomatedExecutionExtenderDao automatedExecutionExtenderDao;

    @PersistenceContext private EntityManager entityManager;

    @Override
    public Execution findAndInitExecution(Long executionId) {
        return executionDao.findAndInit(executionId);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public void setExecutionDescription(@Id Long executionId, String description) {
        checkIfParentSprintIsClosed(executionId);
        Execution execution = executionDao.getReferenceById(executionId);
        execution.setDescription(description);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExecutionStep> findExecutionSteps(long executionId) {
        return executionDao.findSteps(executionId);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECSTEP_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = ExecutionStep.class)
    public void setExecutionStepComment(@Id Long executionStepId, String comment) {
        ExecutionStep executionStep = executionStepDao.findById(executionStepId);
        checkIfParentSprintIsClosed(executionStep.getExecution().getId());
        executionStep.setComment(comment);
    }

    @Override
    public PagedCollectionHolder<List<ExecutionStep>> findExecutionSteps(
            long executionId, Paging filter) {
        List<ExecutionStep> list = executionDao.findStepsFiltered(executionId, filter);
        long count = executionDao.countSteps(executionId);
        return new PagingBackedPagedCollectionHolder<>(filter, count, list);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    public List<SuppressionPreviewReport> simulateExecutionDeletion(Long executionId) {
        return deletionHandler.simulateExecutionDeletion(executionId);
    }

    @Override
    @PreAuthorize("hasPermission(#execution, 'DELETE_EXECUTION') " + OR_HAS_ROLE_ADMIN)
    public void deleteExecution(Execution execution) {
        if (milestoneDao.isExecutionBoundToBlockingMilestone(execution.getId())) {
            throw new MilestoneForbidModificationException();
        }

        deletionHandler.deleteExecution(execution);
    }

    @Override
    @Transactional(readOnly = true)
    public Execution findById(long id) {
        return executionDao.getReferenceById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ExecutionStep findExecutionStepById(long id) {
        return executionStepDao.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Execution> findAllById(List<Long> executionIds) {
        return executionDao.findAllById(executionIds);
    }

    @Override
    public Map<Long, List<ExecutionSummaryDto>> findIterationTestPlanItemLastExecStatuses(
            Collection<Long> itemTestPlanIds) {
        return executionDao.findIterationTestPlanItemLastExecStatuses(itemTestPlanIds);
    }

    @Override
    public AutomatedExecutionExtender findExecutionExtenderByExtenderId(long extenderId) {
        return automatedExecutionExtenderDao.findById(extenderId);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public void setExecutionStatus(@Id Long executionId, ExecutionStatus status) {
        checkIfParentSprintIsClosed(executionId);
        Execution execution = executionDao.getReferenceById(executionId);
        execution.setExecutionStatus(status);
    }

    @Override
    @PreventConcurrent(entityType = Execution.class)
    public long updateSteps(@Id long executionId) {
        checkIfParentSprintIsClosed(executionId);
        Execution execution = executionDao.loadWithSteps(executionId);
        List<ExecutionStep> toBeUpdated = executionStepModifHelper.findStepsToUpdate(execution);

        long result = executionStepModifHelper.doUpdateStep(toBeUpdated, execution);

        if (execution.getSteps().isEmpty()) {
            throw new ExecutionHasNoStepsException();
        }
        return result;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Execution> findExecutionByTestCaseId(Long testCaseId, Pageable pageable) {
        return executionDao.findAllByReferencedTestCaseId(testCaseId, pageable);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public SessionNoteDto createSessionNote(
            @Id long executionId, SessionNoteKind noteKind, String noteContent, Integer noteOrder) {
        checkIfParentSprintIsClosed(executionId);
        ExploratoryExecutionRunningState currentState =
                exploratoryExecutionService.findExploratoryExecutionRunningState(executionId);
        exploratoryExecutionService.checkSessionIsNotPaused(currentState);
        exploratoryExecutionService.checkSessionIsNotStopped(currentState);

        final Long noteId =
                sessionNoteDao.createSessionNote(executionId, noteKind, noteContent, noteOrder);
        SessionNote sessionNote = sessionNoteDao.findById(noteId);
        createAttachments(sessionNote);

        exploratoryExecutionService.updateExecutionMetadata(executionId);

        return sessionNoteDisplayDao.findById(noteId);
    }

    private void createAttachments(SessionNote sessionNote) {
        if (sessionNote.getId() == null) {
            return;
        }

        String content = sessionNote.getContent();

        if (content == null || content.isEmpty()) {
            return;
        }

        String newContent =
                attachmentManagerService.handleRichTextAttachments(
                        content, sessionNote.getAttachmentList());

        if (!content.equals(newContent)) {
            sessionNote.setContent(newContent);
        }
    }

    @Override
    public SessionNoteDto updateSessionNoteKind(long noteId, SessionNoteKind kind) {
        checkPermissionsAndSessionState(noteId);
        sessionNoteDao.updateSessionNoteKind(noteId, kind);

        final Long executionId = sessionNoteDao.findExploratoryExecutionId(noteId);
        exploratoryExecutionService.updateExecutionMetadata(executionId);

        return sessionNoteDisplayDao.findById(noteId);
    }

    @Override
    public SessionNoteDto updateSessionNoteContent(long noteId, String content) {
        checkPermissionsAndSessionState(noteId);
        sessionNoteDao.updateSessionNoteContent(noteId, content);

        final Long executionId = sessionNoteDao.findExploratoryExecutionId(noteId);
        exploratoryExecutionService.updateExecutionMetadata(executionId);

        return sessionNoteDisplayDao.findById(noteId);
    }

    @Override
    public void deleteSessionNote(long noteId) {
        checkPermissionsAndSessionState(noteId);
        sessionNoteDao.deleteSessionNote(noteId);
    }

    @Override
    @PreAuthorize(EXECUTE_EXECUTION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Execution.class)
    public void changeNotesIndex(@Id long executionId, Integer newIndex, List<Long> movedNoteIds) {
        ExploratoryExecution execution = entityManager.find(ExploratoryExecution.class, executionId);
        List<SessionNote> allSessionNotes = execution.getSessionNotes();
        List<SessionNote> movedSessionNotes =
                movedNoteIds.stream()
                        .map(
                                noteId ->
                                        allSessionNotes.stream()
                                                .filter(note -> note.getId().equals(noteId))
                                                .findFirst()
                                                .orElseThrow())
                        .toList();

        execution.moveSessionNotes(newIndex, movedSessionNotes);
    }

    private void checkPermissionsAndSessionState(long noteId) {
        final Long executionId = sessionNoteDao.findExploratoryExecutionId(noteId);
        checkExecutePermissionAndBlockingMilestone(executionId);
        checkIfParentSprintIsClosed(executionId);

        ExploratoryExecutionRunningState currentState =
                exploratoryExecutionService.findExploratoryExecutionRunningState(executionId);
        exploratoryExecutionService.checkSessionIsNotStopped(currentState);
    }

    @CheckBlockingMilestone(entityType = Execution.class)
    private void checkExecutePermissionAndBlockingMilestone(@Id Long executionId) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                Collections.singletonList(executionId),
                Permissions.EXECUTE.name(),
                Execution.class.getName());
    }

    private void checkIfParentSprintIsClosed(Long executionId) {
        if (SprintStatus.CLOSED.equals(sprintDisplayDao.getSprintStatusByExecutionId(executionId))) {
            throw new SprintClosedException();
        }
    }
}
