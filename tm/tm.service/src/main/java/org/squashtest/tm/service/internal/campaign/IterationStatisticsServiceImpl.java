/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.campaign.IterationStatisticsService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao;
import org.squashtest.tm.service.statistics.CountOnEnum;
import org.squashtest.tm.service.statistics.campaign.CampaignTestCaseSuccessRateStatistics;
import org.squashtest.tm.service.statistics.campaign.ProgressionStatistics;
import org.squashtest.tm.service.statistics.campaign.ScheduledIteration;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;
import org.squashtest.tm.service.statistics.iteration.TestSuiteTestInventoryStatistics;

@Transactional(readOnly = true)
@Service("IterationStatisticsService")
public class IterationStatisticsServiceImpl implements IterationStatisticsService {

    private static final String ID = "id";
    private static final String ITERATION_IDS = "iterationIds";
    private static final String ITPI_IDS = "itpiIds";
    private static final Logger LOGGER =
            LoggerFactory.getLogger(IterationStatisticsServiceImpl.class);

    @Inject private IterationTestPlanManagerService itpManagerService;
    @Inject private CustomItpiLastExecutionFilterDao itpiLastExecutionFilterDao;

    @PersistenceContext private EntityManager em;

    /**
     * Given a list of iteration ids, gathers and returns the number of non-executed test cases
     * grouped by weight.
     *
     * @param iterationIds
     * @return
     */
    private Map<ExecutionStatus, Integer> gatherIterationTestCaseStatusStatistics(
            List<Long> iterationIds) {
        // get the data
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery("IterationStatistics.multipleglobaltestinventory", Object[].class);
        query.setParameter(ITERATION_IDS, iterationIds);
        List<Object[]> res = query.getResultList();
        return CountOnEnum.fromTuples(res, ExecutionStatus.class)
                .getStatistics(
                        ExecutionStatus::getCanonicalStatus, ExecutionStatus.getCanonicalStatusSet());
    }

    private Map<ExecutionStatus, Integer> gatherIterationTestCaseStatusStatisticsForTCLastExecScope(
            List<Long> itpiIds) {
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery(
                                "IterationStatistics.multipleglobaltestinventorybyitpi", Object[].class);
        query.setParameter(ITPI_IDS, itpiIds);
        List<Object[]> res = query.getResultList();
        return CountOnEnum.fromTuples(res, ExecutionStatus.class)
                .getStatistics(
                        ExecutionStatus::getCanonicalStatus, ExecutionStatus.getCanonicalStatusSet());
    }

    /**
     * Given a list of iteration ids, gathers and returns the number of test cases grouped by
     * execution status.
     *
     * @param iterationIds
     * @return
     */
    private Map<TestCaseImportance, Integer> gatherIterationNonExecutedTestCaseImportanceStatistics(
            List<Long> iterationIds) {
        // get the data
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery(
                                "IterationStatistics.multiplenonexecutedTestcaseImportance", Object[].class);
        query.setParameter(ITERATION_IDS, iterationIds);
        List<Object[]> res = query.getResultList();
        return CountOnEnum.fromTuples(res, TestCaseImportance.class).getStatistics();
    }

    private Map<TestCaseImportance, Integer>
            gatherIterationNonExecutedTestCaseImportanceStatisticsForTCLastExecScope(List<Long> itpiIds) {
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery(
                                "IterationStatistics.multiplenonexecutedTestcaseImportancebyitpi", Object[].class);
        query.setParameter(ITPI_IDS, itpiIds);
        List<Object[]> res = query.getResultList();
        return CountOnEnum.fromTuples(res, TestCaseImportance.class).getStatistics();
    }

    /**
     * Given one or more iteration ids, gathers and returns the number of passed and failed test cases
     * grouped by weight.
     *
     * @param iterationIds
     * @return
     */
    private CampaignTestCaseSuccessRateStatistics gatherIterationTestCaseSuccessRateStatistics(
            List<Long> iterationIds) {
        // get the data
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery("IterationStatistics.multipleSuccessRate", Object[].class);
        query.setParameter(ITERATION_IDS, iterationIds);
        List<Object[]> res = query.getResultList();
        return CampaignTestCaseSuccessRateStatistics.fromTuples(res);
    }

    private CampaignTestCaseSuccessRateStatistics
            gatherIterationTestCaseSuccessRateStatisticsForTCLastExecScope(List<Long> itpiIds) {
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery("IterationStatistics.multipleSuccessRateByItpi", Object[].class);
        query.setParameter(ITPI_IDS, itpiIds);
        List<Object[]> res = query.getResultList();
        return CampaignTestCaseSuccessRateStatistics.fromTuples(res);
    }

    /**
     * Given an iteration id, returns the inventory statistics for each suite The suites are displayed
     * by reference and name : {test suite ref} - {test suite name}
     */
    /* A few comments about test suites having an empty test plan or referencing test case(s) that were deleted.
     *
     *  We want to report the test suites with the following rules :
     *  a) test suites with an empty test plan must be reported anyway,
     *  b) deleted test cases must be excluded of the statistics.
     *
     *  This implies that we must detect those exceptions. We can achieve that thanks to the following behavior :
     *
     * 1/ By the virtue of "left outer join" there always will be at least one row in the resultset for each test suite
     * even when its test plan is empty. In that later case the ExecutionStatus will be null.
     *
     * 2/ When the test plan is not empty but contains one or several deleted test cases there will be a row for them anyway.
     * In that later case the TestCaseImportance will be null.
     *
     *
     * (non-Javadoc)
     * @see org.squashtest.tm.service.campaign.IterationStatisticsService#gatherTestSuiteTestInventoryStatistics(long)
     */
    private List<TestSuiteTestInventoryStatistics> gatherSingleTestSuiteTestInventoryStatistics(
            long iterationId) {

        List<TestSuiteTestInventoryStatistics> result = new LinkedList<>();

        // ****************** gather the model *******************

        // get the test suites and their tests
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery("IterationStatistics.testSuiteStatistics", Object[].class);
        query.setParameter(ID, iterationId);

        // get tests that belongs to no test suite
        Query<Object[]> requery =
                getCurrentSession()
                        .createNamedQuery(
                                "IterationStatistics.testSuiteStatistics-testsLeftover", Object[].class);
        requery.setParameter(ID, iterationId);

        List<Object[]> res = mergeQueryResultLists(query.list(), requery.list());

        processTestSuiteTestInventoryStatistics(res, result);

        return result;
    }

    /**
     * Given a list of iteration ids, this method returns the inventory statistics for each suite. The
     * suites are referred to by {iteration ref} - {iteration name} / {test suite ref} - {test suite
     * name}
     *
     * @param iterationIds
     * @return
     */
    private List<TestSuiteTestInventoryStatistics> gatherManyTestSuiteTestInventoryStatistics(
            List<Long> iterationIds) {

        List<TestSuiteTestInventoryStatistics> result = new LinkedList<>();

        // ****************** gather the model *******************

        // get the test suites and their tests
        Query<Object[]> query =
                getCurrentSession()
                        .createNamedQuery("IterationStatistics.multipletestSuiteStatistics", Object[].class);
        query.setParameter(ITERATION_IDS, iterationIds);

        // get tests that belongs to no test suite
        Query<Object[]> requery =
                getCurrentSession()
                        .createNamedQuery(
                                "IterationStatistics.multipletestSuiteStatistics-testsLeftover", Object[].class);
        requery.setParameter(ITERATION_IDS, iterationIds);

        List<Object[]> res = mergeQueryResultLists(query.list(), requery.list());

        processTestSuiteTestInventoryStatistics(res, result);

        return result;
    }

    private List<Object[]> mergeQueryResultLists(
            List<Object[]> queryResult, List<Object[]> leftoverQueryResult) {
        // merge the second list into the first. The first element of the tuple must be set to '--'
        // - see the reason in IterationStatistics.testSuiteStatistics-testsLeftover
        for (Object[] retuple : leftoverQueryResult) {
            retuple[0] = null;
            queryResult.add(retuple);
        }

        return queryResult;
    }

    private void processTestSuiteTestInventoryStatistics(
            List<Object[]> res, List<TestSuiteTestInventoryStatistics> result) {
        TestSuiteTestInventoryStatistics newStatistics = new TestSuiteTestInventoryStatistics();
        String previousSuiteName = "";

        for (Object[] tuple : res) {

            // basic information on the test suite : we always want them.
            String suiteName = (String) tuple[0];
            Date scheduledStart = (Date) tuple[4];
            Date scheduledEnd = (Date) tuple[5];

            if (!sameSuite(previousSuiteName, suiteName)) {
                newStatistics = new TestSuiteTestInventoryStatistics();
                newStatistics.setTestsuiteName(suiteName);
                newStatistics.setScheduledStart(scheduledStart);
                newStatistics.setScheduledEnd(scheduledEnd);
                result.add(newStatistics);
            }

            previousSuiteName = suiteName;

            /*
             * corner cases as discussed in the comments above. We skip the rest of that test suite if :
             *
             * 1/ (status == null) because it means that the test plan is empty,
             * 2/ (importance == null) because means that those test cases were deleted
             *
             */
            ExecutionStatus status = (ExecutionStatus) tuple[1];
            TestCaseImportance importance = (TestCaseImportance) tuple[2];
            Long howmany = (Long) tuple[3];

            if (status == null || importance == null) {
                continue;
            }

            /*
             * In any other cases we can process the row
             */
            newStatistics.addExecutionCount(howmany.intValue(), status.getCanonicalStatus(), importance);
        }

        sortTestSuiteTestInventoryStatisticsByTestSuiteName(result);
    }

    private boolean sameSuite(String name1, String name2) {
        return name1 == null && name2 == null || name1 != null && name1.equals(name2);
    }

    private void sortTestSuiteTestInventoryStatisticsByTestSuiteName(
            List<TestSuiteTestInventoryStatistics> result) {
        result.sort(
                (t1, t2) -> {
                    String t1SuiteName = t1.getTestsuiteName();
                    String t2SuiteName = t2.getTestsuiteName();

                    if (t1SuiteName != null && t2SuiteName != null) {
                        return t1SuiteName.compareToIgnoreCase(t2SuiteName);
                    } else if (t1SuiteName == null) {
                        return 1;
                    }
                    return -1;
                });
    }

    @Override
    public StatisticsBundle gatherIterationStatisticsBundle(
            List<Long> iterationIds, boolean lastExecScope) {

        StatisticsBundle bundle = initializeStatisticsBundle(iterationIds, lastExecScope);

        boolean isSolitaryIteration = iterationIds.size() == 1;

        List<TestSuiteTestInventoryStatistics> testSuiteTestInventoryStatistics =
                isSolitaryIteration
                        ? gatherSingleTestSuiteTestInventoryStatistics(iterationIds.get(0))
                        : gatherManyTestSuiteTestInventoryStatistics(iterationIds);
        ProgressionStatistics iterationProgressionStatistics =
                isSolitaryIteration
                        ? gatherIterationProgressionStatistics(iterationIds.get(0))
                        : new ProgressionStatistics();

        bundle.setTestsuiteTestInventoryStatisticsList(testSuiteTestInventoryStatistics);
        bundle.setProgressionStatistics(iterationProgressionStatistics);
        bundle.setSelectedIds(iterationIds);

        return bundle;
    }

    private StatisticsBundle initializeStatisticsBundle(
            List<Long> iterationIds, boolean lastExecScope) {
        StatisticsBundle bundle = new StatisticsBundle();
        Map<ExecutionStatus, Integer> testcaseStatuses;
        Map<TestCaseImportance, Integer> testcaseImportance;
        CampaignTestCaseSuccessRateStatistics testCaseSuccessRate;

        if (lastExecScope) {
            // get last executed or created itpi by tc and dataset
            List<Long> itpiIdsInScope =
                    itpiLastExecutionFilterDao.gatherLatestItpiIdsForTCInScopeForIteration(iterationIds);
            // calculate stats
            testcaseStatuses = gatherIterationTestCaseStatusStatisticsForTCLastExecScope(itpiIdsInScope);
            testcaseImportance =
                    gatherIterationNonExecutedTestCaseImportanceStatisticsForTCLastExecScope(itpiIdsInScope);
            testCaseSuccessRate =
                    gatherIterationTestCaseSuccessRateStatisticsForTCLastExecScope(itpiIdsInScope);
        } else {
            testcaseStatuses = gatherIterationTestCaseStatusStatistics(iterationIds);
            testcaseImportance = gatherIterationNonExecutedTestCaseImportanceStatistics(iterationIds);
            testCaseSuccessRate = gatherIterationTestCaseSuccessRateStatistics(iterationIds);
        }

        bundle.setTestCaseStatusStatistics(testcaseStatuses);
        bundle.setNonExecutedTestCaseImportanceStatistics(testcaseImportance);
        bundle.setTestCaseSuccessRateStatistics(testCaseSuccessRate);

        return bundle;
    }

    private ProgressionStatistics gatherIterationProgressionStatistics(long iterationId) {
        ProgressionStatistics progression = new ProgressionStatistics();
        Session session = getCurrentSession();
        Query<ScheduledIteration> query =
                session.createNamedQuery(
                        "IterationStatistics.findScheduledIterations", ScheduledIteration.class);
        query.setParameter("id", iterationId, LongType.INSTANCE);
        ScheduledIteration scheduledIteration = query.uniqueResult();

        Query<Date> requery =
                session.createNamedQuery("IterationStatistics.findExecutionsHistory", Date.class);
        requery.setParameter("id", iterationId, LongType.INSTANCE);
        requery.setParameterList("nonterminalStatuses", ExecutionStatus.getNonTerminatedStatusSet());
        List<Date> executionHistory = requery.getResultList();

        try {
            progression.setScheduledIterations(Collections.singletonList(scheduledIteration));
            ScheduledIteration.checkIterationDatesAreSet(scheduledIteration);

            progression.computeSchedule();

            // actual executions
            progression.computeCumulativeTestPerDate(executionHistory);
        } catch (IllegalArgumentException ex) {
            LOGGER.info(
                    "CampaignStatistics : could not generate iteration progression statistics for iteration"
                            + "{} : some dates are wrong",
                    iterationId);
            progression.addi18nErrorMessage(ex.getMessage());
        }

        return progression;
    }

    private Session getCurrentSession() {
        return em.unwrap(Session.class);
    }
}
