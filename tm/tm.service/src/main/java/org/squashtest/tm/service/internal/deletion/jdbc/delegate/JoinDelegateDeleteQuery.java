/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc.delegate;

import static org.squashtest.tm.jooq.domain.tables.WorkDeleteEntities.WORK_DELETE_ENTITIES;

import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.TableField;

/**
 * Delete query for database using delete A from A inner join ... Mainly MariaDB, for which this
 * form is 2 order of magnitude faster than in sub-request clause.
 *
 * <p>Note that jooq is affected by a bug resolved in 3.15+ which require java 11... So I have to
 * patch the request manually
 */
public class JoinDelegateDeleteQuery extends AbstractDelegateDeleteQuery
        implements DelegateDeleteQuery {

    public JoinDelegateDeleteQuery(
            TableField<?, Long> originalField, DSLContext dslContext, String operationId) {
        super(originalField, dslContext, operationId);
    }

    @Override
    public void delete(TableField<?, Long> linkedColumn) {
        Table<?> table = linkedColumn.getTable();

        dslContext
                .deleteFrom(table)
                .using(table, WORK_DELETE_ENTITIES)
                .where(
                        WORK_DELETE_ENTITIES
                                .ENTITY_ID
                                .eq(linkedColumn)
                                .and(
                                        WORK_DELETE_ENTITIES.ENTITY_TYPE.eq(
                                                extractFieldTableNameAsParam(originalField)))
                                .and(WORK_DELETE_ENTITIES.OPERATION_ID.eq(operationId)))
                .execute();
    }
}
