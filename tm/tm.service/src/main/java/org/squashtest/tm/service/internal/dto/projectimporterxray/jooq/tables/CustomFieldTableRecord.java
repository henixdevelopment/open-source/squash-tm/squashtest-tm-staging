/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;

/** This class is generated by jOOQ and modified. */
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class CustomFieldTableRecord extends UpdatableRecordImpl<CustomFieldTableRecord> {

    private static final long serialVersionUID = 1L;

    public void setId(Long value) {
        set(0, value);
    }

    public Long getId() {
        return (Long) get(0);
    }

    public void setItemId(Long value) {
        set(2, value);
    }

    public Long getItemId() {
        return (Long) get(2);
    }

    public void setKey(String value) {
        set(3, value);
    }

    public String getKey() {
        return (String) get(3);
    }

    public void setName(String value) {
        set(4, value);
    }

    public String getName() {
        return (String) get(4);
    }

    public void setValue(String value) {
        set(5, value);
    }

    public String getValue() {
        return (String) get(5);
    }

    public void setStepIndex(Integer value) {
        set(6, value);
    }

    public Integer getStepIndex() {
        return (Integer) get(6);
    }

    public void setStepAction(String value) {
        set(7, value);
    }

    public String getStepAction() {
        return (String) get(7);
    }

    public void setStepData(String value) {
        set(8, value);
    }

    public String getStepData() {
        return (String) get(8);
    }

    public void setStepExpectedResult(String value) {
        set(9, value);
    }

    public String getStepExpectedResult() {
        return (String) get(9);
    }

    public void setStepCalledTestKey(String value) {
        set(10, value);
    }

    public String getStepCalledTestKey() {
        return (String) get(10);
    }

    public void setStepCalledTestParameters(String value) {
        set(11, value);
    }

    public String getStepCalledTestParameters() {
        return (String) get(11);
    }

    public void setDatasetRow(Integer value) {
        set(12, value);
    }

    public Integer getDatasetRow() {
        return (Integer) get(12);
    }

    public void setDatasetName(String value) {
        set(13, value);
    }

    public String getDatasetName() {
        return (String) get(13);
    }

    public void setDatasetValue(String value) {
        set(14, value);
    }

    public String getDatasetValue() {
        return (String) get(14);
    }

    public void setPivotId(String value) {
        set(15, value);
    }

    public String getPivotId() {
        return (String) get(15);
    }

    public void setIssueLinkName(String value) {
        set(16, value);
    }

    public String getIssueLinkName() {
        return (String) get(16);
    }

    public void setIssueLinkKey(String value) {
        set(17, value);
    }

    public String getIssueLinkKey() {
        return (String) get(17);
    }

    public void setIssueLinkRelationship(String value) {
        set(18, value);
    }

    public String getIssueLinkRelationship() {
        return (String) get(18);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /** Create a detached CustomFieldXrayRecord */
    public CustomFieldTableRecord(CustomFieldTable customFieldTable) {
        super(customFieldTable);
    }

    /** Create a detached, initialised CustomFieldXrayRecord */
    public CustomFieldTableRecord(
            CustomFieldTable customFieldTable,
            Long id,
            Long itemId,
            String key,
            String name,
            String value,
            Integer stepIndex,
            String stepAction,
            String stepData,
            String stepExpectedResult,
            String stepCalledTestKey,
            String stepCalledTestParameters,
            Integer datasetRow,
            String datasetName,
            String datasetValue,
            String pivotId,
            String issueLinkName,
            String issueLinkKey,
            String issueLinkRelationship) {
        super(customFieldTable);

        setId(id);
        setItemId(itemId);
        setKey(key);
        setName(name);
        setValue(value);
        setStepIndex(stepIndex);
        setStepAction(stepAction);
        setStepData(stepData);
        setStepExpectedResult(stepExpectedResult);
        setStepCalledTestKey(stepCalledTestKey);
        setStepCalledTestParameters(stepCalledTestParameters);
        setDatasetRow(datasetRow);
        setDatasetName(datasetName);
        setDatasetValue(datasetValue);
        setPivotId(pivotId);
        setIssueLinkName(issueLinkName);
        setIssueLinkKey(issueLinkKey);
        setIssueLinkRelationship(issueLinkRelationship);
    }
}
