/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace;

import java.util.Objects;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignStatus;

public class CampaignToImport extends AbstractCampaignWorkspaceEntityWithTimePeriodInfo {

    private EntityType parentType;
    CampaignStatus status;

    public EntityType getParentType() {
        return parentType;
    }

    public void setParentType(EntityType parentType) {
        this.parentType = parentType;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public void setStatus(CampaignStatus status) {
        this.status = status;
    }

    public Campaign toCampaign() {
        Campaign campaign = new Campaign();
        campaign.setName(name);
        campaign.setDescription(description);

        if (Objects.nonNull(reference)) {
            campaign.setReference(reference);
        }

        campaign.setScheduledEndDate(scheduledTimePeriod.getScheduledEndDate());
        campaign.setScheduledStartDate(scheduledTimePeriod.getScheduledStartDate());

        if (!actualTimePeriod.isActualStartAuto()) {
            campaign.setActualStartDate(actualTimePeriod.getActualStartDate());
        }

        if (!actualTimePeriod.isActualEndAuto()) {
            campaign.setActualEndDate(actualTimePeriod.getActualEndDate());
        }

        campaign.setActualStartAuto(actualTimePeriod.isActualStartAuto());
        campaign.setActualEndAuto(actualTimePeriod.isActualEndAuto());

        if (Objects.nonNull(status)) {
            campaign.setStatus(status);
        }

        return campaign;
    }
}
