/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import org.hibernate.Session;
import org.hibernate.annotations.QueryHints;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.internal.copier.ChildEntityDto;
import org.squashtest.tm.service.internal.copier.ChildEntityDtoResult;
import org.squashtest.tm.service.internal.copier.ChildEntityDtoResultTransformer;
import org.squashtest.tm.service.internal.repository.EntityDao;

public class HibernateEntityDao<ENTITY_TYPE> extends HibernateDao<ENTITY_TYPE>
        implements EntityDao<ENTITY_TYPE> {

    @Override
    public /*final*/ ENTITY_TYPE findById(long id) {
        return getEntity(id);
    }

    /**
     * @return a list of all entities found in the database with no restriction
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<ENTITY_TYPE> findAll() {
        return entityManager.createQuery("from " + entityType.getName()).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ENTITY_TYPE> findAllByIds(Collection<Long> ids) {
        if (Objects.isNull(ids)) {
            throw new IllegalArgumentException("The list can't be null.");
        }

        if (ids.isEmpty()) {
            return Collections.emptyList();
        } else {
            return entityManager
                    .createQuery(
                            "from %s where %s in :ids".formatted(entityType.getName(), getIdPropertyName()))
                    .setParameter(IDS, ids)
                    .getResultList();
        }
    }

    public String getIdPropertyName() {
        return "id";
    }

    @SuppressWarnings("unchecked")
    protected List<Long> findDescendantIds(List<Long> params, String sql) {
        if (!params.isEmpty()) {
            Session session = currentSession();

            List<BigInteger> list;
            List<Long> result = new ArrayList<>();
            result.addAll(params); // the inputs are also part of the output.
            List<Long> local = params;

            do {
                NativeQuery sqlQuery = session.createNativeQuery(sql);
                sqlQuery.setParameterList("list", local, new LongType());
                list = sqlQuery.list();
                if (!list.isEmpty()) {
                    local.clear();
                    addingDescendantIds(local, result, list);
                }
            } while (!list.isEmpty());
            if (result.isEmpty()) {
                return null;
            }
            return result;

        } else {
            return Collections.emptyList();
        }
    }

    private void addingDescendantIds(List<Long> local, List<Long> result, List<BigInteger> list) {
        for (BigInteger bint : list) {
            local.add(bint.longValue());
            result.add(bint.longValue());
        }
    }

    @SuppressWarnings("unchecked")
    public ChildEntityDtoResult getChildEntityDtoForPaste(
            String query,
            Collection<Long> ids,
            int maxResult,
            int offset,
            ClipboardPayload clipboardPayload,
            BiConsumer<EntityType, List<Long>> additionalContent) {
        ChildEntityDtoResultTransformer childEntityDtoResultTransformer =
                new ChildEntityDtoResultTransformer(clipboardPayload);
        List<ChildEntityDto> childEntityDtos =
                entityManager
                        .createQuery(query)
                        .setParameter(IDS, ids)
                        .setMaxResults(maxResult)
                        .setFirstResult(offset)
                        .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                        .unwrap(org.hibernate.query.Query.class)
                        .setResultTransformer(childEntityDtoResultTransformer)
                        .getResultList();
        childEntityDtos.stream()
                .flatMap(c -> c.getChildren().stream())
                .collect(
                        Collectors.groupingBy(
                                treeNode -> EntityType.fromSimpleName(treeNode.getClass().getSimpleName()),
                                Collectors.mapping(Identified::getId, Collectors.toList())))
                .forEach(additionalContent);
        return new ChildEntityDtoResult(
                childEntityDtos, maxResult == childEntityDtoResultTransformer.getEntitiesCount());
    }
}
