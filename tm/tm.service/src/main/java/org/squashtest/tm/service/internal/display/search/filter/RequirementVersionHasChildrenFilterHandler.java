/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import static org.squashtest.tm.domain.requirement.QRequirementVersion.requirementVersion;
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasChildrenFilterHandler.HasChildrenFilterValue.NO_CHILDREN;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.requirement.QRequirement;
import org.squashtest.tm.domain.requirement.QRequirementVersion;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

@Component
public class RequirementVersionHasChildrenFilterHandler implements FilterHandler {

    private final Set<String> handledPrototypes = Sets.newHashSet("REQUIREMENT_VERSION_HAS_CHILDREN");

    @Override
    public boolean canHandleFilter(GridFilterValue filter) {
        return this.handledPrototypes.contains(filter.getColumnPrototype());
    }

    @Override
    public void handleFilter(
            ExtendedHibernateQuery<?> query, GridFilterValue filter, GridRequest gridRequest) {
        HasChildrenFilterValue hasChildrenFilterValue = extractFilterValue(filter);

        QRequirement parentRequirement = new QRequirement("parentRequirement");
        QRequirementVersion parentVersion = new QRequirementVersion("parentVersion");

        // This is the column on which we join with the outer query. We already know that the engine
        // will
        // select the default alias for requirementVersion.
        QRequirementVersion outerVersion = requirementVersion;

        HibernateQuery<Integer> subquery =
                new ExtendedHibernateQuery<>()
                        .select(Expressions.ONE)
                        .from(parentRequirement)
                        .join(parentRequirement.versions, parentVersion)
                        .where(parentVersion.id.eq(outerVersion.id))
                        .groupBy(parentRequirement.id);

        // now check if we need to verify that at least one relation exist, or at most zero :
        if (NO_CHILDREN.equals(hasChildrenFilterValue)) {
            subquery.having(parentRequirement.children.size().eq(0));
        } else {
            subquery.having(parentRequirement.children.size().gt(0));
        }

        // append to the superquery
        query.where(subquery.exists());
    }

    private HasChildrenFilterValue extractFilterValue(GridFilterValue filter) {
        List<String> values = filter.getValues();
        if (values.size() != 1) {
            throw new IllegalArgumentException(
                    "Invalid value for filter REQUIREMENT_VERSION_HAS_CHILDREN");
        }

        return HasChildrenFilterValue.valueOf(values.get(0));
    }

    public enum HasChildrenFilterValue {
        HAS_CHILDREN,
        NO_CHILDREN;
    }
}
