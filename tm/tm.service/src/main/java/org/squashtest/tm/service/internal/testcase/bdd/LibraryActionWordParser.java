/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.exception.actionword.InvalidActionWordParameterNameException;

/**
 * @author qtran - created on 27/07/2020
 */
public class LibraryActionWordParser extends AbstractActionWordParser {

    /** Used in action word library plugin */
    public ActionWord createActionWordInLibrary(String input) {
        List<ActionWordTokenizer.AWToken> tokens = checkInputAndParseTokens(input, false);

        for (ActionWordTokenizer.AWToken token : tokens) {
            switch (token.getType()) {
                case PLAIN_TEXT:
                    addTextFragment(token);
                    break;
                case QUOTED_VALUE:
                    addNamedParameter(token);
                    break;
                case NUMBER:
                    addNumberFragment(token);
                    break;
                default:
                    throw new IllegalStateException(
                            String.format(
                                    "Found unexpected AWToken of type '%s' with text '%s'.",
                                    token.getType(), token.getLexeme()));
            }
        }

        return new ActionWord(fragments);
    }

    private void addNamedParameter(ActionWordTokenizer.AWToken token) {
        final String parameterName = canonicalizeKeywordParameterName(token.getLexeme());
        addParameter(parameterName, "", "");
    }

    private String canonicalizeKeywordParameterName(String candidateName) {
        // Remove opening and closing tags
        if (candidateName.startsWith("\"") && candidateName.endsWith("\"")) {
            // Less than 3 chars is illegal as delimiters are included
            if (candidateName.length() < 3) {
                throw new InvalidActionWordParameterNameException(
                        "Action word parameter name cannot be blank.");
            }

            candidateName = candidateName.substring(1, candidateName.length() - 1);
        }

        if (StringUtils.isBlank(candidateName)) {
            throw new InvalidActionWordParameterNameException(
                    "Action word parameter name cannot be blank.");
        }

        // Replace successive whitespaces with 1 underscore
        candidateName = candidateName.trim().replaceAll("(\\s)+", "_");

        if (!Pattern.compile(Parameter.NAME_REGEXP).matcher(candidateName).matches()) {
            throw new InvalidActionWordParameterNameException(
                    "Action word parameter name must contain only alphanumeric, dash or underscore characters.");
        }

        // Replace successive whitespaces with 1 underscore
        return candidateName;
    }
}
