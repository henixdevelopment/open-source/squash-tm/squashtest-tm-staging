/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.instruction.container;

import java.util.List;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.batchimport.Facility;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;

public class CoverageInstructionContainer extends InstructionContainer<CoverageInstruction> {

    public CoverageInstructionContainer(List<CoverageInstruction> instructions) {
        super(instructions);
    }

    @Override
    protected void executeCreate(Facility facility, Project project) {
        facility.createCoverages(getCreateInstructions(), project);
    }

    @Override
    protected void executeUpdate(Facility facility, Project project) {
        facility.createCoverages(getUpdateInstructions(), project);
    }
}
