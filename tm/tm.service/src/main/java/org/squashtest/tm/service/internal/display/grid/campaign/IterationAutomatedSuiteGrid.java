/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;

import org.jooq.DSLContext;
import org.jooq.Table;
import org.springframework.context.MessageSource;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;

public class IterationAutomatedSuiteGrid extends AbstractAutomatedSuiteGrid {

    private final Long iterationId;
    private final AutomatedSuiteDisplayDao automatedSuiteDisplayDao;
    private final PermissionEvaluationService permissionEvaluationService;

    public IterationAutomatedSuiteGrid(
            Long iterationId,
            MessageSource messageSource,
            AutomatedSuiteDisplayDao automatedSuiteDisplayDao,
            PermissionEvaluationService permissionEvaluationService) {
        super(messageSource);
        this.iterationId = iterationId;
        this.automatedSuiteDisplayDao = automatedSuiteDisplayDao;
        this.permissionEvaluationService = permissionEvaluationService;
    }

/*
     If no filters are applied and the user can read unassigned items, we directly retrieve the count
     using a simpler, faster method. When filters are present, we must use getRows() to apply them,
     but this approach is significantly slower due to the complex query and filtering logic.
*/
    @Override
    protected Long countRows(DSLContext dslContext, GridRequest request) {
        if (request.getFilterValues().isEmpty() && currentUserCanReadUnassigned(iterationId)) {
            return automatedSuiteDisplayDao.countAutomatedSuiteByIterationId(iterationId);
        } else {
            return super.countRows(dslContext, request);
        }
    }

    @Override
    protected Table<?> getTable() {
        return getSelectFields()
                .from(AUTOMATED_SUITE)
                .innerJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(AUTOMATED_SUITE.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .where(AUTOMATED_SUITE.ITERATION_ID.eq(iterationId))
                .union(
                    getSelectFields()
                                .from(AUTOMATED_SUITE)
                                .innerJoin(ITERATION_TEST_SUITE)
                                .on(AUTOMATED_SUITE.TEST_SUITE_ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
                                .innerJoin(CAMPAIGN_ITERATION)
                                .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITERATION_TEST_SUITE.ITERATION_ID))
                                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .where(ITERATION_TEST_SUITE.ITERATION_ID.eq(iterationId))
                .union(
                    getSelectFields()
                                .from(AUTOMATED_SUITE)
                                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                                .on(
                                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                .innerJoin(ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(CAMPAIGN_ITERATION)
                                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                .where(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(iterationId))
                    )
                )
                .asTable();
    }

    private boolean currentUserCanReadUnassigned(Long iterationId) {
        return this.permissionEvaluationService.hasRoleOrPermissionOnObject(
                Roles.ROLE_ADMIN,
                Permissions.READ_UNASSIGNED.name(),
                iterationId,
                Iteration.SIMPLE_CLASS_NAME);
    }
}
