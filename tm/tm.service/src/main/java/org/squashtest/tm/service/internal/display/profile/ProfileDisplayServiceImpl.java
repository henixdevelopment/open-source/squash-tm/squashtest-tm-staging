/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.profile;

import static org.squashtest.tm.domain.acl.AclGroup.isSystem;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.List;
import java.util.Objects;
import javax.persistence.EntityNotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.profile.ProfileDisplayService;
import org.squashtest.tm.service.internal.display.dto.PermissionsDto;
import org.squashtest.tm.service.internal.display.dto.ProfileAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.ProfileDto;
import org.squashtest.tm.service.internal.display.dto.ProfilePermissionsDto;
import org.squashtest.tm.service.internal.repository.display.ProfileDisplayDao;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Service
@Transactional(readOnly = true)
public class ProfileDisplayServiceImpl implements ProfileDisplayService {

    private final ProfileDisplayDao profileDisplayDao;
    private final PermissionEvaluationService permissionEvaluationService;
    private final UltimateLicenseAvailabilityService ultimateLicenseService;

    public ProfileDisplayServiceImpl(
            ProfileDisplayDao profileDisplayDao,
            PermissionEvaluationService permissionEvaluationService,
            UltimateLicenseAvailabilityService ultimateLicenseService) {
        this.profileDisplayDao = profileDisplayDao;
        this.permissionEvaluationService = permissionEvaluationService;
        this.ultimateLicenseService = ultimateLicenseService;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public List<ProfileDto> findAll() {
        return profileDisplayDao.findAll();
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public ProfileAdminViewDto getProfileView(long profileId) {
        ProfileAdminViewDto profile = profileDisplayDao.findProfileAdminViewDto(profileId);
        if (Objects.isNull(profile)) {
            throw new EntityNotFoundException(
                    String.format("%s %d %s", "Profile with id", profileId, "not found."));
        }

        profile.setPermissions(profileDisplayDao.fetchProfilePermissions(profileId));
        profile.setPartyProfileAuthorizations(
                profileDisplayDao.fetchPartyProfileAuthorizations(profileId));

        return profile;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public List<PermissionsDto> getPermissions(long profileId) {
        return profileDisplayDao.fetchProfilePermissions(profileId);
    }

    @Override
    public List<ProfilePermissionsDto> getProfilesAndPermissions() {
        permissionEvaluationService.checkAtLeastOneClearanceManagementPermissionOrAdmin();
        List<ProfilePermissionsDto> profilesAndPermissions =
                profileDisplayDao.fetchProfilesAndPermissions();

        if (ultimateLicenseService.isAvailable()) {
            return profilesAndPermissions;
        }

        return profilesAndPermissions.stream()
                .filter(profile -> isSystem(profile.profileName()))
                .toList();
    }
}
