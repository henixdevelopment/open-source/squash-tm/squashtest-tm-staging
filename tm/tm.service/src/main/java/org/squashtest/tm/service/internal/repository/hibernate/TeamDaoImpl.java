/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM_MEMBER;

import java.util.List;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.squashtest.tm.service.internal.repository.CustomTeamDao;

public class TeamDaoImpl implements CustomTeamDao {
    @Inject private DSLContext dsl;

    @Override
    public List<Long> findTeamIds(Long userId) {
        return dsl.select(CORE_TEAM.PARTY_ID)
                .from(CORE_TEAM)
                .join(CORE_TEAM_MEMBER)
                .on(CORE_TEAM_MEMBER.TEAM_ID.eq(CORE_TEAM.PARTY_ID))
                .where(CORE_TEAM_MEMBER.USER_ID.eq(userId))
                .fetch(CORE_TEAM.PARTY_ID, Long.class);
    }
}
