/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.campaign;

import java.util.List;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignFolderDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignMultiSelectionDto;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationPlanningDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

public interface CampaignDisplayService {
    CampaignDto getCampaignView(long campaignId);

    LibraryDto getCampaignLibraryView(long libraryId);

    CampaignFolderDto getCampaignFolderView(long campaignFolderId);

    List<IterationPlanningDto> findIterationsPlanningByCampaign(Long campaignId);

    GridResponse findTestPlan(Long campaignId, GridRequest gridRequest);

    /**
     * Method for retrieving campaign library node full names from campaign library node ids
     *
     * @param campaignLibraryNodeIds list of campaign library node ids
     * @return ordered list of campaign full names (ref - name) or folder names
     */
    List<String> retrieveFullNameByCampaignLibraryNodeIds(
            List<Long> campaignLibraryNodeIds, List<Long> projectIds);

    CampaignMultiSelectionDto getCampaignMultiView(NodeReferences nodeReferences);

    boolean isMultiSelectionScopeValid(NodeReferences nodeReferences);
}
