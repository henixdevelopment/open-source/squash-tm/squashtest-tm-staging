/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.environmentvariable;

import java.util.List;
import javax.transaction.Transactional;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.display.environmentvariable.EnvironmentVariableDisplayService;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableOptionDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.EnvironmentVariableGrid;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.display.EnvironmentVariableDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Service
@Transactional
public class EnvironmentVariableDisplayServiceImpl implements EnvironmentVariableDisplayService {

    private final DSLContext dsl;
    private final EnvironmentVariableDisplayDao dao;
    private final EnvironmentVariableBindingDao bindingDao;
    private final PermissionEvaluationService permissionEvaluationService;

    public EnvironmentVariableDisplayServiceImpl(
            DSLContext dsl,
            EnvironmentVariableDisplayDao dao,
            EnvironmentVariableBindingDao bindingDao,
            PermissionEvaluationService permissionEvaluationService) {
        this.dsl = dsl;
        this.dao = dao;
        this.bindingDao = bindingDao;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    @Override
    public GridResponse findAll(GridRequest request) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        EnvironmentVariableGrid evGrid = new EnvironmentVariableGrid();
        return evGrid.getRows(request, dsl);
    }

    @Override
    public List<EnvironmentVariableDto> getAllEnvironmentVariableReferences() {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        return dao.getAllEnvironmentVariableDto();
    }

    @Override
    public EnvironmentVariableDto getEnvironmentVariableView(Long environmentVariableId) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        EnvironmentVariableDto dto = dao.getEnvironmentVariableById(environmentVariableId);
        dto.setIsBoundToServer(isEnvironmentVariableBound(dto.getId()));
        return dto;
    }

    @Override
    public List<EnvironmentVariableOptionDto> getOptionsByEvId(Long environmentVariableId) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        return dao.getEnvironmentVariableOptionListByEvId(environmentVariableId);
    }

    private boolean isEnvironmentVariableBound(Long environmentVariableId) {
        return !bindingDao.findAllByEnvironmentVariable_Id(environmentVariableId).isEmpty();
    }
}
