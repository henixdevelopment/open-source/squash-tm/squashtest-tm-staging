/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.referential;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.documentation.DocumentationLinkProvider;
import org.squashtest.tm.service.internal.display.dto.AiServerDto;
import org.squashtest.tm.service.internal.display.dto.BugTrackerReferentialDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldDto;
import org.squashtest.tm.service.internal.display.dto.GlobalConfigurationDto;
import org.squashtest.tm.service.internal.dto.DetailedUserDto;
import org.squashtest.tm.service.spi.ScmServerKind;

public class AdminReferentialData {
    private DetailedUserDto user;
    private GlobalConfigurationDto globalConfiguration;
    private LicenseInformationDto licenseInformation;
    private List<CustomFieldDto> customFields = new ArrayList<>();
    private Set<TestAutomationServerKind> availableTestAutomationServerKinds;
    private Set<ScmServerKind> availableScmServerKinds;
    private boolean canManageLocalPassword;
    private List<TemplateConfigurablePluginDto> templateConfigurablePlugins;
    private List<BugTrackerReferentialDto> bugTrackers;
    private List<DocumentationLinkProvider.Link> documentationLinks;
    private String callbackUrl;
    private boolean premiumPluginInstalled;
    private boolean ultimateLicenseAvailable;
    private List<String> availableReportIds;
    private List<SynchronisationPluginDto> synchronizationPlugins = new ArrayList<>();
    private List<AiServerDto> aiServers = new ArrayList<>();
    private boolean jwtSecretDefined;
    private boolean isAdmin;
    private boolean unsafeAttachmentPreviewEnabled;

    public DetailedUserDto getUser() {
        return user;
    }

    public void setUser(DetailedUserDto user) {
        this.user = user;
    }

    public GlobalConfigurationDto getGlobalConfiguration() {
        return globalConfiguration;
    }

    public void setGlobalConfiguration(GlobalConfigurationDto globalConfiguration) {
        this.globalConfiguration = globalConfiguration;
    }

    public LicenseInformationDto getLicenseInformation() {
        return licenseInformation;
    }

    public void setLicenseInformation(LicenseInformationDto licenseInformation) {
        this.licenseInformation = licenseInformation;
    }

    public List<CustomFieldDto> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(List<CustomFieldDto> customFields) {
        this.customFields = customFields;
    }

    public Set<TestAutomationServerKind> getAvailableTestAutomationServerKinds() {
        return availableTestAutomationServerKinds;
    }

    public void setAvailableTestAutomationServerKinds(
            Set<TestAutomationServerKind> availableTestAutomationServerKinds) {
        this.availableTestAutomationServerKinds = availableTestAutomationServerKinds;
    }

    public Set<ScmServerKind> getAvailableScmServerKinds() {
        return availableScmServerKinds;
    }

    public void setAvailableScmServerKinds(Set<ScmServerKind> availableScmServerKinds) {
        this.availableScmServerKinds = availableScmServerKinds;
    }

    public boolean isCanManageLocalPassword() {
        return canManageLocalPassword;
    }

    public void setCanManageLocalPassword(boolean canManageLocalPassword) {
        this.canManageLocalPassword = canManageLocalPassword;
    }

    public List<TemplateConfigurablePluginDto> getTemplateConfigurablePlugins() {
        return templateConfigurablePlugins;
    }

    public void setTemplateConfigurablePlugins(
            List<TemplateConfigurablePluginDto> templateConfigurablePlugins) {
        this.templateConfigurablePlugins = templateConfigurablePlugins;
    }

    public List<BugTrackerReferentialDto> getBugTrackers() {
        return bugTrackers;
    }

    public void setBugTrackers(List<BugTrackerReferentialDto> bugTrackers) {
        this.bugTrackers = bugTrackers;
    }

    public List<DocumentationLinkProvider.Link> getDocumentationLinks() {
        return documentationLinks;
    }

    public void setDocumentationLinks(List<DocumentationLinkProvider.Link> documentationLinks) {
        this.documentationLinks = documentationLinks;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public boolean isPremiumPluginInstalled() {
        return premiumPluginInstalled;
    }

    public void setPremiumPluginInstalled(boolean premiumPluginInstalled) {
        this.premiumPluginInstalled = premiumPluginInstalled;
    }

    public List<String> getAvailableReportIds() {
        return availableReportIds;
    }

    public void setAvailableReportIds(List<String> availableReportIds) {
        this.availableReportIds = availableReportIds;
    }

    public boolean isUltimateLicenseAvailable() {
        return ultimateLicenseAvailable;
    }

    public void setUltimateLicenseAvailable(boolean ultimateLicenseAvailable) {
        this.ultimateLicenseAvailable = ultimateLicenseAvailable;
    }

    public List<SynchronisationPluginDto> getSynchronizationPlugins() {
        return synchronizationPlugins;
    }

    public void setSynchronizationPlugins(List<SynchronisationPluginDto> synchronisationPlugins) {
        this.synchronizationPlugins = synchronisationPlugins;
    }

    public List<AiServerDto> getAiServers() {
        return aiServers;
    }

    public void setAiServers(List<AiServerDto> aiServers) {
        this.aiServers = aiServers;
    }

    public boolean isJwtSecretDefined() {
        return jwtSecretDefined;
    }

    public void setJwtSecretDefined(boolean jwtSecretDefined) {
        this.jwtSecretDefined = jwtSecretDefined;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isUnsafeAttachmentPreviewEnabled() {
        return unsafeAttachmentPreviewEnabled;
    }

    public void setUnsafeAttachmentPreviewEnabled(boolean unsafeAttachmentPreviewEnabled) {
        this.unsafeAttachmentPreviewEnabled = unsafeAttachmentPreviewEnabled;
    }
}
