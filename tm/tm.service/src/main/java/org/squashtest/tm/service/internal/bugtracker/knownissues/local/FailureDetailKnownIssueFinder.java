/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import org.jooq.DSLContext;
import org.jooq.Record5;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.bugtracker.knownissues.local.FailureDetailLocalKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssue;

@Repository
@Transactional(readOnly = true)
public class FailureDetailKnownIssueFinder
        extends BaseLocalKnownIssueFinder<Record5<Long, Long, String, String, String>> {

    protected FailureDetailKnownIssueFinder(DSLContext dsl) {
        super(dsl);
    }

    @Override
    protected SelectHavingStep<Record5<Long, Long, String, String, String>> selectKnownIssues(
            long failureDetailId) {
        return dsl.select(
                        PROJECT.PROJECT_ID,
                        ISSUE.BUGTRACKER_ID,
                        ISSUE.REMOTE_ISSUE_ID,
                        DSL.groupConcatDistinct(EXECUTION.EXECUTION_ID),
                        DSL.groupConcatDistinct(ISSUE.ISSUE_ID))
                .from(
                        ISSUE
                                .innerJoin(FAILURE_DETAIL)
                                .on(FAILURE_DETAIL.ISSUE_LIST_ID.eq(ISSUE.ISSUE_LIST_ID))
                                .innerJoin(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                                .on(
                                        AUTOMATED_EXECUTION_FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(
                                                FAILURE_DETAIL.FAILURE_DETAIL_ID))
                                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                                .on(
                                        AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.eq(
                                                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID))
                                .innerJoin(EXECUTION)
                                .on(EXECUTION.EXECUTION_ID.eq(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID))
                                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                                .on(FAILURE_DETAIL.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                                .innerJoin(ITEM_TEST_PLAN_LIST)
                                .on(
                                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                                ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                .innerJoin(CAMPAIGN_ITERATION)
                                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                                .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
                                .innerJoin(PROJECT)
                                .on(PROJECT.PROJECT_ID.eq(CAMPAIGN_LIBRARY_NODE.PROJECT_ID)))
                .where(FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(failureDetailId))
                .and(ISSUE.BUGTRACKER_ID.eq(PROJECT.BUGTRACKER_ID))
                .groupBy(PROJECT.PROJECT_ID, ISSUE.BUGTRACKER_ID, ISSUE.REMOTE_ISSUE_ID);
    }

    @Override
    protected LocalKnownIssue buildIssueFromRecord(
            Record5<Long, Long, String, String, String> record) {
        return new FailureDetailLocalKnownIssue(
                record.component1(),
                record.component2(),
                record.component3(),
                LocalKnownIssueFinderHelper.parseLongsAndSortDesc(record.component4()),
                LocalKnownIssueFinderHelper.parseLongs(record.component5()));
    }
}
