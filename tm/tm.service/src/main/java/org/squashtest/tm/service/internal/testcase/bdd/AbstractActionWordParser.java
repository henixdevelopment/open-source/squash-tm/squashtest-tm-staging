/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bdd.ActionWordFragment;
import org.squashtest.tm.domain.bdd.ActionWordParameter;
import org.squashtest.tm.domain.bdd.ActionWordParameterValue;
import org.squashtest.tm.domain.bdd.ActionWordText;
import org.squashtest.tm.domain.bdd.util.ActionWordUtil;
import org.squashtest.tm.exception.actionword.InvalidActionWordInputException;
import org.squashtest.tm.exception.actionword.InvalidActionWordParameterNameException;

/**
 * @author qtran - created on 27/07/2020
 * @author pckerneis - rewrote parsing (11/2022). See {@link ActionWordTokenizer}.
 */
public abstract class AbstractActionWordParser {
    private static final String ACTION_WORD_PARAM_NAME_PREFIX = "param";

    private final List<ActionWordParameterValue> parameterValues = new ArrayList<>();
    protected final List<String> paramNames = new ArrayList<>();
    private int generatedParamIndex = 0;
    protected final List<ActionWordFragment> fragments = new ArrayList<>();

    public List<ActionWordParameterValue> getParameterValues() {
        return parameterValues;
    }

    protected List<ActionWordTokenizer.AWToken> checkInputAndParseTokens(
            String trimmedInput, boolean allowsNamedParameters) {
        resetState();

        checkIfInputNullOrEmpty(trimmedInput);
        checkIfActionWordContainsIllegalEscapeSequences(trimmedInput);
        checkIfInputExceed255Char(trimmedInput);

        final ActionWordTokenizer tokenizer =
                new ActionWordTokenizer(trimmedInput, allowsNamedParameters);
        final List<ActionWordTokenizer.AWToken> tokens = tokenizer.tokenize();
        checkIfActionWordHasText(tokens);
        return tokens;
    }

    private void resetState() {
        parameterValues.clear();
        paramNames.clear();
        fragments.clear();
        generatedParamIndex = 0;
    }

    protected void addTextFragment(ActionWordTokenizer.AWToken token) {
        ActionWordText text = new ActionWordText(token.getLexeme());
        fragments.add(text);
    }

    protected void addNumberFragment(ActionWordTokenizer.AWToken token) {
        final String value = token.getLexeme();
        addParameter(getNextParamName(), value, value);
    }

    protected void addParameter(String paramName, String defaultValue, String currentValue) {
        if (paramNames.contains(paramName)) {
            throw new InvalidActionWordParameterNameException(
                    "Action word parameter name must be unique.");
        }

        final ActionWordParameter param = new ActionWordParameter(paramName, defaultValue);
        final ActionWordParameterValue paramValue = new ActionWordParameterValue(currentValue);

        paramNames.add(paramName);
        parameterValues.add(paramValue);
        fragments.add(param);
    }

    protected String getNextParamName() {
        String candidate;

        do {
            candidate = ACTION_WORD_PARAM_NAME_PREFIX + (++generatedParamIndex);
        } while (paramNames.contains(candidate));

        return candidate;
    }

    private void checkIfInputNullOrEmpty(String trimmedInput) {
        if (trimmedInput == null || trimmedInput.isEmpty()) {
            throw new InvalidActionWordInputException("Action word cannot be empty.");
        }
    }

    private void checkIfInputExceed255Char(String trimmedInput) {
        if (trimmedInput.length() > ActionWord.ACTION_WORD_MAX_LENGTH) {
            throw new InvalidActionWordInputException("Action word cannot exceed 255 characters.");
        }
    }

    protected void checkIfActionWordHasText(List<ActionWordTokenizer.AWToken> tokens) {
        final long textCount =
                tokens.stream()
                        .map(ActionWordTokenizer.AWToken::getType)
                        .filter(ActionWordTokenizer.AWTokenType.PLAIN_TEXT::equals)
                        .count();

        if (textCount == 0) {
            throw new InvalidActionWordInputException("Action word must contain at least some texts.");
        }
    }

    private void checkIfActionWordContainsIllegalEscapeSequences(String trimmedInput) {
        int charIndex = 0;

        if (trimmedInput.indexOf('\\') < 0) {
            return;
        }

        while ((charIndex = trimmedInput.indexOf('\\', charIndex)) >= 0) {
            if (charIndex == trimmedInput.length() - 1) {
                throw new InvalidEscapeSequenceException(charIndex + 1, trimmedInput);
            }

            final char nextChar = trimmedInput.charAt(charIndex + 1);

            if (!ActionWordUtil.isEscapableCharacter(nextChar)) {
                throw new InvalidEscapeSequenceException(charIndex + 1, trimmedInput);
            }

            if (nextChar == '\\') {
                charIndex += 2;
            } else {
                charIndex += 1;
            }
        }
    }

    static class InvalidEscapeSequenceException extends ContextualizedActionWordException {
        InvalidEscapeSequenceException(int illegalEscapeSequencePosition, String textInput) {
            super(
                    String.format("Invalid escape sequence at position %s.", illegalEscapeSequencePosition),
                    textInput,
                    illegalEscapeSequencePosition);
        }

        @Override
        public String getI18nKey() {
            return "sqtm-core.error.action-word.invalid-escape-sequence";
        }
    }
}
