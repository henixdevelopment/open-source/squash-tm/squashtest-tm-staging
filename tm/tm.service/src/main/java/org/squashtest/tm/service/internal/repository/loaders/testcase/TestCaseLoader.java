/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.loaders.testcase;

import java.util.EnumSet;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.internal.repository.loaders.common.Loader;

public interface TestCaseLoader extends Loader<TestCase, Long, EnumSet<TestCaseLoader.Options>> {

    enum Options {
        FETCH_AUTOMATED_TEST("automatedTest"),
        FETCH_AUTOMATED_TEST_TECHNOLOGY("automatedTestTechnology"),
        FETCH_AUTOMATION_REQUEST("automationRequest"),
        FETCH_DATASETS("datasets"),
        FETCH_MILESTONES("milestones"),
        FETCH_NATURE("nature"),
        FETCH_PARAMETERS("parameters"),
        FETCH_REQUIREMENT_VERSION_COVERAGES("requirementVersionCoverages"),
        FETCH_SCM_REPO("scmRepository"),
        FETCH_STEPS("steps"),
        FETCH_TYPE("type"),
        FETCH_ATTACHMENT_LIST("attachmentList");

        public final String field;

        Options(String field) {
            this.field = field;
        }
    }
}
