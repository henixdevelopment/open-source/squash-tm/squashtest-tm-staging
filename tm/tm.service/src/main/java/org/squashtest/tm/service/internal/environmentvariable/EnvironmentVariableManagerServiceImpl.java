/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption;
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.exception.customfield.StringDoesNotMatchesPatternException;
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentVariableManagerService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingValueService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableManagerService;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao;

@Transactional
@Service("EnvironmentVariableManagerService")
public class EnvironmentVariableManagerServiceImpl implements EnvironmentVariableManagerService {

    private final EnvironmentVariableDao environmentVariableDao;
    private final EnvironmentVariableBindingService environmentVariableBindingService;
    private final EnvironmentVariableBindingValueService environmentVariableValueService;
    private final DenormalizedEnvironmentVariableManagerService
            denormalizedEnvironmentVariableManagerService;

    public EnvironmentVariableManagerServiceImpl(
            EnvironmentVariableDao environmentVariableDao,
            EnvironmentVariableBindingService environmentVariableBindingService,
            EnvironmentVariableBindingValueService environmentVariableValueService,
            DenormalizedEnvironmentVariableManagerService denormalizedEnvironmentVariableManagerService) {
        this.environmentVariableDao = environmentVariableDao;
        this.environmentVariableBindingService = environmentVariableBindingService;
        this.environmentVariableValueService = environmentVariableValueService;
        this.denormalizedEnvironmentVariableManagerService =
                denormalizedEnvironmentVariableManagerService;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void persist(EnvironmentVariable environmentVariable) {
        checkDuplicateName(environmentVariable.getName());
        checkNameMatchPattern(environmentVariable.getName());
        environmentVariableDao.save(environmentVariable);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteEnvironmentVariable(List<Long> environmentVariableIds) {
        environmentVariableIds.forEach(
                id -> {
                    environmentVariableBindingService.unbindByEnvironmentVariableId(id);
                    EnvironmentVariable environmentVariable = environmentVariableDao.getReferenceById(id);
                    denormalizedEnvironmentVariableManagerService.handleEnvironmentVariableDeletion(
                            environmentVariable);
                    environmentVariableDao.delete(environmentVariable);
                });
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeName(Long evId, String newName) {
        String trimmedNewName = newName.trim();
        EnvironmentVariable environmentVariable = environmentVariableDao.getReferenceById(evId);
        String oldName = environmentVariable.getName();

        if (StringUtils.equals(oldName, trimmedNewName)) {
            return;
        }
        checkNameMatchPattern(newName);
        checkDuplicateName(newName);
        environmentVariable.setName(trimmedNewName);
    }

    private void checkNameMatchPattern(String newName) {
        if (!newName.matches(EnvironmentVariable.NAME_REGEXP)) {
            throw new StringDoesNotMatchesPatternException(
                    newName, EnvironmentVariable.NAME_REGEXP, "name");
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeOptionLabel(long environmentVariableId, String optionLabel, String newLabel) {
        if (newLabel == null || newLabel.trim().isEmpty()) {
            throw new RequiredFieldException("label");
        }
        SingleSelectEnvironmentVariable environmentVariable =
                environmentVariableDao.findSingleSelectEnvironmentVariableById(environmentVariableId);
        environmentVariable.changeOptionLabel(optionLabel, newLabel);
        environmentVariableValueService.replaceAllExistingValuesByEvId(
                environmentVariableId, optionLabel, newLabel);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addOption(long evId, String option) {
        SingleSelectEnvironmentVariable environmentVariable =
                environmentVariableDao.findSingleSelectEnvironmentVariableById(evId);
        EnvironmentVariableOption environmentVariableOption = new EnvironmentVariableOption(option);
        environmentVariable.addOption(environmentVariableOption);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void removeOptions(Long evId, List<String> optionLabels) {
        optionLabels.forEach(
                optionLabel -> {
                    SingleSelectEnvironmentVariable environmentVariable =
                            environmentVariableDao.findSingleSelectEnvironmentVariableById(evId);
                    environmentVariable.removeOptionAndReorderList(optionLabel);
                });
        environmentVariableValueService.reinitializeEnvironmentVariableValuesByValueAndEvId(
                optionLabels, evId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeOptionsPosition(
            Long environmentVariableId, Integer position, List<String> labels) {
        SingleSelectEnvironmentVariable environmentVariable =
                environmentVariableDao.findSingleSelectEnvironmentVariableById(environmentVariableId);
        environmentVariable.moveOptions(position, labels);
    }

    private void checkDuplicateName(String name) {
        EnvironmentVariable nameDuplicate = environmentVariableDao.findByName(name);
        if (nameDuplicate != null) {
            throw new NameAlreadyInUseException("EnvironmentVariable", HtmlUtils.htmlEscape(name));
        }
    }
}
