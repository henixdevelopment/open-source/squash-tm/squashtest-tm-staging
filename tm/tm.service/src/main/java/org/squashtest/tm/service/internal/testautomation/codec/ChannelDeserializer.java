/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.codec;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;

public class ChannelDeserializer extends JsonDeserializer<AutomatedExecutionEnvironment> {

    @Override
    public AutomatedExecutionEnvironment deserialize(JsonParser parser, DeserializationContext ctxt)
            throws IOException {
        JsonNode channelNode = parser.getCodec().readTree(parser);
        AutomatedExecutionEnvironment environment = new AutomatedExecutionEnvironment();
        environment.setName(channelNode.get("metadata").get("name").textValue());

        String namespacesText = channelNode.get("metadata").get("namespaces").textValue();
        environment.setNamespaces(Arrays.asList(namespacesText.split(",")));

        ArrayNode tagArrayNode = (ArrayNode) channelNode.get("spec").get("tags");
        environment.setTags(getStringValuesFromArrayNode(tagArrayNode));

        environment.setStatus(channelNode.get("status").get("phase").textValue());

        return environment;
    }

    private List<String> getStringValuesFromArrayNode(ArrayNode arrayNode) {
        List<String> outputValues = new ArrayList<>();
        Iterator<JsonNode> inputValues = arrayNode.elements();
        while (inputValues.hasNext()) {
            outputValues.add(inputValues.next().textValue());
        }
        return outputValues;
    }
}
