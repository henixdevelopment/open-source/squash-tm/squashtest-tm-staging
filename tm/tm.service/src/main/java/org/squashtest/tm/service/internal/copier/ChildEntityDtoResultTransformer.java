/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.copier;

import java.io.Serial;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.transform.ResultTransformer;
import org.squashtest.tm.domain.library.TreeNode;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;

public class ChildEntityDtoResultTransformer implements ResultTransformer {
    @Serial private static final long serialVersionUID = 2374818507417800346L;
    private final Map<Long, ChildEntityDto> childEntityDtoMap = new LinkedHashMap<>();
    private final ClipboardPayload clipboardPayload;
    private int entitiesCount = 0;

    public ChildEntityDtoResultTransformer(ClipboardPayload clipboardPayload) {
        this.clipboardPayload = clipboardPayload;
    }

    @Override
    public ChildEntityDto transformTuple(Object[] tuple, String[] aliases) {
        TreeNode treeNode = (TreeNode) tuple[0];
        Long id = (Long) tuple[1];
        entitiesCount++;
        if (!clipboardPayload.shouldWhiteListBeIgnored()
                && !clipboardPayload.getWhiteListNodeIds().contains(treeNode.getId())) {
            return null;
        }
        ChildEntityDto childEntityDto =
                childEntityDtoMap.computeIfAbsent(id, k -> new ChildEntityDto(id));
        childEntityDto.getChildren().add(treeNode);
        return childEntityDto;
    }

    @Override
    public List<ChildEntityDto> transformList(List collection) {
        return new ArrayList<>(childEntityDtoMap.values());
    }

    public int getEntitiesCount() {
        return entitiesCount;
    }
}
