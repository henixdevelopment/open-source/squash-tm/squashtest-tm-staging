/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import org.springframework.context.MessageSource;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.service.internal.testcase.bdd.robot.KeywordSectionBuilder;
import org.squashtest.tm.service.internal.testcase.bdd.robot.SettingSectionBuilder;
import org.squashtest.tm.service.internal.testcase.bdd.robot.TestCaseSectionBuilder;

public class RobotScriptWriter implements BddScriptWriter {
    /**
     * The implementation for Robot Framework
     * <li>does not escape arrow symbols: escapeArrows is not used
     *
     * @param testCase the test case
     * @param messageSource message source
     * @param escapeArrows whether to escape arrow symbols
     * @return the Robot Script of the given KeywordTestCase
     */
    @Override
    public String writeBddScript(
            KeywordTestCase testCase, MessageSource messageSource, boolean escapeArrows) {
        return SettingSectionBuilder.buildSettingsSection(testCase, messageSource)
                + TestCaseSectionBuilder.buildTestCasesSection(testCase)
                + KeywordSectionBuilder.buildKeywordsSection(testCase);
    }

    public static String writeBddStepScript(
            KeywordTestStep testStep, int dataTableCounter, int docStringCounter) {
        return TestCaseSectionBuilder.writeBddStepScript(testStep, dataTableCounter, docStringCounter);
    }
}
