/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;

public class ProfileAdminViewDto extends ProfileDto {
    private final String description;
    private final String createdBy;
    private final Date createdOn;
    private final String lastModifiedBy;
    private final Date lastModifiedOn;
    private List<PermissionsDto> permissions = new ArrayList<>();
    private List<PartyProfileAuthorizationsDto> partyProfileAuthorizations = new ArrayList<>();

    public ProfileAdminViewDto( // NOSONAR
            long id,
            String qualifiedName,
            int partyCount,
            String description,
            boolean active,
            String createdBy,
            Date createdOn,
            String lastModifiedBy,
            Date lastModifiedOn) {
        super(id, qualifiedName, partyCount, active);
        this.description = description;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedOn = lastModifiedOn;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public List<PermissionsDto> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionsDto> permissions) {
        this.permissions = permissions;
    }

    public List<PartyProfileAuthorizationsDto> getPartyProfileAuthorizations() {
        return partyProfileAuthorizations;
    }

    public void setPartyProfileAuthorizations(
            List<PartyProfileAuthorizationsDto> partyProfileAuthorizations) {
        this.partyProfileAuthorizations = partyProfileAuthorizations;
    }
}
