/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.xrayimporter.topivot;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import gherkin.ParserException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Record1;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CalledTestXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CustomFieldXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayImportModel;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayItemStatus;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.ActionStepToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.CalledTestCasesToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.CommonTestCase;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.DatasetParamToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.DatasetToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.GherkinScriptedStep;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.TestCaseFolderToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.TestCaseGherkinToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.TestCaseManualToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace.TreeFolder;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayTestType;
import org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception.ImportXrayException;
import org.squashtest.tm.service.projectimporter.xrayimporter.SquashRules;
import org.squashtest.tm.service.projectimporter.xrayimporter.XrayTablesDao;
import org.squashtest.tm.service.projectimporter.xrayimporter.topivot.AbstractImporterXrayHelper;
import org.squashtest.tm.service.projectimporter.xrayimporter.topivot.TestCaseToPivotImporterService;

@Service("TestCaseToPivotImporterService")
public class TestCaseToPivotImporterServiceImpl extends AbstractImporterXrayHelper
        implements TestCaseToPivotImporterService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestCaseToPivotImporterServiceImpl.class);

    private final XrayTablesDao xrayTablesDao;
    private final SquashRules squashRules;

    private final Map<String, String> testCaseFolderPathPivotIdMap =
            new HashMap<>(); // Key : path folder -- Value : pivot id
    private String precedingGherkinLanguage;

    public TestCaseToPivotImporterServiceImpl(XrayTablesDao xrayTablesDao, SquashRules squashRules) {
        this.xrayTablesDao = xrayTablesDao;
        this.squashRules = squashRules;
    }

    @Override
    public void writeTestCaseFolder(
            JsonFactory jsonFactory,
            PrintWriter logWriter,
            ArchiveOutputStream<ZipArchiveEntry> archive,
            XrayImportModel xrayImportModel)
            throws IOException {
        boolean isEmptyEntity = xrayTablesDao.isEmptyTestCaseFolder(XrayField.Type.TEST);
        writeJson(
                jsonFactory,
                this::handleTestCaseFolder,
                JsonImportFile.TEST_CASE_FOLDERS.getFileName(),
                archive,
                isEmptyEntity);
    }

    @Override
    public void writeTestCase(
            JsonFactory jsonFactory,
            PrintWriter logWriter,
            ArchiveOutputStream<ZipArchiveEntry> archive,
            XrayImportModel xrayImportModel)
            throws IOException {
        boolean isEmptyEntity = xrayTablesDao.isEmptyItemTable(XrayField.Type.TEST);
        logWriter.write(
                String.format("%sTest Cases%s", System.lineSeparator(), System.lineSeparator()));
        writeJson(
                jsonFactory,
                jsonGenerator -> handleTestCase(jsonGenerator, logWriter, xrayImportModel),
                JsonImportFile.TEST_CASES.getFileName(),
                archive,
                isEmptyEntity);
    }

    @Override
    public void writeCalledTestCase(
            JsonFactory jsonFactory,
            PrintWriter logWriter,
            ArchiveOutputStream<ZipArchiveEntry> archive,
            XrayImportModel xrayImportModel)
            throws IOException {
        boolean isEmptyEntity = xrayTablesDao.isEmptyCalledTestCase();
        logWriter.write(
                String.format("%sCalled Test Cases%s", System.lineSeparator(), System.lineSeparator()));
        writeJson(
                jsonFactory,
                jsonGenerator -> handleCalledTestCases(jsonGenerator, logWriter, xrayImportModel),
                JsonImportFile.CALLED_TEST_CASES.getFileName(),
                archive,
                isEmptyEntity);
    }

    private void handleTestCaseFolder(JsonGenerator jsonGenerator) throws IOException {
        TreeFolder rootFolder = handleTestRepository();
        jsonGenerator.writeFieldName(JsonImportField.TEST_CASE_FOLDERS);
        jsonGenerator.writeStartArray();
        writeEachFolder(jsonGenerator, rootFolder); // create basic folder (id, name, parent_type)
        jsonGenerator.writeEndArray();
    }

    private TreeFolder handleTestRepository() {
        List<String> folders =
                xrayTablesDao
                        .selectTypeRepositoryFolder(XrayField.Type.TEST)
                        .map(Record1::value1)
                        .map(this::extractFolderPath)
                        .flatMap(List::stream)
                        .distinct()
                        .toList();
        return createTreeFolder(folders);
    }

    private List<String> extractFolderPath(String path) {
        Document document = Jsoup.parse(path);
        return document.body().getAllElements().stream().map(Element::text).toList();
    }

    private TreeFolder createTreeFolder(List<String> folders) {
        AtomicInteger idGenerator = new AtomicInteger(0);
        TreeFolder rootTree = new TreeFolder();
        TreeFolder current = rootTree;
        for (String folder : folders) {
            for (String data : folder.split("/")) {
                if (!data.isEmpty()) {
                    current = current.child(data, idGenerator);
                }
            }
            current = rootTree;
        }
        return rootTree;
    }

    private void writeEachFolder(JsonGenerator jsonGenerator, TreeFolder treeRoot)
            throws IOException {
        if (StringUtils.isNotEmpty(treeRoot.getFullPath())) {
            TestCaseFolderToPivot testCaseFolderToPivot = new TestCaseFolderToPivot();
            testCaseFolderToPivot.setPivotId(treeRoot.getId());
            testCaseFolderToPivot.setName(treeRoot.getData());
            testCaseFolderToPivot.setParentType(treeRoot.isRootFolder());
            if (!treeRoot.isRootFolder()) {
                testCaseFolderToPivot.setParentId(treeRoot.getParentId());
            }
            testCaseFolderPathPivotIdMap.put(treeRoot.getFullPath(), testCaseFolderToPivot.getPivotId());
            jsonGenerator.writeObject(testCaseFolderToPivot);
        }
        for (TreeFolder child : treeRoot.getChildren()) {
            writeEachFolder(jsonGenerator, child);
        }
    }

    private void handleTestCase(
            JsonGenerator jsonGenerator, PrintWriter logWriter, XrayImportModel xrayImportModel)
            throws IOException {
        squashRules.handleDuplicateNameWithParentType(
                xrayTablesDao.selectDuplicateNameGroupBy(
                        XrayField.CustomFieldKey.TEST_REPOSITORY_PATH, XrayField.Type.TEST));
        jsonGenerator.writeFieldName(JsonImportField.TEST_CASES);
        jsonGenerator.writeStartArray();
        XrayImportModel.EntityCount entityCount =
                new XrayImportModel.EntityCount(XrayImportModel.EntityType.TEST_CASE);
        xrayTablesDao.selectTest(
                consumerThrowingIOException(
                        itemXrayDto -> generateTestCase(jsonGenerator, logWriter, itemXrayDto, entityCount)),
                XrayField.Type.TEST);
        testCaseFolderPathPivotIdMap.clear();
        xrayImportModel.getEntityCounts().add(entityCount);
        jsonGenerator.writeEndArray();
    }

    private void generateTestCase(
            JsonGenerator jsonGenerator,
            PrintWriter logWriter,
            ItemXrayDto itemXrayDto,
            XrayImportModel.EntityCount entityCount)
            throws IOException {
        try {
            CommonTestCase commonTestCase = handleTypeTestCase(itemXrayDto);
            jsonGenerator.writeObject(commonTestCase);
            squashRules.markWithSuccessIfStatusIsNull(itemXrayDto);
        } catch (ImportXrayException importXrayException) {
            LOGGER.error("Error during the generation pivot format: ", importXrayException);
            itemXrayDto.setItemStatus(XrayItemStatus.FAILURE);
        }
        entityCount.countEntity(itemXrayDto);
        writeLog(logWriter, itemXrayDto);
    }

    private CommonTestCase handleTypeTestCase(ItemXrayDto itemXrayDto) {
        String testType = itemXrayDto.getFilterCufValue(XrayField.CustomFieldKey.TEST_TYPE);
        XrayTestType xrayTestType = XrayTestType.getTestType(testType);
        return switch (xrayTestType) {
            case MANUAL -> handleGenericManualTestCase(itemXrayDto, false);
            case GENERIC -> handleGenericManualTestCase(itemXrayDto, true);
            case CUCUMBER -> handleCucumberTestCase(itemXrayDto);
            default -> {
                squashRules.markWithErrorAndThrow(itemXrayDto, typeTestCaseError(testType));
                yield null;
            }
        };
    }

    private static String typeTestCaseError(String testType) {
        String message;
        if (Objects.isNull(testType)) {
            message =
                    String.format(
                            "The value of the test type is null. There is no Custom Field containing the key '%s'",
                            XrayField.CustomFieldKey.TEST_TYPE.getKey());
        } else {
            message =
                    String.format(
                            "The test type '%s' is not equals to %s, %s or %s",
                            testType,
                            XrayTestType.MANUAL.getName(),
                            XrayTestType.GENERIC.getName(),
                            XrayTestType.CUCUMBER.getName());
        }
        return message;
    }

    private CommonTestCase handleGenericManualTestCase(ItemXrayDto itemXrayDto, boolean isGeneric) {
        squashRules.validateMandatoryCommonField(itemXrayDto);
        TestCaseManualToPivot testCaseManualToPivot = new TestCaseManualToPivot();
        testCaseManualToPivot.setTestCaseKind(TestCaseKind.STANDARD);
        testCaseManualToPivot.setXrayKey(itemXrayDto.getKey());
        createCommonPartTestCase(testCaseManualToPivot, itemXrayDto);

        // Create Atomic to generate pivot id
        testCaseManualToPivot.setDatasetId(new AtomicInteger(0));
        testCaseManualToPivot.setDatasetParamId(new AtomicInteger(0));
        testCaseManualToPivot.setActionStepId(new AtomicInteger(0));

        // Handle dataset - dataset Param - Steps
        handleTestCaseDataset(testCaseManualToPivot, itemXrayDto);
        if (isGeneric) {
            handleTestCaseStepGeneric(
                    testCaseManualToPivot,
                    itemXrayDto.getFilterCufs(XrayField.CustomFieldKey.GENERIC_DEFINITION));
        } else {
            handleTestCaseStep(itemXrayDto, testCaseManualToPivot);
        }
        handlePrerequisite(
                itemXrayDto,
                prerequisites ->
                        testCaseManualToPivot.setPrerequisite(generatePrerequisite(prerequisites)));
        return testCaseManualToPivot;
    }

    private String generatePrerequisite(Map<String, String> prerequisites) {
        Document document = new Document("");
        Element body = document.body();
        prerequisites.forEach(
                (key, value) -> {
                    body.appendElement("p").text(String.format("#%s", key));
                    body.appendElement("p").text(value);
                });
        return body.children().stream().map(Node::toString).collect(Collectors.joining());
    }

    private void createCommonPartTestCase(CommonTestCase commonTestCase, ItemXrayDto itemXrayDto) {
        commonTestCase.setPivotId(itemXrayDto.getPivotId());
        commonTestCase.setName(itemXrayDto.getSummary());
        String description =
                generateDescription(itemXrayDto, this::addLinkPriorityStatusLabelToDescription);
        commonTestCase.setDescription(description);
        commonTestCase.setStatus(squashRules.checkStatus(itemXrayDto, itemXrayDto.getStatus()));
        commonTestCase.setImportance(squashRules.checkPriority(itemXrayDto, itemXrayDto.getPriority()));
        commonTestCase.setReference(itemXrayDto.getKey());
        commonTestCase.setParentTypeAndId(
                itemXrayDto.getFilterCufs(XrayField.CustomFieldKey.TEST_REPOSITORY_PATH),
                testCaseFolderPathPivotIdMap);
        // NYI : Create VerifiedRequirement - Bind CUF
    }

    private void handlePrerequisite(
            ItemXrayDto itemXrayDto, Consumer<Map<String, String>> addPrerequisite) {
        List<CustomFieldXrayDto> preconditions =
                itemXrayDto.getFilterCufs(XrayField.CustomFieldKey.TEST_ASSOCIATED_PRECONDITION);
        Map<String, String> preconditionsMap = itemXrayDto.getAssociatedIssuesWithXrayKey();
        if (preconditions.size() == preconditionsMap.size()) {
            if (!preconditionsMap.isEmpty()) {
                addPrerequisite.accept(preconditionsMap);
            }
        } else {
            squashRules.markWithWarning(
                    itemXrayDto,
                    String.format(
                            "Not all preconditions were found in the import (%s)",
                            preconditions.stream()
                                    .map(CustomFieldXrayDto::getValue)
                                    .collect(Collectors.joining(", "))));
        }
    }

    private void checkingDataset(TestCaseManualToPivot testCaseToJson, ItemXrayDto itemXrayDto) {
        testCaseToJson
                .getDatasetParams()
                .forEach(
                        datasetParamToPivot -> {
                            String datasetParamName =
                                    squashRules.checkDatasetName(itemXrayDto, datasetParamToPivot.getName());
                            datasetParamToPivot.setName(datasetParamName);
                        });
        testCaseToJson
                .getDatasets()
                .forEach(
                        datasetToPivot -> checkReferenceDatasetParamInsideDataset(datasetToPivot, itemXrayDto));
    }

    private void checkReferenceDatasetParamInsideDataset(
            DatasetToPivot datasetToPivot, ItemXrayDto itemXrayDto) {
        List<DatasetToPivot.ParamValue> paramValues = datasetToPivot.getParamValues();
        if (Objects.nonNull(paramValues)) {
            paramValues.removeIf(
                    paramValue -> {
                        boolean isNull = Objects.isNull(paramValue.getId());
                        if (isNull) {
                            squashRules.markWithWarning(
                                    itemXrayDto,
                                    String.format(
                                            "The dataset parameter %s is not assigned to the dataset %s because its id is null",
                                            paramValue.getName(), datasetToPivot.getName()));
                        }
                        return isNull;
                    });
        }
    }

    private void handleTestCaseDataset(
            TestCaseManualToPivot testCaseManualToPivot, ItemXrayDto itemXrayDto) {
        // Add dataset - DatasetParm to testCaseManualToPivot
        List<CustomFieldXrayDto> datasets = itemXrayDto.getDataset();
        datasets.stream()
                .collect(Collectors.groupingBy(CustomFieldXrayDto::getDatasetRow))
                .forEach((row, datasetXray) -> createDatasets(testCaseManualToPivot, datasetXray));
        // Add missing dataset - DatasetParm from Call Test case to testCaseManualToPivot
        if (itemXrayDto.isCalledParameter()) {
            correctionAndSaveDatasetWithCalledTc(testCaseManualToPivot);
        }
        // Update Dataset Param name
        checkingDataset(testCaseManualToPivot, itemXrayDto);
    }

    private void createDatasets(
            TestCaseManualToPivot testCaseManualToPivot, List<CustomFieldXrayDto> dataset) {
        DatasetToPivot datasetToPivot = new DatasetToPivot();
        datasetToPivot.setPivotId(
                testCaseManualToPivot.getPivotId(), testCaseManualToPivot.getDatasetId().incrementAndGet());
        datasetToPivot.setName(String.format("Dataset_%s", testCaseManualToPivot.getDatasetId().get()));
        dataset.forEach(
                datasetParam ->
                        createDatasetParamAndValue(testCaseManualToPivot, datasetToPivot, datasetParam));
        testCaseManualToPivot.getDatasets().add(datasetToPivot);
    }

    private void createDatasetParamAndValue(
            TestCaseManualToPivot testCaseManualToPivot,
            DatasetToPivot datasetToPivot,
            CustomFieldXrayDto datasetParameter) {
        if (!testCaseManualToPivot.isDatasetParamExist(datasetParameter.getDatasetName())) {
            DatasetParamToPivot datasetParamToPivot = new DatasetParamToPivot();
            datasetParamToPivot.setPivotId(
                    testCaseManualToPivot.getPivotId(),
                    testCaseManualToPivot.getDatasetParamId().incrementAndGet());
            datasetParamToPivot.setName(datasetParameter.getDatasetName());
            testCaseManualToPivot.getDatasetParams().add(datasetParamToPivot);
        }
        DatasetToPivot.ParamValue paramValue = new DatasetToPivot.ParamValue();
        String idDatasetParam =
                testCaseManualToPivot.getIdDatasetParam(datasetParameter.getDatasetName());
        if (idDatasetParam != null) {
            paramValue.setId(testCaseManualToPivot.getIdDatasetParam(datasetParameter.getDatasetName()));
            paramValue.setName(datasetParameter.getDatasetName());
            paramValue.setValue(datasetParameter.getDatasetValue());
        }
        datasetToPivot.getParamValues().add(paramValue);
    }

    private void handleTestCaseStepGeneric(
            TestCaseManualToPivot testCaseManualToPivot, List<CustomFieldXrayDto> genericDefinitions) {
        AtomicInteger idGenerator = new AtomicInteger(0);
        ActionStepToPivot actionStepToPivot = new ActionStepToPivot();
        actionStepToPivot.setPivotId(testCaseManualToPivot.getPivotId(), idGenerator.incrementAndGet());
        String action =
                genericDefinitions.stream()
                        .map(CustomFieldXrayDto::getValue)
                        .collect(Collectors.joining(System.lineSeparator()));
        actionStepToPivot.setAction(action);
        testCaseManualToPivot.getActionSteps().add(actionStepToPivot);
    }

    private void handleTestCaseStep(
            ItemXrayDto itemXrayDto, TestCaseManualToPivot testCaseManualToPivot) {
        itemXrayDto.getStep().stream()
                .filter(
                        customFieldXray ->
                                Objects.nonNull(customFieldXray.getStepAction())
                                        || Objects.nonNull(customFieldXray.getStepData())
                                        || Objects.nonNull(customFieldXray.getStepExpectedResult()))
                .sorted(Comparator.comparingInt(CustomFieldXrayDto::getStepIndex))
                .forEach(
                        customFieldXray ->
                                createActionStepToPivot(
                                        itemXrayDto,
                                        testCaseManualToPivot,
                                        customFieldXray,
                                        testCaseManualToPivot.getActionStepId()));
    }

    private void createActionStepToPivot(
            ItemXrayDto itemXrayDto,
            TestCaseManualToPivot testCaseManualToPivot,
            CustomFieldXrayDto customFieldXray,
            AtomicInteger idGenerator) {
        ActionStepToPivot actionStepToPivot = new ActionStepToPivot();
        actionStepToPivot.setPivotId(testCaseManualToPivot.getPivotId(), idGenerator.incrementAndGet());
        String action = customFieldXray.getStepAction();
        if (StringUtils.isNotEmpty(customFieldXray.getStepData())) {
            Document actionDocument = normalizeToHTMLDocument(customFieldXray.getStepAction());
            actionDocument.body().appendElement("p").text(customFieldXray.getStepData());
            action =
                    actionDocument.body().children().stream()
                            .map(Node::toString)
                            .collect(Collectors.joining());
        }
        action = checkStep(action, "Action and Data", customFieldXray.getStepIndex(), itemXrayDto);
        String expectedResult =
                checkStep(
                        customFieldXray.getStepExpectedResult(),
                        "Expected Result",
                        customFieldXray.getStepIndex(),
                        itemXrayDto);
        actionStepToPivot.setAction(action);
        actionStepToPivot.setExpectedResult(expectedResult);
        testCaseManualToPivot.getActionSteps().add(actionStepToPivot);
    }

    private String checkStep(String step, String typeStep, int index, ItemXrayDto itemXrayDto) {
        if (Objects.isNull(step)) {
            return null;
        }
        Matcher datasetParamMatcher =
                Pattern.compile("\\{test-param}(.+?)\\{test-param}").matcher(step);
        StringBuilder stepBuilder = new StringBuilder();
        while (datasetParamMatcher.find()) {
            String datasetParam = squashRules.checkDatasetNameInsideStep(datasetParamMatcher.group(1));
            datasetParamMatcher.appendReplacement(
                    stepBuilder, String.format("\\$\\{%s\\}", datasetParam));
        }
        datasetParamMatcher.appendTail(stepBuilder);
        return squashRules.checkSanitizer(
                itemXrayDto, stepBuilder.toString(), String.format("%s (index %s)", typeStep, index));
    }

    private CommonTestCase handleCucumberTestCase(ItemXrayDto itemXrayDto) {
        TestCaseGherkinToPivot testCaseGherkinToPivot = new TestCaseGherkinToPivot();
        testCaseGherkinToPivot.setTestCaseKind(TestCaseKind.GHERKIN);
        createCommonPartTestCase(testCaseGherkinToPivot, itemXrayDto);
        GherkinScriptedStep gherkinScriptedStep = new GherkinScriptedStep();
        gherkinScriptedStep.setFeatureName(itemXrayDto.getSummary());
        gherkinScriptedStep.setScenarioName(itemXrayDto.getSummary());
        gherkinScriptedStep.setDatasetTable(itemXrayDto.getDatasetGherkin());
        handlePrerequisite(
                itemXrayDto,
                preconditions -> generateBackgroundInstructions(gherkinScriptedStep, preconditions));
        try {
            String gherkinScript =
                    gherkinScriptedStep.generateGherkinScript(
                            precedingGherkinLanguage,
                            itemXrayDto.getFilterCufs(XrayField.CustomFieldKey.TEST_STEP_CUCUMBER));
            testCaseGherkinToPivot.setScript(gherkinScript);
        } catch (ParserException parserException) {
            LOGGER.error(
                    "Error on parsing Gherkin script during the generation pivot format:", parserException);
            squashRules.markWithErrorAndThrow(itemXrayDto, parserException.getMessage());
        }
        if (Objects.nonNull(gherkinScriptedStep.getGherkinLanguage())) {
            precedingGherkinLanguage = gherkinScriptedStep.getGherkinLanguage();
        }
        return testCaseGherkinToPivot;
    }

    private void generateBackgroundInstructions(
            GherkinScriptedStep gherkinScriptedStep, Map<String, String> preconditions) {
        List<String> backgroundInstructions = new ArrayList<>();
        preconditions.forEach(
                (key, value) -> {
                    backgroundInstructions.add(String.format("#@%s", key));
                    gherkinScriptedStep.addFormattedBackground(value, backgroundInstructions);
                });
        gherkinScriptedStep.setBackgroundInstructions(backgroundInstructions);
    }

    // Correction -> If dataset doesn't exist inside the Test, we create it
    // Save -> save pivot for called test cases
    private void correctionAndSaveDatasetWithCalledTc(TestCaseManualToPivot testCaseManualToPivot) {
        xrayTablesDao.selectCalledTestCasesToAssignDataset(
                testCaseManualToPivot.getXrayKey(),
                (rawParameters, id) -> {
                    String datasetPivotId =
                            getDatasetPivotIdForCalledTc(rawParameters, testCaseManualToPivot);
                    xrayTablesDao.updateCustomFieldPivotId(id, datasetPivotId);
                });
    }

    private String getDatasetPivotIdForCalledTc(
            String rawParameters, TestCaseManualToPivot testCaseManualToPivot) {
        Map<String, String> calledTestCaseParameters = convertStringToMap(rawParameters);
        correctionDatasetParamIfMissing(testCaseManualToPivot, calledTestCaseParameters);
        String datasetPivotIdTestCase =
                testCaseManualToPivot.getDatasetPivotForCalledTC(calledTestCaseParameters);
        if (Objects.isNull(datasetPivotIdTestCase)) {
            // Create new dataset
            return correctionDataset(testCaseManualToPivot, calledTestCaseParameters);
        } else {
            return datasetPivotIdTestCase;
        }
    }

    private void correctionDatasetParamIfMissing(
            TestCaseManualToPivot testCaseManualToPivot, Map<String, String> calledTestCaseParameters) {
        List<DatasetParamToPivot> datasetParams =
                new ArrayList<>(testCaseManualToPivot.getDatasetParams());
        List<String> datasetParamNames =
                datasetParams.stream().map(DatasetParamToPivot::getName).toList();
        calledTestCaseParameters.keySet().stream()
                .filter(paramName -> !datasetParamNames.contains(paramName))
                .forEach(
                        missingParam -> {
                            DatasetParamToPivot datasetParamToPivot = new DatasetParamToPivot();
                            datasetParamToPivot.setName(missingParam);
                            datasetParamToPivot.setPivotId(
                                    testCaseManualToPivot.getPivotId(),
                                    testCaseManualToPivot.getDatasetParamId().incrementAndGet());
                            datasetParams.add(datasetParamToPivot);
                        });
        testCaseManualToPivot.setDatasetParams(datasetParams);
    }

    private String correctionDataset(
            TestCaseManualToPivot testCaseManualToPivot, Map<String, String> calledTestCaseParameters) {
        DatasetToPivot datasetToPivot = new DatasetToPivot();
        datasetToPivot.setPivotId(
                testCaseManualToPivot.getPivotId(), testCaseManualToPivot.getDatasetId().incrementAndGet());
        datasetToPivot.setName(
                String.format("Dataset_Called_%s", testCaseManualToPivot.getDatasetId().get()));
        calledTestCaseParameters.forEach(
                (paramName, paramValue) -> {
                    DatasetToPivot.ParamValue datasetParamValue = new DatasetToPivot.ParamValue();
                    datasetParamValue.setId(testCaseManualToPivot.getIdDatasetParam(paramName));
                    datasetParamValue.setValue(paramValue);
                    datasetToPivot.getParamValues().add(datasetParamValue);
                });
        testCaseManualToPivot.getDatasets().add(datasetToPivot);
        return datasetToPivot.getPivotId();
    }

    private void handleCalledTestCases(
            JsonGenerator jsonGenerator, PrintWriter logWriter, XrayImportModel xrayImportModel)
            throws IOException {
        jsonGenerator.writeFieldName(JsonImportField.CALLED_TEST_CASES);
        jsonGenerator.writeStartArray();
        XrayImportModel.EntityCount entityCount =
                new XrayImportModel.EntityCount(XrayImportModel.EntityType.CALLED_TEST_CASE);
        xrayTablesDao.selectCalledTestCases(
                consumerThrowingRecordAndId(
                        (calledTestCaseDtos, id) ->
                                handleMultipleCalledInsideTestCase(
                                        jsonGenerator, logWriter, calledTestCaseDtos, id, entityCount)));
        xrayImportModel.getEntityCounts().add(entityCount);
        jsonGenerator.writeEndArray();
    }

    private void handleMultipleCalledInsideTestCase(
            JsonGenerator jsonGenerator,
            PrintWriter logWriter,
            List<CalledTestXrayDto> calledTestXrayDtos,
            AtomicInteger id,
            XrayImportModel.EntityCount entityCount) {
        AtomicInteger indexCorrector =
                new AtomicInteger(1); // Xray start index to 1 -- Pivot start index to 0
        calledTestXrayDtos.forEach(
                consumerThrowingIOException(
                        calledTestXrayDto ->
                                createCalledTestCase(
                                        jsonGenerator, logWriter, calledTestXrayDto, id, entityCount, indexCorrector)));
    }

    private void createCalledTestCase(
            JsonGenerator jsonGenerator,
            PrintWriter logWriter,
            CalledTestXrayDto calledTestXrayDto,
            AtomicInteger id,
            XrayImportModel.EntityCount entityCount,
            AtomicInteger indexCorrector)
            throws IOException {
        try {
            CalledTestCasesToPivot calledTestCasesToPivot =
                    generateCalledTestCase(calledTestXrayDto, id, indexCorrector);
            jsonGenerator.writeObject(calledTestCasesToPivot);
            squashRules.markWithSuccessIfStatusIsNull(calledTestXrayDto);
        } catch (ImportXrayException importXrayException) {
            LOGGER.error("Error during the generation pivot format: ", importXrayException);
            indexCorrector.set(indexCorrector.get() + 1);
        }
        entityCount.countEntity(calledTestXrayDto);
        writeLog(logWriter, calledTestXrayDto);
    }

    private CalledTestCasesToPivot generateCalledTestCase(
            CalledTestXrayDto calledTestXrayDto, AtomicInteger id, AtomicInteger indexCorrector) {
        squashRules.checkMandatoryField(calledTestXrayDto, calledTestXrayDto.getPivotId(), "caller_id");
        squashRules.checkMandatoryField(
                calledTestXrayDto, calledTestXrayDto.getStepIndex(), "step_index");
        CalledTestCasesToPivot calledTestCasesToPivot;
        if (Objects.isNull(calledTestXrayDto.getCalledId())) {
            String message =
                    String.format(
                            "The issue %s is called in %s but it doesn't exist in the import.",
                            calledTestXrayDto.getKey(), calledTestXrayDto.getStepCalledTestKey());
            squashRules.markWithErrorAndThrow(calledTestXrayDto, message);
            return null;
        } else {
            calledTestCasesToPivot = new CalledTestCasesToPivot();
            calledTestCasesToPivot.setPivotId(id.incrementAndGet());
            calledTestCasesToPivot.setCallerId(calledTestXrayDto.getPivotId());
            calledTestCasesToPivot.setCalledId(calledTestXrayDto.getCalledId());
            calledTestCasesToPivot.setIndex(calledTestXrayDto.getStepIndex() - indexCorrector.get());
            handleCalledTestCasesParameters(calledTestXrayDto, calledTestCasesToPivot);
        }
        return calledTestCasesToPivot;
    }

    private void handleCalledTestCasesParameters(
            CalledTestXrayDto calledTestXrayDto, CalledTestCasesToPivot calledTestCasesToPivot) {
        String datasetPivotId = calledTestXrayDto.getCufPivotId();
        if (Objects.nonNull(calledTestXrayDto.getStepCalledTestParameters())) {
            if (Objects.nonNull(datasetPivotId)) {
                calledTestCasesToPivot.setParameterAssignationMode(ParameterAssignationMode.CALLED_DATASET);
                calledTestCasesToPivot.setDatasetId(datasetPivotId);
            } else {
                calledTestCasesToPivot.setParameterAssignationMode(ParameterAssignationMode.NOTHING);
                squashRules.markWithWarning(
                        calledTestXrayDto, "The dataset is not assigned to the called test case");
            }
        } else {
            calledTestCasesToPivot.setParameterAssignationMode(ParameterAssignationMode.NOTHING);
        }
    }
}
