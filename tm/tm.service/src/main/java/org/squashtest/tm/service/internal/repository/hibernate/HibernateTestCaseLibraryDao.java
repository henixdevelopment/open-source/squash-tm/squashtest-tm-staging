/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.service.internal.repository.TestCaseLibraryDao;

@Repository
public class HibernateTestCaseLibraryDao
        extends HibernateLibraryDao<TestCaseLibrary, TestCaseLibraryNode>
        implements TestCaseLibraryDao {

    @Override
    public List<String> findContentNamesByLibraryId(Long libraryId) {
        return entityManager
                .createQuery(
                        "select content.name "
                                + "from TestCaseLibrary library "
                                + "join library.rootContent content "
                                + "where library.id = :id",
                        String.class)
                .setParameter(ID, libraryId)
                .getResultList();
    }

    @Override
    public TestCaseLibrary loadForNodeAddition(Long libraryId) {
        return entityManager
                .createQuery( """
                    select tcl from TestCaseLibrary tcl
                    join fetch tcl.project project
                    join fetch project.testCaseNatures nature
                    join fetch nature.items
                    join fetch project.testCaseTypes type
                    join fetch type.items
                    left join fetch tcl.rootContent
                    where tcl.id = :id""", TestCaseLibrary.class)
                .setParameter(ID, libraryId)
                .getSingleResult();
    }
}
