/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport;

import static org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration.CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS;
import static org.squashtest.tm.service.grid.ColumnIds.COLUMN_IDS_LINKED_TO_MILESTONES;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.feature.FeatureManager.Feature;
import org.squashtest.tm.service.grid.ColumnIds;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;
import org.squashtest.tm.service.internal.batchexport.models.RequirementExportModel;
import org.squashtest.tm.service.internal.batchexport.models.RequirementModel;
import org.squashtest.tm.service.internal.batchexport.models.RequirementModelFromJooq;
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet;
import org.squashtest.tm.service.internal.dto.NumericCufHelper;
import org.squashtest.tm.service.plugin.PluginFinderService;

/**
 * @author bflessel
 */
@Component
@Scope("prototype")
public class RequirementSearchExcelExporter {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(RequirementSearchExcelExporter.class);
    private static final String REQUIREMENT_SHEET = TemplateWorksheet.REQUIREMENT_SHEET.sheetName;
    private static final String CRITICALITY = "requirement.criticality.";
    private static final String STATUS = "requirement.status.";
    private static final String LABEL_NO = "label.No";
    private static final String LABEL_YES = "label.Yes";

    private List<ColumnIds> customColumnOrder =
            new ArrayList<>(
                    Arrays.asList(
                            ColumnIds.PROJECT_NAME,
                            ColumnIds.REQUIREMENT_ID,
                            ColumnIds.ID,
                            ColumnIds.REFERENCE,
                            ColumnIds.NAME,
                            ColumnIds.STATUS,
                            ColumnIds.REQUIREMENT_CRITICALITY,
                            ColumnIds.REQUIREMENT_CATEGORY,
                            ColumnIds.CREATED_BY,
                            ColumnIds.LAST_MODIFIED_BY,
                            ColumnIds.VERSION_COUNT,
                            ColumnIds.VERSION_NUMBER));
    private List<ColumnIds> optionalColumns =
            new ArrayList<>(
                    Arrays.asList(
                            ColumnIds.ATTACHMENTS,
                            ColumnIds.COVERAGE_OF_ASSOCIATED_TEST_CASE,
                            ColumnIds.MILESTONES));
    List<ColumnIds> premiumColumnList =
            new ArrayList<>(
                    Arrays.asList(
                            ColumnIds.PROJECT_NAME,
                            ColumnIds.REQUIREMENT_ID,
                            ColumnIds.ID,
                            ColumnIds.REFERENCE,
                            ColumnIds.NAME,
                            ColumnIds.STATUS,
                            ColumnIds.REQUIREMENT_CRITICALITY,
                            ColumnIds.REQUIREMENT_CATEGORY,
                            ColumnIds.REQUIREMENT_NATURE,
                            ColumnIds.DESCRIPTION,
                            ColumnIds.REQUIREMENT_VERSION_HAS_CHILDREN,
                            ColumnIds.REQUIREMENT_VERSION_HAS_PARENT,
                            ColumnIds.REQUIREMENT_VERSION_HAS_LINK_TYPE,
                            ColumnIds.REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT,
                            ColumnIds.LINKED_STANDARD_REQUIREMENT,
                            ColumnIds.MILESTONES_LABELS,
                            ColumnIds.MILESTONES_STATUS,
                            ColumnIds.MILESTONES_END_DATE,
                            ColumnIds.CREATED_ON,
                            ColumnIds.CREATED_BY,
                            ColumnIds.LAST_MODIFIED_ON,
                            ColumnIds.LAST_MODIFIED_BY,
                            ColumnIds.VERSION_COUNT,
                            ColumnIds.VERSION_NUMBER,
                            ColumnIds.ATTACHMENTS,
                            ColumnIds.COVERAGE_OF_ASSOCIATED_TEST_CASE,
                            ColumnIds.MILESTONES));

    private Workbook workbook;

    protected boolean milestonesEnabled;

    @Inject private MessageSource messageSource;

    private String errorCellTooLargeMessage;

    private final PluginFinderService pluginFinderService;

    private final GridConfigurationService gridConfigurationService;
    private Map<Long, Integer> cufColumnsById = new HashMap<>();

    public RequirementSearchExcelExporter(
            FeatureManager featureManager,
            MessageSource messageSource,
            GridConfigurationService gridConfigurationService,
            PluginFinderService pluginFinderService) {
        super();
        this.milestonesEnabled = featureManager.isEnabled(Feature.MILESTONE);
        this.gridConfigurationService = gridConfigurationService;
        this.pluginFinderService = pluginFinderService;
        getMessageSource(messageSource);
        createWorkbook();
    }

    void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
        errorCellTooLargeMessage =
                this.messageSource.getMessage(
                        "test-case.export.errors.celltoolarge", null, LocaleContextHolder.getLocale());
    }

    public void createHeaders(boolean simplifiedColumnDisplay) {
        createSheetHeaders(REQUIREMENT_SHEET, simplifiedColumnDisplay);
    }

    public void appendToWorkbook(
            RequirementExportModel model, boolean keepRteFormat, boolean simplifiedColumnDisplay) {
        if (!keepRteFormat) {
            removeRteFormat(model);
        }

        sort(model);
        appendRequirementModel(model, simplifiedColumnDisplay);
    }

    private void sort(RequirementExportModel model) {
        Collections.sort(model.getRequirementsModels(), RequirementModel.COMPARATOR);
    }

    private void appendRequirementModel(
            RequirementExportModel model, boolean simplifiedColumnDisplay) {
        List<RequirementModelFromJooq> models = model.getRequirementModelFromJooqs();
        Sheet reqSheet = workbook.getSheet(REQUIREMENT_SHEET);
        int rowIndex = reqSheet.getLastRowNum() + 1;
        List<String> activeColumns =
                gridConfigurationService.findActiveColumnIdsForUser("requirement-search");

        List<CustomField> allCufs =
                models.stream()
                        .flatMap(tcm -> tcm.getCufs().stream())
                        .sorted(Comparator.comparing((CustomField c) -> c.getLabel().toLowerCase()))
                        .toList();

        for (RequirementModelFromJooq reqModel : models) {

            Row row = reqSheet.createRow(rowIndex);

            try {

                insertValues(row, reqModel, activeColumns, simplifiedColumnDisplay);
                appendCustomFields(row, reqModel.getCufs(), activeColumns, allCufs);

            } catch (IllegalArgumentException wtf) {
                reqSheet.removeRow(row);
                row = reqSheet.createRow(rowIndex);
                row.createCell(0).setCellValue(errorCellTooLargeMessage);
            }
            rowIndex++;
        }
    }

    private void createSheetHeaders(String sheetName, boolean simplifiedColumnDisplay) {
        Sheet dsSheet = workbook.getSheet(sheetName);
        Row h = dsSheet.createRow(0);
        List<String> activeColumns =
                gridConfigurationService.findActiveColumnIdsForUser("requirement-search");
        int cIdx = 0;
        List<String> columnsToProcess = columnList(activeColumns, simplifiedColumnDisplay);
        Map<String, String> customLabels = createColumnIdsMap();

        for (String columnName : columnsToProcess) {
            insertActiveColumnHeader(h, cIdx, columnName, customLabels);
            cIdx++;
        }
    }

    private List<ColumnIds> genericColumnList(
            List<String> activeColumns,
            List<ColumnIds> columnsToProcess,
            boolean simplifiedColumnDisplayForTest) {

        if (!pluginFinderService.isPremiumPluginInstalled()) {
            if (!simplifiedColumnDisplayForTest) {
                columnsToProcess.addAll(optionalColumns);
            }
        } else if (!activeColumns.isEmpty()) {
            columnsToProcess = premiumColumnList;
        }
        if (!milestonesEnabled) {
            columnsToProcess.removeAll(COLUMN_IDS_LINKED_TO_MILESTONES);
        }
        return columnsToProcess;
    }

    private List<String> columnList(
            List<String> activeColumns, boolean simplifiedColumnDisplayForTest) {

        List<ColumnIds> columnsToProcess = new ArrayList<>(customColumnOrder);
        List<ColumnIds> initialList =
                genericColumnList(activeColumns, columnsToProcess, simplifiedColumnDisplayForTest);

        List<String> columnIdStrings =
                initialList.stream().map(ColumnIds::getColumnId).collect(Collectors.toList());
        if (pluginFinderService.isPremiumPluginInstalled() && !activeColumns.isEmpty()) {
            columnIdStrings.retainAll(activeColumns);

            activeColumns.forEach(
                    column -> {
                        if (column.startsWith(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS)) {
                            columnIdStrings.add(column);
                        }
                    });
        }
        return columnIdStrings;
    }

    private Map<String, String> createColumnIdsMap() {
        Map<String, String> customLabels = new HashMap<>();
        List<ColumnIds> columnsToProcess = new ArrayList<>(premiumColumnList);
        if (!milestonesEnabled) {
            columnsToProcess.removeAll(COLUMN_IDS_LINKED_TO_MILESTONES);
        }
        for (ColumnIds columnName : columnsToProcess) {
            putCustomLabel(customLabels, columnName);
        }
        return customLabels;
    }

    private void putCustomLabel(Map<String, String> customLabels, ColumnIds columnId) {
        customLabels.put(columnId.getColumnId(), columnId.getLabel());
    }

    private void insertActiveColumnHeader(
            Row headerRow, int cIdx, String activeColumn, Map<String, String> customLabels) {

        if (customLabels.containsKey(activeColumn)) {
            headerRow.createCell(cIdx).setCellValue(getMessage(customLabels.get(activeColumn)));
        } else if (activeColumn.startsWith(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS)) {
            gridConfigurationService.registerCuf(headerRow, cIdx, activeColumn, cufColumnsById);
        }
    }

    private void insertValues(
            Row row,
            RequirementModelFromJooq reqModel,
            List<String> activeColumns,
            Boolean simplifiedColumnDisplay) {

        Map<String, String> fieldValues = new HashMap<>();
        fieldValues.put(
                "Criticality", handleMessages(CRITICALITY + reqModel.getCriticality().toString()));
        fieldValues.put("Category", handleMessages(reqModel.getCategory()));
        fieldValues.put("Status", handleMessages(STATUS + reqModel.getStatus().toString()));
        fieldValues.put(
                "Description", handleMessages("".equals(reqModel.getDescription()) ? LABEL_NO : LABEL_YES));
        fieldValues.put(
                "IsHighLevel",
                handleMessages(
                        reqModel.isHighLevel() ? "requirement.high-level.label" : "test-case.format.standard"));
        fieldValues.put(
                "HasParent",
                handleMessages(Boolean.TRUE.equals(reqModel.getHasParent()) ? LABEL_YES : LABEL_NO));
        fieldValues.put(
                "HasLinkType",
                handleMessages(Boolean.TRUE.equals(reqModel.getHasLinkType()) ? LABEL_YES : LABEL_NO));
        fieldValues.put(
                "RequirementBoundToHighLevelRequirement",
                handleMessages(
                        Boolean.TRUE.equals(reqModel.getRequirementBoundToHighLevelRequirement())
                                ? LABEL_YES
                                : LABEL_NO));

        Map<String, BiConsumer<Row, Integer>> columnValueHandlers =
                createColumnValueHandlers(reqModel, fieldValues);
        int colIndex = 0;
        List<String> columnsToProcess = columnList(activeColumns, simplifiedColumnDisplay);
        for (String columnName : columnsToProcess) {
            if (columnValueHandlers.containsKey(columnName)) {
                columnValueHandlers.get(columnName).accept(row, colIndex);
            }
            colIndex++;
        }
    }

    private Map<String, BiConsumer<Row, Integer>> createColumnValueHandlers(
            RequirementModelFromJooq reqModel, Map<String, String> fieldValues) {
        Map<String, BiConsumer<Row, Integer>> handlers = new HashMap<>();
        putCustomHandlers(handlers, ColumnIds.PROJECT_NAME, reqModel.getProjectName());
        putCustomHandlers(handlers, ColumnIds.REQUIREMENT_ID, reqModel.getRequirementId());
        putCustomHandlers(handlers, ColumnIds.ID, reqModel.getId());
        putCustomHandlers(handlers, ColumnIds.REFERENCE, reqModel.getReference());
        putCustomHandlers(handlers, ColumnIds.NAME, reqModel.getName());
        putCustomHandlers(handlers, ColumnIds.STATUS, fieldValues.get("Status"));
        putCustomHandlers(handlers, ColumnIds.REQUIREMENT_CRITICALITY, fieldValues.get("Criticality"));
        putCustomHandlers(handlers, ColumnIds.REQUIREMENT_CATEGORY, fieldValues.get("Category"));
        putCustomHandlers(handlers, ColumnIds.CREATED_BY, reqModel.getCreatedBy());
        putCustomHandlers(handlers, ColumnIds.LAST_MODIFIED_BY, reqModel.getLastModifiedBy());
        putCustomHandlers(handlers, ColumnIds.VERSION_COUNT, reqModel.getRequirementVersionNumber());
        putCustomHandlers(handlers, ColumnIds.VERSION_NUMBER, reqModel.getVersionsCount());
        putCustomHandlers(
                handlers,
                ColumnIds.COVERAGE_OF_ASSOCIATED_TEST_CASE,
                (reqModel.getCoverages() != null ? reqModel.getCoverages() : 0));
        putCustomHandlers(handlers, ColumnIds.ATTACHMENTS, reqModel.getAttachments());
        putCustomHandlers(
                handlers,
                ColumnIds.CREATED_ON,
                gridConfigurationService.reformatSimpleDateForExport(reqModel.getCreatedOn()));
        putCustomHandlers(
                handlers,
                ColumnIds.LAST_MODIFIED_ON,
                gridConfigurationService.reformatSimpleDateForExport(reqModel.getLastModifiedOn()));
        putCustomHandlers(handlers, ColumnIds.DESCRIPTION, fieldValues.get("Description"));
        putCustomHandlers(handlers, ColumnIds.REQUIREMENT_NATURE, fieldValues.get("IsHighLevel"));

        putCustomHandlers(
                handlers, ColumnIds.REQUIREMENT_VERSION_HAS_PARENT, fieldValues.get("HasParent"));
        putCustomHandlers(
                handlers, ColumnIds.REQUIREMENT_VERSION_HAS_CHILDREN, reqModel.getChildOfRequirement());
        putCustomHandlers(
                handlers, ColumnIds.REQUIREMENT_VERSION_HAS_LINK_TYPE, fieldValues.get("HasLinkType"));
        putCustomHandlers(
                handlers,
                ColumnIds.REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT,
                fieldValues.get("RequirementBoundToHighLevelRequirement"));
        putCustomHandlers(
                handlers, ColumnIds.LINKED_STANDARD_REQUIREMENT, reqModel.getLinkedStandardRequirement());

        if (milestonesEnabled) {
            putCustomHandlers(
                    handlers,
                    ColumnIds.MILESTONES,
                    reqModel.getMilestones() != null ? reqModel.getMilestones() : 0);
            putCustomHandlers(handlers, ColumnIds.MILESTONES_LABELS, reqModel.getMilestoneLabels());
            putCustomHandlers(
                    handlers,
                    ColumnIds.MILESTONES_STATUS,
                    reformatStatusForExport(reqModel.getMilestoneStatus()));
            putCustomHandlers(
                    handlers,
                    ColumnIds.MILESTONES_END_DATE,
                    gridConfigurationService.reformatMultipleDateForExport(reqModel.getMilestoneEndDate()));
        }
        return handlers;
    }

    private void putCustomHandlers(
            Map<String, BiConsumer<Row, Integer>> handlers, ColumnIds columnId, Object value) {
        handlers.put(
                columnId.getColumnId(),
                (row, colIndex) -> {
                    Cell cell = row.createCell(colIndex);
                    if (value instanceof String) {
                        cell.setCellValue((String) value);
                    } else if (value instanceof Integer) {
                        cell.setCellValue((Integer) value);
                    } else if (value instanceof Long) {
                        cell.setCellValue((Long) value);
                    } else if (value instanceof Date) {
                        cell.setCellValue((Date) value);
                    }
                });
    }

    private String reformatStatusForExport(String concatenatedStatus) {
        List<String> formattedStatus = new ArrayList<>();
        if (StringUtils.isNotBlank(concatenatedStatus)) {
            String[] statuses = concatenatedStatus.split("\\|");
            formattedStatus =
                    Arrays.stream(statuses).map(st -> handleMessages("milestone.status." + st)).toList();
        }
        return String.join(", ", formattedStatus);
    }

    private void removeRteFormat(RequirementExportModel model) {
        removeRteFormatFromRequirement(model.getRequirementsModels());
    }

    private void removeRteFormatFromRequirement(List<RequirementModel> requirementsModels) {
        for (RequirementModel requirementModel : requirementsModels) {
            requirementModel.setDescription(removeHtml(requirementModel.getDescription()));
            for (CustomField cf : requirementModel.getCufs()) {
                cf.setValue(removeHtml(cf.getValue()));
            }
        }
    }

    private String removeHtml(String html) {
        if (StringUtils.isBlank(html)) {
            return "";
        }
        return html.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", "");
    }

    public File print() {
        try {
            File temp = File.createTempFile("req_export_", "xls");
            temp.deleteOnExit();

            FileOutputStream fos = new FileOutputStream(temp);
            workbook.write(fos);
            fos.close();

            return temp;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    // for now we care only of Excel 2003
    private void createWorkbook() {
        Workbook wb = new HSSFWorkbook();
        wb.createSheet(REQUIREMENT_SHEET);

        this.workbook = wb;
    }

    // ***************** other things ******************************

    private String getMessage(String key) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(key, null, locale);
    }

    public void getMessageSource(MessageSource source) {
        this.messageSource = source;
    }

    private String handleMessages(String key) {
        try {
            return getMessage(key);
        } catch (NoSuchMessageException e) {
            LOGGER.debug("No corresponding message for key '{}'", key, e);
            return key;
        }
    }

    private void appendCustomFields(
            Row row,
            List<CustomField> requirementCufs,
            List<String> activeColumns,
            List<CustomField> allCufs) {
        for (CustomField cuf : allCufs) {
            Long id = cuf.getCufId();
            String columnName = GridColumnDisplayConfiguration.getCufColumnNameFromCufId(id);
            if (activeColumns.contains(columnName)) {
                Integer index = cufColumnsById.get(id);
                CustomField requirementCufValue =
                        gridConfigurationService.findCufById(requirementCufs, id, index);
                if (requirementCufValue != null) {
                    setCellValue(requirementCufValue, index, row);
                }
            }
        }
    }

    private void setCellValue(CustomField cuf, Integer idx, Row r) {
        Cell c = r.createCell(idx);
        String value = gridConfigurationService.nullSafeValue(cuf);
        switch (cuf.getType()) {
            case NUMERIC:
                value = NumericCufHelper.formatOutputNumericCufValue(value);
                break;
            case CHECKBOX:
                value = handleMessages("label." + StringUtils.capitalize(value));
                break;
            case TAG:
                value = gridConfigurationService.reformatLabelForExport(value);
                break;
            default:
                break;
        }
        c.setCellValue(value);
    }
}
