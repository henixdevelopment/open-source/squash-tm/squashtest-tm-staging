/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.instruction.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Provider;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.importer.ImportLog;
import org.squashtest.tm.service.importer.ImportStatus;
import org.squashtest.tm.service.importer.LogEntry;
import org.squashtest.tm.service.internal.batchimport.Facility;
import org.squashtest.tm.service.internal.batchimport.LogTrain;
import org.squashtest.tm.service.internal.batchimport.Messages;
import org.squashtest.tm.service.internal.batchimport.ProjectImport;
import org.squashtest.tm.service.internal.batchimport.instruction.Instruction;
import org.squashtest.tm.service.internal.repository.ProjectDao;

public class Importer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Importer.class);

    private final Map<String, ProjectImport> projectImports = new HashMap<>();

    private final List<Instruction<?>> invalidInstructions = new ArrayList<>();

    public void addErrorInstruction(Instruction<?> instruction) {
        invalidInstructions.add(instruction);
    }

    public void addInstructionContainer(
            String project, InstructionContainer<?> instructionContainer) {
        projectImports
                .computeIfAbsent(project, ProjectImport::new)
                .addInstructionContainer(instructionContainer);
    }

    public <T extends Facility> ImportLog performImport(
            Provider<T> facilityProvider, ProjectDao projectDao) {
        ImportLog importLog = new ImportLog();

        for (ProjectImport projectImport : projectImports.values()) {
            LOGGER.info("Data import for the project '{}'", projectImport.getProject());
            Project project = projectDao.findByName(projectImport.getProject());

            if (project == null) {
                handleMissingProject(projectImport, importLog);
            } else {
                Facility facility = facilityProvider.get();
                for (var container : projectImport.getContainers()) {
                    container
                            .executeInstructions(facility, project)
                            .forEach(instruction -> processLog(instruction, importLog));
                }
            }
        }

        invalidInstructions.forEach(instruction -> processLog(instruction, importLog));

        importLog.packLogs();

        return importLog;
    }

    private void handleMissingProject(ProjectImport projectImport, ImportLog importLog) {
        LOGGER.error(
                "Project '{}' does not exist, instructions will not be executed",
                projectImport.getProject());
        for (var containers : projectImport.getContainers()) {
            containers
                    .getAllInstructions()
                    .forEach(
                            instruction -> {
                                createLogForMissingProject(instruction);
                                processLog(instruction, importLog);
                            });
        }
    }

    private void processLog(Instruction<?> instruction, ImportLog importLog) {
        LogTrain logs = instruction.getLogTrain();
        handleLogs(instruction, logs);
        logs.setForAll(instruction.getMode());
        logs.setForAll(instruction.getLine());
        importLog.appendLogTrain(logs);
    }

    private void createLogForMissingProject(Instruction<?> instruction) {
        instruction.addLogEntry(ImportStatus.FAILURE, Messages.ERROR_PROJECT_NOT_EXIST, null);
    }

    private void handleLogs(final Instruction<?> instruction, final LogTrain logs) {
        if (logs.hasNoErrorWhatsoever()) {
            LOGGER.debug(
                    "No errors when executing instruction : (line {}) {} '{}'",
                    instruction.getLine(),
                    instruction.getMode(),
                    instruction.getTarget());
            logs.addEntry(LogEntry.ok().forTarget(instruction.getTarget()).build());
        } else {
            LOGGER.warn(
                    "Errors were encountered for instruction : (line {}) {} '{}', they will be reported to the user.",
                    instruction.getLine(),
                    instruction.getMode(),
                    instruction.getTarget());
        }
    }
}
