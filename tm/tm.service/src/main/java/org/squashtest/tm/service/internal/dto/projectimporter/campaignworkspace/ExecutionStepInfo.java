/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.internal.dto.projectimporter.AttachmentToImport;
import org.squashtest.tm.service.internal.projectimporter.pivotimporter.ProjectPivotImportTaskScheduler;

public class ExecutionStepInfo {
    private String testStepId;
    private ExecutionStatus status;
    private String comment;
    private Map<Long, RawValue> customFields;
    private String lastExecutedBy = ProjectPivotImportTaskScheduler.PIVOT_FORMAT_IMPORT_USER;
    private Date lastExecutedOn = new Date();
    private List<AttachmentToImport> attachments = new ArrayList<>();

    public String getTestStepId() {
        return testStepId;
    }

    public void setTestStepId(String testStepId) {
        this.testStepId = testStepId;
    }

    public ExecutionStatus getStatus() {
        return status;
    }

    public void setStatus(ExecutionStatus status) {
        this.status = status;
    }

    @CleanHtml
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Map<Long, RawValue> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<Long, RawValue> customFields) {
        this.customFields = customFields;
    }

    public String getLastExecutedBy() {
        return lastExecutedBy;
    }

    public void setLastExecutedBy(String lastExecutedBy) {
        this.lastExecutedBy = lastExecutedBy;
    }

    public Date getLastExecutedOn() {
        return lastExecutedOn;
    }

    public void setLastExecutedOn(Date lastExecutedOn) {
        this.lastExecutedOn = lastExecutedOn;
    }

    public List<AttachmentToImport> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentToImport> attachments) {
        this.attachments = attachments;
    }
}
