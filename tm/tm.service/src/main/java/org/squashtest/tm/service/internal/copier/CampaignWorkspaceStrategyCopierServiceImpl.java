/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.copier;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.domain.library.TreeNode;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.copier.CampaignWorkspaceCopierService;
import org.squashtest.tm.service.copier.StrategyCopierService;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.CampaignFolderDao;
import org.squashtest.tm.service.internal.repository.CampaignLibraryDao;
import org.squashtest.tm.service.internal.repository.EntityDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.SprintDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateSprintGroupDao;
import org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

@Service
@Transactional
public class CampaignWorkspaceStrategyCopierServiceImpl implements StrategyCopierService {
    private static final Integer MAX_BATCH_SIZE = HibernateConfig.BATCH_50;

    private final PermissionEvaluationService permissionService;
    private final CampaignWorkspaceCopierService copierService;

    private final EntityManager entityManager;
    private final CampaignLibraryDao campaignLibraryDao;
    private final CampaignFolderDao campaignFolderDao;
    private final CampaignDao campaignDao;
    private final IterationDao iterationDao;
    private final TestSuiteDao testSuiteDao;
    private final SprintDao sprintDao;
    private final HibernateSprintGroupDao sprintGroupDao;

    public CampaignWorkspaceStrategyCopierServiceImpl(
            PermissionEvaluationService permissionService,
            CampaignWorkspaceCopierService copierService,
            EntityManager entityManager,
            CampaignLibraryDao campaignLibraryDao,
            CampaignFolderDao campaignFolderDao,
            CampaignDao campaignDao,
            IterationDao iterationDao,
            TestSuiteDao testSuiteDao,
            SprintDao sprintDao,
            HibernateSprintGroupDao sprintGroupDao) {
        this.permissionService = permissionService;
        this.copierService = copierService;
        this.entityManager = entityManager;
        this.campaignLibraryDao = campaignLibraryDao;
        this.campaignFolderDao = campaignFolderDao;
        this.campaignDao = campaignDao;
        this.iterationDao = iterationDao;
        this.testSuiteDao = testSuiteDao;
        this.sprintDao = sprintDao;
        this.sprintGroupDao = sprintGroupDao;
    }

    @Override
    public NodeType verifyPermissionAndGetNodePaste(ClipboardPayload clipboardPayload) {
        List<NodeType> nodeTypes =
                clipboardPayload.getSelectedNode().stream()
                        .map(NodeReference::getNodeType)
                        .distinct()
                        .toList();
        if (nodeTypes.size() > 1) {
            throw new IllegalArgumentException("Only one type of node can be copied at a time");
        }
        NodeType nodeType = nodeTypes.get(0);
        PermissionsUtils.checkPermission(
                permissionService,
                clipboardPayload.getSelectedNodeIds(),
                Permissions.READ.name(),
                nodeType.toEntityType().getEntityClass().getName());
        return nodeType;
    }

    @Override
    public void copyNodeToCampaign(
            Long destinationId, ClipboardPayload clipboardPayload, NodeType nodePaste) {
        if (!NodeType.ITERATION.equals(nodePaste)) {
            throw new IllegalArgumentException(
                    String.format("Cannot paste %s in a campaign", nodePaste.getTypeName()));
        }
        copyNodeToContainer(Campaign.class, Iteration.class, destinationId, clipboardPayload);
    }

    @Override
    public void copyNodeToIteration(
            Long destinationId, ClipboardPayload clipboardPayload, NodeType nodePaste) {
        if (!NodeType.TEST_SUITE.equals(nodePaste)) {
            throw new IllegalArgumentException(
                    String.format("Cannot paste %s in an iteration", nodePaste.getTypeName()));
        }

        copyNodeToContainer(
                Iteration.class, testSuiteDao::loadNodeForPaste, destinationId, clipboardPayload);
    }

    @Override
    public void copyNodeToSpringGroup(
            Long destinationId, ClipboardPayload clipboardPayload, NodeType nodePaste) {
        switch (nodePaste) {
            case SPRINT ->
                    copyNodeToContainer(SprintGroup.class, Sprint.class, destinationId, clipboardPayload);
            case CAMPAIGN_FOLDER ->
                    copyNodeToContainer(
                            SprintGroup.class, CampaignFolder.class, destinationId, clipboardPayload);
            default ->
                    throw new IllegalArgumentException(
                            String.format("Cannot paste %s in a sprint group", nodePaste.getTypeName()));
        }
    }

    @Override
    public void copyNodeToCampaignFolder(
            Long destinationId, ClipboardPayload clipboardPayload, NodeType nodePaste) {
        switch (nodePaste) {
            case SPRINT ->
                    copyNodeToContainer(CampaignFolder.class, Sprint.class, destinationId, clipboardPayload);
            case SPRINT_GROUP ->
                    copyNodeToContainer(
                            CampaignFolder.class, SprintGroup.class, destinationId, clipboardPayload);
            case CAMPAIGN ->
                    copyNodeToContainer(
                            CampaignFolder.class, Campaign.class, destinationId, clipboardPayload);
            case CAMPAIGN_FOLDER ->
                    copyNodeToContainer(
                            CampaignFolder.class, CampaignFolder.class, destinationId, clipboardPayload);
            default ->
                    throw new IllegalArgumentException(
                            String.format("Cannot paste %s in a campaign folder", nodePaste.getTypeName()));
        }
    }

    @Override
    public void copyNodeToCampaignLibrary(
            Long destinationId, ClipboardPayload clipboardPayload, NodeType nodePaste) {
        switch (nodePaste) {
            case SPRINT ->
                    copyNodeToContainer(CampaignLibrary.class, Sprint.class, destinationId, clipboardPayload);
            case SPRINT_GROUP ->
                    copyNodeToContainer(
                            CampaignLibrary.class, SprintGroup.class, destinationId, clipboardPayload);
            case CAMPAIGN ->
                    copyNodeToContainer(
                            CampaignLibrary.class, Campaign.class, destinationId, clipboardPayload);
            case CAMPAIGN_FOLDER ->
                    copyNodeToContainer(
                            CampaignLibrary.class, CampaignFolder.class, destinationId, clipboardPayload);
            default ->
                    throw new IllegalArgumentException(
                            String.format("Cannot paste %s in a campaign folder", nodePaste.getTypeName()));
        }
    }

    private <CONTAINER, NODE extends TreeNode> void copyNodeToContainer(
            Class<CONTAINER> containerClass,
            Class<NODE> nodeClass,
            Long destinationId,
            ClipboardPayload clipboardPayload) {
        EntityDao<NODE> dao = getDaoFromNodeClass(nodeClass.getSimpleName());
        copyNodeToContainer(containerClass, dao::loadNodeForPaste, destinationId, clipboardPayload);
    }

    @SuppressWarnings("unchecked")
    private <CONTAINER, NODE extends TreeNode> void copyNodeToContainer(
            Class<CONTAINER> containerClass,
            Function<List<Long>, List<NODE>> loadNodeForPaste,
            Long destinationId,
            ClipboardPayload clipboardPayload) {
        HibernateConfig.enableBatch(entityManager, MAX_BATCH_SIZE);
        EntityDao<CONTAINER> daoContainer = getDaoFromNodeClass(containerClass.getSimpleName());

        Map<String, Map<Long, Long>> pairingIdsSourceCopyNode = new HashMap<>();
        CONTAINER container = daoContainer.loadContainerForPaste(destinationId);
        int countIds = 0;
        for (List<Long> ids : Lists.partition(clipboardPayload.getSelectedNodeIds(), MAX_BATCH_SIZE)) {
            if (countIds > 200) {
                // copyNodeToContainer evict only nodes that container is not the parent of to avoid
                // DetachedEntityException
                // Child contains Attachment, CustomFieldValue, etc. So we need to clear the entityManager
                // to avoid memory leak
                entityManager.clear();
                container = daoContainer.loadContainerForPaste(destinationId);
                countIds = 0;
            }
            List<NODE> nodes = loadNodeForPaste.apply(ids);
            copierService.copyNodeToContainer(
                    (NodeContainer<TreeNode>) container, nodes, pairingIdsSourceCopyNode);
            countIds += MAX_BATCH_SIZE;
        }
        entityManager.clear();
        copyAllChildren(pairingIdsSourceCopyNode, clipboardPayload);
    }

    private <ENTITY extends NodeContainer<TreeNode>> void copyAllChildren(
            Map<String, Map<Long, Long>> pairingIdsSourceCopyNode, ClipboardPayload clipboardPayload) {
        if (pairingIdsSourceCopyNode.isEmpty()) {
            return;
        }
        Map<String, Map<Long, Long>> pairingIdsSourceCopyNodeChild = new HashMap<>();
        for (Map.Entry<String, Map<Long, Long>> entry : pairingIdsSourceCopyNode.entrySet()) {
            if (entry.getValue().isEmpty()) {
                continue;
            }
            EntityDao<ENTITY> dao = getDaoFromNodeClass(entry.getKey());
            Lists.partition(new ArrayList<>(entry.getValue().entrySet()), 1000)
                    .forEach(
                            entries -> {
                                Map<Long, Long> pairs =
                                        entries.stream()
                                                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                                copyChildByEntities(dao, pairs, clipboardPayload, pairingIdsSourceCopyNodeChild);
                            });
            entry.getValue().clear();
        }
        pairingIdsSourceCopyNode.clear();
        copyAllChildren(pairingIdsSourceCopyNodeChild, clipboardPayload);
    }

    private <ENTITY extends NodeContainer<TreeNode>> void copyChildByEntities(
            EntityDao<ENTITY> dao,
            Map<Long, Long> pairs,
            ClipboardPayload clipboardPayload,
            Map<String, Map<Long, Long>> pairingIdsSourceCopyNodeChild) {
        ChildEntityDtoResult childEntityDtoResult;
        int offset = 0;
        do {
            childEntityDtoResult =
                    dao.loadChildForPaste(pairs.keySet(), MAX_BATCH_SIZE, offset, clipboardPayload);
            List<Long> ids =
                    childEntityDtoResult.childEntityDtos().stream()
                            .map(
                                    childEntityDto -> {
                                        Long targetContainerId = pairs.get(childEntityDto.getParentId());
                                        childEntityDto.setTargetContainerId(targetContainerId);
                                        return targetContainerId;
                                    })
                            .toList();
            Map<Long, ENTITY> containers =
                    dao.loadContainersForPaste(ids).stream()
                            .collect(Collectors.toMap(Identified::getId, container -> container));
            childEntityDtoResult
                    .childEntityDtos()
                    .forEach(
                            childEntityDto ->
                                    childEntityDto.setTargetContainer(
                                            containers.get(childEntityDto.getTargetContainerId())));
            copierService.copyNodeToMultipleContainers(
                    childEntityDtoResult.childEntityDtos(), pairingIdsSourceCopyNodeChild);
            offset += MAX_BATCH_SIZE;
            entityManager.clear();
        } while (childEntityDtoResult.hasMore());
        pairs.clear();
    }

    @SuppressWarnings("unchecked")
    private <NODE> EntityDao<NODE> getDaoFromNodeClass(String clazzSimpleName) {
        EntityType entityType = EntityType.fromSimpleName(clazzSimpleName);
        return switch (entityType) {
            case CAMPAIGN_LIBRARY -> (EntityDao<NODE>) campaignLibraryDao;
            case CAMPAIGN_FOLDER -> (EntityDao<NODE>) campaignFolderDao;
            case CAMPAIGN -> (EntityDao<NODE>) campaignDao;
            case ITERATION -> (EntityDao<NODE>) iterationDao;
                // Cannot create a case for TestSuite because TestSuiteDao is not an EntityDao
            case SPRINT -> (EntityDao<NODE>) sprintDao;
            case SPRINT_GROUP -> (EntityDao<NODE>) sprintGroupDao;
            default -> throw new IllegalArgumentException("Unsupported node type");
        };
    }
}
