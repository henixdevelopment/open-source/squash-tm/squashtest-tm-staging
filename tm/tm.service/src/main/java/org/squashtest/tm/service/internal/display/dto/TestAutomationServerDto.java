/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;

public class TestAutomationServerDto {

    private Long id;
    private String baseUrl;
    private String kind;
    private String name;
    private String description;
    private String createdBy;
    private Date createdOn;
    private String lastModifiedBy;
    private Date lastModifiedOn;
    private Boolean manualSlaveSelection;
    private String authProtocol;
    private boolean supportsAutomatedExecutionEnvironments;
    private List<String> environmentTags;
    private String observerUrl;
    private String eventBusUrl;
    private String additionalConfiguration;
    private List<BoundEnvironmentVariableDto> boundEnvironmentVariables;

    private String killswitchUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Boolean getManualSlaveSelection() {
        return manualSlaveSelection;
    }

    public void setManualSlaveSelection(Boolean manualSlaveSelection) {
        this.manualSlaveSelection = manualSlaveSelection;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getAuthProtocol() {
        return authProtocol;
    }

    public void setAuthProtocol(String authProtocol) {
        this.authProtocol = authProtocol;
    }

    public List<String> getEnvironmentTags() {
        return environmentTags;
    }

    public void setEnvironmentTags(List<String> environmentTags) {
        this.environmentTags = environmentTags;
    }

    @JsonProperty("supportsAutomatedExecutionEnvironments")
    public boolean isSupportsAutomatedExecutionEnvironments() {
        return supportsAutomatedExecutionEnvironments;
    }

    public void setSupportsAutomatedExecutionEnvironments(
            boolean supportsAutomatedExecutionEnvironments) {
        this.supportsAutomatedExecutionEnvironments = supportsAutomatedExecutionEnvironments;
    }

    public String getObserverUrl() {
        return observerUrl;
    }

    public void setObserverUrl(String observerUrl) {
        this.observerUrl = observerUrl;
    }

    public String getEventBusUrl() {
        return eventBusUrl;
    }

    public void setEventBusUrl(String eventBusUrl) {
        this.eventBusUrl = eventBusUrl;
    }

    public List<BoundEnvironmentVariableDto> getBoundEnvironmentVariables() {
        return boundEnvironmentVariables;
    }

    public void setBoundEnvironmentVariables(
            List<BoundEnvironmentVariableDto> boundEnvironmentVariables) {
        this.boundEnvironmentVariables = boundEnvironmentVariables;
    }

    public String getAdditionalConfiguration() {
        return additionalConfiguration;
    }

    public void setAdditionalConfiguration(String additionalConfiguration) {
        this.additionalConfiguration = additionalConfiguration;
    }

    public String getKillSwitchUrl() {
        return killswitchUrl;
    }

    public void setKillSwitchUrl(String killSwitchUrl) {
        this.killswitchUrl = killSwitchUrl;
    }
}
