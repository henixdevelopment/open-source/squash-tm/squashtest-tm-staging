/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.adapter;

import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException;
import org.squashtest.csp.core.bugtracker.core.ProjectNotFoundException;
import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerCacheInfo;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerInterfaceDescriptor;
import org.squashtest.tm.bugtracker.advanceddomain.DelegateCommand;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueFindContext;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchForm;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest;
import org.squashtest.tm.bugtracker.definition.Attachment;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.bugtracker.definition.RemoteProject;
import org.squashtest.tm.bugtracker.definition.context.BugTrackerBindingInfo;
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.service.spi.AdvancedBugTrackerConnector;

/**
 * This interface declares how to wrap the various bugtracker connector types (simple, advanced and
 * oslc) in a unified set of methods that Squash will use for its internal needs.
 */
public interface InternalBugtrackerConnector {

    /**
     * Declares which authentication protocols are supported by this BugTrackerConnector. Default
     * implementation returns [AuthenticationMode.USERNAME_PASSWORD]
     *
     * @return array of supported protocols
     */
    AuthenticationProtocol[] getSupportedAuthProtocols();

    /**
     * Declares whether the given connector supports a given connection protocol.
     *
     * @param protocol to test
     */
    boolean supports(AuthenticationProtocol protocol);

    /**
     * Authenticates to the bug tracker with the given credentials. If authentication does not fail,
     * it should not be required again at least for the current thread.
     *
     * @param credentials the credentials
     * @throws UnsupportedAuthenticationModeException if the connector cannot use the given
     *     credentials
     */
    void authenticate(Credentials credentials) throws UnsupportedAuthenticationModeException;

    /**
     * will check if the current credentials are actually acknowledged by the bugtracker
     *
     * @param credentials to check
     * @throws UnsupportedAuthenticationModeException if the connector cannot use the given
     *     credentials
     * @throws BugTrackerNoCredentialsException if the credentials are invalid
     * @throws BugTrackerRemoteException for other network exceptions.
     */
    void checkCredentials(Credentials credentials)
            throws BugTrackerNoCredentialsException, BugTrackerRemoteException;

    boolean areCredentialsValid(Credentials credentials);

    /**
     * Must return the URL where one can browse the issue.
     *
     * <p>Because implementations may use findIssue/findIssues, the issue ID must correspond to a
     * known issue.
     *
     * @see InternalBugtrackerConnector#findIssue(String key)
     * @param issueId remote issue ID
     * @param bugTracker used bug tracker
     * @return external URL to issue
     */
    URL makeViewIssueUrl(BugTracker bugTracker, String issueId);

    /**
     * Must return a project, given its name, with metadata such as which versions or categories are
     * defined in there.
     *
     * @param projectName
     * @return
     * @throws ProjectNotFoundException
     * @throws BugTrackerRemoteException
     */
    RemoteProject findProject(String projectName)
            throws ProjectNotFoundException, BugTrackerRemoteException;

    /**
     * @see #findProject(String), except that one uses the Id.
     * @param projectId
     * @return
     * @throws ProjectNotFoundException
     * @throws BugTrackerRemoteException
     */
    RemoteProject findProjectById(String projectId)
            throws ProjectNotFoundException, BugTrackerRemoteException;

    /**
     * Must create an issue on the remote bugtracker, then return the 'persisted' version of it (ie,
     * having its id)
     *
     * @param issue
     * @return
     * @throws BugTrackerRemoteException
     */
    RemoteIssue createIssue(RemoteIssue issue) throws BugTrackerRemoteException;

    /**
     * Must return ready-to-fill issue, ie with empty fields and its project configured with as many
     * metadata as possible related to issue creation.
     *
     * @param projectName
     * @return
     */
    RemoteIssue createReportIssueTemplate(String projectName, RemoteIssueContext context);

    /**
     * Returns an {@link BugTrackerInterfaceDescriptor}
     *
     * @return
     */
    BugTrackerInterfaceDescriptor getInterfaceDescriptor();

    /**
     * Retrieve a remote issue that are linked inside SquashTM.
     *
     * <p>Note : for some connectors (e.g. GitLab), this method won't allow to find issues that are
     * not known by SquashTM because SquashTM because the key alone is not enough and the missing
     * information may not be in database. For this reason, the method
     * searchIssue(RemoteIssueSearchRequest searchRequest) was added in 3.0.
     *
     * @param key bugtracker-dependant external key to be found
     */
    RemoteIssue findIssue(String key);

    /**
     * Retrieve many remote issues that are linked inside SquashTM.
     *
     * @param issueKeys a list of remote issue keys (external key that is bugtracker dependant) to be
     *     found
     * @return the matching issues
     */
    List<RemoteIssue> findIssues(Collection<String> issueKeys);

    default List<RemoteIssue> findKnownIssues(
            Collection<String> issueKeys, RemoteIssueFindContext context) {
        return findIssues(issueKeys);
    }

    /**
     * @return the search form to display when searching for existing issues
     */
    default RemoteIssueSearchForm createIssueSearchForm(BugTrackerBindingInfo bugTrackerBindingInfo) {
        return RemoteIssueSearchForm.defaultForm();
    }

    /**
     * Look for an existing remote issue.
     *
     * @see AdvancedBugTrackerConnector#searchIssue
     * @param searchRequest the search terms
     * @return a list of remote issues that match the search terms
     */
    Optional<? extends RemoteIssue> searchIssue(RemoteIssueSearchRequest searchRequest);

    /**
     * Given a remote issue key, will ask the bugtracker to attach the attachments to that issue.
     *
     * @param remoteIssueKey remote issue key
     * @param attachments to attach
     */
    void forwardAttachments(String remoteIssueKey, List<Attachment> attachments);

    /**
     * As of 3.0, only advanced bugtracker connectors support commands.
     *
     * @see AdvancedBugTrackerConnector#executeDelegateCommand(DelegateCommand)
     */
    Object executeDelegateCommand(DelegateCommand command);

    /**
     * Create Issue links between the issue identified by remoteIssueKey and each issue identified by
     * key of remoteReqIds
     *
     * @param remoteIssueKey
     * @param remoteReqIds
     */
    void linkIssues(String remoteIssueKey, List<String> remoteReqIds);

    String getKeyForLogin(BugTracker bugTracker);

    String getKeyForPassword(BugTracker bugTracker);

    boolean allowsReportingCache();

    void refreshCacheForBugTracker(BugTracker bugTracker);

    void refreshCacheForProject(BugTracker bugTracker, long projectId);

    BugTrackerCacheInfo getCacheInfo();
}
