/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.WRITE_SPRINT_GROUP_OR_ADMIN;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.campaign.CustomSprintGroupModificationService;
import org.squashtest.tm.service.internal.library.NodeManagementService;

@Transactional
@Service("CustomSprintGroupModificationService")
public class CustomSprintGroupModificationServiceImpl
        implements CustomSprintGroupModificationService {

    @Inject
    @Named("squashtest.tm.service.internal.SprintGroupManagementService")
    private NodeManagementService<SprintGroup, CampaignLibraryNode, CampaignFolder>
            sprintGroupManagementService;

    public CustomSprintGroupModificationServiceImpl() {
        super();
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_GROUP_OR_ADMIN)
    public void rename(@Id long sprintGroupId, String newName) {
        sprintGroupManagementService.renameNode(sprintGroupId, newName);
    }
}
