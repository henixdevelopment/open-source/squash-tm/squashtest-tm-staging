/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.coalesce;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;

import java.sql.Timestamp;
import java.util.List;
import org.jooq.DSLContext;
import org.jooq.Record9;
import org.jooq.SelectSelectStep;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.exception.UnknownEntityException;
import org.squashtest.tm.service.internal.display.dto.execution.SessionNoteDto;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SessionNoteDisplayDao;

@Repository
@Transactional(readOnly = true)
public class SessionNoteDisplayDaoImpl implements SessionNoteDisplayDao {

    private final DSLContext dsl;
    private final AttachmentDisplayDao attachmentDisplayDao;

    public SessionNoteDisplayDaoImpl(DSLContext dsl, AttachmentDisplayDao attachmentDisplayDao) {
        this.dsl = dsl;
        this.attachmentDisplayDao = attachmentDisplayDao;
    }

    @Override
    public List<SessionNoteDto> fetchSessionNotesByExecutionId(long executionId) {
        List<SessionNoteDto> sessionNoteViews =
                selectSessionNoteView()
                        .from(SESSION_NOTE)
                        .where(SESSION_NOTE.EXECUTION_ID.eq(executionId))
                        .orderBy(SESSION_NOTE.NOTE_ORDER)
                        .fetchInto(SessionNoteDto.class);

        for (SessionNoteDto sessionNoteView : sessionNoteViews) {
            Long attachmentListId = sessionNoteView.getAttachmentListId();
            sessionNoteView.setAttachmentList(
                    this.attachmentDisplayDao.findAttachmentListById(attachmentListId));
        }

        return sessionNoteViews;
    }

    @Override
    public SessionNoteDto findById(long noteId) {
        SessionNoteDto sessionNoteView =
                selectSessionNoteView()
                        .from(SESSION_NOTE)
                        .where(SESSION_NOTE.NOTE_ID.eq(noteId))
                        .fetchOneInto(SessionNoteDto.class);

        if (sessionNoteView != null) {
            Long attachmentListId = sessionNoteView.getAttachmentListId();
            sessionNoteView.setAttachmentList(
                    this.attachmentDisplayDao.findAttachmentListById(attachmentListId));
            return sessionNoteView;
        } else {
            throw new UnknownEntityException(noteId, SessionNote.class);
        }
    }

    private SelectSelectStep<
                    Record9<Long, String, String, Integer, Timestamp, String, Timestamp, String, Long>>
            selectSessionNoteView() {
        return dsl.select(
                SESSION_NOTE.NOTE_ID,
                SESSION_NOTE.KIND,
                SESSION_NOTE.CONTENT,
                SESSION_NOTE.NOTE_ORDER,
                SESSION_NOTE.CREATED_ON,
                SESSION_NOTE.CREATED_BY,
                SESSION_NOTE.LAST_MODIFIED_ON,
                SESSION_NOTE.LAST_MODIFIED_BY,
                SESSION_NOTE.ATTACHMENT_LIST_ID);
    }

    @Override
    public Long findProjectIdBySessionNoteId(long noteId) {
        return dsl.select(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(SESSION_NOTE)
                .on(SESSION_NOTE.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                .where(SESSION_NOTE.NOTE_ID.eq(noteId))
                .fetchOneInto(Long.class);
    }

    @Override
    public List<SessionNoteDto> fetchSessionNotesByOverviewId(long overviewId) {
        List<SessionNoteDto> sessionNoteViews =
                selectSessionNoteView()
                        .from(SESSION_NOTE)
                        .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                        .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(SESSION_NOTE.EXECUTION_ID))
                        .leftJoin(EXECUTION)
                        .on(EXECUTION.EXECUTION_ID.eq(SESSION_NOTE.EXECUTION_ID))
                        .leftJoin(EXPLORATORY_SESSION_OVERVIEW)
                        .on(
                                EXPLORATORY_SESSION_OVERVIEW
                                        .ITEM_TEST_PLAN_ID
                                        .eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID)
                                        .or(
                                                EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(
                                                        EXECUTION.TEST_PLAN_ITEM_ID)))
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                        .orderBy(coalesce(SESSION_NOTE.LAST_MODIFIED_ON, SESSION_NOTE.CREATED_ON).desc())
                        .fetchInto(SessionNoteDto.class);

        for (SessionNoteDto sessionNoteView : sessionNoteViews) {
            Long attachmentListId = sessionNoteView.getAttachmentListId();
            sessionNoteView.setAttachmentList(
                    this.attachmentDisplayDao.findAttachmentListById(attachmentListId));
        }

        return sessionNoteViews;
    }
}
