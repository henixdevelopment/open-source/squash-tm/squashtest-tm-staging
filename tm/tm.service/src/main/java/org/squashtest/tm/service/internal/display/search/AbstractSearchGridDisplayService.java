/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.display.search.SearchGridDisplayService;
import org.squashtest.tm.service.grid.ColumnIds;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.SearchDisplayDao;
import org.squashtest.tm.service.plugin.PluginFinderService;

public abstract class AbstractSearchGridDisplayService implements SearchGridDisplayService {

    private static final String REQUIREMENT_SEARCH = "requirement-search";
    private static final String TEST_CASE_SEARCH = "test-case-search";
    private static final String TC_BY_REQUIREMENT = "tc-by-requirement";

    private final SearchDisplayDao searchDisplayDao;

    @Inject private GridConfigurationService gridConfigurationService;

    @Inject private PluginFinderService pluginFinderService;

    protected AbstractSearchGridDisplayService(SearchDisplayDao searchDisplayDao) {
        this.searchDisplayDao = searchDisplayDao;
    }

    @Override
    public GridResponse fetchResearchRows(ResearchResult researchResult, GridRequest gridRequest) {
        String gridId = gridRequest.getGridId();
        List<String> activeColumnsIds = gridConfigurationService.findActiveColumnIdsForUser(gridId);
        if (pluginFinderService.isPremiumPluginInstalled()) {
            if (gridId.equals(REQUIREMENT_SEARCH)
                    && !ColumnIds.COLUMNS_NOT_TRIGGERING_REQ_REQUEST.containsAll(activeColumnsIds)) {
                gridRequest.getSimplifiedColumnDisplayGridIds().remove(REQUIREMENT_SEARCH);
            } else if ((gridId.equals(TEST_CASE_SEARCH) || gridId.equals(TC_BY_REQUIREMENT))
                    && !ColumnIds.COLUMNS_NOT_TRIGGERING_TEST_CASE_REQUEST.containsAll(activeColumnsIds)) {
                gridRequest.getSimplifiedColumnDisplayGridIds().remove(TEST_CASE_SEARCH);
            }
        }
        GridResponse gridResponse = searchDisplayDao.getRows(researchResult.getIds(), gridRequest);
        gridResponse.setCount(researchResult.getCount());
        gridResponse.setPage(
                GridResponse.getPageNumberWithinBounds(gridRequest, researchResult.getCount()));
        reorderAccordingToResults(researchResult, gridResponse);
        gridResponse.setActiveColumnIds(gridConfigurationService.findActiveColumnIdsForUser(gridId));
        return gridResponse;
    }

    private void reorderAccordingToResults(ResearchResult researchResult, GridResponse gridResponse) {
        Map<Long, DataRow> dataRowMap = createDataRowMapById(gridResponse);
        List<DataRow> orderedRows = reorderRows(researchResult, dataRowMap);
        gridResponse.setDataRows(orderedRows);
    }

    private List<DataRow> reorderRows(ResearchResult researchResult, Map<Long, DataRow> dataRowMap) {
        return researchResult.getIds().stream().map(dataRowMap::get).collect(Collectors.toList());
    }

    private Map<Long, DataRow> createDataRowMapById(GridResponse gridResponse) {
        return gridResponse.getDataRows().stream()
                .collect(Collectors.toMap(row -> Long.parseLong(row.getId()), Function.identity()));
    }
}
