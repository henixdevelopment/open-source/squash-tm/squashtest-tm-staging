/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.library;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_TA_API_CLIENT;
import static org.squashtest.tm.service.security.Authorizations.READ_CAMPAIGN_LIBRARY_NODE_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_EXECUTION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_ITERATION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_LIBRARY_NODE_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_SESSION_OVERVIEW_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_TEST_CASE_LIBRARY_NODE_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_TS_OR_ROLE_ADMIN;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;

@Service("squashtest.tm.service.PathHeaderService")
public class EntityPathHeaderServiceImpl implements EntityPathHeaderService {

    private final EntityPathHeaderDao entityPathHeaderDao;

    public EntityPathHeaderServiceImpl(EntityPathHeaderDao entityPathHeaderDao) {
        this.entityPathHeaderDao = entityPathHeaderDao;
    }

    @Override
    @PreAuthorize(READ_TEST_CASE_LIBRARY_NODE_OR_ROLE_ADMIN + OR_HAS_ROLE_TA_API_CLIENT)
    public String buildTCLNPathHeader(Long tclnId) {
        return entityPathHeaderDao.buildTestCaseLibraryNodePathHeader(tclnId);
    }

    @Override
    @PreAuthorize(READ_REQUIREMENT_LIBRARY_NODE_OR_ROLE_ADMIN)
    public String buildRLNPathHeader(Long rlnId) {
        return entityPathHeaderDao.buildRequirementLibraryNodePathHeader(rlnId);
    }

    @Override
    @PreAuthorize(READ_CAMPAIGN_LIBRARY_NODE_OR_ROLE_ADMIN)
    public String buildCLNPathHeader(Long clnId) {
        return entityPathHeaderDao.buildCampaignLibraryNodePathHeader(clnId);
    }

    @Override
    @PreAuthorize(READ_ITERATION_OR_ROLE_ADMIN + OR_HAS_ROLE_TA_API_CLIENT)
    public String buildIterationPathHeader(Long iterationId) {
        return entityPathHeaderDao.buildIterationPathHeader(iterationId);
    }

    @Override
    @PreAuthorize(READ_TS_OR_ROLE_ADMIN + OR_HAS_ROLE_TA_API_CLIENT)
    public String buildTestSuitePathHeader(Long testSuiteId) {
        return entityPathHeaderDao.buildTestSuitePathHeader(testSuiteId);
    }

    @Override
    @PreAuthorize(READ_EXECUTION_OR_ROLE_ADMIN)
    public String buildExecutionPathHeader(Long executionId) {
        return entityPathHeaderDao.buildExecutionPathHeader(executionId);
    }

    @Override
    @PreAuthorize(READ_SESSION_OVERVIEW_OR_ROLE_ADMIN)
    public String buildExploratorySessionOverviewPathHeader(Long sessionOverviewId) {
        return entityPathHeaderDao.buildExploratorySessionOverviewPathHeader(sessionOverviewId);
    }

    @Override
    public Map<Long, String> buildRequirementPathsByIds(Collection<Long> requirementIds) {
        if (requirementIds == null || requirementIds.isEmpty()) {
            return Collections.emptyMap();
        }

        return entityPathHeaderDao.buildRequirementPathByIds(requirementIds);
    }
}
