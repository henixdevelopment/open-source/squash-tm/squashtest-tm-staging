/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable;

import static org.squashtest.tm.service.security.Authorizations.EXECUTE_ITERATION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.environmentvariable.EVInputType;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingValueService;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableDto;
import org.squashtest.tm.service.internal.repository.CustomEnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao;
import org.squashtest.tm.service.internal.repository.display.EnvironmentVariableDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Service("squashtest.tm.service.EnvironmentVariableBindingService")
@Transactional
public class EnvironmentVariableBindingServiceImpl implements EnvironmentVariableBindingService {

    private final EnvironmentVariableDao environmentVariableDao;
    private final EnvironmentVariableBindingDao environmentVariableBindingDao;
    private final EnvironmentVariableBindingValueService bindingValueService;
    private final CustomEnvironmentVariableBindingDao customEnvironmentVariableBindingDao;
    private final EnvironmentVariableDisplayDao environmentVariableDisplayDao;
    private final ProjectDisplayDao projectDisplayDao;
    private final PermissionEvaluationService permissionEvaluationService;

    public EnvironmentVariableBindingServiceImpl(
            EnvironmentVariableDao environmentVariableDao,
            EnvironmentVariableBindingDao environmentVariableBindingDao,
            EnvironmentVariableBindingValueService bindingValueService,
            CustomEnvironmentVariableBindingDao customEnvironmentVariableBindingDao,
            EnvironmentVariableDisplayDao environmentVariableDisplayDao,
            ProjectDisplayDao projectDisplayDao,
            PermissionEvaluationService permissionEvaluationService) {
        this.environmentVariableDao = environmentVariableDao;
        this.environmentVariableBindingDao = environmentVariableBindingDao;
        this.bindingValueService = bindingValueService;
        this.customEnvironmentVariableBindingDao = customEnvironmentVariableBindingDao;
        this.environmentVariableDisplayDao = environmentVariableDisplayDao;
        this.projectDisplayDao = projectDisplayDao;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    @Override
    public void createNewBindings(
            Long entityId, EVBindableEntity entityType, List<Long> environmentVariableIds) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        List<EnvironmentVariable> environmentVariables =
                environmentVariableDao.findAllByIdIn(environmentVariableIds);

        for (EnvironmentVariable environmentVariable : environmentVariables) {
            createBinding(entityId, entityType, environmentVariable);
            if (isTestAutomationServerEntityType(entityType)) {
                bindNewServerVariableToAllLinkedProjects(entityId, environmentVariable);
            }
        }
    }

    private boolean isTestAutomationServerEntityType(EVBindableEntity entityType) {
        return EVBindableEntity.TEST_AUTOMATION_SERVER.equals(entityType);
    }

    private void bindNewServerVariableToAllLinkedProjects(
            Long serverId, EnvironmentVariable environmentVariable) {
        List<Long> projectsId =
                customEnvironmentVariableBindingDao.findProjectsLinkedToServerWhereVariableIsNotBound(
                        serverId, environmentVariable);
        projectsId.forEach(id -> createBinding(id, EVBindableEntity.PROJECT, environmentVariable));
    }

    @Override
    public void unbind(Long entityId, EVBindableEntity entityType, List<Long> evIds) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        Collection<Long> bindingIds =
                customEnvironmentVariableBindingDao.findAllBindingIdsByEvIdsAndEntityIdType(
                        evIds, entityId, entityType);
        environmentVariableBindingDao.deleteAllById(bindingIds);
    }

    /**
     * Remove all existing bindings where deleted environment variable is used.
     *
     * @param environmentVariableId Deleted environment variable
     */
    @Override
    public void unbindByEnvironmentVariableId(Long environmentVariableId) {
        List<EnvironmentVariableBinding> bindings =
                environmentVariableBindingDao.findAllByEnvironmentVariable_Id(environmentVariableId);
        deleteAllBindings(bindings);
    }

    @Override
    public void unbindAllByEntityIdAndType(Long entityId, EVBindableEntity entityType) {
        List<EnvironmentVariableBinding> bindings =
                environmentVariableBindingDao.findAllByEntityIdAndEntityType(entityId, entityType);
        deleteAllBindings(bindings);
    }

    @Override
    public List<BoundEnvironmentVariableDto> getBoundEnvironmentVariablesByEntity(
            Long entityId, EVBindableEntity entityType) {
        List<BoundEnvironmentVariableDto> boundEnvironmentVariableDtos =
                customEnvironmentVariableBindingDao.findAllBoundEnvironmentVariables(entityId, entityType);
        setOptionValues(boundEnvironmentVariableDtos);
        return boundEnvironmentVariableDtos;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public List<BoundEnvironmentVariableDto> getBoundEnvironmentVariablesByTestAutomationServerId(
            Long testAutomationServerId) {
        return getBoundEnvironmentVariablesByEntity(
                testAutomationServerId, EVBindableEntity.TEST_AUTOMATION_SERVER);
    }

    private void setOptionValues(List<BoundEnvironmentVariableDto> boundEnvironmentVariables) {
        boundEnvironmentVariables.forEach(
                boundEnvironmentVariableDto -> {
                    if (boundEnvironmentVariableDto.getInputType().equals(EVInputType.DROPDOWN_LIST.name())) {
                        boundEnvironmentVariableDto.setOptions(
                                environmentVariableDisplayDao.getEnvironmentVariableOptionListByEvId(
                                        boundEnvironmentVariableDto.getId()));
                    }
                });
    }

    @Override
    public void bindServerEnvironmentVariablesToProject(Long taServerId, Long projectId) {

        Map<EnvironmentVariable, String> serverBindingValueByVariableMap =
                getVariablesToBindToProject(taServerId, projectId);

        List<EnvironmentVariableBinding> bindingsToCreate = new ArrayList<>();

        serverBindingValueByVariableMap.forEach(
                (environmentVariable, serverBindingValue) ->
                        bindingsToCreate.add(
                                new EnvironmentVariableBinding(
                                        environmentVariable, projectId, EVBindableEntity.PROJECT, serverBindingValue)));

        environmentVariableBindingDao.saveAll(bindingsToCreate);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public List<BoundEnvironmentVariableDto> getBoundEnvironmentVariableFromProjectView(
            Long projectId) {
        List<BoundEnvironmentVariableDto> boundEnvironmentVariableDtos =
                getBoundEnvironmentVariablesByEntity(projectId, EVBindableEntity.PROJECT);
        setIsEnvironmentVariableBoundToServer(projectId, boundEnvironmentVariableDtos);
        return boundEnvironmentVariableDtos;
    }

    @Override
    @PreAuthorize(EXECUTE_ITERATION_OR_ROLE_ADMIN)
    public List<BoundEnvironmentVariableDto> getBoundEnvironmentVariableFromProjectViewByIteration(
            Long projectId, Long iterationId) {
        List<BoundEnvironmentVariableDto> boundEnvironmentVariableDtos =
                getBoundEnvironmentVariablesByEntity(projectId, EVBindableEntity.PROJECT);
        setIsEnvironmentVariableBoundToServer(projectId, boundEnvironmentVariableDtos);
        return boundEnvironmentVariableDtos;
    }

    /**
     * Check if project is bound to server. Then, if the project's environment variables are also
     * bound to the current server, set 'isBoundToServer' property to true.
     *
     * @param projectId project id
     * @param boundEnvironmentVariableDtos environment variables bound to project
     */
    private void setIsEnvironmentVariableBoundToServer(
            Long projectId, List<BoundEnvironmentVariableDto> boundEnvironmentVariableDtos) {
        Long taServerId = projectDisplayDao.getTaServerIdByProjectId(projectId);
        if (Objects.nonNull(taServerId)) {
            List<BoundEnvironmentVariableDto> serverBoundVariableDtos =
                    getBoundEnvironmentVariablesByEntity(taServerId, EVBindableEntity.TEST_AUTOMATION_SERVER);
            List<Long> serverBoundEvIds =
                    serverBoundVariableDtos.stream().map(EnvironmentVariableDto::getId).toList();
            boundEnvironmentVariableDtos.forEach(
                    environmentVariable -> {
                        if (serverBoundEvIds.contains(environmentVariable.getId())) {
                            environmentVariable.setIsBoundToServer(true);
                        }
                    });
        }
    }

    /**
     * Compare project bindings and server bindings and get environment variables with server value
     * not yet linked to project.
     *
     * @param taServerId server id linked to project
     * @param projectId target project for binding
     * @return Environment variables to bind to project with server value as Map.
     */
    private Map<EnvironmentVariable, String> getVariablesToBindToProject(
            Long taServerId, Long projectId) {
        Map<EnvironmentVariable, String> serverBindingValueByVariableMap = new HashMap<>();

        List<EnvironmentVariableBinding> projectBindings =
                environmentVariableBindingDao.findAllByEntityIdAndEntityType(
                        projectId, EVBindableEntity.PROJECT);

        List<EnvironmentVariableBinding> serverBindings =
                environmentVariableBindingDao.findAllByEntityIdAndEntityType(
                        taServerId, EVBindableEntity.TEST_AUTOMATION_SERVER);

        filterOutVariablesBoundToProject(
                serverBindingValueByVariableMap, projectBindings, serverBindings);

        return serverBindingValueByVariableMap;
    }

    private void filterOutVariablesBoundToProject(
            Map<EnvironmentVariable, String> serverBindingValueByVariableMap,
            List<EnvironmentVariableBinding> projectBindings,
            List<EnvironmentVariableBinding> serverBindings) {
        List<EnvironmentVariable> variablesAlreadyBoundToProject =
                projectBindings.stream().map(EnvironmentVariableBinding::getEnvironmentVariable).toList();

        serverBindings.stream()
                .filter(
                        binding -> !variablesAlreadyBoundToProject.contains(binding.getEnvironmentVariable()))
                .forEach(
                        binding ->
                                serverBindingValueByVariableMap.put(
                                        binding.getEnvironmentVariable(), binding.getValue()));
    }

    private void deleteAllBindings(List<EnvironmentVariableBinding> bindings) {
        List<Long> bindingIds = bindings.stream().map(EnvironmentVariableBinding::getId).toList();
        environmentVariableBindingDao.deleteAllById(bindingIds);
    }

    private void createBinding(
            Long entityId, EVBindableEntity entityType, EnvironmentVariable environmentVariable) {

        EnvironmentVariableBinding environmentVariableBinding =
                new EnvironmentVariableBinding(environmentVariable, entityId, entityType);

        checkIfBindingExist(entityId, entityType, environmentVariable.getId());
        if (EVBindableEntity.PROJECT.equals(entityType)) {
            bindingValueService.setProjectValueFromExistingServerValue(environmentVariableBinding);
        }
        environmentVariableBindingDao.save(environmentVariableBinding);
    }

    private void checkIfBindingExist(
            Long serverId, EVBindableEntity entityType, Long environmentVariableId) {
        EnvironmentVariableBinding environmentVariableBinding =
                environmentVariableBindingDao.findByEntityIdTypeAndEvId(
                        serverId, entityType, environmentVariableId);
        if (environmentVariableBinding != null) {
            throw new EntityExistsException("Binding already exists.");
        }
    }
}
