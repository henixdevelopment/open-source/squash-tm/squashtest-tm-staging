/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.exception.testautomation.DuplicateTMLabelException;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.TestAutomationProjectDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.testautomation.TestAutomationProjectFinderService;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.service.testautomation.spi.TestAutomationServerNoCredentialsException;

@Transactional
@Service("squashtest.tm.service.TestAutomationProjectManagementService")
public class TestAutomationProjectManagerServiceImpl
        implements TestAutomationProjectManagerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestAutomationConnector.class);

    private final TestAutomationProjectDao projectDao;
    private final TestAutomationConnectorRegistry connectorRegistry;
    private final GenericProjectDao genericProjectDao;
    private final CredentialsProvider credentialsProvider;
    private final MessageSource i18nHelper;
    private final PermissionEvaluationService permissionEvaluationService;

    public TestAutomationProjectManagerServiceImpl(
            TestAutomationProjectDao projectDao,
            TestAutomationConnectorRegistry connectorRegistry,
            GenericProjectDao genericProjectDao,
            CredentialsProvider credentialsProvider,
            MessageSource i18nHelper,
            PermissionEvaluationService permissionEvaluationService) {
        this.projectDao = projectDao;
        this.connectorRegistry = connectorRegistry;
        this.genericProjectDao = genericProjectDao;
        this.credentialsProvider = credentialsProvider;
        this.i18nHelper = i18nHelper;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    private String getMessage(String i18nKey) {
        Locale locale = LocaleContextHolder.getLocale();
        return i18nHelper.getMessage(i18nKey, null, locale);
    }

    @Override
    public void persist(TestAutomationProject newProject) {
        projectDao.persist(newProject);
    }

    @Override
    public TestAutomationProject findProjectById(long projectId) {
        return projectDao.findById(projectId);
    }

    @Override
    public void deleteProject(long projectId) {
        projectDao.deleteProjectsByIds(Collections.singletonList(projectId));
    }

    @Override
    public void deleteAllForTMProject(long tmProjectId) {
        Collection<Long> allprojects = projectDao.findAllByTMProject(tmProjectId);
        projectDao.deleteProjectsByIds(allprojects);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeLabel(long projectId, String label) {
        TestAutomationProject project = projectDao.findById(projectId);
        if (!project.getLabel().equals(label)) {
            List<String> taProjectNames =
                    genericProjectDao.findBoundTestAutomationProjectLabels(project.getTmProject().getId());
            if (taProjectNames.contains(label)) {
                throw new DuplicateTMLabelException(label);
            }
        }
        project.setLabel(label);
    }

    private void changeJobName(long projectId, String jobName) {
        TestAutomationProject project = projectDao.findById(projectId);
        project.setJobName(jobName);
    }

    private void changeSlaves(long projectId, String slaveList) {
        TestAutomationProject project = projectDao.findById(projectId);
        project.setSlaves(slaveList);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void changeCanRunGherkin(long projectId, boolean canRunGherkin) {
        TestAutomationProject project = projectDao.findById(projectId);
        if (canRunGherkin) {
            setUniqueBddProject(projectId);
        } else {
            project.setCanRunGherkin(false);
        }
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void editProject(long projectId, TestAutomationProject newValues) {
        changeJobName(projectId, newValues.getJobName());
        changeLabel(projectId, newValues.getLabel());
        changeSlaves(projectId, newValues.getSlaves());
        if (newValues.isCanRunGherkin()) {
            setUniqueBddProject(projectId);
        } else {
            changeCanRunGherkin(projectId, newValues.isCanRunGherkin());
        }
    }

    @Override
    public Collection<Long> findAllIdsByTMProject(Long projectId) {
        return projectDao.findAllByTMProject(projectId);
    }

    @Override
    public Collection<TestAutomationProject> listProjectsOnServer(TestAutomationServer server) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();

        TestAutomationConnector connector = connectorRegistry.getConnectorForKind(server.getKind());

        Optional<Credentials> maybeCredentials = credentialsProvider.getAppLevelCredentials(server);
        Supplier<TestAutomationServerNoCredentialsException> throwIfNull =
                () -> {
                    throw new TestAutomationServerNoCredentialsException(
                            String.format(
                                    getMessage("message.testAutomationServer.noCredentials"), server.getName()));
                };
        Credentials credentials = maybeCredentials.orElseThrow(throwIfNull);

        AuthenticationProtocol protocol = credentials.getImplementedProtocol();
        if (!connector.supports(protocol)) {
            throw new UnsupportedAuthenticationModeException(protocol.toString());
        }

        try {
            connector.checkCredentials(server, credentials);
            return connector.listProjectsOnServer(server, credentials);
        } catch (TestAutomationException ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Test Automation : failed to list projects on server : ", ex);
            }
            throw ex;
        }
    }

    @Override
    public Collection<TestAutomationProject> listProjectsOnServerForCurrentUser(
            TestAutomationServer server) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();

        Optional<Credentials> optionalCredentials =
                credentialsProvider.getCurrentUserCredentials(server);
        TestAutomationConnector connector = connectorRegistry.getConnectorForKind(server.getKind());
        try {
            Credentials credentials = optionalCredentials.orElseThrow();
            connector.checkCredentials(server, credentials);

            return connector.listProjectsOnServer(server, credentials);
        } catch (TestAutomationException ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Test Automation : failed to list projects on server : ", ex);
            }
            throw ex;
        } catch (NoSuchElementException noSuchElementException) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(
                        "Failed to list projects on server {} with url {} because no Credentials are stored for the current user.",
                        server.getName(),
                        server.getUrl(),
                        noSuchElementException);
            }
            throw noSuchElementException;
        }
    }

    @Override
    public URL findProjectURL(TestAutomationProject testAutomationProject) {
        TestAutomationServer server = testAutomationProject.getServer();
        TestAutomationConnector connector = connectorRegistry.getConnectorForKind(server.getKind());
        return connector.findTestAutomationProjectURL(testAutomationProject);
    }

    /**
     * @see TestAutomationProjectFinderService#haveExecutedTests(Collection<Long>)
     */
    @Override
    public boolean haveExecutedTests(Collection<Long> taProjectIds) {
        return projectDao.haveExecutedTestsByIds(taProjectIds);
    }

    @Override
    public List<TestAutomationProjectDto> findAllByTMProject(long tmProjectId) {
        // TODO fix N+1 query
        return projectDao.findAllByTMProject(tmProjectId).stream()
                .map(projectDao::findById)
                .map(TestAutomationProjectDto::fromPersistedTestAutomationProject)
                .toList();
    }

    @Override
    public void setUniqueBddProject(long projectId) {
        final TestAutomationProject taProject = projectDao.findById(projectId);
        final Collection<Long> allProjectsOnServer =
                projectDao.findAllByTMProject(taProject.getTmProject().getId());

        allProjectsOnServer.stream()
                .filter(Objects::nonNull)
                .forEach(
                        testAutomationProjectId -> {
                            TestAutomationProject testAutomationProject =
                                    projectDao.findById(testAutomationProjectId);
                            testAutomationProject.setCanRunGherkin(testAutomationProjectId.equals(projectId));
                        });
    }
}
