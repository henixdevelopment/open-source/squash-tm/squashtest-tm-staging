/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Consumer;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.csp.core.bugtracker.core.BugTrackerLocalException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.core.UnknownConnectorKindException;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationPolicy;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.bugtracker.BugTrackersService;
import org.squashtest.tm.service.bugtracker.CustomBugTrackerModificationService;
import org.squashtest.tm.service.internal.bugtracker.adapter.InternalBugtrackerConnector;
import org.squashtest.tm.service.internal.repository.BugTrackerDao;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

/**
 * @author mpagnon
 */
@Service("CustomBugTrackerModificationService")
@Transactional
public class CustomBugTrackerModificationServiceImpl
        implements CustomBugTrackerModificationService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(CustomBugTrackerModificationServiceImpl.class);

    @Inject private BugTrackerDao bugTrackerDao;

    @Inject private StoredCredentialsManager credentialsManager;

    @Inject private BugTrackerConnectorFactory connectorFactory;

    @Inject private BugTrackersService btService;

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeUrl(long bugtrackerId, String url) {
        final BugTracker bugTracker = bugTrackerDao.getReferenceById(bugtrackerId);
        bugTracker.setUrl(url);
        refreshCacheForBugTracker(bugTracker);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeName(long bugtrackerId, String newName) {
        String trimedNewName = newName.trim();
        BugTracker bugTracker = bugTrackerDao.getReferenceById(bugtrackerId);
        if (!bugTracker.getName().equals(trimedNewName)) {
            BugTracker existing = bugTrackerDao.findByName(trimedNewName);
            if (existing == null) {
                bugTracker.setName(trimedNewName);
            } else {
                throw new NameAlreadyInUseException(
                        NameAlreadyInUseException.EntityType.BUG_TRACKER, trimedNewName);
            }
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void storeCredentials(long serverId, ManageableCredentials credentials) {
        credentialsManager.storeAppLevelCredentials(serverId, credentials);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public ManageableCredentials findCredentials(long serverId) {
        return credentialsManager.findAppLevelCredentials(serverId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteCredentials(long serverId) {
        credentialsManager.deleteAppLevelCredentials(serverId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public AuthenticationProtocol[] getSupportedProtocols(BugTracker bugtracker) {
        InternalBugtrackerConnector connector = connectorFactory.createConnector(bugtracker);
        return connector.getSupportedAuthProtocols();
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeAuthenticationPolicy(long bugtrackerId, AuthenticationPolicy policy) {
        BugTracker tracker = bugTrackerDao.getReferenceById(bugtrackerId);
        tracker.setAuthenticationPolicy(policy);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeAuthenticationProtocol(long bugtrackerId, AuthenticationProtocol protocol) {
        BugTracker tracker = bugTrackerDao.getReferenceById(bugtrackerId);
        changeAuthenticationProtocol(tracker, protocol);
    }

    private void changeAuthenticationProtocol(BugTracker tracker, AuthenticationProtocol protocol) {
        tracker.setAuthenticationProtocol(protocol);

        credentialsManager.deleteAppLevelCredentials(tracker.getId());
        credentialsManager.deleteAllServerCredentials(Collections.singletonList(tracker.getId()));
        credentialsManager.deleteServerAuthConfiguration(tracker.getId());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void testCredentials(long bugtrackerId, ManageableCredentials credentials) {

        BugTracker bt = bugTrackerDao.getReferenceById(bugtrackerId);
        Credentials usableCredentials = credentials.build(credentialsManager, bt, null);

        if (usableCredentials == null) {
            throw new BugTrackerNoCredentialsException(
                    "credentials could not be built, either because the credentials themselves "
                            + "are not suitable, or because the protocol configuration is incomplete/invalid",
                    null);
        }

        btService.testCredentials(bt, usableCredentials);
    }

    @Override
    @UsedInPlugin("rest-api-admin")
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void storeAuthConfiguration(long serverId, ServerAuthConfiguration conf) {
        credentialsManager.storeServerAuthConfiguration(serverId, conf);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public ServerAuthConfiguration findAuthConfiguration(long serverId) {
        return credentialsManager.findServerAuthConfiguration(serverId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeKind(long bugtrackerId, String kind) {
        BugTracker tracker = bugTrackerDao.getReferenceById(bugtrackerId);
        tracker.setKind(kind);
        AuthenticationProtocol[] supportedProtocols = getSupportedProtocols(tracker);
        if (supportedProtocols.length != 0
                && !isTrackerProtocolSupported(tracker, supportedProtocols)) {
            changeAuthenticationProtocol(tracker, supportedProtocols[0]);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void updateDescription(long bugtrackerId, String description) {
        BugTracker bugTracker = bugTrackerDao.getReferenceById(bugtrackerId);
        bugTracker.setDescription(description);
    }

    private boolean isTrackerProtocolSupported(
            BugTracker tracker, AuthenticationProtocol[] supportedProtocols) {
        return Arrays.asList(supportedProtocols).contains(tracker.getAuthenticationProtocol());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void storeCacheCredentials(long serverId, ManageableCredentials credentials) {
        final Optional<BugTracker> bugTracker = bugTrackerDao.findById(serverId);

        if (bugTracker.isPresent()) {
            credentialsManager.storeReportingCacheCredentials(serverId, credentials);
            refreshCacheForBugTracker(bugTracker.get());
        } else {
            throw new BugTrackerLocalException("No bug tracker found with id " + serverId, null);
        }
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public void deleteCacheCredentials(long bugTrackerId) {
        credentialsManager.deleteReportingCacheCredentials(bugTrackerId);
    }

    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    @Override
    public void refreshCacheForProject(long projectId) {
        final BugTracker bugTracker = bugTrackerDao.findByProjectId(projectId);

        if (bugTracker == null) {
            // Part of nominal case: the project has no bug tracker, so there is nothing to refresh.
            return;
        }

        performWithConnector(
                bugTracker, connector -> connector.refreshCacheForProject(bugTracker, projectId));
    }

    private void refreshCacheForBugTracker(@NotNull BugTracker bugTracker) {
        performWithConnector(bugTracker, connector -> connector.refreshCacheForBugTracker(bugTracker));
    }

    private void performWithConnector(
            @NotNull BugTracker bugTracker, @NotNull Consumer<InternalBugtrackerConnector> consumer) {
        try {
            final InternalBugtrackerConnector connector = connectorFactory.createConnector(bugTracker);
            consumer.accept(connector);
        } catch (UnknownConnectorKindException e) {
            // Most likely the plugin was removed, so we can't refresh the cache. This is not a fatal
            // error and the transaction should not be rolled back.
            LOGGER.warn("Could not refresh cache for bug tracker with id {}", bugTracker.getId(), e);
        }
    }
}
