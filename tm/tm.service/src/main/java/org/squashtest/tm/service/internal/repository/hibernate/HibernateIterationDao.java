/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENTS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENT_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CAMPAIGN;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CONTENT;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_PLAN;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_PLANS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_SUITES;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_ITERATIONS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_ITERATION_BY_ID;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_PROJECT_ID_BY_ITERATION_ID;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_TEST_PLAN_IDS_BY_ITERATION_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestPlanStatistics;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseExecutionStatus;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;
import org.squashtest.tm.service.statistics.CountOnEnum;

@Repository
public class HibernateIterationDao extends HibernateEntityDao<Iteration> implements IterationDao {
    private static final String UNCHECKED = "unchecked";

    @PersistenceContext private EntityManager em;

    @Override
    public List<Iteration> findAllIterationContainingTestCase(long testCaseId) {
        return executeListNamedQuery(
                "iterationDao.findAllIterationContainingTestCase",
                new SetIdParameter("testCaseId", testCaseId));
    }

    @Override
    public List<NamedReference> findAllTestSuitesAsNamedReferences(final long iterationId) {
        return entityManager
                .createNamedQuery("iteration.findAllTestSuitesAsNamedReferences")
                .setParameter(ParameterNames.ITERATION_ID, iterationId)
                .getResultList();
    }

    /**
     * <p>Will persist a new Iteration, if its test plan contains transient test plan items they will be persisted too.</p>
     * <p>
     * Deprecation notice : As of TM 1.15 the simpler method {@link #persist(org.​squashtest.​tm.​domain.​campaign.Iteration) will just
     * do the same.
     * </p>
     *
     * @param iteration
     * @deprecated
     */
    @Override
    /**
     * @deprecated does not seem to be used any longer
     */
    @Deprecated
    public void persistIterationAndTestPlan(Iteration iteration) {
        persist(iteration);
    }

    @UsedInPlugin("rest-api")
    public TestPlanStatistics getIterationStatisticsByItpiIds(List<Long> itpiIds) {
        List<Object[]> result =
                entityManager
                        .createNamedQuery("iteration.countStatusesByItpiIds")
                        .setParameter("itpiIds", itpiIds)
                        .getResultList();

        LinkedHashMap<ExecutionStatus, Integer> canonicalStatusMap =
                CountOnEnum.fromTuples(result, ExecutionStatus.class)
                        .getStatistics(
                                ExecutionStatus::getCanonicalStatus, ExecutionStatus.getCanonicalStatusSet());

        return new TestPlanStatistics(canonicalStatusMap);
    }

    // **************************** TEST PLAN ******************************

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<TestCaseExecutionStatus> findExecStatusForIterationsAndTestCases(
            List<Long> testCasesIds, List<Long> iterationsIds) {

        if (testCasesIds.isEmpty()) {
            return Collections.emptyList();
        }

        List<Object[]> results =
                entityManager
                        .createNamedQuery("iteration.findITPIByTestCaseGroupByStatus")
                        .setParameter("testCasesIds", testCasesIds)
                        .setParameter("iterationsIds", iterationsIds)
                        .getResultList();

        List<TestCaseExecutionStatus> formatedResult = new ArrayList<>(results.size());

        for (Object[] result : results) {
            formatedResult.add(
                    new TestCaseExecutionStatus((ExecutionStatus) result[0], (Long) result[1]));
        }
        return formatedResult;
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findVerifiedTcIdsInIterations(
            List<Long> testCasesIds, List<Long> iterationIds) {
        return findAllByTestCasesAndIterations(
                "iteration.findVerifiedTcIdsInIterations", testCasesIds, iterationIds);
    }

    private <R> List<R> findAllByTestCasesAndIterations(
            String queryName, List<Long> testCaseIds, List<Long> iterationIds) {
        if (testCaseIds.isEmpty()) {
            return Collections.emptyList();
        }

        return entityManager
                .createNamedQuery(queryName)
                .setParameter("testCasesIds", testCaseIds)
                .setParameter("iterationsIds", iterationIds)
                .getResultList();
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findVerifiedTcIdsInIterationsWithExecution(
            List<Long> tcIds, List<Long> iterationsIds) {
        return findAllByTestCasesAndIterations(
                "iteration.findVerifiedAndExecutedTcIdsInIterations", tcIds, iterationsIds);
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public MultiMap findVerifiedITPI(List<Long> tcIds, List<Long> iterationsIds) {
        List<Object[]> itpis =
                findAllByTestCasesAndIterations(
                        "iteration.findITPIByTestCaseGroupByStatus", tcIds, iterationsIds);
        MultiMap result = new MultiValueMap();

        for (Object[] itpi : itpis) {
            TestCaseExecutionStatus tcStatus =
                    new TestCaseExecutionStatus((ExecutionStatus) itpi[0], (Long) itpi[1]);
            result.put(tcStatus.getTestCaseId(), tcStatus);
        }

        return result;
    }

    @Override
    public Iteration findByUUID(String targetUUID) {
        return em.createQuery("SELECT i FROM Iteration i " + "WHERE i.uuid = :uuid", Iteration.class)
                .setParameter("uuid", targetUUID)
                .getSingleResult();
    }

    @Override
    public Long getProjectId(long iterationId) {
        return em.createQuery(FIND_PROJECT_ID_BY_ITERATION_ID, Long.class)
                .setParameter("id", iterationId)
                .getSingleResult();
    }

    @Override
    public List<Long> findTestPlanIds(long iterationId) {
        return em.createQuery(FIND_TEST_PLAN_IDS_BY_ITERATION_ID, Long.class)
                .setParameter("id", iterationId)
                .getResultList();
    }

    @Override
    public Iteration loadContainerForPaste(long id) {
        Iteration iteration =
                new EntityGraphQueryBuilder<>(entityManager, Iteration.class, FIND_ITERATION_BY_ID)
                        .addAttributeNodes(TEST_SUITES)
                        .execute(Map.of(ID, id));
        new EntityGraphQueryBuilder<>(entityManager, Iteration.class, FIND_ITERATION_BY_ID)
                .addAttributeNodes(TEST_PLANS)
                .addSubGraph(TEST_PLANS, EXPLORATORY_SESSION_OVERVIEW)
                .execute(Map.of(ID, id));
        return iteration;
    }

    @Override
    public List<Iteration> loadNodeForPaste(Collection<Long> ids) {
        String testPlanSession =
                EntityGraphQueryBuilder.buildParentAttribute(TEST_PLANS, EXPLORATORY_SESSION_OVERVIEW);
        String testPlanSessionAttachment =
                EntityGraphQueryBuilder.buildParentAttribute(
                        TEST_PLANS, EXPLORATORY_SESSION_OVERVIEW, ATTACHMENT_LIST);

        List<Iteration> iterations =
                new EntityGraphQueryBuilder<>(em, Iteration.class, FIND_DISTINCT_ITERATIONS_BY_IDS)
                        .addAttributeNodes(CAMPAIGN, ATTACHMENT_LIST, TEST_PLANS)
                        .addSubGraph(ATTACHMENT_LIST, ATTACHMENTS)
                        .addSubgraphToSubgraph(ATTACHMENT_LIST, ATTACHMENTS, CONTENT)
                        .addSubGraph(TEST_PLANS, EXPLORATORY_SESSION_OVERVIEW)
                        .addSubgraphToSubgraph(TEST_PLANS, EXPLORATORY_SESSION_OVERVIEW, ATTACHMENT_LIST)
                        .addSubgraphToSubgraph(testPlanSession, ATTACHMENT_LIST, ATTACHMENTS)
                        .addSubgraphToSubgraph(testPlanSessionAttachment, ATTACHMENTS, CONTENT)
                        .executeDistinctList(Map.of(IDS, ids));

        String testSuiteAttachment =
                EntityGraphQueryBuilder.buildParentAttribute(TEST_SUITES, ATTACHMENT_LIST);
        String testSuiteTestPlan = EntityGraphQueryBuilder.buildParentAttribute(TEST_SUITES, TEST_PLAN);
        String testSuiteTestPlanSession =
                EntityGraphQueryBuilder.buildParentAttribute(
                        TEST_SUITES, TEST_PLAN, EXPLORATORY_SESSION_OVERVIEW);
        String testSuitetestPlanSessionAttachmentList =
                EntityGraphQueryBuilder.buildParentAttribute(
                        TEST_SUITES, TEST_PLAN, EXPLORATORY_SESSION_OVERVIEW, ATTACHMENT_LIST);
        new EntityGraphQueryBuilder<>(em, Iteration.class, FIND_DISTINCT_ITERATIONS_BY_IDS)
                .addAttributeNodes(TEST_SUITES)
                .addSubGraph(TEST_SUITES, TEST_PLAN, ATTACHMENT_LIST)
                .addSubgraphToSubgraph(TEST_SUITES, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubgraphToSubgraph(testSuiteAttachment, ATTACHMENTS, CONTENT)
                .addSubgraphToSubgraph(TEST_SUITES, TEST_PLAN, EXPLORATORY_SESSION_OVERVIEW)
                .addSubgraphToSubgraph(testSuiteTestPlan, EXPLORATORY_SESSION_OVERVIEW, ATTACHMENT_LIST)
                .addSubgraphToSubgraph(testSuiteTestPlanSession, ATTACHMENT_LIST, ATTACHMENTS)
                .addSubgraphToSubgraph(testSuitetestPlanSessionAttachmentList, ATTACHMENTS, CONTENT)
                .executeDistinctList(Map.of(IDS, ids));

        return iterations;
    }

    @Override
    public boolean hasDeletedTestCaseInTestPlan(long iterationId) {
        return em.createQuery("""
                    select count(i) > 0
                    from Iteration i
                    join i.testPlans tp
                    where i.id = :id and tp.referencedTestCase is null""", Boolean.class)
                .setParameter(ID, iterationId)
                .getSingleResult();
    }

    @Override
    public Iteration loadForExecutionResume(long iterationId) {
        return entityManager.createQuery("""
                 select it
                 from Iteration it
                 left join fetch it.testPlans tp
                 left join fetch tp.referencedTestCase tc
                 left join fetch tc.steps
                 left join fetch tp.exploratorySessionOverview
                 left join fetch tp.executions exec
                 left join fetch exec.steps
                 left join fetch exec.automatedExecutionExtender
                 where it.id = :id""", Iteration.class)
            .setParameter(ID, iterationId)
            .getSingleResult();
    }
}
