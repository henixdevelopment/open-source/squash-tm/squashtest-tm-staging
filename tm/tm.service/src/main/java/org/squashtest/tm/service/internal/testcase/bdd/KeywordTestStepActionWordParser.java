/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.exception.actionword.InvalidTestCaseParameterNameInActionException;

/**
 * @author qtran - created on 24/04/2020
 */
public class KeywordTestStepActionWordParser extends AbstractActionWordParser {

    public ActionWord createActionWordFromKeywordTestStep(String input) {
        final List<ActionWordTokenizer.AWToken> tokens = checkInputAndParseTokens(input, true);

        for (ActionWordTokenizer.AWToken token : tokens) {
            switch (token.getType()) {
                case PLAIN_TEXT -> addTextFragment(token);
                case ANGLED_VALUE -> addNamedParameter(token);
                case QUOTED_VALUE -> addFreeValue(token);
                case NUMBER -> addNumberFragment(token);
            }
        }

        return new ActionWord(fragments);
    }

    private void addNamedParameter(ActionWordTokenizer.AWToken token) {
        final String testCaseParameterName = canonicalizeTestCaseParameterName(token.getLexeme());
        addParameter(getNextParamName(), "", testCaseParameterName);
    }

    private String canonicalizeTestCaseParameterName(String candidateName) {
        // Remove opening and closing tags
        if (candidateName.startsWith("<") && candidateName.endsWith(">")) {
            // Less than 3 chars is illegal as '<' and '>' are included
            if (candidateName.length() < 3) {
                throw new InvalidTestCaseParameterNameInActionException(
                        "Test case parameter name cannot be blank.");
            }

            candidateName = candidateName.substring(1, candidateName.length() - 1);
        }

        if (StringUtils.isBlank(candidateName)) {
            throw new InvalidTestCaseParameterNameInActionException(
                    "Test case parameter name cannot be blank.");
        }

        // Replace successive whitespaces with 1 underscore
        candidateName = candidateName.trim().replaceAll("(\\s)+", "_");

        if (!isLegalTestCaseParameterName(candidateName)) {
            throw new InvalidTestCaseParameterNameInActionException(
                    "Test case parameter name can contain only alphanumeric, - and _ characters.");
        }

        // Replace special chars with underscores
        candidateName = candidateName.replaceAll("[^\\w-]", "_");

        // Re-add the delimiters
        return "<" + candidateName + ">";
    }

    private boolean isLegalTestCaseParameterName(String candidate) {
        return candidate.matches("[a-zA-Z0-9\\-_\\s]*");
    }

    private void addFreeValue(ActionWordTokenizer.AWToken token) {
        final String value = token.getLexeme().substring(1, token.getLexeme().length() - 1);
        addParameter(getNextParamName(), value, value);
    }
}
