/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import org.squashtest.tm.core.foundation.collection.ColumnFiltering;
import org.squashtest.tm.core.foundation.collection.PagingAndMultiSorting;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.service.internal.dto.AutomatedSuiteDto;
import org.squashtest.tm.service.testautomation.AutomationDeletionCount;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview.SquashAutomProjectPreview;

public interface AutomatedSuiteDao {

    void delete(AutomatedSuite suite);

    void delete(String id);

    AutomatedSuite createNewSuite();

    AutomatedSuite createNewSuite(Iteration iteration);

    AutomatedSuite createNewSuite(TestSuite testSuite);

    AutomatedSuite findById(String id);

    List<AutomatedSuite> findAll();

    List<AutomatedSuite> findAllByIds(final Collection<String> ids);

    /**
     * Returns the list of TestAutomationProject that would run the automated tests of a given test
     * plan, paired with the number of such tests. The test plan undef consideration is defined by a
     * context (an entity that owns the test plan), which we can further restrict to a given list of
     * item ids (this is optional).
     *
     * @param context : a reference to a TestSuite or an Iteration. A reference to any other entity
     *     will be considered as an error.
     * @param testPlanSubset : optional list of item ids if you need to restrict the test plan. If
     *     null or empty, the parameter is ignored.
     * @throws IllegalArgumentException : if the context is invalid.
     * @return what is described above.
     */
    List<Couple<TestAutomationProject, Long>> findAllCalledByTestPlan(
            EntityReference context, Collection<Long> testPlanSubset, @Nullable Long userId);

    /**
     * Returns the list of test paths for a test plan (optionally restricted), and for a given test
     * automation project. see {@link #findAllCalledByTestPlan(EntityReference, Collection)} for more
     * details of the test plan definition.
     *
     * @param context
     * @param testPlanSubset
     * @param automationProjectId
     * @return
     */
    List<String> findTestPathForAutomatedSuiteAndProject(
            EntityReference context,
            Collection<Long> testPlanSubset,
            long automationProjectId,
            @Nullable Long userId);

    /**
     * retrieve all the {@link AutomatedExecutionExtender} that this suite is bound to.
     *
     * @param suiteId
     * @return
     */
    Collection<AutomatedExecutionExtender> findAllExtenders(String suiteId);

    /**
     * retrieve all the extenders of executions currently waiting to be run by their test automation
     * servers, for a given {@link AutomatedSuite}
     *
     * @param suiteId
     * @return
     */
    Collection<AutomatedExecutionExtender> findAllWaitingExtenders(String suiteId);

    /**
     * retrieve all the extenders of executions currently being run by their test automation servers,
     * for a given {@link AutomatedSuite}
     *
     * @param suiteId
     * @return
     */
    Collection<AutomatedExecutionExtender> findAllRunningExtenders(String suiteId);

    /**
     * retrieve all the extenders of executions which had been ran their test automation servers, for
     * a given {@link AutomatedSuite}
     *
     * @param suiteId
     * @return
     */
    Collection<AutomatedExecutionExtender> findAllCompletedExtenders(String suiteId);

    /**
     * retrieve all the extenders of executions which status is one of the supplied status, for a
     * given {@link AutomatedSuite}
     *
     * @param suiteId
     * @return
     */
    Collection<AutomatedExecutionExtender> findAllExtendersByStatus(
            String suiteId, Collection<ExecutionStatus> statusList);

    List<AutomatedExecutionExtender> findAndFetchForAutomatedExecutionCreation(String id);

    /**
     * Retrieves all the automated suites related to an {@link Iteration}.
     *
     * @param iterationId
     * @param paging
     * @param filter
     * @return
     */
    List<AutomatedSuiteDto> findAutomatedSuitesByIterationID(
            Long iterationId, PagingAndMultiSorting paging, ColumnFiltering filter);

    /**
     * Counts the number of {@link AutomatedSuite} related to an {@link Iteration}
     *
     * @param iterationId
     * @param filter
     * @return
     */
    long countSuitesByIterationId(Long iterationId, ColumnFiltering filter);

    /**
     * Retrieves all the automated suites related to a {@link
     * org.squashtest.tm.domain.campaign.TestSuite}.
     *
     * @param suiteId
     * @param paging
     * @param filter
     * @return
     */
    List<AutomatedSuiteDto> findAutomatedSuitesByTestSuiteID(
            Long suiteId, PagingAndMultiSorting paging, ColumnFiltering filter);

    /**
     * Counts the number of {@link AutomatedSuite} related to a {@link
     * org.squashtest.tm.domain.campaign.TestSuite}
     *
     * @param suiteId
     * @param filter
     * @return
     */
    long countSuitesByTestSuiteId(Long suiteId, ColumnFiltering filter);

    ExecutionStatusReport getStatusReport(String uuid);

    /**
     * Get all ids of old AutomatedSuites which must be deleted. An old AutomatedSuite is an
     * AutomatedSuite which lifetime is greater than the parameter automatedSuitesLifetime configured
     * in its Project.
     */
    List<String> getOldAutomatedSuiteIds();

    void deleteAllByIds(List<String> automatedExecutionIds);

    AutomationDeletionCount countOldAutomatedSuitesAndExecutions();

    AutomationDeletionCount countOldAutomatedSuitesAndExecutionsByProjectId(Long id);

    List<String> getOldAutomatedSuiteIdsByProjectId(Long id);

    boolean itpiSelectionContainsSquashAutomTest(
            EntityReference context, List<Long> testPlanSubsetIds);

    /**
     * Given an {@link EntityReference} and an optional PartialTestPlan, retrieve all {@link
     * SquashAutomProjectPreview}s implied. They are used to displaying information in Automated tests
     * execution popup, in 'select execution server' step.
     *
     * @param context the entity which holds the test plan
     * @param testPlanSubsetIds a partial test plan when only selected tests are to run
     * @param userId present only if the user is a tester not assigned to the test in order to filter
     *     these in order to filter these
     * @return a Map of all found {@link SquashAutomProjectPreview}s mapped by the Project id
     */
    Map<Long, SquashAutomProjectPreview> findAllSquashAutomProjectPreviews(
            EntityReference context, List<Long> testPlanSubsetIds, @Nullable Long userId);

    List<Long> findAttachmentListIdsByIds(List<String> automatedSuiteIds);

    AutomatedSuiteWorkflow getAutomatedSuiteWorkflowByWorkflowId(String workflowId);

    Map<String, List<AutomatedSuiteWorkflow>> getAutomatedSuiteWorkflowsBySuiteIds(
            List<String> suiteIds);

    void deleteAutomatedSuiteWorkflowsBySuiteIds(List<String> suiteIds);

    Map<Long, Long> getAutomatedExecutionIdsWithAttachmentListIds(
            List<String> automatedSuiteIds, boolean complete);

    AutomatedSuite createNewSuiteWithLinkToTestSuite(TestSuite testSuite);

    AutomatedSuite createNewSuiteWithLinkToIteration(Iteration iteration);

    AutomatedSuite findByIdWithExtenders(String id);

    boolean isAutomatedSuiteCreatedByTestAutomationServerUser(String automatedSuiteId);
}
