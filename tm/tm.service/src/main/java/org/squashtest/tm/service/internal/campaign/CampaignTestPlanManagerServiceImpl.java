/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.ObjectIdentityRetrievalStrategy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.IdentifiersOrderComparator;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.CampaignTestPlanItemDao;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.testcase.TestCaseNodeWalker;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;

@Service("squashtest.tm.service.CampaignTestPlanManagerService")
@Transactional
public class CampaignTestPlanManagerServiceImpl implements CampaignTestPlanManagerService {
    /** Permission string for reading returned object. */
    private static final String CAN_READ_RETURNED_OBJECT =
            "hasPermission(returnObject, 'READ')" + OR_HAS_ROLE_ADMIN;

    /** Permission string for linking campaigns to TP / Users based on campaignId param. */
    private static final String CAN_LINK_CAMPAIGN_BY_ID =
            "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'LINK')"
                    + OR_HAS_ROLE_ADMIN;

    @Inject private CampaignDao campaignDao;

    @Inject
    @Qualifier("squashtest.tm.repository.TestCaseLibraryNodeDao")
    private LibraryNodeDao<TestCaseLibraryNode> testCaseLibraryNodeDao;

    @Inject private ObjectAclService aclService;

    @Inject private CampaignTestPlanItemDao campaignTestPlanItemDao;

    @Inject private UserDao userDao;

    @Inject private DatasetDao datasetDao;

    @Inject
    @Qualifier("squashtest.core.security.ObjectIdentityRetrievalStrategy")
    private ObjectIdentityRetrievalStrategy objIdRetrievalStrategy;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Override
    @PreAuthorize(CAN_LINK_CAMPAIGN_BY_ID)
    @PreventConcurrent(entityType = Campaign.class)
    @CheckBlockingMilestone(entityType = Campaign.class)
    public Map<String, Object> addTestCasesToCampaignTestPlan(
            final List<Long> testCasesIds, @Id long campaignId) {
        return addTestCasesToCampaignTestPlanUnsecured(testCasesIds, campaignId);
    }

    @Override
    public Map<String, Object> addTestCasesToCampaignTestPlanUnsecured(
            final List<Long> testCasesIds, @Id long campaignId) {
        boolean hasDataset = false;

        // nodes are returned unsorted
        List<TestCaseLibraryNode> nodes = testCaseLibraryNodeDao.findAllByIds(testCasesIds);

        // now we resort them according to the order in which the testcaseids were given
        IdentifiersOrderComparator comparator = new IdentifiersOrderComparator(testCasesIds);
        nodes.sort(comparator);

        List<TestCase> testCases = new TestCaseNodeWalker().walk(nodes);

        final Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();

        activeMilestone.ifPresent(
                milestone ->
                        CollectionUtils.filter(
                                testCases,
                                new Predicate() {

                                    @Override
                                    public boolean evaluate(Object tc) {
                                        return ((TestCase) tc).getAllMilestones().contains(milestone);
                                    }
                                }));

        Campaign campaign = campaignDao.findById(campaignId);

        /*
         * Feat 3700 campaign test plans are now populated the same way than iteration
         * are
         */
        long totalNewTestPlanItemsNumber = 0;
        for (TestCase testCase : testCases) {

            Set<Dataset> datasets = testCase.getDatasets();

            if (datasets.isEmpty()) {
                CampaignTestPlanItem itp = new CampaignTestPlanItem(testCase);
                campaign.addToTestPlan(itp);
                campaignTestPlanItemDao.persist(itp);
                totalNewTestPlanItemsNumber++;
            } else {
                hasDataset = true;
                for (Dataset ds : datasets) {
                    CampaignTestPlanItem itp = new CampaignTestPlanItem(testCase, ds);
                    campaign.addToTestPlan(itp);
                    campaignTestPlanItemDao.persist(itp);
                    totalNewTestPlanItemsNumber++;
                }
            }
        }
        Map<String, Object> response = new HashMap<>();
        response.put("hasDataSet", hasDataset);
        response.put("totalNewTestPlanItemsNumber", totalNewTestPlanItemsNumber);
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAssignableUserForTestPlan(long campaignId) {

        Campaign campaign = campaignDao.findById(campaignId);

        List<ObjectIdentity> entityRefs = new ArrayList<>();
        ObjectIdentity oid = objIdRetrievalStrategy.getObjectIdentity(campaign);
        entityRefs.add(oid);

        List<String> loginList = aclService.findUsersWithExecutePermission(entityRefs);
        return userDao.findUsersByLoginList(loginList);
    }

    /**
     * @param campaignId not necessary but actually used for security check
     */
    @PreAuthorize(CAN_LINK_CAMPAIGN_BY_ID)
    @CheckBlockingMilestone(entityType = Campaign.class)
    public void assignUserToTestPlanItems(
            @NotNull List<Long> itemsIds, @Id long campaignId, long userId) {
        User assignee = null;
        if (userId != 0) {
            assignee = userDao.getReferenceById(userId);
        }

        List<CampaignTestPlanItem> items = campaignTestPlanItemDao.findAllByIds(itemsIds);

        for (CampaignTestPlanItem item : items) {
            item.setUser(assignee);
        }
    }

    /**
     * @see CampaignTestPlanManagerService#moveTestPlanItems(long, int, List)
     */
    @Override
    @PreAuthorize(CAN_LINK_CAMPAIGN_BY_ID)
    @PreventConcurrent(entityType = Campaign.class)
    @CheckBlockingMilestone(entityType = Campaign.class)
    public void moveTestPlanItems(@Id long campaignId, int targetIndex, List<Long> itemIds) {
        Campaign campaign = campaignDao.findById(campaignId);
        campaign.moveTestPlanItems(targetIndex, itemIds);
    }

    /**
     * @see CampaignTestPlanManagerService#removeTestPlanItem(long, long)
     */
    @Override
    @PreAuthorize(CAN_LINK_CAMPAIGN_BY_ID)
    @PreventConcurrent(entityType = Campaign.class)
    @CheckBlockingMilestone(entityType = Campaign.class)
    public void removeTestPlanItem(@Id long campaignId, long itemId) {
        Campaign campaign = campaignDao.findById(campaignId);
        campaign.removeTestPlanItem(itemId);
    }

    /**
     * @see CampaignTestPlanManagerService#removeTestPlanItems(long, List)
     */
    @Override
    @PreAuthorize(CAN_LINK_CAMPAIGN_BY_ID)
    @PreventConcurrent(entityType = Campaign.class)
    @CheckBlockingMilestone(entityType = Campaign.class)
    public void removeTestPlanItems(@Id long campaignId, List<Long> itemIds) {
        Campaign campaign = campaignDao.findById(campaignId);
        campaign.removeTestPlanItems(itemIds);
    }

    /**
     * @see CampaignTestPlanManagerService#findById(long)
     */
    @Override
    @Transactional(readOnly = true)
    @PostAuthorize(CAN_READ_RETURNED_OBJECT)
    public CampaignTestPlanItem findById(long itemId) {
        return campaignTestPlanItemDao.findById(itemId);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#itemId, 'org.squashtest.tm.domain.campaign.CampaignTestPlanItem', 'WRITE')"
                    + OR_HAS_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = CampaignTestPlanItem.class)
    public void changeDataset(@Id long itemId, Long datasetId) {
        CampaignTestPlanItem item = campaignTestPlanItemDao.findById(itemId);

        if (datasetId == null) {
            item.setReferencedDataset(null);
        } else {
            TestCase tc = item.getReferencedTestCase();
            Dataset ds = datasetDao.getReferenceById(datasetId);
            if (!ds.getTestCase().equals(tc)) {
                throw new IllegalArgumentException(
                        "dataset [id:'"
                                + ds.getId()
                                + "', name:'"
                                + ds.getName()
                                + "'] doesn't belong to test case [id:'"
                                + tc.getId()
                                + "', name:'"
                                + tc.getName()
                                + "']");
            }
            item.setReferencedDataset(ds);
        }
    }

    @Override
    public void removeTestPlanItemsAssignments(List<Long> itemIds) {
        // check permission
        PermissionsUtils.checkPermission(
                permissionEvaluationService, itemIds, "WRITE", CampaignTestPlanItem.class.getName());
        List<CampaignTestPlanItem> items = campaignTestPlanItemDao.findAllByIds(itemIds);

        for (CampaignTestPlanItem item : items) {
            if (!item.isTestCaseDeleted()) {
                item.setUser(null);
            }
        }
    }

    @Override
    public void assignUserToTestPlanItems(List<Long> itemIds, Long userId) {
        // check permission
        PermissionsUtils.checkPermission(
                permissionEvaluationService, itemIds, "WRITE", CampaignTestPlanItem.class.getName());
        List<CampaignTestPlanItem> items = campaignTestPlanItemDao.findAllByIds(itemIds);

        User user = userId == 0 ? null : userDao.getReferenceById(userId);

        for (CampaignTestPlanItem item : items) {
            if (!item.isTestCaseDeleted()) {
                item.setUser(user);
            }
        }
    }
}
