/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.internal.testautomation.model.IterationTestPlanItemWithCustomFields;

public class BuildDef {
    private final TestAutomationServer server;
    private final TokenAuthCredentials credentials;
    private final String namespace;
    private final Collection<IterationTestPlanItemWithCustomFields> parameterizedItems;

    public BuildDef(
            @NotNull TestAutomationServer server,
            TokenAuthCredentials credentials,
            String namespace,
            @NotNull List<IterationTestPlanItemWithCustomFields> parameterizedItems) {
        super();
        this.server = server;
        this.namespace = namespace;
        this.parameterizedItems = Collections.unmodifiableList(parameterizedItems);
        this.credentials = credentials;
    }

    public TestAutomationServer getServer() {
        return server;
    }

    public TokenAuthCredentials getCredentials() {
        return credentials;
    }

    public String getNamespace() {
        return namespace;
    }

    public Collection<IterationTestPlanItemWithCustomFields> getParameterizedItems() {
        return parameterizedItems;
    }

    public Long getProjectId() {
        return parameterizedItems.stream()
                .findFirst()
                .map(item -> item.getIterationTestPlanItem().getReferencedTestCase().getProject().getId())
                .orElse(null);
    }
}
