/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.lower;
import static org.squashtest.tm.domain.actionword.ActionWordNodeType.ACTION_WORD_NAME;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.AWLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ACTION_WORD_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KEYWORD_TEST_STEP_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.actionword.ActionWordTreeDefinition;
import org.squashtest.tm.domain.actionword.ActionWordTreeEntity;
import org.squashtest.tm.domain.actionword.GetActionWordTreeDefinitionVisitor;
import org.squashtest.tm.service.internal.display.dto.actionword.ActionWordPreview;
import org.squashtest.tm.service.internal.repository.CustomActionWordLibraryNodeDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

public class ActionWordLibraryNodeDaoImpl implements CustomActionWordLibraryNodeDao {

    @PersistenceContext EntityManager em;

    @Inject private DSLContext dsl;

    @Override
    public ActionWordLibraryNode findNodeFromEntity(ActionWordTreeEntity actionWordTreeEntity) {
        GetActionWordTreeDefinitionVisitor visitor = new GetActionWordTreeDefinitionVisitor();
        actionWordTreeEntity.accept(visitor);
        Query query = em.createNamedQuery("ActionWordLibraryNode.findNodeFromEntity");
        query.setParameter("entityType", visitor.getActionWordTreeDefinition());
        query.setParameter("entityId", actionWordTreeEntity.getId());
        return (ActionWordLibraryNode) query.getSingleResult();
    }

    @Override
    public List<Long> findAllFirstLevelDescendantIds(Long nodeId) {
        List<Long> ids = new ArrayList<>();
        ids.add(nodeId);
        return findAllFirstLevelDescendantIds(ids);
    }

    @Override
    public List<Long> findAllFirstLevelDescendantIds(List<Long> nodesIds) {
        Query query =
                em.createNamedQuery("ActionWordLibraryNodePathEdge.findAllFirstLevelDescendantIds");
        query.setParameter("ids", nodesIds);
        return query.getResultList();
    }

    @Override
    public List<Long> findAllCanBeCopiedNodesInTarget(
            List<ActionWordLibraryNode> copiedNodes,
            ActionWordLibraryNode target,
            boolean caseInsensitivity) {
        final Field<String> actionWordTokenField;
        if (caseInsensitivity) {
            actionWordTokenField = lower(ACTION_WORD.TOKEN);
        } else {
            actionWordTokenField = ACTION_WORD.TOKEN;
        }
        Long targetId = target.getId();
        List<Long> nodeIds = copiedNodes.stream().map(ActionWordLibraryNode::getId).toList();
        if (!nodeIds.isEmpty() && targetId != null) {
            return dsl.selectDistinct(ACTION_WORD_LIBRARY_NODE.AWLN_ID)
                    .from(ACTION_WORD)
                    .join(ACTION_WORD_LIBRARY_NODE)
                    .on(
                            ACTION_WORD
                                    .ACTION_WORD_ID
                                    .eq(ACTION_WORD_LIBRARY_NODE.ENTITY_ID)
                                    .and(ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE.eq(ACTION_WORD_NAME)))
                    .where(
                            (ACTION_WORD_LIBRARY_NODE.AWLN_ID.in(nodeIds))
                                    .and(
                                            actionWordTokenField.notIn(
                                                    dsl.selectDistinct(actionWordTokenField)
                                                            .from(AWLN_RELATIONSHIP_CLOSURE)
                                                            .join(ACTION_WORD_LIBRARY_NODE)
                                                            .on(
                                                                    AWLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(
                                                                            ACTION_WORD_LIBRARY_NODE.AWLN_ID))
                                                            .join(ACTION_WORD)
                                                            .on(
                                                                    ACTION_WORD_LIBRARY_NODE
                                                                            .ENTITY_ID
                                                                            .eq(ACTION_WORD.ACTION_WORD_ID)
                                                                            .and(
                                                                                    ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE.eq(
                                                                                            ACTION_WORD_NAME)))
                                                            .where(
                                                                    AWLN_RELATIONSHIP_CLOSURE
                                                                            .ANCESTOR_ID
                                                                            .eq(targetId)
                                                                            .and(AWLN_RELATIONSHIP_CLOSURE.DEPTH.eq((short) 1))))))
                    .fetch()
                    .into(Long.class);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Long> getActionWordLibraryNodeIdsWithoutSteps(List<Long> ids) {
        return dsl.select(ACTION_WORD_LIBRARY_NODE.AWLN_ID)
                .from(ACTION_WORD_LIBRARY_NODE)
                .leftJoin(KEYWORD_TEST_STEP)
                .on(ACTION_WORD_LIBRARY_NODE.ENTITY_ID.eq(KEYWORD_TEST_STEP.ACTION_WORD_ID))
                .where(KEYWORD_TEST_STEP.ACTION_WORD_ID.isNull())
                .and(ACTION_WORD_LIBRARY_NODE.AWLN_ID.in(ids))
                .fetchInto(Long.class);
    }

    @Override
    public List<ActionWordPreview> fetchActionWordPreviewDtos(List<Long> nodeIds) {
        return dsl.select(
                        ACTION_WORD_LIBRARY_NODE.ENTITY_ID.as(ACTION_WORD_ID),
                        ACTION_WORD_LIBRARY_NODE.NAME.as(RequestAliasesConstants.ACTION_WORD_NAME),
                        PROJECT.NAME.as(PROJECT_NAME),
                        count(KEYWORD_TEST_STEP.TEST_STEP_ID).as(KEYWORD_TEST_STEP_COUNT))
                .from(ACTION_WORD_LIBRARY_NODE)
                .join(PROJECT)
                .on(ACTION_WORD_LIBRARY_NODE.AWL_ID.eq(PROJECT.AWL_ID))
                .leftJoin(KEYWORD_TEST_STEP)
                .on(ACTION_WORD_LIBRARY_NODE.ENTITY_ID.eq(KEYWORD_TEST_STEP.ACTION_WORD_ID))
                .where(ACTION_WORD_LIBRARY_NODE.AWLN_ID.in(nodeIds))
                .and(ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE.eq(ActionWordTreeDefinition.ACTION_WORD.name()))
                .groupBy(ACTION_WORD_LIBRARY_NODE.AWLN_ID, PROJECT.PROJECT_ID)
                .orderBy(PROJECT.NAME, ACTION_WORD_LIBRARY_NODE.NAME)
                .fetchInto(ActionWordPreview.class);
    }
}
