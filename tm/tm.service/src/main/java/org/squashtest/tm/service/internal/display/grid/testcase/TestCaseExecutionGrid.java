/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.testcase;

import static org.jooq.impl.DSL.coalesce;
import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CLN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_LABEL;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ORDER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_PATH;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXEC_PLAN_TYPE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NB_ISSUES;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TCLN_ID;

import java.util.Arrays;
import java.util.List;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.SelectLimitStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.jooq.domain.tables.ClnRelationshipClosure;
import org.squashtest.tm.service.internal.display.execution.ExecPlanType;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

public class TestCaseExecutionGrid extends AbstractGrid {

    private static final String EXEC_ID = "EXEC_ID";

    private static final String CLN_FULL_PATH = "CLN_FULL_PATH";

    private static final String CLN_PATH = "CLN_PATH";

    private static final String PATH_SEPARATOR = " > ";

    private final Long testCaseId;

    public TestCaseExecutionGrid(Long testCaseId) {
        this.testCaseId = testCaseId;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(EXEC_ID).as(EXECUTION_ID)),
                new GridColumn(DSL.field(DATASET_LABEL)),
                new GridColumn(DSL.field(EXECUTION_MODE).as(INFERRED_EXECUTION_MODE)),
                new LevelEnumColumn(ExecutionStatus.class, DSL.field(EXECUTION_STATUS, String.class)),
                new GridColumn(DSL.field(LAST_EXECUTED_BY)),
                new GridColumn(DSL.field(LAST_EXECUTED_ON)),
                new GridColumn(DSL.field(EXECUTION_ORDER)),
                new GridColumn(generateFieldIssues()),
                new GridColumn(DSL.field(EXEC_PLAN_TYPE)),
                new GridColumn(DSL.field(EXECUTION_PATH)),
                new GridColumn(DSL.field(ASSIGNEE_ID)),
                new GridColumn(DSL.field(PROJECT_ID)));
    }

    private Field<Integer> generateFieldIssues() {
        return DSL.select(DSL.countDistinct(ISSUE.REMOTE_ISSUE_ID))
                .from(EXECUTION_ISSUES_CLOSURE)
                .leftJoin(ISSUE)
                .on(EXECUTION_ISSUES_CLOSURE.ISSUE_ID.eq(ISSUE.ISSUE_ID))
                .where(DSL.field(EXEC_ID).eq(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID))
                .asField(NB_ISSUES);
    }

    @Override
    protected Table<?> getTable() {
        var clnPathTable = getClnPathTable();

        return DSL.select(
                        EXECUTION.EXECUTION_ID.as(
                                EXEC_ID), // Alias is used to avoid ambiguity in generateFieldIssues(),
                        EXECUTION.TCLN_ID,
                        EXECUTION.DATASET_LABEL,
                        EXECUTION.EXECUTION_MODE,
                        EXECUTION.EXECUTION_STATUS,
                        EXECUTION.LAST_EXECUTED_BY,
                        EXECUTION.LAST_EXECUTED_ON,
                        ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.as(EXECUTION_ORDER),
                        when(TEST_SUITE.NAME.isNotNull(), DSL.inline(ExecPlanType.TEST_SUITE.name()))
                                .otherwise(DSL.inline(ExecPlanType.ITERATION.name()))
                                .as(EXEC_PLAN_TYPE),
                        concat(
                                        clnPathTable.field(CLN_FULL_PATH, String.class),
                                        DSL.val(PATH_SEPARATOR),
                                        ITERATION.NAME,
                                        coalesce(concat(PATH_SEPARATOR, TEST_SUITE.NAME), ""))
                                .as(EXECUTION_PATH),
                        ITERATION_TEST_PLAN_ITEM.USER_ID.as(ASSIGNEE_ID),
                        PROJECT.PROJECT_ID.as(PROJECT_ID))
                .from(EXECUTION)
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                .on(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(ITERATION)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                .leftJoin(TEST_SUITE)
                .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
                .leftJoin(clnPathTable)
                .on(clnPathTable.field(CLN_ID, Long.class).eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .union(
                        DSL.select(
                                        EXECUTION.EXECUTION_ID.as(
                                                EXEC_ID), // Alias is used to avoid ambiguity in generateFieldIssues(),
                                        EXECUTION.TCLN_ID,
                                        EXECUTION.DATASET_LABEL,
                                        EXECUTION.EXECUTION_MODE,
                                        EXECUTION.EXECUTION_STATUS,
                                        EXECUTION.LAST_EXECUTED_BY,
                                        EXECUTION.LAST_EXECUTED_ON,
                                        EXECUTION.EXECUTION_ORDER.as(EXECUTION_ORDER),
                                        DSL.val(ExecPlanType.SPRINT.name()).as(EXEC_PLAN_TYPE),
                                        clnPathTable.field(CLN_FULL_PATH, String.class).as(EXECUTION_PATH),
                                        TEST_PLAN_ITEM.ASSIGNEE_ID.as(ASSIGNEE_ID),
                                        clnPathTable.field(PROJECT_ID, Long.class).as(PROJECT_ID))
                                .from(EXECUTION)
                                .innerJoin(TEST_PLAN_ITEM)
                                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                                .innerJoin(SPRINT_REQ_VERSION)
                                .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                                .innerJoin(SPRINT)
                                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                                .leftJoin(clnPathTable)
                                .on(clnPathTable.field(CLN_ID, Long.class).eq(SPRINT.CLN_ID)))
                .asTable();
    }

    private Table<?> getClnPathTable() {
        short zero = 0;

        ClnRelationshipClosure descendant = CLN_RELATIONSHIP_CLOSURE.as("descendant");

        Table<Record4<Long, String, Long, String>> path =
                DSL.select(
                                PROJECT.PROJECT_ID.as(PROJECT_ID),
                                PROJECT.NAME,
                                CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.as(CLN_ID),
                                DSL.groupConcat(CAMPAIGN_LIBRARY_NODE.NAME)
                                        .orderBy(CLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                        .separator(PATH_SEPARATOR)
                                        .as(CLN_PATH))
                        .from(CLN_RELATIONSHIP_CLOSURE)
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .innerJoin(PROJECT)
                        .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                        .innerJoin(descendant)
                        .on(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(descendant.ANCESTOR_ID))
                        .leftJoin(CAMPAIGN_FOLDER)
                        .on(
                                descendant
                                        .DESCENDANT_ID
                                        .eq(CAMPAIGN_FOLDER.CLN_ID)
                                        .and(CLN_RELATIONSHIP_CLOSURE.DEPTH.ne(zero)))
                        .where(CAMPAIGN_FOLDER.CLN_ID.isNull())
                        .groupBy(PROJECT.PROJECT_ID, CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID)
                        .asTable("CLN_PATH_TABLE", PROJECT_ID, PROJECT_NAME, CLN_ID, CLN_PATH);

        return DSL.select(
                        path.field(CLN_ID),
                        path.field(PROJECT_ID),
                        DSL.concat(
                                        path.field(PROJECT_NAME),
                                        DSL.value(PATH_SEPARATOR),
                                        path.field(CLN_PATH),
                                        when(CAMPAIGN_FOLDER.CLN_ID.isNotNull(), DSL.value(PATH_SEPARATOR))
                                                .otherwise(""))
                                .as(CLN_FULL_PATH))
                .from(path)
                .leftJoin(CAMPAIGN_FOLDER)
                .on(path.field(CLN_ID, Long.class).eq(CAMPAIGN_FOLDER.CLN_ID))
                .where(path.field(CLN_ID, Long.class).in(getClnIdsContainingTestCaseExecutions()))
                .asTable();
    }

    private SelectLimitStep<Record1<Long>> getClnIdsContainingTestCaseExecutions() {
        return DSL.selectDistinct(CAMPAIGN_LIBRARY_NODE.CLN_ID)
                .from(EXECUTION)
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .innerJoin(ITERATION_TEST_PLAN_ITEM)
                .on(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(ITERATION)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(EXECUTION.TCLN_ID.eq(testCaseId))
                .union(
                        DSL.selectDistinct(CAMPAIGN_LIBRARY_NODE.CLN_ID)
                                .from(EXECUTION)
                                .innerJoin(TEST_PLAN_ITEM)
                                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                                .innerJoin(SPRINT_REQ_VERSION)
                                .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                                .innerJoin(SPRINT)
                                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                                .on(SPRINT.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                .where(EXECUTION.TCLN_ID.eq(testCaseId)));
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(EXECUTION_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return DSL.field(PROJECT_ID);
    }

    @Override
    protected Condition craftInvariantFilter() {
        return DSL.field(TCLN_ID).eq(testCaseId);
    }
}
