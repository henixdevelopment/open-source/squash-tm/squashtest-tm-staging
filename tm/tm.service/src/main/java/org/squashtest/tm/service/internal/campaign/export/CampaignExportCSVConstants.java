/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign.export;

public final class CampaignExportCSVConstants {

    static final String HEADER_CPG_SCHEDULED_START_ON = "CPG_SCHEDULED_START_ON";
    static final String HEADER_CPG_SCHEDULED_END_ON = "CPG_SCHEDULED_END_ON";
    static final String HEADER_CPG_ACTUAL_START_ON = "CPG_ACTUAL_START_ON";
    static final String HEADER_CPG_ACTUAL_END_ON = "CPG_ACTUAL_END_ON";

    // Only used in Standard (S) export, concatenation "#" + IT_NUM + IT_NAME
    static final String HEADER_ITERATION = "ITERATION";
    // The 3 headers below are only used in Light (L) and Full (F) export
    static final String HEADER_IT_ID = "IT_ID";
    static final String HEADER_IT_NUM = "IT_NUM";
    static final String HEADER_IT_NAME = "IT_NAME";
    // Only in milestone mode, but this is the campaign milestone, this header is named wrong
    // TODO: RENAME
    static final String HEADER_IT_MILESTONE = "IT_MILESTONE";

    static final String HEADER_IT_SCHEDULED_START_ON = "IT_SCHEDULED_START_ON";
    static final String HEADER_IT_SCHEDULED_END_ON = "IT_SCHEDULED_END_ON";
    static final String HEADER_IT_ACTUAL_START_ON = "IT_ACTUAL_START_ON";
    static final String HEADER_IT_ACTUAL_END_ON = "IT_ACTUAL_END_ON";
    // Only used in Standard (S) export, name of the test case
    static final String HEADER_TEST_CASE = "TEST_CASE";
    // The 2 headers below are only used in Light (L) and Full (F) export
    static final String HEADER_TC_ID = "TC_ID";
    static final String HEADER_TC_NAME = "TC_NAME";

    // Only used in Full (F) export
    static final String HEADER_TC_PROJECT_ID = "TC_PROJECT_ID";
    // Name of the project
    static final String HEADER_TC_PROJECT = "TC_PROJECT";
    static final String HEADER_TC_MILESTONE = "TC_MILESTONE";
    static final String HEADER_TC_WEIGHT = "TC_WEIGHT";
    // Name of the execution's test suite
    static final String HEADER_TEST_SUITE = "TEST_SUITE";
    // Amount of executions
    static final String HEADER_HASH_EXECUTIONS = "#_EXECUTIONS";
    // Amount of associated requirements
    static final String HEADER_HASH_REQUIREMENTS = "#_REQUIREMENTS";
    // Amount of declared issues
    static final String HEADER_HASH_ISSUES = "#_ISSUES";
    // Name of the dataset
    static final String HEADER_DATASET = "DATASET";

    static final String HEADER_EXEC_STATUS = "EXEC_STATUS";
    static final String HEADER_EXEC_USER = "EXEC_USER";
    // Only used in Standard (S) export
    static final String HEADER_EXEC_SUCCESS_RATE = "EXEC_SUCCESS_RATE";
    static final String HEADER_EXECUTION_DATE = "EXECUTION_DATE";
    // Only used in Standard (S) export
    static final String HEADER_DESCRIPTION = "DESCRIPTION";

    static final String HEADER_TC_REF = "TC_REF";
    static final String HEADER_TC_NATURE = "TC_NATURE";
    static final String HEADER_TC_TYPE = "TC_TYPE";
    static final String HEADER_TC_STATUS = "TC_STATUS";
    // Only used in Standard (S) export
    static final String HEADER_PREREQUISITE = "PREREQUISITE";

    // The steps are only exported in Full (F) export
    static final String HEADER_STEP_ID = "STEP_ID";
    static final String HEADER_STEP_NUM = "STEP_NUM";
    static final String HEADER_STEP_REQ = "STEP_#_REQ";
    static final String HEADER_EXEC_STEP_STATUS = "EXEC_STEP_STATUS";
    static final String HEADER_EXEC_STEP_DATE = "EXEC_STEP_DATE";
    static final String HEADER_EXEC_STEP_USER = "EXEC_STEP_USER";
    static final String HEADER_EXEC_STEP_ISSUES = "EXEC_STEP_#_ISSUES";
    static final String HEADER_EXEC_STEP_COMMENT = "EXEC_STEP_COMMENT";

    static final String HEADER_CPG_CUF = "CPG_CUF_";
    static final String HEADER_IT_CUF = "IT_CUF_";
    static final String HEADER_TC_CUF = "TC_CUF_";
    static final String HEADER_EXEC_CUF = "HEADER_EXEC_CUF_";
    static final String HEADER_STEP_CUF = "STEP_CUF_";

    private CampaignExportCSVConstants() {}
}
