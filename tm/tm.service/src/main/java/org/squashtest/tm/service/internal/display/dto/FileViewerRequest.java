/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.io.File;
import java.util.Arrays;
import org.apache.commons.io.FilenameUtils;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.service.internal.display.attachment.FileViewerExtension;

public final class FileViewerRequest {

    final String source; // attachment id or file path
    final String fileName; // source name or target name
    final FileViewerExtension fileType; // source type or target type
    final boolean isAttachment;
    final String target; // selected file in archive

    private FileViewerRequest(
            String source, String fileName, FileViewerExtension fileType, boolean isAttachment) {
        this.source = source;
        this.fileType = fileType;
        this.fileName = fileName;
        this.isAttachment = isAttachment;
        this.target = null;
    }

    private FileViewerRequest(
            String source,
            String fileName,
            FileViewerExtension fileType,
            boolean isAttachment,
            String target) {
        this.source = source;
        this.fileName = fileName;
        this.fileType = fileType;
        this.isAttachment = isAttachment;
        this.target = target;
    }

    public static FileViewerRequest fromAttachment(Attachment attachment, String target) {
        String attachmentId = attachment.getId().toString();
        if (target != null) {
            String type = target.substring(target.lastIndexOf(".") + 1);
            FileViewerExtension targetType = findViewerType(type);
            String targetName = target.substring(target.lastIndexOf("/") + 1);

            return new FileViewerRequest(attachmentId, targetName, targetType, true, target);
        } else {
            String attachmentName = attachment.getName().replace(" ", "_");
            FileViewerExtension attachmentType = findViewerType(attachment.getType());

            return new FileViewerRequest(attachmentId, attachmentName, attachmentType, true);
        }
    }

    public static FileViewerRequest fromFile(File file) {
        String formattedName = file.getName().replace(" ", "_");
        String fileExtension = FilenameUtils.getExtension(formattedName);
        FileViewerExtension fileType = findViewerType(fileExtension);

        return new FileViewerRequest(file.getAbsolutePath(), formattedName, fileType, false);
    }

    private static FileViewerExtension findViewerType(String type) {
        return Arrays.stream(FileViewerExtension.values())
                .filter(extension -> extension.name().equalsIgnoreCase(type))
                .findAny()
                .orElse(null);
    }

    public String getSource() {
        return source;
    }

    public FileViewerExtension getFileType() {
        return fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isAttachment() {
        return isAttachment;
    }

    public String getTarget() {
        return target;
    }
}
