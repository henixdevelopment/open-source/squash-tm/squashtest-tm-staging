/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import static org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus.FAILURE;
import static org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus.RUNNING;
import static org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus.SUCCESS;
import static org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus.WARNING;
import static org.squashtest.tm.service.security.Authorizations.IMPORT_PROJECT_OR_ROLE_ADMIN;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportType;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.CouldNotParseJsonFileException;
import org.squashtest.tm.exception.pivotformatimport.CouldNotCreateEntityDuringImportException;
import org.squashtest.tm.exception.pivotformatimport.CouldNotCreateImportFileException;
import org.squashtest.tm.exception.pivotformatimport.InvalidJsonFileNameForImport;
import org.squashtest.tm.exception.pivotformatimport.NoDataImportedException;
import org.squashtest.tm.exception.projectimport.CannotDeleteImportStatusRunningException;
import org.squashtest.tm.exception.projectimport.CannotDeletePivotFileException;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.internal.dto.projectimporter.ImportWarningEntry;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.ProjectIdsReferences;
import org.squashtest.tm.service.internal.repository.PivotFormatImportDao;
import org.squashtest.tm.service.internal.repository.hibernate.CustomPivotFormatImportDao;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.projectimporter.pivotimporter.CustomFieldPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.ExecutionWorkspacePivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.FolderPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.RequirementPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.TestCasePivotImporterService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.user.UserAccountService;

@Service("GlobalProjectPivotImporterService")
public class GlobalProjectPivotImporterServiceImpl implements GlobalProjectPivotImporterService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(GlobalProjectPivotImporterServiceImpl.class);

    private static final String FILES_DIR = "Files";
    private static final String LOGS_DIR = "Logs";

    @Inject private PivotFormatImportDao pivotFormatImportDao;

    private final CustomPivotFormatImportDao customPivotFormatImportDao;
    private final GenericProjectManagerService genericProjectManager;
    private final UserAccountService userAccountService;
    private final CustomFieldPivotImporterService customFieldImporterService;
    private final FolderPivotImporterService folderImporterService;
    private final RequirementPivotImporterService requirementImporterService;
    private final TestCasePivotImporterService testCaseImporterService;
    private final ExecutionWorkspacePivotImporterService executionWorkspacePivotImporterService;
    private final PlatformTransactionManager transactionManager;
    private final PermissionEvaluationService permissionEvaluationService;
    private final ConfigurationService configurationService;
    private final Environment environment;

    @PersistenceContext private EntityManager entityManager;

    public GlobalProjectPivotImporterServiceImpl(
            CustomPivotFormatImportDao customPivotFormatImportDao,
            GenericProjectManagerService genericProjectManager,
            UserAccountService userAccountService,
            CustomFieldPivotImporterService customFieldImporterService,
            FolderPivotImporterService folderImporterService,
            RequirementPivotImporterService requirementImporterService,
            TestCasePivotImporterService testCaseImporterService,
            ExecutionWorkspacePivotImporterService executionWorkspacePivotImporterService,
            PlatformTransactionManager transactionManager,
            PermissionEvaluationService permissionEvaluationService,
            ConfigurationService configurationService,
            Environment environment) {
        this.customPivotFormatImportDao = customPivotFormatImportDao;
        this.genericProjectManager = genericProjectManager;
        this.userAccountService = userAccountService;
        this.customFieldImporterService = customFieldImporterService;
        this.folderImporterService = folderImporterService;
        this.requirementImporterService = requirementImporterService;
        this.testCaseImporterService = testCaseImporterService;
        this.executionWorkspacePivotImporterService = executionWorkspacePivotImporterService;
        this.transactionManager = transactionManager;
        this.permissionEvaluationService = permissionEvaluationService;
        this.configurationService = configurationService;
        this.environment = environment;
    }

    @Override
    @PreAuthorize(IMPORT_PROJECT_OR_ROLE_ADMIN)
    @Transactional
    public void createImportRequest(
            long projectId,
            MultipartFile multipartFile,
            String importName,
            PivotFormatImportType importType) {
        User currentUser = userAccountService.findCurrentUser();
        String filesFolderPath = getFilesFolderPath(importType);
        try {
            checkJsonFile(multipartFile);
            File filesFolder = new File(filesFolderPath);

            if (!filesFolder.exists()) {
                filesFolder.mkdirs();
            }

            String importFileName = generateImportFileName(multipartFile);
            File importFile = new File(filesFolderPath, importFileName);
            multipartFile.transferTo(importFile);
            importFile.createNewFile();
            PivotFormatImport pivotFormatImport = new PivotFormatImport();

            if (Objects.nonNull(projectId)) {
                GenericProject project = genericProjectManager.findById(projectId);
                pivotFormatImport.setProject((Project) project);
            }

            pivotFormatImport.setName(importName);
            pivotFormatImport.setCreatedBy(currentUser);
            pivotFormatImport.setCreatedOn(new Date());
            pivotFormatImport.setType(importType);
            pivotFormatImport.setFilePath(importFile.getPath());

            pivotFormatImportDao.save(pivotFormatImport);

        } catch (IOException e) {
            LOGGER.error(
                    "GlobalProjectPivotImporterService - Failed to create import file \"{}\" in folder path: {}",
                    multipartFile.getOriginalFilename(),
                    filesFolderPath,
                    e);
            throw new CouldNotCreateImportFileException();
        }
    }

    private static void checkJsonFile(MultipartFile multipartFile) throws IOException {

        try (ZipInputStream zis = new ZipInputStream(multipartFile.getInputStream())) {
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                String entryName = entry.getName();
                if (entryName.endsWith(".json")) {
                    checkJsonFileName(entryName);
                    checkJsonSyntaxError(entry, zis);
                }
            }
        }
    }

    private static void checkJsonFileName(String entryName) {
        boolean nameIsValid =
                EnumSet.allOf(JsonImportFile.class).stream()
                        .anyMatch(enumEntry -> enumEntry.getFileName().equals(entryName));
        if (!nameIsValid) {
            String message = String.format("Invalid json file name for pivot import: \" %s\"", entryName);
            LOGGER.error(message);
            throw new InvalidJsonFileNameForImport(entryName, message);
        }
    }

    private static void checkJsonSyntaxError(ZipEntry entry, ZipInputStream zis) throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(zis)) {

            // I don't want the jsonParser to close the ZipInputream after reading the first zip entry.
            jsonParser.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);

            while (jsonParser.nextToken() != null) {
                // Just parsing the entire file to check for syntax errors
            }
        } catch (JsonParseException e) {
            String message =
                    String.format(
                            "Failed to parse json file with name \" %s\". Please verify the syntax of your file.",
                            entry.getName());
            LOGGER.error(message, e);
            throw new CouldNotParseJsonFileException(entry.getName(), message);
        }
    }

    private String generateImportFileName(MultipartFile multipartFile) {
        String originalFilename = multipartFile.getOriginalFilename();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SSS");
        String timestamp = LocalDateTime.now().format(formatter);
        return String.format(
                "%s-%s.%s",
                FilenameUtils.getBaseName(originalFilename),
                timestamp,
                FilenameUtils.getExtension(originalFilename));
    }

    private String getPivotFormatImportFolderPath() {
        String pivotFormatImportFolderPath =
                environment.getProperty("squash.project-imports.folder-path");
        if (Objects.isNull(pivotFormatImportFolderPath)) {
            LOGGER.error("The property \"squash.project-imports.folder-path\" is not set");
            throw new CouldNotCreateImportFileException();
        }
        return pivotFormatImportFolderPath;
    }

    private String getFilesFolderPath(PivotFormatImportType importType) {
        Path filesFolderPath =
                Paths.get(getPivotFormatImportFolderPath(), getFolderForImportType(importType), FILES_DIR);
        return filesFolderPath.toString();
    }

    private String getFolderForImportType(PivotFormatImportType importType) {
        return StringUtils.capitalize(importType.name().toLowerCase());
    }

    @Override
    @Transactional
    public void deleteImportRequests(long projectId, List<Long> idImportRequests) {
        checkCanImportPermission(projectId);
        if (idImportRequests.isEmpty()) {
            return;
        }

        List<PivotFormatImport> pivotFormatImports = pivotFormatImportDao.findAllById(idImportRequests);
        for (PivotFormatImport pivotFormatImport : pivotFormatImports) {
            if (pivotFormatImport.getStatus() == RUNNING) {
                throw new CannotDeleteImportStatusRunningException(
                        String.format(
                                "Cannot delete the import \"%s\" #%s because it is running",
                                pivotFormatImport.getName(), pivotFormatImport.getId()),
                        pivotFormatImport.getName());
            } else {
                pivotFormatImportDao.delete(pivotFormatImport);
                clearImportFiles(pivotFormatImport);
            }
        }
    }

    private void clearImportFiles(PivotFormatImport pivotFormatImport) {
        File folderFile = new File(getPivotFormatImportFolderPath());
        File importFile = new File(pivotFormatImport.getFilePath());
        File importLogFile =
                new File(getImportErrorLogFilePath(pivotFormatImport.getId(), pivotFormatImport.getType()));
        checkAndDeleteImportFile(folderFile, importFile, pivotFormatImport);
        checkAndDeleteImportFile(folderFile, importLogFile, pivotFormatImport);
    }

    private void checkAndDeleteImportFile(
            File folderFile, File importFile, PivotFormatImport pivotFormatImport) {
        if (importFile.exists()) {
            checkImportFilePath(folderFile, importFile, pivotFormatImport.getName());
            try {
                FileUtils.forceDelete(importFile);
            } catch (IOException ioException) {
                LOGGER.error(
                        "Failed to delete import file \"{}\" in folder path: {}",
                        importFile.getName(),
                        importFile.getPath(),
                        ioException);
                throw new CannotDeletePivotFileException(pivotFormatImport.getName());
            }
        }
    }

    private void checkImportFilePath(File folderFile, File importFile, String importName) {
        if (Objects.isNull(folderFile) || !importFile.toPath().startsWith(folderFile.toPath())) {
            LOGGER.error("Import file is not in the \"Files\" folder directory");
            throw new CannotDeletePivotFileException(importName);
        }
    }

    @Override
    public void importProject(PivotFormatImport pivotFormatImport) {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

        TransactionStatus transaction = transactionManager.getTransaction(transactionDefinition);
        PivotFormatImportStatus finalStatus = FAILURE;
        try {
            logImportStarted(pivotFormatImport, false);
            GenericProject project = new Project();
            File importFile = new File(pivotFormatImport.getFilePath());
            try (ZipFile zipFile = new ZipFile(importFile)) {
                importProjectInfoFromZipArchive(project, zipFile, pivotFormatImport);
                ProjectIdsReferences projectIdsReferences = new ProjectIdsReferences(project);
                finalStatus = importEntitiesInProject(zipFile, projectIdsReferences, pivotFormatImport);
            }
            transactionManager.commit(transaction);
        } catch (Throwable e) {
            handleError(pivotFormatImport, transaction, e);
        } finally {
            saveFinalImportStatus(pivotFormatImport.getId(), finalStatus);
        }
    }

    @Override
    public void importInExistingProject(PivotFormatImport pivotFormatImport) {
        Project existingProject = pivotFormatImport.getProject();
        checkCanImportPermission(existingProject.getId());
        PivotFormatImportStatus finalStatus = FAILURE;
        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

        TransactionStatus transaction = transactionManager.getTransaction(transactionDefinition);
        try {

            logImportStarted(pivotFormatImport, true);
            ProjectIdsReferences projectIdsReferences = new ProjectIdsReferences(existingProject);
            File importFile = new File(pivotFormatImport.getFilePath());
            try (ZipFile zipFile = new ZipFile(importFile)) {
                finalStatus = importEntitiesInProject(zipFile, projectIdsReferences, pivotFormatImport);
            }
            transactionManager.commit(transaction);
        } catch (Throwable e) {
            handleError(pivotFormatImport, transaction, e);
        } finally {
            saveFinalImportStatus(pivotFormatImport.getId(), finalStatus);
        }
    }

    private void saveFinalImportStatus(Long importId, PivotFormatImportStatus finalStatus) {

        doInTransaction(
                () -> {
                    LOGGER.info(
                            "GlobalProjectPivotImporterService - The import with id: {} has ended with status: {}",
                            importId,
                            finalStatus);
                    customPivotFormatImportDao.updatePivotFormatImportStatus(importId, finalStatus);

                    if (SUCCESS.equals(finalStatus) || WARNING.equals(finalStatus)) {
                        customPivotFormatImportDao.updatePivotFormatImportSuccessfullyImportedOn(
                                importId, new Date());
                    }
                });
    }

    private void logImportStarted(
            PivotFormatImport pivotFormatImport, boolean importInExistingProject) {
        doInTransaction(
                () -> {
                    if (importInExistingProject) {
                        LOGGER.info(
                                "GlobalProjectPivotImporterService - Starting new import with id: {} for project: {}. Project id: {}",
                                pivotFormatImport.getId(),
                                pivotFormatImport.getProject().getName(),
                                pivotFormatImport.getProject().getId());
                    } else {
                        LOGGER.info(
                                "GlobalProjectPivotImporterService - Starting new import with id: {}. This import will create a new project.",
                                pivotFormatImport.getId());
                    }
                    customPivotFormatImportDao.updatePivotFormatImportStatus(
                            pivotFormatImport.getId(), RUNNING);
                });
    }

    private void handleError(
            PivotFormatImport pivotFormatImport, TransactionStatus transaction, Throwable e) {
        transactionManager.rollback(transaction);
        final String message =
                String.format(
                        " Import id: %s - Failed to import data in existing project \"%s\" with id %s.",
                        pivotFormatImport.getId(),
                        pivotFormatImport.getProject().getName(),
                        pivotFormatImport.getProject().getId());
        createErrorLogFile(pivotFormatImport, e);
        LOGGER.error(message, e);
    }

    private void createErrorLogFile(PivotFormatImport pivotFormatImport, Throwable ex) {
        File logFolder = new File(getLogsFolderPath(pivotFormatImport.getType()));

        if (!logFolder.exists()) {
            logFolder.mkdirs();
        }

        String logFilePath =
                getImportErrorLogFilePath(pivotFormatImport.getId(), pivotFormatImport.getType());

        try (PrintWriter pw = new PrintWriter(logFilePath)) {
            ex.printStackTrace(pw);
        } catch (IOException e) {
            LOGGER.error(
                    String.format(
                            "Could not generate error log file for import: %s", pivotFormatImport.getId()),
                    e);
        }
    }

    @Override
    public String getImportErrorLogFilePath(Long importId, PivotFormatImportType importType) {
        return String.format("%s/import-%s-error.log", getLogsFolderPath(importType), importId);
    }

    @Override
    public String getImportWarningLogFilePath(Long importId, PivotFormatImportType importType) {
        return String.format("%s/import-%s-warning.log", getLogsFolderPath(importType), importId);
    }

    private String getLogsFolderPath(PivotFormatImportType importType) {
        Path logsFolderPath =
                Paths.get(getPivotFormatImportFolderPath(), getFolderForImportType(importType), LOGS_DIR);
        return logsFolderPath.toString();
    }

    private PivotFormatImportStatus importEntitiesInProject(
            ZipFile zipFile,
            ProjectIdsReferences projectIdsReferences,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotImportMetadata pivotImportMetadata = new PivotImportMetadata();
        appendAttachmentLimitsMetadata(pivotImportMetadata);

        customFieldImporterService.importCustomFieldsFromZipArchive(
                zipFile, projectIdsReferences, pivotImportMetadata, pivotFormatImport);

        folderImporterService.importFoldersByJsonFileName(
                zipFile,
                JsonImportFile.REQUIREMENT_FOLDERS,
                projectIdsReferences,
                pivotImportMetadata,
                pivotFormatImport);

        requirementImporterService.importRequirementsFromZipArchive(
                zipFile, projectIdsReferences, pivotImportMetadata, pivotFormatImport);

        folderImporterService.importFoldersByJsonFileName(
                zipFile,
                JsonImportFile.TEST_CASE_FOLDERS,
                projectIdsReferences,
                pivotImportMetadata,
                pivotFormatImport);

        testCaseImporterService.importTestCasesFromZipArchive(
                zipFile, projectIdsReferences, pivotImportMetadata, pivotFormatImport);

        testCaseImporterService.importCalledTestCasesFromZipArchive(
                zipFile, pivotImportMetadata, pivotFormatImport);

        folderImporterService.importFoldersByJsonFileName(
                zipFile,
                JsonImportFile.CAMPAIGN_FOLDERS,
                projectIdsReferences,
                pivotImportMetadata,
                pivotFormatImport);

        executionWorkspacePivotImporterService.importCampaignsFromZipArchive(
                zipFile, projectIdsReferences, pivotImportMetadata, pivotFormatImport);

        executionWorkspacePivotImporterService.importIterationsFromZipArchive(
                zipFile, pivotImportMetadata, pivotFormatImport);

        executionWorkspacePivotImporterService.importTestSuitesFromZipArchive(
                zipFile, pivotImportMetadata, pivotFormatImport);

        executionWorkspacePivotImporterService.importExecutionsFromZipArchive(
                zipFile, pivotImportMetadata, pivotFormatImport);

        zipFile.close();
        checkIfImportHasImportedAtLeastOneEntity(pivotFormatImport, pivotImportMetadata);
        boolean importHasWarnings = !pivotImportMetadata.getImportWarningEntries().isEmpty();
        if (importHasWarnings) {
            createWarningLogFile(pivotImportMetadata.getImportWarningEntries(), pivotFormatImport);
        }
        return importHasWarnings ? WARNING : SUCCESS;
    }

    private void createWarningLogFile(
            List<ImportWarningEntry> importWarningEntries, PivotFormatImport pivotFormatImport)
            throws IOException {
        File logFolder = new File(getLogsFolderPath(pivotFormatImport.getType()));

        if (!logFolder.exists()) {
            logFolder.mkdirs();
        }

        String logFilePath =
                getImportWarningLogFilePath(pivotFormatImport.getId(), pivotFormatImport.getType());

        writeWarningLogToFile(importWarningEntries, logFilePath);
    }

    private static void writeWarningLogToFile(List<ImportWarningEntry> entries, String filePath)
            throws IOException {
        Set<EntityType> types =
                entries.stream().map(ImportWarningEntry::entityType).collect(Collectors.toSet());
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (EntityType type : types) {
                writer.write(type.name());
                writer.newLine();
                List<ImportWarningEntry> filteredEntries =
                        entries.stream().filter(e -> e.entityType().equals(type)).toList();
                for (ImportWarningEntry entry : filteredEntries) {
                    writer.write(entry.message());
                    writer.newLine();
                }
                writer.newLine();
            }
        }
    }

    private void appendAttachmentLimitsMetadata(PivotImportMetadata pivotImportMetadata) {
        String whiteListStr =
                configurationService.findConfiguration(
                        ConfigurationService.Properties.UPLOAD_EXTENSIONS_WHITELIST);
        List<String> whiteList = Arrays.stream(whiteListStr.split(",")).map(String::trim).toList();
        pivotImportMetadata.setAttachmentTypeWhiteList(whiteList);

        Long maxAttachmentSizeLimit =
                Long.valueOf(
                        configurationService.findConfiguration(
                                ConfigurationService.Properties.UPLOAD_SIZE_LIMIT));
        pivotImportMetadata.setMaxAttachmentSize(maxAttachmentSizeLimit);
    }

    private void checkIfImportHasImportedAtLeastOneEntity(
            PivotFormatImport pivotFormatImport, PivotImportMetadata pivotImportMetadata) {

        if (!pivotImportMetadata.hasAtLeastOneImportedEntity()) {
            String message =
                    String.format(
                            "Import Id: %s - No data has been imported. Please check the import file and try again.",
                            pivotFormatImport.getId());

            LOGGER.error(message);
            throw new NoDataImportedException(message);
        }
    }

    private void importProjectInfoFromZipArchive(
            GenericProject project, ZipFile zipFile, PivotFormatImport pivotFormatImport)
            throws IOException {
        LOGGER.info(
                "Import Id: {} -  Starting to create new project from zip archive \"{}\"",
                pivotFormatImport.getId(),
                zipFile.getName());

        ZipEntry entry = zipFile.getEntry(JsonImportFile.PROJECT.getFileName());
        try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
            handleProject(jsonInputStream, project, pivotFormatImport);
        }
    }

    private void handleProject(
            InputStream jsonInputStream, GenericProject project, PivotFormatImport pivotFormatImport) {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                if (JsonImportField.PROJECT.equals(jsonParser.getCurrentName())) {
                    project = parseAndCreateProject(jsonParser, project);
                    LOGGER.info(
                            "Import Id: {} - New project \"{}\" created successfully with id {}",
                            pivotFormatImport.getId(),
                            project.getName(),
                            project.getId());
                }
            }
        } catch (Exception e) {
            String message =
                    String.format(
                            "Import id: %s - Could not create new project named \"%s\"",
                            pivotFormatImport.getId(), project.getName());
            LOGGER.error(message);
            throw new CouldNotCreateEntityDuringImportException(message, e);
        }
    }

    private GenericProject parseAndCreateProject(JsonParser jsonParser, GenericProject project)
            throws IOException {
        jsonParser.nextToken();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            switch (fieldName) {
                case JsonImportField.NAME -> project.setName(jsonParser.getText());
                case JsonImportField.DESCRIPTION -> project.setDescription(jsonParser.getText());
                case JsonImportField.LABEL -> project.setLabel(jsonParser.getText());
                default -> {
                    // continue parsing
                }
            }
        }
        return genericProjectManager.persist(project);
    }

    /**
     * This allows to run code in an isolated transaction. The transaction is immediately committed.
     * This replaces @Transactional(propagation = REQUIRES_NEW) annotations, that broke with the
     * Hibernate(v5.6.7.Final) and Spring boot upgrades (v2.6.6).
     *
     * @param runnable the specific code to run
     */
    private void doInTransaction(Runnable runnable) {
        TransactionStatus transaction = null;

        try {
            DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
            transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            transaction = transactionManager.getTransaction(transactionDefinition);

            runnable.run();

            entityManager.flush();
            entityManager.clear();
            transactionManager.commit(transaction);
        } catch (Exception ex) {
            if (transaction != null) {
                transactionManager.rollback(transaction);
            }

            throw ex;
        }
    }

    private void checkCanImportPermission(long projectId) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                Collections.singletonList(projectId),
                Permissions.IMPORT.name(),
                Project.class.getName());
    }
}
