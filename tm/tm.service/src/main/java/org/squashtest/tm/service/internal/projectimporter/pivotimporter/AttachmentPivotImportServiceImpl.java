/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.attachment.UploadedData;
import org.squashtest.tm.service.internal.dto.projectimporter.AttachmentToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.ImportWarningEntry;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.projectimporter.pivotimporter.AttachmentPivotImportService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;

@Service("AttachmentPivotImportService")
public class AttachmentPivotImportServiceImpl implements AttachmentPivotImportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentPivotImportService.class);
    private static final String ATTACHMENT_DIR = "attachments";
    private static final String TEST_CASES_DIR = "test_cases";
    private static final String TEST_CASE_FOLDERS_DIR = "test_case_folders";
    private static final String REQUIREMENTS_DIR = "requirements";
    private static final String REQUIREMENT_FOLDERS_DIR = "requirement_folders";
    private static final String CAMPAIGNS_DIR = "campaigns";
    private static final String CAMPAIGN_FOLDERS_DIR = "campaign_folders";
    private static final String ITERATIONS_DIR = "iterations";
    private static final String TEST_SUITES_DIR = "test_suites";
    private static final String EXECUTIONS_DIR = "executions";
    private static final String PATH_SEPARATOR = "/";

    private final AttachmentManagerService attachmentManagerService;

    public AttachmentPivotImportServiceImpl(AttachmentManagerService attachmentManagerService) {
        this.attachmentManagerService = attachmentManagerService;
    }

    @Override
    public void addAttachmentsToEntity(
            List<AttachmentToImport> attachments,
            AttachmentHolderInfo holderInfo,
            PivotFormatImport pivotFormatImport,
            PivotImportMetadata pivotImportMetadata)
            throws IOException {

        File importFile = new File(pivotFormatImport.getFilePath());
        try (ZipFile zipFile = new ZipFile(importFile)) {
            for (AttachmentToImport attachment : attachments) {
                addAttachmentToEntity(
                        attachment, holderInfo, zipFile, pivotFormatImport, pivotImportMetadata);
            }
        } catch (IOException ex) {
            LOGGER.error("Could not open zip file to find attachments", ex);
            throw ex;
        }
    }

    private void addAttachmentToEntity(
            AttachmentToImport attachment,
            AttachmentHolderInfo holderInfo,
            ZipFile zipFile,
            PivotFormatImport pivotFormatImport,
            PivotImportMetadata pivotImportMetadata)
            throws IOException {

        String attachmentPath = getAttachmentPathByHolderType(attachment, holderInfo);
        ZipEntry entry = zipFile.getEntry(attachmentPath);

        if (Objects.isNull(entry)) {
            PivotFormatLoggerHelper.handleAttachmentNotFound(
                    LOGGER, attachmentPath, holderInfo, pivotFormatImport, pivotImportMetadata);
            return;
        }

        // The zipFileInputStream is a ZipFileInflaterInputStream which is working fine with a
        // MariaDB database but is breaking with Postgres
        // (Error: Unexpected error writing large object to database).
        // We need to convert it to a ByteArrayInputStream so that it also works fine on Postgres
        try (InputStream zipFileInputStream = zipFile.getInputStream(entry);
                InputStream convertedInputStream =
                        convertZipInputStreamToByteArrayInputStream(zipFileInputStream)) {

            EntityReference permissionOwner = getEntityReferenceForHolderType(holderInfo);

            UploadedData data =
                    new UploadedData(convertedInputStream, attachment.getOriginalFileName(), entry.getSize());
            checkAttachmentType(
                    data, holderInfo, pivotImportMetadata.getAttachmentTypeWhiteList(), pivotFormatImport);
            checkAttachmentMaxSize(
                    data, pivotImportMetadata.getMaxAttachmentSize(), holderInfo, pivotFormatImport);
            attachmentManagerService.addAttachment(
                    holderInfo.attachmentListId(), data, permissionOwner, holderInfo.holderType());

        } catch (Exception ex) {
            LOGGER.error(
                    "Import {} - Could not add attachment \"{}\" for entity type {}",
                    pivotFormatImport.getId(),
                    attachment.getZipImportFileName(),
                    holderInfo.holderType(),
                    ex);

            pivotImportMetadata
                    .getImportWarningEntries()
                    .add(new ImportWarningEntry(holderInfo.holderType(), ex.getMessage()));
        }
    }

    private InputStream convertZipInputStreamToByteArrayInputStream(InputStream zipFileInputStream)
            throws IOException {
        byte[] bytes = zipFileInputStream.readAllBytes();
        return new ByteArrayInputStream(bytes);
    }

    private void checkAttachmentType(
            UploadedData data,
            AttachmentHolderInfo holderInfo,
            List<String> attachmentWhiteList,
            PivotFormatImport pivotFormatImport) {
        String fileType = FilenameUtils.getExtension(data.getName());
        boolean isAllowed =
                attachmentWhiteList.stream().anyMatch(type -> type.equalsIgnoreCase(fileType));
        if (!isAllowed) {
            PivotFormatLoggerHelper.handleAttachmentTypeNotAllowed(
                    LOGGER, fileType, data.getName(), holderInfo, pivotFormatImport);
        }
    }

    private void checkAttachmentMaxSize(
            UploadedData data,
            Long maxAllowedSize,
            AttachmentHolderInfo holderInfo,
            PivotFormatImport pivotFormatImport) {
        if (data.getSizeInBytes() > maxAllowedSize) {
            PivotFormatLoggerHelper.handleAttachmentMaxSizeExceeded(
                    LOGGER,
                    data.getName(),
                    data.getSizeInBytes(),
                    maxAllowedSize,
                    holderInfo,
                    pivotFormatImport);
        }
    }

    private EntityReference getEntityReferenceForHolderType(AttachmentHolderInfo holderInfo) {
        if (EntityType.ACTION_TEST_STEP.equals(holderInfo.holderType())) {
            return new EntityReference(EntityType.TEST_CASE, holderInfo.ownerId());
        } else if (EntityType.EXECUTION_STEP.equals(holderInfo.holderType())) {
            return new EntityReference(EntityType.EXECUTION, holderInfo.ownerId());
        } else {
            return new EntityReference(holderInfo.holderType(), holderInfo.ownerId());
        }
    }

    private String getAttachmentPathByHolderType(
            AttachmentToImport attachment, AttachmentHolderInfo holderInfo) {
        String entityTypeDir =
                switch (holderInfo.holderType()) {
                    case TEST_CASE_FOLDER -> TEST_CASE_FOLDERS_DIR;
                    case TEST_CASE, ACTION_TEST_STEP -> TEST_CASES_DIR;
                    case REQUIREMENT_FOLDER -> REQUIREMENT_FOLDERS_DIR;
                    case REQUIREMENT -> REQUIREMENTS_DIR;
                    case CAMPAIGN_FOLDER -> CAMPAIGN_FOLDERS_DIR;
                    case CAMPAIGN -> CAMPAIGNS_DIR;
                    case ITERATION -> ITERATIONS_DIR;
                    case TEST_SUITE -> TEST_SUITES_DIR;
                    case EXECUTION, EXECUTION_STEP -> EXECUTIONS_DIR;
                    default ->
                            throw new IllegalArgumentException(
                                    GlobalProjectPivotImporterService.ENTITY_TYPE_NOT_HANDLED
                                            + holderInfo.holderType());
                };

        return ATTACHMENT_DIR
                + PATH_SEPARATOR
                + entityTypeDir
                + PATH_SEPARATOR
                + attachment.getZipImportFileName();
    }
}
