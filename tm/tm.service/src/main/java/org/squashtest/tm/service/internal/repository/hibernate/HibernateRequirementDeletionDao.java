/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.jooq.BatchBindStep;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Result;
import org.jooq.Table;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.event.RequirementAuditEvent;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.jooq.domain.tables.records.RequirementLibraryContentRecord;
import org.squashtest.tm.jooq.domain.tables.records.RlnRelationshipRecord;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.RequirementDeletionDao;

@Repository
public class HibernateRequirementDeletionDao extends AbstractHibernateDeletionDao
        implements RequirementDeletionDao {

    private static final String REQUIREMENT_IDS = "requirementIds";
    private static final String VERSION_IDS = "versionIds";
    private static final String FOLDER_IDS = "folderIds";
    private static final Logger LOGGER =
            LoggerFactory.getLogger(HibernateRequirementDeletionDao.class);

    private final DSLContext dslContext;

    @Qualifier("squashtest.tm.repository.RequirementLibraryNodeDao")
    private final LibraryNodeDao<RequirementLibraryNode> requirementLibraryNodeDao;

    public HibernateRequirementDeletionDao(
            DSLContext dslContext, LibraryNodeDao<RequirementLibraryNode> requirementLibraryNodeDao) {
        this.dslContext = dslContext;
        this.requirementLibraryNodeDao = requirementLibraryNodeDao;
    }

    /*
     * This method remove requirement versions. It assumes that no conflict will occur with Requirement#currentVersion
     */
    @Override
    public void deleteVersions(List<Long> versionIds) {
        executeDeleteNamedQuery("requirementDeletionDao.deleteVersions", VERSION_IDS, versionIds);
    }

    // note 1 : this method will be ran twice per batch : one for folder deletion, one for requirement
    // deletion
    // ( it is so because two distincts calls to #deleteNodes, see
    // RequirementDeletionHandlerImpl#deleteNodes() )
    // It should run fine tho, at the cost of a few useless extra queries.

    // note 2 : the code below must handle the references of requirements and requirement folders to
    // their Resource and SimpleResource, making the thing a lot more funny and pleasant to maintain.
    @Override
    public void removeEntities(List<Long> entityIds) {
        if (!entityIds.isEmpty()) {
            Set<Long> rootNodeParentIds = findParentRequirementLibraryIds(entityIds);
            Set<Long> childrenNodeParentIds = findParentRequirementLibraryNodeIds(entityIds);

            deleteContentAndRelationships(entityIds);

            deleteNodes(entityIds);

            reorderAfterDelete(rootNodeParentIds, childrenNodeParentIds);
        }
    }

    private void reorderAfterDelete(Set<Long> rootNodeParentIds, Set<Long> childrenNodeParentIds) {
        reorderRootNodes(rootNodeParentIds);
        reorderRelationships(childrenNodeParentIds);
    }

    private void deleteContentAndRelationships(List<Long> entityIds) {
        executeWithinHibernateSession(
                dslContext
                        .delete(REQUIREMENT_LIBRARY_CONTENT)
                        .where(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID.in(entityIds)));
        executeWithinHibernateSession(
                dslContext.delete(RLN_RELATIONSHIP).where(RLN_RELATIONSHIP.DESCENDANT_ID.in(entityIds)));
    }

    private void deleteNodes(List<Long> entityIds) {
        List<RequirementLibraryNode> nodes = requirementLibraryNodeDao.findAllByIds(entityIds);

        for (RequirementLibraryNode node : nodes) {
            if (node != null) {
                entityManager().remove(node);
                entityManager().flush();
            }
        }

        deleteRemainingRequirements(entityIds);
    }

    private void deleteRemainingRequirements(List<Long> entityIds) {
        executeWithinHibernateSession(
                dslContext.delete(REQUIREMENT).where(REQUIREMENT.RLN_ID.in(entityIds)));
        executeWithinHibernateSession(
                dslContext
                        .delete(REQUIREMENT_LIBRARY_NODE)
                        .where(REQUIREMENT_LIBRARY_NODE.RLN_ID.in(entityIds)));
    }

    private void reorderRootNodes(Set<Long> parentIds) {
        if (!parentIds.isEmpty()) {
            Map<Long, Result<RequirementLibraryContentRecord>> groups =
                    dslContext
                            .selectFrom(REQUIREMENT_LIBRARY_CONTENT)
                            .where(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID.in(parentIds))
                            .orderBy(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ORDER)
                            .fetchGroups(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID);

            dslContext
                    .delete(REQUIREMENT_LIBRARY_CONTENT)
                    .where(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID.in(parentIds))
                    .execute();

            doBatchReorder(
                    groups,
                    REQUIREMENT_LIBRARY_CONTENT,
                    REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID,
                    REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID,
                    REQUIREMENT_LIBRARY_CONTENT.CONTENT_ORDER);
        }
    }

    private <R extends Record3<Long, Long, Integer>> void doBatchReorder(
            Map<Long, Result<R>> groups,
            Table<R> table,
            Field<Long> parentColumn,
            Field<Long> childrenColumn,
            Field<Integer> orderColumn) {
        int numberOfItems = groups.values().stream().mapToInt(List::size).sum();
        if (numberOfItems > 0) { // avoiding insert into with (null, null, null) bound to statement
            BatchBindStep batch =
                    dslContext.batch(
                            dslContext
                                    .insertInto(table, parentColumn, childrenColumn, orderColumn)
                                    .values((Long) null, null, null));
            // see jooq documentation to see why this form is mandatory. Please check that there will be
            // at least one bind
            // else when executing the batch if no bind is done, it will result in bad statement : insert
            // into... values(null, null, null)

            groups.forEach(
                    (parentId, children) ->
                            IntStream.range(0, children.size())
                                    .forEach(
                                            position -> {
                                                Record3<Long, Long, Integer> record = children.get(position);
                                                batch.bind(
                                                        record.getValue(parentColumn),
                                                        record.getValue(childrenColumn),
                                                        position);
                                            }));
            batch.execute();
        }
    }

    private Set<Long> findParentRequirementLibraryIds(List<Long> rootRlnIds) {
        return dslContext
                .selectDistinct(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID)
                .from(REQUIREMENT_LIBRARY_CONTENT)
                .where(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID.in(rootRlnIds))
                .fetchSet(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID);
    }

    private void reorderRelationships(Set<Long> parentIds) {
        if (!parentIds.isEmpty()) {
            Map<Long, Result<RlnRelationshipRecord>> groups =
                    dslContext
                            .selectFrom(RLN_RELATIONSHIP)
                            .where(RLN_RELATIONSHIP.ANCESTOR_ID.in(parentIds))
                            .orderBy(RLN_RELATIONSHIP.CONTENT_ORDER)
                            .fetchGroups(RLN_RELATIONSHIP.ANCESTOR_ID);

            dslContext
                    .delete(RLN_RELATIONSHIP)
                    .where(RLN_RELATIONSHIP.ANCESTOR_ID.in(parentIds))
                    .execute();

            doBatchReorder(
                    groups,
                    RLN_RELATIONSHIP,
                    RLN_RELATIONSHIP.ANCESTOR_ID,
                    RLN_RELATIONSHIP.DESCENDANT_ID,
                    RLN_RELATIONSHIP.CONTENT_ORDER);
        }
    }

    private Set<Long> findParentRequirementLibraryNodeIds(List<Long> rlnIds) {
        return dslContext
                .select(RLN_RELATIONSHIP.ANCESTOR_ID)
                .from(RLN_RELATIONSHIP)
                .where(RLN_RELATIONSHIP.DESCENDANT_ID.in(rlnIds))
                .and(RLN_RELATIONSHIP.ANCESTOR_ID.notIn(rlnIds))
                .fetchSet(RLN_RELATIONSHIP.ANCESTOR_ID);
    }

    private void executeWithinHibernateSession(org.jooq.Query jooqQuery) {
        NativeQuery<?> nativeQuery = getSession().createNativeQuery(jooqQuery.getSQL());
        List<Object> bindValues = jooqQuery.getBindValues();

        for (int i = 0; i < bindValues.size(); i++) {
            nativeQuery.setParameter(i + 1, bindValues.get(i));
        }

        nativeQuery.executeUpdate();
    }

    @Override
    public List<Long>[] separateFolderFromRequirementIds(List<Long> originalIds) {

        List<Long> folderIds = new ArrayList<>(0);
        List<Long> requirementIds = new ArrayList<>(0);

        List<BigInteger> filtredFolderIds =
                executeSelectSQLQuery(
                        NativeQueries.REQUIREMENTLIBRARYNODE_SQL_FILTERFOLDERIDS, REQUIREMENT_IDS, originalIds);

        for (Long oId : originalIds) {
            if (filtredFolderIds.contains(BigInteger.valueOf(oId))) {
                folderIds.add(oId);
            } else {
                requirementIds.add(oId);
            }
        }

        return new List[] {folderIds, requirementIds};
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> findRequirementAttachmentListIds(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query query = getSession().getNamedQuery("requirement.findAllAttachmentLists");
            query.setParameterList(REQUIREMENT_IDS, requirementIds);
            return query.list();
        }
        return new ArrayList<>(0);
    }

    @Override
    public List<Long> findRequirementVersionAttachmentListIds(List<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            Query query = getSession().getNamedQuery("requirementVersion.findAllAttachmentLists");
            query.setParameterList(VERSION_IDS, versionIds);
            return query.list();
        }
        return new ArrayList<>(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> findRequirementFolderAttachmentListIds(List<Long> folderIds) {
        if (!folderIds.isEmpty()) {
            Query query = getSession().getNamedQuery("requirementFolder.findAllAttachmentLists");
            query.setParameterList(FOLDER_IDS, folderIds);
            return query.list();
        }
        return Collections.emptyList();
    }

    @Override
    public void removeFromVerifiedVersionsLists(List<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            executeDeleteSQLQuery(
                    NativeQueries.REQUIREMENT_SQL_REMOVEFROMVERIFIEDVERSIONSLISTS, VERSION_IDS, versionIds);
        }
    }

    @Override
    public void removeFromLinkedVersionsLists(List<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            executeDeleteSQLQuery(
                    NativeQueries.REQUIREMENT_SQL_REMOVEFROMLINKEDVERSIONSLISTS, VERSION_IDS, versionIds);
        }
    }

    @Override
    public void removeFromVerifiedRequirementLists(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            executeDeleteSQLQuery(
                    NativeQueries.REQUIREMENT_SQL_REMOVEFROMVERIFIEDREQUIREMENTLISTS,
                    REQUIREMENT_IDS,
                    requirementIds);
        }
    }

    @Override
    public void removeTestStepsCoverageByRequirementVersionIds(List<Long> requirementVersionIds) {
        if (!requirementVersionIds.isEmpty()) {
            executeDeleteSQLQuery(
                    NativeQueries.REQUIREMENT_SQL_REMOVE_TEST_STEP_COVERAGE_BY_REQ_VERSION_IDS,
                    VERSION_IDS,
                    requirementVersionIds);
        }
    }

    @Override
    public void deleteRequirementAuditEvents(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            // we borrow the following from RequirementAuditDao
            List<RequirementAuditEvent> events =
                    executeSelectNamedQuery(
                            "requirementAuditEvent.findAllByRequirementIds", "ids", requirementIds);

            // because Hibernate sucks so much at polymorphic bulk delete, we're going to remove
            // them one by one.
            for (RequirementAuditEvent event : events) {
                removeEntity(event);
            }

            flush();
        }
    }

    @Override
    public void deleteHighLevelRequirementReferenceIfExists(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query query =
                    getSession()
                            .getNamedQuery("Requirement.removeHighLvlReqReferenceOnAllLinkedRequirements");
            query.setParameterList(REQUIREMENT_IDS, requirementIds);
            query.executeUpdate();
        }
    }

    @Override
    public void deleteRequirementVersionAuditEvents(List<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            // we borrow the following from RequirementAuditDao
            List<RequirementAuditEvent> events =
                    executeSelectNamedQuery(
                            "requirementAuditEvent.findAllByRequirementVersionIds", "ids", versionIds);

            // because Hibernate sucks so much at polymorphic bulk delete, we're going to remove
            // them one by one.
            for (RequirementAuditEvent event : events) {
                removeEntity(event);
            }

            flush();
        }
    }

    @Override
    public List<Long> findVersionIds(List<Long> requirementIds) {
        return executeSelectNamedQuery(
                "requirementDeletionDao.findVersionIds", "reqIds", requirementIds);
    }

    @Override
    public List<Long> findRemainingRequirementIds(List<Long> originalIds) {
        List<BigInteger> rawids =
                executeSelectSQLQuery(
                        NativeQueries.REQUIREMENT_SQL_FINDNOTDELETED, "allRequirementIds", originalIds);
        List<Long> cIds = new ArrayList<>(rawids.size());
        for (BigInteger rid : rawids) {
            cIds.add(rid.longValue());
        }
        return cIds;
    }

    /* *************************************************************
     *  			Methods for the milestone mode
     ************************************************************ */

    /** See javadoc on the interface */
    @Override
    public List<Long> findDeletableVersions(List<Long> requirementIds, Long milestoneId) {

        List<Long> deletableVersions = new ArrayList<>(0);

        // 1 - must belong to milestone
        List<Long> versionsBelongingToMilestone =
                findVersionIdsForMilestone(requirementIds, milestoneId);
        deletableVersions.addAll(versionsBelongingToMilestone);

        // 2 - must not belong to many milestones
        List<Long> hasManyMilestones = filterVersionIdsHavingMultipleMilestones(deletableVersions);
        deletableVersions.removeAll(hasManyMilestones);

        // 3 - must not be locked
        List<Long> lockedVersions = filterVersionIdsWhichMilestonesForbidsDeletion(deletableVersions);
        deletableVersions.removeAll(lockedVersions);

        return deletableVersions;
    }

    /** See javadoc on the interface */
    @Override
    public List<Long> findUnbindableVersions(List<Long> requirementIds, Long milestoneId) {

        List<Long> unbindableVersions = new ArrayList<>(0);

        // 1 - must belong to the milestone
        List<Long> versionsBelongingToMilestone =
                findVersionIdsForMilestone(requirementIds, milestoneId);
        unbindableVersions.addAll(versionsBelongingToMilestone);

        // 2 - must belong to many milestones
        versionsBelongingToMilestone = filterVersionIdsHavingMultipleMilestones(unbindableVersions);

        // 3 - must not be locked
        List<Long> lockedVersions = filterVersionIdsWhichMilestonesForbidsDeletion(unbindableVersions);
        versionsBelongingToMilestone.removeAll(lockedVersions);

        return versionsBelongingToMilestone;
    }

    @Override
    public List<Long> filterRequirementsHavingDeletableVersions(
            List<Long> requirementIds, Long milestoneId) {
        List<Long> deletableVersions = findDeletableVersions(requirementIds, milestoneId);
        return findByRequirementVersion(deletableVersions);
    }

    @Override
    public List<Long> filterRequirementsHavingUnbindableVersions(
            List<Long> requirementIds, Long milestoneId) {
        List<Long> deletableVersions = findUnbindableVersions(requirementIds, milestoneId);
        return findByRequirementVersion(deletableVersions);
    }

    @Override
    public List<Long> filterRequirementsIdsWhichMilestonesForbidsDeletion(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            MilestoneStatus[] blockingStatuses =
                    new MilestoneStatus[MilestoneStatus.MILESTONE_BLOCKING_STATUSES.size()];
            MilestoneStatus.MILESTONE_BLOCKING_STATUSES.toArray(blockingStatuses);
            Query query =
                    getSession()
                            .getNamedQuery(
                                    "requirementDeletionDao.findRequirementsWhichMilestonesForbidsDeletion");
            query.setParameterList(REQUIREMENT_IDS, requirementIds, LongType.INSTANCE);
            query.setParameterList("lockedStatuses", blockingStatuses);
            return query.list();
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public List<Long> filterVersionIdsWhichMilestonesForbidsDeletion(List<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            MilestoneStatus[] blockingStatuses =
                    new MilestoneStatus[MilestoneStatus.MILESTONE_BLOCKING_STATUSES.size()];
            MilestoneStatus.MILESTONE_BLOCKING_STATUSES.toArray(blockingStatuses);
            Query query =
                    getSession()
                            .getNamedQuery("requirementDeletionDao.findVersionsWhichMilestonesForbidsDeletion");
            query.setParameterList(VERSION_IDS, versionIds, LongType.INSTANCE);
            query.setParameterList("lockedStatuses", blockingStatuses);
            return query.list();
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public List<Long> filterVersionIdsHavingMultipleMilestones(List<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            Query q =
                    getSession()
                            .getNamedQuery("requirementDeletionDao.findVersionIdsHavingMultipleMilestones");
            q.setParameterList(VERSION_IDS, versionIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public List<Long> filterRequirementHavingMultipleMilestones(List<Long> requirementIds) {
        if (requirementIds.isEmpty()) {
            return Collections.emptyList();
        }
        return entityManager()
                .createNamedQuery(
                        "requirementDeletionDao.findRequirementHavingMultipleMilestones", Long.class)
                .setParameter(REQUIREMENT_IDS, requirementIds)
                .getResultList();
    }

    @Override
    public List<Long> findVersionIdsForMilestone(List<Long> requirementIds, Long milestoneId) {
        if (!requirementIds.isEmpty()) {
            Query query = getSession().getNamedQuery("requirementDeletionDao.findAllVersionForMilestone");
            query.setParameterList("nodeIds", requirementIds, LongType.INSTANCE);
            query.setParameter("milestoneId", milestoneId);
            return query.list();
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public void unbindFromMilestone(List<Long> requirementIds, Long milestoneId) {
        if (!requirementIds.isEmpty()) {
            NativeQuery query =
                    getSession().createNativeQuery(NativeQueries.REQUIREMENT_SQL_UNBIND_MILESTONE);
            query.setParameterList(REQUIREMENT_IDS, requirementIds, LongType.INSTANCE);
            query.setParameter("milestoneId", milestoneId);
            query.executeUpdate();
        }
    }

    @Override
    public void unsetRequirementCurrentVersion(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query q = getSession().getNamedQuery("requirement.findAllById");
            q.setParameterList(REQUIREMENT_IDS, requirementIds);

            List<Requirement> requirements = q.list();

            for (Requirement r : requirements) {
                r.setCurrentVersion(null);
            }
        }
    }

    @Override
    public void resetRequirementCurrentVersion(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query q = getSession().getNamedQuery("requirement.findAllRequirementsWithLatestVersionByIds");
            q.setParameterList(REQUIREMENT_IDS, requirementIds);

            List<Object[]> tuples = q.list();

            for (Object[] tuple : tuples) {
                RequirementVersion latest = (RequirementVersion) tuple[1];
                ((Requirement) tuple[0]).setCurrentVersion(latest);
            }
        }
    }

    @Override
    public void reorderRequirementVersions(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query q = getSession().getNamedQuery("requirement.findRequirementWithVersions");
            q.setParameterList(REQUIREMENT_IDS, requirementIds);
            List<Requirement> requirements = (List<Requirement>) q.list();

            requirements.forEach(
                    requirement -> {
                        List<RequirementVersion> versions =
                                requirement.getRequirementVersions().stream()
                                        .sorted(Comparator.comparingInt(RequirementVersion::getVersionNumber))
                                        .toList();

                        doReorderRequirementVersions(versions);
                    });
        }
    }

    private void doReorderRequirementVersions(List<RequirementVersion> versions) {
        IntStream.range(0, versions.size())
                .forEach(
                        index -> {
                            RequirementVersion version = versions.get(index);
                            int newVersionNumber = index + 1;
                            if (version.getVersionNumber() != newVersionNumber) {
                                if (LOGGER.isDebugEnabled()) {
                                    LOGGER.debug(
                                            "Reorder incorrect RequirementVersion Version number. Requirement Id {} - RequirementVersion Id {}. Update old version number {} to new version number {}",
                                            version.getRequirement().getId(),
                                            version.getId(),
                                            version.getVersionNumber(),
                                            newVersionNumber);
                                }
                                version.setVersionNumber(newVersionNumber);
                                entityManager().flush();
                            }
                        });
    }

    private List<Long> findByRequirementVersion(List<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            Query q = getSession().getNamedQuery("requirement.findByRequirementVersion");
            q.setParameterList(VERSION_IDS, versionIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>(0);
        }
    }
}
