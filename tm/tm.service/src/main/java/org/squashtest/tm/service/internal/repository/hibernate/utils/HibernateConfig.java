/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.utils;

import javax.persistence.EntityManager;
import org.hibernate.Session;

public final class HibernateConfig {

    public static final int BATCH_10 = 10;
    public static final int BATCH_20 = 20;
    public static final int BATCH_30 = 30;
    public static final int BATCH_50 = 50;

    private HibernateConfig() {}

    public static void enableBatch(EntityManager entityManager, int batchSize) {
        entityManager.unwrap(Session.class).setJdbcBatchSize(batchSize);
    }

    public static void disabledBatch(EntityManager entityManager) {
        entityManager.unwrap(Session.class).setJdbcBatchSize(null);
    }
}
