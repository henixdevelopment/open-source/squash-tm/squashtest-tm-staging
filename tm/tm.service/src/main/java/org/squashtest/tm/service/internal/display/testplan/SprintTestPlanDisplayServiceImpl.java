/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.testplan;

import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTION_STATUSES;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_PLAN_ITEM_ID;
import static org.squashtest.tm.service.security.Authorizations.READ_SPRINT_OR_ADMIN;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.testplan.TestPlan;
import org.squashtest.tm.service.display.testplan.SprintTestPlanDisplayService;
import org.squashtest.tm.service.internal.display.campaign.AvailableDatasetAppender;
import org.squashtest.tm.service.internal.display.campaign.ReadUnassignedTestPlanHelper;
import org.squashtest.tm.service.internal.display.campaign.SprintReqTestPlanItemSuccessRateCalculator;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.SprintOverallExecPlanGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.SprintReqVersionTestPlanGrid;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Service
@Transactional(readOnly = true)
public class SprintTestPlanDisplayServiceImpl implements SprintTestPlanDisplayService {

    private final DSLContext dslContext;
    private final AvailableDatasetAppender availableDatasetAppender;
    private final ExecutionDao executionDao;
    private final SprintReqTestPlanItemSuccessRateCalculator successRateCalculator;
    private final ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper;

    public SprintTestPlanDisplayServiceImpl(
            DSLContext dslContext,
            AvailableDatasetAppender availableDatasetAppender,
            ExecutionDao executionDao,
            SprintReqTestPlanItemSuccessRateCalculator successRateCalculator,
            ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper) {
        this.dslContext = dslContext;
        this.availableDatasetAppender = availableDatasetAppender;
        this.executionDao = executionDao;
        this.successRateCalculator = successRateCalculator;
        this.readUnassignedTestPlanHelper = readUnassignedTestPlanHelper;
    }

    @Override
    public GridResponse findSprintReqVersionTestPlan(Long testPlanId, GridRequest gridRequest) {
        String userToRestrictTo =
                readUnassignedTestPlanHelper.getUserToRestrictTo(testPlanId, TestPlan.class.getName());
        final SprintReqVersionTestPlanGrid sprintReqVersionTestPlanGrid =
                new SprintReqVersionTestPlanGrid(testPlanId, userToRestrictTo);
        final GridResponse gridResponse = sprintReqVersionTestPlanGrid.getRows(gridRequest, dslContext);
        availableDatasetAppender.appendAvailableDatasets(gridResponse);
        gridResponse.getDataRows().forEach(this::addLastExecutionStatuses);
        successRateCalculator.appendSuccessRate(gridResponse);
        return gridResponse;
    }

    @Override
    @PreAuthorize(READ_SPRINT_OR_ADMIN)
    public GridResponse findSprintOverallExecPlan(long sprintId, GridRequest gridRequest) {
        String userToRestrictTo =
                readUnassignedTestPlanHelper.getUserToRestrictTo(sprintId, Sprint.class.getName());
        final SprintOverallExecPlanGrid sprintOverallExecPlanGrid =
                new SprintOverallExecPlanGrid(sprintId, userToRestrictTo);
        final GridResponse gridResponse = sprintOverallExecPlanGrid.getRows(gridRequest, dslContext);
        availableDatasetAppender.appendAvailableDatasets(gridResponse);
        gridResponse.getDataRows().forEach(this::addLastExecutionStatuses);
        successRateCalculator.appendSuccessRate(gridResponse);
        return gridResponse;
    }

    private void addLastExecutionStatuses(DataRow row) {
        final Long testPlanItemId =
                (Long) row.getData().get(RequestAliasesConstants.toCamelCase(TEST_PLAN_ITEM_ID));

        row.addData(
                RequestAliasesConstants.toCamelCase(LAST_EXECUTION_STATUSES),
                executionDao.findSprintTestPlanItemLastExecStatuses(testPlanItemId));
    }
}
