/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.sum;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SUCCESS_RATE;

import java.util.Map;
import java.util.Set;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.execution.ExecutionStatus;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExecutionSuccessRateCalculator extends AbstractSuccessRateCalculator {

    public ExecutionSuccessRateCalculator(DSLContext dslContext) {
        super(dslContext);
    }

    @Override
    protected Map<Long, Float> createRateByDataRowIdMap(Set<Long> executionIds) {
        Field<Float> successRate =
                (sum(when(EXECUTION_STEP.EXECUTION_STATUS.eq(ExecutionStatus.SUCCESS.name()), 1))
                                .cast(Float.class)
                                .mul(100))
                        .div(when(count().eq(0), 1).otherwise(count()))
                        .as(SUCCESS_RATE);

        return dslContext
                .select(EXECUTION.EXECUTION_ID, successRate)
                .from(EXECUTION)
                .join(EXECUTION_EXECUTION_STEPS)
                .on(EXECUTION.EXECUTION_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_ID))
                .join(EXECUTION_STEP)
                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(EXECUTION_STEP.EXECUTION_STEP_ID))
                .where(EXECUTION.EXECUTION_ID.in(executionIds))
                .groupBy(EXECUTION.EXECUTION_ID)
                .orderBy(EXECUTION.EXECUTION_ID)
                .fetchMap(EXECUTION.EXECUTION_ID, successRate);
    }
}
