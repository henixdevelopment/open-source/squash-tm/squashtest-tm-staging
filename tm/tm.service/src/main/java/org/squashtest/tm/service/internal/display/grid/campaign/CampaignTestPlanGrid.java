/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.countDistinct;
import static org.squashtest.tm.domain.milestone.MilestoneStatus.MILESTONE_BLOCKING_STATUSES;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_FULL_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_TO_BLOCKING_MILESTONE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CAMPAIGN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_LABELS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MAX_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MIN_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_REFERENCE;

import java.util.Arrays;
import java.util.List;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.jooq.domain.tables.records.CampaignTestPlanItemRecord;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

public class CampaignTestPlanGrid extends AbstractTestPlanGrid<CampaignTestPlanItemRecord> {

    private final Long campaignId;

    public CampaignTestPlanGrid(Long campaignId, String userLoginToRestrictTo) {
        super(userLoginToRestrictTo);
        this.campaignId = campaignId;
    }

    @Override
    protected TableField<CampaignTestPlanItemRecord, Long> getItemIdColumn() {
        return CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID;
    }

    @Override
    protected TableField<CampaignTestPlanItemRecord, Long> getAssigneeIdColumn() {
        return CAMPAIGN_TEST_PLAN_ITEM.USER_ID;
    }

    @Override
    protected TableField<CampaignTestPlanItemRecord, Long> getTestCaseIdColumn() {
        return CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID;
    }

    @Override
    protected Table<CampaignTestPlanItemRecord> getItemTable() {
        return CAMPAIGN_TEST_PLAN_ITEM;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID),
                new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as(TEST_CASE_NAME), TEST_CASE_LIBRARY_NODE.NAME),
                new GridColumn(TEST_CASE_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID)),
                new GridColumn(CAMPAIGN_LIBRARY_NODE.CLN_ID.as(CAMPAIGN_ID)),
                new GridColumn(PROJECT.NAME.as(PROJECT_NAME), PROJECT.NAME),
                new GridColumn(TEST_CASE.REFERENCE.as(TEST_CASE_REFERENCE), TEST_CASE.REFERENCE),
                new GridColumn(TEST_CASE.TCLN_ID.as(TEST_CASE_ID)),
                new LevelEnumColumn(TestCaseImportance.class, TEST_CASE.IMPORTANCE),
                new GridColumn(getUser().field(ASSIGNEE_FULL_NAME)),
                new GridColumn(getUser().field(ASSIGNEE_LOGIN)), // this field is used only for filters
                new GridColumn(DATASET.NAME.as(DATASET_NAME), DATASET.NAME),
                new GridColumn(DSL.field(MILESTONE_MIN_DATE)),
                new GridColumn(DSL.field(MILESTONE_MAX_DATE)),
                new GridColumn(DSL.field(MILESTONE_LABELS)),
                new GridColumn(DSL.field(BOUND_TO_BLOCKING_MILESTONE)));
    }

    @Override
    protected Table<?> getTable() {
        final SelectHavingStep<?> getUser = getUser();
        final SelectHavingStep<?> milestoneDates = getMilestoneDates();
        final SelectHavingStep<?> boundToLockedMilestone = getBoundToBlockingMilestone();

        return getFilteredTableOnAssigneeIfNotExploratory(
                CAMPAIGN_TEST_PLAN_ITEM
                        .innerJoin(CAMPAIGN)
                        .on(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .leftJoin(TEST_CASE)
                        .on(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                        .leftJoin(TEST_CASE_LIBRARY_NODE)
                        .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .innerJoin(PROJECT)
                        .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                        .leftJoin(DATASET)
                        .on(CAMPAIGN_TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID))
                        .leftJoin(getUser)
                        .on(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.eq(getUser.field(ITEM_ID, Long.class)))
                        .leftJoin(milestoneDates)
                        .on(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.eq(milestoneDates.field(ITEM_ID, Long.class)))
                        .leftJoin(boundToLockedMilestone)
                        .on(
                                CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.eq(
                                        boundToLockedMilestone.field(ITEM_ID, Long.class)))
                        .leftJoin(EXPLORATORY_TEST_CASE)
                        .on(EXPLORATORY_TEST_CASE.TCLN_ID.eq(TEST_CASE.TCLN_ID)));
    }

    // In other grids, we check if there's an exploratory session overview for the item. This is not
    // possible for campaigns
    // as they can't be executed, thus they don't have session overviews. As a consequence, we check
    // for the existence of
    // an exploratory test case instead. This means that if the test case was deleted, the information
    // is lost.
    @Override
    protected Condition isExploratoryRow() {
        return EXPLORATORY_TEST_CASE.TCLN_ID.isNotNull();
    }

    @Override
    protected Condition craftInvariantFilter() {
        return CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(this.campaignId);
    }

    @Override
    protected Field<?> getIdentifier() {
        return CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID;
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return CAMPAIGN_LIBRARY_NODE.PROJECT_ID;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return CAMPAIGN_TEST_PLAN_ITEM.TEST_PLAN_ORDER.asc();
    }

    @Override
    protected SelectHavingStep<Record2<Long, Boolean>> getBoundToBlockingMilestone() {
        // ITEM_ID | BOUND_TO_BLOCKING_MILESTONE
        return DSL.select(
                        CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.as(ITEM_ID),
                        DSL.field(countDistinct(MILESTONE.MILESTONE_ID).gt(0)).as(BOUND_TO_BLOCKING_MILESTONE))
                .from(CAMPAIGN_TEST_PLAN_ITEM)
                .innerJoin(MILESTONE_CAMPAIGN)
                .on(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID))
                .innerJoin(MILESTONE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_CAMPAIGN.MILESTONE_ID))
                .where(MILESTONE.STATUS.in(MILESTONE_BLOCKING_STATUSES))
                .groupBy(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID);
    }
}
