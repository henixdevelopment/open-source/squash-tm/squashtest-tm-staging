/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem;
import org.squashtest.tm.service.internal.repository.CustomSprintReqVersionDao;
import org.squashtest.tm.service.internal.repository.TestPlanItemDao;

@Repository
public class CustomSprintReqVersionDaoImpl implements CustomSprintReqVersionDao {
    private final TestPlanItemDao testPlanItemDao;

    public CustomSprintReqVersionDaoImpl(TestPlanItemDao testPlanItemDao) {
        this.testPlanItemDao = testPlanItemDao;
    }

    @Override
    public void removeTestPlanItemOrNullifyReferencedTestCase(List<Long> tclnIds) {
        // Fetch test plan items with their executions
        final List<TestPlanItem> testPlanItems =
                testPlanItemDao.findByReferencedTestCasesWithExecutions(tclnIds);

        final List<TestPlanItem> itemsWithoutExecutions =
                testPlanItems.stream().filter(item -> item.getExecutions().isEmpty()).toList();

        final List<TestPlanItem> itemsWithExecutions =
                testPlanItems.stream().filter(item -> !item.getExecutions().isEmpty()).toList();

        // Delete test plan items without executions
        testPlanItemDao.deleteAll(itemsWithoutExecutions);

        // Nullify referenced test cases in test plan items with executions
        itemsWithExecutions.forEach(item -> item.setReferencedTestCase(null));
    }
}
