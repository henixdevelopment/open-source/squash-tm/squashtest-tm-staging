/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.squashtest.tm.service.internal.batchimport.Existence.EXISTS;
import static org.squashtest.tm.service.internal.batchimport.Existence.NOT_EXISTS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.library.structures.LibraryGraph;
import org.squashtest.tm.domain.library.structures.LibraryGraph.SimpleNode;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.service.importer.Target;
import org.squashtest.tm.service.importer.WithPath;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.ImportRequirementFinder;
import org.squashtest.tm.service.internal.batchimport.requirement.tree.ImportedRequirementNode;
import org.squashtest.tm.service.internal.batchimport.requirement.tree.ImportedRequirementTree;
import org.squashtest.tm.service.internal.batchimport.testcase.TestCaseCallGraph;
import org.squashtest.tm.service.internal.batchimport.testcase.TestCaseCallGraph.Node;
import org.squashtest.tm.service.internal.dto.ReqVersionMilestone;
import org.squashtest.tm.service.internal.repository.CustomCustomFieldDao;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.RequirementImportDao;
import org.squashtest.tm.service.internal.repository.TestStepDao;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder;
import org.squashtest.tm.service.milestone.MilestoneMembershipFinder;
import org.squashtest.tm.service.requirement.LinkedRequirementVersionManagerService;
import org.squashtest.tm.service.testcase.ParameterFinder;
import org.squashtest.tm.service.testcase.TestCaseFinder;
import org.squashtest.tm.service.testcase.TestCaseLibraryFinderService;

@Component
@Scope("prototype")
public class Model {

    private static final Logger LOGGER = LoggerFactory.getLogger(Model.class);
    private static final String UNCHECKED = "unchecked";

    private final CustomCustomFieldDao customFieldDao;

    private final TestCaseLibraryFinderService finderService;

    private final RequirementImportDao requirementImportDao;

    private final TestCaseCallTreeFinder calltreeFinder;

    private final TestCaseFinder testCaseFinder;

    private final ParameterFinder paramFinder;

    private final MilestoneMembershipFinder milestoneMemberFinder;

    private final DatasetDao dsDao;

    private final LinkedRequirementVersionManagerService reqlinkService;

    private final EntityPathHeaderDao pathHeaderDao;

    private final TestStepDao testStepDao;

    private final ProjectDao projectDao;

    /* **********************************************************************************************************************************
     *
     * The following properties are initialized all together during
     * init(List<TestCaseTarget>) :
     *
     * - testCaseStatusByTarget - testCaseStepsByTarget - projectStatusByName -
     * tcCufsPerProjectname - stepCufsPerProjectname
     *
     * **************************************************************************
     * **************************************** *****************
     */

    /**
     * Maps a reference to a TestCase (namely a TestCaseTarget). It keeps track of its status (see
     * TargetStatus) and possibly its id (when there is a concrete instance of it in the database).
     * <br>
     * <br>
     * Because a test case might be referenced multiple time, once a test case is loaded in that map
     * it'll stay there.
     */
    private final Map<TestCaseTarget, TargetStatus> testCaseStatusByTarget = new HashMap<>();

    /**
     * Maps a test case (given its target) to a list of step models. We only care of their position
     * (because they are identified by position), their type (because we want to detect potential
     * attempts of modifications of an action step whereas the target is actually a call step and
     * conversely), and possibly a called test case (if the step is a call step, and we want to keep
     * track of possible cycles).
     */
    private final Map<TestCaseTarget, List<InternalStepModel>> testCaseStepsByTarget =
            new HashMap<>();

    /*
     * Introduced with issue 4973
     */
    /**
     * This maps says whether a given test case is locked because of its milestones or not The answer
     * is yes if :
     *
     * <p>
     *
     * <ul>
     *   <li>The entity exists in the database (ie it's not new : status is EXISTS)
     *   <li>The entity indeed belongs to a milestone which status forbids any modification
     * </ul>
     *
     * <p>
     *
     * <p>Note that when the entity doesn't exist yet the answer is always no, because one can never
     * add or remove an entity a locked milestone. Same goes for entities that are deleted or plain
     * inexistant (status NOT_EXIST : neither in DB nor in the import file)
     *
     * <p>
     *
     * <p>Also note that for now we make no distinction between milestones that prevent deletion and
     * milestones that prevent edition because they are exactly the same at the moment
     */
    private final Map<Target, Boolean> isTargetMilestoneLocked = new HashMap<>();

    /** nothing special, plain wysiwyg */
    private final Map<String, ProjectTargetStatus> projectStatusByName = new HashMap<>();

    /**
     * caches the custom fields defined for the test cases in a given project. This is a multimap that
     * matches a project name against a collection of CustomField
     */
    private final MultiValueMap tcCufsPerProjectname = new MultiValueMap();

    /** caches the requirement link roles */
    private Set<String> requirementLinkRoles;

    /** same as tcCufsPerProjectname, but regarding the test steps */
    private final MultiValueMap stepCufsPerProjectname = new MultiValueMap();

    /** same as tcCufsPerProjectname, but regarding requirement */
    private final MultiValueMap reqCufsPerProjectname = new MultiValueMap();

    /** keeps track of which parameters are defined for which test cases */
    private final Map<TestCaseTarget, Collection<ParameterTarget>> parametersByTestCase =
            new HashMap<>();

    /** keeps track of which datasets are defined for which test cases */
    private final Map<TestCaseTarget, Set<DatasetTarget>> datasetsByTestCase = new HashMap<>();

    /**
     * This property keeps track of the test case call graph. It is not initialized like the rest,
     * it's rather initialized on demand (see isCalled for instance )
     */
    private final TestCaseCallGraph callGraph = new TestCaseCallGraph();

    /** This property tracks a hierarchy of requirement folders, requirements and their versions */
    private final ImportedRequirementTree requirementTree = new ImportedRequirementTree();

    public Model(
            CustomCustomFieldDao customFieldDao,
            TestCaseLibraryFinderService finderService,
            RequirementImportDao requirementImportDao,
            TestCaseCallTreeFinder calltreeFinder,
            TestCaseFinder testCaseFinder,
            ParameterFinder paramFinder,
            MilestoneMembershipFinder milestoneMemberFinder,
            DatasetDao dsDao,
            LinkedRequirementVersionManagerService reqlinkService,
            EntityPathHeaderDao pathHeaderDao,
            TestStepDao testStepDao,
            ProjectDao projectDao) {
        this.customFieldDao = customFieldDao;
        this.finderService = finderService;
        this.requirementImportDao = requirementImportDao;
        this.calltreeFinder = calltreeFinder;
        this.testCaseFinder = testCaseFinder;
        this.paramFinder = paramFinder;
        this.milestoneMemberFinder = milestoneMemberFinder;
        this.dsDao = dsDao;
        this.reqlinkService = reqlinkService;
        this.pathHeaderDao = pathHeaderDao;
        this.testStepDao = testStepDao;
        this.projectDao = projectDao;
    }

    // ===============================================================================================
    // ===============================================================================================

    // ************************** project access
    // *****************************************************

    public ProjectTargetStatus getProjectStatus(String projectName) {
        projectName = PathUtils.unescapePathPartSlashes(projectName);
        if (!projectStatusByName.containsKey(projectName)) {
            initProject(projectName);
        }
        return projectStatusByName.get(projectName);
    }

    // *************** Test Case status management ***************

    public void initStatuses(Set<TestCaseTarget> targets) {
        Set<TestCaseTarget> targetList =
                targets.stream()
                        .filter(target -> !testCaseStatusByTarget.containsKey(target))
                        .collect(Collectors.toSet());

        if (!targetList.isEmpty()) {
            mainInitTestCase(targetList);
        }
    }

    public TargetStatus getStatus(TestCaseTarget target) {
        LOGGER.debug("searching status for test case target : '{}'", target);
        if (!testCaseStatusByTarget.containsKey(target)) {
            mainInitTestCase(target);
        }

        TargetStatus status = testCaseStatusByTarget.get(target);
        LOGGER.trace("status for test case target '{}' : {}", target, status);

        return status;
    }

    public Map<TestCaseTarget, Long> getTargetIds(Set<TestCaseTarget> targets) {
        initStatuses(targets);

        return targets.stream()
                .map(target -> Map.entry(target, testCaseStatusByTarget.get(target)))
                .filter(entry -> entry.getValue() != null && entry.getValue().id != null)
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().id));
    }

    public void setExists(TestCaseTarget target, Long id) {
        LOGGER.trace("setting test case target '{}' to ");
        target.setId(id);
        testCaseStatusByTarget.put(target, new TargetStatus(EXISTS, id));
    }

    public void setToBeCreated(TestCaseTarget target) {
        testCaseStatusByTarget.put(target, new TargetStatus(Existence.TO_BE_CREATED));
        clearSteps(target);
    }

    public void setToBeDeleted(TestCaseTarget target) {
        testCaseStatusByTarget.computeIfPresent(
                target, (k, oldStatus) -> new TargetStatus(Existence.TO_BE_DELETED, oldStatus.id));
        clearSteps(target);
        callGraph.removeNode(target);
    }

    public void setDeleted(TestCaseTarget target) {
        testCaseStatusByTarget.put(target, new TargetStatus(NOT_EXISTS, null));
        clearSteps(target);
        callGraph.removeNode(target);
    }

    /** virtually an alias of setDeleted */
    public void setNotExists(TestCaseTarget target) {
        testCaseStatusByTarget.put(target, new TargetStatus(NOT_EXISTS, null));
        clearSteps(target);
    }

    private void clearSteps(TestCaseTarget target) {
        if (testCaseStepsByTarget.containsKey(target)) {
            testCaseStepsByTarget.get(target).clear();
        }
    }

    // *************** Test Case accessors ***************

    /** may return null */
    public Long getId(TestCaseTarget target) {
        return getStatus(target).id;
    }

    public TestCase get(TestCaseTarget target) {
        Long id = getId(target);
        if (id == null) {
            return null;
        } else {
            return testCaseFinder.findById(id);
        }
    }

    public boolean isTestCaseLockedByMilestones(TestCaseTarget target) {
        if (!testCaseStatusByTarget.containsKey(target)) {
            mainInitTestCase(target);
        }

        return isTargetMilestoneLocked.get(target);
    }

    // *************** test case calls code ***************

    /**
     * returns true if the test case is being called by another test case or else false.
     *
     * <p>Note : the problem arises only if the test case already exists in the database (test cases
     * instructions are all processed before step-instructions are thus newly imported test cases
     * aren't bound to any test step yet).
     */
    public boolean isCalled(TestCaseTarget target) {

        if (!callGraph.knowsNode(target)) {
            initCallGraph(target);
        }

        return callGraph.isCalled(target);
    }

    public boolean wouldCreateCycle(TestCaseTarget srcTestCase, TestCaseTarget destTestCase) {
        if (!callGraph.knowsNode(srcTestCase)) {
            initCallGraph(srcTestCase);
        }

        if (!callGraph.knowsNode(destTestCase)) {
            initCallGraph(destTestCase);
        }

        return callGraph.wouldCreateCycle(srcTestCase, destTestCase);
    }

    public boolean wouldCreateCycle(TestStepTarget step, TestCaseTarget destTestCase) {
        return wouldCreateCycle(step.getTestCase(), destTestCase);
    }

    /**
     * initialize the call graph from the database. The whole graph will be pulled so be carefull to
     * load it only when necessary.
     */
    private void initCallGraph(TestCaseTarget target) {

        Long id = finderService.findNodeIdByPath(target.getPath());
        if (id == null) {
            // this is probably a new node
            callGraph.addNode(target);
            return;
        }

        LibraryGraph<NamedReference, SimpleNode<NamedReference>> targetCallers =
                calltreeFinder.getExtendedGraph(singletonList(id));

        // some data transform now
        Collection<SimpleNode<NamedReference>> refs = targetCallers.getNodes();
        swapNameForPath(refs);

        // now create the graph
        callGraph.addGraph(targetCallers);
    }

    public void initCallGraph(Collection<TestCaseTarget> targets) {
        Set<Long> testCaseIds =
                targets.stream()
                        .filter(target -> !callGraph.knowsNode(target))
                        .map(
                                target -> {
                                    Long id = getId(target);
                                    if (id == null) {
                                        callGraph.addNode(target);
                                    }
                                    return id;
                                })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());

        if (!testCaseIds.isEmpty()) {
            initRecursiveCallGraph(testCaseIds);
        }
    }

    public void initRecursiveCallGraph(Collection<Long> testCaseIds) {
        LibraryGraph<NamedReference, SimpleNode<NamedReference>> targetCallers =
                calltreeFinder.getExtendedGraph(testCaseIds);

        // some data transform now
        Collection<SimpleNode<NamedReference>> refs = targetCallers.getNodes();
        swapNameForPath(refs);

        // now create the graph
        callGraph.addGraph(targetCallers);
    }

    private void addCallGraphEdge(TestCaseTarget src, TestCaseTarget dest) {

        if (!callGraph.knowsNode(src)) {
            initCallGraph(src);
        }

        if (!callGraph.knowsNode(dest)) {
            initCallGraph(dest);
        }

        callGraph.addEdge(src, dest);
    }

    // *************** Test Step management ***************

    /** Adds a step of the specified type to the model. Not to the database. */
    public void addActionStep(TestStepTarget target) {
        List<InternalStepModel> steps = findInternalStepModels(target);

        Integer index = target.getIndex();

        if (index == null || index >= steps.size() || index < 0) {
            index = steps.size();
        }

        steps.add(index, new InternalStepModel(StepType.ACTION, null));
    }

    public Integer addCallStep(
            TestStepTarget target,
            TestCaseTarget calledTestCase,
            ParameterAssignationMode parameterMode) {

        boolean delegates = ParameterAssignationMode.DELEGATE.equals(parameterMode);

        // set the call graph
        addCallGraphEdge(target.getTestCase(), calledTestCase);

        // set the model
        List<InternalStepModel> steps = findInternalStepModels(target);

        Integer index = target.getIndex();

        if (index == null || index >= steps.size() || index < 0) {
            index = steps.size();
        }

        steps.add(index, new InternalStepModel(StepType.CALL, calledTestCase, delegates));

        return index;
    }

    /**
     * warning : won't check that the operation will not create a cycle. Such check needs to be done
     * beforehand.
     */
    public void updateCallStepTarget(
            TestStepTarget step, TestCaseTarget newTarget, CallStepParamsInfo paramInfo) {

        if (!stepExists(step)) {
            throw new IllegalArgumentException("cannot update non existant step '" + step + "'");
        }

        if (getType(step) != StepType.CALL) {
            throw new IllegalArgumentException(
                    "cannot update the called test case for step '"
                            + step
                            + "' because that step is not a call step");
        }

        InternalStepModel model = findInternalStepModel(step);
        boolean delegates = ParameterAssignationMode.DELEGATE.equals(paramInfo.getParamMode());
        model.setDelegates(delegates);
        TestCaseTarget src = step.getTestCase();
        TestCaseTarget oldDest = model.getCalledTC();

        // update the model
        model.setCalledTC(newTarget);

        // update the call graph
        callGraph.removeEdge(src, oldDest);
        callGraph.addEdge(src, newTarget);
    }

    public void remove(TestStepTarget target) {

        if (!stepExists(target)) {
            throw new IllegalArgumentException("cannot remove non existant step '" + target + "'");
        }

        List<InternalStepModel> steps = findInternalStepModels(target);
        Integer index = target.getIndex();

        // remove from the model
        // .intValue() disambiguate with the other method - remove(Object) -
        InternalStepModel step = steps.remove(index.intValue());

        // remove from the call graph
        if (step.type == StepType.CALL) {
            callGraph.removeEdge(target.getTestCase(), step.getCalledTC());
        }
    }

    // *************** Test Step accessors ***************

    public boolean stepExists(TestStepTarget target) {

        InternalStepModel model = findInternalStepModel(target);

        return model != null;
    }

    public boolean indexIsFirstAvailable(TestStepTarget target) {
        Integer index = target.getIndex();
        TestCaseTarget tc = target.getTestCase();
        if (!testCaseStatusByTarget.containsKey(tc)) {
            mainInitTestCase(tc);
        }
        List<InternalStepModel> steps = testCaseStepsByTarget.get(tc);
        if (index == null || steps == null) {
            return false;
        } else {
            return index == steps.size();
        }
    }

    /**
     * @return null if the step is unknown
     */
    public StepType getType(TestStepTarget target) {

        InternalStepModel model = findInternalStepModel(target);

        if (model != null) {
            return model.getType();
        } else {
            return null;
        }
    }

    /** may return null */
    public TestStep getStep(TestStepTarget target) {

        Long tcId = getStatus(target.getTestCase()).id;
        Integer index = target.getIndex();

        // this condition is heavily defensive and the caller code should check
        // those beforehand
        if (!stepExists(target) || tcId == null || index == null) {
            return null;
        }

        return testStepDao.findByTestCaseAndPosition(tcId, index);
    }

    private List<InternalStepModel> findInternalStepModels(TestStepTarget step) {
        TestCaseTarget tc = step.getTestCase();

        if (!testCaseStatusByTarget.containsKey(tc)) {
            mainInitTestCase(tc);
        }

        return testCaseStepsByTarget.get(tc);
    }

    private InternalStepModel findInternalStepModel(TestStepTarget step) {
        Integer index = step.getIndex();
        List<InternalStepModel> steps = findInternalStepModels(step);

        if (index != null && steps.size() > index && index >= 0) {
            return steps.get(index);
        } else {
            return null;
        }
    }

    // *************** parameters ***************

    public boolean doesParameterExists(ParameterTarget target) {
        TestCaseTarget tc = target.getOwner();
        if (!parametersByTestCase.containsKey(tc)) {
            initParameters(singleton(tc));
        }

        return parametersByTestCase.get(tc).contains(target);
    }

    public void addParameter(ParameterTarget target) {
        TestCaseTarget tc = target.getOwner();
        if (!parametersByTestCase.containsKey(tc)) {
            initParameters(singleton(tc));
        }

        parametersByTestCase.get(tc).add(target);
    }

    public void removeParameter(ParameterTarget target) {
        TestCaseTarget tc = target.getOwner();
        if (!parametersByTestCase.containsKey(tc)) {
            initParameters(singleton(tc));
        }

        parametersByTestCase.get(tc).remove(target);
    }

    /**
     * returns the parameters owned by this test case. It doesn't include all the parameters from the
     * test case call tree of this test case.
     */
    public Collection<ParameterTarget> getOwnParameters(TestCaseTarget testCase) {
        if (!parametersByTestCase.containsKey(testCase)) {
            initParameters(singleton(testCase));
        }

        return parametersByTestCase.get(testCase);
    }

    /**
     * returns all parameters available to a test case. This includes every ParameterTarget from the
     * test cases being called directly or indirectly by this test case, not just the one owner by the
     * test case (unlike getOwnParameters). Parameters from downstream test cases will be included if
     * they are inherited in some ways.
     */
    public Collection<ParameterTarget> getAllParameters(TestCaseTarget testCase) {

        if (!callGraph.knowsNode(testCase)) {
            initCallGraph(testCase);
        }

        Collection<ParameterTarget> result = new HashSet<>();
        LinkedList<Node> processing = new LinkedList<>();

        processing.add(callGraph.getNode(testCase));

        while (!processing.isEmpty()) {
            Node current = processing.pop();
            result.addAll(getOwnParameters(current.getKey()));

            // modification patron
            for (Node child : current.getOutbounds()) {

                List<InternalStepModel> steps = testCaseStepsByTarget.get(current.getKey());
                extractParametersFromSteps(processing, child, steps);
            }
        }

        return result;
    }

    private void extractParametersFromSteps(
            Collection<Node> processing, Node child, List<InternalStepModel> steps) {
        if (steps != null) {
            for (InternalStepModel step : steps) {
                if (step.type == StepType.CALL
                        && step.calledTC.equals(child.getKey())
                        && step.getDeleguates()) {
                    // FIXME calledTC is not a Node so there's probably a bug here
                    processing.add(child);
                }
            }
        }
    }

    /**
     * @return true if the parameter legitimately belongs to the dataset, false otherwise
     */
    public boolean isParamInDataset(ParameterTarget param, DatasetTarget ds) {
        Collection<ParameterTarget> allparams = getAllParameters(ds.getTestCase());
        return allparams.contains(param);
    }

    // **************************** datasets
    // ****************************************

    public boolean datasetNotExist(DatasetTarget target) {
        TestCaseTarget tc = target.getTestCase();
        if (!datasetsByTestCase.containsKey(tc)) {
            initDatasets(singletonList(tc));
        }

        return !datasetsByTestCase.get(tc).contains(target);
    }

    /** This operation is imdepotent */
    public void addDataset(DatasetTarget target) {
        TestCaseTarget tc = target.getTestCase();
        if (!datasetsByTestCase.containsKey(tc)) {
            initDatasets(singletonList(tc));
        }

        datasetsByTestCase.get(tc).add(target);
    }

    public void addDatasets(List<DatasetTarget> datasetTargets) {
        Set<TestCaseTarget> targetList =
                datasetTargets.stream()
                        .map(DatasetTarget::getTestCase)
                        .filter(target -> !datasetsByTestCase.containsKey(target))
                        .collect(Collectors.toSet());

        if (!targetList.isEmpty()) {
            initDatasets(targetList);
        }
    }

    public void removeDataset(DatasetTarget target) {
        TestCaseTarget tc = target.getTestCase();
        if (!datasetsByTestCase.containsKey(tc)) {
            initDatasets(singletonList(tc));
        }

        datasetsByTestCase.get(tc).remove(target);
    }

    /**
     * returns the parameters owned by this test case. It doesn't include all the parameters from the
     * test case call tree of this test case.
     */
    public Collection<DatasetTarget> getDatasets(TestCaseTarget testCase) {
        if (!datasetsByTestCase.containsKey(testCase)) {
            initDatasets(singletonList(testCase));
        }

        return datasetsByTestCase.get(testCase);
    }

    // *************** Custom fields accessors ***************

    @SuppressWarnings(UNCHECKED)
    public Collection<CustomField> getTestCaseCufs(TestCaseTarget target) {
        if (!testCaseStatusByTarget.containsKey(target)) {
            mainInitTestCase(target);
        }

        String projectName = PathUtils.extractProjectName(target.getPath());
        Collection<CustomField> cufs = tcCufsPerProjectname.getCollection(projectName);

        return Objects.requireNonNullElse(cufs, emptyList());
    }

    @SuppressWarnings(UNCHECKED)
    public Collection<CustomField> getRequirementVersionCufs(RequirementVersionTarget target) {
        // if requirement version is unknown in model (ie in req tree),
        // init the requirement version
        if (!requirementTree.targetAlreadyLoaded(target)) {
            initRequirementVersion(target);
        }

        String projectName = PathUtils.extractProjectName(target.getPath());
        Collection<CustomField> cufs = reqCufsPerProjectname.getCollection(projectName);

        return Objects.requireNonNullElse(cufs, emptyList());
    }

    private void initRequirementVersion(RequirementVersionTarget target) {
        ImportedRequirementNode req = requirementTree.getNode(target.getRequirement());

        if (req == null) {
            mainInitRequirements(target);
        } else {
            requirementTree.addVersion(target, TargetStatus.NOT_EXISTS);
        }
    }

    @SuppressWarnings(UNCHECKED)
    public Collection<CustomField> getTestStepCufs(TestStepTarget target) {
        TestCaseTarget tc = target.getTestCase();

        if (!testCaseStatusByTarget.containsKey(tc)) {
            mainInitTestCase(tc);
        }

        String projectName = PathUtils.extractProjectName(tc.getPath());
        Collection<CustomField> cufs = stepCufsPerProjectname.getCollection(projectName);

        return Objects.requireNonNullElse(cufs, emptyList());
    }

    // *************** Requirement management ***************

    public TargetStatus getStatus(RequirementTarget target) {
        if (!requirementTree.targetAlreadyLoaded(target)) {
            loadRequirements(Collections.singleton(target));
        }

        return requirementTree.getStatus(target);
    }

    public TargetStatus getStatus(RequirementVersionTarget target) {
        // init the requirement version in tree if unknown
        if (!requirementTree.targetAlreadyLoaded(target)) {
            mainInitRequirements(target);
        }

        return requirementTree.getStatus(target);
    }

    public Set<String> getRequirementLinkRoles() {
        if (requirementLinkRoles == null) {
            requirementLinkRoles = Collections.unmodifiableSet(reqlinkService.findAllRoleCodes());
        }
        return requirementLinkRoles;
    }

    // *************** loading code ***************

    public void mainInitTestCase(TestCaseTarget target) {
        mainInitTestCase(singleton(target));
    }

    public void mainInitTestCase(Set<TestCaseTarget> targets) {
        // init the test cases
        initTestCases(targets);

        // init the steps
        initTestSteps(targets);

        // init the projects
        initProjects(targets);
    }

    private void initTestCases(Set<TestCaseTarget> initialTargets) {

        // filter out the test cases we already know of
        List<TestCaseTarget> targets =
                initialTargets.stream()
                        .filter(target -> !testCaseStatusByTarget.containsKey(target))
                        .toList();

        // exit if they are all known
        if (targets.isEmpty()) {
            return;
        }

        Map<String, List<TestCaseTarget>> targetsByProjectName =
                initialTargets.stream().collect(Collectors.groupingBy(TestCaseTarget::getProject));

        targetsByProjectName.forEach(this::initTestCaseInProject);
    }

    private void initTestCaseInProject(String projectName, List<TestCaseTarget> targetList) {
        Map<Long, TestCaseTarget> existingTargets = new HashMap<>();
        TreeMap<String, Long> testCasePathsInProject =
                finderService.buildTestCasePathsTree(projectName);

        for (TestCaseTarget target : targetList) {
            String path = PathUtils.asSquashPath(target.getPath());

            Long id = testCasePathsInProject.get(path);

            Existence existence = id == null ? NOT_EXISTS : EXISTS;
            TargetStatus status = new TargetStatus(existence, id);

            testCaseStatusByTarget.put(target, status);

            if (existence == EXISTS) {
                target.setId(id);
                existingTargets.put(id, target);
            } else {
                isTargetMilestoneLocked.put(target, false);
            }
        }

        if (existingTargets.isEmpty()) {
            return;
        }

        List<Long> testIdsBoundToBlockingMilestone =
                milestoneMemberFinder.findTestCaseIdsBoundToBlockingMilestone(existingTargets.keySet());
        existingTargets.forEach(
                (testCaseId, existingTarget) ->
                        isTargetMilestoneLocked.put(
                                existingTarget, testIdsBoundToBlockingMilestone.contains(testCaseId)));
    }

    public void initParameters(Set<TestCaseTarget> testCaseTargets) {
        Map<Long, TestCaseTarget> testCaseTargetById =
                collectExistingTestCaseTargetById(testCaseTargets);

        testCaseTargets.stream()
                .filter(target -> !parametersByTestCase.containsKey(target))
                .forEach(target -> parametersByTestCase.putIfAbsent(target, new HashSet<>()));

        if (testCaseTargetById.isEmpty()) {
            return;
        }

        Map<Long, List<Parameter>> parametersByTestCaseId =
                paramFinder.findOwnParametersByTestCaseIds(testCaseTargetById.keySet());

        for (var entry : testCaseTargetById.entrySet()) {
            List<Parameter> parameters = parametersByTestCaseId.getOrDefault(entry.getKey(), List.of());

            Set<ParameterTarget> parameterTargets =
                    parameters.stream()
                            .map(p -> new ParameterTarget(entry.getValue(), p.getName()))
                            .collect(Collectors.toSet());

            parametersByTestCase.computeIfAbsent(entry.getValue(), key -> parameterTargets);
        }
    }

    private Map<Long, TestCaseTarget> collectExistingTestCaseTargetById(
            Set<TestCaseTarget> testCaseTargets) {
        initStatuses(testCaseTargets);

        return testCaseTargets.stream()
                .filter(target -> !parametersByTestCase.containsKey(target))
                .map(target -> Map.entry(target, getStatus(target)))
                .filter(
                        entry ->
                                entry.getValue().id != null && entry.getValue().status != Existence.TO_BE_DELETED)
                .collect(Collectors.toMap(entry -> entry.getValue().id, Map.Entry::getKey, (a, b) -> a));
    }

    private void initDatasets(Collection<TestCaseTarget> testCaseTargets) {
        Map<Long, TestCaseTarget> testCaseTargetById =
                testCaseTargets.stream()
                        .filter(target -> !datasetsByTestCase.containsKey(target))
                        .map(target -> Map.entry(target, getStatus(target)))
                        .filter(
                                entry ->
                                        entry.getValue().id != null
                                                && entry.getValue().status != Existence.TO_BE_DELETED)
                        .collect(
                                Collectors.toMap(entry -> entry.getValue().id, Map.Entry::getKey, (a, b) -> a));

        Map<Long, List<String>> datasetsByTestCaseId =
                dsDao.findOwnDatasetNamesByTestCaseIds(testCaseTargetById.keySet());

        for (var entry : testCaseTargetById.entrySet()) {
            List<String> datasetsNames =
                    datasetsByTestCaseId.getOrDefault(entry.getKey(), Collections.emptyList());
            TestCaseTarget target = entry.getValue();

            Set<DatasetTarget> datasetTarget = new HashSet<>(datasetsNames.size());
            for (String name : datasetsNames) {
                datasetTarget.add(new DatasetTarget(target, name));
            }

            datasetsByTestCase.put(target, datasetTarget);
        }

        testCaseTargets.stream()
                .filter(target -> !datasetsByTestCase.containsKey(target))
                .forEach(target -> datasetsByTestCase.putIfAbsent(target, new HashSet<>()));
    }

    /**
     * this method assumes that the targets were all processed through initTestCases(targets)
     * beforehand.
     */
    private void initTestSteps(Set<TestCaseTarget> targets) {
        Map<Long, List<TestCaseTarget>> targetsByTestCaseId =
                targets.stream()
                        .filter(
                                target ->
                                        !testCaseStepsByTarget.containsKey(target)) // do not double process the steps
                        .filter(
                                target -> {
                                    TargetStatus status = testCaseStatusByTarget.get(target);
                                    return status.id != null && status.status != Existence.TO_BE_DELETED;
                                })
                        .collect(
                                Collectors.groupingBy(
                                        target -> testCaseStatusByTarget.get(target).id,
                                        HashMap::new,
                                        Collectors.toList()));

        targets.stream()
                .filter(target -> !testCaseStepsByTarget.containsKey(target))
                .forEach(target -> testCaseStepsByTarget.putIfAbsent(target, new ArrayList<>()));

        if (targetsByTestCaseId.isEmpty()) {
            return;
        }

        Map<Long, List<InternalStepModel>> stepModelsByTestCaseId =
                loadStepsModel(targetsByTestCaseId.keySet());

        stepModelsByTestCaseId.forEach(
                (id, steps) -> {
                    List<TestCaseTarget> testCaseTargets = targetsByTestCaseId.getOrDefault(id, List.of());
                    testCaseTargets.forEach(target -> testCaseStepsByTarget.put(target, steps));
                });
    }

    public boolean isHighLevelRequirement(RequirementTarget target) {
        return requirementTree.isHighLevelRequirement(target);
    }

    public Map<ImportedRequirementNode, Set<ImportedRequirementNode>> getMissingNodes() {
        return requirementTree.collectMissingNode();
    }

    public boolean isCurrentVersion(RequirementVersionTarget version) {
        return requirementTree.getNode(version.getRequirement()).isCurrentVersion(version.getVersion());
    }

    public void fixRequirementVersion(RequirementVersionTarget target) {
        requirementTree.fixVersion(target);
    }

    public boolean existPriorVersion(RequirementVersionTarget target) {
        return requirementTree.existPriorVersion(target);
    }

    public Long getRequirementVersionId(RequirementVersionTarget versionTarget) {
        Long id = versionTarget.getId();

        if (id == null) {
            id = requirementTree.getVersionId(versionTarget.getRequirement(), versionTarget.getVersion());
            versionTarget.setId(id);
        }

        return id;
    }

    public Long getTestCaseId(TestCaseTarget testCase) {
        TargetStatus status = getStatus(testCase);

        return status.getId();
    }

    public void setExists(RequirementVersionTarget target, Long id) {
        requirementTree.updateVersionStatus(target, new TargetStatus(EXISTS, id));
    }

    public void setExists(RequirementTarget target, Long id) {
        target.setId(id);
        requirementTree.updateStatus(target, new TargetStatus(EXISTS, id));
    }

    private record StepDetail(String stepType, Long calledTestCase, boolean delegate) {}

    private Map<Long, List<InternalStepModel>> loadStepsModel(Collection<Long> tcIds) {
        List<Object[]> stepsData = testStepDao.findTestStepsDetails(tcIds);

        Map<Long, List<StepDetail>> stepDetailsByTestCaseId =
                stepsData.stream()
                        .map(this::createStepDetail)
                        .collect(
                                Collectors.groupingBy(
                                        Map.Entry::getKey,
                                        Collectors.mapping(Map.Entry::getValue, Collectors.toList())));

        Map<Long, String> calledTestCasePath =
                stepsData.stream()
                        .map(data -> (Long) data[2])
                        .filter(Objects::nonNull)
                        .collect(Collectors.collectingAndThen(Collectors.toSet(), this::getTestCasePathByIds));

        return stepDetailsByTestCaseId.entrySet().stream()
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                entry -> createInternalStepModels(entry.getValue(), calledTestCasePath)));
    }

    private Map<Long, String> getTestCasePathByIds(Set<Long> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyMap();
        }
        return pathHeaderDao.buildTestCasePathByIds(ids, "/").entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> "/" + entry.getValue()));
    }

    private Map.Entry<Long, StepDetail> createStepDetail(Object[] data) {
        Long testCaseId = (Long) data[0];
        String stepType = (String) data[1];
        Long calledTC = (Long) data[2];
        Boolean delegates = (Boolean) data[3];

        StepDetail stepDetail = new StepDetail(stepType, calledTC, delegates);
        return Map.entry(testCaseId, stepDetail);
    }

    private List<InternalStepModel> createInternalStepModels(
            List<StepDetail> stepDetails, Map<Long, String> calledTestCasePath) {
        return stepDetails.stream()
                .map(
                        stepDetail -> {
                            StepType type = StepType.valueOf(stepDetail.stepType);
                            TestCaseTarget calledTC = null;
                            boolean delegates = false;

                            if (type == StepType.CALL) {
                                if (stepDetail.calledTestCase != null) {
                                    String path = calledTestCasePath.get(stepDetail.calledTestCase);
                                    calledTC = new TestCaseTarget(path);
                                }
                                delegates = stepDetail.delegate;
                            }

                            return new InternalStepModel(type, calledTC, delegates);
                        })
                .collect(Collectors.toList());
    }

    private void initProject(String projectName) {
        initProjectsByName(singletonList(projectName));
    }

    private void initProjects(Set<TestCaseTarget> targets) {
        initProjectsByName(collectProjects(targets));
    }

    private void initProjectsByName(Collection<String> allNames) {
        // [Issue 6032] unescape the projects names
        List<String> allUnescapedNames = PathUtils.unescapePathPartSlashes(allNames);

        // filter out projects we already know of
        List<String> projectNames = new LinkedList<>();
        for (String name : allUnescapedNames) {
            if (!projectStatusByName.containsKey(name)) {
                projectNames.add(name);
            }
        }

        // exit if they are all known
        if (projectNames.isEmpty()) {
            return;
        }

        // now begin
        List<ProjectLibrariesIds> projects = projectDao.getLibrariesIdsByNames(projectNames);

        // add the projects that were found
        for (ProjectLibrariesIds p : projects) {
            Long projectId = p.projectId();
            LOGGER.debug("ReqImport - Trying to import project in model {}", projectId);
            ProjectTargetStatus status =
                    new ProjectTargetStatus(
                            EXISTS, projectId, p.testCaseLibraryId(), p.requirementLibraryId());
            projectStatusByName.put(p.name(), status);
        }

        initCustomFields(projects.stream().map(ProjectLibrariesIds::projectId).toList());

        // add the projects that weren't found
        projectNames.stream()
                .filter(name -> !projectStatusByName.containsKey(name))
                .forEach(name -> projectStatusByName.put(name, new ProjectTargetStatus(NOT_EXISTS)));
    }

    /** assumes that the project exists and that we have its ID */
    private void initCustomFields(List<Long> projectIds) {
        List<BindableEntity> bindableEntities =
                List.of(
                        BindableEntity.TEST_CASE, BindableEntity.TEST_STEP, BindableEntity.REQUIREMENT_VERSION);

        var customFieldsByProjectNames =
                customFieldDao.findByProjectIdsAndEntities(projectIds, bindableEntities);

        customFieldsByProjectNames.forEach(
                (projectName, customFieldsByEntity) -> {
                    Optional.ofNullable(customFieldsByEntity.get(BindableEntity.TEST_CASE))
                            .ifPresent(customFields -> tcCufsPerProjectname.putAll(projectName, customFields));

                    Optional.ofNullable(customFieldsByEntity.get(BindableEntity.TEST_STEP))
                            .ifPresent(customFields -> stepCufsPerProjectname.putAll(projectName, customFields));

                    Optional.ofNullable(customFieldsByEntity.get(BindableEntity.REQUIREMENT_VERSION))
                            .ifPresent(customFields -> reqCufsPerProjectname.putAll(projectName, customFields));
                });
    }

    public void mainInitRequirements(RequirementVersionTarget target) {
        mainInitRequirements(Collections.singleton(target));
    }

    public void mainInitRequirements(Set<RequirementVersionTarget> targets) {
        mainInitRequirementVersions(targets);
        initRequirementProjects(targets);
    }

    private void initRequirementProjects(Collection<RequirementVersionTarget> uniqueTargets) {
        Set<String> projectPaths = collectRequirementProjects(uniqueTargets);
        LOGGER.debug("ReqImport - Looking for project {}", projectPaths);
        initProjectsByName(projectPaths);
    }

    public void initRequirements(Set<RequirementVersionTarget> targets) {
        initRequirementVersions(targets);
        initRequirementProjects(targets);
    }

    private void initRequirementVersions(Set<RequirementVersionTarget> versionTargets) {
        List<RequirementVersionTarget> targets =
                versionTargets.stream()
                        .filter(target -> !requirementTree.targetAlreadyLoaded(target))
                        .toList();

        // exit if they are all known
        if (targets.isEmpty()) {
            return;
        }

        Set<RequirementTarget> requirementTargets =
                versionTargets.stream()
                        .map(RequirementVersionTarget::getRequirement)
                        .filter(requirement -> !requirementTree.targetAlreadyLoaded(requirement))
                        .collect(Collectors.toSet());

        loadRequirements(requirementTargets);

        loadRequirementVersions(targets);
    }

    /*
     * Load target nodes only from the database.
     */
    private void loadRequirements(Set<RequirementTarget> targets) {
        if (targets.isEmpty()) {
            return;
        }

        Set<String> projects = collectProjects(targets);

        ImportRequirementFinder finder =
                new ImportRequirementFinder(requirementImportDao, Collections.emptySet());

        finder.fetch(targets, projects);

        requirementTree.addNodes(targets, finder);
    }

    public void mainInitRequirementVersions(Collection<RequirementVersionTarget> versionTargets) {
        LOGGER.debug("ReqImport - Initialize targets");

        // filter out the requirement version we already know of
        List<RequirementVersionTarget> targets =
                versionTargets.stream()
                        .filter(target -> !requirementTree.targetAlreadyLoaded(target))
                        .toList();

        // exit if they are all known
        if (targets.isEmpty()) {
            return;
        }

        mainLoadRequirements(targets);
        loadRequirementVersions(targets);
    }

    private void loadRequirementVersions(List<RequirementVersionTarget> targets) {
        Map<RequirementTarget, Map<Integer, RequirementVersionTarget>> versionsByTarget =
                targets.stream()
                        .collect(
                                Collectors.groupingBy(
                                        RequirementVersionTarget::getRequirement,
                                        Collectors.toMap(RequirementVersionTarget::getVersion, Function.identity())));

        Map<Long, Map<Integer, ReqVersionMilestone>> versionMilestonesByRequirementId =
                getVersionsMilestones(targets);

        versionsByTarget.forEach(
                (requirement, versions) -> {
                    Map<Integer, ReqVersionMilestone> versionNumberMilestones =
                            versionMilestonesByRequirementId.getOrDefault(
                                    getRequirementId(requirement), emptyMap());

                    versions.entrySet().stream()
                            .filter(entry -> !versionNumberMilestones.containsKey(entry.getKey()))
                            .forEach(
                                    entry ->
                                            requirementTree.addVersion(entry.getValue(), new TargetStatus(NOT_EXISTS)));

                    initializeExistingVersions(requirement, versions, versionNumberMilestones);
                });
    }

    private Map<Long, Map<Integer, ReqVersionMilestone>> getVersionsMilestones(
            Collection<RequirementVersionTarget> versionTargets) {
        Set<Long> requirementIds =
                versionTargets.stream()
                        .filter(this::isRequirementExisting)
                        .map(requirementTree::getNodeId)
                        .collect(Collectors.toSet());

        return requirementImportDao.findReqVersionAndMilestonesByReqId(requirementIds);
    }

    /*
     * Load the requirements from the database.
     * This method searches for the first existing parent if the target node doesn't exist.
     * Necessary only for creating requirement versions, otherwise use #loadRequirements() method
     */
    private void mainLoadRequirements(List<RequirementVersionTarget> versionTargets) {
        Set<RequirementTarget> targets =
                versionTargets.stream()
                        .map(RequirementVersionTarget::getRequirement)
                        .filter(requirement -> !requirementTree.targetAlreadyLoaded(requirement))
                        .collect(Collectors.toSet());

        if (targets.isEmpty()) {
            return;
        }

        String projectName = targets.stream().findFirst().orElseThrow().getProject();

        Long requirementLibraryId = projectDao.findRequirementLibraryIdByName(projectName);

        ImportRequirementFinder finder = new ImportRequirementFinder(requirementImportDao, targets);

        finder.fetch(targets, projectName);

        requirementTree.addNodes(targets, finder, projectName, requirementLibraryId);
    }

    private boolean isRequirementExisting(RequirementVersionTarget target) {
        return isRequirementExisting(target.getRequirement());
    }

    private boolean isRequirementExisting(RequirementTarget target) {
        return getStatus(target).getStatus() == EXISTS;
    }

    private void initializeExistingVersions(
            RequirementTarget requirementTarget,
            Map<Integer, RequirementVersionTarget> versions,
            Map<Integer, ReqVersionMilestone> milestonesByVersionNum) {

        milestonesByVersionNum.forEach(
                (versionNumber, reqVersionMilestones) -> {
                    RequirementVersionTarget targetVersion =
                            versions.getOrDefault(
                                    versionNumber, new RequirementVersionTarget(requirementTarget, versionNumber));

                    requirementTree.addVersion(
                            targetVersion, new TargetStatus(EXISTS, reqVersionMilestones.getReqVersionId()));

                    bindMilestones(reqVersionMilestones, targetVersion);
                });
    }

    private void bindMilestones(
            ReqVersionMilestone reqVersionMilestones, RequirementVersionTarget targetVersion) {
        reqVersionMilestones
                .getMilestoneStatusByLabel()
                .forEach(
                        (label, status) -> {
                            requirementTree.bindMilestone(targetVersion, label);
                            if (MilestoneStatus.LOCKED.equals(status) || MilestoneStatus.PLANNED.equals(status)) {
                                requirementTree.milestoneLock(targetVersion);
                            }
                        });
    }

    public void updateRequirementVersionStatus(
            RequirementVersionTarget target, TargetStatus targetStatus, List<String> milestones) {
        requirementTree.updateVersionStatus(target, targetStatus);
        requirementTree.bindMilestone(target, milestones);
    }

    public void updateRequirementStatus(RequirementTarget target, TargetStatus status) {
        requirementTree.updateStatus(target, status);
    }

    public Long getRequirementId(RequirementVersionTarget target) {
        return getRequirementId(target.getRequirement());
    }

    public Long getRequirementId(RequirementTarget target) {
        Long id = target.getId();

        if (id == null) {
            id = requirementTree.getNodeId(target);
            target.setId(id);
        }

        return id;
    }

    // ********************* REQUIREMENT STATUS METHOD *****************
    public void setNotExists(RequirementVersionTarget target) {
        requirementTree.setNotExists(target);
    }

    public boolean checkMilestonesAlreadyUsedInRequirement(
            String milestone, RequirementVersionTarget target) {
        return requirementTree.isMilestoneUsedByOneVersion(target, milestone);
    }

    public boolean isRequirementFolder(RequirementVersionTarget target) {
        return requirementTree.isRequirementFolder(target);
    }

    public boolean isRequirementFolder(RequirementTarget target) {
        return requirementTree.isRequirementFolder(target);
    }

    public void bindMilestonesToRequirementVersion(
            RequirementVersionTarget target, List<String> milestones) {
        requirementTree.bindMilestone(target, milestones);
    }

    public boolean isRequirementVersionLockedByMilestones(RequirementVersionTarget target) {
        if (!requirementTree.targetAlreadyLoaded(target)) {
            mainInitRequirements(target);
        }

        return requirementTree.isMilestoneLocked(target);
    }

    // *************** private methods ***************

    /**
     * note (GRF) I dont know what this is supposed to do but it does not preserve the order of the
     * input list !
     */
    private Set<String> collectProjects(Collection<? extends WithPath> targets) {
        Set<String> paths = collectPaths(targets);
        return PathUtils.extractProjectNames(paths);
    }

    private Set<String> collectRequirementProjects(Collection<RequirementVersionTarget> targets) {
        Set<String> paths = collectRequirementPaths(targets);
        return PathUtils.extractProjectNames(paths);
    }

    private Set<String> collectPaths(Collection<? extends WithPath> targets) {
        return targets.stream().map(WithPath::getPath).collect(Collectors.toSet());
    }

    private Set<String> collectRequirementPaths(Collection<RequirementVersionTarget> targets) {
        return targets.stream()
                .map(target -> target.getRequirement().getPath())
                .collect(Collectors.toSet());
    }

    /**
     * substitutes the value of the name attribute of NamedReference so that it becomes a path
     * instead.<br>
     * All references are supposed to exist in the database that's foul play but saves more bloat
     */
    private void swapNameForPath(Collection<SimpleNode<NamedReference>> references) {

        // first ensures that the references will be iterated in a constant
        // order
        List<SimpleNode<NamedReference>> listedRefs = new ArrayList<>(references);

        // now collect the ids. Node : the javadoc claims that the result is a
        // new list.
        List<Long> ids = listedRefs.stream().map(ref -> ref.getKey().getId()).toList();

        List<String> paths = finderService.getPathsAsString(ids);

        for (int i = 0; i < paths.size(); i++) {
            SimpleNode<NamedReference> currentNode = listedRefs.get(i);
            Long id = ids.get(i);
            String path = paths.get(i);
            currentNode.setKey(new NamedReference(id, path));
        }
    }

    public boolean isRequirementChild(RequirementTarget requirementTarget) {
        return requirementTree.isRequirementChild(requirementTarget);
    }

    // ********************************** Internal types for Test Step
    // management *************************

    private static final class InternalStepModel {
        private final StepType type;
        private TestCaseTarget calledTC;
        private Boolean delegates = null;

        public InternalStepModel(StepType type, TestCaseTarget calledTC) {
            this.type = type;
            this.calledTC = calledTC;
        }

        public InternalStepModel(StepType type, TestCaseTarget calledTC, boolean delegates) {
            this.type = type;
            this.calledTC = calledTC;
            this.delegates = delegates;
        }

        public void setDelegates(boolean delegates) {
            this.delegates = delegates;
        }

        public boolean getDeleguates() {
            return delegates;
        }

        public TestCaseTarget getCalledTC() {
            return calledTC;
        }

        public void setCalledTC(TestCaseTarget calledTC) {
            this.calledTC = calledTC;
        }

        public StepType getType() {
            return type;
        }
    }
}
