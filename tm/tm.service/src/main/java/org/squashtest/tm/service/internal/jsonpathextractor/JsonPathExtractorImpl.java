/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.jsonpathextractor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.jsonpathextractor.JsonPathExtractor;

@Component
public class JsonPathExtractorImpl implements JsonPathExtractor {

    // In theory, A JSON field name can contain any character, but we will limit ourselves to
    // alphanumeric and underscore characters
    static final Pattern validJsonPathPattern =
            Pattern.compile(
                    "((\\p{L}|\\d|_)+|\\[\\d+])"
                            + // first component
                            "(\\.(\\p{L}|\\d|_)+|\\[\\d+])*"); // following components

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public boolean isPathValid(final String path) {
        return validJsonPathPattern.matcher(path).matches();
    }

    public static String extract(final String json, final String path) throws IOException {
        final JsonNode rootNode = objectMapper.readTree(json);
        return extract(rootNode, split(path), 0).asText();
    }

    private static JsonNode extract(
            final JsonNode node, final List<String> pathParts, final int startIndex) {
        if (startIndex == pathParts.size()) {
            return node;
        }
        final String part = pathParts.get(startIndex);
        if (part.startsWith("[")) {
            final int index = Integer.parseInt(part.substring(1, part.length() - 1));
            return extract(node.get(index), pathParts, startIndex + 1);
        } else {
            return extract(node.get(part), pathParts, startIndex + 1);
        }
    }

    private static List<String> split(final String str) {
        final Matcher m = Pattern.compile("\\[\\d+]|\\w+").matcher(str);
        final List<String> matches = new ArrayList<>();
        while (m.find()) {
            matches.add(m.group());
        }
        return matches;
    }
}
