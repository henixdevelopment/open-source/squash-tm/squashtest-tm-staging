/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customfield;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.AbstractCustomFieldValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.CampaignFolderValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.CampaignValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.CustomReportFolderValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.ExecutionStepValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.ExecutionValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.IterationValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.RequirementFolderValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.RequirementVersionValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.SprintGroupValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.SprintValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.TestCaseFolderValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.TestCaseValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.TestStepValuesFactory;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.TestSuiteValuesFactory;

@Service
public class CustomFieldValuesFactoryProvider {

    private final Map<BindableEntity, AbstractCustomFieldValuesFactory> factoryMap;

    public CustomFieldValuesFactoryProvider(
            TestCaseValuesFactory testCaseValuesFactory,
            TestCaseFolderValuesFactory testCaseFolderValuesFactory,
            TestStepValuesFactory testStepValuesFactory,
            RequirementVersionValuesFactory requirementVersionValuesFactory,
            RequirementFolderValuesFactory requirementFolderValuesFactory,
            CampaignValuesFactory campaignValuesFactory,
            CampaignFolderValuesFactory campaignFolderValuesFactory,
            ExecutionValuesFactory executionValuesFactory,
            ExecutionStepValuesFactory executionStepValuesFactory,
            IterationValuesFactory iterationValuesFactory,
            TestSuiteValuesFactory testSuiteValuesFactory,
            SprintValuesFactory sprintValuesFactory,
            SprintGroupValuesFactory sprintGroupValuesFactory,
            CustomReportFolderValuesFactory customReportFolderValuesFactory) {
        factoryMap = new HashMap<>();
        factoryMap.put(BindableEntity.TEST_CASE, testCaseValuesFactory);
        factoryMap.put(BindableEntity.TESTCASE_FOLDER, testCaseFolderValuesFactory);
        factoryMap.put(BindableEntity.TEST_STEP, testStepValuesFactory);
        factoryMap.put(BindableEntity.REQUIREMENT_VERSION, requirementVersionValuesFactory);
        factoryMap.put(BindableEntity.REQUIREMENT_FOLDER, requirementFolderValuesFactory);
        factoryMap.put(BindableEntity.CAMPAIGN, campaignValuesFactory);
        factoryMap.put(BindableEntity.CAMPAIGN_FOLDER, campaignFolderValuesFactory);
        factoryMap.put(BindableEntity.EXECUTION, executionValuesFactory);
        factoryMap.put(BindableEntity.EXECUTION_STEP, executionStepValuesFactory);
        factoryMap.put(BindableEntity.ITERATION, iterationValuesFactory);
        factoryMap.put(BindableEntity.TEST_SUITE, testSuiteValuesFactory);
        factoryMap.put(BindableEntity.SPRINT, sprintValuesFactory);
        factoryMap.put(BindableEntity.SPRINT_GROUP, sprintGroupValuesFactory);
        factoryMap.put(BindableEntity.CUSTOM_REPORT_FOLDER, customReportFolderValuesFactory);
    }

    public AbstractCustomFieldValuesFactory getFactory(BindableEntity entity) {
        AbstractCustomFieldValuesFactory factory = factoryMap.get(entity);
        if (factory == null) {
            throw new IllegalArgumentException(
                    "Unable to instantiate factory, entity " + entity.name() + " is not handled.");
        }
        return factory;
    }
}
