/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.Comparator;
import org.apache.commons.lang3.builder.ToStringBuilder;

public final class LinkedLowLevelRequirementModel {
    public static final Comparator<LinkedLowLevelRequirementModel> LINKED_LOW_LEVEL_REQ_COMPARATOR =
            new Comparator<LinkedLowLevelRequirementModel>() {
                @Override
                public int compare(LinkedLowLevelRequirementModel o1, LinkedLowLevelRequirementModel o2) {

                    int comp = o1.getHighLevelReqPath().compareTo(o2.getHighLevelReqPath());
                    if (comp != 0) {
                        return comp;
                    }

                    comp = o1.getLinkedLowLevelReqPath().compareTo(o2.getLinkedLowLevelReqPath());
                    if (comp != 0) {
                        return comp;
                    }
                    return comp;
                }
            };

    private Long highLevelReqId;
    private Long linkedLowLevelReqId;
    private String highLevelReqPath;
    private String linkedLowLevelReqPath;

    public LinkedLowLevelRequirementModel() {}

    public LinkedLowLevelRequirementModel(
            Long highLevelReqId,
            Long linkedLowLevelReqId,
            String highLevelReqPath,
            String linkedLowLevelReqPath) {
        this.highLevelReqId = highLevelReqId;
        this.linkedLowLevelReqId = linkedLowLevelReqId;
        this.highLevelReqPath = highLevelReqPath;
        this.linkedLowLevelReqPath = linkedLowLevelReqPath;
    }

    public LinkedLowLevelRequirementModel(String highLevelReqPath, String linkedLowLevelReqPath) {
        this.highLevelReqPath = highLevelReqPath;
        this.linkedLowLevelReqPath = linkedLowLevelReqPath;
    }

    public static LinkedLowLevelRequirementModel createLinkLowLevelReqModel(
            String highLevelReqPath, String linkedLowLevelReqPath) {

        LinkedLowLevelRequirementModel model =
                new LinkedLowLevelRequirementModel(highLevelReqPath, linkedLowLevelReqPath);

        model.setHighLevelReqPath(highLevelReqPath);
        model.setLinkedLowLevelReqPath(linkedLowLevelReqPath);

        return model;
    }

    public Long getHighLevelReqId() {
        return highLevelReqId;
    }

    public void setHighLevelReqId(Long highLevelReqId) {
        this.highLevelReqId = highLevelReqId;
    }

    public Long getLinkedLowLevelReqId() {
        return linkedLowLevelReqId;
    }

    public void setLinkedLowLevelReqId(Long linkedLowLevelReqId) {
        this.linkedLowLevelReqId = linkedLowLevelReqId;
    }

    public String getHighLevelReqPath() {
        return highLevelReqPath;
    }

    public void setHighLevelReqPath(String highLevelReqPath) {
        this.highLevelReqPath = highLevelReqPath;
    }

    public String getLinkedLowLevelReqPath() {
        return linkedLowLevelReqPath;
    }

    public void setLinkedLowLevelReqPath(String linkedLowLevelReqPath) {
        this.linkedLowLevelReqPath = linkedLowLevelReqPath;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("high-level-req-id", highLevelReqId)
                .append("high-level-req-path", highLevelReqPath)
                .append("linked-low-level-req-id", linkedLowLevelReqId)
                .append("linked-low-level-req-path", linkedLowLevelReqPath)
                .toString();
    }
}
