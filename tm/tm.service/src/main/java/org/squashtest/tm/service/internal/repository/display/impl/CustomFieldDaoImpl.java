/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static java.util.Objects.nonNull;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_RENDERING_LOCATION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.ResultQuery;
import org.jooq.SelectOnConditionStep;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.RenderingLocation;
import org.squashtest.tm.service.internal.display.dto.BoundProjectToCufDto;
import org.squashtest.tm.service.internal.display.dto.CufBindingDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldOptionDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.repository.display.CustomFieldDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class CustomFieldDaoImpl implements CustomFieldDao {

    @Inject DSLContext dsl;

    @Override
    public CustomFieldDto findByIdWithPossibleValues(Long customFieldId) {
        Map<Record, ? extends Result<?>> resultAsMap =
                getBaseRequest()
                        .where(CUSTOM_FIELD.CF_ID.equal(customFieldId))
                        .orderBy(CUSTOM_FIELD_OPTION.POSITION, CUSTOM_FIELD_OPTION.LABEL)
                        .fetch()
                        .intoGroups(
                                Arrays.asList(
                                                CUSTOM_FIELD.CF_ID.as(RequestAliasesConstants.ID),
                                                CUSTOM_FIELD.NAME,
                                                CUSTOM_FIELD.OPTIONAL,
                                                CUSTOM_FIELD.LABEL,
                                                CUSTOM_FIELD.INPUT_TYPE,
                                                CUSTOM_FIELD.CODE,
                                                CUSTOM_FIELD.DEFAULT_VALUE,
                                                CUSTOM_FIELD.LARGE_DEFAULT_VALUE,
                                                CUSTOM_FIELD.NUMERIC_DEFAULT_VALUE)
                                        .toArray(new Field[0]));

        return getAsCustomFieldDto(resultAsMap).get(0);
    }

    @Override
    public List<CustomFieldDto> findAllWithPossibleValues() {
        Map<Record, ? extends Result<?>> resultAsMap =
                getBaseRequest()
                        .orderBy(CUSTOM_FIELD_OPTION.POSITION, CUSTOM_FIELD_OPTION.LABEL)
                        .fetch()
                        .intoGroups(
                                Arrays.asList(
                                                CUSTOM_FIELD.CF_ID.as(RequestAliasesConstants.ID),
                                                CUSTOM_FIELD.NAME,
                                                CUSTOM_FIELD.OPTIONAL,
                                                CUSTOM_FIELD.LABEL,
                                                CUSTOM_FIELD.INPUT_TYPE,
                                                CUSTOM_FIELD.CODE,
                                                CUSTOM_FIELD.DEFAULT_VALUE,
                                                CUSTOM_FIELD.LARGE_DEFAULT_VALUE,
                                                CUSTOM_FIELD.NUMERIC_DEFAULT_VALUE)
                                        .toArray(new Field[0]));

        return getAsCustomFieldDto(resultAsMap);
    }

    private SelectOnConditionStep<?> getBaseRequest() {
        return dsl.select(
                        CUSTOM_FIELD.CF_ID.as(RequestAliasesConstants.ID),
                        CUSTOM_FIELD.NAME,
                        CUSTOM_FIELD.OPTIONAL,
                        CUSTOM_FIELD.LABEL,
                        CUSTOM_FIELD.INPUT_TYPE,
                        CUSTOM_FIELD.CODE,
                        CUSTOM_FIELD.DEFAULT_VALUE,
                        CUSTOM_FIELD.LARGE_DEFAULT_VALUE,
                        CUSTOM_FIELD.NUMERIC_DEFAULT_VALUE,
                        CUSTOM_FIELD_OPTION.LABEL,
                        CUSTOM_FIELD_OPTION.CF_ID,
                        CUSTOM_FIELD_OPTION.CODE,
                        CUSTOM_FIELD_OPTION.COLOUR,
                        CUSTOM_FIELD_OPTION.POSITION)
                .from(CUSTOM_FIELD)
                .leftJoin(CUSTOM_FIELD_OPTION)
                .on(CUSTOM_FIELD.CF_ID.eq(CUSTOM_FIELD_OPTION.CF_ID));
    }

    private List<CustomFieldDto> getAsCustomFieldDto(
            Map<Record, ? extends Result<? extends Record>> cufWithOptions) {
        List<CustomFieldDto> customFields = new ArrayList<>();

        cufWithOptions.entrySet().stream()
                .forEach(
                        cufAttributes -> {
                            CustomFieldDto customField = new CustomFieldDto();
                            customField.setId(
                                    cufAttributes.getKey().get(CUSTOM_FIELD.CF_ID.as(RequestAliasesConstants.ID)));
                            customField.setName(cufAttributes.getKey().get(CUSTOM_FIELD.NAME));
                            customField.setLabel(cufAttributes.getKey().get(CUSTOM_FIELD.LABEL));
                            customField.setInputType(cufAttributes.getKey().get(CUSTOM_FIELD.INPUT_TYPE));
                            customField.setCode(cufAttributes.getKey().get(CUSTOM_FIELD.CODE));
                            customField.setOptional(cufAttributes.getKey().get(CUSTOM_FIELD.OPTIONAL));
                            customField.setDefaultValue(cufAttributes.getKey().get(CUSTOM_FIELD.DEFAULT_VALUE));
                            customField.setLargeDefaultValue(
                                    cufAttributes.getKey().get(CUSTOM_FIELD.LARGE_DEFAULT_VALUE));
                            customField.setNumericDefaultValue(
                                    cufAttributes.getKey().get(CUSTOM_FIELD.NUMERIC_DEFAULT_VALUE));
                            customFields.add(customField);
                            cufAttributes.getValue().stream()
                                    // [SQUASH-1883] take care with intoGroups it can make nulls values if a left join
                                    // was used...
                                    // and front end components and other code don't like null bags...
                                    .filter(c -> nonNull(c.get(CUSTOM_FIELD_OPTION.CF_ID)))
                                    .forEach(
                                            customFieldOptionAttributes -> {
                                                CustomFieldOptionDto customFieldOption = new CustomFieldOptionDto();
                                                customFieldOption.setCfId(
                                                        customFieldOptionAttributes.get(CUSTOM_FIELD_OPTION.CF_ID));
                                                customFieldOption.setLabel(
                                                        customFieldOptionAttributes.get(CUSTOM_FIELD_OPTION.LABEL));
                                                customFieldOption.setCode(
                                                        customFieldOptionAttributes.get(CUSTOM_FIELD_OPTION.CODE));
                                                customFieldOption.setColour(
                                                        customFieldOptionAttributes.get(CUSTOM_FIELD_OPTION.COLOUR));
                                                customFieldOption.setPosition(
                                                        customFieldOptionAttributes.get(CUSTOM_FIELD_OPTION.POSITION));
                                                customField.getOptions().add(customFieldOption);
                                            });
                        });

        return customFields;
    }

    /**
     * @param projects
     */
    @Override
    public void appendCustomFieldBindings(List<ProjectDto> projects) {
        // @formatter:off
        appendCustomFieldBindings(
                projects,
                projectDtoMap -> {
                    Set<Long> projectIds = projectDtoMap.keySet();
                    return dsl.select(getFieldCustomFieldBinding())
                            .from(CUSTOM_FIELD_BINDING)
                            .leftJoin(CUSTOM_FIELD_RENDERING_LOCATION)
                            .on(CUSTOM_FIELD_BINDING.CFB_ID.eq(CUSTOM_FIELD_RENDERING_LOCATION.CFB_ID))
                            .where(CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.in(projectIds))
                            .orderBy(
                                    CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID,
                                    CUSTOM_FIELD_BINDING.BOUND_ENTITY,
                                    CUSTOM_FIELD_BINDING.POSITION);
                    // @formatter:on
                });
    }

    @Override
    public void appendCustomFieldBindings(
            List<ProjectDto> projects, CommonTableExpression<Record1<Long>> projectIdsCte) {
        // @formatter:off
        ResultQuery<Record> query =
                dsl.with(projectIdsCte)
                        .select(getFieldCustomFieldBinding())
                        .from(CUSTOM_FIELD_BINDING)
                        .join(projectIdsCte)
                        .on(
                                CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.eq(
                                        projectIdsCte.field(RequestAliasesConstants.PROJECT_ID_CTE, Long.class)))
                        .leftJoin(CUSTOM_FIELD_RENDERING_LOCATION)
                        .on(CUSTOM_FIELD_BINDING.CFB_ID.eq(CUSTOM_FIELD_RENDERING_LOCATION.CFB_ID))
                        .orderBy(
                                CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID,
                                CUSTOM_FIELD_BINDING.BOUND_ENTITY,
                                CUSTOM_FIELD_BINDING.POSITION);
        // @formatter:on
        appendCustomFieldBindings(projects, () -> query);
    }

    private <T extends Record> void appendCustomFieldBindings(
            List<ProjectDto> projects, Supplier<ResultQuery<T>> query) {
        appendCustomFieldBindings(projects, projectDtoMap -> query.get());
    }

    private <T extends Record> void appendCustomFieldBindings(
            List<ProjectDto> projects, Function<Map<Long, ProjectDto>, ResultQuery<T>> query) {
        if (nonNull(projects) && !projects.isEmpty()) {
            Map<Long, ProjectDto> projectsMap =
                    projects.stream().collect(Collectors.toMap(ProjectDto::getId, Function.identity()));
            query
                    .apply(projectsMap)
                    .forEach(
                            cufRecord -> {
                                ProjectDto projectDto =
                                        projectsMap.get(cufRecord.get(CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID));
                                projectDto.addBinding(getCustomFieldBindingFromRecord(cufRecord));
                            });
        }
    }

    private List<Field<?>> getFieldCustomFieldBinding() {
        return List.of(
                CUSTOM_FIELD_BINDING.CFB_ID,
                CUSTOM_FIELD_BINDING.CF_ID,
                CUSTOM_FIELD_BINDING.BOUND_ENTITY,
                CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID,
                CUSTOM_FIELD_BINDING.POSITION,
                CUSTOM_FIELD_RENDERING_LOCATION.RENDERING_LOCATION);
    }

    private CufBindingDto getCustomFieldBindingFromRecord(Record cufRecord) {
        BindableEntity bindableEntity =
                EnumUtils.getEnum(BindableEntity.class, cufRecord.get(CUSTOM_FIELD_BINDING.BOUND_ENTITY));
        CufBindingDto binding =
                new CufBindingDto(
                        cufRecord.get(CUSTOM_FIELD_BINDING.CFB_ID),
                        cufRecord.get(CUSTOM_FIELD_BINDING.CF_ID),
                        bindableEntity,
                        cufRecord.get(CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID),
                        cufRecord.get(CUSTOM_FIELD_BINDING.POSITION));
        if (StringUtils.isNotBlank(cufRecord.get(CUSTOM_FIELD_RENDERING_LOCATION.RENDERING_LOCATION))) {
            binding.addRenderingLocation(
                    EnumUtils.getEnum(
                            RenderingLocation.class,
                            cufRecord.get(CUSTOM_FIELD_RENDERING_LOCATION.RENDERING_LOCATION)));
        }
        return binding;
    }

    /**
     * Retrieves a list of projects bound to the custom field whose id is passed as an argument.
     *
     * @param cufId : the id of the custom field for which this method retrieves all the related
     *     projects.
     * @return : a list of BoundProjectToCufDto corresponding to the projects bound to the custom
     *     field passed as an argument.
     */
    @Override
    public List<BoundProjectToCufDto> findAllBoundProjectsByCufId(Long cufId) {
        return dsl.select(
                        CUSTOM_FIELD_BINDING.CFB_ID.as(RequestAliasesConstants.CUSTOM_FIELD_BINDING_ID),
                        CUSTOM_FIELD_BINDING.BOUND_ENTITY.as(RequestAliasesConstants.BINDABLE_ENTITY),
                        PROJECT.PROJECT_ID,
                        PROJECT.NAME.as(RequestAliasesConstants.PROJECT_NAME))
                .from(CUSTOM_FIELD_BINDING)
                .innerJoin(PROJECT)
                .on(CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(CUSTOM_FIELD_BINDING.CF_ID.eq(cufId))
                .orderBy(PROJECT.PROJECT_ID, CUSTOM_FIELD_BINDING.CFB_ID)
                .fetchInto(BoundProjectToCufDto.class);
    }
}
