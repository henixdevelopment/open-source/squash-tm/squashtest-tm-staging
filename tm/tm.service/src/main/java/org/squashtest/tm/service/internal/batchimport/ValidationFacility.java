/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport;

import static org.squashtest.tm.service.importer.EntityType.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.service.internal.batchimport.Existence.EXISTS;
import static org.squashtest.tm.service.internal.batchimport.Existence.NOT_EXISTS;
import static org.squashtest.tm.service.internal.batchimport.Existence.TO_BE_CREATED;
import static org.squashtest.tm.service.internal.batchimport.Messages.IMPACT_VERSION_NUMBER_MODIFIED;
import static org.squashtest.tm.service.internal.batchimport.Messages.IMPACT_VERSION_NUMBER_MODIFIED_TO_PREVIOUS_NUMBER;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.LinkedLowLevelRequirementsSheetColumn.HIGH_LEVEL_REQ_PATH;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.LinkedLowLevelRequirementsSheetColumn.STANDARD_REQ_PATH;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementSheetColumn.REQ_NATURE;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementSheetColumn.REQ_VERSION_MILESTONE;
import static org.squashtest.tm.service.internal.batchimport.column.requirement.RequirementSheetColumn.REQ_VERSION_NUM;
import static org.squashtest.tm.service.security.Authorizations.MILESTONE_FEAT_ENABLED;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.importer.EntityType;
import org.squashtest.tm.service.importer.ImportMode;
import org.squashtest.tm.service.importer.ImportStatus;
import org.squashtest.tm.service.importer.LogEntry;
import org.squashtest.tm.service.importer.LogEntry.Builder;
import org.squashtest.tm.service.importer.Target;
import org.squashtest.tm.service.importer.WithPath;
import org.squashtest.tm.service.infolist.InfoListItemFinderService;
import org.squashtest.tm.service.internal.batchimport.MilestoneImportHelper.Partition;
import org.squashtest.tm.service.internal.batchimport.column.TemplateColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageTarget;
import org.squashtest.tm.service.internal.batchimport.column.testcase.StepSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn;
import org.squashtest.tm.service.internal.batchimport.instruction.ActionStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.CallStepInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetParamValueInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.Instruction;
import org.squashtest.tm.service.internal.batchimport.instruction.LinkedLowLevelRequirementInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementLinkInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.DatasetTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.HighLevelRequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.LinkedLowLevelRequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.ParameterTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementLinkTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestStepTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementImportValidationBag;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseImportValidationBag;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.RequirementImportDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.requirement.RequirementLibraryFinderService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;

/**
 * This implementation solely focuses on validating data. It doesn't perform any operation against
 * the database, nor modifies the model : it justs uses the current data available.
 */
@Component
@Scope("prototype")
public class ValidationFacility implements Facility {

    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String PERM_READ = "READ";
    private static final String PERM_LINK = "LINK";
    private static final String PERM_IMPORT = "IMPORT";
    private static final String TEST_CASE_LIBRARY_CLASSNAME =
            "org.squashtest.tm.domain.testcase.TestCaseLibrary";
    private static final String REQUIREMENT_VERSION_LIBRARY_CLASSNAME =
            "org.squashtest.tm.domain.requirement.RequirementLibrary";

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationFacility.class);

    @Value("#{" + MILESTONE_FEAT_ENABLED + "}")
    private boolean milestonesEnabled;

    private final PermissionEvaluationService permissionService;
    private final InfoListItemFinderService infoListItemService;
    private final Model model;
    private final UserDao userDao;
    private final MilestoneImportHelper milestoneHelper;
    private final RequirementLibraryFinderService reqFinderService;
    private final ProjectDao projectDao;
    private final UserContextService userContextService;
    private final TestCaseDao testCaseDao;
    private final EntityValidator entityValidator;
    private final RequirementImportDao requirementImportDao;

    private final CustomFieldValidator cufValidator = new CustomFieldValidator();
    private final CreationStrategy<TestCaseInstruction, TestCaseTarget> testCaseCreationStrategy =
            new CreationStrategy<>();
    private final UpdateStrategy<TestCaseInstruction, TestCaseTarget> testCaseUpdateStrategy =
            new UpdateStrategy<>();
    private final CreationStrategy<RequirementVersionInstruction, RequirementVersionTarget>
            requirementVersionCreationStrategy = new CreationStrategy<>();
    private final UpdateStrategy<RequirementVersionInstruction, RequirementVersionTarget>
            requirementVersionUpdateStrategy = new UpdateStrategy<>();

    public ValidationFacility(
            PermissionEvaluationService permissionService,
            InfoListItemFinderService infoListItemService,
            Model model,
            UserDao userDao,
            MilestoneImportHelper milestoneHelper,
            RequirementLibraryFinderService reqFinderService,
            ProjectDao projectDao,
            UserContextService userContextService,
            TestCaseDao testCaseDao,
            EntityValidator entityValidator,
            RequirementImportDao requirementImportDao) {
        this.permissionService = permissionService;
        this.infoListItemService = infoListItemService;
        this.model = model;
        this.userDao = userDao;
        this.milestoneHelper = milestoneHelper;
        this.reqFinderService = reqFinderService;
        this.projectDao = projectDao;
        this.userContextService = userContextService;
        this.testCaseDao = testCaseDao;
        this.entityValidator = entityValidator;
        this.requirementImportDao = requirementImportDao;
        this.entityValidator.setModel(model);
    }

    public Model getModel() {
        return model;
    }

    private static void checkProjectValidity(Project project) {
        if (project == null) {
            throw new IllegalArgumentException("Project must not be null");
        }
    }

    private void checkPathForUpdate(TestCaseTarget target, String name, LogTrain logs) {
        if (StringUtils.isBlank(name)) {
            // no name means no rename means we're good -> bail out
            return;
        }

        String path = target.getPath();

        if (!PathUtils.arePathsAndNameConsistents(path, name)) {

            String newPath = PathUtils.rename(path, name);
            TestCaseTarget newTarget = new TestCaseTarget(newPath);
            TargetStatus newStatus = model.getStatus(newTarget);
            if (newStatus.status != NOT_EXISTS) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_TC_CANT_RENAME, path, newPath)
                                .build());
            }
        }
    }

    /**
     * Will replace {@code mixin.createdBy} and {@code mixin.createdOn} if the values are invalid :
     *
     * <ul>
     *   <li>{@code mixin.createdBy} will be replaced by the current user's login
     *   <li>{@code mixin.createdOn} will be replaced by the import date.
     * </ul>
     *
     * @return a list of logEntries
     */
    private List<LogEntry> fixMetadata(
            Target target,
            AuditableMixin auditable,
            ImportMode importMode,
            EntityType type,
            List<String> activeLogins,
            String currentUsername) {
        // init vars
        List<LogEntry> logEntries = new ArrayList<>();
        String login = auditable.getCreatedBy();
        boolean fixUser = false;
        if (StringUtils.isBlank(login)) {
            // no value for created by
            fixUser = true;
        } else {
            if (!activeLogins.contains(login)) {
                // user not found or not active
                String warningMessage = null;
                String impactMessage =
                        switch (importMode) {
                            case CREATE -> Messages.IMPACT_USE_CURRENT_LOGIN;
                            case UPDATE -> Messages.IMPACT_NO_CHANGE;
                            default -> Messages.IMPACT_NO_CHANGE;
                        };
                switch (type) {
                    case REQUIREMENT_VERSION -> warningMessage = Messages.ERROR_REQ_USER_NOT_FOUND;
                    case TEST_CASE -> warningMessage = Messages.ERROR_TC_USER_NOT_FOUND;
                    default -> {
                        // do nothing
                    }
                }
                LogEntry logEntry =
                        LogEntry.warning()
                                .forTarget(target)
                                .withMessage(warningMessage)
                                .withImpact(impactMessage)
                                .build();
                logEntries.add(logEntry);
                fixUser = true;
            }
        }
        if (fixUser) {
            auditable.setCreatedBy(currentUsername);
        }

        if (auditable.getCreatedOn() == null) {
            auditable.setCreatedOn(new Date());
        }
        return logEntries;
    }

    @Override
    public LogTrain deleteTestCase(TestCaseTarget target) {

        LogTrain logs = new LogTrain();

        TargetStatus status = model.getStatus(target);

        // 1 - does the target exist
        if (status.getStatus() == NOT_EXISTS) {
            logs.addEntry(
                    LogEntry.failure().forTarget(target).withMessage(Messages.ERROR_TC_NOT_FOUND).build());
        }

        // 2 - can the user actually do it ?
        LogEntry hasntPermission = checkPermissionOnProject(PERM_IMPORT, target, target);
        if (hasntPermission != null) {
            logs.addEntry(hasntPermission);
        }

        // 3 - is the test case called by another test case ?
        if (model.isCalled(target)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_REMOVE_CALLED_TC)
                            .build());
        }

        // 4 - milestone lock ?
        if (model.isTestCaseLockedByMilestones(target)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MILESTONE_LOCKED)
                            .build());
        }

        return logs;
    }

    private void checkActionStepAddition(ActionStepInstruction instruction) {
        TestStepTarget target = instruction.getTarget();
        ActionTestStep testStep = instruction.getTestStep();
        Map<String, String> cufValues = instruction.getCustomFields();

        LogTrain logs;

        // 1 - basic checks
        logs = entityValidator.basicTestStepChecks(target);

        // 2 - custom fields (create)
        logs.append(
                cufValidator.checkCreateCustomFields(target, cufValues, model.getTestStepCufs(target)));

        // 3 - the user must be approved
        LogEntry hasntPermission = checkPermissionOnProject(PERM_IMPORT, target.getTestCase(), target);
        if (hasntPermission != null) {
            logs.addEntry(hasntPermission);
        }

        // 4 - the test case must not be locked by a milestone
        if (model.isTestCaseLockedByMilestones(target.getTestCase())) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MILESTONE_LOCKED)
                            .build());
        }

        // 5 - check the index
        TestCaseTarget testCase = target.getTestCase();
        TargetStatus tcStatus = model.getStatus(testCase);
        if (tcStatus.status == TO_BE_CREATED || tcStatus.status == EXISTS) {
            LogEntry indexCheckLog =
                    checkStepIndex(
                            ImportMode.CREATE, target, ImportStatus.WARNING, Messages.IMPACT_STEP_CREATED_LAST);
            if (indexCheckLog != null) {
                logs.addEntry(indexCheckLog);
            }
        }

        // 6 - check the parameter name validity in action step and expected result
        checkParamNameInString(target, logs, testStep.getAction(), StepSheetColumn.TC_STEP_ACTION);
        checkParamNameInString(
                target, logs, testStep.getExpectedResult(), StepSheetColumn.TC_STEP_EXPECTED_RESULT);

        instruction.addLogs(logs);
    }

    private void checkParamNameInString(
            Target target, LogTrain logs, String action, TemplateColumn column) {
        boolean hasInvalidParameterNamesInAction = Parameter.hasInvalidParameterNamesInString(action);
        if (hasInvalidParameterNamesInAction) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_INVALID_PARAM_NAME_IN_FIELD, column.getHeader())
                            .build());
        }
    }

    public void checkCallStepCreation(CallStepInstruction instruction) {

        TestStepTarget target = instruction.getTarget();
        TestCaseTarget calledTestCase = instruction.getCalledTC();

        LogTrain logs;

        // 1 - basic checks
        logs = entityValidator.basicTestStepChecks(target);

        // 2 - call step specific checks
        logs.append(
                entityValidator.validateCallStep(
                        target, calledTestCase, instruction.getDatasetInfo(), ImportMode.CREATE));

        // 3 - cufs : call steps have no cufs -> skip

        // 4.1 - the user must be approved on the source test case
        LogEntry hasntOwnerPermission =
                checkPermissionOnProject(PERM_IMPORT, target.getTestCase(), target);
        if (hasntOwnerPermission != null) {
            logs.addEntry(hasntOwnerPermission);
        }

        // 4.2 - the user must be approved on the target test case
        LogEntry hasntCallPermission = checkPermissionOnProject(PERM_READ, calledTestCase, target);
        if (hasntCallPermission != null) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_CALL_NOT_READABLE)
                            .withImpact(Messages.IMPACT_CALL_AS_ACTION_STEP)
                            .build());
        }

        // 4.3 - the test case must not be locked by a milestone
        if (model.isTestCaseLockedByMilestones(target.getTestCase())) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MILESTONE_LOCKED)
                            .build());
        }

        // 5 - check the index
        LogEntry indexCheckLog =
                checkStepIndex(
                        ImportMode.CREATE, target, ImportStatus.WARNING, Messages.IMPACT_STEP_CREATED_LAST);
        if (indexCheckLog != null) {
            logs.addEntry(indexCheckLog);
        }

        instruction.addLogs(logs);
    }

    @Override
    public LogTrain updateActionStep(
            TestStepTarget target, ActionTestStep testStep, Map<String, String> cufValues) {

        LogTrain logs;

        // 1 - basic checks
        logs = entityValidator.basicTestStepChecks(target);

        // 2 - custom fields (create)
        logs.append(
                cufValidator.checkUpdateCustomFields(target, cufValues, model.getTestStepCufs(target)));

        // 3 - the user must be approved
        LogEntry hasntPermission = checkPermissionOnProject(PERM_IMPORT, target.getTestCase(), target);
        if (hasntPermission != null) {
            logs.addEntry(hasntPermission);
        }

        // 4 - the test case must not be locked by a milestone
        if (model.isTestCaseLockedByMilestones(target.getTestCase())) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MILESTONE_LOCKED)
                            .build());
        }

        // 5 - the step must exist
        boolean exists = model.stepExists(target);
        if (!exists) {
            if (target.getIndex() == null) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_STEPINDEX_EMPTY)
                                .build());
            } else if (target.getIndex() < 0) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_STEPINDEX_NEGATIVE)
                                .build());
            } else {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_STEP_NOT_EXISTS)
                                .build());
            }
        } else {
            // 5 - the step must be actually an action step
            StepType type = model.getType(target);
            if (type != StepType.ACTION) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_NOT_AN_ACTIONSTEP)
                                .build());
            }
        }

        // 6 check parameter name value
        checkParamNameInString(target, logs, testStep.getAction(), StepSheetColumn.TC_STEP_ACTION);
        checkParamNameInString(
                target, logs, testStep.getExpectedResult(), StepSheetColumn.TC_STEP_EXPECTED_RESULT);

        return logs;
    }

    @Override
    public LogTrain updateCallStep(
            TestStepTarget target,
            CallTestStep testStep,
            TestCaseTarget calledTestCase,
            CallStepParamsInfo paramInfos,
            ActionTestStep actionStepBackup) {

        LogTrain logs;

        // 1 - basic checks
        logs = entityValidator.basicTestStepChecks(target);

        // 2 - call step specific checks
        logs.append(
                entityValidator.validateCallStep(target, calledTestCase, paramInfos, ImportMode.UPDATE));

        // 3 - cufs : call steps have no cufs -> skip

        // 4.1 - the user must be approved on the source test case
        LogEntry hasntOwnerPermission =
                checkPermissionOnProject(PERM_IMPORT, target.getTestCase(), target);
        if (hasntOwnerPermission != null) {
            logs.addEntry(hasntOwnerPermission);
        }

        // 4.2 - the user must be approved on the target test case
        LogEntry hasntCallPermission = checkPermissionOnProject(PERM_READ, calledTestCase, target);
        if (hasntCallPermission != null) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_CALL_NOT_READABLE)
                            .build());
        }

        // 4.3 - the test case must not be locked by a milestone
        if (model.isTestCaseLockedByMilestones(target.getTestCase())) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MILESTONE_LOCKED)
                            .build());
        }

        // 5 - the step must exist
        boolean exists = model.stepExists(target);
        if (!exists) {
            logs.addEntry(
                    LogEntry.failure().forTarget(target).withMessage(Messages.ERROR_STEP_NOT_EXISTS).build());
        } else {
            // 6 - check that this is a call step
            StepType type = model.getType(target);
            if (type != StepType.CALL) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_NOT_A_CALLSTEP)
                                .build());
            }

            // 7 - no call step cycles allowed
            if (model.wouldCreateCycle(target, calledTestCase)) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(
                                        Messages.ERROR_CYCLIC_STEP_CALLS,
                                        target.getTestCase().getPath(),
                                        calledTestCase.getPath())
                                .build());
            }
        }

        return logs;
    }

    @Override
    public LogTrain deleteTestStep(TestStepTarget target) {

        LogTrain logs;

        // 1 - basic checks
        logs = entityValidator.basicTestStepChecks(target);

        // 2 - can the user do it
        LogEntry hasntPermission = checkPermissionOnProject(PERM_IMPORT, target.getTestCase(), target);
        if (hasntPermission != null) {
            logs.addEntry(hasntPermission);
        }

        // 3 - the test case must not be locked by a milestone
        if (model.isTestCaseLockedByMilestones(target.getTestCase())) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_MILESTONE_LOCKED)
                            .build());
        }

        // 4 - can that step be identified precisely ?
        LogEntry indexCheckLog = checkStepIndex(ImportMode.DELETE, target, ImportStatus.FAILURE, null);
        if (indexCheckLog != null) {
            logs.addEntry(indexCheckLog);
        }

        return logs;
    }

    private void checkParameterCreation(ParameterInstruction instruction) {
        LogTrain logs;
        ParameterTarget target = instruction.getTarget();

        // 1 - basic checks
        logs = entityValidator.basicParameterChecks(target);

        // 2 - does it already exists ?
        if (model.doesParameterExists(target)) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_PARAMETER_ALREADY_EXISTS)
                            .withImpact(Messages.IMPACT_PARAM_UPDATED)
                            .build());
        }

        // 3 - is the user approved ?
        LogEntry hasNoPermission = checkPermissionOnProject(PERM_IMPORT, target.getOwner(), target);
        if (hasNoPermission != null) {
            logs.addEntry(hasNoPermission);
        }

        instruction.addLogs(logs);
    }

    @Override
    public LogTrain updateParameter(ParameterTarget target, Parameter param) {

        LogTrain logs;

        // 1 - basic checks
        logs = entityValidator.basicParameterChecks(target);

        // 2 - does it exists ?
        if (!model.doesParameterExists(target)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_PARAMETER_NOT_FOUND)
                            .build());
        }

        // 3 - is the user approved ?
        LogEntry hasNoPermission = checkPermissionOnProject(PERM_IMPORT, target.getOwner(), target);
        if (hasNoPermission != null) {
            logs.addEntry(hasNoPermission);
        }

        return logs;
    }

    @Override
    public LogTrain deleteParameter(ParameterTarget target) {

        LogTrain logs = new LogTrain();

        // 1 - does it exists ?
        if (!model.doesParameterExists(target)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_PARAMETER_NOT_FOUND)
                            .build());
        }

        // 2 - is the user approved ?
        LogEntry hasNoPermission = checkPermissionOnProject(PERM_IMPORT, target.getOwner(), target);
        if (hasNoPermission != null) {
            logs.addEntry(hasNoPermission);
        }

        return logs;
    }

    @Override
    public LogTrain failsafeUpdateParameterValue(
            DatasetTarget dataset, ParameterTarget param, String value, boolean isUpdate) {

        /*
         * Feat 3695 in this method we must assume that all the checks on the
         * dataset were already logged. For our purpose here we still need to
         * check if the dataset is correct (because the rest depends on it), but
         * we don't need to log it twice. That's why we keep ther log trains
         * appart.
         */
        LogTrain logs;
        LogTrain junk;

        // 0 - is the dataset correctly identifed ?
        junk = entityValidator.basicDatasetCheck(dataset);

        // 1 - is the parameter correctly identified ?
        logs = entityValidator.basicParameterValueChecks(param);

        // in this context specifically we set the target explicitly as being
        // the dataset, not the parameter
        // (or the logs will be reported at the wrong place)
        logs.setForAll(dataset);

        // go further if no blocking errors are detected
        if (!(logs.hasCriticalErrors() || junk.hasCriticalErrors())) {

            // 2 - is such parameter available for this dataset ?
            if (!model.isParamInDataset(param, dataset)) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(dataset)
                                .withMessage(Messages.ERROR_DATASET_PARAMETER_MISMATCH)
                                .build());
            }

            // 3 - is the user allowed to do so ?
            LogEntry hasNoPermission =
                    checkPermissionOnProject(PERM_IMPORT, dataset.getTestCase(), dataset);
            if (hasNoPermission != null) {
                logs.addEntry(hasNoPermission);
            }
        }
        return logs;
    }

    private void checkDatasetCreation(DatasetInstruction instruction) {
        DatasetTarget target = instruction.getTarget();

        LogTrain logs;

        // 1 - is the dataset correctly identifed ?
        logs = entityValidator.basicDatasetCheck(target);

        // 2 - is the user allowed to do so ?
        LogEntry hasNoPermission = checkPermissionOnProject(PERM_IMPORT, target.getTestCase(), target);
        if (hasNoPermission != null) {
            logs.addEntry(hasNoPermission);
        }

        instruction.addLogs(logs);
    }

    @Override
    public LogTrain deleteDataset(DatasetTarget dataset) {

        LogTrain logs;

        // 1 - is the dataset correctly identified ?
        logs = entityValidator.basicDatasetCheck(dataset);

        // 2 - does the dataset exists ?
        if (model.datasetNotExist(dataset)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(dataset)
                            .withMessage(Messages.ERROR_DATASET_NOT_FOUND)
                            .build());
        }

        // 3 - has the user the required privilege ?
        LogEntry hasNoPermission =
                checkPermissionOnProject(PERM_IMPORT, dataset.getTestCase(), dataset);
        if (hasNoPermission != null) {
            logs.addEntry(hasNoPermission);
        }

        return logs;
    }

    // **************************** private utilities
    // *****************************************

    /**
     * checks permission on a project that may exist or not. <br>
     * the case where the project doesn't exist (and thus has no id) is already covered in the basic
     * checks.
     */
    private LogEntry checkPermissionOnProject(
            String permission, TestCaseTarget target, Target checkedTarget) {

        LogEntry entry = null;

        Long libid = model.getProjectStatus(target.getProject()).getTestCaseLibraryId();
        if (libid != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, permission, libid, TEST_CASE_LIBRARY_CLASSNAME)) {
            entry =
                    LogEntry.failure()
                            .forTarget(checkedTarget)
                            .withMessage(Messages.ERROR_NO_PERMISSION, permission, target.getPath())
                            .build();
        }

        return entry;
    }

    private LogEntry checkPermissionOnProject(
            String permission, RequirementVersionTarget target, Target checkedTarget) {

        LogEntry entry = null;

        Long libid = model.getProjectStatus(target.getProject()).getRequirementLibraryId();
        if (libid != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, permission, libid, REQUIREMENT_VERSION_LIBRARY_CLASSNAME)) {
            entry =
                    LogEntry.failure()
                            .forTarget(checkedTarget)
                            .withMessage(Messages.ERROR_NO_PERMISSION, permission, target.getPath())
                            .build();
        }

        return entry;
    }

    private LogEntry checkPermissionOnProject(
            LinkedLowLevelRequirementTarget linkedLowLevelRequirementTarget) {

        LogEntry entry = null;

        String highLevelReqPath = linkedLowLevelRequirementTarget.getHighLevelReqPath();
        Project highLevelReqProject = getProjectFromPath(highLevelReqPath);
        Long highLevelReqLibid = highLevelReqProject.getRequirementLibrary().getId();

        if (highLevelReqLibid != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, PERM_IMPORT, highLevelReqLibid, REQUIREMENT_VERSION_LIBRARY_CLASSNAME)) {
            entry =
                    LogEntry.failure()
                            .forTarget(linkedLowLevelRequirementTarget)
                            .withMessage(Messages.ERROR_NO_PERMISSION, PERM_IMPORT, highLevelReqPath)
                            .build();
        }

        String standardReqPath = linkedLowLevelRequirementTarget.getStandardReqPath();
        Project reqProject = getProjectFromPath(standardReqPath);
        Long standardReqLibid = reqProject.getRequirementLibrary().getId();

        if (standardReqLibid != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, PERM_IMPORT, standardReqLibid, REQUIREMENT_VERSION_LIBRARY_CLASSNAME)) {
            entry =
                    LogEntry.failure()
                            .forTarget(linkedLowLevelRequirementTarget)
                            .withMessage(Messages.ERROR_NO_PERMISSION, PERM_IMPORT, standardReqPath)
                            .build();
        }

        return entry;
    }

    private Project getProjectFromPath(String path) {
        String projectName = PathUtils.extractProjectName(path);
        projectName = PathUtils.unescapePathPartSlashes(projectName);
        return projectDao.findByName(projectName);
    }

    private LogEntry checkStepIndex(
            ImportMode mode, TestStepTarget target, ImportStatus importStatus, String optionalImpact) {
        Integer index = target.getIndex();
        LogEntry entry = null;

        if (index == null) {
            entry =
                    LogEntry.status(importStatus)
                            .forTarget(target)
                            .withMessage(Messages.ERROR_STEPINDEX_EMPTY)
                            .withImpact(optionalImpact)
                            .build();
        } else if (index < 0) {
            entry =
                    LogEntry.status(importStatus)
                            .forTarget(target)
                            .withMessage(Messages.ERROR_STEPINDEX_NEGATIVE)
                            .withImpact(optionalImpact)
                            .build();

        } else if (!model.stepExists(target)
                && (!model.indexIsFirstAvailable(target) || mode != ImportMode.CREATE)) {
            // when index doesn't match a step in the target model
            // this error message is not needed for creation when the target
            // index is the first one available
            entry =
                    LogEntry.status(importStatus)
                            .forTarget(target)
                            .withMessage(Messages.ERROR_STEPINDEX_OVERFLOW)
                            .withImpact(optionalImpact)
                            .build();
        }

        return entry;
    }

    @Override
    public void createTestCases(List<TestCaseInstruction> instructions, Project project) {
        checkProjectValidity(project);

        Set<TestCaseTarget> targets =
                instructions.stream().map(Instruction::getTarget).collect(Collectors.toSet());

        model.initStatuses(targets);

        TestCaseImportValidationBag bag =
                buildValidationBag(instructions, project.getId(), project.getTestCaseLibrary().getId());

        instructions.forEach(instruction -> checkTestCaseCreation(instruction, bag));
    }

    private TestCaseImportValidationBag buildValidationBag(
            List<TestCaseInstruction> instructions, Long projectId, Long testCaseLibraryId) {

        String currentUsername = userContextService.getUsername();
        List<String> activeLogins = collectActiveLogins(instructions);
        List<String> uuidList = collectExistingTestCaseUuids(instructions);

        Set<String> naturesConsistency = collectConsistentNatures(instructions, projectId);
        Set<String> typesConsistency = collectConsistentTypes(instructions, projectId);
        InfoListItem defaultNature = infoListItemService.findDefaultTestCaseNature(projectId);
        InfoListItem defaultType = infoListItemService.findDefaultTestCaseType(projectId);

        return new TestCaseImportValidationBag(
                testCaseLibraryId,
                activeLogins,
                uuidList,
                naturesConsistency,
                defaultNature,
                typesConsistency,
                defaultType,
                currentUsername);
    }

    private List<String> collectActiveLogins(List<TestCaseInstruction> instructions) {
        Set<String> createdByList =
                instructions.stream()
                        .map(instruction -> instruction.getTestCase().getCreatedBy())
                        .filter(user -> !Strings.isBlank(user))
                        .collect(Collectors.toSet());

        return createdByList.isEmpty()
                ? Collections.emptyList()
                : userDao.filterActiveUserLogins(createdByList);
    }

    private List<String> collectRequirementActiveLogins(
            List<RequirementVersionInstruction> instructions) {
        Set<String> createdByList =
                instructions.stream()
                        .map(instruction -> instruction.getRequirementVersion().getCreatedBy())
                        .filter(user -> !Strings.isBlank(user))
                        .collect(Collectors.toSet());

        return createdByList.isEmpty()
                ? Collections.emptyList()
                : userDao.filterActiveUserLogins(createdByList);
    }

    private List<String> collectExistingTestCaseUuids(List<TestCaseInstruction> instructions) {
        Set<String> uuids =
                instructions.stream()
                        .map(instruction -> instruction.getTestCase().getUuid())
                        .filter(user -> !Strings.isBlank(user))
                        .collect(Collectors.toSet());

        return uuids.isEmpty()
                ? Collections.emptyList()
                : testCaseDao.filterExistingTestCaseUuids(uuids);
    }

    private Set<String> collectConsistentNatures(
            List<TestCaseInstruction> instructions, Long projectId) {
        Set<String> natures =
                instructions.stream()
                        .map(TestCaseInstruction::getTestCase)
                        .filter(testCase -> testCase.getNature() != null)
                        .map(testCase -> testCase.getNature().getCode())
                        .filter(code -> !Strings.isBlank(code))
                        .collect(Collectors.toSet());

        return infoListItemService.filterConsistentNatures(projectId, natures);
    }

    private Set<String> collectConsistentCategories(
            List<RequirementVersionInstruction> instructions, Long projectId) {
        Set<String> natures =
                instructions.stream()
                        .map(RequirementVersionInstruction::getRequirementVersion)
                        .filter(requirementVersion -> requirementVersion.getCategory() != null)
                        .map(testCase -> testCase.getCategory().getCode())
                        .filter(code -> !Strings.isBlank(code))
                        .collect(Collectors.toSet());

        return infoListItemService.filterConsistentCategories(projectId, natures);
    }

    private Set<String> collectConsistentTypes(
            List<TestCaseInstruction> instructions, Long projectId) {
        Set<String> types =
                instructions.stream()
                        .map(TestCaseInstruction::getTestCase)
                        .filter(testCase -> testCase.getType() != null)
                        .map(testCase -> testCase.getType().getCode())
                        .filter(code -> !Strings.isBlank(code))
                        .collect(Collectors.toSet());

        return infoListItemService.filterConsistentTypes(projectId, types);
    }

    private void checkTestCaseCreation(
            TestCaseInstruction instr, TestCaseImportValidationBag validationBag) {

        TestCaseTarget target = instr.getTarget();
        String path = target.getPath();

        TestCase testCase = instr.getTestCase();
        String name = testCase.getName();

        Map<String, String> cufValues = instr.getCustomFields();

        LogTrain logs;
        TargetStatus status = model.getStatus(target);

        // 1 - basic verifications
        logs = entityValidator.createTestCaseChecks(target, testCase, validationBag);

        // 2 - custom fields (create)
        logs.append(
                cufValidator.checkCreateCustomFields(target, cufValues, model.getTestCaseCufs(target)));

        // 3 - other checks
        // 3-1 : names clash
        if (status.getStatus() != NOT_EXISTS) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_TC_ALREADY_EXISTS, target.getPath())
                            .withImpact(Messages.IMPACT_TC_WITH_SUFFIX)
                            .build());
        }

        // 3-2 : permissions.
        LogEntry hasntPermission = checkPermissionOnProject(validationBag.testCaseLibraryId(), target);
        if (hasntPermission != null) {
            logs.addEntry(hasntPermission);
        }

        // 3-3 : name and path must be consistent, only if the name is not empty
        if (!StringUtils.isBlank(name) && !PathUtils.arePathsAndNameConsistents(path, name)) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_INCONSISTENT_PATH_AND_NAME, path, name)
                            .build());
        }

        // 3-4 : check the milestones
        testCaseCreationStrategy.validateMilestones(instr, logs);

        // 3-5 : fix test case metadatas
        List<LogEntry> logEntries =
                fixMetadata(
                        target,
                        testCase,
                        ImportMode.CREATE,
                        EntityType.TEST_CASE,
                        validationBag.activeLogins(),
                        validationBag.currentUsername());
        logs.addEntries(logEntries);

        // 3-6 : check the parameters names inside prerequisite
        checkParamNameInString(
                target, logs, testCase.getPrerequisite(), TestCaseSheetColumn.TC_PRE_REQUISITE);

        // 3-7 : check UUID unicity and format
        if (!StringUtils.isBlank(testCase.getUuid())) {
            if (validationBag.existingUuids().contains(testCase.getUuid())) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_ALREADY_EXISTING_UUID, testCase.getUuid())
                                .build());
            } else {
                Pattern uuidPattern =
                        Pattern.compile(
                                "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}");
                if (!uuidPattern.matcher(testCase.getUuid()).matches()) {
                    logs.addEntry(
                            LogEntry.failure()
                                    .forTarget(target)
                                    .withMessage(Messages.ERROR_WRONG_UUID_FORMAT)
                                    .build());
                }
            }
        }

        instr.addLogs(logs);
    }

    private LogEntry checkPermissionOnProject(Long testCaseLibraryId, TestCaseTarget target) {
        if (testCaseLibraryId != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, PERM_IMPORT, testCaseLibraryId, TEST_CASE_LIBRARY_CLASSNAME)) {

            return LogEntry.failure()
                    .forTarget(target)
                    .withMessage(Messages.ERROR_NO_PERMISSION, PERM_IMPORT, target.getPath())
                    .build();
        }

        return null;
    }

    private LogEntry checkPermissionOnProject(
            Long requirementLibraryId, RequirementVersionTarget target) {
        if (requirementLibraryId != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, PERM_IMPORT, requirementLibraryId, REQUIREMENT_VERSION_LIBRARY_CLASSNAME)) {

            return LogEntry.failure()
                    .forTarget(target)
                    .withMessage(Messages.ERROR_NO_PERMISSION, PERM_IMPORT, target.getPath())
                    .build();
        }

        return null;
    }

    /**
     * @see
     *     org.squashtest.tm.service.internal.batchimport.Facility#updateTestCase(TestCaseInstruction)
     */
    @Override
    public LogTrain updateTestCase(TestCaseInstruction instr) {
        TestCase testCase = instr.getTestCase();
        TestCaseTarget target = instr.getTarget();
        Map<String, String> cufValues = instr.getCustomFields();

        LogTrain logs = new LogTrain();
        String name = testCase.getName();

        TargetStatus status = model.getStatus(target);

        // if the test case doesn't exist
        if (status.getStatus() == NOT_EXISTS) {
            logs.addEntry(
                    LogEntry.failure().forTarget(target).withMessage(Messages.ERROR_TC_NOT_FOUND).build());
        } else {

            // 1 - basic verifications
            ProjectTargetStatus projectStatus = model.getProjectStatus(target.getProject());
            TestCaseImportValidationBag bag =
                    buildValidationBag(
                            Collections.singletonList(instr),
                            projectStatus.getId(),
                            projectStatus.getTestCaseLibraryId());
            logs.append(entityValidator.updateTestCaseChecks(target, testCase, bag));

            // 2 - custom fields (create)
            logs.append(
                    cufValidator.checkUpdateCustomFields(target, cufValues, model.getTestCaseCufs(target)));

            // 3 - other checks
            // 3-1 : check if the test case is renamed and would induce a
            // potential name clash.
            // arePathsAndNameConsistent() will tell us if the test case is
            // renamed
            checkPathForUpdate(target, name, logs);

            // 3-2 : permissions. note about the following 'if' : the case where
            // the project doesn't exist (and thus has
            // no id) is already covered in the basic checks.
            LogEntry hasntPermission = checkPermissionOnProject(PERM_IMPORT, target, target);
            if (hasntPermission != null) {
                logs.addEntry(hasntPermission);
            }

            // 3-3 : milestone lock ?
            if (model.isTestCaseLockedByMilestones(target)) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_MILESTONE_LOCKED)
                                .build());
            }

            // 3-4 : check audit datas
            // backup the audit log
            List<LogEntry> logEntries =
                    fixMetadata(
                            target,
                            testCase,
                            ImportMode.UPDATE,
                            EntityType.TEST_CASE,
                            bag.activeLogins(),
                            bag.currentUsername());
            logs.addEntries(logEntries);

            testCaseUpdateStrategy.validateMilestones(instr, logs);

            // 3 - 5: check parameters names in prerequisite column
            checkParamNameInString(
                    target, logs, testCase.getPrerequisite(), TestCaseSheetColumn.TC_PRE_REQUISITE);
        }

        return logs;
    }

    @Override
    public void updateRequirementVersions(
            List<RequirementVersionInstruction> instructions, Project project) {
        checkProjectValidity(project);

        preValidationInit(instructions);

        RequirementImportValidationBag bag =
                buildRequirementValidationBag(
                        instructions, project.getId(), project.getRequirementLibrary().getId());

        instructions.forEach(
                instruction -> instruction.addLogs(updateRequirementVersion(instruction, bag)));
    }

    @Override
    public void createRequirementVersions(
            List<RequirementVersionInstruction> instructions, Project project) {
        checkProjectValidity(project);

        preValidationInit(instructions);

        checkRequirementVersionsCreation(instructions, project);
    }

    private void preValidationInit(List<RequirementVersionInstruction> instructions) {
        Set<RequirementVersionTarget> targets = new HashSet<>();

        for (RequirementVersionInstruction instruction : instructions) {
            entityValidator.preVersionValidation(instruction);

            targets.add(instruction.getTarget());
        }

        model.mainInitRequirements(targets);
    }

    private void checkRequirementVersionsCreation(
            List<RequirementVersionInstruction> instructions, Project project) {
        RequirementImportValidationBag bag =
                buildRequirementValidationBag(
                        instructions, project.getId(), project.getRequirementLibrary().getId());

        instructions.forEach(instruction -> requirementVersionCreationValidation(instruction, bag));
    }

    private RequirementImportValidationBag buildRequirementValidationBag(
            List<RequirementVersionInstruction> instructions, Long projectId, Long requirementLibraryId) {

        String currentUsername = userContextService.getUsername();
        List<String> activeLogins = collectRequirementActiveLogins(instructions);

        Set<String> naturesConsistency = collectConsistentCategories(instructions, projectId);
        InfoListItem defaultNature = infoListItemService.findDefaultRequirementCategory(projectId);

        return new RequirementImportValidationBag(
                requirementLibraryId, activeLogins, naturesConsistency, defaultNature, currentUsername);
    }

    private void requirementVersionCreationValidation(
            RequirementVersionInstruction instr, RequirementImportValidationBag validationBag) {

        RequirementVersionTarget target = instr.getTarget();
        RequirementTarget reqTarget = target.getRequirement();
        RequirementVersion reqVersion = instr.getRequirementVersion();
        Map<String, String> cufValues = instr.getCustomFields();

        LogTrain logs;
        LOGGER.debug(
                "Req-Import - In Validation Facility for create {} version {}",
                target.getPath(),
                target.getVersion());

        // 1 - basic verifications
        logs = entityValidator.createRequirementVersionChecks(target, reqVersion, validationBag);

        //   - Check conflict between folder already created by previous import
        // and the path the line we are presently importing
        checkFolderConflict(instr, logs);

        //   - Check status and put the requirement version status to WIP if needed
        //   - The required status will be affected to requirement version in post process
        checkImportedRequirementVersionStatusForCreate(target, reqVersion);

        // 2 - custom fields (create)
        logs.append(
                cufValidator.checkCreateCustomFields(
                        target, cufValues, model.getRequirementVersionCufs(target)));

        // 3 - Check permissions
        LogEntry hasntPermission =
                checkPermissionOnProject(validationBag.requirementLibraryId(), target);
        if (hasntPermission != null) {
            logs.addEntry(hasntPermission);
        }

        // 4 - Check version number
        //		-> change it to the last index +1 if problem and keep trace of the user version number
        checkAndFixRequirementVersionNumber(target, logs);

        // check premium plugin and parent node for high level requirement - needs folder status updated
        entityValidator.createHighLevelRequirementChecks(target, logs);

        // needs version number fixed
        checkNatureUnicity(target, logs);

        // 5 - Check and fix name consistency between path and req name version
        checkAndFixNameConsistency(target, reqVersion);

        // 6 - Check milestone validity
        requirementVersionCreationStrategy.validateMilestones(instr, logs);

        // 7 - Check milestone already used by other requirement version of the same requirement
        checkMilestonesAlreadyUsedInRequirement(instr, logs);

        // 8 - Fix createdOn and createdBy
        logs.addEntries(
                fixMetadata(
                        target,
                        reqVersion,
                        ImportMode.CREATE,
                        EntityType.REQUIREMENT_VERSION,
                        validationBag.activeLogins(),
                        validationBag.currentUsername()));

        // 9 - Now update model if the requirement version has passed all check
        if (!logs.hasCriticalErrors()) {
            model.updateRequirementVersionStatus(
                    target, new TargetStatus(TO_BE_CREATED), instr.getMilestones());
            // if requirement not exists then pass it to status TO_BE_CREATED
            // with actual implementation of addNode in requirement tree, parent node should already have
            // the good status
            if (model.getStatus(reqTarget).getStatus() == NOT_EXISTS) {
                model.updateRequirementStatus(reqTarget, new TargetStatus(TO_BE_CREATED));
            }
        } else {
            // this instruction smell bad..
            instr.fatalError();
        }

        instr.addLogs(logs);
    }

    /**
     * Set the requirementVersionStatus to {@link RequirementStatus#WORK_IN_PROGRESS} to avoid illegal
     * modification exception on {@link RequirementVersion} with other status and save the modified
     * {@link RequirementStatus} in target to reassign it in postprocess
     */
    private void checkImportedRequirementVersionStatusForCreate(
            RequirementVersionTarget target, RequirementVersion reqVersion) {
        RequirementStatus requirementVersionStatus = reqVersion.getStatus();
        if (requirementVersionStatus != null
                && requirementVersionStatus != RequirementStatus.WORK_IN_PROGRESS) {
            target.setImportedRequirementStatus(requirementVersionStatus);
            reqVersion.setStatus(RequirementStatus.WORK_IN_PROGRESS);
        }
    }

    /**
     * Check if an existing requirement version has a status which forbid modifications. Used for
     * update
     */
    private void checkExistingRequirementVersionStatus(
            RequirementVersionTarget target, LogTrain logs, RequirementVersion reqVersion) {
        if (logs.hasCriticalErrors()) {
            return;
        }

        Requirement requirement = reqFinderService.findRequirement(target.getRequirement().getId());
        RequirementVersion persistedReqVersion =
                requirement.findRequirementVersion(target.getVersion());
        RequirementStatus persistedStatus = persistedReqVersion.getStatus();

        if (!persistedStatus.isRequirementModifiable()
                && requirement.isHighLevel() == target.getRequirement().isHighLevel()) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_REQUIREMENT_VERSION_STATUS)
                            .build());
        } else if (!persistedStatus.isRequirementModifiable()
                && requirement.isHighLevel() != target.getRequirement().isHighLevel()) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(
                                    Messages.WARN_REQ_VERSION_LOCKED_STATUS_WITH_NATURE_TRANSFORMATION,
                                    REQ_NATURE.header)
                            .withImpact(Messages.IMPACT_REQ_VERSION_LOCKED_STATUS_WITH_NATURE_TRANSFORMATION)
                            .build());
            target.setImportedRequirementStatus(persistedStatus);
            reqVersion.updateStatusWithoutCheck(
                    persistedStatus); // #294 we don't want to check status for req transformation
        } else if (persistedStatus.isRequirementModifiable()) {
            target.setImportedRequirementStatus(reqVersion.getStatus());
            if (!persistedStatus.equals(reqVersion.getStatus())) {
                reqVersion.setStatus(persistedStatus);
            }
        }
    }

    private LogTrain updateRequirementVersion(
            RequirementVersionInstruction instr, RequirementImportValidationBag bag) {
        RequirementVersionTarget target = instr.getTarget();
        RequirementVersion reqVersion = instr.getRequirementVersion();
        Map<String, String> cufValues = instr.getCustomFields();

        LogTrain logs;

        // 1 - basic verifications, if failure on preliminary verification the import is corrupted,
        // return to avoid exception
        logs = entityValidator.updateRequirementChecks(target, reqVersion, bag);

        if (logs.hasCriticalErrors()) {
            instr.fatalError();
            return logs;
        }

        // milestone locked ?
        if (model.isRequirementVersionLockedByMilestones(target)) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_REQUIREMENT_VERSION_MILESTONE_LOCKED)
                            .build());
        }

        if (logs.hasCriticalErrors()) {
            instr.fatalError();
            return logs;
        }

        // 2 - Check if target requirement version exists in database (targetStatus == EXISTS) and id
        // isn't null
        checkRequirementVersionExists(target, logs);
        checkExistingRequirementVersionStatus(target, logs, reqVersion);

        if (logs.hasCriticalErrors()) {
            instr.fatalError();
            return logs;
        }

        logs.append(
                cufValidator.checkUpdateCustomFields(
                        target, cufValues, model.getRequirementVersionCufs(target)));

        entityValidator.highLevelRequirementUpdateTests(target, logs);

        // if something is wrong at this stage, either the requirement or the version are unknown or
        // locked,
        // return now to avoid nasty exception in next checks
        if (logs.hasCriticalErrors()) {
            instr.fatalError();
            return logs;
        }

        LogEntry hasntPermission = checkPermissionOnProject(PERM_IMPORT, target, target);
        if (hasntPermission != null) {
            logs.addEntry(hasntPermission);
        }

        checkAndFixNameConsistency(target, reqVersion);

        requirementVersionUpdateStrategy.validateMilestones(instr, logs);

        checkMilestonesAlreadyUsedInRequirement(instr, logs);

        logs.addEntries(
                fixMetadata(
                        target,
                        reqVersion,
                        ImportMode.UPDATE,
                        EntityType.REQUIREMENT_VERSION,
                        bag.activeLogins(),
                        bag.currentUsername()));

        if (logs.hasCriticalErrors()) {
            instr.fatalError();
        }

        return logs;
    }

    private void checkRequirementVersionExists(RequirementVersionTarget target, LogTrain logs) {

        TargetStatus status = model.getStatus(target);
        TargetStatus reqStatus = model.getStatus(target.getRequirement());

        // checking requirement and loading his id
        if (reqStatus.getStatus() != Existence.EXISTS || reqStatus.getId() == null) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_REQUIREMENT_VERSION_NOT_EXISTS)
                            .build());
        }

        // checking requirement version and loading his id
        else if (status.getStatus() != Existence.EXISTS || status.getId() == null) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(Messages.ERROR_REQUIREMENT_VERSION_NOT_EXISTS)
                            .build());
        } else {
            // setting the id also in the instruction target, more convenient for updating operations
            target.getRequirement().setId(reqStatus.getId());
        }
    }

    private void checkFolderConflict(RequirementVersionInstruction instr, LogTrain logs) {
        RequirementVersionTarget target = instr.getTarget();
        if (model.isRequirementFolder(target)) {
            logs.addEntry(
                    LogEntry.warning()
                            .forTarget(target)
                            .withMessage(Messages.WARN_REQ_PATH_IS_FOLDER)
                            .withImpact(Messages.IMPACT_REQ_RENAMED)
                            .build());
            // now changing the path of target
            fixPathFolderConflict(instr);
        }
    }

    private void fixPathFolderConflict(RequirementVersionInstruction instr) {
        RequirementVersionTarget target = instr.getTarget();

        // changing path
        target.getRequirement().setPath(appendReqNameSuffix(target.getPath()));

        // changing name to avoid further conflict when postProcessRenaming occurs
        // so already renamed requirement for conflict will not be renamed again...
        String name = PathUtils.extractEntityName(target.getPath());
        instr.getRequirementVersion().setName(name);
    }

    private String appendReqNameSuffix(String name) {
        return name + Messages.REQ_RENAME_SUFFIX;
    }

    private void checkAndFixNameConsistency(
            RequirementVersionTarget target, RequirementVersion reqVersion) {
        if (!model.isCurrentVersion(target)) {
            return;
        }
        String reqName = PathUtils.extractEntityName(target.getPath());
        String reqVersionName = reqVersion.getName();
        // TM-107 : if reqName have any "/" this will be escaped by "\/", so we need to unescape it
        // before name comparison
        reqName = PathUtils.unescapePathPartSlashes(reqName);
        if (!reqName.equals(reqVersionName)) {
            target.setUnconsistentName(reqVersionName);
            reqVersion.setName(reqName);
        }
    }

    private void checkMilestonesAlreadyUsedInRequirement(
            RequirementVersionInstruction instr, LogTrain logs) {

        List<String> milestones = instr.getMilestones();
        RequirementVersionTarget target = instr.getTarget();

        for (String milestone : milestones) {
            // checking in model wich keep trace of change since the begining of the batch
            if (model.checkMilestonesAlreadyUsedInRequirement(milestone, target)) {
                logs.addEntry(
                        LogEntry.warning()
                                .forTarget(target)
                                .withMessage(Messages.WARN_MILESTONE_USED, REQ_VERSION_MILESTONE.header)
                                .withImpact(Messages.IMPACT_MILESTONE_NOT_BINDED)
                                .build());
            }
        }
    }

    private void checkNatureUnicity(RequirementVersionTarget target, LogTrain logs) {
        if (target.getVersion() <= 1) {
            return;
        }

        RequirementTarget requirementTarget = target.getRequirement();
        TargetStatus requirementStatus = model.getStatus(requirementTarget);
        if (requirementStatus.getStatus() == EXISTS) {
            boolean isHighLevel = model.isHighLevelRequirement(requirementTarget);
            if (isHighLevel != HIGH_LEVEL_REQUIREMENT.equals(requirementTarget.getType())) {
                addRequirementNatureCollisionErrorEntry(logs, target);
            }
        }
    }

    private void addRequirementNatureCollisionErrorEntry(
            LogTrain logs, RequirementVersionTarget target) {
        logs.addEntry(
                LogEntry.failure()
                        .forTarget(target)
                        .withMessage(Messages.ERROR_REQUIREMENT_NATURE_COLLISION, REQ_NATURE.header)
                        .build());
    }

    private void checkAndFixRequirementVersionNumber(RequirementVersionTarget target, LogTrain logs) {
        // fixing null/0 values from user -> setting versionNumber to 1 (default value for new
        // requirement)
        if (target.getVersion() == null || target.getVersion() <= 0) {
            fixVersionNumber(
                    target, logs, Messages.ERROR_REQUIREMENT_VERSION_NULL, IMPACT_VERSION_NUMBER_MODIFIED);
        }
        // check if requirement exist
        Existence requirementVersionStatus = model.getStatus(target).getStatus();
        Existence requirementStatus = model.getStatus(target.getRequirement()).getStatus();

        LOGGER.debug("ReqImport Checking for version number: {}", target.getVersion());
        LOGGER.debug(
                "ReqImport Status of version: {} {}", target.getVersion(), requirementVersionStatus);

        if (!NOT_EXISTS.equals(requirementStatus) && !NOT_EXISTS.equals(requirementVersionStatus)) {
            fixVersionNumber(
                    target,
                    logs,
                    Messages.ERROR_REQUIREMENT_VERSION_COLLISION,
                    IMPACT_VERSION_NUMBER_MODIFIED);
        }

        checkPreviousVersionExistsAndReplaceVersionNumber(
                target, logs, requirementVersionStatus, requirementStatus);
    }

    // SQUASH-3237
    private void checkPreviousVersionExistsAndReplaceVersionNumber(
            RequirementVersionTarget target,
            LogTrain logs,
            Existence requirementVersionStatus,
            Existence requirementStatus) {

        if (!(requirementVersionStatus == NOT_EXISTS && target.getVersion() > 1)) {
            return;
        }

        if (requirementStatus == NOT_EXISTS || !model.existPriorVersion(target)) {
            fixVersionNumber(
                    target,
                    logs,
                    Messages.ERROR_REQUIREMENT_VERSION_PREVIOUS_NOT_EXISTS,
                    IMPACT_VERSION_NUMBER_MODIFIED_TO_PREVIOUS_NUMBER);
        }
    }

    private void fixVersionNumber(
            RequirementVersionTarget target, LogTrain logs, String errorMessage, String impactMessage) {
        model.fixRequirementVersion(target);
        logs.addEntry(
                LogEntry.warning()
                        .forTarget(target)
                        .withMessage(errorMessage, REQ_VERSION_NUM.header)
                        .withImpact(impactMessage)
                        .build());
    }

    @Override
    public LogTrain deleteRequirementVersion(RequirementVersionInstruction instr) {
        throw new NotImplementedException("Implement me");
    }

    public boolean areMilestoneValid(TestCaseInstruction instr) {
        LogTrain dummy = new LogTrain();
        testCaseUpdateStrategy.validateMilestones(instr, dummy);
        return dummy.hasNoErrorWhatsoever();
    }

    @Override
    public void createCoverages(List<CoverageInstruction> instructions, Project project) {
        checkProjectValidity(project);

        preCoverageValidation(instructions);

        Set<Long> versionIds =
                instructions.stream()
                        .filter(instruction -> !instruction.hasCriticalErrors())
                        .map(
                                instruction ->
                                        model.getRequirementVersionId(instruction.getTarget().getRequirementVersion()))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());

        Set<Long> requirementIds =
                instructions.stream()
                        .filter(instruction -> !instruction.hasCriticalErrors())
                        .map(
                                instruction ->
                                        model.getRequirementId(instruction.getTarget().getRequirementVersion()))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());

        Map<Long, List<Long>> existingVerifiedVersions =
                requirementImportDao.findVerifiedTestCaseIdsByVersionIds(versionIds);
        Map<Long, RequirementStatus> requirementStatuses =
                requirementImportDao.findRequirementStatusesByVersionIds(versionIds);
        Map<Long, Map<Long, Long>> existingCoverages =
                requirementImportDao.findExistingCoveragesByRequirementIds(requirementIds);

        instructions.stream()
                .filter(instruction -> !instruction.hasCriticalErrors())
                .forEach(
                        instruction ->
                                checkCoverageCreation(
                                        instruction, existingVerifiedVersions, requirementStatuses, existingCoverages));
    }

    private void checkCoverageCreation(
            CoverageInstruction instruction,
            Map<Long, List<Long>> existingVerifiedVersions,
            Map<Long, RequirementStatus> requirementStatuses,
            Map<Long, Map<Long, Long>> existingCoverages) {
        CoverageTarget target = instruction.getTarget();

        RequirementVersionTarget versionTarget = target.getRequirementVersion();

        TargetStatus versionStatus = model.getStatus(versionTarget);
        TargetStatus requirementStatus = model.getStatus(versionTarget.getRequirement());
        TargetStatus testCaseStatus = model.getStatus(target.getTestCase());

        if (!coversExitingEntities(instruction, versionStatus, requirementStatus, testCaseStatus)) {
            return;
        }

        if (!hasPermissions(instruction)) {
            return;
        }

        Long versionId = versionStatus.getId();
        Long requirementId = requirementStatus.getId();
        Long tcId = testCaseStatus.getId();

        target.getTestCase().setId(tcId);
        target.getRequirementVersion().getRequirement().setId(requirementId);

        // Current version already covered the test case
        List<Long> existingCoveragesForReq =
                existingVerifiedVersions.getOrDefault(versionId, Collections.emptyList());
        if (existingCoveragesForReq.contains(tcId)) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_LINK_ALREADY_EXIST, Messages.IMPACT_LINE_IGNORED);
            return;
        }

        // Prevent duplicate line in the same import
        existingVerifiedVersions.computeIfAbsent(versionId, k -> new ArrayList<>()).add(tcId);

        if (versionStatus.getStatus() == Existence.EXISTS
                && !requirementStatuses.get(versionId).isRequirementLinkable()) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_REQUIREMENT_VERSION_STATUS, null);
            return;
        }

        // a previous version covers the test case
        Long existingCoverageId =
                existingCoverages.getOrDefault(requirementId, Collections.emptyMap()).get(tcId);

        if (existingCoverageId != null) {
            instruction.setCoverageId(existingCoverageId);
        }
    }

    private static boolean coversExitingEntities(
            CoverageInstruction instruction,
            TargetStatus versionStatus,
            TargetStatus requirementStatus,
            TargetStatus testCaseStatus) {
        if (requirementStatus.getStatus() == NOT_EXISTS) {
            instruction.addLogEntry(ImportStatus.FAILURE, Messages.ERROR_REQUIREMENT_NOT_EXISTS, null);
            return false;
        }

        if (versionStatus.getStatus() == NOT_EXISTS) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_REQUIREMENT_VERSION_NOT_EXISTS, null);
            return false;
        }

        if (testCaseStatus.getStatus() == NOT_EXISTS) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE,
                    Messages.ERROR_TC_NOT_FOUND,
                    null,
                    instruction.getTarget().getTcPath());
            return false;
        }

        return true;
    }

    private boolean hasPermissions(CoverageInstruction instruction) {
        boolean hasPermission = true;

        CoverageTarget coverageTarget = instruction.getTarget();

        String tcPath = coverageTarget.getTcPath();
        Long tcLibraryId =
                model.getProjectStatus(coverageTarget.getTestCase().getProject()).getTestCaseLibraryId();

        if (tcLibraryId != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, PERM_IMPORT, tcLibraryId, TEST_CASE_LIBRARY_CLASSNAME)) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_NO_PERMISSION, null, PERM_IMPORT, tcPath);
            hasPermission = false;
        }

        String reqPath = coverageTarget.getReqPath();
        Long reqLibraryId =
                model
                        .getProjectStatus(coverageTarget.getRequirementVersion().getProject())
                        .getRequirementLibraryId();

        if (reqLibraryId != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, PERM_IMPORT, reqLibraryId, REQUIREMENT_VERSION_LIBRARY_CLASSNAME)) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_NO_PERMISSION, null, PERM_IMPORT, reqPath);
            hasPermission = false;
        }

        return hasPermission;
    }

    private void preCoverageValidation(List<CoverageInstruction> coverageInstructions) {
        Set<RequirementVersionTarget> requirementVersionTargets = new HashSet<>();
        Set<TestCaseTarget> testCaseTargets = new HashSet<>();

        for (CoverageInstruction instruction : coverageInstructions) {
            entityValidator.preCoverageValidation(instruction);

            if (instruction.hasCriticalErrors()) {
                continue;
            }

            CoverageTarget target = instruction.getTarget();
            RequirementTarget reqTarget = new RequirementTarget(target.getReqPath());
            RequirementVersionTarget reqVersionTarget =
                    new RequirementVersionTarget(reqTarget, target.getReqVersion());

            target.setRequirementVersion(reqVersionTarget);
            requirementVersionTargets.add(reqVersionTarget);

            TestCaseTarget tcTarget = new TestCaseTarget(target.getTcPath());

            target.setTestCase(tcTarget);
            testCaseTargets.add(tcTarget);
        }

        model.initRequirements(requirementVersionTargets);
        model.mainInitTestCase(testCaseTargets);
    }

    private void checkLinkedLowLevelAlreadyExist(
            LinkedLowLevelRequirementTarget target,
            LogTrain logs,
            Long highLevelReqId,
            Long standardReqId) {
        Requirement req = reqFinderService.findRequirement(standardReqId);
        if (req.getHighLevelRequirement() != null) {
            if (highLevelReqId.equals(req.getHighLevelRequirement().getId())) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_LINK_ALREADY_EXIST)
                                .withImpact(Messages.IMPACT_LINE_IGNORED)
                                .build());
            } else {
                logs.addEntry(
                        LogEntry.warning()
                                .forTarget(target)
                                .withMessage(Messages.WARN_REQ_ALREADY_LINKED_TO_HIGH_LVL_REQ)
                                .withImpact(Messages.IMPACT_HIGH_LVL_REQ_REFERENCE_LOSS)
                                .build());
            }
        }
    }

    @Override
    public void createOrUpdateRequirementLinks(
            List<RequirementLinkInstruction> instructions, Project project) {
        checkProjectValidity(project);

        preRequirementLinkValidation(instructions);

        Set<Long> versionIds =
                instructions.stream()
                        .map(RequirementLinkInstruction::getTarget)
                        .flatMap(
                                target ->
                                        Stream.of(
                                                model.getRequirementVersionId(target.getSourceVersion()),
                                                model.getRequirementVersionId(target.getDestVersion())))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());

        Map<Long, Map<Long, Long>> existingLinks =
                requirementImportDao.findExistingLinksByVersionIds(versionIds);
        Map<Long, RequirementStatus> requirementStatuses =
                requirementImportDao.findRequirementStatusesByVersionIds(versionIds);
        String defaultRole = requirementImportDao.getDefaultRequirementVersionLinkRole();

        instructions.stream()
                .filter(instruction -> !instruction.hasCriticalErrors())
                .forEach(
                        instruction ->
                                checkRequirementLink(instruction, requirementStatuses, existingLinks, defaultRole));
    }

    private void checkRequirementLink(
            RequirementLinkInstruction instruction,
            Map<Long, RequirementStatus> requirementStatuses,
            Map<Long, Map<Long, Long>> linkIdByVersions,
            String defaultRole) {
        RequirementLinkTarget target = instruction.getTarget();

        // check source requirement
        RequirementVersionTarget sourceVersion = target.getSourceVersion();
        existAndLinkable(
                instruction,
                sourceVersion,
                requirementStatuses,
                Messages.ERROR_SOURCE_REQUIREMENT_PATH_MALFORMED,
                Messages.ERROR_SOURCE_REQUIREMENT_NOT_EXIST);

        // check dest requirement
        RequirementVersionTarget dest = target.getDestVersion();
        existAndLinkable(
                instruction,
                dest,
                requirementStatuses,
                Messages.ERROR_DEST_REQUIREMENT_PATH_MALFORMED,
                Messages.ERROR_DEST_REQUIREMENT_NOT_EXIST);

        // cannot link a requirement to itself
        if (!instruction.hasCriticalErrors()
                && target.getSourceVersion().equals(target.getDestVersion())) {
            instruction.addLogEntry(
                    ImportStatus.FAILURE, Messages.ERROR_REQ_LINK_SAME_VERSION, Messages.IMPACT_LINE_IGNORED);
            return;
        }

        // check the role
        checkRequirementLinkRole(instruction, defaultRole);

        // check if the link already exists
        Long sourceId = model.getRequirementVersionId(sourceVersion);
        Long destId = model.getRequirementVersionId(dest);

        Long existingOutboundLinkId =
                linkIdByVersions.getOrDefault(sourceId, Collections.emptyMap()).get(destId);
        Long existingInboundLinkId =
                linkIdByVersions.getOrDefault(destId, Collections.emptyMap()).get(sourceId);

        if (existingOutboundLinkId != null && existingInboundLinkId != null) {
            target.setExistingOutboundLinkId(existingOutboundLinkId);
            target.setExistingInboundLinkId(existingInboundLinkId);
        }
    }

    private void preRequirementLinkValidation(
            List<RequirementLinkInstruction> requirementLinkInstructions) {
        Set<RequirementVersionTarget> requirementVersionTargets = new HashSet<>();

        for (RequirementLinkInstruction instruction : requirementLinkInstructions) {
            entityValidator.preRequirementLinkValidation(instruction);

            if (instruction.hasCriticalErrors()) {
                continue;
            }

            RequirementLinkTarget target = instruction.getTarget();

            requirementVersionTargets.add(target.getSourceVersion());
            requirementVersionTargets.add(target.getDestVersion());
        }

        model.initRequirements(requirementVersionTargets);
    }

    private Long checkHighLevelReqForLinkedLowLevelRequirement(
            LinkedLowLevelRequirementTarget target, LogTrain logs) {
        Long highLevelReqId = reqFinderService.findNodeIdByPath(target.getHighLevelReqPath());
        if (highLevelReqId != null) {
            if (!permissionService.hasRoleOrPermissionOnObject(
                    ROLE_ADMIN, PERM_READ, highLevelReqId, Requirement.class.getName())) {
                logs.addEntry(
                        createLogFailure(
                                target, Messages.ERROR_NO_PERMISSION, "READ", target.getHighLevelReqPath()));
                return null;
            }
            if (!permissionService.hasRoleOrPermissionOnObject(
                    ROLE_ADMIN, PERM_LINK, highLevelReqId, Requirement.class.getName())) {
                logs.addEntry(
                        createLogFailure(
                                target, Messages.ERROR_NO_PERMISSION, "LINK", target.getHighLevelReqPath()));
                return null;
            }
        }

        HighLevelRequirementTarget highLevelReqTarget =
                new HighLevelRequirementTarget(target.getHighLevelReqPath());
        Existence reqStatus = model.getStatus(highLevelReqTarget).getStatus();

        return checkReqTargetExistenceForLinkedLowLevelRequirement(
                logs, reqStatus, target, highLevelReqId, true);
    }

    private Long checkReqTargetExistenceForLinkedLowLevelRequirement(
            LogTrain logs,
            Existence reqStatus,
            LinkedLowLevelRequirementTarget target,
            Long reqId,
            boolean isFromHighLevelReqPath) {
        if (reqStatus == NOT_EXISTS) {
            logs.addEntry(createLogFailure(target, Messages.ERROR_REQUIREMENT_NOT_EXISTS));
            return null;
        }

        if (reqStatus == EXISTS && reqId != null) {
            Requirement req = reqFinderService.findRequirement(reqId);
            if (!req.isHighLevel() && isFromHighLevelReqPath) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(
                                        Messages.ERROR_REQUIREMENT_NOT_HIGH_LEVEL, HIGH_LEVEL_REQ_PATH.getHeader())
                                .build());
                return null;
            }
            if (req.isHighLevel() && !isFromHighLevelReqPath) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_REQUIREMENT_NOT_STANDARD, STANDARD_REQ_PATH.getHeader())
                                .build());
                return null;
            }
            if (!req.getStatus().isRequirementLinkable()) {
                logs.addEntry(createLogFailure(target, Messages.ERROR_REQUIREMENT_VERSION_STATUS));
                return null;
            }
            return req.getId();
        }
        return null;
    }

    private Long checkReqForLinkedLowLevelRequirement(
            LinkedLowLevelRequirementTarget target, LogTrain logs) {
        Long reqId = reqFinderService.findNodeIdByPath(target.getStandardReqPath());
        if (reqId != null) {
            if (!permissionService.hasRoleOrPermissionOnObject(
                    ROLE_ADMIN, PERM_READ, reqId, Requirement.class.getName())) {
                logs.addEntry(
                        createLogFailure(
                                target, Messages.ERROR_NO_PERMISSION, "READ", target.getStandardReqPath()));
                return null;
            }
            if (!permissionService.hasRoleOrPermissionOnObject(
                    ROLE_ADMIN, PERM_LINK, reqId, Requirement.class.getName())) {
                logs.addEntry(
                        createLogFailure(
                                target, Messages.ERROR_NO_PERMISSION, "LINK", target.getStandardReqPath()));
                return null;
            }
        }
        if (entityValidator.isChildRequirementByPath(target.getStandardReqPath())) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(target)
                            .withMessage(
                                    Messages.ERROR_CANNOT_LINK_TO_CHILD_LOW_LEVEL_REQUIREMENT,
                                    STANDARD_REQ_PATH.getHeader())
                            .build());
            return null;
        }

        RequirementTarget reqTarget = new RequirementTarget(target.getStandardReqPath());
        Existence reqStatus = model.getStatus(reqTarget).getStatus();

        return checkReqTargetExistenceForLinkedLowLevelRequirement(
                logs, reqStatus, target, reqId, false);
    }

    private boolean checkHighLevelStandardRequirementPathsAreValid(
            LinkedLowLevelRequirementTarget target, LogTrain logs) {
        boolean isTargetWellFormed = target.isWellFormed();
        if (!isTargetWellFormed) {
            if (!target.isHighLevelReqPathWellFormed()) {
                logs.addEntry(
                        createLogFailure(target, Messages.ERROR_MALFORMED_PATH, target.getHighLevelReqPath()));
            }
            if (!target.isStandardReqPathWellFormed()) {
                logs.addEntry(
                        createLogFailure(target, Messages.ERROR_MALFORMED_PATH, target.getStandardReqPath()));
            }
        }
        return isTargetWellFormed;
    }

    // *************************** linked low level requirements ********************
    @Override
    public LogTrain createLinkedLowLevelRequirement(LinkedLowLevelRequirementInstruction instr) {
        LogTrain logs = new LogTrain();

        LinkedLowLevelRequirementTarget target = instr.getTarget();

        if (!checkHighLevelStandardRequirementPathsAreValid(target, logs)) {
            logs.addEntry(createLogFailure(target, Messages.ERROR_REQUIREMENT_NOT_EXISTS));
            return logs;
        }

        Long highLevelReqId = checkHighLevelReqForLinkedLowLevelRequirement(target, logs);
        Long standardReqId = checkReqForLinkedLowLevelRequirement(target, logs);

        if (logs.hasCriticalErrors()) {
            return logs;
        }

        if (highLevelReqId != null && standardReqId != null) {
            checkLinkedLowLevelAlreadyExist(target, logs, highLevelReqId, standardReqId);
        }

        if (logs.hasCriticalErrors()) {
            return logs;
        }

        logs.addEntry(checkPermissionOnProject(target));

        return logs;
    }

    @Override
    public LogTrain deleteRequirementLink(RequirementLinkInstruction instr) {
        // check that the target is valid
        return requirementsExistAndLinkable(instr);
    }

    private void checkRequirementLinkRole(RequirementLinkInstruction instr, String defaultRole) {
        String role = instr.getRelationRole();

        // if no role set -> issue a warning that the system will use the default
        if (StringUtils.isBlank(role)) {
            instr.addLogEntry(
                    ImportStatus.WARNING,
                    Messages.WARN_REQ_LINK_ROLE_NOT_SET,
                    Messages.IMPACT_REQ_LINK_ROLE_NOT_SET);
            instr.setRelationRole(defaultRole);
            return;
        }

        // if role is set -> check it exists
        Set<String> allRoles = model.getRequirementLinkRoles();

        if (!allRoles.contains(role)) {
            instr.addLogEntry(ImportStatus.FAILURE, Messages.ERROR_REQ_LINK_ROLE_NOT_EXIST, role);
        }
    }

    private void existAndLinkable(
            RequirementLinkInstruction instr,
            RequirementVersionTarget versionTarget,
            Map<Long, RequirementStatus> requirementStatuses,
            String malformedPathMessage,
            String nonexistentMessage) {
        // 1 - source path must be supplied and well formed
        if (!versionTarget.isWellFormed()) {
            instr.addLogEntry(ImportStatus.FAILURE, malformedPathMessage, null, versionTarget.getPath());
            return;
        }

        // 2 - project must exist and have the right permissions
        ProjectTargetStatus projectStatus = model.getProjectStatus(versionTarget.getProject());
        if (projectStatus.getStatus() != Existence.EXISTS) {
            instr.addLogEntry(ImportStatus.FAILURE, Messages.ERROR_PROJECT_NOT_EXIST, null);
            return;
        }

        Long requirementLibraryId = projectStatus.getRequirementLibraryId();
        if (requirementLibraryId != null
                && !permissionService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN,
                        Permissions.LINK.name(),
                        requirementLibraryId,
                        REQUIREMENT_VERSION_LIBRARY_CLASSNAME)) {
            instr.addLogEntry(
                    ImportStatus.FAILURE,
                    Messages.ERROR_NO_PERMISSION,
                    null,
                    Permissions.LINK.name(),
                    versionTarget.getPath());
            return;
        }

        // 3 - RequirementVersion must exist or will be created
        TargetStatus versionStatus = model.getStatus(versionTarget);

        if (versionStatus.getStatus() != Existence.EXISTS
                && versionStatus.getStatus() != Existence.TO_BE_CREATED) {
            instr.addLogEntry(ImportStatus.FAILURE, nonexistentMessage, null);
            return;
        }

        // 4 - RequirementVersion must be linkable (i.e. not Obsolete) or will be linkable
        Long versionId = model.getRequirementVersionId(versionTarget);
        if (versionId != null) {

            RequirementStatus status = requirementStatuses.get(versionId);

            if (!status.isRequirementLinkable()) {
                instr.addLogEntry(ImportStatus.FAILURE, Messages.ERROR_REQ_LINK_NOT_LINKABLE, null);
            }
        } else {
            // in case of the requirement will be created, and now we verify its imported status
            if (!versionTarget.getImportedRequirementStatus().isRequirementLinkable()) {
                instr.addLogEntry(ImportStatus.FAILURE, Messages.ERROR_REQ_LINK_NOT_LINKABLE, null);
            }
        }
    }

    /*
     * both version must :
     * - exist,
     * - be linkable
     */
    private LogTrain requirementsExistAndLinkable(RequirementLinkInstruction instr) {
        LogTrain logs = new LogTrain();

        RequirementLinkTarget linkTarget = instr.getTarget();

        // check source requirement
        RequirementVersionTarget source = linkTarget.getSourceVersion();
        existAndLinkable(
                linkTarget,
                source,
                logs,
                Messages.ERROR_SOURCE_REQUIREMENT_PATH_MALFORMED,
                Messages.ERROR_SOURCE_REQUIREMENT_NOT_EXIST);

        // check dest requirement
        RequirementVersionTarget dest = linkTarget.getDestVersion();
        existAndLinkable(
                linkTarget,
                dest,
                logs,
                Messages.ERROR_DEST_REQUIREMENT_PATH_MALFORMED,
                Messages.ERROR_DEST_REQUIREMENT_NOT_EXIST);

        // cannot link a requirement to itself
        if (!logs.hasCriticalErrors()
                && linkTarget.getSourceVersion().equals(linkTarget.getDestVersion())) {

            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(linkTarget)
                            .withMessage(Messages.ERROR_REQ_LINK_SAME_VERSION)
                            .build());
        }
        return logs;
    }

    private void existAndLinkable(
            RequirementLinkTarget linkTarget,
            RequirementVersionTarget versionTarget,
            LogTrain logs,
            String malformedPathMessage,
            String nonexistentMessage) {
        // 1 - source path must be supplied and well formed
        if (!versionTarget.isWellFormed()) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(linkTarget)
                            .withMessage(malformedPathMessage, versionTarget.getPath())
                            .build());
            return;
        }

        // 2 - project must exist
        TargetStatus projectStatus = model.getProjectStatus(versionTarget.getProject());
        if (projectStatus.getStatus() != Existence.EXISTS) {
            logs.addEntry(
                    LogEntry.failure()
                            .forTarget(linkTarget)
                            .withMessage(Messages.ERROR_PROJECT_NOT_EXIST)
                            .build());
            return;
        }

        // 3 - RequirementVersion must exist or will be created
        TargetStatus versionStatus = model.getStatus(versionTarget);
        if (versionStatus.getStatus() != Existence.EXISTS
                && versionStatus.getStatus() != Existence.TO_BE_CREATED) {
            logs.addEntry(
                    LogEntry.failure().forTarget(linkTarget).withMessage(nonexistentMessage).build());
            return;
        }

        // 4 - RequirementVersion must be linkable (i.e. not Obsolete) or will be linkable
        Long reqId = reqFinderService.findNodeIdByPath(versionTarget.getPath());
        if (reqId != null) {
            // in case of the requirement already exist
            Requirement req = reqFinderService.findRequirement(reqId);
            RequirementVersion reqVersion = req.findRequirementVersion(versionTarget.getVersion());
            if (!reqVersion.getStatus().isRequirementLinkable()) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(linkTarget)
                                .withMessage(Messages.ERROR_REQ_LINK_NOT_LINKABLE)
                                .build());
                return;
            }
        } else {
            // in case of the requirement will be created, and now we verify its imported status
            if (!versionTarget.getImportedRequirementStatus().isRequirementLinkable()) {
                logs.addEntry(
                        LogEntry.failure()
                                .forTarget(linkTarget)
                                .withMessage(Messages.ERROR_REQ_LINK_NOT_LINKABLE)
                                .build());
                return;
            }
        }

        // 5 - User must have high enough credentials
        LogEntry entry = checkPermissionOnProject("LINK", versionTarget, linkTarget);
        if (entry != null) {
            logs.addEntry(entry);
        }
    }

    private LogEntry createLogFailure(Target target, String msg, Object... msgArgs) {
        return LogEntry.failure().forTarget(target).withMessage(msg, msgArgs).build();
    }

    @Override
    public void createParameters(List<ParameterInstruction> parameterInstructions, Project project) {
        checkProjectValidity(project);

        Set<TestCaseTarget> testCaseTargets =
                parameterInstructions.stream()
                        .map(
                                instruction -> {
                                    ParameterTarget target = instruction.getTarget();
                                    target.setName(instruction.getParameter().getName());
                                    return target.getOwner();
                                })
                        .collect(Collectors.toSet());

        model.initParameters(testCaseTargets);

        parameterInstructions.forEach(this::checkParameterCreation);
    }

    @Override
    public void createDatasets(List<DatasetInstruction> instructions, Project project) {
        checkProjectValidity(project);

        Set<TestCaseTarget> testCaseTargets =
                instructions.stream()
                        .map(instruction -> instruction.getTarget().getTestCase())
                        .collect(Collectors.toSet());

        model.initStatuses(testCaseTargets);

        instructions.forEach(this::checkDatasetCreation);
    }

    @Override
    public void addActionSteps(List<ActionStepInstruction> instructions, Project project) {
        checkProjectValidity(project);

        Set<TestCaseTarget> testCaseTargets =
                instructions.stream()
                        .map(instruction -> instruction.getTarget().getTestCase())
                        .collect(Collectors.toSet());

        model.initStatuses(testCaseTargets);

        instructions.forEach(this::checkActionStepAddition);
    }

    @Override
    public void addCallSteps(List<CallStepInstruction> instructions, Project project) {
        checkProjectValidity(project);

        initializationsForCallSteps(instructions);

        instructions.forEach(this::checkCallStepCreation);
    }

    private void initializationsForCallSteps(List<CallStepInstruction> instructions) {
        Map<String, Set<TestCaseTarget>> targetsByProject =
                instructions.stream()
                        .flatMap(i -> Stream.of(i.getTarget().getTestCase(), i.getCalledTC()))
                        .collect(Collectors.groupingBy(TestCaseTarget::getProject, Collectors.toSet()));

        Set<Long> ids = new HashSet<>();

        for (var entry : targetsByProject.entrySet()) {
            Map<TestCaseTarget, Long> targetIds = model.getTargetIds(entry.getValue());
            instructions.stream()
                    .filter(
                            i ->
                                    targetIds.containsKey(i.getTarget().getTestCase())
                                            && targetIds.containsKey(i.getCalledTC()))
                    .flatMap(i -> Stream.of(i.getTarget().getTestCase(), i.getCalledTC()))
                    .map(targetIds::get)
                    .forEach(ids::add);
        }

        if (ids.isEmpty()) {
            return;
        }

        model.initRecursiveCallGraph(ids);
    }

    @Override
    public void addDatasetParametersValues(
            List<DatasetParamValueInstruction> instructions, Project project) {
        checkProjectValidity(project);

        Set<TestCaseTarget> testCaseTargets =
                instructions.stream()
                        .flatMap(i -> Stream.of(i.getTarget().getTestCase(), i.getParameterTarget().getOwner()))
                        .collect(Collectors.toSet());

        model.initStatuses(testCaseTargets);
        model.initCallGraph(testCaseTargets);

        instructions.forEach(this::checkDatasetParameterValueCreation);
    }

    private void checkDatasetParameterValueCreation(DatasetParamValueInstruction instruction) {
        LogTrain logs =
                failsafeUpdateParameterValue(
                        instruction.getTarget(),
                        instruction.getParameterTarget(),
                        instruction.getDatasetValue().getValue(),
                        false);
        instruction.addLogs(logs);
    }

    /**
     * Strategy for validating milestones. Should be specialized in create and update
     *
     * @author Gregory Fouquet
     */
    private abstract class AbstractMilestonesValidationStrategy<
            I extends Instruction<T> & Milestoned, T extends Target & WithPath> {
        public void validateMilestones(I instr, LogTrain logs) {
            T target = instr.getTarget();

            if (!(milestonesEnabled || instr.getMilestones().isEmpty())) {
                logs.addEntry(
                        logEntry()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_MILESTONE_FEATURE_DEACTIVATED)
                                .build());
            }

            if (milestonesEnabled) {
                Partition existing = milestoneHelper.partitionExisting(instr.getMilestones());
                Partition bindables =
                        milestoneHelper.partitionBindable(existing.passing, target.getProject());
                logs.addEntries(logUnknownMilestones(target, existing.rejected));
                logs.addEntries(logUnbindableMilestones(target, bindables.rejected));
            }
        }

        protected abstract LogEntry.Builder logEntry();

        protected List<LogEntry> logUnbindableMilestones(T target, List<String> rejected) {
            ArrayList<LogEntry> logs = new ArrayList<>(rejected.size());
            for (String name : rejected) {
                logs.add(
                        logEntry()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_WRONG_MILESTONE_STATUS, name)
                                .build());
            }
            return logs;
        }

        protected List<LogEntry> logUnknownMilestones(T target, List<String> rejected) {
            ArrayList<LogEntry> logs = new ArrayList<>(rejected.size());
            for (String name : rejected) {
                logs.add(
                        logEntry()
                                .forTarget(target)
                                .withMessage(Messages.ERROR_UNKNOWN_MILESTONE, name)
                                .build());
            }
            return logs;
        }
    }

    private final class CreationStrategy<
                    I extends Instruction<T> & Milestoned, T extends Target & WithPath>
            extends AbstractMilestonesValidationStrategy<I, T> {
        /**
         * @see AbstractMilestonesValidationStrategy#logEntry()
         */
        @Override
        protected Builder logEntry() {
            return LogEntry.failure();
        }
    }

    private final class UpdateStrategy<
                    I extends Instruction<T> & Milestoned, T extends Target & WithPath>
            extends AbstractMilestonesValidationStrategy<I, T> {
        /**
         * @see AbstractMilestonesValidationStrategy#logEntry()
         */
        @Override
        protected Builder logEntry() {
            return LogEntry.warning();
        }
    }
}
