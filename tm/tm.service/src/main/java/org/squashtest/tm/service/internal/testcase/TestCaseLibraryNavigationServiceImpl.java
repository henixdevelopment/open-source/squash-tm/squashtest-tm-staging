/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import static org.squashtest.tm.domain.EntityType.TEST_CASE_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bdd.BddImplementationTechnology;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.library.NewFolderDto;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNodeVisitor;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.library.NameAlreadyExistsAtDestinationException;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.CheckBlockingMilestones;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.annotation.PreventConcurrents;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.importer.ImportLog;
import org.squashtest.tm.service.importer.XlsImportLimitationHandler;
import org.squashtest.tm.service.internal.batchexport.TestCaseExcelExporterService;
import org.squashtest.tm.service.internal.batchimport.testcase.TestCaseExcelBatchImporter;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.customreport.NameResolver;
import org.squashtest.tm.service.internal.library.AbstractLibraryNavigationService;
import org.squashtest.tm.service.internal.library.NodeDeletionHandler;
import org.squashtest.tm.service.internal.library.PasteStrategy;
import org.squashtest.tm.service.internal.library.PathService;
import org.squashtest.tm.service.internal.repository.FolderDao;
import org.squashtest.tm.service.internal.repository.KeywordTestCaseDao;
import org.squashtest.tm.service.internal.repository.LibraryDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.ScriptedTestCaseDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.TestCaseFolderDao;
import org.squashtest.tm.service.internal.repository.TestCaseLibraryDao;
import org.squashtest.tm.service.internal.repository.TestCaseLibraryNodeDao;
import org.squashtest.tm.service.internal.testcase.coercers.TCLNAndParentIdsCoercerForArray;
import org.squashtest.tm.service.internal.testcase.coercers.TCLNAndParentIdsCoercerForList;
import org.squashtest.tm.service.internal.testcase.coercers.TestCaseLibraryIdsCoercerForArray;
import org.squashtest.tm.service.internal.testcase.coercers.TestCaseLibraryIdsCoercerForList;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.milestone.MilestoneMembershipManager;
import org.squashtest.tm.service.requirement.VerifiedRequirementsManagerService;
import org.squashtest.tm.service.statistics.testcase.TestCaseStatisticsBundle;
import org.squashtest.tm.service.testcase.TestCaseLibraryFinderService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.service.testcase.TestCaseStatisticsService;
import org.squashtest.tm.service.testcase.fromreq.ReqToTestCaseConfiguration;
import org.squashtest.tm.service.testcase.scripted.KeywordTestCaseToFileStrategy;

@Service("squashtest.tm.service.TestCaseLibraryNavigationService")
@Transactional
public class TestCaseLibraryNavigationServiceImpl
        extends AbstractLibraryNavigationService<TestCaseLibrary, TestCaseFolder, TestCaseLibraryNode>
        implements TestCaseLibraryNavigationService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestCaseLibraryNavigationServiceImpl.class);

    private static final String EXPORT = "EXPORT";
    private static final String TEST_CASE_CLASS_NAME = "org.squashtest.tm.domain.testcase.TestCase";
    private static final String DESTINATION_ID = "destinationId";
    private static final String SOURCE_NODES_IDS = "sourceNodesIds";
    private static final String TARGET_ID = "targetId";
    private static final String TARGET_IDS = "targetIds";
    private static final String SIMPLE = "simple";
    private static final String EXTENSION_DELIMITER = ".";

    @Inject private TestCaseLibraryDao testCaseLibraryDao;
    @Inject private TestCaseFolderDao testCaseFolderDao;
    @Inject private TestCaseDao testCaseDao;

    @Inject
    @Qualifier("squashtest.tm.repository.TestCaseLibraryNodeDao")
    private TestCaseLibraryNodeDao testCaseLibraryNodeDao;

    @Inject private ScriptedTestCaseDao scriptedTestCaseDao;
    @Inject private KeywordTestCaseDao keywordTestCaseDao;

    @Inject private TestCaseNodeDeletionHandler deletionHandler;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToTestCaseFolderStrategy")
    private Provider<PasteStrategy<TestCaseFolder, TestCaseLibraryNode>>
            pasteToTestCaseFolderStrategyProvider;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToTestCaseLibraryStrategy")
    private Provider<PasteStrategy<TestCaseLibrary, TestCaseLibraryNode>>
            pasteToTestCaseLibraryStrategyProvider;

    @Inject private TestCaseStatisticsService statisticsService;

    @Inject private TestCaseCallTreeFinder calltreeService;

    @Inject private TestCaseExcelExporterService excelService;

    @Inject private ProjectDao projectDao;

    @Inject private TestCaseExcelBatchImporter batchImporter;

    @Inject private PathService pathService;

    @Inject private MilestoneMembershipManager milestoneService;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private CustomFieldBindingFinderService customFieldBindingFinderService;

    @Inject private PrivateCustomFieldValueService customFieldValueService;

    @Inject private DSLContext dsl;

    @Inject private VerifiedRequirementsManagerService verifiedRequirementsManagerService;

    @Inject private NameResolver nameResolver;

    @Inject private MessageSource messageSource;

    @Inject private XlsImportLimitationHandler xlsImportLimitationHandler;

    @PersistenceContext private EntityManager entityManager;

    @Override
    protected NodeDeletionHandler<TestCaseLibraryNode, TestCaseFolder> getDeletionHandler() {
        return deletionHandler;
    }

    @Override
    protected LibraryDao<TestCaseLibrary, TestCaseLibraryNode> getLibraryDao() {
        return testCaseLibraryDao;
    }

    @Override
    protected FolderDao<TestCaseFolder, TestCaseLibraryNode> getFolderDao() {
        return testCaseFolderDao;
    }

    @Override
    protected TestCaseLibraryNodeDao getLibraryNodeDao() {
        return testCaseLibraryNodeDao;
    }

    @Override
    protected PasteStrategy<TestCaseFolder, TestCaseLibraryNode> getPasteToFolderStrategy() {
        return pasteToTestCaseFolderStrategyProvider.get();
    }

    @Override
    protected PasteStrategy<TestCaseLibrary, TestCaseLibraryNode> getPasteToLibraryStrategy() {
        return pasteToTestCaseLibraryStrategyProvider.get();
    }

    @Override
    @Transactional(readOnly = true)
    public String getPathAsString(long entityId) {
        return pathService.buildTestCasePath(entityId);
    }

    /**
     * {@inheritDoc}
     *
     * @see TestCaseLibraryFinderService#getPathsAsString(List)
     */
    @Override
    @Transactional(readOnly = true)
    public List<String> getPathsAsString(List<Long> ids) {
        return pathService.buildTestCasesPaths(ids);
    }

    /**
     * {@inheritDoc}
     *
     * @see TestCaseLibraryFinderService#findNodesByPath(List)
     */
    @Override
    @Transactional(readOnly = true)
    public List<TestCaseLibraryNode> findNodesByPath(List<String> paths) {
        return getLibraryNodeDao().findNodesByPath(paths);
    }

    /**
     * {@inheritDoc}
     *
     * @see TestCaseLibraryFinderService#findNodeIdsByPath(List)
     */
    @Override
    @Transactional(readOnly = true)
    public List<Long> findNodeIdsByPath(List<String> paths) {
        return getLibraryNodeDao().findNodeIdsByPath(paths);
    }

    /**
     * {@inheritDoc}
     *
     * @see TestCaseLibraryFinderService#findNodeIdByPath(String)
     */
    @Override
    @Transactional(readOnly = true)
    public Long findNodeIdByPath(String path) {
        return getLibraryNodeDao().findNodeIdByPath(path);
    }

    /**
     * {@inheritDoc}
     *
     * @see TestCaseLibraryFinderService#findNodeByPath(String)
     */
    @Override
    @Transactional(readOnly = true)
    public TestCaseLibraryNode findNodeByPath(String path) {
        return getLibraryNodeDao().findNodeByPath(path);
    }

    @Override
    public TestCase findTestCaseByPath(String path) {
        Long id = findNodeIdByPath(path);
        return testCaseDao.findById(id);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.testcase.TestCaseLibrary' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public TestCaseFolder addFolderToLibrary(@Id long destinationId, TestCaseFolder newFolder) {

        TestCaseLibrary container = testCaseLibraryDao.loadForNodeAddition(destinationId);
        container.addContent(newFolder);

        // fix the nature and type for the possible nested test cases inside
        // that folder
        new NatureTypeChainFixer().fix(newFolder);

        // now proceed
        getFolderDao().persist(newFolder);

        // and then create the custom field values, as a better fix for [Issue
        // 2061]
        new CustomFieldValuesFixer().fix(newFolder);

        generateCustomField(newFolder);
        createAttachmentsFromLibraryNode(newFolder, newFolder);
        return newFolder;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.testcase.TestCaseLibrary' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public TestCaseFolder addFolderToLibrary(@Id long destinationId, NewFolderDto folderDto) {
        TestCaseFolder newFolder = (TestCaseFolder) folderDto.toFolder(TEST_CASE_FOLDER);
        return addFolderToLibrary(destinationId, newFolder, folderDto.getCustomFields());
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibraryNode.class)
    public TestCaseFolder addFolderToFolder(@Id long destinationId, TestCaseFolder newFolder) {

        TestCaseFolder container = getFolderDao().loadForNodeAddition(destinationId);
        container.addContent(newFolder);

        // fix the nature and type for the possible nested test cases inside
        // that folder
        new NatureTypeChainFixer().fix(newFolder);

        // now proceed
        getFolderDao().persist(newFolder);

        // and then create the custom field values, as a better fix for [Issue
        // 2061]
        new CustomFieldValuesFixer().fix(newFolder);
        generateCustomField(newFolder);
        createAttachmentsFromLibraryNode(newFolder, newFolder);
        return newFolder;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibraryNode.class)
    public TestCaseFolder addFolderToFolder(@Id long destinationId, NewFolderDto folderDto) {
        TestCaseFolder newFolder = (TestCaseFolder) folderDto.toFolder(TEST_CASE_FOLDER);
        return addFolderToFolder(destinationId, newFolder, folderDto.getCustomFields());
    }

    @Override
    @PreAuthorize(
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.testcase.TestCaseLibrary' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public void addTestCaseToLibrary(@Id long libraryId, TestCase testCase, Integer position) {

        TestCaseLibrary library = testCaseLibraryDao.loadForNodeAddition(libraryId);

        if (!library.isContentNameAvailable(testCase.getName())) {
            throw new DuplicateNameException(testCase.getName(), testCase.getName(), testCase.getName());
        } else {
            if (position != null) {
                library.addContent(testCase, position);
            } else {
                library.addContent(testCase);
            }
            replaceInfoListReferences(testCase);
            testCaseDao.safePersist(testCase);
            createCustomFieldValuesForTestCase(testCase);
        }
    }

    @Override
    public void addFromReqTestCaseToLibrary(
            @Id long libraryId, TestCase testCase, RequirementVersion version, Integer position) {
        TestCaseLibrary library = testCaseLibraryDao.loadForNodeAddition(libraryId);

        if (!library.isContentNameAvailable(testCase.getName())) {
            resolveNameConflict(library.getContentNames(), testCase, 1);
        }
        if (position != null) {
            library.addContent(testCase, position);
        } else {
            library.addContent(testCase);
        }

        replaceInfoListReferences(testCase);
        testCaseDao.safePersist(testCase);
        addCurrentMilestone(testCase);
        verifiedRequirementsManagerService.addVerifiedRequirementVersionsToTestCaseFromReq(
                version, testCase);
        copyAttachmentsFromLibraryNode(testCase, version.getAttachmentList().getId());
    }

    private void addCurrentMilestone(TestCase testCase) {
        this.activeMilestoneHolder
                .getActiveMilestone()
                .ifPresent(
                        milestone ->
                                milestoneService.bindTestCaseToMilestones(
                                        testCase.getId(), Collections.singletonList(milestone.getId())));
    }

    public void addFromReqFolderToLibrary(
            @Id long destinationId, TestCaseFolder newFolder, Long copiedAttachmentListId) {

        TestCaseLibrary container = getLibraryDao().loadForNodeAddition(destinationId);
        if (container.getContentNames().contains(newFolder.getName())) {
            resolveNameConflict(container.getContentNames(), newFolder, 1);
        }

        container.addContent(newFolder);

        new NatureTypeChainFixer().fix(newFolder);

        getFolderDao().persist(newFolder);

        new CustomFieldValuesFixer().fix(newFolder);

        copyAttachmentsFromLibraryNode(newFolder, copiedAttachmentListId);
        generateCustomField(newFolder);
    }

    void resolveNameConflict(List<String> target, TestCaseLibraryNode node, int i) {
        String copySuffix =
                messageSource.getMessage("label.CopySuffix", null, LocaleContextHolder.getLocale());
        String testedName = node.getName() + copySuffix + i;
        if (target.contains(testedName)) {
            resolveNameConflict(target, node, i + 1);
        } else {
            node.setName(testedName);
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#libraryId, 'org.squashtest.tm.domain.testcase.TestCaseLibrary' , 'CREATE' )"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public TestCase addTestCaseToLibrary(
            @Id long libraryId,
            TestCase testCase,
            Map<Long, RawValue> customFieldValues,
            Integer position,
            List<Long> milestoneIds) {
        return addTestCaseToLibraryUnsecured(
                libraryId, testCase, customFieldValues, position, milestoneIds);
    }

    @Override
    public TestCase addTestCaseToLibraryUnsecured(
            long libraryId,
            TestCase testCase,
            Map<Long, RawValue> customFieldValues,
            Integer position,
            List<Long> milestoneIds) {
        addTestCaseToLibrary(libraryId, testCase, position);
        customFieldValueService.initCustomFieldValues(testCase, customFieldValues);
        createAttachmentsFromLibraryNode(testCase, testCase);
        milestoneService.bindTestCaseToMilestones(testCase.getId(), milestoneIds);
        return testCase;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#folderId, 'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'CREATE') "
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibraryNode.class)
    public void addTestCaseToFolder(@Id long folderId, TestCase testCase, Integer position) {
        TestCaseFolder folder = testCaseFolderDao.loadForNodeAddition(folderId);

        if (!folder.isContentNameAvailable(testCase.getName())) {
            throw new DuplicateNameException(testCase.getName(), testCase.getName());
        }
        if (position != null) {
            folder.addContent(testCase, position);
        } else {
            folder.addContent(testCase);
        }
        replaceInfoListReferences(testCase);
        testCaseDao.safePersist(testCase);
        createCustomFieldValuesForTestCase(testCase);
    }

    @Override
    public void addFromReqTestCaseToFolder(
            @Id long folderId, TestCase testCase, RequirementVersion version, Integer position) {
        TestCaseFolder folder = testCaseFolderDao.loadForNodeAddition(folderId);
        if (!folder.isContentNameAvailable(testCase.getName())) {

            resolveNameConflict(folder.getContentNames(), testCase, 1);
        }
        if (position != null) {
            folder.addContent(testCase, position);
        } else {
            folder.addContent(testCase);
        }
        replaceInfoListReferences(testCase);
        testCaseDao.safePersist(testCase);
        addCurrentMilestone(testCase);
        copyAttachmentsFromLibraryNode(testCase, version.getAttachmentList().getId());
        verifiedRequirementsManagerService.addVerifiedRequirementVersionsToTestCaseFromReq(
                version, testCase);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#folderId, 'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'CREATE') "
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibraryNode.class)
    public TestCase addTestCaseToFolder(
            @Id long folderId,
            TestCase testCase,
            Map<Long, RawValue> customFieldValues,
            Integer position,
            List<Long> milestoneIds) {
        return addTestCaseToFolderUnsecured(
                folderId, testCase, customFieldValues, position, milestoneIds);
    }

    @Override
    public TestCase addTestCaseToFolderUnsecured(
            @Id long folderId,
            TestCase testCase,
            Map<Long, RawValue> customFieldValues,
            Integer position,
            List<Long> milestoneIds) {
        addTestCaseToFolder(folderId, testCase, position);
        customFieldValueService.initCustomFieldValues(testCase, customFieldValues);
        createAttachmentsFromLibraryNode(testCase, testCase);
        milestoneService.bindTestCaseToMilestones(testCase.getId(), milestoneIds);
        return testCase;
    }

    @Override
    public void addReqFolderToTcFolder(
            @Id long destinationId, TestCaseFolder newFolder, Long copiedAttachmentListId) {
        TestCaseFolder container = getFolderDao().loadForNodeAddition(destinationId);
        if (container.getContentNames().contains(newFolder.getName())) {
            resolveNameConflict(container.getContentNames(), newFolder, 1);
        }

        container.addContent(newFolder);

        new NatureTypeChainFixer().fix(newFolder);

        getFolderDao().persist(newFolder);

        new CustomFieldValuesFixer().fix(newFolder);
        copyAttachmentsFromLibraryNode(newFolder, copiedAttachmentListId);
        generateCustomField(newFolder);
    }

    @Override
    public Long mkdirs(String folderpath) {

        String path = folderpath.replaceFirst("^/", "").replaceFirst("/$", "");

        StringBuilder buffer = new StringBuilder();
        String[] split = path.split("(?<!\\\\)/");
        List<String> paths = new ArrayList<>(split.length - 1);

        // build all the paths on the way.
        buffer.append("/").append(split[0]);
        for (int i = 1; i < split.length; i++) {
            buffer.append("/");
            buffer.append(split[i]);
            paths.add(buffer.toString());
        }

        // find the folder ids, if exist
        List<Long> foundIds = findNodeIdsByPath(paths);

        int nullIdx = foundIds.indexOf(null);
        TestCaseFolder foldertree;

        switch (nullIdx) {
            case -1:
                return foundIds.get(foundIds.size() - 1); // all folders do exist,
                // simply return the last element

            case 0:
                Long libraryId =
                        projectDao.findByName(split[0].replace("\\/", "/")).getTestCaseLibrary().getId();
                foldertree = mkTransFolders(1, split);
                addFolderToLibrary(libraryId, foldertree);
                break;

            default:
                Long parentFolder = foundIds.get(nullIdx - 1);
                foldertree = mkTransFolders(nullIdx + 1, split);
                addFolderToFolder(parentFolder, foldertree);
                break;
        }

        TestCaseFolder lastFolder = foldertree;
        do {
            if (lastFolder.hasContent()) {
                lastFolder = (TestCaseFolder) lastFolder.getContent().get(0);
            }
        } while (lastFolder.hasContent());

        return lastFolder.getId();
    }

    private TestCaseFolder mkTransFolders(int startIndex, String[] names) {

        TestCaseFolder baseFolder = null;
        TestCaseFolder currentParent = null;
        TestCaseFolder currentChild;

        for (int i = startIndex; i < names.length; i++) {
            currentChild = new TestCaseFolder();
            currentChild.setName(names[i].replace("\\/", "/")); // unescapes
            // escaped
            // slashes
            // '\/'
            // to
            // slashes
            // '/'
            currentChild.setDescription("");

            // if this is the first round in the loop we must initialize some
            // variables
            if (baseFolder == null) {
                baseFolder = currentChild;
            } else {
                currentParent.addContent(currentChild);
            }

            currentParent = currentChild;
        }

        return baseFolder;
    }

    @Override
    public ImportLog simulateImportExcelTestCase(File excelFile) {
        xlsImportLimitationHandler.checkMaxNumberOfTestCasesInsideXlsFile(excelFile);
        xlsImportLimitationHandler.checkMaxNumberOfTestStepsInsideXlsFile(excelFile);
        return batchImporter.simulateImport(excelFile);
    }

    @Override
    public ImportLog performImportExcelTestCase(File excelFile) {
        try {
            xlsImportLimitationHandler.incrementImportProcessCounter();

            xlsImportLimitationHandler.checkMaxNumberOfTestCasesInsideXlsFile(excelFile);
            xlsImportLimitationHandler.checkMaxNumberOfTestStepsInsideXlsFile(excelFile);
            xlsImportLimitationHandler.checkIfImportSlotIsAvailable();
            return batchImporter.performImport(excelFile);
        } finally {
            xlsImportLimitationHandler.decrementImportProcessCounter();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public File exportTestCaseAsExcel(
            List<Long> libraryIds,
            List<Long> nodeIds,
            boolean includeCalledTests,
            boolean keepRteFormat,
            MessageSource messageSource) {

        Collection<Long> allIds = findTestCaseIdsFromSelection(libraryIds, nodeIds, includeCalledTests);
        allIds = securityFilterIds(allIds, TEST_CASE_CLASS_NAME, EXPORT);

        return excelService.exportAsExcel(new ArrayList<>(allIds), keepRteFormat, messageSource);
    }

    @Override
    @Transactional(readOnly = true)
    public File exportGherkinTestCaseAsFeatureFiles(
            List<Long> libraryIds, List<Long> nodeIds, MessageSource messageSource) {
        Collection<Long> ids = findTestCaseIdsFromSelection(libraryIds, nodeIds);
        return doGherkinExport(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public File exportKeywordTestCaseAsScriptFiles(
            List<Long> libraryIds, List<Long> nodeIds, MessageSource messageSource) {
        Collection<Long> ids = findTestCaseIdsFromSelection(libraryIds, nodeIds);
        return doKeywordExport(ids, messageSource);
    }

    private File doGherkinExport(Collection<Long> ids) {
        List<ScriptedTestCase> scriptedTestCases = scriptedTestCaseDao.findAllById(ids);

        FileOutputStream fileOutputStream = null;
        try {
            File zipFile = File.createTempFile("export-feature-", ".zip");
            fileOutputStream = new FileOutputStream(zipFile);
            zipFile.deleteOnExit();

            ArchiveOutputStream archive =
                    new ArchiveStreamFactory()
                            .createArchiveOutputStream(ArchiveStreamFactory.ZIP, fileOutputStream);

            for (ScriptedTestCase scriptedTestCase : scriptedTestCases) {
                String name = "tc_" + scriptedTestCase.getId();
                ZipArchiveEntry entry = new ZipArchiveEntry(name + ".feature");
                archive.putArchiveEntry(entry);
                archive.write(scriptedTestCase.getScript().getBytes(Charset.forName("UTF-8")));
                archive.closeArchiveEntry();
            }

            archive.close();
            return zipFile;
        } catch (IOException | ArchiveException e) {
            throw new RuntimeException(e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    LOGGER.error("Unable to close FileOutputStream: ", e);
                }
            }
        }
    }

    private File doKeywordExport(Collection<Long> ids, MessageSource messageSource) {
        List<KeywordTestCase> keywordTestCases = keywordTestCaseDao.findAllById(ids);

        FileOutputStream fileOutputStream = null;
        try {
            File zipFile = File.createTempFile("export-keyword-", ".zip");
            fileOutputStream = new FileOutputStream(zipFile);
            zipFile.deleteOnExit();

            ArchiveOutputStream archive =
                    new ArchiveStreamFactory()
                            .createArchiveOutputStream(ArchiveStreamFactory.ZIP, fileOutputStream);

            for (KeywordTestCase keywordTestCase : keywordTestCases) {
                String name = "tc_" + keywordTestCase.getId();
                BddImplementationTechnology technology =
                        keywordTestCase.getProject().getBddImplementationTechnology();
                KeywordTestCaseToFileStrategy strategy =
                        KeywordTestCaseToFileStrategy.strategyFor(technology);
                String extension = strategy.getExtension();

                ZipArchiveEntry entry =
                        new ZipArchiveEntry(String.join(EXTENSION_DELIMITER, name, extension));
                archive.putArchiveEntry(entry);
                archive.write(
                        strategy
                                .getWritableFileContent(keywordTestCase, messageSource, false)
                                .getBytes(Charset.forName("UTF-8")));
                archive.closeArchiveEntry();
            }

            archive.close();
            return zipFile;
        } catch (IOException | ArchiveException e) {
            throw new RuntimeException(e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    LOGGER.error("Unable to close FileOutputStream: ", e);
                }
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public File searchExportTestCaseAsExcel(
            List<Long> nodeIds,
            boolean includeCalledTests,
            boolean keepRteFormat,
            MessageSource messageSource,
            String type,
            Boolean simplifiedColumnDisplayForTest) {

        Collection<Long> allIds =
                findTestCaseIdsFromSelection(CollectionUtils.EMPTY_COLLECTION, nodeIds, includeCalledTests);
        allIds = securityFilterIds(allIds, TEST_CASE_CLASS_NAME, EXPORT);
        File file;
        if (type.equals(SIMPLE)) {
            file =
                    excelService.searchSimpleExportAsExcel(
                            new ArrayList<>(allIds),
                            keepRteFormat,
                            messageSource,
                            simplifiedColumnDisplayForTest);
        } else {
            file =
                    excelService.searchExportAsExcel(new ArrayList<>(allIds), keepRteFormat, messageSource);
        }
        return file;
    }

    @Override
    public TestCaseStatisticsBundle getStatisticsForSelection(
            Collection<Long> libraryIds, Collection<Long> nodeIds) {

        Collection<Long> tcIds = findTestCaseIdsFromSelection(libraryIds, nodeIds);

        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        if (activeMilestone.isPresent()) {
            tcIds = filterTcIdsListsByMilestone(tcIds, activeMilestone.get());
        }

        return statisticsService.gatherTestCaseStatisticsBundle(tcIds);
    }

    @Override
    public Collection<Long> findTestCaseIdsFromSelection(
            Collection<Long> libraryIds, Collection<Long> nodeIds) {

        /*
         * first, let's check the permissions on those root nodes By
         * transitivity, if the user can read them then it will be allowed to
         * read the test case below
         */

        Collection<Long> readLibIds =
                securityFilterIds(libraryIds, TestCaseLibrary.class.getName(), "READ");
        Collection<Long> readNodeIds =
                securityFilterIds(nodeIds, TestCaseLibraryNode.class.getName(), "READ");

        // now we can collect the test cases
        Set<Long> tcIds = new HashSet<>();

        if (!readLibIds.isEmpty()) {
            tcIds.addAll(testCaseDao.findAllTestCaseIdsByLibraries(readLibIds));
        }
        if (!readNodeIds.isEmpty()) {
            tcIds.addAll(testCaseDao.findAllTestCaseIdsByNodeIds(readNodeIds));
        }

        // return
        return tcIds;
    }

    @Override
    public Collection<Long> findTestCaseIdsFromSelection(
            Collection<Long> libraryIds, Collection<Long> nodeIds, boolean includeCalledTests) {

        // first collect the test cases
        // the implementation guarantee there are no duplicates in the returned
        // collection
        Collection<Long> tcIds = findTestCaseIdsFromSelection(libraryIds, nodeIds);

        // collect if needed the called test cases
        if (includeCalledTests) {
            Set<Long> called = calltreeService.getTestCaseCallTree(tcIds);
            called = securityFilterIds(called, TEST_CASE_CLASS_NAME, "READ");
            tcIds.addAll(called);
        }

        // return
        return tcIds;
    }

    @Override
    public int countSiblingsOfNode(long nodeId) {
        return testCaseLibraryNodeDao.countSiblingsOfNode(nodeId);
    }

    // ******************** more private code *******************

    private void createCustomFieldValuesForTestCase(TestCase testCase) {
        createCustomFieldValues(testCase);

        // also create the custom field values for the steps if any
        if (!testCase.getSteps().isEmpty()) {
            createCustomFieldValues(testCase.getActionSteps());
        }
    }

    private void replaceInfoListReferences(TestCase testCase) {
        NatureTypeChainFixer.fix(testCase);
    }

    private class CustomFieldValuesFixer implements TestCaseLibraryNodeVisitor {

        private void fix(TestCaseFolder folder) {
            for (TestCaseLibraryNode node : folder.getContent()) {
                node.accept(this);
            }
        }

        @Override
        public void visit(TestCase visited) {
            createCustomFieldValuesForTestCase(visited);
        }

        @Override
        public void visit(TestCaseFolder visited) {
            fix(visited);
        }
    }

    @Override
    public List<Long> findAllTestCasesLibraryNodeForMilestone(Milestone activeMilestone) {
        if (activeMilestone != null) {
            List<Long> milestoneIds = new ArrayList<>();
            milestoneIds.add(activeMilestone.getId());
            return testCaseDao.findAllTestCasesLibraryNodeForMilestone(milestoneIds);
        } else {
            return new ArrayList<>();
        }
    }

    @SuppressWarnings("unchecked")
    private Collection<Long> filterTcIdsListsByMilestone(
            Collection<Long> tcIds, Milestone activeMilestone) {

        List<Long> tcInMilestone = findAllTestCasesLibraryNodeForMilestone(activeMilestone);
        return CollectionUtils.retainAll(tcIds, tcInMilestone);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = TestCaseLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = TestCaseLibraryNode.class,
                        paramName = SOURCE_NODES_IDS,
                        coercer = TCLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = TestCaseLibrary.class,
                        paramName = SOURCE_NODES_IDS,
                        coercer = TestCaseLibraryIdsCoercerForArray.class)
            })
    public void copyNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(SOURCE_NODES_IDS) Long[] sourceNodesIds,
            ClipboardPayload clipboardPayload) {
        super.copyNodesToFolder(destinationId, sourceNodesIds, clipboardPayload);
    }

    @Override
    @PreventConcurrent(entityType = TestCaseLibraryNode.class)
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.testcase.TestCaseFolder', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    public void copyReqToTestCasesToFolder(
            @Id long destinationId, Long[] sourceNodesIds, ReqToTestCaseConfiguration configuration) {
        if (sourceNodesIds.length == 0) {
            return;
        }
        try {
            PasteStrategy<TestCaseFolder, TestCaseLibraryNode> pasteStrategy = getPasteToFolderStrategy();
            makeMoverStrategy(pasteStrategy);

            pasteStrategy.pasteReqToTestCasesNodes(
                    destinationId, Arrays.asList(sourceNodesIds), configuration);
        } catch (NullArgumentException | DuplicateNameException dne) {
            throw new NameAlreadyExistsAtDestinationException(dne);
        }
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = TestCaseLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = TestCaseLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = TCLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = TestCaseLibrary.class,
                        paramName = TARGET_ID,
                        coercer = TestCaseLibraryIdsCoercerForArray.class)
            })
    public void copyNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload) {
        super.copyNodesToLibrary(destinationId, targetId, clipboardPayload);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#destinationId, 'org.squashtest.tm.domain.testcase.TestCaseLibrary', 'CREATE')"
                    + OR_HAS_ROLE_ADMIN)
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public void copyReqToTestCasesToLibrary(
            @Id long destinationId, Long[] targetId, ReqToTestCaseConfiguration configuration) {
        if (targetId.length == 0) {
            return;
        }
        try {
            PasteStrategy<TestCaseLibrary, TestCaseLibraryNode> pasteStrategy =
                    getPasteToLibraryStrategy();
            makeMoverStrategy(pasteStrategy);

            pasteStrategy.pasteReqToTestCasesNodes(destinationId, Arrays.asList(targetId), configuration);
        } catch (NullArgumentException | DuplicateNameException dne) {
            throw new NameAlreadyExistsAtDestinationException(dne);
        }
    }

    @Override
    public void copyReqToTestCasesToTestCases(
            long destinationId, Long[] targetId, ReqToTestCaseConfiguration configuration) {
        TestCaseFolder folder = findParentIfExists(destinationId);
        if (null == folder) {
            Long tclId =
                    dsl.select(TEST_CASE_LIBRARY.TCL_ID)
                            .from(org.squashtest.tm.jooq.domain.tables.TestCaseLibrary.TEST_CASE_LIBRARY)
                            .join(PROJECT)
                            .using(TEST_CASE_LIBRARY.TCL_ID)
                            .join(TEST_CASE_LIBRARY_NODE)
                            .using(TEST_CASE_LIBRARY_NODE.PROJECT_ID)
                            .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.equal(destinationId))
                            .fetchOne(TEST_CASE_LIBRARY.TCL_ID);
            copyReqToTestCasesToLibrary(tclId, targetId, configuration);
        } else {
            copyReqToTestCasesToFolder(folder.getId(), targetId, configuration);
        }
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = TestCaseLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = TestCaseLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = TCLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = TestCaseLibrary.class,
                        paramName = TARGET_ID,
                        coercer = TestCaseLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToFolder(destinationId, targetId, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = TestCaseLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = TestCaseLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = TCLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = TestCaseLibrary.class,
                        paramName = TARGET_ID,
                        coercer = TestCaseLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToLibrary(destinationId, targetId, clipboardPayload);
    }

    @Override
    public void moveNodesToLibrary(long destinationId, Long[] targetId, int position) {
        super.moveNodesToLibrary(
                destinationId,
                targetId,
                position,
                ClipboardPayload.withWhiteListIgnored(Arrays.asList(targetId)));
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = TestCaseLibraryNode.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = TestCaseLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = TCLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = TestCaseLibrary.class,
                        paramName = TARGET_ID,
                        coercer = TestCaseLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToFolder(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            int position,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToFolder(destinationId, targetId, position, clipboardPayload);
    }

    @Override
    public void moveNodesToLibrary(long destinationId, Long[] targetId) {
        super.moveNodesToLibrary(
                destinationId, targetId, ClipboardPayload.withWhiteListIgnored(Arrays.asList(targetId)));
    }

    @Override
    @PreventConcurrents(
            simplesLocks = {
                @PreventConcurrent(entityType = TestCaseLibrary.class, paramName = DESTINATION_ID)
            },
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = TestCaseLibraryNode.class,
                        paramName = TARGET_ID,
                        coercer = TCLNAndParentIdsCoercerForArray.class),
                @BatchPreventConcurrent(
                        entityType = TestCaseLibrary.class,
                        paramName = TARGET_ID,
                        coercer = TestCaseLibraryIdsCoercerForArray.class)
            })
    public void moveNodesToLibrary(
            @Id(DESTINATION_ID) long destinationId,
            @Ids(TARGET_ID) Long[] targetId,
            int position,
            ClipboardPayload clipboardPayload) {
        super.moveNodesToLibrary(destinationId, targetId, position, clipboardPayload);
    }

    @Override
    @PreventConcurrents(
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = TestCaseLibraryNode.class,
                        paramName = TARGET_IDS,
                        coercer = TCLNAndParentIdsCoercerForList.class),
                @BatchPreventConcurrent(
                        entityType = TestCaseLibrary.class,
                        paramName = TARGET_IDS,
                        coercer = TestCaseLibraryIdsCoercerForList.class)
            })
    @CheckBlockingMilestones(entityType = TestCase.class)
    public OperationReport deleteNodes(@Ids(TARGET_IDS) List<Long> targetIds) {
        return super.deleteNodes(targetIds);
    }

    private void generateCustomField(TestCaseFolder newFolder) {
        List<CustomFieldBinding> projectsBindings =
                customFieldBindingFinderService.findCustomFieldsForProjectAndEntity(
                        newFolder.getProject().getId(), BindableEntity.TESTCASE_FOLDER);
        for (CustomFieldBinding binding : projectsBindings) {
            customFieldValueService.cascadeCustomFieldValuesCreationNotCreatedFolderYet(
                    binding, newFolder);
        }
    }

    @Override
    public TreeMap<String, Long> buildTestCasePathsTree(String projectName) {
        return getLibraryNodeDao().buildNodePathsTree(projectName);
    }

    @Override
    public List<String> findContentNamesByLibraryId(Long libraryId) {
        return testCaseLibraryDao.findContentNamesByLibraryId(libraryId);
    }

    @Override
    public Map<Long, List<String>> findContentNamesByFolderIds(Collection<Long> folderIds) {
        return testCaseFolderDao.findContentNamesByFolderIds(folderIds);
    }

    @Override
    @PreventConcurrent(entityType = TestCaseLibrary.class)
    public void addTestCasesToLibrary(
            @Id Long libraryId, List<TestCase> testCases, List<Long> milestoneIds) {
        TestCaseLibrary library = testCaseLibraryDao.loadForNodeAddition(libraryId);

        for (var testCase : testCases) {
            library.addContent(testCase);
            replaceInfoListReferences(testCase);
            testCaseDao.safePersist(testCase);
        }

        initCustomFieldsAndMilestones(testCases, milestoneIds, library.getProject().getProject());

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    @PreventConcurrent(entityType = TestCaseFolder.class)
    public void addTestCasesToFolder(
            @Id Long folderId, List<TestCase> testCases, List<Long> milestoneIds) {
        TestCaseFolder folder = testCaseFolderDao.loadForNodeAddition(folderId);

        for (var testCase : testCases) {
            folder.addContent(testCase);
            replaceInfoListReferences(testCase);
            testCaseDao.safePersist(testCase);
        }

        initCustomFieldsAndMilestones(testCases, milestoneIds, folder.getProject());

        entityManager.flush();
        entityManager.clear();
    }

    private void initCustomFieldsAndMilestones(
            List<TestCase> testCases, List<Long> milestoneIds, Project project) {
        Map<TestCase, List<Long>> testCaseMilestones =
                testCases.stream()
                        .collect(Collectors.toMap(testCase -> testCase, testCase -> milestoneIds));

        milestoneService.bindHoldersToMilestones(testCaseMilestones);

        customFieldValueService.createAllCustomFieldValues(testCases, project);
    }
}
