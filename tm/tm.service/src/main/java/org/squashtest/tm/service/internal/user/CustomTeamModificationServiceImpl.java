/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.user;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.IdentifiedUtil;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.profile.CannotAddUserToTeamWithCustomProfileException;
import org.squashtest.tm.service.internal.repository.ProfileDao;
import org.squashtest.tm.service.internal.repository.TeamDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;
import org.squashtest.tm.service.user.CustomTeamModificationService;

@Service("CustomTeamModificationService")
@PreAuthorize(HAS_ROLE_ADMIN)
@Transactional
public class CustomTeamModificationServiceImpl implements CustomTeamModificationService {

    @Inject private TeamDao teamDao;
    @Inject private UserDao userDao;
    @Inject private ObjectAclService aclService;
    @Inject private ProfileDao profileDao;
    @Inject private UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService;

    /**
     * @see CustomTeamModificationService#persist(Team)
     */
    @Override
    public void persist(Team team) {
        if (teamDao.findAllByName(team.getName()).isEmpty()) {
            teamDao.save(team);
        } else {
            throw new NameAlreadyInUseException("Team", team.getName());
        }
    }

    /**
     * @see CustomTeamModificationService#deleteTeam(long)
     * @param teamId the id of the team
     */
    @Override
    public void deleteTeam(long teamId) {
        Team team = teamDao.getReferenceById(teamId);
        List<Long> memberIds = IdentifiedUtil.extractIds(team.getMembers());
        removeMembers(team, memberIds);
        aclService.removeAllResponsibilities(teamId);
        teamDao.delete(team);
    }

    /**
     * @see CustomTeamModificationService#deleteTeam(long)
     */
    @Override
    public void deleteTeam(List<Long> teamId) {
        for (Long id : teamId) {
            deleteTeam(id);
        }
    }

    @Override
    public void changeName(long teamId, String name) {
        String trimName = name.trim();
        if (!teamDao.findAllByName(trimName).isEmpty()) {
            throw new NameAlreadyInUseException("Team", trimName);
        }
        Team team = teamDao.getReferenceById(teamId);
        team.setName(trimName);
    }

    @Override
    public void addMember(long teamId, String login) {
        addMembers(teamId, Arrays.asList(login));
    }

    @Override
    public void addMembers(long teamId, List<String> logins) {
        verifyTeamDoesNothaveCustomProfiles(teamId);

        List<User> users = userDao.findUsersByLoginList(logins);
        Team team = teamDao.getReferenceById(teamId);
        team.addMembers(users);
        aclService.refreshAcls();
    }

    private void verifyTeamDoesNothaveCustomProfiles(long teamId) {
        if (ultimateLicenseAvailabilityService.isAvailable()) {
            return;
        }
        Set<String> profileNames = profileDao.findProfileNamesByPartyId(teamId);
        if (profileNames.stream().anyMatch(profileName -> !AclGroup.isSystem(profileName))) {
            throw new CannotAddUserToTeamWithCustomProfileException();
        }
    }

    @Override
    public List<User> findAllNonMemberUsers(long teamId) {
        return userDao.findAllNonTeamMembers(teamId);
    }

    @Override
    public void removeMember(long teamId, long memberId) {
        User user = userDao.getReferenceById(memberId);
        Team team = teamDao.getReferenceById(teamId);
        team.removeMember(user);
        aclService.refreshAcls();
    }

    @Override
    public void removeMembers(long teamId, List<Long> memberIds) {
        Team team = teamDao.getReferenceById(teamId);
        removeMembers(team, memberIds);
    }

    private void removeMembers(Team team, List<Long> memberIds) {
        List<User> users = userDao.findAllById(memberIds);
        team.removeMember(users);
        aclService.refreshAcls();
    }

    @Override
    public void removeMemberFromAllTeams(long memberId) {
        User user = userDao.getReferenceById(memberId);
        List<Long> teamIds = new ArrayList<>();
        Set<Team> teams = user.getTeams();
        for (Team team : teams) {
            teamIds.add(team.getId());
            team.removeMember(user);
        }
        user.removeTeams(teamIds);

        aclService.refreshAcls();
    }
}
