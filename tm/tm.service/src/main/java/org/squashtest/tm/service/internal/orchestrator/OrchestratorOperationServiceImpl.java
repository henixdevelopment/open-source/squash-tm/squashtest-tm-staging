/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.orchestrator;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.internal.dto.WorkflowDto;
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao;
import org.squashtest.tm.service.internal.testautomation.TestAutomationConnectorRegistry;
import org.squashtest.tm.service.orchestrator.OrchestratorOperationService;
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;

@Service
public class OrchestratorOperationServiceImpl implements OrchestratorOperationService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(OrchestratorOperationServiceImpl.class);

    private final TestAutomationServerDao testAutomationServerDao;

    private final TestAutomationConnectorRegistry testAutomationConnectorRegistry;

    private final StoredCredentialsManager storedCredentialsManager;

    public OrchestratorOperationServiceImpl(
            TestAutomationServerDao testAutomationServerDao,
            TestAutomationConnectorRegistry testAutomationConnectorRegistry,
            StoredCredentialsManager storedCredentialsManager) {
        this.testAutomationServerDao = testAutomationServerDao;
        this.testAutomationConnectorRegistry = testAutomationConnectorRegistry;
        this.storedCredentialsManager = storedCredentialsManager;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfVersions(
            Long testAutomationServerId) {
        return performOrchestratorOperation(
                testAutomationServerId, TestAutomationConnector::getOrchestratorConfVersions);
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public OrchestratorResponse<List<WorkflowDto>> getProjectWorkflows(
            Long serverId, Long projectId) {
        TokenAuthCredentials credentials = findProjectToken(serverId, projectId).orElse(null);
        return performOrchestratorOperation(
                serverId,
                (connector, server) -> connector.getProjectWorkflows(server, projectId, credentials));
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public OrchestratorResponse<Void> killWorkflow(Long serverId, Long projectId, String workflowId) {
        TokenAuthCredentials credentials = findProjectToken(serverId, projectId).orElse(null);
        return performOrchestratorOperation(
                serverId,
                (connector, server) -> connector.killWorkflow(server, projectId, credentials, workflowId));
    }

    @Override
    public <T> OrchestratorResponse<T> performOrchestratorOperation(
            Long testAutomationServerId,
            BiFunction<TestAutomationConnector, TestAutomationServer, OrchestratorResponse<T>>
                    operation) {
        Optional<TestAutomationServer> server =
                testAutomationServerDao.findById(testAutomationServerId);

        if (server.isEmpty()) {
            throw new IllegalArgumentException("Invalid server id");
        }

        TestAutomationConnector connector =
                testAutomationConnectorRegistry.getConnectorForKind(server.get().getKind());

        try {
            return operation.apply(connector, server.get());
        } catch (Exception ex) {
            LOGGER.error("An error occurred during the operation.", ex);
            return new OrchestratorResponse<>(false);
        }
    }

    private Optional<TokenAuthCredentials> findProjectToken(
            Long testAutomationServerId, Long projectId) {
        return Optional.ofNullable(
                        storedCredentialsManager.findProjectCredentials(testAutomationServerId, projectId))
                .filter(TokenAuthCredentials.class::isInstance)
                .map(TokenAuthCredentials.class::cast);
    }
}
