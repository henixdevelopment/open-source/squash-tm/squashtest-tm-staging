/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.utils;

import ch.x28.inscriptis.Inscriptis;
import ch.x28.inscriptis.ParserConfig;
import net.htmlparser.jericho.Renderer;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.Source;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Safelist;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.core.foundation.sanitizehtml.HTMLSanitizeUtils;

public final class HTMLCleanupUtils {

    private HTMLCleanupUtils() {}

    public static String htmlToTrimmedText(String html) {
        return htmlToText(html).trim();
    }

    public static String htmlToText(String html) {
        String fixedHtml = html != null ? html : "";

        String replacedHtml = fixedHtml.replaceFirst("\n", "");

        Source htmlSource = new Source(replacedHtml);
        Segment htmlSegment = new Segment(htmlSource, 0, replacedHtml.length());
        Renderer htmlRend = new Renderer(htmlSegment);
        return htmlRend.toString();
    }

    /**
     * Transforms html to text while preserving layout, for example lists are rendered with bullets,
     * according to the level of the list or sublist.
     *
     * @param html
     * @param shouldDisplayLinks Explicits the embedded link by displaying the label and the href.
     */
    public static String htmlToLayoutAwareText(String html, boolean shouldDisplayLinks) {
        org.w3c.dom.Document document = W3CDom.convert(Jsoup.parse(html));
        final ParserConfig parserConfig = new ParserConfig();
        parserConfig.setDisplayLinks(shouldDisplayLinks);
        Inscriptis inscriptis = new Inscriptis(document, parserConfig);

        return inscriptis.getText();
    }

    /* note : Unescape is idempotent when applied on unescaped data. We use that trick to prevent double html encoding*/
    public static String forceHtmlEscape(String html) {
        String fixedHtml = html != null ? html : "";
        String unescaped = HtmlUtils.htmlUnescape(fixedHtml);
        return HtmlUtils.htmlEscape(unescaped);
    }

    /**
     * If the argument is not null, returns the HTML-escaped version, else returns the default value
     *
     * @param toEscape
     * @return
     */
    public static String escapeOrDefault(String toEscape, String defaultString) {
        return (toEscape != null) ? HtmlUtils.htmlEscape(toEscape) : defaultString;
    }

    public static String stripJavascript(String json) {
        if (StringUtils.isNotBlank(json)) {
            Document.OutputSettings outputSettings = new Document.OutputSettings();
            outputSettings.prettyPrint(false);
            outputSettings.outline(false);
            String cleaned = Jsoup.clean(json, "", Safelist.relaxed(), outputSettings);
            // We need to unescape here as JSoup escape json characters and make subsequent use of JSON
            // crash
            // For html content we should escape before persistence
            // There is a little performance hit but it's safer to use JSoup than a custom solution.
            return HtmlUtils.htmlUnescape(cleaned);
        }
        return StringUtils.EMPTY;
    }

    public static String getCleanedBriefText(String text, int maxLength) {
        text = htmlToTrimmedText(cleanHtml(text));
        if (text.length() > maxLength) {
            text = text.substring(0, maxLength - 3) + "...";
        }
        return text;
    }

    public static String cleanHtml(String unsecureHtml) {
        return HTMLSanitizeUtils.cleanHtml(unsecureHtml);
    }

    public static String cleanAndUnescapeHTML(String unsecureHtml) {
        return HtmlUtils.htmlUnescape(cleanHtml(unsecureHtml));
    }

    public static String removeHtml(String html) {
        if (StringUtils.isBlank(html)) {
            return "";
        }
        return html.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", "");
    }
}
