/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport;

import static org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn.TC_AUTOMATABLE;
import static org.squashtest.tm.service.internal.utils.HTMLCleanupUtils.removeHtml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.core.foundation.lang.DateUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.feature.FeatureManager.Feature;
import org.squashtest.tm.service.internal.batchexport.models.CoverageModel;
import org.squashtest.tm.service.internal.batchexport.models.DatasetModel;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;
import org.squashtest.tm.service.internal.batchexport.models.ParameterModel;
import org.squashtest.tm.service.internal.batchexport.models.TestCaseModel;
import org.squashtest.tm.service.internal.batchexport.models.TestStepModel;
import org.squashtest.tm.service.internal.batchimport.column.TemplateColumn;
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.DatasetSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.ParameterSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.StepSheetColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn;
import org.squashtest.tm.service.internal.dto.NumericCufHelper;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;
import org.squashtest.tm.service.project.ProjectFinder;

/**
 * @author bsiri
 */
@Component
@Scope("prototype")
class ExcelExporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelExporter.class);

    private static final String DATA_EXCEED_MAX_CELL_SIZE_MESSAGE =
            "some data exceed the maximum size of an excel cell";

    private static final String DATASET_SHEET = TemplateWorksheet.DATASETS_SHEET.sheetName;
    private static final String PARAMETER_SHEET = TemplateWorksheet.PARAMETERS_SHEET.sheetName;
    private static final String TEST_STEP_SHEET = TemplateWorksheet.STEPS_SHEET.sheetName;
    private static final String COVERAGE_SHEET = TemplateWorksheet.COVERAGE_SHEET.sheetName;

    private static final List<CoverageSheetColumn> COVERAGE_COLUMNS =
            Arrays.asList(
                    CoverageSheetColumn.REQ_PATH,
                    CoverageSheetColumn.REQ_VERSION_NUM,
                    CoverageSheetColumn.TC_PATH);

    private static final List<DatasetSheetColumn> DS_COLUMNS =
            Arrays.asList(
                    DatasetSheetColumn.TC_OWNER_PATH,
                    DatasetSheetColumn.TC_OWNER_ID,
                    DatasetSheetColumn.TC_DATASET_ID,
                    DatasetSheetColumn.TC_DATASET_NAME,
                    DatasetSheetColumn.TC_PARAM_OWNER_PATH,
                    DatasetSheetColumn.TC_PARAM_OWNER_ID,
                    DatasetSheetColumn.TC_DATASET_PARAM_NAME,
                    DatasetSheetColumn.TC_DATASET_PARAM_VALUE);

    private static final List<ParameterSheetColumn> PRM_COLUMNS =
            Arrays.asList(
                    ParameterSheetColumn.TC_OWNER_PATH,
                    ParameterSheetColumn.TC_OWNER_ID,
                    ParameterSheetColumn.TC_PARAM_ID,
                    ParameterSheetColumn.TC_PARAM_NAME,
                    ParameterSheetColumn.TC_PARAM_DESCRIPTION);

    private static final List<StepSheetColumn> ST_COLUMNS =
            Arrays.asList(
                    StepSheetColumn.TC_OWNER_PATH,
                    StepSheetColumn.TC_OWNER_ID,
                    StepSheetColumn.TC_STEP_ID,
                    StepSheetColumn.TC_STEP_NUM,
                    StepSheetColumn.TC_STEP_IS_CALL_STEP,
                    StepSheetColumn.TC_STEP_CALL_DATASET,
                    StepSheetColumn.TC_STEP_ACTION,
                    StepSheetColumn.TC_STEP_EXPECTED_RESULT,
                    StepSheetColumn.TC_STEP_NB_REQ,
                    StepSheetColumn.TC_STEP_NB_ATTACHMENT);

    private static final TestCaseSheetColumn[] BASIC_TC_COLUMNS = {
        TestCaseSheetColumn.PROJECT_ID,
        TestCaseSheetColumn.PROJECT_NAME,
        TestCaseSheetColumn.TC_PATH,
        TestCaseSheetColumn.TC_NUM,
        TestCaseSheetColumn.TC_ID,
        TestCaseSheetColumn.TC_UUID,
        TestCaseSheetColumn.TC_REFERENCE,
        TestCaseSheetColumn.TC_NAME,
        TestCaseSheetColumn.TC_WEIGHT_AUTO,
        TestCaseSheetColumn.TC_WEIGHT,
        TestCaseSheetColumn.TC_NATURE,
        TestCaseSheetColumn.TC_TYPE,
        TestCaseSheetColumn.TC_STATUS,
        TestCaseSheetColumn.TC_DESCRIPTION,
        TestCaseSheetColumn.TC_PRE_REQUISITE,
        TestCaseSheetColumn.TC_NB_REQ,
        TestCaseSheetColumn.TC_NB_CALLED_BY,
        TestCaseSheetColumn.TC_NB_ATTACHMENT,
        TestCaseSheetColumn.TC_CREATED_ON,
        TestCaseSheetColumn.TC_CREATED_BY,
        TestCaseSheetColumn.TC_LAST_MODIFIED_ON,
        TestCaseSheetColumn.TC_LAST_MODIFIED_BY,
        TestCaseSheetColumn.DRAFTED_BY_AI,
        TestCaseSheetColumn.TC_KIND,
        TestCaseSheetColumn.TC_SCRIPT,
    };

    private static final List<TestCaseSheetColumn> TC_COLUMNS_MILESTONES =
            new ArrayList<>(
                    Arrays.asList(ArrayUtils.insert(8, BASIC_TC_COLUMNS, TestCaseSheetColumn.TC_MILESTONE)));

    private static final List<TestCaseSheetColumn> TC_COLUMNS = Arrays.asList(BASIC_TC_COLUMNS);

    protected static final String TC_SHEET = TemplateWorksheet.TEST_CASES_SHEET.sheetName;

    // that map will remember which column index is
    private final Map<String, Integer> cufColumnsByCode = new HashMap<>();

    protected Workbook workbook;

    protected boolean isMilestoneModeEnabled;

    private MessageSource messageSource;

    @Inject private ProjectFinder projectFinder;

    @Inject private ProjectDisplayDao projectDisplayDao;

    private String errorCellTooLargeMessage;

    @Inject
    public ExcelExporter(FeatureManager featureManager, MessageSource messageSource) {
        super();
        isMilestoneModeEnabled = featureManager.isEnabled(Feature.MILESTONE);
    }

    @PostConstruct // So these methods are not called directly by constructor
    public void init() {
        createWorkbook();
        createHeaders();
    }

    void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
        errorCellTooLargeMessage =
                this.messageSource.getMessage(
                        "test-case.export.errors.celltoolarge", null, LocaleContextHolder.getLocale());
    }

    public void appendToWorkbook(ExportModel model, boolean keepRteFormat) {
        if (!keepRteFormat) {
            removeRteFormat(model);
        }
        appendTestCases(model, keepRteFormat);
        appendTestSteps(model, keepRteFormat);
        appendParameters(model, keepRteFormat);
        appendDatasets(model, keepRteFormat);
        appendCoverage(model);
    }

    private void removeRteFormat(ExportModel model) {
        removeRteFormatFromParameters(model.getParameters());
        removeRteFormatFromTestCases(model.getTestCases());
        removeRteFormatFromTestSteps(model.getTestSteps());
    }

    private void removeRteFormatFromTestCases(List<TestCaseModel> testCaseModels) {
        for (TestCaseModel testCaseModel : testCaseModels) {
            testCaseModel.setDescription(removeHtml(testCaseModel.getDescription()));
            testCaseModel.setPrerequisite(removeHtml(testCaseModel.getPrerequisite()));
            for (ExportModel.CustomField cuf : testCaseModel.getCufs()) {
                cuf.setValue(removeHtml(cuf.getValue()));
            }
        }
    }

    private void removeRteFormatFromParameters(List<ParameterModel> parameterModels) {
        for (ParameterModel parameterModel : parameterModels) {
            parameterModel.setDescription(removeHtml(parameterModel.getDescription()));
        }
    }

    private void removeRteFormatFromTestSteps(List<TestStepModel> testStepModels) {
        for (TestStepModel testStepModel : testStepModels) {
            testStepModel.setAction(removeHtml(testStepModel.getAction()));
            testStepModel.setResult(removeHtml(testStepModel.getResult()));
        }
    }

    public File print() {
        try {
            File temp = File.createTempFile("tc_export_", "xls");
            temp.deleteOnExit();

            FileOutputStream fos = new FileOutputStream(temp);
            workbook.write(fos);
            fos.close();

            return temp;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void appendTestCases(ExportModel model, boolean keepRteFormat) {
        List<TestCaseModel> testCaseModels = model.getTestCases();
        Sheet testCaseSheet = workbook.getSheet(TC_SHEET);
        Row row;
        int rowIndex = testCaseSheet.getLastRowNum() + 1;

        boolean shouldAddAutomationWorkflowColumn =
                projectFinder.countProjectsAllowAutomationWorkflow() > 0;
        List<Long> projectIdsInExport =
                testCaseModels.stream().map(TestCaseModel::getProjectId).distinct().toList();
        Map<Long, Boolean> allowAutomationWorkflowByProjectId =
                projectDisplayDao.allowAutomationWorkflowByProjectId(projectIdsInExport);

        for (TestCaseModel testCaseModel : testCaseModels) {
            row = testCaseSheet.createRow(rowIndex);
            int cellIndex = 0;

            try {
                row.createCell(cellIndex++).setCellValue(testCaseModel.getProjectId());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getProjectName());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getPath());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getOrder());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getId());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getUuid());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getReference());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getName());
                if (isMilestoneModeEnabled) {
                    row.createCell(cellIndex++).setCellValue(testCaseModel.getMilestone());
                }
                row.createCell(cellIndex++).setCellValue(testCaseModel.getWeightAuto());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getWeight().toString());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getNature().getCode());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getType().getCode());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getStatus().toString());
                row.createCell(cellIndex++)
                        .setCellValue(formatRichTextField(testCaseModel.getDescription(), keepRteFormat));
                row.createCell(cellIndex++)
                        .setCellValue(formatRichTextField(testCaseModel.getPrerequisite(), keepRteFormat));
                row.createCell(cellIndex++).setCellValue(testCaseModel.getNbReq());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getNbCaller());
                row.createCell(cellIndex++).setCellValue(testCaseModel.getNbAttachments());
                row.createCell(cellIndex++).setCellValue(format(testCaseModel.getCreatedOn()));
                row.createCell(cellIndex++).setCellValue(testCaseModel.getCreatedBy());
                row.createCell(cellIndex++).setCellValue(format(testCaseModel.getLastModifiedOn()));
                row.createCell(cellIndex++).setCellValue(testCaseModel.getLastModifiedBy());
                row.createCell(cellIndex++)
                        .setCellValue(String.valueOf(testCaseModel.getDraftedByAi() ? 1 : 0));

                cellIndex = appendScriptedTestCaseExtender(row, cellIndex, testCaseModel);
                appendCustomFields(row, "TC_CUF_", testCaseModel.getCufs(), keepRteFormat);
                doOptionalAppendTestCases(
                        shouldAddAutomationWorkflowColumn,
                        allowAutomationWorkflowByProjectId,
                        row,
                        cellIndex,
                        testCaseModel);

            } catch (IllegalArgumentException e) {
                LOGGER.warn(
                        "cannot export content for test case '{}': {}",
                        testCaseModel.getId(),
                        DATA_EXCEED_MAX_CELL_SIZE_MESSAGE,
                        e);

                testCaseSheet.removeRow(row);
                row = testCaseSheet.createRow(rowIndex);
                row.createCell(0).setCellValue(errorCellTooLargeMessage);
            }

            rowIndex++;
        }
    }

    private int appendScriptedTestCaseExtender(Row row, int cellIndex, TestCaseModel testCaseModel) {
        TestCaseKind testCaseKind = testCaseModel.getTestCaseKind();
        row.createCell(cellIndex++).setCellValue(testCaseKind.name());

        if (testCaseKind.isScripted()) {
            row.createCell(cellIndex++).setCellValue(testCaseModel.getTcScript());
        } else {
            row.createCell(cellIndex++).setCellValue("");
        }
        return cellIndex;
    }

    protected int doOptionalAppendTestCases(
            boolean shouldAddAutomationWorkflowColumn,
            Map<Long, Boolean> allowAutomationWorkflowByProjectId,
            Row row,
            int cellIndex,
            TestCaseModel testCaseModel) {
        if (shouldAddAutomationWorkflowColumn) {
            boolean allowAutomationWorkflow =
                    allowAutomationWorkflowByProjectId.get(testCaseModel.getProjectId());
            return appendAutomationWorkflow(allowAutomationWorkflow, row, cellIndex, testCaseModel);
        } else {
            return cellIndex;
        }
    }

    protected int appendAutomationWorkflow(
            boolean allowAutomationWorkflow, Row row, int cellIndex, TestCaseModel testCaseModel) {
        if (allowAutomationWorkflow) {
            row.createCell(cellIndex++).setCellValue(testCaseModel.getAutomatable().name());
        } else {
            row.createCell(cellIndex++).setCellValue("-");
        }
        return cellIndex;
    }

    private void appendTestSteps(ExportModel model, boolean keepRteFormat) {
        List<TestStepModel> testStepModels = model.getTestSteps();
        Sheet testStepSheet = workbook.getSheet(TEST_STEP_SHEET);
        Row row;
        int rowIndex = testStepSheet.getLastRowNum() + 1;

        for (TestStepModel testStepModel : testStepModels) {
            row = testStepSheet.createRow(rowIndex);
            int cellIndex = 0;

            try {
                row.createCell(cellIndex++).setCellValue(testStepModel.getTcOwnerPath());
                row.createCell(cellIndex++).setCellValue(testStepModel.getTcOwnerId());
                row.createCell(cellIndex++).setCellValue(testStepModel.getId());
                row.createCell(cellIndex++).setCellValue(testStepModel.getOrder());
                row.createCell(cellIndex++).setCellValue(testStepModel.getIsCallStep());
                row.createCell(cellIndex++).setCellValue(testStepModel.getDsName());
                row.createCell(cellIndex++)
                        .setCellValue(formatRichTextField(testStepModel.getAction(), keepRteFormat));
                row.createCell(cellIndex++)
                        .setCellValue(formatRichTextField(testStepModel.getResult(), keepRteFormat));
                row.createCell(cellIndex++).setCellValue(testStepModel.getNbReq());
                row.createCell(cellIndex).setCellValue(testStepModel.getNbAttach());

                appendCustomFields(row, "TC_STEP_CUF_", testStepModel.getCufs(), keepRteFormat);

            } catch (IllegalArgumentException e) {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(
                            "cannot export content for test step '{}': {}",
                            testStepModel.getId(),
                            DATA_EXCEED_MAX_CELL_SIZE_MESSAGE,
                            e);
                }

                testStepSheet.removeRow(row);
                row = testStepSheet.createRow(rowIndex);
                row.createCell(0).setCellValue(errorCellTooLargeMessage);
            }

            rowIndex++;
        }
    }

    private void appendParameters(ExportModel model, boolean keepRteFormat) {
        List<ParameterModel> parameterModels = model.getParameters();
        Sheet parameterSheet = workbook.getSheet(PARAMETER_SHEET);
        Row row;
        int rowIndex = parameterSheet.getLastRowNum() + 1;

        for (ParameterModel parameterModel : parameterModels) {
            row = parameterSheet.createRow(rowIndex);
            int cellIndex = 0;

            try {
                row.createCell(cellIndex++).setCellValue(parameterModel.getTcOwnerPath());
                row.createCell(cellIndex++).setCellValue(parameterModel.getTcOwnerId());
                row.createCell(cellIndex++).setCellValue(parameterModel.getId());
                row.createCell(cellIndex++).setCellValue(parameterModel.getName());
                row.createCell(cellIndex)
                        .setCellValue(formatRichTextField(parameterModel.getDescription(), keepRteFormat));
            } catch (IllegalArgumentException e) {

                LOGGER.warn(
                        "cannot export content for parameter '{}': {}",
                        parameterModel.getId(),
                        DATA_EXCEED_MAX_CELL_SIZE_MESSAGE,
                        e);
                parameterSheet.removeRow(row);
                row = parameterSheet.createRow(rowIndex);

                row.createCell(0).setCellValue(errorCellTooLargeMessage);
            }

            rowIndex++;
        }
    }

    private void appendDatasets(ExportModel model, boolean keepRteFormat) {
        List<DatasetModel> datasetModels = model.getDatasets();
        Sheet datasetSheet = workbook.getSheet(DATASET_SHEET);
        Row row;
        int rowIndex = datasetSheet.getLastRowNum() + 1;

        for (DatasetModel datasetModel : datasetModels) {
            row = datasetSheet.createRow(rowIndex);
            int cellIndex = 0;

            try {
                row.createCell(cellIndex++).setCellValue(datasetModel.getTcOwnerPath());
                row.createCell(cellIndex++).setCellValue(datasetModel.getOwnerId());
                row.createCell(cellIndex++).setCellValue(datasetModel.getId());
                row.createCell(cellIndex++).setCellValue(datasetModel.getName());
                row.createCell(cellIndex++).setCellValue(datasetModel.getParamOwnerPath());
                row.createCell(cellIndex++).setCellValue(datasetModel.getParamOwnerId());
                row.createCell(cellIndex++).setCellValue(datasetModel.getParamName());
                row.createCell(cellIndex)
                        .setCellValue(formatRichTextField(datasetModel.getParamValue(), keepRteFormat));
            } catch (IllegalArgumentException e) {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(
                            "cannot export content for dataset '{}': {}",
                            datasetModel.getId(),
                            DATA_EXCEED_MAX_CELL_SIZE_MESSAGE,
                            e);
                }

                datasetSheet.removeRow(row);
                row = datasetSheet.createRow(rowIndex);
                row.createCell(0).setCellValue(errorCellTooLargeMessage);
            }

            rowIndex++;
        }
    }

    private String formatRichTextField(String text, boolean keepRteFormat) {
        return keepRteFormat ? text : HtmlUtils.htmlUnescape(text);
    }

    private void appendCustomFields(
            Row row, String codePrefix, List<CustomField> cufs, boolean keepRteFormat) {
        for (CustomField cuf : cufs) {
            String code = codePrefix + cuf.getCode();
            Integer idx = cufColumnsByCode.get(code);

            // if unknown : register it
            if (idx == null) {
                idx = registerCuf(row.getSheet(), code);
            }

            Cell cell = row.createCell(idx);
            String value = nullSafeValue(cuf);

            if (cuf.getType().equals(InputType.NUMERIC)) {
                value = NumericCufHelper.formatOutputNumericCufValue(value);
            } else if (cuf.getType().equals(InputType.RICH_TEXT)) {
                value = formatRichTextField(value, keepRteFormat);
            }

            cell.setCellValue(value);
        }
    }

    private void appendCoverage(ExportModel model) {
        List<CoverageModel> coverageModels = model.getCoverages();
        Sheet coverageSheet = workbook.getSheet(COVERAGE_SHEET);

        Row row;
        int rowIndex = coverageSheet.getLastRowNum() + 1;

        for (CoverageModel coverageModel : coverageModels) {
            row = coverageSheet.createRow(rowIndex);
            int cellIndex = 0;

            row.createCell(cellIndex++).setCellValue(coverageModel.getReqPath());
            row.createCell(cellIndex++).setCellValue(coverageModel.getReqVersion());
            row.createCell(cellIndex).setCellValue(coverageModel.getTcPath());

            rowIndex++;
        }
    }

    private String nullSafeValue(CustomField customField) {
        String value = customField.getValue();
        return value == null ? "" : value;
    }

    private int registerCuf(Sheet sheet, String code) {
        Row headers = sheet.getRow(0);
        int nextIdx = headers.getLastCellNum();
        headers.createCell(nextIdx).setCellValue(code);

        cufColumnsByCode.put(code, nextIdx);

        return nextIdx;
    }

    private String format(Date date) {
        if (date == null) {
            return "";
        } else {
            return DateUtils.formatIso8601Date(date);
        }
    }

    // for now we care only of Excel 2003
    private void createWorkbook() {
        Workbook wb = new HSSFWorkbook();
        wb.createSheet(TC_SHEET);
        wb.createSheet(TEST_STEP_SHEET);
        wb.createSheet(PARAMETER_SHEET);
        wb.createSheet(DATASET_SHEET);
        wb.createSheet(COVERAGE_SHEET);
        this.workbook = wb;
    }

    private void createHeaders() {
        createTestCaseSheetHeaders();
        createStepSheetHeaders();
        createParameterSheetHeaders();
        createDatasetSheetHeaders();
        createCoverageSheetHeaders();
    }

    private void createCoverageSheetHeaders() {
        createSheetHeaders(COVERAGE_SHEET, COVERAGE_COLUMNS);
    }

    private void createSheetHeaders(
            String sheetName, List<? extends TemplateColumn> templateColumns) {
        Sheet sheet = workbook.getSheet(sheetName);
        Row header = sheet.createRow(0);
        int cellIndex = 0;
        for (TemplateColumn templateColumn : templateColumns) {
            header.createCell(cellIndex++).setCellValue(templateColumn.getHeader());
        }
    }

    private void createDatasetSheetHeaders() {
        createSheetHeaders(DATASET_SHEET, DS_COLUMNS);
    }

    private void createParameterSheetHeaders() {
        createSheetHeaders(PARAMETER_SHEET, PRM_COLUMNS);
    }

    private void createStepSheetHeaders() {
        createSheetHeaders(TEST_STEP_SHEET, ST_COLUMNS);
    }

    private void createTestCaseSheetHeaders() {
        List<TestCaseSheetColumn> columns = isMilestoneModeEnabled ? TC_COLUMNS_MILESTONES : TC_COLUMNS;
        createSheetHeaders(TC_SHEET, columns);
        createOptionalTestCaseSheetHeaders();
    }

    protected void createOptionalTestCaseSheetHeaders() {
        Sheet testCaseSheet = workbook.getSheet(TC_SHEET);
        Row header = testCaseSheet.getRow(0);
        int cellIndex = header.getLastCellNum();

        if (projectFinder.countProjectsAllowAutomationWorkflow() > 0) {
            header.createCell(cellIndex).setCellValue(TC_AUTOMATABLE.name());
        }
    }
}
