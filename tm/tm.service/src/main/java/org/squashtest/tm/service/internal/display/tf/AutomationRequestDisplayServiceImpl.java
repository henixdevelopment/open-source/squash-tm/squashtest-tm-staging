/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.tf;

import java.util.Arrays;
import java.util.List;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.query.Operation;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.service.display.tf.AutomationRequestDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.tf.AutomationRequestGrid;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional(readOnly = true)
public class AutomationRequestDisplayServiceImpl implements AutomationRequestDisplayService {
    private final UserAccountService userAccountService;
    private final DSLContext dslContext;
    private final ProjectFinder projectFinder;

    public AutomationRequestDisplayServiceImpl(
            UserAccountService userAccountService, DSLContext dslContext, ProjectFinder projectFinder) {
        this.userAccountService = userAccountService;
        this.dslContext = dslContext;
        this.projectFinder = projectFinder;
    }

    @Override
    public GridResponse findAutomationRequestAssignedToCurrentUser(GridRequest request) {
        final UserDto currentUser = userAccountService.findCurrentUserDto();
        return fetchData(request, false, currentUser.getUserId());
    }

    @Override
    public GridResponse findAutomationRequestToTreat(GridRequest request) {
        GridFilterValue requestStatusFilterValue = new GridFilterValue();
        requestStatusFilterValue.setId("requestStatus");
        requestStatusFilterValue.setValues(
                Arrays.asList(
                        AutomationRequestStatus.AUTOMATION_IN_PROGRESS.name(),
                        AutomationRequestStatus.TRANSMITTED.name()));
        requestStatusFilterValue.setOperation(Operation.IN.name());
        request.getFilterValues().add(requestStatusFilterValue);
        return fetchData(request, true, null);
    }

    @Override
    public GridResponse findAutomationRequests(GridRequest request) {
        return fetchData(request, false, null);
    }

    private GridResponse fetchData(GridRequest request, boolean withAssigneeNull, Long assigneeId) {
        List<Long> readableProjectIds = projectFinder.findAllReadableIdsForAutomationWriter();
        AutomationRequestGrid grid =
                new AutomationRequestGrid(readableProjectIds, withAssigneeNull, assigneeId);
        return grid.getRows(request, dslContext);
    }
}
