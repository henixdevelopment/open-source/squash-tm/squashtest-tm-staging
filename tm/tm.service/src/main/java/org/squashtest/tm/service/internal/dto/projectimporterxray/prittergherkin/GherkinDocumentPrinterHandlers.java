/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.prittergherkin;

import gherkin.ast.Background;
import gherkin.ast.Comment;
import gherkin.ast.DataTable;
import gherkin.ast.DocString;
import gherkin.ast.Examples;
import gherkin.ast.Feature;
import gherkin.ast.Location;
import gherkin.ast.Scenario;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
import gherkin.ast.Tag;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class GherkinDocumentPrinterHandlers
        implements GherkinDocumentHandlers<GherkinDocumentPrinterResult> {
    private static final int SCENARIO_LEVEL = 1;
    private static final String INDENT = "  ";

    private final Pattern startOfLine = Pattern.compile("^", Pattern.MULTILINE);
    private final List<Comment> comments;

    public GherkinDocumentPrinterHandlers(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public GherkinDocumentPrinterResult handleFeature(
            Feature feature, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        gherkinDocumentPrinterResult.append(handleLanguageHeader(feature.getLanguage()));
        return appendFeature(gherkinDocumentPrinterResult, feature);
    }

    @Override
    public GherkinDocumentPrinterResult handleBackground(
            Background background, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        return appendBackground(gherkinDocumentPrinterResult, background);
    }

    @Override
    public GherkinDocumentPrinterResult handleDataTable(
            DataTable dataTable, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        int level = SCENARIO_LEVEL + 2;
        return appendTableRows(gherkinDocumentPrinterResult, dataTable.getRows(), level);
    }

    @Override
    public GherkinDocumentPrinterResult handleComment(
            Comment comment, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        appendComment(0, gherkinDocumentPrinterResult, comment);
        return gherkinDocumentPrinterResult;
    }

    @Override
    public GherkinDocumentPrinterResult handleDocString(
            DocString docString, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        String docstringDelimiter = "\"\"\"";
        int level = SCENARIO_LEVEL + 2;
        String indent = addIndent(level);
        String docStringContent = startOfLine.matcher(docString.getContent()).replaceAll(indent);

        return gherkinDocumentPrinterResult
                .append(indent)
                .append(docstringDelimiter)
                .append(docString.getContentType() == null ? "" : docString.getContentType())
                .append("\n")
                .append(docStringContent)
                .append("\n")
                .append(indent)
                .append(docstringDelimiter)
                .append("\n");
    }

    @Override
    public GherkinDocumentPrinterResult handleExamples(
            Examples examples, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        List<TableRow> tableRows = new ArrayList<>();
        if (examples.getTableHeader() != null) {
            tableRows.add(examples.getTableHeader());
            tableRows.addAll(examples.getTableBody());
        }
        gherkinDocumentPrinterResult = appendExamples(gherkinDocumentPrinterResult, examples);
        return appendTableRows(gherkinDocumentPrinterResult, tableRows, SCENARIO_LEVEL + 2);
    }

    @Override
    public GherkinDocumentPrinterResult handleScenario(
            Scenario scenario, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        return appendScenario(gherkinDocumentPrinterResult, scenario);
    }

    @Override
    public GherkinDocumentPrinterResult handleScenarioOutline(
            ScenarioOutline scenarioOutline, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        return appendScenarioOutline(gherkinDocumentPrinterResult, scenarioOutline);
    }

    @Override
    public GherkinDocumentPrinterResult handleStep(
            Step step, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        appendComments(
                step.getLocation(), gherkinDocumentPrinterResult, comments, SCENARIO_LEVEL + 1, true);
        appendComments(
                step.getLocation(), gherkinDocumentPrinterResult, comments, SCENARIO_LEVEL + 1, false);
        return gherkinDocumentPrinterResult
                .append(addIndent(SCENARIO_LEVEL + 1))
                .append(step.getKeyword())
                .append(step.getText())
                .append("\n");
    }

    @Override
    public GherkinDocumentPrinterResult handleTableCell(
            TableCell tableCell, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        return gherkinDocumentPrinterResult;
    }

    @Override
    public GherkinDocumentPrinterResult handleTableRow(
            TableRow tableRow, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        return gherkinDocumentPrinterResult;
    }

    @Override
    public GherkinDocumentPrinterResult handleTag(
            Tag tag, GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        return gherkinDocumentPrinterResult;
    }

    @Override
    public void appendNewLine(GherkinDocumentPrinterResult gherkinDocumentPrinterResult) {
        gherkinDocumentPrinterResult.append("\n");
    }

    private static String handleLanguageHeader(String language) {
        return String.format("# language: %s%n", language);
    }

    private String semiColumnAndName(String name) {
        return (name == null || name.isEmpty()) ? ":" : ": " + name;
    }

    private GherkinDocumentPrinterResult appendScenarioOutline(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult, ScenarioOutline scenarioOutline) {
        return appendScenarioCommon(
                gherkinDocumentPrinterResult,
                scenarioOutline.getLocation(),
                scenarioOutline.getTags(),
                scenarioOutline.getSteps(),
                scenarioOutline.getDescription(),
                scenarioOutline.getKeyword(),
                scenarioOutline.getName());
    }

    private GherkinDocumentPrinterResult appendScenario(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult, Scenario scenario) {
        return appendScenarioCommon(
                gherkinDocumentPrinterResult,
                scenario.getLocation(),
                scenario.getTags(),
                scenario.getSteps(),
                scenario.getDescription(),
                scenario.getKeyword(),
                scenario.getName());
    }

    private GherkinDocumentPrinterResult appendScenarioCommon(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult,
            Location location,
            List<Tag> scenarioTags,
            List<Step> steps,
            String description,
            String keyword,
            String name) {
        appendCommentsKeyword(location, gherkinDocumentPrinterResult, comments, SCENARIO_LEVEL);
        List<Tag> tags = scenarioTags != null ? scenarioTags : Collections.emptyList();
        int stepCount = steps != null ? steps.size() : 0;
        String prettyDescription = prettyDescription(description, SCENARIO_LEVEL);
        appendTags(gherkinDocumentPrinterResult, tags, SCENARIO_LEVEL, comments);
        return gherkinDocumentPrinterResult
                .append(addIndent(SCENARIO_LEVEL))
                .append(keyword)
                .append(semiColumnAndName(name))
                .append("\n")
                .append(prettyDescription)
                .append(!prettyDescription.trim().isEmpty() && stepCount > 0 ? "\n" : "");
    }

    private GherkinDocumentPrinterResult appendFeature(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult, Feature feature) {
        int level = 0;
        appendCommentsKeyword(feature.getLocation(), gherkinDocumentPrinterResult, comments, level);

        List<Tag> tags = feature.getTags() != null ? feature.getTags() : Collections.emptyList();
        appendTags(gherkinDocumentPrinterResult, tags, level, comments);
        return gherkinDocumentPrinterResult
                .append(addIndent(level))
                .append(feature.getKeyword())
                .append(semiColumnAndName(feature.getName()))
                .append("\n")
                .append(prettyDescription(feature.getDescription(), level));
    }

    private GherkinDocumentPrinterResult appendExamples(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult, Examples examples) {
        int level = SCENARIO_LEVEL + 1;
        appendCommentsKeyword(examples.getLocation(), gherkinDocumentPrinterResult, comments, level);
        List<Tag> tags = examples.getTags() != null ? examples.getTags() : Collections.emptyList();
        String description = prettyDescription(examples.getDescription(), level);
        appendTags(gherkinDocumentPrinterResult, tags, level, comments);
        return gherkinDocumentPrinterResult
                .append(addIndent(level))
                .append(examples.getKeyword())
                .append(semiColumnAndName(examples.getName()))
                .append("\n")
                .append(description);
    }

    private GherkinDocumentPrinterResult appendBackground(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult, Background background) {
        appendCommentsKeyword(
                background.getLocation(), gherkinDocumentPrinterResult, comments, SCENARIO_LEVEL);
        int stepCount = background.getSteps() != null ? background.getSteps().size() : 0;
        String description = prettyDescription(background.getDescription(), SCENARIO_LEVEL);
        return gherkinDocumentPrinterResult
                .append(addIndent(SCENARIO_LEVEL))
                .append(background.getKeyword())
                .append(semiColumnAndName(background.getName()))
                .append("\n")
                .append(description)
                .append(!description.trim().isEmpty() && stepCount > 0 ? "\n" : "");
    }

    private String prettyDescription(String description, int level) {
        if (description == null || description.trim().isEmpty()) {
            return "";
        }
        return addIndent(level + 1) + description.trim() + "\n";
    }

    private void appendTags(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult,
            List<Tag> tags,
            int level,
            List<Comment> comments) {
        if (tags.isEmpty()) {
            return;
        }
        String prefix = addIndent(level);
        String tagQuote = "";
        appendComments(tags.get(0).getLocation(), gherkinDocumentPrinterResult, comments, level, false);
        gherkinDocumentPrinterResult
                .append(prefix)
                .append(
                        tags.stream()
                                .map(tag -> tagQuote + tag.getName() + tagQuote)
                                .collect(Collectors.joining(" ")))
                .append("\n");
    }

    private String addIndent(int level) {
        return INDENT.repeat(Math.max(0, level));
    }

    private GherkinDocumentPrinterResult appendTableRows(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult,
            List<TableRow> tableRows,
            int level) {
        if (tableRows.isEmpty()) {
            return gherkinDocumentPrinterResult;
        }
        int[] maxWidths = new int[tableRows.get(0).getCells().size()];
        Arrays.fill(maxWidths, 0);
        for (TableRow tableRow : tableRows) {
            for (int j = 0; j < tableRow.getCells().size(); ++j) {
                TableCell tableCell = tableRow.getCells().get(j);
                maxWidths[j] = Math.max(maxWidths[j], getStringWidth(escapeCell(tableCell.getValue())));
            }
        }
        for (TableRow row : tableRows) {
            appendTableRow(gherkinDocumentPrinterResult, row, level, maxWidths, comments);
        }
        return gherkinDocumentPrinterResult;
    }

    private List<Comment> popComments(
            Location currentLocation, List<Comment> comments, int level, boolean previousBlock) {
        List<Comment> res = new ArrayList<>();
        Iterator<Comment> iter = comments.iterator();
        while (iter.hasNext()) {
            Comment comment = iter.next();
            if (currentLocation.getLine() > comment.getLocation().getLine()) {
                if (previousBlock && comment.getText().startsWith(addIndent(level))) {
                    // We skip this comment as we are appending previous block comments, and this comment has
                    // enough indents
                    // to be attached to the current block
                } else {
                    res.add(comment);
                    iter.remove();
                }
            }
        }
        return res;
    }

    private void appendTableRow(
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult,
            TableRow row,
            int level,
            int[] maxWidths,
            List<Comment> comments) {
        appendComments(row.getLocation(), gherkinDocumentPrinterResult, comments, level, false);

        gherkinDocumentPrinterResult.append(addIndent(level)).append("| ");
        for (int j = 0; j < row.getCells().size(); ++j) {
            if (j > 0) {
                gherkinDocumentPrinterResult.append(" | ");
            }
            TableCell tableCell = row.getCells().get(j);
            String escapedCellValue = escapeCell(tableCell.getValue());
            int spaceCount = maxWidths[j] - getStringWidth(escapedCellValue);
            String spaces = " ".repeat(spaceCount);
            gherkinDocumentPrinterResult.append(escapedCellValue + spaces);
        }
        gherkinDocumentPrinterResult.append(" |\n");
    }

    private void appendCommentsKeyword(
            Location location,
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult,
            List<Comment> comments,
            int level) {
        appendComments(location, gherkinDocumentPrinterResult, comments, level, true);
        gherkinDocumentPrinterResult.append(level == 0 ? "" : "\n");
        appendComments(location, gherkinDocumentPrinterResult, comments, level, false);
    }

    private void appendComments(
            Location location,
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult,
            List<Comment> comments,
            int level,
            boolean previousBlock) {
        int actualLevel = previousBlock ? level - 1 : level;

        for (Comment nextComment : popComments(location, comments, level, previousBlock)) {
            appendComment(actualLevel, gherkinDocumentPrinterResult, nextComment);
        }
    }

    private void appendComment(
            int actualLevel,
            GherkinDocumentPrinterResult gherkinDocumentPrinterResult,
            Comment nextComment) {
        if (nextComment.getText() == null || nextComment.getText().trim().isEmpty()) {
            return;
        }
        String comment = nextComment.getText().trim();
        if (!comment.isEmpty()) {
            gherkinDocumentPrinterResult
                    .append(addIndent(actualLevel))
                    .append("# " + comment.substring(1).trim())
                    .append("\n");
        }
    }

    private static int getStringWidth(String s) {
        int width = 0;
        for (char character : s.toCharArray()) {
            width += isCJKorFullWidth(character) ? 2 : 1;
        }
        return width;
    }

    private static boolean isCJKorFullWidth(char character) {
        Pattern pattern = Pattern.compile("[\\u3000-\\u9fff\\uac00-\\ud7af\\uff01-\\uff60]");
        return pattern.matcher(String.valueOf(character)).find();
    }

    private String escapeCell(String s) {
        StringBuilder e = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            switch (c) {
                case '\\':
                    e.append("\\\\");
                    break;
                case '\n':
                    e.append("\\n");
                    break;
                case '|':
                    e.append("\\|");
                    break;
                default:
                    e.append(c);
            }
        }
        return e.toString();
    }
}
