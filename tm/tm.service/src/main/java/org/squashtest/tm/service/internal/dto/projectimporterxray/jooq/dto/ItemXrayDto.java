/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;

public class ItemXrayDto extends AbstractGenericItem {
    private static final String PIVOT_ID_FORMAT = "%s_%s";

    private Long id;
    private String link;
    private String project;
    private String description;
    private String summary;
    private String type;
    private String priority;
    private String status;
    private String created;
    private String reporter;
    private String label;
    private String pivotId;
    private String parentId;
    private final List<CustomFieldXrayDto> customFields = new ArrayList<>();
    private final List<String> associatedIssues = new ArrayList<>();
    private final Map<String, String> associatedIssuesWithXrayKey = new HashMap<>();
    private final List<String> testPlanPivotIds = new ArrayList<>();
    private final List<String> labels = new ArrayList<>();
    private boolean calledParameter;

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPivotId() {
        return pivotId;
    }

    public void setPivotId(String pivotId) {
        this.pivotId = pivotId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<CustomFieldXrayDto> getCustomFields() {
        return customFields;
    }

    public List<String> getAssociatedIssues() {
        return associatedIssues;
    }

    public List<String> getTestPlanPivotIds() {
        return testPlanPivotIds;
    }

    public Map<String, String> getAssociatedIssuesWithXrayKey() {
        return associatedIssuesWithXrayKey;
    }

    public List<String> getLabels() {
        return labels;
    }

    public boolean isCalledParameter() {
        return calledParameter;
    }

    public void setCalledParameter(boolean calledParameter) {
        this.calledParameter = calledParameter;
    }

    // Methods
    public void addPivotId(XrayField.Type type) {
        switch (type) {
            case STORY ->
                    setPivotId(String.format(PIVOT_ID_FORMAT, XrayField.BASE_PIVOT_ID_STORY, this.id));
            case TEST ->
                    setPivotId(String.format(PIVOT_ID_FORMAT, XrayField.BASE_PIVOT_ID_TEST, this.id));
            case TEST_PLAN, TEST_EXECUTION, SUB_TEST_EXECUTION -> {
                if (isCampaign(type)) {
                    setPivotId(String.format(PIVOT_ID_FORMAT, XrayField.BASE_PIVOT_ID_CAMPAIGN, this.id));
                }
            }
            default -> {
                // Not yet supported
            }
        }
    }

    private boolean isCampaign(XrayField.Type type) {
        if (type == XrayField.Type.TEST_PLAN) {
            return true;
        } else {
            return getFilterCuf(XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TEST_PLAN)
                    .map(CustomFieldXrayDto::getValue)
                    .allMatch(Objects::isNull);
        }
    }

    public void setLabelFromLabels(List<String> labels) {
        this.label = StringUtils.join(labels, ',');
    }

    public List<CustomFieldXrayDto> getDataset() {
        return getFilterListCustomFieldXray(cuf -> Objects.nonNull(cuf.getDatasetRow())).toList();
    }

    public Map<Integer, Map<String, String>> getDatasetGherkin() {
        return getFilterListCustomFieldXray(cuf -> Objects.nonNull(cuf.getDatasetRow()))
                .collect(
                        Collectors.groupingBy(
                                CustomFieldXrayDto::getDatasetRow,
                                Collectors.toMap(
                                        CustomFieldXrayDto::getDatasetName,
                                        value -> {
                                            if (StringUtils.isNotBlank(value.getDatasetValue())) {
                                                return value.getDatasetValue();
                                            } else {
                                                return "null";
                                            }
                                        },
                                        (x, y) -> y,
                                        LinkedHashMap::new))); // keep the initial order of the datasetName
    }

    public List<CustomFieldXrayDto> getStep() {
        return getFilterListCustomFieldXray(cuf -> Objects.nonNull(cuf.getStepIndex())).toList();
    }

    public List<CustomFieldXrayDto> getFilterCufs(XrayField.CustomFieldKey... customFieldKey) {
        return getFilterCuf(customFieldKey).toList();
    }

    public List<String> getFilterCufValues(XrayField.CustomFieldKey... customFieldKey) {
        return getFilterCuf(customFieldKey).map(CustomFieldXrayDto::getValue).toList();
    }

    public String getFilterCufValue(XrayField.CustomFieldKey... customFieldKey) {
        return getFilterCuf(customFieldKey).map(CustomFieldXrayDto::getValue).findFirst().orElse(null);
    }

    public String getDateCufValue(
            String customFieldName, XrayField.CustomFieldKey... customFieldKey) {
        return getFilterCuf(customFieldKey)
                .filter(cuf -> customFieldName.equalsIgnoreCase(cuf.getName()))
                .map(CustomFieldXrayDto::getValue)
                .findFirst()
                .orElse(null);
    }

    private Stream<CustomFieldXrayDto> getFilterCuf(XrayField.CustomFieldKey... customFieldKey) {
        return getFilterListCustomFieldXray(
                cuf ->
                        Arrays.stream(customFieldKey)
                                .anyMatch(
                                        cufKey -> cufKey.equals(XrayField.CustomFieldKey.convertKey(cuf.getKey()))));
    }

    private Stream<CustomFieldXrayDto> getFilterListCustomFieldXray(
            Predicate<CustomFieldXrayDto> filterLambda) {
        return customFields.stream().filter(filterLambda);
    }
}
