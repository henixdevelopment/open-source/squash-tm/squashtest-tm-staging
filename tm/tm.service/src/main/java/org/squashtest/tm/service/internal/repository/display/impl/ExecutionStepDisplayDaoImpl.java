/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;

import com.google.common.collect.ListMultimap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.DenormalizedCustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionStepView;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.DenormalizedCustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ExecutionStepDisplayDao;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

@Repository
public class ExecutionStepDisplayDaoImpl implements ExecutionStepDisplayDao {

    private DSLContext jooq;
    private AttachmentDisplayDao attachmentDisplayDao;
    private DenormalizedCustomFieldValueDisplayDao denormalizedCustomFieldValueDisplayDao;
    private CustomFieldValueDisplayDao customFieldValueDisplayDao;

    public ExecutionStepDisplayDaoImpl(
            DSLContext jooq,
            AttachmentDisplayDao attachmentDisplayDao,
            DenormalizedCustomFieldValueDisplayDao denormalizedCustomFieldValueDisplayDao,
            CustomFieldValueDisplayDao customFieldValueDisplayDao) {
        this.jooq = jooq;
        this.attachmentDisplayDao = attachmentDisplayDao;
        this.denormalizedCustomFieldValueDisplayDao = denormalizedCustomFieldValueDisplayDao;
        this.customFieldValueDisplayDao = customFieldValueDisplayDao;
    }

    @Override
    public List<ExecutionStepView> findByExecutionId(Long executionId) {
        List<ExecutionStepView> executionStepViews =
                jooq
                        .select(
                                EXECUTION_STEP.EXECUTION_STEP_ID,
                                EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER,
                                EXECUTION_STEP.ATTACHMENT_LIST_ID,
                                EXECUTION_STEP.EXECUTION_STATUS,
                                EXECUTION_STEP.ACTION,
                                EXECUTION_STEP.EXPECTED_RESULT,
                                EXECUTION_STEP.COMMENT,
                                EXECUTION_STEP.LAST_EXECUTED_ON,
                                EXECUTION_STEP.LAST_EXECUTED_BY)
                        .from(EXECUTION_STEP)
                        .naturalJoin(EXECUTION_EXECUTION_STEPS)
                        .where(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(executionId))
                        .orderBy(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER.asc())
                        .stream()
                        .map(
                                record -> {
                                    ExecutionStepView executionStepView = new ExecutionStepView();
                                    executionStepView.setId(record.get(EXECUTION_STEP.EXECUTION_STEP_ID));
                                    executionStepView.setOrder(
                                            record.get(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER));
                                    executionStepView.setExecutionStatus(record.get(EXECUTION_STEP.EXECUTION_STATUS));
                                    executionStepView.setAction(
                                            HTMLCleanupUtils.cleanHtml(record.get(EXECUTION_STEP.ACTION)));
                                    executionStepView.setExpectedResult(
                                            HTMLCleanupUtils.cleanHtml(record.get(EXECUTION_STEP.EXPECTED_RESULT)));
                                    executionStepView.setComment(
                                            HTMLCleanupUtils.cleanHtml(record.get(EXECUTION_STEP.COMMENT)));
                                    executionStepView
                                            .getAttachmentList()
                                            .setId(record.get(EXECUTION_STEP.ATTACHMENT_LIST_ID));
                                    executionStepView.setLastExecutedOn(record.get(EXECUTION_STEP.LAST_EXECUTED_ON));
                                    executionStepView.setLastExecutedBy(record.get(EXECUTION_STEP.LAST_EXECUTED_BY));
                                    return executionStepView;
                                })
                        .toList();
        this.addAttachments(executionStepViews);
        this.addCustomFieldValues(executionStepViews);
        this.addDenormalizedCustomFieldValues(executionStepViews);
        return executionStepViews;
    }

    private void addCustomFieldValues(List<ExecutionStepView> executionStepViews) {
        List<Long> executionStepIds =
                executionStepViews.stream().map(ExecutionStepView::getId).toList();

        ListMultimap<Long, CustomFieldValueDto> customFieldValues =
                this.customFieldValueDisplayDao.findCustomFieldValues(
                        BindableEntity.EXECUTION_STEP, executionStepIds);

        executionStepViews.forEach(
                executionStepView ->
                        executionStepView.setCustomFieldValues(
                                customFieldValues.get(executionStepView.getId())));
    }

    private void addDenormalizedCustomFieldValues(List<ExecutionStepView> executionStepViews) {
        List<Long> executionStepIds =
                executionStepViews.stream().map(ExecutionStepView::getId).toList();

        ListMultimap<Long, DenormalizedCustomFieldValueDto> denormalizedCustomFieldValues =
                this.denormalizedCustomFieldValueDisplayDao.findDenormalizedCustomFieldValues(
                        DenormalizedFieldHolderType.EXECUTION_STEP, executionStepIds);

        executionStepViews.forEach(
                executionStepView ->
                        executionStepView.setDenormalizedCustomFieldValues(
                                denormalizedCustomFieldValues.get(executionStepView.getId())));
    }

    private void addAttachments(List<ExecutionStepView> executionStepViews) {
        Set<Long> attachmentListIds =
                executionStepViews.stream()
                        .map(ExecutionStepView::getAttachmentList)
                        .map(AttachmentListDto::getId)
                        .collect(Collectors.toSet());

        Map<Long, AttachmentListDto> attachmentMap =
                this.attachmentDisplayDao.findAttachmentListByIds(attachmentListIds).stream()
                        .collect(Collectors.toMap(AttachmentListDto::getId, Function.identity()));

        executionStepViews.forEach(
                executionStepView -> {
                    AttachmentListDto attachmentListDto =
                            attachmentMap.get(executionStepView.getAttachmentList().getId());
                    executionStepView.setAttachmentList(attachmentListDto);
                });
    }
}
