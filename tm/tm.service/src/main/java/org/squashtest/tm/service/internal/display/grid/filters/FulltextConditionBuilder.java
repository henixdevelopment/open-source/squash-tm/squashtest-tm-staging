/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.filters;

import static java.util.Objects.requireNonNull;

import java.util.List;
import org.jooq.Condition;
import org.jooq.Field;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

public class FulltextConditionBuilder<T> implements GridFilterConditionBuilder {

    private final Field<T> field;
    private final GridFilterValue gridFilterValue;

    FulltextConditionBuilder(Field<T> field, GridFilterValue gridFilterValue) {
        requireNonNull(field);
        requireNonNull(gridFilterValue);
        List<String> values = gridFilterValue.getValues();
        if (values.isEmpty()) {
            throw new IllegalArgumentException(
                    "At least one value is required to build a fulltext condition");
        }
        this.field = field;
        this.gridFilterValue = gridFilterValue;
    }

    @Override
    public Condition build() {
        T value = (T) gridFilterValue.getValues().get(0);
        return field.containsIgnoreCase(value);
    }
}
