/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;

public class ActionStepToPivot {
    @JsonProperty(JsonImportField.ID)
    private String pivotId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.ACTION)
    private String action;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.EXPECTED_RESULT)
    private String expectedResult;

    @JsonProperty(JsonImportField.CUSTOM_FIELDS)
    private List<String> customFieldToPivotList = new ArrayList<>(); // NYI

    @JsonProperty(JsonImportField.VERIFIED_REQUIREMENTS)
    private List<String> verifiedRequirements = new ArrayList<>();

    public String getPivotId() {
        return pivotId;
    }

    public void setPivotId(String testCasePivotId, Integer id) {
        this.pivotId = String.format("%s_%s%s", testCasePivotId, XrayField.BASE_PIVOT_ID_STEP, id);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    public List<String> getCustomFieldToPivotList() {
        return customFieldToPivotList;
    }

    public void setCustomFieldToPivotList(List<String> customFieldToPivotList) {
        this.customFieldToPivotList = customFieldToPivotList;
    }

    public List<String> getVerifiedRequirements() {
        return verifiedRequirements;
    }

    public void setVerifiedRequirements(List<String> verifiedRequirements) {
        this.verifiedRequirements = verifiedRequirements;
    }
}
