/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.testplanretriever;

import static org.squashtest.tm.domain.EntityType.ITERATION;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.testplanretriever.RestTestPlanFinder;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NoITPIFoundException;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NotFoundTargetUUIDException;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional
public class RestTestPlanFinderImpl implements RestTestPlanFinder {

    @Inject private IterationDao iterationDao;

    @Inject private TestSuiteDao testSuiteDao;

    @Inject private IterationTestPlanDao iterationTestPlanDao;

    @Inject private PermissionEvaluationService permissionService;

    @Inject private UserAccountService userAccountService;

    @Override
    public Iteration findIterationByUuid(String uuid) throws NotFoundTargetUUIDException {
        try {
            return iterationDao.findByUUID(uuid);
        } catch (NoResultException | EmptyResultDataAccessException ex) {
            throw new NotFoundTargetUUIDException();
        }
    }

    @Override
    public TestSuite findTestSuiteByUuid(String uuid) throws NotFoundTargetUUIDException {
        try {
            return testSuiteDao.findByUUID(uuid);
        } catch (NoResultException | EmptyResultDataAccessException ex) {
            throw new NotFoundTargetUUIDException();
        }
    }

    @Override
    public List<Long> getItemTestPlanIdsByIterationUuid(String uuid) throws NoITPIFoundException {
        List<Long> items = iterationTestPlanDao.getITPIsByIterationUUIDAndIsAutomated(uuid);
        if (items.isEmpty()) {
            throw new NoITPIFoundException();
        }

        return items;
    }

    @Override
    public List<Long> getItemTestPlanIdsByTestSuiteUuid(String uuid) throws NoITPIFoundException {
        List<Long> items = iterationTestPlanDao.getITPIsByTestSuiteUUIDAndIsAutomated(uuid);
        if (items.isEmpty()) {
            throw new NoITPIFoundException();
        }

        return items;
    }

    @Override
    public List<Long> getItemTestPlanIdsByIterationId(Long iterationId) {
        return iterationDao.findTestPlanIds(iterationId);
    }

    @Override
    public List<Long> getItemTestPlanIdsByTestSuiteId(Long testSuiteId) {
        return testSuiteDao.getTestPlanIds(testSuiteId);
    }

    @Override
    public List<Long> getItemTestPlanIdsFromSpecification(
            AutomatedSuiteCreationSpecification specification) {
        List<Long> itemTestPlanIds = specification.getTestPlanSubsetIds();
        boolean hasTestPlanSubset = itemTestPlanIds != null && !itemTestPlanIds.isEmpty();

        EntityType entityType = specification.getContext().getType();
        Long entityId = specification.getContext().getId();

        if (hasTestPlanSubset) {
            PermissionsUtils.checkPermission(
                    permissionService,
                    itemTestPlanIds,
                    Permissions.EXECUTE.name(),
                    IterationTestPlanItem.class.getName());
            return itemTestPlanIds;
        }

        return findItpiList(entityId, entityType);
    }

    private List<Long> findItpiList(Long entityId, EntityType entityType) {
        if (ITERATION.equals(entityType)) {
            return filterWithUserPermissionsOnItpi(entityId);
        } else {
            return iterationTestPlanDao.findAllByTestSuiteId(entityId);
        }
    }

    private List<Long> filterWithUserPermissionsOnItpi(Long iterationId) {
        if (!currentUserCanReadUnassigned(iterationId)) {
            String login = userAccountService.findCurrentUser().getLogin();

            return iterationTestPlanDao.findAllByIterationIdAndLogin(iterationId, login);
        }

        return iterationTestPlanDao.findAllByIterationId(iterationId);
    }

    private boolean currentUserCanReadUnassigned(Long iterationId) {
        return permissionService.hasRoleOrPermissionOnObject(
                Roles.ROLE_ADMIN,
                Permissions.READ_UNASSIGNED.name(),
                iterationId,
                Iteration.SIMPLE_CLASS_NAME);
    }
}
