/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.system;

import java.io.File;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.system.LogFileDownloadService;
import org.squashtest.tm.service.user.UserAccountService;

@Service
public class LogFileDownloadServiceImpl implements LogFileDownloadService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogFileDownloadServiceImpl.class);

    private final UserAccountService userAccountService;
    private final CustomProjectFinder customProjectFinder;
    private final Environment environment;

    @Value("${logging.dir:#{null}}")
    private String loggingPath;

    @Value("${squash.project-imports.folder-path:#{null}}")
    private String loggingPathImport;

    public LogFileDownloadServiceImpl(
            UserAccountService userAccountService,
            CustomProjectFinder customProjectFinder,
            Environment environment) {
        this.userAccountService = userAccountService;
        this.customProjectFinder = customProjectFinder;
        this.environment = environment;
    }

    @Override
    public File getCurrentLogFile() {
        checkIsLoggingPathSet();

        if (isDevMode()) {
            return new File(loggingPath + "/spring.log");
        } else {
            return new File(loggingPath + "/squash-tm.log");
        }
    }

    @Override
    public List<String> getAllPreviousLogFileNames() {
        checkIsLoggingPathSet();

        if (isDevMode()) {
            return new ArrayList<>();
        } else {
            return findAllLogFiles().stream().map(File::getName).toList();
        }
    }

    private List<File> findAllLogFiles() {
        final File logsFolder = new File(loggingPath);

        if (!logsFolder.exists() || !logsFolder.isDirectory()) {
            LOGGER.warn(
                    "Logs path '{}' is not a readable folder. There will be no log files",
                    logsFolder.getAbsolutePath());
            return new ArrayList<>();
        }

        LOGGER.info("Enumerating log files in folder '{}'", logsFolder.getAbsolutePath());

        final File[] logFiles =
                logsFolder.listFiles((File dir, String name) -> name.startsWith("squash-tm.log."));

        if (logFiles != null) {
            // Sort the log files by name
            return Arrays.stream(logFiles).sorted(Comparator.comparing(File::getName)).toList();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public File getPreviousLogFile(String fileName) throws AccessDeniedException {
        checkIsLoggingPathSet();

        if (!isPreviousLogFile(fileName)) {
            throw new AccessDeniedException("File " + fileName + " is not a valid log file.");
        }

        if (isDevMode()) {
            throw new IllegalStateException("You should not call getPreviousLogFile in a dev build.");
        } else {
            return new File(loggingPath + "/" + fileName);
        }
    }

    private boolean isPreviousLogFile(String fileName) {
        final List<String> logFileNames = getAllPreviousLogFileNames();
        return logFileNames.contains(fileName);
    }

    private void checkIsLoggingPathSet() {
        if (loggingPath == null) {
            throw new IllegalStateException(
                    "Logging path is not set. It should be set in application.properties or as an environment variable with key 'logging.dir'.");
        }
    }

    private void checkIsLoggingPathImportSet() {
        if (loggingPathImport == null) {
            throw new IllegalStateException(
                    "Logging import path is not set. It should be set in application.properties or as an environment variable with key 'squash.project-imports.folder-path'.");
        }
    }

    @Override
    public File getLogFileWithPath(String filePath, String origin) throws IOException {
        checkIsAdminOrCanManageAtLeastOneProject();
        if (!isDevMode()) {
            // Log properties are checked in the method below
            checkIsInsideLogFolder(filePath, origin);
        }

        return new File(filePath);
    }

    private void checkIsAdminOrCanManageAtLeastOneProject() throws AccessDeniedException {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        List<Long> manageableProjectIds = customProjectFinder.findAllManageableIds(currentUser);

        if (!currentUser.isAdmin() && manageableProjectIds.isEmpty()) {
            throw new AccessDeniedException("Current user cannot manage any project");
        }
    }

    private void checkIsInsideLogFolder(String filePath, String origin) throws IOException {
        switch (LogFileOrigin.getLogFileOrigin(origin)) {
            case SYNCHRO -> {
                checkIsLoggingPathSet();
                checkIsInsideLogFolder(new File(loggingPath), filePath, LogFileOrigin.SYNCHRO.getName());
            }
            case IMPORT -> {
                checkIsLoggingPathImportSet();
                checkIsInsideLogFolder(
                        new File(loggingPathImport), filePath, LogFileOrigin.IMPORT.getName());
            }
            default -> throw new IllegalArgumentException("Unknown log folder origin: " + origin);
        }
    }

    private void checkIsInsideLogFolder(File logsFolder, String filePath, String name)
            throws IOException {
        final File candidate = new File(filePath);
        if (!candidate.getCanonicalPath().startsWith(logsFolder.getCanonicalPath())) {
            throw new AccessDeniedException(
                    "File " + filePath + " is not a valid " + name + " log file.");
        }
    }

    private boolean isDevMode() {
        return Arrays.asList(environment.getActiveProfiles()).contains("dev");
    }

    private enum LogFileOrigin {
        SYNCHRO("synchronisation"),
        IMPORT("import");

        private final String name;

        LogFileOrigin(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static LogFileOrigin getLogFileOrigin(String origin) {
            try {
                return LogFileOrigin.valueOf(origin);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Unsupported log file origin: " + origin);
            }
        }
    }
}
