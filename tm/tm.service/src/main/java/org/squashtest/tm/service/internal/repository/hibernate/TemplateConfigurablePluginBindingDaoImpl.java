/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.TEMPLATE_CONFIGURABLE_PLUGIN_BINDING;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.project.TemplateConfigurablePluginBinding;
import org.squashtest.tm.service.internal.display.dto.TemplateConfigurablePluginBindingDto;
import org.squashtest.tm.service.internal.repository.TemplateConfigurablePluginBindingDao;

@Repository
public class TemplateConfigurablePluginBindingDaoImpl
        implements TemplateConfigurablePluginBindingDao {

    @PersistenceContext private EntityManager entityManager;

    private final DSLContext dsl;

    public TemplateConfigurablePluginBindingDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public TemplateConfigurablePluginBinding createTemplateConfigurablePluginBinding(
            ProjectTemplate template, Project project, TemplateConfigurablePlugin plugin) {
        TemplateConfigurablePluginBinding binding =
                TemplateConfigurablePluginBinding.createBinding(template, project, plugin);
        entityManager.persist(binding);
        return binding;
    }

    @Override
    public void removeAllForTemplate(long templateId) {
        entityManager
                .createQuery(
                        "delete from TemplateConfigurablePluginBinding binding where binding.projectTemplate.id = :templateId")
                .setParameter("templateId", templateId)
                .executeUpdate();
    }

    @Override
    public void removeAllForProject(long projectId) {
        entityManager
                .createQuery(
                        "delete from TemplateConfigurablePluginBinding binding where binding.project.id = :projectId")
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public TemplateConfigurablePluginBindingDto findOne(
            long templateId, long projectId, String pluginId) {
        return dsl.selectFrom(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING)
                .where(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PLUGIN_ID.eq(pluginId))
                .and(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_TEMPLATE_ID.eq(templateId))
                .and(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_ID.eq(projectId))
                .fetchOneInto(TemplateConfigurablePluginBindingDto.class);
    }

    @Override
    public List<TemplateConfigurablePluginBindingDto> findAll(
            long templateId, long projectId, List<String> pluginId) {
        return dsl.selectFrom(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING)
                .where(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PLUGIN_ID.in(pluginId))
                .and(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_TEMPLATE_ID.eq(templateId))
                .and(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_ID.eq(projectId))
                .fetchInto(TemplateConfigurablePluginBindingDto.class);
    }

    @Override
    public List<TemplateConfigurablePluginBindingDto> findAllByTemplateId(long templateId) {
        return dsl.selectFrom(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING)
                .where(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_TEMPLATE_ID.eq(templateId))
                .fetchInto(TemplateConfigurablePluginBindingDto.class);
    }

    @Override
    public List<TemplateConfigurablePluginBindingDto> findAllByTemplateIdAndPluginId(
            long templateId, String pluginId) {
        return dsl.selectFrom(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING)
                .where(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PLUGIN_ID.eq(pluginId))
                .and(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_TEMPLATE_ID.eq(templateId))
                .fetchInto(TemplateConfigurablePluginBindingDto.class);
    }

    @Override
    public boolean isProjectConfigurationBoundToTemplate(long projectId) {
        return dsl.selectCount()
                        .from(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING)
                        .where(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_ID.eq(projectId))
                        .fetchOneInto(Integer.class)
                > 0;
    }

    @Override
    public boolean isProjectConfigurationBoundToTemplate(long projectId, String pluginId) {
        return dsl.selectCount()
                        .from(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING)
                        .where(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PLUGIN_ID.eq(pluginId))
                        .and(TEMPLATE_CONFIGURABLE_PLUGIN_BINDING.PROJECT_ID.eq(projectId))
                        .fetchOneInto(Integer.class)
                > 0;
    }
}
