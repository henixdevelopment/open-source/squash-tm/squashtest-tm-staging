/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserAdminViewDto {
    private Long id;
    private String login;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean active;
    private Date createdOn;
    private String createdBy;
    private String lastModifiedBy;
    private Date lastModifiedOn;
    private Date lastConnectedOn;
    private Integer usersGroupBinding;
    private List<UsersGroupDto> usersGroups = new ArrayList<>();
    private List<ProjectPermissionDto> projectPermissions = new ArrayList<>();
    private List<ProfileDto> profiles = new ArrayList<>();
    private List<UserAdminViewTeamDto> teams = new ArrayList<>();
    private boolean canManageLocalPassword;
    private boolean canDeleteFromFront;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Date getLastConnectedOn() {
        return lastConnectedOn;
    }

    public void setLastConnectedOn(Date lastConnectedOn) {
        this.lastConnectedOn = lastConnectedOn;
    }

    public Integer getUsersGroupBinding() {
        return usersGroupBinding;
    }

    public void setUsersGroupBinding(Integer usersGroupBinding) {
        this.usersGroupBinding = usersGroupBinding;
    }

    public List<UsersGroupDto> getUsersGroups() {
        return usersGroups;
    }

    public void setUsersGroups(List<UsersGroupDto> usersGroups) {
        this.usersGroups = usersGroups;
    }

    public List<ProjectPermissionDto> getProjectPermissions() {
        return projectPermissions;
    }

    public void setProjectPermissions(List<ProjectPermissionDto> projectPermissions) {
        this.projectPermissions = projectPermissions;
    }

    public List<UserAdminViewTeamDto> getTeams() {
        return teams;
    }

    public void setTeams(List<UserAdminViewTeamDto> teams) {
        this.teams = teams;
    }

    public boolean isCanManageLocalPassword() {
        return canManageLocalPassword;
    }

    public void setCanManageLocalPassword(boolean canManageLocalPassword) {
        this.canManageLocalPassword = canManageLocalPassword;
    }

    public boolean isCanDeleteFromFront() {
        return canDeleteFromFront;
    }

    public void setCanDeleteFromFront(boolean canDeleteFromFront) {
        this.canDeleteFromFront = canDeleteFromFront;
    }

    public List<ProfileDto> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<ProfileDto> profiles) {
        this.profiles = profiles;
    }
}
