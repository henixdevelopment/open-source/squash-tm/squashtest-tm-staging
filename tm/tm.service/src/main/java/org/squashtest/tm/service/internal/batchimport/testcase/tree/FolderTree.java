/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase.tree;

import static org.squashtest.tm.core.foundation.lang.PathUtils.splitPath;
import static org.squashtest.tm.core.foundation.lang.PathUtils.unescapePathPartSlashes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import org.squashtest.tm.service.internal.batchimport.Batches;
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseImportData;

public class FolderTree {
    private final FolderNode root;

    public FolderTree(String rootPath, Map<String, List<TestCaseImportData>> paths) {
        this.root = new FolderNode(rootPath, rootPath);
        createTree(paths);
    }

    public FolderNode getRoot() {
        return root;
    }

    private void createTree(Map<String, List<TestCaseImportData>> contentByPath) {
        for (var entry : contentByPath.entrySet()) {
            String[] parts = splitPath(entry.getKey());
            FolderNode current = root;
            StringBuilder currentPath = new StringBuilder(root.getPath());

            for (int i = 1; i < parts.length; i++) {
                String part = unescapePathPartSlashes(parts[i]);
                appendToPath(currentPath, part);

                current = current.getChildOrCreate(part, currentPath.toString());

                if (i == parts.length - 1) {
                    current.addContents(entry.getValue());
                }
            }
        }
    }

    private void appendToPath(StringBuilder path, String part) {
        path.append("/").append(part);
    }

    public List<MissingNode> collectMissingNodes(
            NavigableMap<String, Long> existingPaths, Long testCaseLibraryId) {
        List<MissingNode> missingNodes = new ArrayList<>();

        for (FolderNode node : root.getChildren().values()) {
            findMissingNodes(node, existingPaths, missingNodes, testCaseLibraryId, true);
        }

        return missingNodes;
    }

    private void findMissingNodes(
            FolderNode node,
            NavigableMap<String, Long> existingPaths,
            List<MissingNode> missingNodes,
            Long parentId,
            boolean isLibraryContent) {
        Long currentNodeId = existingPaths.get(node.getPath());

        if (currentNodeId == null) {
            missingNodes.add(new MissingNode(node, parentId, isLibraryContent));
            return;
        }

        for (FolderNode child : node.getChildren().values()) {
            findMissingNodes(child, existingPaths, missingNodes, currentNodeId, false);
        }
    }

    public Batches<TestCaseImportData> collectExistingNodes(
            NavigableMap<String, Long> existingPaths) {
        Batches<TestCaseImportData> batch = new Batches<>();

        for (FolderNode node : root.getChildren().values()) {
            findExistingNodes(node, existingPaths, batch);
        }

        return batch;
    }

    private void findExistingNodes(
            FolderNode node,
            NavigableMap<String, Long> existingPaths,
            Batches<TestCaseImportData> batch) {
        Long currentNodeId = existingPaths.get(node.getPath());

        if (currentNodeId == null) {
            return;
        }

        if (!node.getContents().isEmpty()) {
            batch.addBatch(currentNodeId, node.getContents());
        }

        for (FolderNode child : node.getChildren().values()) {
            findExistingNodes(child, existingPaths, batch);
        }
    }
}
