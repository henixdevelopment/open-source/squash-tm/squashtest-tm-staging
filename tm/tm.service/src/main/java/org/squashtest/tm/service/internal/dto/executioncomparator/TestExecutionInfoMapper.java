/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.executioncomparator;

import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;
import static org.squashtest.tm.jooq.domain.tables.Execution.EXECUTION;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jooq.Record;
import org.jooq.RecordMapper;

public class TestExecutionInfoMapper implements RecordMapper<Record, TestExecutionInfo> {

    @Override
    public TestExecutionInfo map(Record record) {
        TestExecutionInfo testExecutionInfo = new TestExecutionInfo();

        testExecutionInfo.setTestId(record.get(EXECUTION.TCLN_ID));
        testExecutionInfo.setTestName(record.get(EXECUTION.NAME));
        testExecutionInfo.setDataset(record.get(EXECUTION.DATASET_LABEL));

        Map<String, String> statusBySuite = new HashMap<>();
        statusBySuite.put(record.get(AUTOMATED_SUITE.SUITE_ID), record.get(EXECUTION.EXECUTION_STATUS));
        testExecutionInfo.setStatusBySuite(statusBySuite);

        return testExecutionInfo;
    }

    public static List<TestExecutionInfo> aggregate(List<TestExecutionInfo> testExecutionInfos) {
        Map<String, TestExecutionInfo> filter = new HashMap<>();

        for (TestExecutionInfo testInfo : testExecutionInfos) {
            String key = testInfo.getTestId() + "-" + testInfo.getDataset();
            filter.putIfAbsent(key, testInfo);

            Map<String, String> statusBySuite = filter.get(key).getStatusBySuite();
            testInfo
                    .getStatusBySuite()
                    .forEach(
                            (statusKey, statusValue) ->
                                    statusBySuite.merge(statusKey, statusValue, (oldValue, newValue) -> oldValue));
        }

        return new ArrayList<>(filter.values());
    }
}
