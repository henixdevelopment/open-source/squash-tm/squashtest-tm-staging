/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.count;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import java.util.Collection;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.DSLContext;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.infolist.SystemInfoListItemCode;
import org.squashtest.tm.service.internal.repository.CustomInfoListItemDao;

public class InfoListItemDaoImpl implements CustomInfoListItemDao {
    private static final String ITEM_CODE = "itemCode";
    private static final String PROJECT_ID = "projectId";

    @PersistenceContext private EntityManager entityManager;
    @Inject private DSLContext dslContext;

    @Override
    public boolean isCategoryConsistent(long projectId, String itemCode) {
        Query q = entityManager.createNamedQuery("infoListItem.foundCategoryInProject");
        q.setParameter(PROJECT_ID, projectId);
        q.setParameter(ITEM_CODE, itemCode);
        return (Long) q.getSingleResult() == 1;
    }

    @Override
    public Set<String> filterConsistentCategories(long projectId, Collection<String> codes) {
        return dslContext
                .select(INFO_LIST_ITEM.CODE)
                .from(PROJECT)
                .innerJoin(INFO_LIST_ITEM)
                .on(PROJECT.REQ_CATEGORIES_LIST.eq(INFO_LIST_ITEM.LIST_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId).and(INFO_LIST_ITEM.CODE.in(codes)))
                .groupBy(INFO_LIST_ITEM.CODE)
                .having(count(INFO_LIST_ITEM.ITEM_ID).eq(1))
                .fetchSet(INFO_LIST_ITEM.CODE);
    }

    @Override
    public boolean isNatureConsistent(long projectId, String itemCode) {
        Query q = entityManager.createNamedQuery("infoListItem.foundNatureInProject");
        q.setParameter(PROJECT_ID, projectId);
        q.setParameter(ITEM_CODE, itemCode);
        return (Long) q.getSingleResult() == 1;
    }

    @Override
    public Set<String> filterConsistentNatures(long projectId, Collection<String> itemCodes) {
        return dslContext
                .select(INFO_LIST_ITEM.CODE)
                .from(PROJECT)
                .innerJoin(INFO_LIST_ITEM)
                .on(PROJECT.TC_NATURES_LIST.eq(INFO_LIST_ITEM.LIST_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId).and(INFO_LIST_ITEM.CODE.in(itemCodes)))
                .groupBy(INFO_LIST_ITEM.CODE)
                .having(count(INFO_LIST_ITEM.ITEM_ID).eq(1))
                .fetchSet(INFO_LIST_ITEM.CODE);
    }

    @Override
    public boolean isTypeConsistent(long projectId, String itemCode) {
        Query q = entityManager.createNamedQuery("infoListItem.foundTypeInProject");
        q.setParameter(PROJECT_ID, projectId);
        q.setParameter(ITEM_CODE, itemCode);
        return (Long) q.getSingleResult() == 1;
    }

    @Override
    public Set<String> filterConsistentTypes(long projectId, Collection<String> itemCodes) {
        return dslContext
                .select(INFO_LIST_ITEM.CODE)
                .from(PROJECT)
                .innerJoin(INFO_LIST_ITEM)
                .on(PROJECT.TC_TYPES_LIST.eq(INFO_LIST_ITEM.LIST_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId).and(INFO_LIST_ITEM.CODE.in(itemCodes)))
                .groupBy(INFO_LIST_ITEM.CODE)
                .having(count(INFO_LIST_ITEM.ITEM_ID).eq(1))
                .fetchSet(INFO_LIST_ITEM.CODE);
    }

    @Override
    public void unbindFromLibraryObjects(long infoListId) {
        InfoListItem defaultReqCat = findByCode(SystemInfoListItemCode.CAT_UNDEFINED.getCode());
        execUpdateQuery(infoListId, "infoList.setReqCatToDefault", defaultReqCat);
        InfoListItem defaultTcNat = findByCode(SystemInfoListItemCode.NAT_UNDEFINED.getCode());
        execUpdateQuery(infoListId, "infoList.setTcNatToDefault", defaultTcNat);
        InfoListItem defaultTcType = findByCode(SystemInfoListItemCode.TYP_UNDEFINED.getCode());
        execUpdateQuery(infoListId, "infoList.setTcTypeToDefault", defaultTcType);
    }

    private InfoListItem findByCode(String code) {
        return (InfoListItem)
                entityManager
                        .createNamedQuery("InfoListItem.findByCode")
                        .setParameter("code", code)
                        .getSingleResult();
    }

    private void execUpdateQuery(long infoListId, String queryName, InfoListItem defaultParam) {
        Query query = entityManager.createNamedQuery(queryName);
        query.setParameter("default", defaultParam);
        query.setParameter("id", infoListId);
        query.executeUpdate();
    }

    @Override
    public boolean isUsed(long infoListItemId) {
        Query q = entityManager.createNamedQuery("infoListItem.isUsedInRequirements");
        q.setParameter("id", infoListItemId);

        // [Issue 7568] separated the previous query in two simpler ones to improve the performance
        Query q1 = entityManager.createNamedQuery("infoListItem.isUsedInTestCases");
        q1.setParameter("id", infoListItemId);
        return (Long) q.getSingleResult() > 0 || (Long) q1.getSingleResult() > 0;
    }

    @Override
    public void removeInfoListItem(long infoListItemId, InfoListItem defaultItem) {

        execUpdateQuery(infoListItemId, "infoListItem.setReqCatToDefault", defaultItem);
        execUpdateQuery(infoListItemId, "infoListItem.setTcNatToDefault", defaultItem);
        execUpdateQuery(infoListItemId, "infoListItem.setTcTypeToDefault", defaultItem);
    }
}
