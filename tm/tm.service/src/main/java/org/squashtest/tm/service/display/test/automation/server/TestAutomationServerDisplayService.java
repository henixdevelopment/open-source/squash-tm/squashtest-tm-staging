/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.test.automation.server;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

public interface TestAutomationServerDisplayService {

    GridResponse getTestAutomationServerGrid(GridRequest request);

    List<TestAutomationServerDto> findAll();

    TestAutomationServerAdminViewDto getTestAutomationServerView(long testAutomationServerId);

    List<String> getDefaultEnvironmentTags(long testAutomationServerId);

    boolean hasDefinedCredentials(long testAutomationServerId);

    Map<Long, Long> findProjectAndServerIdsByIterationId(Long iterationId);

    Map<Long, Long> findProjectAndServerIdsByTestSuiteId(Long testSuiteId);

    Long findTestAutomationServerIdByGenericProjectId(long genericProjectId);

    Long findTestAutomationServerIdByGenericProjectIdUnsecured(long genericProjectId);
}
