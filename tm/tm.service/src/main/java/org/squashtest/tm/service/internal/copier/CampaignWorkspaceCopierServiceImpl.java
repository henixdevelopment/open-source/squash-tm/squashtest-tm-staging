/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.copier;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.domain.library.TreeNode;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.copier.CampaignWorkspaceCopierService;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;

@Service
@Transactional
public class CampaignWorkspaceCopierServiceImpl implements CampaignWorkspaceCopierService {
    private final PrivateCustomFieldValueService customFieldValueManagerService;
    private final AttachmentManagerService attachmentManagerService;
    private final MessageSource messageSource;
    private final EntityManager entityManager;

    public CampaignWorkspaceCopierServiceImpl(
            PrivateCustomFieldValueService customFieldValueManagerService,
            AttachmentManagerService attachmentManagerService,
            MessageSource messageSource,
            EntityManager entityManager) {
        this.customFieldValueManagerService = customFieldValueManagerService;
        this.attachmentManagerService = attachmentManagerService;
        this.messageSource = messageSource;
        this.entityManager = entityManager;
    }

    @Override
    public <ENTITY extends TreeNode, NODE extends ENTITY> void copyNodeToContainer(
            NodeContainer<ENTITY> container,
            List<NODE> nodes,
            Map<String, Map<Long, Long>> pairingIdsSourceCopyNode) {
        String copySuffix =
                messageSource.getMessage("label.CopySuffix", null, LocaleContextHolder.getLocale());
        NodeCopierVisitor nodeCopierVisitor =
                new NodeCopierVisitor(
                        entityManager,
                        attachmentManagerService,
                        container,
                        copySuffix,
                        pairingIdsSourceCopyNode);

        nodes.forEach(
                source -> {
                    boolean projectChanged =
                            !source.getProject().getId().equals(container.getProject().getId());
                    nodeCopierVisitor.setProjectChanged(projectChanged);
                    nodeCopierVisitor.performCopy(source);
                });
        customFieldValueManagerService.copyCustomFieldValues(nodeCopierVisitor.getBoundEntityCopies());
        customFieldValueManagerService.copyCustomFieldValuesOnProjectChanged(
                nodeCopierVisitor.getBoundEntityCopiesProjectChanged(), container.getProject().getId());
        attachmentManagerService.batchCopyContentsOnExternalRepository(
                nodeCopierVisitor.getAttachmentHolders());
        entityManager.flush();

        // Evict only nodes that container is not the parent of to avoid DetachedEntityException
        nodes.stream()
                .filter(node -> !container.getContent().contains(node))
                .forEach(entityManager::detach);
    }

    @Override
    public void copyNodeToMultipleContainers(
            List<ChildEntityDto> childEntityDtos, Map<String, Map<Long, Long>> pairingIdsSourceCopyNode) {
        if (childEntityDtos.isEmpty()) {
            return;
        }
        String copySuffix =
                messageSource.getMessage("label.CopySuffix", null, LocaleContextHolder.getLocale());
        NodeCopierVisitor nodeCopierVisitor =
                new NodeCopierVisitor(
                        entityManager, attachmentManagerService, null, copySuffix, pairingIdsSourceCopyNode);
        childEntityDtos.forEach(
                childEntityDto -> {
                    nodeCopierVisitor.setDestination(childEntityDto.getTargetContainer());
                    childEntityDto
                            .getChildren()
                            .forEach(
                                    source -> {
                                        boolean projectChanged =
                                                !source
                                                        .getProject()
                                                        .getId()
                                                        .equals(childEntityDto.getTargetContainer().getProject().getId());
                                        nodeCopierVisitor.setProjectChanged(projectChanged);
                                        nodeCopierVisitor.performCopy(source);
                                    });
                });

        customFieldValueManagerService.copyCustomFieldValues(nodeCopierVisitor.getBoundEntityCopies());
        customFieldValueManagerService.copyCustomFieldValuesOnProjectChanged(
                nodeCopierVisitor.getBoundEntityCopiesProjectChanged(),
                childEntityDtos.get(0).getTargetContainer().getProject().getId());
        attachmentManagerService.batchCopyContentsOnExternalRepository(
                nodeCopierVisitor.getAttachmentHolders());
        entityManager.flush();
    }
}
