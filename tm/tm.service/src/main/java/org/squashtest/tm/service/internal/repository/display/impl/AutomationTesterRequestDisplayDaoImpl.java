/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;

import java.util.List;
import org.jooq.DSLContext;
import org.jooq.SelectConditionStep;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.service.internal.repository.display.AutomationTesterRequestDisplayDao;

@Repository
public class AutomationTesterRequestDisplayDaoImpl implements AutomationTesterRequestDisplayDao {

    private final DSLContext dsl;

    public AutomationTesterRequestDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public int countGlobalAutomationRequests(List<Long> projectIds) {
        Integer count = baseRequest(projectIds).fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public int countReadyForTransmissionAutomRequests(List<Long> projectIds) {
        Integer count =
                baseRequest(projectIds)
                        .and(
                                AUTOMATION_REQUEST.REQUEST_STATUS.eq(
                                        AutomationRequestStatus.READY_TO_TRANSMIT.name()))
                        .fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public int countToBeValidatedAutomRequests(List<Long> projectIds) {
        Integer count =
                baseRequest(projectIds)
                        .and(
                                AUTOMATION_REQUEST.REQUEST_STATUS.in(
                                        AutomationRequestStatus.WORK_IN_PROGRESS.name(),
                                        AutomationRequestStatus.REJECTED.name(),
                                        AutomationRequestStatus.SUSPENDED.name()))
                        .fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public List<String> getLastModifyingUserLoginsForTesterView(
            List<Long> projectIds, List<String> requestStatus) {
        return dsl.selectDistinct(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
                .from(TEST_CASE_LIBRARY_NODE)
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .where(AUTOMATION_REQUEST.REQUEST_STATUS.in(requestStatus))
                .and(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()))
                .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                .and(PROJECT.PROJECT_ID.in(projectIds))
                .orderBy(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
                .fetch(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY);
    }

    private SelectConditionStep<?> baseRequest(List<Long> projectIds) {
        return dsl.selectCount()
                .from(AUTOMATION_REQUEST)
                .innerJoin(TEST_CASE)
                .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID))
                .innerJoin(PROJECT)
                .on(AUTOMATION_REQUEST.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(
                        AUTOMATION_REQUEST
                                .PROJECT_ID
                                .in(projectIds)
                                .and(TEST_CASE.AUTOMATABLE.eq("Y"))
                                .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                                .and(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(AutomationWorkflowType.NATIVE.name())));
    }
}
