/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption;
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingValueService;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;

@Service
@Transactional
public class EnvironmentVariableBindingValueServiceImpl
        implements EnvironmentVariableBindingValueService {

    private final ProjectDisplayDao projectDisplayDao;
    private final EnvironmentVariableBindingDao environmentVariableBindingDao;
    private final EnvironmentVariableDao environmentVariableDao;

    public EnvironmentVariableBindingValueServiceImpl(
            ProjectDisplayDao projectDisplayDao,
            EnvironmentVariableBindingDao environmentVariableBindingDao,
            EnvironmentVariableDao environmentVariableDao) {
        this.projectDisplayDao = projectDisplayDao;
        this.environmentVariableBindingDao = environmentVariableBindingDao;
        this.environmentVariableDao = environmentVariableDao;
    }

    @Override
    public void editEnvironmentVariableValue(Long bindingId, String value) {
        EnvironmentVariableBinding binding = environmentVariableBindingDao.getReferenceById(bindingId);
        checkValueIsValidOption(binding.getEnvironmentVariable().getId(), value);
        binding.updateValue(value);
    }

    private void checkValueIsValidOption(Long environmentVariableId, String value) {
        SingleSelectEnvironmentVariable environmentVariable =
                environmentVariableDao.findSingleSelectEnvironmentVariableById(environmentVariableId);
        if (environmentVariable != null) {
            List<String> values =
                    environmentVariable.getOptions().stream()
                            .map(EnvironmentVariableOption::getLabel)
                            .toList();
            if (StringUtils.hasText(value) && !values.contains(value)) {
                throw new IllegalArgumentException();
            }
        }
    }

    /**
     * Sets the value of an environment variable to the nearest defined value on a higher-level
     * entity. If no value is defined on the previous-level, the method will return the value of the
     * variable at the highest-level (server).
     *
     * @param bindingId id of the binding
     * @return String value
     */
    @Override
    public String resetDefaultValue(Long bindingId, String entityType) {
        EnvironmentVariableBinding binding = environmentVariableBindingDao.getReferenceById(bindingId);

        if (EVBindableEntity.PROJECT.name().equals(entityType)) {
            setProjectValueFromExistingServerValue(binding);
        }
        return binding.getValue();
    }

    @Override
    public void setProjectValueFromExistingServerValue(EnvironmentVariableBinding binding) {
        Long testAutomationServerId = projectDisplayDao.getTaServerIdByProjectId(binding.getEntityId());
        if (Objects.nonNull(testAutomationServerId)) {
            EnvironmentVariableBinding serverBinding =
                    environmentVariableBindingDao.findByEntityIdTypeAndEvId(
                            testAutomationServerId,
                            EVBindableEntity.TEST_AUTOMATION_SERVER,
                            binding.getEnvironmentVariable().getId());

            if (Objects.nonNull(serverBinding)) {
                binding.setValue(serverBinding.getValue());
            }
        }
    }

    /**
     * Retrieve all bindings where value in deleted option labels and set it to an empty value.
     *
     * @param values Deleted option labels.
     * @param environmentVariableId Environment variable id containing deleted options.
     */
    @Override
    public void reinitializeEnvironmentVariableValuesByValueAndEvId(
            List<String> values, Long environmentVariableId) {
        findAndReplaceBindingValues(environmentVariableId, values, "");
    }

    /**
     * Retrieve all values equal to environment variable option old label value and update to new
     * label value
     *
     * @param environmentVariableId Environment variable id containing updated option.
     * @param oldValue Old option label
     * @param newValue New option label
     */
    @Override
    public void replaceAllExistingValuesByEvId(
            Long environmentVariableId, String oldValue, String newValue) {
        findAndReplaceBindingValues(
                environmentVariableId, Collections.singletonList(oldValue), newValue);
    }

    private void findAndReplaceBindingValues(
            Long environmentVariableId, List<String> oldValues, String newValue) {
        List<EnvironmentVariableBinding> environmentVariableBindings =
                environmentVariableBindingDao.findAllByEvIdAndValues(environmentVariableId, oldValues);
        environmentVariableBindings.forEach(binding -> binding.setValue(newValue));
    }
}
