/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.service.internal.display.campaign.CampaignDisplayServiceImpl.DEACTIVATED_USER_I18N_KEY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_FULL_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_PATH;

import java.util.Locale;
import java.util.Map;
import org.jooq.SelectFieldOrAsterisk;
import org.jooq.impl.DSL;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

public final class TestPlanGridHelpers {

    private TestPlanGridHelpers() {
        // This class is not meant to be instantiated
    }

    /*	Format the user infos as "LASTNAME (LOGIN)", "FIRSTNAME LASTNAME (LOGIN)", "LASTNAME (not_active)",
    "FIRSTNAME LASTNAME (not_active)"
    The deactivated message is replaced by locale message after request */
    public static SelectFieldOrAsterisk formatAssigneeFullName() {
        return DSL.when(
                        CORE_USER
                                .FIRST_NAME
                                .isNotNull()
                                .and(CORE_USER.FIRST_NAME.notEqual(""))
                                .and(CORE_USER.ACTIVE.isTrue()),
                        CORE_USER.FIRST_NAME.concat(
                                val(" "), CORE_USER.LAST_NAME, val(" ("), CORE_USER.LOGIN, val(")")))
                .when(
                        CORE_USER
                                .FIRST_NAME
                                .isNotNull()
                                .and(CORE_USER.FIRST_NAME.notEqual(""))
                                .and(CORE_USER.ACTIVE.isFalse()),
                        CORE_USER.FIRST_NAME.concat(val(" "), CORE_USER.LAST_NAME, val(" (not_active)")))
                .when(
                        CORE_USER
                                .FIRST_NAME
                                .isNull()
                                .and(CORE_USER.ACTIVE.isTrue())
                                .or(CORE_USER.FIRST_NAME.eq(""))
                                .and(CORE_USER.ACTIVE.isTrue()),
                        CORE_USER.LAST_NAME.concat(val(" ("), CORE_USER.LOGIN, val(")")))
                .otherwise(CORE_USER.LAST_NAME.concat(val(" (not_active)")))
                .as(ASSIGNEE_FULL_NAME);
    }

    public static void formatDeactivatedUserNameInRowData(DataRow row, MessageSource messageSource) {
        String assigneeFullName = RequestAliasesConstants.toCamelCase(ASSIGNEE_FULL_NAME);
        String rowUserName = (String) row.getData().get(assigneeFullName);

        if (rowUserName != null && rowUserName.contains("(not_active)")) {
            Locale locale = LocaleContextHolder.getLocale();
            String translatedMessage =
                    messageSource.getMessage(DEACTIVATED_USER_I18N_KEY, null, locale).toLowerCase();
            rowUserName = rowUserName.replace("not_active", translatedMessage);
            row.addData(assigneeFullName, rowUserName);
        }
    }

    public static void addTestCasePath(DataRow row, Map<Long, String> testCasePathById) {
        final Long rowTestCaseId =
                (Long) row.getData().get(RequestAliasesConstants.toCamelCase(TEST_CASE_ID));

        row.addData(
                RequestAliasesConstants.toCamelCase(TEST_CASE_PATH), testCasePathById.get(rowTestCaseId));
    }
}
