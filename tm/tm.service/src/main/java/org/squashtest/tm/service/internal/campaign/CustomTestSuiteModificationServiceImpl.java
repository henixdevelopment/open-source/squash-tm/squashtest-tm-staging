/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStatusReport;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;

@Transactional
@Service("CustomTestSuiteModificationService")
public class CustomTestSuiteModificationServiceImpl implements CustomTestSuiteModificationService {

    private static final String HAS_WRITE_PERMISSION_ID =
            "hasPermission(#suiteId, 'org.squashtest.tm.domain.campaign.TestSuite', 'WRITE') ";
    private static final String HAS_READ_PERMISSION_ID =
            "hasPermission(#suiteId, 'org.squashtest.tm.domain.campaign.TestSuite','READ') ";
    private static final String PERMISSION_EXECUTE_ITEM =
            "hasPermission(#testPlanItemId, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem', 'EXECUTE') ";

    @Inject private TestSuiteDao testSuiteDao;

    @Inject private IterationModificationService iterationService;

    @Override
    @PreAuthorize(HAS_WRITE_PERMISSION_ID + OR_HAS_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = TestSuite.class)
    public void rename(@Id long suiteId, String newName) throws DuplicateNameException {
        TestSuite suite = testSuiteDao.getReferenceById(suiteId);
        suite.rename(newName);
    }

    @Override
    public void updateExecutionStatus(Collection<TestSuite> tests) {
        List<Long> testIds = tests.stream().map(TestSuite::getId).toList();
        Map<Long, ExecutionStatusReport> reportsMap = testSuiteDao.getStatusReport(testIds);

        for (TestSuite test : tests) {
            ExecutionStatusReport report = reportsMap.get(test.getId());
            if (report != null) {
                ExecutionStatus newStatus = ExecutionStatus.computeNewStatus(report);
                test.setExecutionStatus(newStatus);
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize(HAS_READ_PERMISSION_ID + OR_HAS_ROLE_ADMIN)
    public TestSuite findById(long suiteId) {
        return testSuiteDao.getReferenceById(suiteId);
    }

    @Override
    @PreAuthorize(PERMISSION_EXECUTE_ITEM + OR_HAS_ROLE_ADMIN)
    public Execution addExecution(long testPlanItemId) {
        return iterationService.addExecution(testPlanItemId);
    }
}
