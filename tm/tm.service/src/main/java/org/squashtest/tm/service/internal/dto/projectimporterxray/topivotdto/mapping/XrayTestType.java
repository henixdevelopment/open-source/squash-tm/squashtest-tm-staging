/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping;

import java.util.Arrays;
import java.util.Objects;

public enum XrayTestType {
    MANUAL("manual"),
    CUCUMBER("cucumber"),
    GENERIC("generic"),
    UNSUPPORTED("unsupported-type");

    private final String name;

    XrayTestType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static XrayTestType getTestType(String status) {
        if (Objects.isNull(status)) {
            return UNSUPPORTED;
        }
        return Arrays.stream(XrayTestType.values())
                .filter(xrayTestType -> xrayTestType.getName().toLowerCase().contains(status.toLowerCase()))
                .findFirst()
                .orElse(UNSUPPORTED);
    }
}
