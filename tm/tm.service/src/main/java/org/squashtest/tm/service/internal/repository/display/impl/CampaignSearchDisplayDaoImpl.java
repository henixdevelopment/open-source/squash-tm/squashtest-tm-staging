/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.exists;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.inline;
import static org.jooq.impl.DSL.selectOne;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.project.AutomationWorkflowType.NONE;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.jenkins;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.squashOrchestrator;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;
import static org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus.AUTOMATED;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CAMPAIGN_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.HAS_BLOCKING_MILESTONE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITERATION_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LABEL;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_END_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_LABELS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUEST_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_SUITES;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.jooq.domain.tables.Project;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.CampaignSearchDisplayDao;

@Repository
public class CampaignSearchDisplayDaoImpl implements CampaignSearchDisplayDao {

    private static final String EXECUTION_MODE = "EXECUTION_EXECUTION_MODE";
    private final Project aliasProject = PROJECT.as("aliasProject");

    private final DSLContext jooq;

    public CampaignSearchDisplayDaoImpl(DSLContext jooq) {
        this.jooq = jooq;
    }

    @Override
    public GridResponse getRows(List<Long> itpiIds, GridRequest gridRequest) {
        GridResponse gridResponse = new GridResponse();

        jooq
                .select(getFields())
                .from(getTable())
                .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(itpiIds))
                .groupBy(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                        CAMPAIGN_LIBRARY_NODE.CLN_ID,
                        ITERATION.ITERATION_ID,
                        TEST_CASE_LIBRARY_NODE.TCLN_ID,
                        TEST_CASE.TCLN_ID,
                        DATASET.DATASET_ID,
                        PROJECT.PROJECT_ID,
                        EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID,
                        DSL.field(EXECUTION_MODE),
                        TEST_CASE.AUTOMATABLE,
                        AUTOMATION_REQUEST.REQUEST_STATUS,
                        CORE_USER.FIRST_NAME,
                        CORE_USER.LAST_NAME,
                        CORE_USER.LOGIN,
                        CORE_USER.PARTY_ID)
                .stream()
                .forEach(
                        record -> {
                            DataRow dataRow = new DataRow();
                            dataRow.setId(
                                    record.get(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ID)).toString());
                            dataRow.setProjectId(record.get(PROJECT.PROJECT_ID));
                            Map<String, Object> rawData = record.intoMap();
                            Map<String, Object> data = new HashMap<>();

                            // Using 'Collectors.toMap' won't work for entries with null values
                            for (Map.Entry<String, Object> entry : rawData.entrySet()) {
                                data.put(convertField(entry.getKey()), entry.getValue());
                            }
                            dataRow.setData(data);
                            gridResponse.addDataRow(dataRow);
                        });

        return gridResponse;
    }

    private List<Field<?>> getFields() {
        return Arrays.asList(
                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ID),
                ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY,
                ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON,
                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS,
                CAMPAIGN_LIBRARY_NODE.CLN_ID,
                CAMPAIGN_LIBRARY_NODE.NAME.as(CAMPAIGN_NAME),
                ITERATION.NAME.as(ITERATION_NAME),
                ITERATION.ITERATION_ID,
                TEST_CASE_LIBRARY_NODE.NAME.as(LABEL),
                TEST_CASE.IMPORTANCE,
                TEST_CASE.AUTOMATABLE,
                TEST_CASE.REFERENCE,
                TEST_CASE.TCLN_ID,
                groupConcat(MILESTONE.LABEL).orderBy(MILESTONE.LABEL).separator(", ").as(MILESTONE_LABELS),
                groupConcat(MILESTONE.STATUS).orderBy(MILESTONE.LABEL).separator(", ").as(MILESTONE_STATUS),
                groupConcat(MILESTONE.END_DATE)
                        .orderBy(MILESTONE.LABEL)
                        .separator(", ")
                        .as(MILESTONE_END_DATE),
                EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID,
                hasBlockingMilestone().as(HAS_BLOCKING_MILESTONE),
                getAutomationFields().as(EXECUTION_MODE),
                DATASET.NAME.as(DATASET_NAME),
                PROJECT.PROJECT_ID,
                PROJECT.NAME.as(PROJECT_NAME),
                when(TEST_CASE.AUTOMATABLE.eq(Y.name()), AUTOMATION_REQUEST.REQUEST_STATUS)
                        .otherwise("")
                        .as(REQUEST_STATUS),
                DSL.concat(
                                CORE_USER.FIRST_NAME,
                                inline(" "),
                                CORE_USER.LAST_NAME,
                                inline(" ("),
                                CORE_USER.LOGIN,
                                inline(")"))
                        .as(ASSIGNEE_LOGIN),
                CORE_USER.PARTY_ID.as(ASSIGNEE_ID),
                DSL.select(DSL.groupConcatDistinct(TEST_SUITE.NAME))
                        .from(TEST_SUITE)
                        .join(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
                        .where(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                        .asField(TEST_SUITES));
    }

    private Table<?> getTable() {

        return ITERATION_TEST_PLAN_ITEM
                .innerJoin(ITEM_TEST_PLAN_LIST)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .innerJoin(ITERATION)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(CAMPAIGN)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(PROJECT)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftJoin(TEST_CASE)
                .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(DATASET)
                .on(ITERATION_TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .leftJoin(CORE_USER)
                .on(ITERATION_TEST_PLAN_ITEM.USER_ID.eq(CORE_USER.PARTY_ID))
                .leftJoin(EXPLORATORY_SESSION_OVERVIEW)
                .on(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID))
                .leftJoin(MILESTONE_TEST_CASE)
                .on(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(MILESTONE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
                .leftJoin(aliasProject)
                .on(aliasProject.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .leftJoin(TEST_AUTOMATION_SERVER)
                .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(aliasProject.TA_SERVER_ID));
    }

    private String convertField(String fieldName) {
        Converter<String, String> converter =
                CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
        return converter.convert(fieldName);
    }

    private Field<Boolean> hasBlockingMilestone() {
        return exists(
                selectOne()
                        .from(MILESTONE_CAMPAIGN)
                        .innerJoin(MILESTONE)
                        .on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                        .where(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .and(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name())));
    }

    private Field<?> getAutomationFields() {
        return when(
                        TEST_CASE.EXECUTION_MODE.eq(TestCaseExecutionMode.EXPLORATORY.name()),
                        TestCaseExecutionMode.EXPLORATORY.name())
                .otherwise(
                        when(
                                        (aliasProject
                                                        .AUTOMATION_WORKFLOW_TYPE
                                                        .eq(NONE.name())
                                                        .or(
                                                                aliasProject
                                                                        .AUTOMATION_WORKFLOW_TYPE
                                                                        .ne(NONE.name())
                                                                        .and(TEST_CASE.AUTOMATABLE.eq(Y.name()))
                                                                        .and(AUTOMATION_REQUEST.REQUEST_STATUS.eq(AUTOMATED.name()))))
                                                // if Jenkins server, TestCase only has to be linked to an
                                                // automation test
                                                .and(
                                                        (TEST_AUTOMATION_SERVER
                                                                        .KIND
                                                                        .eq(jenkins.name())
                                                                        .and(TEST_CASE.TA_TEST.isNotNull()))
                                                                // if Squash Autom server, then the 3 automation attributes
                                                                // must exist
                                                                .or(
                                                                        TEST_AUTOMATION_SERVER
                                                                                .KIND
                                                                                .eq(squashOrchestrator.name())
                                                                                .and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
                                                                                .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
                                                                                .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull()))),
                                        TestCaseExecutionMode.AUTOMATED.name())
                                .otherwise(TestCaseExecutionMode.MANUAL.name()));
    }
}
