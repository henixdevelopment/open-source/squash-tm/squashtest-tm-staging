/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.tf;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.display.dto.UserView;

public class AutomationWorkspaceDataDto {

    private int nbAssignedAutomReq;
    private int nbAutomReqToTreat;
    private int nbTotal;
    private List<UserView> usersWhoModifiedTestCasesAssignView = new ArrayList<>();
    private List<UserView> usersWhoModifiedTestCasesTreatmentView = new ArrayList<>();
    private List<UserView> usersWhoModifiedTestCasesGlobalView = new ArrayList<>();
    private List<UserView> usersAssignedTo = new ArrayList<>();

    public List<UserView> getUsersWhoModifiedTestCasesAssignView() {
        return usersWhoModifiedTestCasesAssignView;
    }

    public void setUsersWhoModifiedTestCasesAssignView(
            List<UserView> usersWhoModifiedTestCasesAssignView) {
        this.usersWhoModifiedTestCasesAssignView = usersWhoModifiedTestCasesAssignView;
    }

    public List<UserView> getUsersWhoModifiedTestCasesTreatmentView() {
        return usersWhoModifiedTestCasesTreatmentView;
    }

    public void setUsersWhoModifiedTestCasesTreatmentView(
            List<UserView> usersWhoModifiedTestCasesTreatmentView) {
        this.usersWhoModifiedTestCasesTreatmentView = usersWhoModifiedTestCasesTreatmentView;
    }

    public List<UserView> getUsersWhoModifiedTestCasesGlobalView() {
        return usersWhoModifiedTestCasesGlobalView;
    }

    public void setUsersWhoModifiedTestCasesGlobalView(
            List<UserView> usersWhoModifiedTestCasesGlobalView) {
        this.usersWhoModifiedTestCasesGlobalView = usersWhoModifiedTestCasesGlobalView;
    }

    public int getNbAssignedAutomReq() {
        return nbAssignedAutomReq;
    }

    public void setNbAssignedAutomReq(int nbAssignedAutomReq) {
        this.nbAssignedAutomReq = nbAssignedAutomReq;
    }

    public int getNbAutomReqToTreat() {
        return nbAutomReqToTreat;
    }

    public void setNbAutomReqToTreat(int nbAutomReqToTreat) {
        this.nbAutomReqToTreat = nbAutomReqToTreat;
    }

    public int getNbTotal() {
        return nbTotal;
    }

    public void setNbTotal(int nbTotal) {
        this.nbTotal = nbTotal;
    }

    public List<UserView> getUsersAssignedTo() {
        return usersAssignedTo;
    }

    public void setUsersAssignedTo(List<UserView> usersAssignedTo) {
        this.usersAssignedTo = usersAssignedTo;
    }
}
