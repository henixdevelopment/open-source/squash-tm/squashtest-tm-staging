/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static java.util.Objects.nonNull;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.SelectConditionStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.repository.display.MultipleHierarchyTreeBrowserDao;

@Repository
public class MultipleHierarchyTreeBrowserDaoImpl implements MultipleHierarchyTreeBrowserDao {

    private static final String SIMPLE_CLASS_NAME = "SIMPLE_CLASS_NAME";
    private final DSLContext dsl;
    private final Map<NodeType, RelationshipDefinition> relationshipDefinitions;
    private final Map<NodeType, AncestorLookupDefinition> ancestorLookupDefinitions;

    public MultipleHierarchyTreeBrowserDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
        this.relationshipDefinitions =
                EnumSet.allOf(RelationshipDefinition.class).stream()
                        .collect(Collectors.toMap(RelationshipDefinition::getNodeType, Function.identity()));
        this.ancestorLookupDefinitions =
                EnumSet.allOf(AncestorLookupDefinition.class).stream()
                        // test suites and iterations cannot be handled like others relations
                        .filter(
                                def ->
                                        def != AncestorLookupDefinition.TEST_SUITES_ANCESTORS
                                                && def != AncestorLookupDefinition.ITERATION_ANCESTORS)
                        .collect(Collectors.toMap(AncestorLookupDefinition::getNodeType, Function.identity()));
    }

    public Set<NodeReference> findLibraryReferences(
            NodeWorkspace workspace, Collection<Long> projectIds) {
        return dsl
                .select(workspace.getColumnRef())
                .from(PROJECT)
                .where(PROJECT.PROJECT_TYPE.eq(Project.PROJECT_TYPE))
                .and(PROJECT.PROJECT_ID.in(projectIds))
                .fetch(workspace.getColumnRef())
                .stream()
                .map(id -> new NodeReference(workspace.getLibraryType(), id))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public ListMultimap<NodeReference, NodeReference> findChildrenReference(
            Set<NodeReference> parentReferences) {
        Map<NodeType, List<NodeReference>> parentReferencesByType =
                parentReferences.stream().collect(Collectors.groupingBy(NodeReference::getNodeType));
        ListMultimap<NodeReference, NodeReference> childrenReferences = ArrayListMultimap.create();

        for (Map.Entry<NodeType, List<NodeReference>> entry : parentReferencesByType.entrySet()) {
            NodeType nodeType = entry.getKey();
            Set<Long> ids =
                    entry.getValue().stream().map(NodeReference::getId).collect(Collectors.toSet());
            if (this.relationshipDefinitions.containsKey(nodeType)) {
                childrenReferences.putAll(
                        getChildrenReferences(relationshipDefinitions.get(nodeType), ids));
            } else {
                throw new UnsupportedOperationException(
                        "No relationship definition implemented for type : " + nodeType);
            }
        }
        return childrenReferences;
    }

    @Override
    public Set<NodeReference> findAncestors(NodeReferences nodeReferences) {
        Set<NodeReference> filteredReferences = nodeReferences.extractNonLibraries();
        if (filteredReferences.isEmpty()) {
            return new HashSet<>();
        }
        Map<NodeType, List<NodeReference>> nodeReferencesByType =
                filteredReferences.stream().collect(Collectors.groupingBy(NodeReference::getNodeType));
        HashSet<NodeReference> ancestors = new HashSet<>();

        // Handle the degenerated campaign tree here aka a fantastic tree with relationship data spread
        // across FOUR tables.
        // A tree is not complex enough, so we need to separate nodes relations on four tables... Genius
        // !
        List<NodeReference> testSuitesReferences =
                nodeReferencesByType.getOrDefault(NodeType.TEST_SUITE, new ArrayList<>());
        // fetch iteration ancestors of suites
        Set<NodeReference> iterationsFromSuites =
                getAncestorReferences(AncestorLookupDefinition.TEST_SUITES_ANCESTORS, testSuitesReferences);
        List<NodeReference> iterationsReferences =
                nodeReferencesByType.getOrDefault(NodeType.ITERATION, new ArrayList<>());
        // add to ancestors and to iterations that will be looked for ancestors.
        ancestors.addAll(iterationsFromSuites);
        iterationsReferences.addAll(iterationsFromSuites);
        // fetch campaign references for these iterations
        Set<NodeReference> campaignFromIterations =
                getAncestorReferences(AncestorLookupDefinition.ITERATION_ANCESTORS, iterationsReferences);
        // add to ancestors
        ancestors.addAll(campaignFromIterations);
        // add these campaigns to the originals campaigns
        List<NodeReference> campaigns =
                nodeReferencesByType.getOrDefault(NodeType.CAMPAIGN, new ArrayList<>());
        campaigns.addAll(campaignFromIterations);
        nodeReferencesByType.put(NodeType.CAMPAIGN, new ArrayList<>(campaigns));

        for (Map.Entry<NodeType, List<NodeReference>> entry : nodeReferencesByType.entrySet()) {
            NodeType nodeType = entry.getKey();
            if (this.ancestorLookupDefinitions.containsKey(nodeType)) {
                Set<NodeReference> ancestorReferences =
                        getAncestorReferences(ancestorLookupDefinitions.get(nodeType), entry.getValue());
                ancestors.addAll(ancestorReferences);
            }
        }
        return ancestors;
    }

    private Set<NodeReference> getAncestorReferences(
            AncestorLookupDefinition ancestorLookupDefinition, List<NodeReference> descendants) {
        if (descendants.isEmpty()) {
            return new HashSet<>();
        }
        Set<Long> descendantIds =
                descendants.stream().map(NodeReference::getId).collect(Collectors.toSet());
        SelectConditionStep<Record3<String, Long, Long>> selectQuery =
                craftSelectAncestorRequest(ancestorLookupDefinition, descendantIds);
        return buildAncestorsSet(ancestorLookupDefinition, descendants, selectQuery);
    }

    private Set<NodeReference> buildAncestorsSet(
            AncestorLookupDefinition ancestorLookupDefinition,
            List<NodeReference> descendants,
            SelectConditionStep<? extends Record> selectQuery) {
        HashSet<NodeReference> ancestors = new HashSet<>();
        selectQuery.forEach(
                tuple -> {
                    NodeType nodeType = NodeType.fromTypeName(tuple.get(SIMPLE_CLASS_NAME, String.class));
                    NodeReference candidate =
                            new NodeReference(nodeType, tuple.get(ancestorLookupDefinition.getAncestorField()));
                    if (!descendants.contains(candidate)) {
                        ancestors.add(candidate);
                    }
                    if (nonNull(ancestorLookupDefinition.getLibraryField())) {
                        Long libraryId = tuple.get(ancestorLookupDefinition.getLibraryField());
                        if (nonNull(libraryId)) {
                            ancestors.add(
                                    new NodeReference(
                                            ancestorLookupDefinition.getNodeType().getLibraryType(), libraryId));
                        }
                    }
                });

        return ancestors;
    }

    private SelectConditionStep<Record3<String, Long, Long>> craftSelectAncestorRequest(
            AncestorLookupDefinition ancestorLookupDefinition, Set<Long> descendantIds) {
        Stack<SelectConditionStep<Record3<String, Long, Long>>> requestPart = new Stack<>();
        ancestorLookupDefinition
                .getRelationshipIds()
                .forEach(
                        (key, field) -> {
                            SelectOnConditionStep<Record3<String, Long, Long>> initialJoin =
                                    dsl.select(
                                                    DSL.val(key.getTypeName()).as(SIMPLE_CLASS_NAME),
                                                    ancestorLookupDefinition.getAncestorField(),
                                                    ancestorLookupDefinition.getLibraryField())
                                            .from(ancestorLookupDefinition.getAncestorField().getTable())
                                            .innerJoin(field.getTable())
                                            .on(ancestorLookupDefinition.getAncestorField().eq(field));

                            if (Objects.nonNull(ancestorLookupDefinition.getLibraryField())) {
                                initialJoin =
                                        initialJoin
                                                .leftJoin(ancestorLookupDefinition.getLibraryContentField().getTable())
                                                .on(
                                                        ancestorLookupDefinition
                                                                .getLibraryContentField()
                                                                .eq(ancestorLookupDefinition.getAncestorField()));
                            }

                            ancestorLookupDefinition.addAdditionalJoins(initialJoin, key);

                            SelectConditionStep<Record3<String, Long, Long>> query =
                                    initialJoin.where(
                                            ancestorLookupDefinition.getDescendantField().in(descendantIds));

                            ancestorLookupDefinition.addAdditionalCondition(query, key);

                            requestPart.push(query);
                        });
        // first request part, we make a simple select
        SelectConditionStep<Record3<String, Long, Long>> selectQuery = requestPart.pop();

        // append the next steps while we have in stack as union clause
        while (!requestPart.empty()) {
            selectQuery.union(requestPart.pop());
        }
        return selectQuery;
    }

    private ListMultimap<NodeReference, NodeReference> getChildrenReferences(
            RelationshipDefinition relationshipDefinition, Set<Long> ancestorIds) {
        Stack<SelectConditionStep<Record4<String, Long, Long, Integer>>> requestPart = new Stack<>();
        String simpleClassName = SIMPLE_CLASS_NAME;
        relationshipDefinition
                .getRelationshipIds()
                .forEach(
                        (key, field) -> {
                            SelectOnConditionStep<Record4<String, Long, Long, Integer>> joinQuery =
                                    dsl.select(
                                                    DSL.val(key.getTypeName()).as(simpleClassName),
                                                    relationshipDefinition.getAncestorField(),
                                                    relationshipDefinition.getDescendantField(),
                                                    relationshipDefinition.getOrderField())
                                            .from(relationshipDefinition.getAncestorField().getTable())
                                            .innerJoin(field.getTable())
                                            .on(relationshipDefinition.getDescendantField().eq(field));
                            relationshipDefinition.addAdditionalJoins(joinQuery, key);
                            SelectConditionStep<Record4<String, Long, Long, Integer>> query =
                                    joinQuery.where(relationshipDefinition.getAncestorField().in(ancestorIds));
                            relationshipDefinition.addAdditionalCriteria(query, key);
                            requestPart.push(query);
                        });

        // first request part, we make a simple select
        SelectConditionStep<Record4<String, Long, Long, Integer>> selectQuery = requestPart.pop();

        // append the next steps while we have in stack as union clause
        while (!requestPart.empty()) {
            selectQuery.union(requestPart.pop());
        }

        // finally append the order close on the full union result
        selectQuery.orderBy(
                relationshipDefinition.getAncestorField(), relationshipDefinition.getOrderField());

        ListMultimap<NodeReference, NodeReference> childrenRef = ArrayListMultimap.create();
        selectQuery
                .fetch()
                .forEach(
                        tuple -> {
                            NodeType nodeType = NodeType.fromTypeName(tuple.get(simpleClassName, String.class));
                            NodeReference childRef =
                                    new NodeReference(
                                            nodeType, tuple.get(relationshipDefinition.getDescendantField()));
                            childrenRef.put(
                                    new NodeReference(
                                            relationshipDefinition.getNodeType(),
                                            tuple.get(relationshipDefinition.getAncestorField())),
                                    childRef);
                        });
        return childrenRef;
    }
}
