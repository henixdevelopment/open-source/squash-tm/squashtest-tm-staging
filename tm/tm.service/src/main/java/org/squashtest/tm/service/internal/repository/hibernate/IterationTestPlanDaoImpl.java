/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanExecution.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENTS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENT_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.AUTOMATED_TEST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.AUTOMATED_TEST_REFERENCE;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.AUTOMATED_TEST_TECHNOLOGY;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.AUTOMATION_REQUEST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CAMPAIGN;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.DATASET;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.DATASETS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ITERATION;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.NATURE;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.PARAMETER;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.PARAMETERS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.PARAMETER_VALUES;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.REFERENCED_TEST_CASE;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.SCM_REPOSITORY;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.SCM_SERVER;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.STEPS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_PLANS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_SUITES;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TYPE;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_DATASETS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_ITERATIONS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_ITPIS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_TEST_CASES_BY_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.TEST_CASE_IDS;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.hibernate.jpa.QueryHints;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.bdd.BddScriptLanguage;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.service.internal.repository.CustomIterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;

@Repository
public class IterationTestPlanDaoImpl implements CustomIterationTestPlanDao {

    private static final Condition TEST_CASE_HAS_AUTOMATED_STATUS =
            TEST_CASE
                    .AUTOMATABLE
                    .eq(TestCaseAutomatable.Y.name())
                    .and(
                            Tables.AUTOMATION_REQUEST.REQUEST_STATUS.eq(
                                    AutomationRequestStatus.AUTOMATED.name()));

    @PersistenceContext private EntityManager entityManager;

    @Inject private DSLContext dslContext;

    @Override
    public List<IterationTestPlanItem> findAllByIterationIdWithTCAutomated(Long iterationId) {
        Query q =
                entityManager.createNamedQuery("IterationTestPlanItem.findAllByIterationIdWithTCAutomated");
        q.setParameter("iterationId", iterationId);
        return q.getResultList();
    }

    @Override
    public List<IterationTestPlanItem> findAllByTestSuiteIdWithTCAutomated(Long testSuiteId) {
        Query q =
                entityManager.createNamedQuery("IterationTestPlanItem.findAllByTestSuiteIdWithTCAutomated");
        q.setParameter("testSuiteId", testSuiteId);
        return q.getResultList();
    }

    @Override
    public List<IterationTestPlanItem> findAllByItemsIdWithTCAutomated(List<Long> itemsIds) {
        Query q =
                entityManager.createNamedQuery("IterationTestPlanItem.findAllByItemsIdWithTCAutomated");
        q.setParameter("itemsIds", itemsIds);
        return q.getResultList();
    }

    @Override
    public List<IterationTestPlanItem> fetchForAutomatedExecutionCreation(
            Collection<Long> itemTestPlanIds) {
        List<IterationTestPlanItem> testPlanItems = fetchIterationTestPlanItems(itemTestPlanIds);

        // We order fetch ITPI according to itemTestPlanIds order because fetch request don't respect
        // that order depending on the database.
        Map<Long, IterationTestPlanItem> itemMap =
                testPlanItems.stream()
                        .collect(Collectors.toMap(IterationTestPlanItem::getId, item -> item));

        return itemTestPlanIds.stream().map(itemMap::get).toList();
    }

    private List<IterationTestPlanItem> fetchIterationTestPlanItems(
            Collection<Long> itemTestPlanIds) {

        List<IterationTestPlanItem> items =
                new EntityGraphQueryBuilder<>(
                                entityManager, IterationTestPlanItem.class, FIND_DISTINCT_ITPIS_BY_IDS)
                        .addAttributeNodes(TEST_SUITES, ITERATION, EXPLORATORY_SESSION_OVERVIEW)
                        .executeDistinctList(Collections.singletonMap(IDS, itemTestPlanIds));

        if (items.isEmpty()) {
            return Collections.emptyList();
        }

        loadItemsAttributes(items);

        return items;
    }

    private void loadItemsAttributes(List<IterationTestPlanItem> items) {
        Set<Long> tcIds =
                items.stream()
                        .map(item -> item.getReferencedTestCase().getId())
                        .collect(Collectors.toSet());

        Set<Long> datasetIds =
                items.stream()
                        .filter(item -> item.getReferencedDataset() != null)
                        .map(item -> item.getReferencedDataset().getId())
                        .collect(Collectors.toSet());

        Set<Long> iterationIds =
                items.stream()
                        .filter(item -> item.getIteration() != null)
                        .map(item -> item.getIteration().getId())
                        .collect(Collectors.toSet());

        new EntityGraphQueryBuilder<>(entityManager, TestCase.class, FIND_DISTINCT_TEST_CASES_BY_IDS)
                .addAttributeNodes(
                        PARAMETERS,
                        AUTOMATION_REQUEST,
                        AUTOMATED_TEST,
                        NATURE,
                        TYPE,
                        ATTACHMENT_LIST,
                        AUTOMATED_TEST_REFERENCE,
                        AUTOMATED_TEST_TECHNOLOGY,
                        DATASETS,
                        SCM_REPOSITORY,
                        STEPS)
                .addSubGraph(ATTACHMENT_LIST, ATTACHMENTS)
                .addSubGraph(SCM_REPOSITORY, SCM_SERVER)
                .executeDistinctList(Collections.singletonMap(IDS, tcIds));

        new EntityGraphQueryBuilder<>(entityManager, Dataset.class, FIND_DISTINCT_DATASETS_BY_IDS)
                .addAttributeNodes(PARAMETER_VALUES)
                .addSubGraph(PARAMETER_VALUES, DATASET, PARAMETER)
                .executeDistinctList(Collections.singletonMap(IDS, datasetIds));

        new EntityGraphQueryBuilder<>(entityManager, Iteration.class, FIND_DISTINCT_ITERATIONS_BY_IDS)
                .addAttributeNodes(CAMPAIGN, TEST_PLANS)
                .addSubGraph(TEST_PLANS, EXPLORATORY_SESSION_OVERVIEW)
                .addSubGraph(CAMPAIGN, "iterations")
                .executeDistinctList(Collections.singletonMap(IDS, iterationIds));

        // fetching the associated steps at least directly executables. For call steps it will be N+1...
        // but call step should be fairly rare in automated executions...
        fetchTestStepsForAutomatedExecutionCreation(tcIds);
    }

    private void fetchTestStepsForAutomatedExecutionCreation(Set<Long> testCaseIds) {
        entityManager
                .createNamedQuery("ActionTestSteps.fetchWithAttachmentReferences")
                .setParameter(TEST_CASE_IDS, testCaseIds)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public List<Long> findAllByIterationIdAndLogin(Long iterationId, String login) {
        Query q =
                entityManager.createNamedQuery("IterationTestPlanItem.findAllByIterationIdAndUserLogin");
        q.setParameter("iterationId", iterationId);
        q.setParameter("login", login);
        return q.getResultList();
    }

    @Override
    public List<Long> findAllByTestSuiteId(Long testSuiteId) {
        Query q =
                entityManager.createNamedQuery(
                        "IterationTestPlanItem.findAllByTestSuiteIdWithReferencedDataset");
        q.setParameter("testSuiteId", testSuiteId);
        return q.getResultList();
    }

    @Override
    public List<IterationTestPlanItem> findIterationTestPlanItemsToRemoveInDeleteTestSuite(
            List<TestSuite> suites, final List<Long> targetIds) {

        Query q =
                entityManager.createNamedQuery(
                        "IterationTestPlanItem.findIterationTestPlanItemsToRemoveInDeleteTestSuite");
        q.setParameter("suites", suites);
        q.setParameter("targetIds", targetIds);

        return q.getResultList();
    }

    @Override
    public List<IterationTestPlanItem> fetchWithServerByIds(Collection<Long> itemIds) {
        return new EntityGraphQueryBuilder<>(
                        entityManager, IterationTestPlanItem.class, FIND_DISTINCT_ITPIS_BY_IDS)
                .addAttributeNodes(
                        REFERENCED_TEST_CASE, ITERATION, TEST_SUITES, EXPLORATORY_SESSION_OVERVIEW)
                .addSubGraph(REFERENCED_TEST_CASE, SCM_REPOSITORY)
                .addSubgraphToSubgraph(REFERENCED_TEST_CASE, SCM_REPOSITORY, SCM_SERVER)
                .executeDistinctList(Map.of(IDS, itemIds));
    }

    /**
     * This request get ITPI ids whose iteration includes a test case that has been automated, either
     * by having the automated workflow activated, or the two automation related field of the test
     * case filled.
     *
     * @param iterationUUID a valid iteration UUID
     * @return a list of iteration test plan items
     */
    @Override
    public List<Long> getITPIsByIterationUUIDAndIsAutomated(String iterationUUID) {
        return entityManager
                .createQuery(
                        "select distinct itpi.id from Iteration it "
                                + "join it.testPlans itpi "
                                + "join itpi.referencedTestCase tc "
                                + "left join tc.automationRequest ar "
                                + "WHERE it.uuid = :uuid "
                                + "AND ("
                                + "(tc.project.allowAutomationWorkflow = TRUE "
                                + "AND tc.automatable = 'Y' "
                                + "AND ar.id = tc.automationRequest.id "
                                + "AND ar.requestStatus = 'AUTOMATED') "
                                + "OR "
                                + "tc.project.allowAutomationWorkflow = FALSE "
                                + ")"
                                + "AND ("
                                + "tc.scmRepository IS NOT NULL "
                                + "AND tc.automatedTestReference IS NOT NULL "
                                + "AND tc.automatedTestReference != '' "
                                + "AND tc.automatedTestTechnology IS NOT NULL) "
                                + "ORDER BY index(itpi)",
                        Long.class)
                .setParameter("uuid", iterationUUID)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    /**
     * This request get ITPI ids whose test suite includes a test case that has been automated, either
     * by having the automated workflow activated, or the two automation related fields of the test
     * case filled.
     *
     * @param testSuiteUUID a valid test suite UUID
     * @return a list of iteration test plan items
     */
    @Override
    public List<Long> getITPIsByTestSuiteUUIDAndIsAutomated(String testSuiteUUID) {
        return entityManager
                .createQuery(
                        "SELECT distinct itpi.id FROM TestSuite ts "
                                + "JOIN ts.testPlan itpi "
                                + "JOIN itpi.referencedTestCase tc "
                                + "LEFT JOIN tc.automationRequest ar "
                                + "WHERE ts.uuid = :uuid "
                                + "AND ("
                                + "(tc.project.allowAutomationWorkflow = TRUE "
                                + "AND tc.automatable = 'Y' "
                                + "AND ar.id = tc.automationRequest.id "
                                + "AND ar.requestStatus = 'AUTOMATED') "
                                + "OR "
                                + "tc.project.allowAutomationWorkflow = FALSE "
                                + ")"
                                + "AND ("
                                + "tc.scmRepository IS NOT NULL "
                                + "AND tc.automatedTestReference IS NOT NULL "
                                + "AND tc.automatedTestReference != '' "
                                + "AND tc.automatedTestTechnology IS NOT NULL)"
                                + "ORDER BY index(itpi)",
                        Long.class)
                .setParameter("uuid", testSuiteUUID)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public List<Long> filterSquashTfItemIds(List<Long> itemIds) {
        final Condition isSquashTFAutomated =
                TEST_AUTOMATION_SERVER
                        .KIND
                        .eq(TestAutomationServerKind.jenkins.name())
                        .and(TEST_CASE.TA_TEST.isNotNull());

        final Condition condition =
                (PROJECT
                                .ALLOW_AUTOMATION_WORKFLOW
                                .eq(Boolean.TRUE)
                                .and(TEST_CASE_HAS_AUTOMATED_STATUS)
                                .and(isSquashTFAutomated))
                        .or(PROJECT.ALLOW_AUTOMATION_WORKFLOW.eq(Boolean.FALSE).and(isSquashTFAutomated));

        return filterItemIds(itemIds, condition);
    }

    public List<Long> filterSquashOrchestratorItemIds(List<Long> itemIds) {
        final Condition isSquashOrchestratorAutomated =
                TEST_AUTOMATION_SERVER
                        .KIND
                        .eq(TestAutomationServerKind.squashOrchestrator.name())
                        .and(
                                TEST_CASE
                                        .AUTOMATED_TEST_TECHNOLOGY
                                        .isNotNull()
                                        .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.ne(""))
                                        .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull()));

        final Condition condition =
                (PROJECT
                                .ALLOW_AUTOMATION_WORKFLOW
                                .eq(Boolean.TRUE)
                                .and(TEST_CASE_HAS_AUTOMATED_STATUS)
                                .and(isSquashOrchestratorAutomated))
                        .or(
                                PROJECT
                                        .ALLOW_AUTOMATION_WORKFLOW
                                        .eq(Boolean.FALSE)
                                        .and(isSquashOrchestratorAutomated));

        return filterItemIds(itemIds, condition);
    }

    private List<Long> filterItemIds(List<Long> itemIds, Condition condition) {
        return dslContext
                .select(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
                .from(ITERATION_TEST_PLAN_ITEM)
                .innerJoin(TEST_CASE)
                .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(Tables.AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(Tables.AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftJoin(TEST_AUTOMATION_SERVER)
                .on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
                .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(itemIds).and(condition))
                .fetchInto(Long.class);
    }

    @Override
    public Map<Long, Integer> getNextTestPlanExecutionOrders(Collection<Long> testPlanIds) {

        return dslContext
                .select(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
                        DSL.coalesce(DSL.max(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER).plus(1), 0).as("ORDER"))
                .from(ITERATION_TEST_PLAN_ITEM)
                .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(
                        ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(testPlanIds))
                .groupBy(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
                .fetchMap(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, DSL.field("ORDER", Integer.class));
    }

    @Override
    public BddScriptLanguage findProjectBddScriptLanguageByIterationTestPlanItemId(
            long testPlanItemId) {
        return dslContext
                .select(PROJECT.BDD_SCRIPT_LANGUAGE)
                .from(PROJECT)
                .join(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .join(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(testPlanItemId))
                .fetchOneInto(BddScriptLanguage.class);
    }

    @Override
    public void loadForExecutionCreation(Long itemId) {
        entityManager.createQuery("""
                select itpi
                from IterationTestPlanItem itpi
                join fetch itpi.iteration
                join fetch itpi.referencedTestCase tc
                left join fetch itpi.testSuites
                left join fetch tc.datasets
                left join fetch tc.steps st
                left join fetch st.attachmentList sal
                left join fetch sal.attachments
                left join fetch tc.nature
                left join fetch tc.type
                left join fetch tc.attachmentList al
                left join fetch al.attachments
                left join fetch itpi.exploratorySessionOverview
                left join fetch itpi.referencedDataset ds
                left join fetch ds.parameterValues pv
                left join fetch pv.parameter
                where itpi.id = :id""", IterationTestPlanItem.class)
                .setParameter(ID, itemId)
                .getSingleResult();
    }
}
