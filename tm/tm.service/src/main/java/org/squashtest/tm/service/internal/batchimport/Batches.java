/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport;

import com.google.common.collect.Lists;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Batches<T> {

    private final Map<Long, Batch<T>> batch = new HashMap<>();

    public void addBatch(Long targetId, T entity) {
        batch.computeIfAbsent(targetId, k -> new Batch<>(targetId)).addEntity(entity);
    }

    public void addBatch(Long targetId, List<T> entities) {
        batch.computeIfAbsent(targetId, k -> new Batch<>(targetId)).addEntities(entities);
    }

    public List<T> getEntitiesByTargetId(Long parentId) {
        return batch.get(parentId).getEntities();
    }

    public Set<Long> getTargetIds() {
        return batch.keySet();
    }

    public boolean contains(Long targetId) {
        return batch.containsKey(targetId);
    }

    public boolean isEmpty() {
        return batch.isEmpty();
    }

    public List<List<Batch<T>>> partition(int partition) {
        return Lists.partition(batch.values().stream().toList(), partition);
    }
}
