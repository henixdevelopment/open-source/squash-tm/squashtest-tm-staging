/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.testplanretriever;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.testautomation.testplanretriever.CustomFieldValuesForExec;

@Service
@Transactional
public class RestAutomatedSuiteManagerService<C extends CustomFieldValuesForExec> {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(RestAutomatedSuiteManagerService.class);

    @Inject private AutomatedSuiteDao automatedSuiteDao;

    @Inject private PermissionEvaluationService permissionService;

    @Inject private AutomatedTestBuilderService<C> testBuilder;

    @Inject private CredentialsProvider credentialsProvider;

    @Inject private EntityPathHeaderDao pathHeaderDao;

    /**
     * Creates a test list with parameters (dataset) from an automated suite.
     *
     * @param items
     */
    public Collection<Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>>
            prepareExecutionOrder(List<IterationTestPlanItem> items, Map<Long, Long> itemExecutionMap) {
        if (!permissionService.hasRole(Roles.ROLE_TA_API_CLIENT)) {
            throw new AccessDeniedException("Access is denied");
        }
        return collectAutomatedTests(items, itemExecutionMap);
    }

    /**
     * Creates an empty automated suite linked to the given {@link Iteration}.
     *
     * @param iteration the {@link Iteration} to link with the automated suite
     * @return an empty {@link AutomatedSuite} link to the given {@link Iteration}
     */
    public AutomatedSuite createAutomatedSuiteLinkedToIteration(Iteration iteration) {
        return createAutomatedSuiteLinkedToIterationAndClearSession(iteration);
    }

    /**
     * Creates an empty automated suite linked to the given {@link TestSuite}.
     *
     * @param testSuite the {@link TestSuite} to link with the automated suite
     * @return an empty {@link AutomatedSuite} link to the given {@link TestSuite}
     */
    public AutomatedSuite createAutomatedSuiteLinkedToTestSuite(TestSuite testSuite) {
        return createAutomatedSuiteLinkedToTestSuiteAndClearSession(testSuite);
    }

    private AutomatedSuite createAutomatedSuiteLinkedToIterationAndClearSession(Iteration iteration) {
        return automatedSuiteDao.createNewSuiteWithLinkToIteration(iteration);
    }

    private AutomatedSuite createAutomatedSuiteLinkedToTestSuiteAndClearSession(TestSuite testSuite) {
        return automatedSuiteDao.createNewSuiteWithLinkToTestSuite(testSuite);
    }

    private Collection<Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>>
            collectAutomatedTests(
                    Collection<IterationTestPlanItem> items, Map<Long, Long> itemExecutionMap) {
        Collection<Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>> tests =
                new ArrayList<>();
        CustomFieldValuesForExec customFieldValuesForExec = testBuilder.fetchCustomFieldValues(items);

        Map<Long, String> pathById = getTestCasePathByIds(items);
        Map<Long, Credentials> scmServerCredentialsMap = getScmServerCredentialsMap(items);

        for (IterationTestPlanItem item : items) {
            Credentials credentials = null;

            ScmRepository scmRepository = item.getReferencedTestCase().getScmRepository();

            if (scmRepository != null) {
                if (scmRepository.getScmServer().isCredentialsNotShared()) {
                    LOGGER.info(
                            "Do not share Git credentials for the execution environment for the SCM server : {}. Please make sure you have correctly configured Git credentials in the execution environment.",
                            scmRepository.getScmServer().getFriendlyName());
                } else {
                    credentials = scmServerCredentialsMap.get(scmRepository.getScmServer().getId());
                }
            }

            String testCasePath = pathById.get(item.getReferencedTestCase().getId());
            Long executionId = itemExecutionMap.get(item.getId());

            tests.add(
                    testBuilder.createTestWithParams(
                            item, (C) customFieldValuesForExec, credentials, executionId, testCasePath));
        }
        return tests;
    }

    private Map<Long, String> getTestCasePathByIds(Collection<IterationTestPlanItem> items) {
        Set<Long> testCaseIds =
                items.stream()
                        .map(item -> item.getReferencedTestCase().getId())
                        .collect(Collectors.toSet());

        return pathHeaderDao.buildTestCasePathByIds(testCaseIds);
    }

    private Map<Long, Credentials> getScmServerCredentialsMap(
            Collection<IterationTestPlanItem> itpis) {

        Map<Long, ScmServer> scmServerMap =
                itpis.stream()
                        .filter(item -> item.getReferencedTestCase().getScmRepository() != null)
                        .map(item -> item.getReferencedTestCase().getScmRepository().getScmServer())
                        .collect(
                                Collectors.toMap(
                                        ScmServer::getId,
                                        server -> server,
                                        (existing, replacement) -> existing,
                                        LinkedHashMap::new));

        Map<Long, Credentials> scmServerCredentialsMap = new HashMap<>();

        scmServerMap
                .values()
                .forEach(
                        server -> {
                            Optional<Credentials> maybeCredentials =
                                    credentialsProvider.getAppLevelCredentials(server);
                            scmServerCredentialsMap.put(server.getId(), maybeCredentials.orElse(null));
                        });

        return scmServerCredentialsMap;
    }
}
