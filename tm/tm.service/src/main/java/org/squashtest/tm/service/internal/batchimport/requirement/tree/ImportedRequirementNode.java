/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.tree;

import static org.squashtest.tm.service.internal.batchimport.Existence.EXISTS;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.internal.batchimport.Existence;
import org.squashtest.tm.service.internal.batchimport.RequirementVersionModel;
import org.squashtest.tm.service.internal.batchimport.TargetStatus;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementVersionImportDto;

public final class ImportedRequirementNode {

    private final String name;
    private final RequirementTarget target;
    private NodeType type = NodeType.REQUIREMENT;
    private TargetStatus status;
    private final Map<String, Set<ImportedRequirementNode>> contents = new HashMap<>();
    private ImportedRequirementNode parent;

    private SortedMap<Integer, RequirementVersionModel> requirementVersions;
    private Set<String> milestonesInVersion;

    private ImportedRequirementNode(String name, RequirementTarget target, TargetStatus status) {
        this.name = name;
        this.target = target;
        this.status = status;
        this.milestonesInVersion = new HashSet<>();
        this.requirementVersions = new TreeMap<>();
    }

    private ImportedRequirementNode(
            String name, RequirementTarget target, TargetStatus status, NodeType type) {
        this.name = name;
        this.target = target;
        this.status = status;
        this.type = type;
    }

    public static ImportedRequirementNode createRequirementNode(
            RequirementTarget target, TargetStatus status) {
        return new ImportedRequirementNode(target.getName(), target, status);
    }

    public static ImportedRequirementNode createRequirementFolderNode(
            RequirementTarget target, TargetStatus status) {
        return new ImportedRequirementNode(target.getName(), target, status, NodeType.FOLDER);
    }

    public static ImportedRequirementNode createRequirementLibraryNode(String projectName, Long id) {
        return new ImportedRequirementNode(
                projectName,
                new RequirementTarget(projectName),
                new TargetStatus(Existence.EXISTS, id),
                NodeType.LIBRARY);
    }

    public void setNotExists(Integer version) {
        requirementVersions.put(version, new RequirementVersionModel(TargetStatus.NOT_EXISTS));
    }

    public boolean isRequirement() {
        return NodeType.REQUIREMENT.equals(this.type);
    }

    public boolean isRequirementFolder() {
        return NodeType.FOLDER.equals(this.type);
    }

    public boolean isLibrary() {
        return NodeType.LIBRARY.equals(this.type);
    }

    TargetStatus getStatus() {
        return status;
    }

    boolean versionAlreadyLoaded(Integer versionNo) {
        return requirementVersions.containsKey(versionNo);
    }

    TargetStatus getVersionStatus(Integer versionNo) {
        RequirementVersionModel versionModel = requirementVersions.get(versionNo);
        if (versionModel != null) {
            return versionModel.getStatus();
        }

        return null; // NPE is better than a false information from tree
    }

    void addVersion(Integer noVersion, TargetStatus status) {
        requirementVersions.put(noVersion, new RequirementVersionModel(status));
    }

    public void updateVersionStatus(Integer noVersion, TargetStatus status) {
        requirementVersions.get(noVersion).setStatus(status);
    }

    boolean isMilestoneUsedByOneVersion(String milestone) {
        return milestonesInVersion.contains(milestone);
    }

    boolean isVersionMilestoneLocked(Integer noVersion) {
        return requirementVersions.get(noVersion).isMilestoneLocked();
    }

    void bindMilestoneToVersion(Integer noVersion, String milestone) {
        if (!isMilestoneUsedByOneVersion(milestone)) {
            RequirementVersionModel rvModel = requirementVersions.get(noVersion);
            rvModel.addMilestone(milestone);
            milestonesInVersion.add(milestone);
        }
    }

    public void setVersionMilestoneLocked(Integer noVersion) {
        RequirementVersionModel rvModel = requirementVersions.get(noVersion);
        rvModel.setMilestoneLocked(true);
    }

    public RequirementTarget getTarget() {
        return target;
    }

    public void addContent(ImportedRequirementNode child) {
        if (child.parent != null) {
            return;
        }
        contents.computeIfAbsent(child.getName(), k -> new HashSet<>()).add(child);
        child.addParent(this);
    }

    private void addParent(ImportedRequirementNode parent) {
        this.parent = parent;
    }

    public String getName() {
        return this.name;
    }

    public boolean isSynchronized() {
        return this.target.isSynchronized();
    }

    public Long getId() {
        return this.status.getId();
    }

    public List<ImportedRequirementNode> getContents() {
        return this.contents.values().stream().flatMap(Collection::stream).toList();
    }

    public boolean exist() {
        return getId() != null;
    }

    public void updateStatus(TargetStatus status) {
        this.status = status;
        for (ImportedRequirementNode child : getContents()) {
            child.updateStatus(status);
        }
    }

    public boolean isCurrentVersion(Integer version) {
        if (requirementVersions.isEmpty()) {
            return true;
        }

        return requirementVersions.lastKey().equals(version);
    }

    public int fixVersion() {
        int previousKey = 0;

        for (int key : requirementVersions.keySet()) {
            if (key <= 0) {
                continue;
            }

            if (key > previousKey + 1) {
                return previousKey + 1;
            }
            previousKey = key;
        }

        return previousKey + 1;
    }

    public void deleteVersion(int deprecated) {
        requirementVersions.remove(deprecated);
    }

    public boolean existPriorVersion(Integer version) {
        return requirementVersions.containsKey(version - 1);
    }

    public RequirementVersionImportDto toRequirementVersionImportDto(Project project) {
        RequirementVersion requirementVersion = new RequirementVersion();
        requirementVersion.setName(name);
        requirementVersion.setDescription("");
        requirementVersion.setCategory(project.getRequirementCategories().getDefaultItem());

        return new RequirementVersionImportDto(
                requirementVersion,
                Collections.emptyMap(),
                Collections.emptyList(),
                false,
                requirementVersion.getStatus());
    }

    public boolean isHighLevelRequirement() {
        return target.isHighLevel();
    }

    public boolean isRequirementChild() {
        return parent.isRequirement();
    }

    public Long getRemoteSynchronisationId() {
        return target.getRemoteSynchronisationId();
    }

    public String getRemoteKey() {
        return target.getRemoteKey();
    }

    public Long getVersionId(int version) {
        RequirementVersionModel versionModel = requirementVersions.get(version);
        if (versionModel == null) {
            return null;
        }

        if (versionModel.getStatus().getStatus() == EXISTS) {
            return versionModel.getStatus().getId();
        }

        return null;
    }

    public void fix() {
        if (isRequirementFolder()) {
            this.type = NodeType.REQUIREMENT;
            this.milestonesInVersion = new HashSet<>();
            this.requirementVersions = new TreeMap<>();
            for (ImportedRequirementNode child : getContents()) {
                child.fix();
            }
        }
    }

    private enum NodeType {
        LIBRARY,
        FOLDER,
        REQUIREMENT
    }
}
