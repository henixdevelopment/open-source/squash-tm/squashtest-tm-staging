/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.service.deletion.BoundToNotSelectedTestSuite;
import org.squashtest.tm.service.deletion.NotDeletableCampaignsPreviewReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.campaign.LockedCampaignNodeDetectionService;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.CampaignDeletionDao;
import org.squashtest.tm.service.internal.repository.display.CampaignDisplayDao;
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Service
public class LockedCampaignNodeDetectionServiceImpl extends AbstractLockedNodeDetectionService
        implements LockedCampaignNodeDetectionService {
    private static final String EXTENDED_DELETE = "EXTENDED_DELETE";
    private static final String CAMPAIGNS_TYPE = "campaigns";

    private final CampaignDeletionDao deletionDao;
    private final CampaignDao campaignDao;
    private final CampaignDisplayDao campaignDisplayDao;
    private final IterationDisplayDao iterationDisplayDao;
    private final TestSuiteDisplayDao suiteDisplayDao;
    private final PermissionEvaluationService permissionEvaluationService;
    private final SprintDisplayDao sprintDisplayDao;

    @Autowired
    public LockedCampaignNodeDetectionServiceImpl(
            ActiveMilestoneHolder activeMilestoneHolder,
            CampaignDeletionDao deletionDao,
            CampaignDao campaignDao,
            CampaignDisplayDao campaignDisplayDao,
            IterationDisplayDao iterationDisplayDao,
            TestSuiteDisplayDao suiteDisplayDao,
            PermissionEvaluationService permissionEvaluationService,
            SprintDisplayDao sprintDisplayDao) {
        super(activeMilestoneHolder);
        this.deletionDao = deletionDao;
        this.campaignDao = campaignDao;
        this.campaignDisplayDao = campaignDisplayDao;
        this.iterationDisplayDao = iterationDisplayDao;
        this.suiteDisplayDao = suiteDisplayDao;
        this.permissionEvaluationService = permissionEvaluationService;
        this.sprintDisplayDao = sprintDisplayDao;
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedCampaigns(List<Long> nodeIds) {
        final List<NamedReference> campaigns = campaignDisplayDao.findNamedReferences(nodeIds);
        final Map<Long, List<Long>> executionIdsByCampaignIds =
                campaignDisplayDao.findExecutionIdsByCampaignIds(nodeIds);
        return getReportToDetectLockedEntity(
                campaigns, executionIdsByCampaignIds, Campaign.SIMPLE_CLASS_NAME);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedSprints(List<Long> nodeIds) {
        final List<NamedReference> sprintNamedReferences =
                sprintDisplayDao.findNamedReferences(nodeIds);
        final Map<Long, List<Long>> executionIdsBySprintIds =
                sprintDisplayDao.findExecutionIdsBySprintIds(nodeIds);
        return getReportToDetectLockedEntity(
                sprintNamedReferences, executionIdsBySprintIds, Sprint.SIMPLE_CLASS_NAME);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedIteration(List<Long> nodeIds) {
        final List<NamedReference> iterationNamedReferences =
                iterationDisplayDao.findNamedReferences(nodeIds);
        final Map<Long, List<Long>> executionIdsByIterationIds =
                iterationDisplayDao.findExecutionIdsByIterationIds(nodeIds);
        return getReportToDetectLockedEntity(
                iterationNamedReferences, executionIdsByIterationIds, Iteration.SIMPLE_CLASS_NAME);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedSuites(List<Long> nodeIds) {
        final List<NamedReference> suiteNamedReferences = suiteDisplayDao.findNamedReferences(nodeIds);
        final Map<Long, List<Long>> executionIdsBySuiteIds =
                suiteDisplayDao.findExecutionIdsBySuiteIds(nodeIds);
        return getReportToDetectLockedEntity(
                suiteNamedReferences, executionIdsBySuiteIds, TestSuite.SIMPLE_CLASS_NAME);
    }

    private List<SuppressionPreviewReport> getReportToDetectLockedEntity(
            List<NamedReference> namedReferences,
            Map<Long, List<Long>> executionIdsByEntityIds,
            String className) {
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();
        for (NamedReference namedReference : namedReferences) {
            final List<Long> executionIds =
                    executionIdsByEntityIds.getOrDefault(namedReference.getId(), Collections.emptyList());

            if (!executionIds.isEmpty()) {
                final boolean canExtendedDelete =
                        permissionEvaluationService.hasRoleOrPermissionOnObject(
                                Roles.ROLE_ADMIN, EXTENDED_DELETE, namedReference.getId(), className);
                final NotDeletableCampaignsPreviewReport report = new NotDeletableCampaignsPreviewReport();
                report.addName(namedReference.getName());
                report.setHasRights(canExtendedDelete);
                if (!canExtendedDelete) {
                    report.addLockedNode(namedReference.getId());
                }
                reportList.add(report);
            }
        }
        return reportList;
    }

    @Override
    public List<SuppressionPreviewReport> detectTestPlanItemBoundMultipleTestSuite(
            List<Long> nodeIds) {
        final List<SuppressionPreviewReport> reportList = new ArrayList<>();

        // report : Check that test plan item do not belong to other suite that is not in the selection
        // deletion : multiple test plan item bound to multiple test suite can be deleted
        if (containTestPlanItemThatBelongToOtherTestSuite(new HashSet<>(nodeIds))) {
            reportList.add(new BoundToNotSelectedTestSuite());
        }
        return reportList;
    }

    private boolean containTestPlanItemThatBelongToOtherTestSuite(Set<Long> testSuiteIds) {
        final Map<Long, Set<Long>> otherSuites = suiteDisplayDao.findLinkedSuites(testSuiteIds);

        for (Map.Entry<Long, Set<Long>> entry : otherSuites.entrySet()) {
            if (!testSuiteIds.containsAll(entry.getValue())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedByMilestone(List<Long> nodeIds) {
        return super.detectLockedByMilestone(nodeIds, CAMPAIGNS_TYPE);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedByMilestoneIteration(List<Long> nodeIds) {
        final List<Long> campaignIds =
                new ArrayList<>(iterationDisplayDao.findCampaignIdsByIterationIds(new HashSet<>(nodeIds)));
        return detectLockedByMilestone(campaignIds);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedByMilestoneTestSuite(List<Long> nodeIds) {
        final List<Long> campaignIds =
                new ArrayList<>(suiteDisplayDao.findCampaignIdsBySuiteIds(new HashSet<>(nodeIds)));
        return detectLockedByMilestone(campaignIds);
    }

    @Override
    public List<SuppressionPreviewReport> detectLockedWithActiveMilestone(List<Long> nodeIds) {
        return super.detectLockedWithActiveMilestone(nodeIds, CAMPAIGNS_TYPE);
    }

    @Override
    protected List<Long> findNodesWhichMilestonesForbidsDeletion(List<Long> nodeIds) {
        return deletionDao.findCampaignsWhichMilestonesForbidsDeletion(nodeIds);
    }

    @Override
    protected void addAdditionalReportWithActiveMilestoneToReportList(
            List<Long> nodeIds,
            Long activeMilestoneId,
            List<SuppressionPreviewReport> reportList,
            String reportType) {
        List<Long> nonBoundNodes = campaignDao.findNonBoundCampaign(nodeIds, activeMilestoneId);
        addNonBoundNodesWithActiveMilestoneToReportList(nonBoundNodes, reportList, reportType);
    }

    @Override
    protected List<Long>[] getIdsSeparateFolderFromNodeIds(List<Long> nodeIds) {
        return deletionDao.separateFolderFromCampaignIds(nodeIds);
    }

    @Override
    protected List<Long> findNodeIdsHavingMultipleMilestones(List<Long> nodeIds) {
        return campaignDao.findCampaignIdsHavingMultipleMilestones(nodeIds);
    }
}
