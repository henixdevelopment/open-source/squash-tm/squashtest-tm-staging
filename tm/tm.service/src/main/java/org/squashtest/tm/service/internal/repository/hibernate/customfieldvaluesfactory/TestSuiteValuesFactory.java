/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory;

import static org.squashtest.tm.domain.customfield.InputType.NUMERIC;
import static org.squashtest.tm.domain.customfield.InputType.RICH_TEXT;
import static org.squashtest.tm.domain.customfield.InputType.TAG;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_ENTITY_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.FIELD_TYPE;

import java.math.BigDecimal;
import org.jooq.DSLContext;
import org.jooq.Record8;
import org.jooq.Select;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;

@Repository
public class TestSuiteValuesFactory extends AbstractCustomFieldValuesFactory {

    protected TestSuiteValuesFactory(DSLContext dsl) {
        super(dsl);
    }

    @Override
    Select<Record8<Long, String, Long, String, String, BigDecimal, String, Long>> buildSelectClause(
            CustomFieldBinding binding) {
        return DSL.select(
                        CUSTOM_FIELD_BINDING.CFB_ID,
                        CUSTOM_FIELD_BINDING.BOUND_ENTITY,
                        CUSTOM_FIELD_BINDING.CF_ID,
                        CUSTOM_FIELD.DEFAULT_VALUE,
                        CUSTOM_FIELD.LARGE_DEFAULT_VALUE,
                        CUSTOM_FIELD.NUMERIC_DEFAULT_VALUE,
                        DSL.when(CUSTOM_FIELD.INPUT_TYPE.eq(String.valueOf(RICH_TEXT)), DSL.inline("RTF"))
                                .when(CUSTOM_FIELD.INPUT_TYPE.eq(String.valueOf(TAG)), DSL.inline("TAG"))
                                .when(CUSTOM_FIELD.INPUT_TYPE.eq(String.valueOf(NUMERIC)), DSL.inline("NUM"))
                                .otherwise(DSL.inline("CF"))
                                .as(FIELD_TYPE),
                        ITERATION_TEST_SUITE.TEST_SUITE_ID.as(BOUND_ENTITY_ID))
                .from(CUSTOM_FIELD_BINDING)
                .innerJoin(CUSTOM_FIELD)
                .on(CUSTOM_FIELD.CF_ID.eq(CUSTOM_FIELD_BINDING.CF_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID))
                .innerJoin(CAMPAIGN_LIBRARY_NODE)
                .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(CAMPAIGN)
                .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .innerJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .innerJoin(ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .innerJoin(ITERATION_TEST_SUITE)
                .on(ITERATION_TEST_SUITE.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .where(CUSTOM_FIELD_BINDING.CFB_ID.eq(binding.getId()));
    }
}
