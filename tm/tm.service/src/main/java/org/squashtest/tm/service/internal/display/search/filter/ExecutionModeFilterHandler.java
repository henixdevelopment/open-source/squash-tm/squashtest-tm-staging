/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.campaign.QIterationTestPlanItem;
import org.squashtest.tm.domain.execution.QExecution;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

@Component
public class ExecutionModeFilterHandler implements FilterHandler {

    private final Set<String> handledPrototypes = Sets.newHashSet("EXECUTION_EXECUTION_MODE");

    @Override
    public boolean canHandleFilter(GridFilterValue filter) {
        return this.handledPrototypes.contains(filter.getColumnPrototype());
    }

    @Override
    public void handleFilter(
            ExtendedHibernateQuery<?> query, GridFilterValue filter, GridRequest gridRequest) {
        QIterationTestPlanItem outerItpi = QIterationTestPlanItem.iterationTestPlanItem;

        QIterationTestPlanItem initItpi = new QIterationTestPlanItem("initItpi");
        QExecution execution = new QExecution("execution");
        List<String> values = filter.getValues();

        List<BooleanExpression> expressions = new ArrayList<>();

        for (String value : values) {
            if ("UNDEFINED".equals(value)) {
                expressions.add(execution.executionMode.isNull());
            } else {
                expressions.add(execution.executionMode.eq(TestCaseExecutionMode.valueOf(value)));
            }
        }

        HibernateQuery<?> subQuery =
                new ExtendedHibernateQuery<>()
                        .select(Expressions.ONE)
                        .from(initItpi)
                        .leftJoin(initItpi.executions, execution)
                        .where(
                                initItpi
                                        .id
                                        .eq(outerItpi.id)
                                        .and(
                                                Expressions.anyOf(
                                                        expressions.toArray(new BooleanExpression[expressions.size()]))));
        query.where(subQuery.exists());
    }
}
