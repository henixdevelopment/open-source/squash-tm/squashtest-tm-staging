/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/** Utility class to compare parameters of two test cases for dataset duplication. */
public final class ParameterComparator {

    private ParameterComparator() {
        // Utility class
    }

    public static boolean checkHasMatchingParameters(
            List<String> sourceDatasetParameters, List<String> targetDatasetParameters) {
        if (targetDatasetParameters.isEmpty()) {
            return true;
        }

        if (sourceDatasetParameters.isEmpty()) {
            return false;
        }

        if (sourceDatasetParameters.size() != targetDatasetParameters.size()) {
            return false;
        }

        Map<String, Long> sourceMap =
                sourceDatasetParameters.stream()
                        .collect(Collectors.groupingBy(name -> name, Collectors.counting()));

        Map<String, Long> targetMap =
                targetDatasetParameters.stream()
                        .collect(Collectors.groupingBy(name -> name, Collectors.counting()));

        return sourceMap.equals(targetMap);
    }
}
