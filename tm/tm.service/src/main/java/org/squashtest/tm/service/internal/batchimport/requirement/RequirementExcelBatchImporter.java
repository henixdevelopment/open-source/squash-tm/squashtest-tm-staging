/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement;

import static org.squashtest.tm.service.importer.EntityType.COVERAGE;
import static org.squashtest.tm.service.importer.EntityType.LINKED_LOW_LEVEL_REQ;
import static org.squashtest.tm.service.importer.EntityType.REQUIREMENT_LINK;
import static org.squashtest.tm.service.importer.EntityType.REQUIREMENT_VERSION;

import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.importer.EntityType;
import org.squashtest.tm.service.internal.batchimport.column.ExcelWorkbookParser;
import org.squashtest.tm.service.internal.batchimport.excel.ExcelBatchImporter;
import org.squashtest.tm.service.internal.batchimport.instruction.container.CoverageInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.Importer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.LinkedLowLevelRequirementInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.RequirementLinkInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.RequirementVersionInstructionContainer;

@Component
public class RequirementExcelBatchImporter extends ExcelBatchImporter {

    private static final List<EntityType> REQ_ENTITIES_ORDERED_BY_INSTRUCTION_ORDER =
            Arrays.asList(REQUIREMENT_VERSION, COVERAGE, REQUIREMENT_LINK, LINKED_LOW_LEVEL_REQ);

    public RequirementExcelBatchImporter() {
        super(LoggerFactory.getLogger(RequirementExcelBatchImporter.class));
    }

    @Override
    public void addInstructionsByEntity(
            ExcelWorkbookParser parser, EntityType entityType, Importer importer) {

        LOGGER.debug("creating instructions container for entity type : {}", entityType);

        switch (entityType) {
            case REQUIREMENT_VERSION ->
                    addInstructionsToImporter(
                            parser.getRequirementVersionInstructions(),
                            importer,
                            RequirementVersionInstructionContainer::new);
            case COVERAGE ->
                    addInstructionsToImporter(
                            parser.getCoverageInstructions(), importer, CoverageInstructionContainer::new);
            case REQUIREMENT_LINK ->
                    addInstructionsToImporter(
                            parser.getRequirementLinkInstructions(),
                            importer,
                            RequirementLinkInstructionContainer::new);
            case LINKED_LOW_LEVEL_REQ ->
                    addInstructionsToImporter(
                            parser.getLinkedLowLevelRequirementInstruction(),
                            importer,
                            LinkedLowLevelRequirementInstructionContainer::new);
            default ->
                    throw new IllegalArgumentException("Unknown EntityType for Requirement import : " + this);
        }
    }

    @Override
    public List<EntityType> getEntityType() {
        return REQ_ENTITIES_ORDERED_BY_INSTRUCTION_ORDER;
    }
}
