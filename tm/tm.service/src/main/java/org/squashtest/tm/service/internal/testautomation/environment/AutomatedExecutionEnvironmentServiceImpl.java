/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.environment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.ExecutionEnvironmentCountDto;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao;
import org.squashtest.tm.service.internal.testautomation.TestAutomationConnectorRegistry;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException;
import org.squashtest.tm.service.testautomation.spi.OutdatedSquashOrchestratorException;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;

@Service
@Transactional(noRollbackFor = InvalidSquashOrchestratorConfigurationException.class)
public class AutomatedExecutionEnvironmentServiceImpl
        implements AutomatedExecutionEnvironmentService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(AutomatedExecutionEnvironmentServiceImpl.class);
    private final TestAutomationServerDao testAutomationServerDao;
    private final TestAutomationConnectorRegistry testAutomationConnectorRegistry;
    private final TestAutomationServerDisplayService testAutomationServerDisplayService;
    private final ProjectDao customProjectDao;
    private final StoredCredentialsManager credentialsManager;

    @Inject
    public AutomatedExecutionEnvironmentServiceImpl(
            TestAutomationServerDao testAutomationServerDao,
            TestAutomationConnectorRegistry testAutomationConnectorRegistry,
            TestAutomationServerDisplayService testAutomationServerDisplayService,
            ProjectDao customProjectDao,
            StoredCredentialsManager credentialsManager) {
        this.testAutomationServerDao = testAutomationServerDao;
        this.testAutomationConnectorRegistry = testAutomationConnectorRegistry;
        this.testAutomationServerDisplayService = testAutomationServerDisplayService;
        this.customProjectDao = customProjectDao;
        this.credentialsManager = credentialsManager;
    }

    @Override
    public List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(
            Long testAutomationServerId) {
        Optional<TestAutomationServer> server =
                testAutomationServerDao.findById(testAutomationServerId);

        if (server.isEmpty()) {
            return Collections.emptyList();
        }

        TestAutomationConnector connector =
                testAutomationConnectorRegistry.getConnectorForKind(server.get().getKind());
        return connector.getAllAccessibleEnvironments(server.get());
    }

    @Override
    public List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(
            Long testAutomationServerId, Credentials credentials) {
        Optional<TestAutomationServer> server =
                testAutomationServerDao.findById(testAutomationServerId);

        if (server.isEmpty()) {
            return Collections.emptyList();
        }

        TestAutomationConnector connector =
                testAutomationConnectorRegistry.getConnectorForKind(server.get().getKind());
        return connector.getAllAccessibleEnvironments(server.get(), credentials);
    }

    @Override
    public boolean doesServerSupportAutomatedExecutionEnvironments(long testAutomationServerId) {
        TestAutomationServer server = testAutomationServerDao.getReferenceById(testAutomationServerId);
        TestAutomationConnector connector =
                testAutomationConnectorRegistry.getConnectorForKind(server.getKind());
        return connector.supportsAutomatedExecutionEnvironments();
    }

    @Override
    public ExecutionEnvironmentCountDto getExecutionEnvironmentCountDto(
            Map<Long, Long> projectAndServerIds) {
        if (projectAndServerIds.isEmpty()) {
            return new ExecutionEnvironmentCountDto(null, null, 0, null, null);
        }
        List<AutomatedExecutionEnvironment> environments = new ArrayList<>();

        int nbOfUnreachableServers = 0;
        int nbOfNoTokenInProjectOrServer = 0;

        for (Map.Entry<Long, Long> entry : projectAndServerIds.entrySet()) {
            Long projectId = entry.getKey();
            Long serverId = entry.getValue();

            boolean hasServerToken = testAutomationServerDisplayService.hasDefinedCredentials(serverId);
            Optional<TokenAuthCredentials> optionalProjectToken = findProjectToken(serverId, projectId);
            boolean hasProjectToken = optionalProjectToken.isPresent();

            if (!hasServerToken && !hasProjectToken) {
                LOGGER.warn(
                        "Could not get automated execution environments for server {} and project {} because no token was found",
                        serverId,
                        projectId);
                nbOfNoTokenInProjectOrServer++;
                continue;
            }

            try {
                environments.addAll(
                        hasProjectToken
                                ? getAllAccessibleEnvironments(serverId, optionalProjectToken.get())
                                : getAllAccessibleEnvironments(serverId));
            } catch (InvalidSquashOrchestratorConfigurationException
                    | OutdatedSquashOrchestratorException
                    | TestAutomationException ex) {
                nbOfUnreachableServers++;
                LOGGER.warn(
                        "Could not get automated execution environments for server {} and project {}",
                        serverId,
                        projectId,
                        ex);
            }
        }
        Map<String, Long> statusCounts =
                environments.isEmpty()
                        ? null
                        : environments.stream()
                                .collect(
                                        Collectors.groupingBy(
                                                AutomatedExecutionEnvironment::getStatus, Collectors.counting()));

        Set<Long> projectIds = projectAndServerIds.keySet();

        return new ExecutionEnvironmentCountDto(
                statusCounts,
                customProjectDao.findProjectsAndServersByProjectIds(projectIds),
                projectAndServerIds.size(),
                nbOfUnreachableServers,
                nbOfNoTokenInProjectOrServer);
    }

    private Optional<TokenAuthCredentials> findProjectToken(
            Long testAutomationServerId, Long projectId) {
        return Optional.ofNullable(
                        credentialsManager.findProjectCredentials(testAutomationServerId, projectId))
                .filter(TokenAuthCredentials.class::isInstance)
                .map(TokenAuthCredentials.class::cast);
    }
}
