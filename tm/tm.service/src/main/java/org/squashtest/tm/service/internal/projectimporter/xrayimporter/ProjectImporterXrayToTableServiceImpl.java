/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.xrayimporter;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CustomFieldXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayInfoModel;
import org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception.XrayParsingException;
import org.squashtest.tm.service.projectimporter.xrayimporter.ProjectImporterXrayToTableService;
import org.squashtest.tm.service.projectimporter.xrayimporter.XrayTablesDao;

@Service("ProjectImporterXrayToTableService")
public class ProjectImporterXrayToTableServiceImpl implements ProjectImporterXrayToTableService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ProjectImporterXrayToTableServiceImpl.class);
    private final XrayTablesDao xrayTablesDao;
    private final AtomicLong itemId = new AtomicLong(0);
    private final AtomicLong customFieldId = new AtomicLong(0);

    public ProjectImporterXrayToTableServiceImpl(XrayTablesDao xrayTablesDao) {
        this.xrayTablesDao = xrayTablesDao;
    }

    @FunctionalInterface
    private interface ActionNestedElement {
        void apply() throws XMLStreamException;
    }

    @FunctionalInterface
    private interface ActionOnParsing<T, U extends Throwable> {
        void apply(T t) throws U;
    }

    private void parsingXml(
            InputStream inputStream, ActionOnParsing<XMLStreamReader, XMLStreamException> actionOnParsing)
            throws XMLStreamException {
        XMLStreamReader xmlsr = null;
        try {
            XMLInputFactory xmlif = XMLInputFactory.newDefaultFactory();
            xmlif.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, true);
            xmlsr = xmlif.createXMLStreamReader(inputStream);
            actionOnParsing.apply(xmlsr);
        } finally {
            if (Objects.nonNull(xmlsr)) {
                xmlsr.close();
            }
        }
    }

    @Override
    public void getXrayInfoFromXMLParsing(
            InputStream inputStream, XrayInfoModel xrayInfoModel, String filename) {
        AtomicReference<String> name = new AtomicReference<>();
        List<Integer> lines = new ArrayList<>();
        try {
            parsingXml(
                    inputStream,
                    xmlsr -> {
                        XrayInfoModel.Entity xrayInfoEntity = new XrayInfoModel.Entity();
                        while (xmlsr.hasNext()) {
                            xmlsr.next();
                            xrayInfoEntity =
                                    handleAllElement(xmlsr, xrayInfoModel, xrayInfoEntity, name, lines, filename);
                        }
                    });
        } catch (XMLStreamException e) {
            LOGGER.error("Error reading xray file:", e);
            if (!lines.isEmpty()) {
                int line = lines.get(lines.size() - 1);
                if (name.get() == null) {
                    throw new XrayParsingException(filename, line, e.getMessage(), e);
                } else {
                    throw new XrayParsingException(filename, name.get(), line, e.getMessage(), e);
                }
            } else {
                throw new XrayParsingException(filename, e.getMessage(), e);
            }
        }
    }

    private XrayInfoModel.Entity handleAllElement(
            XMLStreamReader xmlsr,
            XrayInfoModel xrayInfoModel,
            XrayInfoModel.Entity xrayInfoEntity,
            AtomicReference<String> name,
            List<Integer> lines,
            String filename)
            throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()) {
            lines.add(xmlsr.getLocation().getLineNumber());
            switch (xmlsr.getLocalName()) {
                case XrayField.ITEM -> {
                    xrayInfoEntity = new XrayInfoModel.Entity();
                    name.set(null);
                }
                case XrayField.KEY -> xrayInfoEntity.setKey(getContentElement(xmlsr));
                case XrayField.TITLE -> name.set(getContentElement(xmlsr));
                case XrayField.TYPE -> xrayInfoEntity.setType(getContentElement(xmlsr));
                case XrayField.STATUS -> xrayInfoEntity.getStatus().add(getContentElement(xmlsr));
                case XrayField.PRIORITY -> xrayInfoEntity.getPriorities().add(getContentElement(xmlsr));
                default -> {
                    // continue analyzing
                }
            }
        }
        // two separate if blocks are necessary because getContentElement() extracts the content of the
        // element by looping on the element.
        if (XMLStreamConstants.END_ELEMENT == xmlsr.getEventType()) {
            if (XrayField.ITEM.equals(xmlsr.getLocalName())) {
                xrayInfoModel.addEntity(xrayInfoEntity, filename);
                xrayInfoEntity = new XrayInfoModel.Entity();
            }
            lines.remove(lines.size() - 1);
        }
        return xrayInfoEntity;
    }

    @Override
    public void convertXrayToTable(InputStream inputStream) throws XMLStreamException {
        parsingXml(inputStream, this::handleAndSaveItem);
    }

    private void handleAndSaveItem(XMLStreamReader xmlsr) throws XMLStreamException {
        List<ItemXrayDto> itemXrayDtos = new ArrayList<>();
        while (xmlsr.hasNext()) {
            int eventType = xmlsr.next();
            if (XMLStreamConstants.START_ELEMENT == eventType
                    && xmlsr.getLocalName().equalsIgnoreCase(XrayField.ITEM)) {
                // on item element
                ItemXrayDto itemXrayDto = new ItemXrayDto();
                itemXrayDto.setId(itemId.incrementAndGet());
                handleNestedElement(xmlsr, xmlsr.getLocalName(), () -> handleElement(xmlsr, itemXrayDto));
                itemXrayDto.addPivotId(XrayField.Type.convertType(itemXrayDto.getType()));
                itemXrayDto.setLabelFromLabels(itemXrayDto.getLabels());
                itemXrayDtos.add(itemXrayDto);
                if (itemXrayDtos.size() % 10 == 0) {
                    xrayTablesDao.createItems(itemXrayDtos);
                    itemXrayDtos.clear();
                }
            }
        }
        if (!itemXrayDtos.isEmpty()) {
            xrayTablesDao.createItems(itemXrayDtos);
            itemXrayDtos.clear();
        }
    }

    private void handleElement(XMLStreamReader xmlsr, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()) {
            // check element inside item element
            switch (xmlsr.getLocalName().toLowerCase()) {
                    // Get content in element
                case XrayField.TITLE -> itemXrayDto.setTitle(getContentElement(xmlsr));
                case XrayField.LINK -> itemXrayDto.setLink(getContentElement(xmlsr));
                case XrayField.PROJECT -> itemXrayDto.setProject(getContentElement(xmlsr));
                case XrayField.KEY -> itemXrayDto.setKey(getContentElement(xmlsr));
                case XrayField.SUMMARY -> itemXrayDto.setSummary(getContentElement(xmlsr));
                case XrayField.TYPE -> itemXrayDto.setType(getContentElement(xmlsr));
                case XrayField.PRIORITY -> itemXrayDto.setPriority(getContentElement(xmlsr));
                case XrayField.STATUS -> itemXrayDto.setStatus(getContentElement(xmlsr));
                case XrayField.CREATED -> itemXrayDto.setCreated(getContentElement(xmlsr));
                case XrayField.REPORTER -> itemXrayDto.setReporter(getContentElement(xmlsr));
                case XrayField.LABEL -> itemXrayDto.getLabels().add(getContentElement(xmlsr));

                    // Get all content
                case XrayField.DESCRIPTION ->
                        itemXrayDto.setDescription(getAllContentElement(xmlsr, false));

                case XrayField.CUSTOM_FIELD -> handleCustomField(xmlsr, itemXrayDto);
                case XrayField.ISSUE_LINKS -> handleIssueLink(xmlsr, itemXrayDto);
                default -> {
                    // continue parsing
                }
            }
        }
    }

    private void handleCustomField(XMLStreamReader xmlsr, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        String elementName = xmlsr.getLocalName();
        // start in customField element, not customfields
        CustomFieldXrayDto customFieldData = new CustomFieldXrayDto();
        // Get attribute KEY in customfield element
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()) {
            customFieldData.setKey(getAttributeValue(xmlsr, XrayField.ATTRIBUTE_KEY));
        }
        // Get element inside customField
        handleNestedElement(
                xmlsr, elementName, () -> getCustomFieldAttributes(xmlsr, customFieldData, itemXrayDto));
    }

    private void getCustomFieldAttributes(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldData, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()) {
            switch (xmlsr.getLocalName()) {
                case XrayField.CustomField.CUSTOM_FIELD_NAME ->
                        customFieldData.setName(getContentElement(xmlsr));
                case XrayField.CustomField.CUSTOM_FIELD_VALUES ->
                        handleNestedElement(
                                xmlsr,
                                xmlsr.getLocalName(),
                                () -> getCustomFieldValues(xmlsr, customFieldData, itemXrayDto));
                default -> {
                    // continue parsing
                }
            }
        }
    }

    private void getCustomFieldValues(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldData, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        // Start in customFieldValues (not the value)
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()) {
            String elementName = xmlsr.getLocalName();
            switch (elementName) {
                case XrayField.CustomField.CUSTOM_FIELD_VALUE ->
                        extractCustomFieldValue(xmlsr, customFieldData, itemXrayDto);
                case XrayField.CustomField.CUSTOM_FIELD_STEPS ->
                        handleCustomFieldSteps(xmlsr, elementName, customFieldData, itemXrayDto);
                case XrayField.CustomField.CUSTOM_FIELD_DATASET ->
                        handleTestCaseDataset(xmlsr, elementName, customFieldData, itemXrayDto);
                    // if html inside customFieldValues
                default ->
                        addValueToCustomFieldDto(
                                customFieldData, itemXrayDto, getAllContentElement(xmlsr, true));
            }
        }
    }

    private void extractCustomFieldValue(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldData, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        String rawContent = getAllContentElement(xmlsr, false);
        if (rawContent.startsWith("[") && rawContent.endsWith("]")) {
            String[] contents = rawContent.substring(1, rawContent.length() - 1).split(",");
            Arrays.stream(contents)
                    .map(StringUtils::trim)
                    .forEach(
                            content -> {
                                if (StringUtils.isNotEmpty(content)) {
                                    addValueToCustomFieldDto(customFieldData, itemXrayDto, content);
                                }
                            });
        } else {
            addValueToCustomFieldDto(customFieldData, itemXrayDto, rawContent);
        }
    }

    private void addValueToCustomFieldDto(
            CustomFieldXrayDto customFieldData, ItemXrayDto itemXrayDto, String value) {
        CustomFieldXrayDto customFieldXrayDto =
                new CustomFieldXrayDto(customFieldData, itemId, customFieldId);
        customFieldXrayDto.setValue(value);
        itemXrayDto.getCustomFields().add(customFieldXrayDto);
    }

    private void handleCustomFieldSteps(
            XMLStreamReader xmlsr,
            String elementName,
            CustomFieldXrayDto customFieldData,
            ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        handleNestedElement(
                xmlsr, elementName, () -> handleCustomFieldStep(xmlsr, customFieldData, itemXrayDto));
    }

    private void handleCustomFieldStep(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldData, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()
                && xmlsr.getLocalName().equalsIgnoreCase(XrayField.CustomField.CUSTOM_FIELD_STEP)) {
            CustomFieldXrayDto customFieldXrayDto =
                    new CustomFieldXrayDto(customFieldData, itemId, customFieldId);
            handleNestedElement(
                    xmlsr,
                    XrayField.CustomField.CUSTOM_FIELD_STEP,
                    () -> handleCustomFieldStepElements(xmlsr, customFieldXrayDto));
            itemXrayDto.getCustomFields().add(customFieldXrayDto);
        }
    }

    private void handleCustomFieldStepElements(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldXrayDto) throws XMLStreamException {
        if ((XMLStreamConstants.START_ELEMENT == xmlsr.getEventType())) {
            switch (xmlsr.getLocalName().toLowerCase()) {
                case XrayField.CustomField.CUSTOM_FIELD_STEP_INDEX ->
                        customFieldXrayDto.setStepIndex(Integer.parseInt(getContentElement(xmlsr)));
                case XrayField.CustomField.CUSTOM_FIELD_STEP_ACTION ->
                        customFieldXrayDto.setStepAction(getContentElement(xmlsr));
                case XrayField.CustomField.CUSTOM_FIELD_STEP_DATA ->
                        customFieldXrayDto.setStepData(getContentElement(xmlsr));
                case XrayField.CustomField.CUSTOM_FIELD_STEP_EXPECTED_RESULT ->
                        customFieldXrayDto.setStepExpectedResult(getContentElement(xmlsr));
                case XrayField.CustomField.CUSTOM_FIELD_STEP_CALL_TEST ->
                        handleCallTestCase(xmlsr, customFieldXrayDto);
                default -> {
                    // continue parsing
                }
            }
        }
    }

    private void handleCallTestCase(XMLStreamReader xmlsr, CustomFieldXrayDto customFieldXrayDto)
            throws XMLStreamException {
        String elementName = xmlsr.getLocalName();
        customFieldXrayDto.setStepCalledTestKey(getAttributeValue(xmlsr, XrayField.ATTRIBUTE_KEY));
        Map<String, String> callTestParameters = new HashMap<>();
        handleNestedElement(
                xmlsr, elementName, () -> handleCallTestCaseParameter(xmlsr, callTestParameters));
        customFieldXrayDto.setToStringStepCalledTestParameters(callTestParameters);
    }

    private void handleCallTestCaseParameter(
            XMLStreamReader xmlsr, Map<String, String> callTestParameters) throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()
                && xmlsr
                        .getLocalName()
                        .equalsIgnoreCase(XrayField.CustomField.CUSTOM_FIELD_STEP_CALL_TEST_PARAMETER)) {
            callTestParameters.put(
                    getAttributeValue(xmlsr, XrayField.ATTRIBUTE_NAME), getContentElement(xmlsr));
        }
    }

    private void handleTestCaseDataset(
            XMLStreamReader xmlsr,
            String elementName,
            CustomFieldXrayDto customFieldData,
            ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        AtomicInteger row = new AtomicInteger(0);
        handleNestedElement(
                xmlsr, elementName, () -> getDatasetAttributes(xmlsr, customFieldData, itemXrayDto, row));
    }

    private void getDatasetAttributes(
            XMLStreamReader xmlsr,
            CustomFieldXrayDto customFieldData,
            ItemXrayDto itemXrayDto,
            AtomicInteger row)
            throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()) {
            if (xmlsr.getLocalName().equalsIgnoreCase(XrayField.CustomField.CUSTOM_FIELD_DATASET_ROW)) {
                row.getAndIncrement();
            }
            if (xmlsr
                    .getLocalName()
                    .equalsIgnoreCase(XrayField.CustomField.CUSTOM_FIELD_DATASET_PARAMETER)) {
                CustomFieldXrayDto customFieldXrayDto =
                        new CustomFieldXrayDto(customFieldData, itemId, customFieldId);
                handleNestedElement(
                        xmlsr, xmlsr.getLocalName(), () -> getDatasetParameter(xmlsr, customFieldXrayDto, row));
                itemXrayDto.getCustomFields().add(customFieldXrayDto);
            }
        }
    }

    private void getDatasetParameter(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldXrayDto, AtomicInteger row)
            throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()) {
            switch (xmlsr.getLocalName().toLowerCase()) {
                case XrayField.CustomField.CUSTOM_FIELD_DATASET_NAME ->
                        customFieldXrayDto.setDatasetName(getContentElement(xmlsr));
                case XrayField.CustomField.CUSTOM_FIELD_DATASET_VALUE ->
                        customFieldXrayDto.setDatasetValue(getContentElement(xmlsr));
                default -> {
                    // continue parsing
                }
            }
        }
        customFieldXrayDto.setDatasetRow(row.get());
    }

    private void handleIssueLink(XMLStreamReader xmlsr, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        String elementName = xmlsr.getLocalName();
        handleNestedElement(xmlsr, elementName, () -> handleIssueLinkType(xmlsr, itemXrayDto));
    }

    private void handleIssueLinkType(XMLStreamReader xmlsr, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        if (XMLStreamConstants.START_ELEMENT == xmlsr.getEventType()
                && xmlsr.getLocalName().equalsIgnoreCase(XrayField.ISSUE_LINK_TYPE)) {
            CustomFieldXrayDto customFieldData = new CustomFieldXrayDto();
            customFieldData.setKey("issuelinks-key");
            handleNestedElement(
                    xmlsr,
                    xmlsr.getLocalName(),
                    () -> handleIssueRelationship(xmlsr, customFieldData, itemXrayDto));
        }
    }

    private void handleIssueRelationship(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldData, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        if (xmlsr.getEventType() == XMLStreamConstants.START_ELEMENT) {
            switch (xmlsr.getLocalName().toLowerCase()) {
                case XrayField.ISSUE_LINK_TYPE_NAME ->
                        customFieldData.setIssueLinkName(getContentElement(xmlsr));
                case XrayField.INWARD_LINK, XrayField.OUTWARD_LINK ->
                        handleIssueLinkKeys(xmlsr, customFieldData, itemXrayDto);
                default -> {
                    // Continue parsing
                }
            }
        }
    }

    private void handleIssueLinkKeys(
            XMLStreamReader xmlsr, CustomFieldXrayDto customFieldData, ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        String elementName = xmlsr.getLocalName();
        handleNestedElement(
                xmlsr,
                elementName,
                () -> extractIssueLinkKey(xmlsr, elementName, customFieldData, itemXrayDto));
    }

    private void extractIssueLinkKey(
            XMLStreamReader xmlsr,
            String elementName,
            CustomFieldXrayDto customFieldData,
            ItemXrayDto itemXrayDto)
            throws XMLStreamException {
        if (xmlsr.getEventType() == XMLStreamConstants.START_ELEMENT
                && xmlsr.getLocalName().equalsIgnoreCase(XrayField.ISSUE_KEY)) {
            CustomFieldXrayDto customFieldXrayDto =
                    new CustomFieldXrayDto(customFieldData, itemId, customFieldId);
            customFieldXrayDto.setIssueLinkKey(getContentElement(xmlsr));
            customFieldXrayDto.setIssueLinkRelationship(elementName);
            itemXrayDto.getCustomFields().add(customFieldXrayDto);
        }
    }

    private String getContentElement(XMLStreamReader xmlsr) throws XMLStreamException {
        String elementName = xmlsr.getLocalName();
        StringBuilder stringBuilder = new StringBuilder();
        handleNestedElement(xmlsr, elementName, () -> appendContentElement(xmlsr, stringBuilder));
        return formatContent(stringBuilder.toString());
    }

    private String getAllContentElement(XMLStreamReader xmlsr, boolean includeLoopElement)
            throws XMLStreamException {
        StringBuilder stringBuilder = new StringBuilder();
        // if we want to extract data in first element
        if (includeLoopElement) {
            extractElementData(xmlsr, stringBuilder);
        }
        extractAllElementData(xmlsr, includeLoopElement, stringBuilder);
        return formatContent(stringBuilder.toString());
    }

    private String formatContent(String content) {
        content = StringUtils.trim(content);
        return HtmlUtils.htmlUnescape(content).replace("\u00A0", StringUtils.SPACE);
    }

    private void appendContentElement(XMLStreamReader xmlsr, StringBuilder stringBuilder) {
        if (XMLStreamConstants.CHARACTERS == xmlsr.getEventType()) {
            // handle indentation -> removing CRLF and excess white space
            stringBuilder.append(
                    xmlsr
                            .getText()
                            .replaceAll("\\R+", StringUtils.SPACE)
                            .replaceAll("\\s{2,}", StringUtils.SPACE));
        }
    }

    // Need to append last element -> cant use handleNestedElement()
    private void extractAllElementData(
            XMLStreamReader xmlsr, boolean includeLoopElement, StringBuilder stringBuilder)
            throws XMLStreamException {
        String elementName = xmlsr.getLocalName();
        boolean nextContent = true;
        while (nextContent) {
            int eventType = xmlsr.next();
            if (XMLStreamConstants.END_ELEMENT == eventType
                    && xmlsr.getLocalName().equalsIgnoreCase(elementName)) {
                nextContent = false;
                if (includeLoopElement) {
                    stringBuilder.append("</").append(xmlsr.getLocalName()).append(">");
                }
            } else {
                extractElementData(xmlsr, stringBuilder);
            }
        }
    }

    private void extractElementData(XMLStreamReader xmlsr, StringBuilder stringBuilder) {
        switch (xmlsr.getEventType()) {
                // recreate node with <element attribute=""/>
            case XMLStreamConstants.START_ELEMENT -> buildStartElement(xmlsr, stringBuilder);
                // extract text content without indentation
            case XMLStreamConstants.CHARACTERS ->
                    stringBuilder.append(
                            xmlsr
                                    .getText()
                                    .replaceAll("\\R+", " ") //  correction auto format document
                                    .replaceAll("\\s{2,}", " "));
                // close the node
            case XMLStreamConstants.END_ELEMENT ->
                    stringBuilder.append("</").append(xmlsr.getLocalName()).append(">");
            case XMLStreamConstants.ENTITY_REFERENCE -> {
                if ("nbsp"
                        .equalsIgnoreCase(xmlsr.getLocalName())) { // IS_REPLACING_ENTITY_REFERENCES is false
                    stringBuilder.append(StringUtils.SPACE);
                }
            }
            default -> {
                // continue parsing
            }
        }
    }

    private void buildStartElement(XMLStreamReader xmlsr, StringBuilder stringBuilder) {
        stringBuilder.append("<").append(xmlsr.getLocalName());
        for (int i = 0; i < xmlsr.getAttributeCount(); i++) {
            stringBuilder
                    .append(StringUtils.SPACE)
                    .append(xmlsr.getAttributeLocalName(i))
                    .append("=\"")
                    .append(xmlsr.getAttributeValue(i))
                    .append("\"");
        }
        stringBuilder.append(">");
    }

    private String getAttributeValue(XMLStreamReader xmlsr, String attributeName) {
        for (int i = 0; i < xmlsr.getAttributeCount(); i++) {
            if (xmlsr.getAttributeLocalName(i).equalsIgnoreCase(attributeName)) {
                return xmlsr.getAttributeValue(i);
            }
        }
        return null;
    }

    private void handleNestedElement(
            XMLStreamReader xmlsr, String elementName, ActionNestedElement action)
            throws XMLStreamException {
        boolean nestedElement = true;
        // Start on the next element, not the current
        while (nestedElement) {
            int eventType = xmlsr.next();
            if (XMLStreamConstants.END_ELEMENT == eventType
                    && xmlsr.getLocalName().equalsIgnoreCase(elementName)) {
                nestedElement = false;
            } else {
                action.apply();
            }
        }
    }
}
