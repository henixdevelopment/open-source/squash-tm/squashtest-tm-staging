/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.requirement.RequirementHelper;

@Service("RequirementHelper")
@Transactional
public class RequirementHelperImpl implements RequirementHelper {

    private final RequirementDao requirementDao;

    public RequirementHelperImpl(RequirementDao requirementDao) {
        this.requirementDao = requirementDao;
    }

    @Override
    public boolean checkIfReqIsHighLevelByReqVersionId(Long reqVersionId) {
        return requirementDao.isHighLevelRequirementVersion(reqVersionId);
    }

    @Override
    public Long findRequirementVersionIdFromRequirementId(Long requirementId) {
        return requirementDao.findRequirementCurrentVersionIdFromRequirementId(requirementId);
    }

    @Override
    public boolean checkIfRequirementIsNotChild(Long requirementId) {
        return !requirementDao.checkIfRequirementIsChild(requirementId);
    }
}
