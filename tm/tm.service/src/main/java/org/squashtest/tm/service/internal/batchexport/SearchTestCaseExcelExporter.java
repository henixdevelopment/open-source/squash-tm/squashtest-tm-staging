/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport;

import static org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn.TC_AUTOMATABLE;

import java.util.Map;
import javax.inject.Inject;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.internal.batchexport.models.TestCaseModel;
import org.squashtest.tm.service.internal.batchimport.column.TemplateColumn;
import org.squashtest.tm.service.internal.batchimport.column.testcase.TestCaseSheetColumn;
import org.squashtest.tm.service.project.ProjectFinder;

@Component
@Scope("prototype")
public class SearchTestCaseExcelExporter extends ExcelExporter {

    private static final TestCaseSheetColumn[] SEARCH_TC_COLUMNS = {
        TestCaseSheetColumn.TC_NB_STEPS, TestCaseSheetColumn.TC_NB_ITERATION
    };

    private static final TestCaseSheetColumn MILESTONE_SEARCH_TC_COLUMNS =
            TestCaseSheetColumn.TC_NB_MILESTONES;

    @Inject ProjectFinder projectFinder;

    @Inject IterationModificationService iterationFinder;

    @Inject
    public SearchTestCaseExcelExporter(FeatureManager featureManager, MessageSource messageSource) {
        super(featureManager, messageSource);
    }

    @Override
    protected void createOptionalTestCaseSheetHeaders() {
        Sheet testCaseSheet = workbook.getSheet(TC_SHEET);
        Row row = testCaseSheet.getRow(0);
        int cellIndex = row.getLastCellNum();

        if (projectFinder.countProjectsAllowAutomationWorkflow() > 0) {
            row.createCell(cellIndex++).setCellValue(TC_AUTOMATABLE.name());
        }
        if (isMilestoneModeEnabled) {
            row.createCell(cellIndex++).setCellValue(MILESTONE_SEARCH_TC_COLUMNS.getHeader());
        }

        for (TemplateColumn t : SEARCH_TC_COLUMNS) {
            row.createCell(cellIndex++).setCellValue(t.getHeader());
        }
    }

    @Override
    protected int doOptionalAppendTestCases(
            boolean shouldAddAutomationWorkflowColumn,
            Map<Long, Boolean> allowAutomationWorkflowByProjectId,
            Row row,
            int cellIndex,
            TestCaseModel testCaseModel) {
        cellIndex =
                super.doOptionalAppendTestCases(
                        shouldAddAutomationWorkflowColumn,
                        allowAutomationWorkflowByProjectId,
                        row,
                        cellIndex,
                        testCaseModel);

        if (isMilestoneModeEnabled) {
            Long nbMilestone = testCaseModel.getMilestonesCount();
            row.createCell(cellIndex++).setCellValue(nbMilestone);
        }

        Long nbSteps = testCaseModel.getTestStepCount();
        row.createCell(cellIndex++).setCellValue(nbSteps);

        int nbIteration = iterationFinder.findIterationContainingTestCase(testCaseModel.getId()).size();
        row.createCell(cellIndex).setCellValue(nbIteration);

        return cellIndex;
    }
}
