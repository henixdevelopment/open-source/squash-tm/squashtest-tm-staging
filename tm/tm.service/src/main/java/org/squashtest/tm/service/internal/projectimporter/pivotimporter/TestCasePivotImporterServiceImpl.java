/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.ProjectIdsReferences;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.ActionTestStepToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.CalledTestCaseStepToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.DatasetParamToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.DatasetToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.KeywordTestStepToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace.TestCaseToImport;
import org.squashtest.tm.service.internal.library.LibraryUtils;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.TestCaseFolderDao;
import org.squashtest.tm.service.internal.repository.TestCaseLibraryDao;
import org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig;
import org.squashtest.tm.service.internal.testcase.NatureTypeChainFixer;
import org.squashtest.tm.service.internal.testcase.ParameterNamesFinder;
import org.squashtest.tm.service.projectimporter.pivotimporter.AttachmentPivotImportService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.TestCasePivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.TestCaseWorkspaceParser;
import org.squashtest.tm.service.requirement.VerifiedRequirementsManagerService;
import org.squashtest.tm.service.testcase.CallStepManagerService;
import org.squashtest.tm.service.testcase.DatasetModificationService;
import org.squashtest.tm.service.testcase.ParameterFinder;
import org.squashtest.tm.service.testcase.ParameterModificationService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;

@Service("TestCasePivotImporterService")
public class TestCasePivotImporterServiceImpl implements TestCasePivotImporterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestCasePivotImporterService.class);

    private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;
    private final TestCaseLibraryDao testCaseLibraryDao;
    private final TestCaseFolderDao testCaseFolderDao;
    private final TestCaseDao testCaseDao;
    private final PrivateCustomFieldValueService customFieldValueService;
    private final TestCaseModificationService testCaseModificationService;
    private final VerifiedRequirementsManagerService verifiedRequirementsManagerService;
    private final ParameterModificationService parameterModificationService;
    private final DatasetModificationService datasetModificationService;
    private final CallStepManagerService callStepManager;
    private final ParameterFinder parameterFinder;
    private final TestCaseWorkspaceParser testCaseWorkspaceParser;

    private final AttachmentPivotImportService attachmentPivotImportService;

    @PersistenceContext private EntityManager entityManager;

    public TestCasePivotImporterServiceImpl(
            TestCaseLibraryNavigationService testCaseLibraryNavigationService,
            TestCaseLibraryDao testCaseLibraryDao,
            TestCaseFolderDao testCaseFolderDao,
            TestCaseDao testCaseDao,
            PrivateCustomFieldValueService customFieldValueService,
            TestCaseModificationService testCaseModificationService,
            VerifiedRequirementsManagerService verifiedRequirementsManagerService,
            ParameterModificationService parameterModificationService,
            DatasetModificationService datasetModificationService,
            CallStepManagerService callStepManager,
            ParameterFinder parameterFinder,
            TestCaseWorkspaceParser testCaseWorkspaceParser,
            AttachmentPivotImportService attachmentPivotImportService) {
        this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
        this.testCaseLibraryDao = testCaseLibraryDao;
        this.testCaseFolderDao = testCaseFolderDao;
        this.testCaseDao = testCaseDao;
        this.customFieldValueService = customFieldValueService;
        this.testCaseModificationService = testCaseModificationService;
        this.verifiedRequirementsManagerService = verifiedRequirementsManagerService;
        this.parameterModificationService = parameterModificationService;
        this.datasetModificationService = datasetModificationService;
        this.callStepManager = callStepManager;
        this.parameterFinder = parameterFinder;
        this.testCaseWorkspaceParser = testCaseWorkspaceParser;
        this.attachmentPivotImportService = attachmentPivotImportService;
    }

    @Override
    public void importTestCasesFromZipArchive(
            ZipFile zipFile,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.TEST_CASES, pivotFormatImport);
        ZipEntry entry = zipFile.getEntry(JsonImportFile.TEST_CASES.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleTestCasesFromJsonFile(
                        jsonInputStream, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.TEST_CASES, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.TEST_CASES, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleTestCasesFromJsonFile(
            InputStream jsonInputStream,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseTestCasesArray(
                        projectIdsReferences, pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
    }

    private void parseTestCasesArray(
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<TestCaseToImport> testCasesToImport = new ArrayList<>();
        HibernateConfig.enableBatch(entityManager, HibernateConfig.BATCH_50);
        if (!JsonImportField.TEST_CASES.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                TestCaseToImport testCaseToImport =
                        testCaseWorkspaceParser.parseTestCase(
                                jsonParser, pivotImportMetadata, pivotFormatImport);
                testCasesToImport.add(testCaseToImport);
            }

            if (testCasesToImport.size() == HibernateConfig.BATCH_50) {
                createTestCases(
                        testCasesToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
            }
        }

        if (!testCasesToImport.isEmpty()) {
            createTestCases(
                    testCasesToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
        }
        HibernateConfig.disabledBatch(entityManager);
    }

    private void createTestCases(
            List<TestCaseToImport> testCasesToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        Map<TestCase, TestCaseToImport> createdTestCases = new HashMap<>();
        batchCreateHibernateTestCases(
                testCasesToImport,
                projectIdsReferences,
                pivotImportMetadata,
                pivotFormatImport,
                createdTestCases);

        createdTestCases.forEach(
                (squashTestCase, testCaseToImport) -> {
                    try {
                        postProcessCreatedTestCase(
                                pivotImportMetadata, pivotFormatImport, squashTestCase, testCaseToImport);

                        createTestStepsForTestCase(
                                testCaseToImport, squashTestCase, pivotImportMetadata, pivotFormatImport);
                    } catch (Exception e) {
                        PivotFormatLoggerHelper.handleEntityCreationFailed(
                                LOGGER,
                                PivotFormatLoggerHelper.TEST_CASE,
                                testCaseToImport.getName(),
                                testCaseToImport.getInternalId(),
                                pivotFormatImport,
                                e);
                    }

                    PivotFormatLoggerHelper.logEntityCreatedSuccessfully(
                            LOGGER,
                            PivotFormatLoggerHelper.TEST_CASE,
                            testCaseToImport.getName(),
                            testCaseToImport.getInternalId(),
                            pivotFormatImport);
                });

        testCasesToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void batchCreateHibernateTestCases(
            List<TestCaseToImport> testCasesToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport,
            Map<TestCase, TestCaseToImport> createdTestCases) {

        List<TestCaseToImport> testCasesToImportInLibrary = new ArrayList<>();
        Map<String, List<TestCaseToImport>> testCaseToImportByParentIdMap = new HashMap<>();
        Map<TestCase, Map<Long, RawValue>> testCaseCustomFields = new HashMap<>();

        for (TestCaseToImport testCaseToImport : testCasesToImport) {
            if (testCaseToImport.getParentId() == null) {
                testCasesToImportInLibrary.add(testCaseToImport);
            } else {
                testCaseToImportByParentIdMap
                        .computeIfAbsent(testCaseToImport.getParentId(), k -> new ArrayList<>())
                        .add(testCaseToImport);
            }
        }

        addTestCasesToLibrary(
                projectIdsReferences,
                testCasesToImportInLibrary,
                createdTestCases,
                testCaseCustomFields,
                pivotFormatImport);
        addTestCasesToFolders(
                pivotImportMetadata,
                testCaseToImportByParentIdMap,
                createdTestCases,
                testCaseCustomFields,
                pivotFormatImport);
        initializeCustomFieldValues(pivotFormatImport, createdTestCases, testCaseCustomFields);
    }

    private void postProcessCreatedTestCase(
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport,
            TestCase squashTestCase,
            TestCaseToImport testCaseToImport)
            throws IOException {
        pivotImportMetadata
                .getTestCaseIdsMap()
                .put(testCaseToImport.getInternalId(), squashTestCase.getId());

        addVerifiedRequirementsToTestCase(
                pivotImportMetadata.getRequirementIdsMap(),
                testCaseToImport.getVerifiedRequirementIds(),
                squashTestCase);

        addDatasetParamsToTestCase(
                squashTestCase,
                testCaseToImport,
                pivotImportMetadata.getDatasetParamIdsMap(),
                pivotFormatImport);

        addDatasetsToTestCase(squashTestCase, testCaseToImport.getDataSets(), pivotImportMetadata);

        attachmentPivotImportService.addAttachmentsToEntity(
                testCaseToImport.getAttachments(),
                new AttachmentHolderInfo(
                        squashTestCase.getId(),
                        squashTestCase.getAttachmentList().getId(),
                        EntityType.TEST_CASE,
                        testCaseToImport.getInternalId()),
                pivotFormatImport,
                pivotImportMetadata);
    }

    private void addTestCasesToLibrary(
            ProjectIdsReferences projectIdsReferences,
            List<TestCaseToImport> testCasesToImportInLibrary,
            Map<TestCase, TestCaseToImport> createdTestCases,
            Map<TestCase, Map<Long, RawValue>> testCaseCustomFields,
            PivotFormatImport pivotFormatImport) {
        TestCaseLibrary library =
                testCaseLibraryDao.loadForNodeAddition(projectIdsReferences.getTestCaseLibraryId());
        List<String> contentNames =
                testCaseLibraryNavigationService.findContentNamesByLibraryId(
                        (projectIdsReferences.getTestCaseLibraryId()));

        // First check for duplicate names and fix conflicts inside current batch
        fixConflictNames(new ArrayList<>(), testCasesToImportInLibrary);

        //  Then check for conflicts with existing content in database
        fixConflictNames(contentNames, testCasesToImportInLibrary);

        for (TestCaseToImport testCaseToImport : testCasesToImportInLibrary) {
            createAndAddTestCaseToEntity(
                    createdTestCases, testCaseCustomFields, pivotFormatImport, testCaseToImport, library);
        }
    }

    private void addTestCasesToFolders(
            PivotImportMetadata pivotImportMetadata,
            Map<String, List<TestCaseToImport>> testCaseToImportByParentIdMap,
            Map<TestCase, TestCaseToImport> createdTestCases,
            Map<TestCase, Map<Long, RawValue>> testCaseCustomFields,
            PivotFormatImport pivotFormatImport) {
        List<Long> squashFolderIds =
                testCaseToImportByParentIdMap.keySet().stream()
                        .map(parentId -> pivotImportMetadata.getTestCaseFoldersIdsMap().get(parentId))
                        .toList();

        Map<Long, List<String>> folderContentNames =
                testCaseLibraryNavigationService.findContentNamesByFolderIds(squashFolderIds);

        Map<Long, TestCaseFolder> testCaseFolderById =
                testCaseFolderDao.loadForNodeAddition(squashFolderIds).stream()
                        .collect(Collectors.toMap(TestCaseLibraryNode::getId, Function.identity()));

        testCaseToImportByParentIdMap.forEach(
                (parentId, testCasesToImportInFolder) -> {
                    Long squashFolderId = pivotImportMetadata.getTestCaseFoldersIdsMap().get(parentId);
                    TestCaseFolder folder = testCaseFolderById.get((squashFolderId));

                    // First check for duplicate names and fix conflicts inside current batch
                    fixConflictNames(new ArrayList<>(), testCasesToImportInFolder);

                    // Then check for conflicts with existing content in database
                    if (folderContentNames.get(squashFolderId) != null) {
                        fixConflictNames(folderContentNames.get(squashFolderId), testCasesToImportInFolder);
                    }

                    for (TestCaseToImport testCaseToImport : testCasesToImportInFolder) {
                        createAndAddTestCaseToEntity(
                                createdTestCases,
                                testCaseCustomFields,
                                pivotFormatImport,
                                testCaseToImport,
                                folder);
                    }
                });
    }

    private void createAndAddTestCaseToEntity(
            Map<TestCase, TestCaseToImport> createdTestCases,
            Map<TestCase, Map<Long, RawValue>> testCaseCustomFields,
            PivotFormatImport pivotFormatImport,
            TestCaseToImport testCaseToImport,
            Object entity) {
        try {
            TestCase squashTestCase = testCaseToImport.toTestCase();

            if (entity instanceof TestCaseLibrary library) {
                library.addContent(squashTestCase);
            } else if (entity instanceof TestCaseFolder folder) {
                folder.addContent(squashTestCase);
            }

            replaceInfoListReferences(squashTestCase);
            testCaseDao.safePersist(squashTestCase);
            createdTestCases.put(squashTestCase, testCaseToImport);
            testCaseCustomFields.put(squashTestCase, testCaseToImport.getCustomFields());
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleEntityCreationFailed(
                    LOGGER,
                    PivotFormatLoggerHelper.TEST_CASE,
                    testCaseToImport.getName(),
                    testCaseToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }
    }

    private void replaceInfoListReferences(TestCase testCase) {
        NatureTypeChainFixer.fix(testCase);
    }

    private static void fixConflictNames(
            List<String> contentNames, List<TestCaseToImport> testCaseToImports) {
        if (testCaseToImports != null && !testCaseToImports.isEmpty()) {
            testCaseToImports.forEach(
                    testCaseToImport -> {
                        String newName =
                                LibraryUtils.generateNonClashingName(
                                        testCaseToImport.getName(), contentNames, Sizes.NAME_MAX);
                        if (!newName.equals(testCaseToImport.getName())) {
                            testCaseToImport.setName(newName);
                        }
                        contentNames.add(newName);
                    });
        }
    }

    private void initializeCustomFieldValues(
            PivotFormatImport pivotFormatImport,
            Map<TestCase, TestCaseToImport> createdTestCases,
            Map<TestCase, Map<Long, RawValue>> testCaseCustomFields) {
        customFieldValueService.createAllCustomFieldValues(
                createdTestCases.keySet(), pivotFormatImport.getProject());
        customFieldValueService.initBatchCustomFieldValues(testCaseCustomFields);
    }

    private void addDatasetParamsToTestCase(
            TestCase squashTestCase,
            TestCaseToImport testCaseToImport,
            Map<String, Long> datasetParamIdsMap,
            PivotFormatImport pivotFormatImport) {
        logStartingDatasetParamsImportForTestCase(testCaseToImport, pivotFormatImport);
        testCaseToImport
                .getDatasetParams()
                .forEach(
                        datasetParam -> {
                            try {
                                Parameter parameter =
                                        parameterModificationService.addNewParameterToTestCaseUnsecured(
                                                datasetParam.toDatasetParameter(), squashTestCase);
                                datasetParamIdsMap.put(datasetParam.getInternalId(), parameter.getId());
                                logDatasetParamImportedSuccessfully(
                                        datasetParam, squashTestCase, pivotFormatImport);
                            } catch (Exception e) {
                                logDatasetParamImportFailed(datasetParam, squashTestCase, pivotFormatImport, e);
                                throw e;
                            }
                        });
    }

    private void addDatasetsToTestCase(
            TestCase squashTestCase,
            List<DatasetToImport> dataSets,
            PivotImportMetadata pivotImportMetadata) {
        dataSets.forEach(
                datasetToImport -> {
                    Dataset dataset =
                            datasetModificationService.persistUnsecured(
                                    datasetToImport.toDataset(
                                            parameterFinder, pivotImportMetadata.getDatasetParamIdsMap()),
                                    squashTestCase);
                    pivotImportMetadata
                            .getDatasetIdsMap()
                            .put(datasetToImport.getInternalId(), dataset.getId());
                });
    }

    private void createTestStepsForTestCase(
            TestCaseToImport testCaseToImport,
            TestCase testCase,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        logStartingTestStepsImportForTestCase(testCaseToImport, pivotFormatImport);
        if (TestCaseKind.STANDARD.equals(TestCaseKind.valueOf(testCaseToImport.getTestCaseKind()))) {
            createActionTestSteps(
                    testCaseToImport.getActionTestSteps(), testCase, pivotImportMetadata, pivotFormatImport);
        } else if (TestCaseKind.KEYWORD.equals(
                TestCaseKind.valueOf(testCaseToImport.getTestCaseKind()))) {
            createKeywordTestSteps(
                    testCaseToImport.getKeywordTestSteps(), testCase, pivotImportMetadata, pivotFormatImport);
        }
    }

    private void createActionTestSteps(
            List<ActionTestStepToImport> actionTestSteps,
            TestCase testCase,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        Map<ActionTestStep, ActionTestStepToImport> createdActionTestSteps = new HashMap<>();
        batchCreateActionTestSteps(
                actionTestSteps, testCase, pivotFormatImport, createdActionTestSteps);

        // I am forced to do a regular loop here because .forEach can not rethrow IOException
        for (Map.Entry<ActionTestStep, ActionTestStepToImport> entry :
                createdActionTestSteps.entrySet()) {
            ActionTestStep testStep = entry.getKey();
            ActionTestStepToImport actionTestStepToImport = entry.getValue();

            try {
                addVerifiedRequirementsToTestStep(
                        pivotImportMetadata.getRequirementIdsMap(), actionTestStepToImport, testStep, testCase);

                pivotImportMetadata
                        .getTestStepsIdsMap()
                        .put(actionTestStepToImport.getInternalId(), testStep.getId());

                attachmentPivotImportService.addAttachmentsToEntity(
                        actionTestStepToImport.getAttachments(),
                        new AttachmentHolderInfo(
                                testCase.getId(),
                                testStep.getAttachmentList().getId(),
                                EntityType.ACTION_TEST_STEP,
                                actionTestStepToImport.getInternalId()),
                        pivotFormatImport,
                        pivotImportMetadata);

                logActionTestStepImportedSuccessfully(actionTestStepToImport, testCase, pivotFormatImport);
            } catch (Exception e) {
                logActionTestStepImportFailed(actionTestStepToImport, testCase, pivotFormatImport, e);
                throw e;
            }
        }
    }

    private void batchCreateActionTestSteps(
            List<ActionTestStepToImport> actionTestSteps,
            TestCase testCase,
            PivotFormatImport pivotFormatImport,
            Map<ActionTestStep, ActionTestStepToImport> createdActionTestSteps) {
        Map<ActionTestStep, Map<Long, RawValue>> testStepCustomFields = new HashMap<>();
        ParameterNamesFinder parameterNamesFinder = new ParameterNamesFinder();
        Set<String> parameterNames = new HashSet<>();

        for (ActionTestStepToImport actionTestStepToImport : actionTestSteps) {
            ActionTestStep step = actionTestStepToImport.toActionTestStep();
            testCase.addStep(step);
            entityManager.persist(step);
            testStepCustomFields.put(step, actionTestStepToImport.getCustomFields());
            parameterNames.addAll(
                    parameterNamesFinder.findParametersNamesInActionAndExpectedResult(step));
            createdActionTestSteps.put(step, actionTestStepToImport);
        }

        customFieldValueService.createAllCustomFieldValues(
                testStepCustomFields.keySet(), pivotFormatImport.getProject());
        customFieldValueService.initBatchCustomFieldValues(testStepCustomFields);

        for (String name : parameterNames) {
            Parameter parameter = testCase.findParameterByName(name);
            if (parameter == null) {
                parameter = new Parameter(name);
                parameter.setTestCase(testCase);
            }
        }
    }

    private void createKeywordTestSteps(
            List<KeywordTestStepToImport> keywordTestSteps,
            TestCase testCase,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        keywordTestSteps.forEach(
                keywordStep -> {
                    try {

                        KeywordTestStep testStep =
                                testCaseModificationService.addKeywordTestStep(
                                        testCase.getId(), keywordStep.getKeyword().name(), keywordStep.getActionWord());
                        testStep.setDocstring(keywordStep.getDocString());
                        testStep.setComment(keywordStep.getComment());
                        testStep.setDatatable(keywordStep.getDataTable());
                        pivotImportMetadata
                                .getTestStepsIdsMap()
                                .put(keywordStep.getInternalId(), testStep.getId());
                        logKeywordTestStepImportedSuccessfully(keywordStep, testCase, pivotFormatImport);
                    } catch (Exception e) {
                        logKeywordTestStepImportFailed(keywordStep, testCase, pivotFormatImport, e);
                        throw e;
                    }
                });
    }

    private void addVerifiedRequirementsToTestCase(
            Map<String, Long> requirementIdsMap,
            List<String> internalVerifiedRequirementIds,
            TestCase testCase) {
        List<Long> verifiedRequirementIds =
                internalVerifiedRequirementIds.stream().map(requirementIdsMap::get).toList();

        verifiedRequirementsManagerService.addVerifiedRequirementsToTestCaseUnsecured(
                verifiedRequirementIds, testCase);
    }

    private void addVerifiedRequirementsToTestStep(
            Map<String, Long> requirementIdsMap,
            ActionTestStepToImport actionTestStepToImport,
            ActionTestStep testStep,
            TestCase testCase) {
        List<String> internalVerifiedRequirementIds =
                actionTestStepToImport.getVerifiedRequirementIds();
        List<Long> verifiedRequirementIds =
                internalVerifiedRequirementIds.stream().map(requirementIdsMap::get).toList();

        verifiedRequirementsManagerService.addVerifiedRequirementsToTestStepUnsecured(
                verifiedRequirementIds, testStep, testCase);
    }

    @Override
    public void importCalledTestCasesFromZipArchive(
            ZipFile zipFile, PivotImportMetadata pivotImportMetadata, PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.CALLED_TEST_CASES, pivotFormatImport);
        ZipEntry entry = zipFile.getEntry(JsonImportFile.CALLED_TEST_CASES.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleCalledTestCasesInJsonFile(jsonInputStream, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.CALLED_TEST_CASES, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.CALLED_TEST_CASES, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleCalledTestCasesInJsonFile(
            InputStream jsonInputStream,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseCalledTestCasesArray(pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
        jsonInputStream.close();
    }

    private void parseCalledTestCasesArray(
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<CalledTestCaseStepToImport> calledTestCaseStepToImports = new ArrayList<>();
        if (JsonImportField.CALLED_TEST_CASES.equals(jsonParser.getCurrentName())) {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
                if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                    CalledTestCaseStepToImport calledTestCaseStepToImport =
                            testCaseWorkspaceParser.parseCalledTestCase(jsonParser, pivotFormatImport);
                    calledTestCaseStepToImports.add(calledTestCaseStepToImport);
                }

                if (calledTestCaseStepToImports.size() == GlobalProjectPivotImporterService.BATCH_SIZE) {
                    createCalledTestCases(
                            calledTestCaseStepToImports, pivotImportMetadata, pivotFormatImport);
                }
            }

            if (!calledTestCaseStepToImports.isEmpty()) {
                createCalledTestCases(calledTestCaseStepToImports, pivotImportMetadata, pivotFormatImport);
            }
        }
    }

    private void createCalledTestCases(
            List<CalledTestCaseStepToImport> calledTestCaseStepToImports,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        for (CalledTestCaseStepToImport calledTestCaseStepToImport : calledTestCaseStepToImports) {
            PivotFormatLoggerHelper.logEntityCreationStarted(
                    LOGGER,
                    PivotFormatLoggerHelper.CALLED_TEST_CASE,
                    calledTestCaseStepToImport.getInternalId(),
                    pivotFormatImport);
            try {
                createCalledTestCase(calledTestCaseStepToImport, pivotImportMetadata);
                PivotFormatLoggerHelper.logUnnamedEntityCreatedSuccessfully(
                        LOGGER,
                        PivotFormatLoggerHelper.CALLED_TEST_CASE,
                        calledTestCaseStepToImport.getInternalId(),
                        pivotFormatImport);

            } catch (Exception e) {
                PivotFormatLoggerHelper.handleUnnamedEntityCreationFailed(
                        LOGGER,
                        PivotFormatLoggerHelper.CALLED_TEST_CASE,
                        calledTestCaseStepToImport.getInternalId(),
                        pivotFormatImport,
                        e);
            }
        }

        calledTestCaseStepToImports.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void createCalledTestCase(
            CalledTestCaseStepToImport calledTestCaseStepToImport,
            PivotImportMetadata pivotImportMetadata) {

        Long squashCallerTestCaseId =
                pivotImportMetadata.getTestCaseIdsMap().get(calledTestCaseStepToImport.getCallerId());
        Long squashCalledTestCaseId =
                pivotImportMetadata
                        .getTestCaseIdsMap()
                        .get(calledTestCaseStepToImport.getCalledTestCaseInternalId());
        CallTestStep callTestStep;

        if (Objects.nonNull(calledTestCaseStepToImport.getIndex())) {
            callTestStep =
                    callStepManager.addCallTestStepUnsecured(
                            squashCallerTestCaseId,
                            squashCalledTestCaseId,
                            calledTestCaseStepToImport.getIndex());
        } else {
            callTestStep =
                    callStepManager.addCallTestStepUnsecured(squashCallerTestCaseId, squashCalledTestCaseId);
        }
        handleCalledTestCaseDataset(
                callTestStep,
                squashCallerTestCaseId,
                calledTestCaseStepToImport,
                pivotImportMetadata.getDatasetIdsMap());
        pivotImportMetadata
                .getTestStepsIdsMap()
                .put(calledTestCaseStepToImport.getInternalId(), callTestStep.getId());
    }

    private void handleCalledTestCaseDataset(
            CallTestStep callTestStep,
            Long squashCallerTestCaseId,
            CalledTestCaseStepToImport calledTestCaseStepToImport,
            Map<String, Long> datasetIdsMap) {
        if (ParameterAssignationMode.CALLED_DATASET.equals(
                        calledTestCaseStepToImport.getParameterAssignationMode())
                && Objects.nonNull(calledTestCaseStepToImport.getInternalDatasetId())) {
            Long squashDatasetId = datasetIdsMap.get(calledTestCaseStepToImport.getInternalDatasetId());
            callStepManager.setParameterAssignationModeUnsecured(
                    callTestStep,
                    squashCallerTestCaseId,
                    calledTestCaseStepToImport.getParameterAssignationMode(),
                    squashDatasetId);
        } else {
            callStepManager.setParameterAssignationModeUnsecured(
                    callTestStep,
                    squashCallerTestCaseId,
                    calledTestCaseStepToImport.getParameterAssignationMode(),
                    null);
        }
    }

    // LOGGER METHODS

    // DATASET PARAMS

    private void logStartingDatasetParamsImportForTestCase(
            TestCaseToImport testCaseToImport, PivotFormatImport pivotFormatImport) {
        LOGGER.debug(
                "Import Id: {} - Starting to dataset params for test case \"{}\". ",
                pivotFormatImport.getId(),
                testCaseToImport.getName());
    }

    private void logDatasetParamImportedSuccessfully(
            DatasetParamToImport datasetParamToImport,
            TestCase testCase,
            PivotFormatImport pivotFormatImport) {
        LOGGER.debug(
                "Import Id: {} - Dataset param with name {} and with internal id {} imported successfully for test case \"{}\" successfully.",
                pivotFormatImport.getId(),
                datasetParamToImport.getName(),
                datasetParamToImport.getInternalId(),
                testCase.getName());
    }

    private static void logDatasetParamImportFailed(
            DatasetParamToImport datasetParamToImport,
            TestCase testCase,
            PivotFormatImport pivotFormatImport,
            Exception e) {
        LOGGER.error(
                "Import Id: {} - Error while importing dataset param with name {} internal id {} for test case \"{}\".",
                pivotFormatImport.getId(),
                datasetParamToImport.getName(),
                datasetParamToImport.getInternalId(),
                testCase.getName(),
                e);
    }

    // TEST STEPS

    private void logStartingTestStepsImportForTestCase(
            TestCaseToImport testCaseToImport, PivotFormatImport pivotFormatImport) {
        LOGGER.debug(
                "Import Id: {} - Starting to import test steps for test case \"{}\". ",
                pivotFormatImport.getId(),
                testCaseToImport.getName());
    }

    private void logActionTestStepImportedSuccessfully(
            ActionTestStepToImport actionTestStepToImport,
            TestCase testCase,
            PivotFormatImport pivotFormatImport) {
        LOGGER.debug(
                "Import Id: {} - Action test step with internal id {} imported successfully for test case \"{}\" successfully.",
                pivotFormatImport.getId(),
                actionTestStepToImport.getInternalId(),
                testCase.getName());
    }

    private static void logActionTestStepImportFailed(
            ActionTestStepToImport actionTestStepToImport,
            TestCase testCase,
            PivotFormatImport pivotFormatImport,
            Exception e) {
        LOGGER.error(
                "Import Id: {} - Error while importing action test step with internal id {} for test case \"{}\".",
                pivotFormatImport.getId(),
                actionTestStepToImport.getInternalId(),
                testCase.getName(),
                e);
    }

    private void logKeywordTestStepImportedSuccessfully(
            KeywordTestStepToImport keywordTestStepToImport,
            TestCase testCase,
            PivotFormatImport pivotFormatImport) {
        LOGGER.debug(
                "Import Id: {} - Keyword test step with internal id {} imported successfully for test case \"{}\" successfully.",
                pivotFormatImport.getId(),
                keywordTestStepToImport.getInternalId(),
                testCase.getName());
    }

    private static void logKeywordTestStepImportFailed(
            KeywordTestStepToImport keywordTestStepToImport,
            TestCase testCase,
            PivotFormatImport pivotFormatImport,
            Exception e) {
        LOGGER.error(
                "Import Id: {} - Error while importing keyword test step with internal id {} for test case \"{}\"",
                pivotFormatImport.getId(),
                keywordTestStepToImport.getInternalId(),
                testCase.getName(),
                e);
    }
}
