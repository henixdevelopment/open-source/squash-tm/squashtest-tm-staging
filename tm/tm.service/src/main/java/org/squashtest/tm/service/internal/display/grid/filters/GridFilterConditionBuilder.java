/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.filters;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Map;
import java.util.function.Function;
import org.jooq.Condition;
import org.jooq.Field;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

public interface GridFilterConditionBuilder {
    Condition build();

    /**
     * Default factory that link an {@link GridFilterOperation} to an implementation of {@link
     * GridFilterConditionBuilder}. If it doesn't feat to specific needs for a given table, another
     * factory and probably another Collector for filters must be implemented. See {@link
     * org.squashtest.tm.service.internal.display.grid.filters.collector.DefaultFilterCollector} for
     * further details.
     *
     * @param gridFilterValue the filter value
     * @return {@link GridFilterConditionBuilder}
     */
    static GridFilterConditionBuilder getConditionBuilder(
            Field<?> field, GridFilterValue gridFilterValue) {
        requireNonNull(gridFilterValue.getOperation());
        GridFilterOperation operation = GridFilterOperation.valueOf(gridFilterValue.getOperation());
        return getConditionBuilder(field, gridFilterValue, operation);
    }

    static GridFilterConditionBuilder getConditionBuilder(
            Field<?> field, GridFilterValue gridFilterValue, GridFilterOperation operation) {
        requireNonNull(gridFilterValue);
        return switch (operation) {
            case LIKE -> new LikeConditionBuilder(field, gridFilterValue);
            case IN -> new InConditionBuilder(field, gridFilterValue);
            case BETWEEN -> new BetweenConditionBuilder<>(field, gridFilterValue);
            case EQUALS -> new EqualConditionBuilder(field, gridFilterValue, false);
            case NOT_EQUALS -> new EqualConditionBuilder(field, gridFilterValue, true);
            case GREATER -> new GreaterConditionBuilder<>(field, gridFilterValue, false);
            case GREATER_EQUAL -> new GreaterConditionBuilder<>(field, gridFilterValue, true);
            case LOWER -> new LowerConditionBuilder<>(field, gridFilterValue, false);
            case LOWER_EQUAL -> new LowerConditionBuilder<>(field, gridFilterValue, true);
            case FULLTEXT -> new FulltextConditionBuilder<>(field, gridFilterValue);
            default ->
                    throw new IllegalArgumentException(
                            "No GridFilterConditionBuilder for operation " + operation);
        };
    }

    @SuppressWarnings("unchecked")
    static <T> T getGridFilterValueConvert(Field<T> target, String value) {
        Class<?> clazz = target.getDataType().getType();
        return (T)
                getConverters()
                        .getOrDefault(
                                clazz,
                                s -> {
                                    throw new IllegalArgumentException(
                                            String.format("No GridFilterValueConvert for type %s", clazz));
                                })
                        .apply(value);
    }

    private static Map<Class<?>, Function<String, ?>> getConverters() {
        return Map.of(
                String.class, s -> s,
                Integer.class, Integer::valueOf,
                Long.class, Long::valueOf,
                Double.class, Double::valueOf,
                BigDecimal.class, BigDecimal::new,
                Timestamp.class, s -> Timestamp.valueOf(LocalDate.parse(s).atStartOfDay()));
    }
}
