/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.Date;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.annotation.Id;

public interface CustomSprintModificationService {

    void rename(long sprintId, String newName);

    void updateStatusBySprintId(Long sprintId, SprintStatus newStatus);

    void denormalizeSprintReqVersions(Long sprintId);

    void updateDescription(@Id long sprintId, String newDescription);

    void updateStartDate(@Id long sprintId, Date newStartDate);

    void updateEndDate(@Id long sprintId, Date newEndDate);
}
