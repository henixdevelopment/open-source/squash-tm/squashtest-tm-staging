/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase;

import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.importer.EntityType;
import org.squashtest.tm.service.internal.batchimport.column.ExcelWorkbookParser;
import org.squashtest.tm.service.internal.batchimport.excel.ExcelBatchImporter;
import org.squashtest.tm.service.internal.batchimport.instruction.container.CoverageInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.DatasetInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.DatasetParamValueInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.Importer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.ParameterInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.StepInstructionContainer;
import org.squashtest.tm.service.internal.batchimport.instruction.container.TestCaseInstructionContainer;

@Component
public class TestCaseExcelBatchImporter extends ExcelBatchImporter {

    private static final List<EntityType> TC_ENTITIES_ORDERED_BY_INSTRUCTION_ORDER =
            Arrays.asList(
                    EntityType.TEST_CASE,
                    EntityType.PARAMETER,
                    EntityType.DATASET,
                    EntityType.TEST_STEP,
                    EntityType.DATASET_PARAM_VALUES,
                    EntityType.COVERAGE);

    public TestCaseExcelBatchImporter() {
        super(LoggerFactory.getLogger(TestCaseExcelBatchImporter.class));
    }

    @Override
    public void addInstructionsByEntity(
            ExcelWorkbookParser parser, EntityType entityType, Importer importer) {

        LOGGER.debug("creating instruction container for entity type : {}", entityType);

        switch (entityType) {
            case TEST_CASE ->
                    addInstructionsToImporter(
                            parser.getTestCaseInstructions(), importer, TestCaseInstructionContainer::new);
            case PARAMETER ->
                    addInstructionsToImporter(
                            parser.getParameterInstructions(), importer, ParameterInstructionContainer::new);
            case DATASET ->
                    addInstructionsToImporter(
                            parser.getDatasetInstructions(), importer, DatasetInstructionContainer::new);
            case TEST_STEP ->
                    addInstructionsToImporter(
                            parser.getTestStepInstructions(), importer, StepInstructionContainer::new);
            case DATASET_PARAM_VALUES ->
                    addInstructionsToImporter(
                            parser.getDatasetParamValuesInstructions(),
                            importer,
                            DatasetParamValueInstructionContainer::new);
            case COVERAGE ->
                    addInstructionsToImporter(
                            parser.getCoverageInstructions(), importer, CoverageInstructionContainer::new);
            default ->
                    throw new IllegalArgumentException("Unknown EntityType for TestCase import : " + this);
        }
    }

    @Override
    public List<EntityType> getEntityType() {
        return TC_ENTITIES_ORDERED_BY_INSTRUCTION_ORDER;
    }
}
