/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.campaign.CustomIterationModificationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.ProjectIdsReferences;
import org.squashtest.tm.service.internal.dto.projectimporter.TestSuiteInfo;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.CampaignToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.ExecutionStepInfo;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.ExecutionToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.IterationToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.TestSuiteToImport;
import org.squashtest.tm.service.projectimporter.pivotimporter.AttachmentPivotImportService;
import org.squashtest.tm.service.projectimporter.pivotimporter.ExecutionWorkspacePivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.ExecutionWorkspaceParser;

@Service("ExecutionWorkspacePivotImporterService")
public class ExecutionWorkspacePivotImporterServiceImpl
        implements ExecutionWorkspacePivotImporterService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ExecutionWorkspacePivotImporterService.class);
    private final CampaignLibraryNavigationService campaignLibraryNavigationService;
    private final CampaignTestPlanManagerService campaignTestPlanManagerService;
    private final IterationTestPlanManagerService iterationTestPlanManagerService;
    private final TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;
    private final IterationModificationService iterationModificationService;
    private final PrivateCustomFieldValueService privateCustomFieldValueService;
    private final CustomIterationModificationService customIterationModificationService;

    private final ExecutionWorkspaceParser executionWorkspaceParser;
    private final IterationTestPlanManagerService testPlanService;
    private final AttachmentPivotImportService attachmentPivotImportService;
    private final DSLContext dsl;

    @PersistenceContext private EntityManager entityManager;

    public ExecutionWorkspacePivotImporterServiceImpl(
            CampaignLibraryNavigationService campaignLibraryNavigationService,
            CampaignTestPlanManagerService campaignTestPlanManagerService,
            IterationTestPlanManagerService iterationTestPlanManagerService,
            TestSuiteTestPlanManagerService testSuiteTestPlanManagerService,
            IterationModificationService iterationModificationService,
            PrivateCustomFieldValueService privateCustomFieldValueService,
            CustomIterationModificationService customIterationModificationService,
            ExecutionWorkspaceParser executionWorkspaceParser,
            IterationTestPlanManagerService testPlanService,
            AttachmentPivotImportService attachmentPivotImportService,
            DSLContext dsl) {
        this.campaignLibraryNavigationService = campaignLibraryNavigationService;
        this.campaignTestPlanManagerService = campaignTestPlanManagerService;
        this.iterationTestPlanManagerService = iterationTestPlanManagerService;
        this.testSuiteTestPlanManagerService = testSuiteTestPlanManagerService;
        this.iterationModificationService = iterationModificationService;
        this.privateCustomFieldValueService = privateCustomFieldValueService;
        this.customIterationModificationService = customIterationModificationService;
        this.executionWorkspaceParser = executionWorkspaceParser;
        this.testPlanService = testPlanService;
        this.attachmentPivotImportService = attachmentPivotImportService;
        this.dsl = dsl;
    }

    @Override
    public void importCampaignsFromZipArchive(
            ZipFile zipFile,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.CAMPAIGNS, pivotFormatImport);

        ZipEntry entry = zipFile.getEntry(JsonImportFile.CAMPAIGNS.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleCampaignsFromJsonFile(
                        jsonInputStream, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.CAMPAIGNS, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.CAMPAIGNS, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleCampaignsFromJsonFile(
            InputStream jsonInputStream,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseCampaignsArray(
                        projectIdsReferences, pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
    }

    private void parseCampaignsArray(
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<CampaignToImport> campaignsToImport = new ArrayList<>();
        if (!JsonImportField.CAMPAIGNS.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                CampaignToImport campaignToImport =
                        executionWorkspaceParser.parseCampaign(
                                jsonParser, pivotImportMetadata, pivotFormatImport);
                campaignsToImport.add(campaignToImport);
            }

            if (campaignsToImport.size() == GlobalProjectPivotImporterService.BATCH_SIZE) {
                createCampaigns(
                        campaignsToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
            }
        }

        if (!campaignsToImport.isEmpty()) {
            createCampaigns(
                    campaignsToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
        }
    }

    private void createCampaigns(
            List<CampaignToImport> campaignsToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {

        for (CampaignToImport campaignToImport : campaignsToImport) {
            PivotFormatLoggerHelper.logEntityCreationStarted(
                    LOGGER,
                    PivotFormatLoggerHelper.CAMPAIGN,
                    campaignToImport.getInternalId(),
                    pivotFormatImport);
            try {
                createCampaign(
                        campaignToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logEntityCreatedSuccessfully(
                        LOGGER,
                        PivotFormatLoggerHelper.CAMPAIGN,
                        campaignToImport.getName(),
                        campaignToImport.getInternalId(),
                        pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.handleEntityCreationFailed(
                        LOGGER,
                        PivotFormatLoggerHelper.CAMPAIGN,
                        campaignToImport.getName(),
                        campaignToImport.getInternalId(),
                        pivotFormatImport,
                        e);
            }
        }

        campaignsToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void createCampaign(
            CampaignToImport campaignToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        Long squashFolderTargetId =
                pivotImportMetadata.getCampaignFoldersIdsMap().get(campaignToImport.getParentId());

        Campaign squashCampaign =
                switch (campaignToImport.getParentType()) {
                    case CAMPAIGN_LIBRARY ->
                            campaignLibraryNavigationService.addCampaignToCampaignLibraryUnsecured(
                                    projectIdsReferences.getCampaignLibraryId(),
                                    campaignToImport.toCampaign(),
                                    campaignToImport.getCustomFields());

                    case CAMPAIGN_FOLDER ->
                            campaignLibraryNavigationService.addCampaignToCampaignFolderUnsecured(
                                    squashFolderTargetId,
                                    campaignToImport.toCampaign(),
                                    campaignToImport.getCustomFields());
                    default ->
                            throw new IllegalArgumentException(
                                    GlobalProjectPivotImporterService.ENTITY_TYPE_NOT_HANDLED
                                            + campaignToImport.getParentType());
                };

        pivotImportMetadata
                .getCampaignIdsMap()
                .put(campaignToImport.getInternalId(), squashCampaign.getId());
        addTestCaseToCampaignTestPlan(
                campaignToImport.getTestPlanTestCaseIds(), pivotImportMetadata, squashCampaign.getId());

        attachmentPivotImportService.addAttachmentsToEntity(
                campaignToImport.getAttachments(),
                new AttachmentHolderInfo(
                        squashCampaign.getId(),
                        squashCampaign.getAttachmentList().getId(),
                        EntityType.CAMPAIGN,
                        campaignToImport.getInternalId()),
                pivotFormatImport,
                pivotImportMetadata);
    }

    private void addTestCaseToCampaignTestPlan(
            List<String> testPlanTestCaseIds, PivotImportMetadata pivotImportMetadata, Long campaignId) {
        List<Long> squashTestCaseIds =
                testPlanTestCaseIds.stream()
                        .map(internalId -> pivotImportMetadata.getTestCaseIdsMap().get(internalId))
                        .toList();
        campaignTestPlanManagerService.addTestCasesToCampaignTestPlanUnsecured(
                squashTestCaseIds, campaignId);
    }

    @Override
    public void importIterationsFromZipArchive(
            ZipFile zipFile, PivotImportMetadata pivotImportMetadata, PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.ITERATIONS, pivotFormatImport);

        ZipEntry entry = zipFile.getEntry(JsonImportFile.ITERATIONS.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleIterationsFromJsonFile(jsonInputStream, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.ITERATIONS, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.ITERATIONS, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleIterationsFromJsonFile(
            InputStream jsonInputStream,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseIterationsArray(pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
    }

    private void parseIterationsArray(
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<IterationToImport> iterationsToImport = new ArrayList<>();
        if (!JsonImportField.ITERATIONS.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                IterationToImport iterationToImport =
                        executionWorkspaceParser.parseIteration(
                                jsonParser, pivotImportMetadata, pivotFormatImport);
                iterationsToImport.add(iterationToImport);
            }

            if (iterationsToImport.size() == GlobalProjectPivotImporterService.BATCH_SIZE) {
                createIterations(iterationsToImport, pivotImportMetadata, pivotFormatImport);
            }
        }

        if (!iterationsToImport.isEmpty()) {
            createIterations(iterationsToImport, pivotImportMetadata, pivotFormatImport);
        }
    }

    private void createIterations(
            List<IterationToImport> iterationsToImport,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {

        for (IterationToImport iterationToImport : iterationsToImport) {
            PivotFormatLoggerHelper.logEntityCreationStarted(
                    LOGGER,
                    PivotFormatLoggerHelper.ITERATION,
                    iterationToImport.getInternalId(),
                    pivotFormatImport);
            try {
                createIteration(iterationToImport, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logEntityCreatedSuccessfully(
                        LOGGER,
                        PivotFormatLoggerHelper.ITERATION,
                        iterationToImport.getName(),
                        iterationToImport.getInternalId(),
                        pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.handleEntityCreationFailed(
                        LOGGER,
                        PivotFormatLoggerHelper.ITERATION,
                        iterationToImport.getName(),
                        iterationToImport.getInternalId(),
                        pivotFormatImport,
                        e);
            }
        }

        iterationsToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void createIteration(
            IterationToImport iterationToImport,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        Long squashTargetId =
                pivotImportMetadata.getCampaignIdsMap().get(iterationToImport.getParentId());
        Iteration squashIteration =
                customIterationModificationService.addIterationToCampaignUnsecured(
                        iterationToImport.toIteration(),
                        squashTargetId,
                        false,
                        iterationToImport.getCustomFields());

        pivotImportMetadata
                .getIterationIdsMap()
                .put(iterationToImport.getInternalId(), squashIteration.getId());
        addTestCaseToIterationTestPlan(
                iterationToImport.getTestPlanTestCaseIds(), pivotImportMetadata, squashIteration.getId());

        attachmentPivotImportService.addAttachmentsToEntity(
                iterationToImport.getAttachments(),
                new AttachmentHolderInfo(
                        squashIteration.getId(),
                        squashIteration.getAttachmentList().getId(),
                        EntityType.ITERATION,
                        iterationToImport.getInternalId()),
                pivotFormatImport,
                pivotImportMetadata);
    }

    private void addTestCaseToIterationTestPlan(
            List<String> testPlanTestCaseIds, PivotImportMetadata pivotImportMetadata, Long testSuiteId) {
        List<Long> squashTestCaseIds =
                testPlanTestCaseIds.stream()
                        .map(internalId -> pivotImportMetadata.getTestCaseIdsMap().get(internalId))
                        .toList();
        iterationTestPlanManagerService.addTestCasesToIterationUnsecured(
                squashTestCaseIds, testSuiteId);
    }

    @Override
    public void importTestSuitesFromZipArchive(
            ZipFile zipFile, PivotImportMetadata pivotImportMetadata, PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.TEST_SUITES, pivotFormatImport);

        ZipEntry entry = zipFile.getEntry(JsonImportFile.TEST_SUITES.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleTestSuitesFromJsonFile(jsonInputStream, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.TEST_SUITES, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.TEST_SUITES, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleTestSuitesFromJsonFile(
            InputStream jsonInputStream,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseTestSuitesArray(pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
    }

    private void parseTestSuitesArray(
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<TestSuiteToImport> testSuitesToImport = new ArrayList<>();
        if (!JsonImportField.TEST_SUITES.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                TestSuiteToImport testSuiteToImport =
                        executionWorkspaceParser.parseTestSuite(
                                jsonParser, pivotImportMetadata, pivotFormatImport);
                testSuitesToImport.add(testSuiteToImport);
            }

            if (testSuitesToImport.size() == GlobalProjectPivotImporterService.BATCH_SIZE) {
                createTestSuites(testSuitesToImport, pivotImportMetadata, pivotFormatImport);
            }
        }

        if (!testSuitesToImport.isEmpty()) {
            createTestSuites(testSuitesToImport, pivotImportMetadata, pivotFormatImport);
        }
    }

    private void createTestSuites(
            List<TestSuiteToImport> testSuitesToImport,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        for (TestSuiteToImport testSuiteToImport : testSuitesToImport) {
            PivotFormatLoggerHelper.logEntityCreationStarted(
                    LOGGER,
                    PivotFormatLoggerHelper.TEST_SUITE,
                    testSuiteToImport.getInternalId(),
                    pivotFormatImport);
            try {
                createTestSuite(testSuiteToImport, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logEntityCreatedSuccessfully(
                        LOGGER,
                        PivotFormatLoggerHelper.TEST_SUITE,
                        testSuiteToImport.getName(),
                        testSuiteToImport.getInternalId(),
                        pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.handleEntityCreationFailed(
                        LOGGER,
                        PivotFormatLoggerHelper.TEST_SUITE,
                        testSuiteToImport.getName(),
                        testSuiteToImport.getInternalId(),
                        pivotFormatImport,
                        e);
            }
        }

        testSuitesToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void createTestSuite(
            TestSuiteToImport testSuiteToImport,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        Long squashTargetId =
                pivotImportMetadata.getIterationIdsMap().get(testSuiteToImport.getParentId());
        TestSuite squashTestSuite =
                campaignLibraryNavigationService.addTestSuiteToIterationUnsecured(
                        squashTargetId, testSuiteToImport.toTestSuite(), testSuiteToImport.getCustomFields());
        pivotImportMetadata
                .getTestSuiteIdsMap()
                .put(
                        testSuiteToImport.getInternalId(),
                        new TestSuiteInfo(squashTestSuite.getId(), testSuiteToImport.getExecutionStatus()));
        addTestCaseToTestSuiteTestPlan(
                testSuiteToImport.getTestPlanTestCaseIds(), pivotImportMetadata, squashTestSuite.getId());
        squashTestSuite.setExecutionStatus(testSuiteToImport.getExecutionStatus());

        attachmentPivotImportService.addAttachmentsToEntity(
                testSuiteToImport.getAttachments(),
                new AttachmentHolderInfo(
                        squashTestSuite.getId(),
                        squashTestSuite.getAttachmentList().getId(),
                        EntityType.TEST_SUITE,
                        testSuiteToImport.getInternalId()),
                pivotFormatImport,
                pivotImportMetadata);
    }

    private void addTestCaseToTestSuiteTestPlan(
            List<String> testPlanTestCaseIds, PivotImportMetadata pivotImportMetadata, Long testSuiteId) {
        List<Long> squashTestCaseIds =
                testPlanTestCaseIds.stream()
                        .map(internalId -> pivotImportMetadata.getTestCaseIdsMap().get(internalId))
                        .toList();
        testSuiteTestPlanManagerService.addTestCasesToIterationAndTestSuiteUnsecured(
                squashTestCaseIds, testSuiteId);
    }

    @Override
    public void importExecutionsFromZipArchive(
            ZipFile zipFile, PivotImportMetadata pivotImportMetadata, PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.EXECUTIONS, pivotFormatImport);

        ZipEntry entry = zipFile.getEntry(JsonImportFile.EXECUTIONS.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleExecutionsFromJsonFile(jsonInputStream, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.EXECUTIONS, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.EXECUTIONS, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleExecutionsFromJsonFile(
            InputStream jsonInputStream,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseExecutionsArray(pivotImportMetadata, jsonParser, pivotFormatImport);
            }
        }
    }

    private void parseExecutionsArray(
            PivotImportMetadata pivotImportMetadata,
            JsonParser jsonParser,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<ExecutionToImport> executionsToImport = new ArrayList<>();
        if (!JsonImportField.EXECUTIONS.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                ExecutionToImport executionToImport =
                        executionWorkspaceParser.parseExecution(
                                jsonParser, pivotImportMetadata, pivotFormatImport);
                executionsToImport.add(executionToImport);
            }

            if (executionsToImport.size() == GlobalProjectPivotImporterService.BATCH_SIZE) {
                createExecutions(executionsToImport, pivotImportMetadata, pivotFormatImport);
            }
        }

        if (!executionsToImport.isEmpty()) {
            createExecutions(executionsToImport, pivotImportMetadata, pivotFormatImport);
        }
    }

    private void createExecutions(
            List<ExecutionToImport> executionsToImport,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        for (ExecutionToImport executionToImport : executionsToImport) {
            PivotFormatLoggerHelper.logEntityCreationStarted(
                    LOGGER,
                    PivotFormatLoggerHelper.EXECUTION,
                    executionToImport.getInternalId(),
                    pivotFormatImport);

            try {
                createExecution(executionToImport, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logUnnamedEntityCreatedSuccessfully(
                        LOGGER,
                        PivotFormatLoggerHelper.EXECUTION,
                        executionToImport.getInternalId(),
                        pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.handleUnnamedEntityCreationFailed(
                        LOGGER,
                        PivotFormatLoggerHelper.EXECUTION,
                        executionToImport.getInternalId(),
                        pivotFormatImport,
                        e);
            }
        }

        executionsToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void createExecution(
            ExecutionToImport executionToImport,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        Long testPlanItemId =
                executionToImport.getParentType().equals(EntityType.ITERATION)
                        ? findTestPlanItemIdForIteration(executionToImport, pivotImportMetadata)
                        : findTestPlanItemIdForTestSuite(executionToImport, pivotImportMetadata);

        Execution execution = iterationModificationService.addManualExecutionUnsecured(testPlanItemId);
        execution.setDescription(executionToImport.getComment());
        execution.setLastExecutedBy(executionToImport.getLastExecutedBy());
        execution.setLastExecutedOn(executionToImport.getLastExecutedOn());

        if (Objects.nonNull(executionToImport.getExecutionStatus())) {
            execution.setExecutionStatus(executionToImport.getExecutionStatus());
        }

        attachmentPivotImportService.addAttachmentsToEntity(
                executionToImport.getAttachments(),
                new AttachmentHolderInfo(
                        execution.getId(),
                        execution.getAttachmentList().getId(),
                        EntityType.EXECUTION,
                        executionToImport.getInternalId()),
                pivotFormatImport,
                pivotImportMetadata);

        privateCustomFieldValueService.initCustomFieldValues(
                execution, executionToImport.getCustomFields());
        updateExecutionSteps(execution, executionToImport, pivotImportMetadata, pivotFormatImport);

        if (executionToImport.getParentType().equals(EntityType.TEST_SUITE)) {
            TestSuiteInfo testSuiteInfo =
                    pivotImportMetadata.getTestSuiteIdsMap().get(executionToImport.getParentId());
            TestSuite testSuite = entityManager.find(TestSuite.class, testSuiteInfo.id());
            testSuite.setExecutionStatus(testSuiteInfo.status());
        }
        IterationTestPlanItem testPlan = execution.getTestPlan();
        testPlanService.updateMetadata(testPlan);
    }

    private void updateExecutionSteps(
            Execution execution,
            ExecutionToImport executionToImport,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {

        List<Long> executionStepIds = execution.getSteps().stream().map(ExecutionStep::getId).toList();
        Map<Long, Long> referencedTestStepIdByExecutionStepIdMap =
                findReferencedTestStepIdForExecutionStepIds(executionStepIds);

        for (ExecutionStepInfo executionStepInfo : executionToImport.getExecutionSteps()) {
            Long testStepId =
                    pivotImportMetadata.getTestStepsIdsMap().get(executionStepInfo.getTestStepId());
            ExecutionStep executionStep =
                    execution.getSteps().stream()
                            .filter(
                                    step ->
                                            referencedTestStepIdByExecutionStepIdMap.get(step.getId()).equals(testStepId))
                            .findFirst()
                            .orElse(null);

            if (Objects.nonNull(executionStep)) {
                updateExecutionStep(
                        execution,
                        executionToImport,
                        executionStepInfo,
                        executionStep,
                        pivotFormatImport,
                        pivotImportMetadata);
            }
        }
    }

    private void updateExecutionStep(
            Execution execution,
            ExecutionToImport executionToImport,
            ExecutionStepInfo executionStepInfo,
            ExecutionStep executionStep,
            PivotFormatImport pivotFormatImport,
            PivotImportMetadata pivotImportMetadata)
            throws IOException {
        executionStep.setComment(executionStepInfo.getComment());
        executionStep.setExecutionStatus(executionStepInfo.getStatus());
        executionStep.setLastExecutedBy(executionStepInfo.getLastExecutedBy());
        executionStep.setLastExecutedOn(executionStepInfo.getLastExecutedOn());
        privateCustomFieldValueService.initCustomFieldValues(
                executionStep, executionToImport.getCustomFields());

        attachmentPivotImportService.addAttachmentsToEntity(
                executionStepInfo.getAttachments(),
                new AttachmentHolderInfo(
                        execution.getId(),
                        executionStep.getAttachmentList().getId(),
                        EntityType.EXECUTION_STEP,
                        executionToImport.getInternalId()),
                pivotFormatImport,
                pivotImportMetadata);
    }

    private Long findTestPlanItemIdForIteration(
            ExecutionToImport execution, PivotImportMetadata pivotImportMetadata) {
        Long squashTestCaseId = pivotImportMetadata.getTestCaseIdsMap().get(execution.getTestCaseId());
        Long squashDatasetId = pivotImportMetadata.getDatasetIdsMap().get(execution.getDatasetId());
        Long squashIterationId = pivotImportMetadata.getIterationIdsMap().get(execution.getParentId());

        SelectConditionStep<Record1<Long>> baseQuery =
                dsl.select(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .leftJoin(ITEM_TEST_PLAN_LIST)
                        .on(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                        .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(squashTestCaseId))
                        .and(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(squashIterationId))
                        .and(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.notIn(
                                        dsl.select(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID).from(TEST_SUITE_TEST_PLAN_ITEM)));

        if (Objects.nonNull(squashDatasetId)) {
            return baseQuery
                    .and(ITERATION_TEST_PLAN_ITEM.DATASET_ID.eq(squashDatasetId))
                    .fetchOneInto(Long.class);
        } else {
            return baseQuery.fetchOneInto(Long.class);
        }
    }

    private Long findTestPlanItemIdForTestSuite(
            ExecutionToImport execution, PivotImportMetadata pivotImportMetadata) {
        Long squashTestCaseId = pivotImportMetadata.getTestCaseIdsMap().get(execution.getTestCaseId());
        Long squashDatasetId = pivotImportMetadata.getDatasetIdsMap().get(execution.getDatasetId());
        Long squashSuiteId = pivotImportMetadata.getTestSuiteIdsMap().get(execution.getParentId()).id();

        SelectConditionStep<Record1<Long>> baseQuery =
                dsl.select(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
                        .from(ITERATION_TEST_PLAN_ITEM)
                        .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                        .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(squashTestCaseId))
                        .and(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(squashSuiteId));

        if (Objects.nonNull(squashDatasetId)) {
            return baseQuery
                    .and(ITERATION_TEST_PLAN_ITEM.DATASET_ID.eq(squashDatasetId))
                    .fetchOneInto(Long.class);
        } else {
            return baseQuery.fetchOneInto(Long.class);
        }
    }

    private Map<Long, Long> findReferencedTestStepIdForExecutionStepIds(List<Long> executionStepIds) {
        return dsl.select(EXECUTION_STEP.TEST_STEP_ID, EXECUTION_STEP.EXECUTION_STEP_ID)
                .from(EXECUTION_STEP)
                .where(EXECUTION_STEP.EXECUTION_STEP_ID.in(executionStepIds))
                .fetchMap(EXECUTION_STEP.EXECUTION_STEP_ID, EXECUTION_STEP.TEST_STEP_ID);
    }
}
