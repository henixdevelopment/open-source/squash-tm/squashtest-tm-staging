/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.remotesynchronisation;

import org.squashtest.tm.exception.sync.PathAlreadyInUseException;
import org.squashtest.tm.exception.sync.PathContainsASprintGroupException;

public interface RemoteSynchronisationService {

    /**
     * Checks if synchonisationPath is available in requirement-workspace
     *
     * @param projectName Name of the project of the synchronisation
     * @param synchronisationPath The path to check, without project name (<i>eg: "folder/target"</i>)
     * @throws PathAlreadyInUseException If the exact same path already exists in a {@link
     *     org.squashtest.tm.domain.synchronisation.RemoteSynchronisation}
     */
    void checkPathAvailability(String projectName, String synchronisationPath);

    /**
     * Checks if sprintSynchonisationPath is available in execution-workspace
     *
     * @param projectName Name of the project of the synchronisation
     * @param sprintSynchronisationPath The path to check, without project name (<i>eg:
     *     "folder/sprintGroup"</i>)
     * @throws PathAlreadyInUseException If the exact same path already exists in a {@link
     *     org.squashtest.tm.domain.synchronisation.RemoteSynchronisation}
     * @throws PathContainsASprintGroupException If first parts of the path already exists in a
     *     RemoteSynchronisation (<i>eg: If the path was "folder/sprintGroup/target" and an existing
     *     RemoteSynchronisation's sprintSynchronisationPath was "folder/sprintGroup" </i>)
     */
    void checkSprintPathAvailability(String projectName, String sprintSynchronisationPath);
}
