/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.selectCount;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONES_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STATUS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.service.grid.ColumnIds;
import org.squashtest.tm.service.internal.batchexport.models.CoverageModel;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;
import org.squashtest.tm.service.internal.batchexport.models.LinkedLowLevelRequirementModel;
import org.squashtest.tm.service.internal.batchexport.models.RequirementExportModel;
import org.squashtest.tm.service.internal.batchexport.models.RequirementLinkModel;
import org.squashtest.tm.service.internal.batchexport.models.RequirementModel;
import org.squashtest.tm.service.internal.batchexport.models.RequirementModelFromJooq;
import org.squashtest.tm.service.internal.library.PathService;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.repository.hibernate.EasyConstructorResultTransformer;
import org.squashtest.tm.service.requirement.HighLevelRequirementService;

@Repository
public class RequirementExportDao {

    private static final String VERSION_IDS = "versionIds";
    private static final String IS_HIGH_LEVEL = "isHighLevel";

    @PersistenceContext private EntityManager em;

    @Inject private DSLContext dsl;

    @Inject private PathService pathService;

    @Inject private HighLevelRequirementService highLevelRequirementService;

    public RequirementExportModel findAllRequirementModel(List<Long> versionIds) {

        RequirementExportModel model;
        model = populateRequirementExportModel(versionIds);

        List<CoverageModel> coverageModels = findRequirementVersionCoverageModels(versionIds);
        List<RequirementLinkModel> linkModels = findRequirementLinkModels(versionIds);
        List<LinkedLowLevelRequirementModel> linkedLowLevelReqModels =
                findLinkedLowLevelReqModels(versionIds);

        ExportDaoUtils.setPathForCoverage(em, pathService, coverageModels);
        model.setCoverages(coverageModels);
        model.setReqLinks(linkModels);
        model.setLinkedLowLevelReqs(linkedLowLevelReqModels);

        return model;
    }

    public RequirementExportModel populateRequirementExportModel(List<Long> versionIds) {

        RequirementExportModel model = new RequirementExportModel();

        List<RequirementModel> requirementsModel = findRequirementModel(versionIds);

        model.setRequirementsModels(requirementsModel);

        return model;
    }

    public RequirementExportModel populateRequirementExportModelFromJooq(List<Long> versionIds) {

        RequirementExportModel model = new RequirementExportModel();
        List<RequirementModelFromJooq> requirementsModelFromJooqs =
                findRequirementModelFromJooq(versionIds);

        model.setRequirementsModelsFromJooq(requirementsModelFromJooqs);

        for (RequirementModelFromJooq requirementsModelFromJooq : requirementsModelFromJooqs) {
            Session session = ExportDaoUtils.getStatelessSession(em);
            Query q = session.getNamedQuery("requirement.excelRequirementExportCUF");
            q.setLong("requirementVersionId", requirementsModelFromJooq.getId());
            q.setResultTransformer(new EasyConstructorResultTransformer(CustomField.class));
            requirementsModelFromJooq.setCufs(q.list());
        }

        return model;
    }

    private List<CoverageModel> findRequirementVersionCoverageModels(List<Long> versionIds) {
        return ExportDaoUtils.loadModels(
                em, "requirementVersion.excelExportCoverage", versionIds, VERSION_IDS, CoverageModel.class);
    }

    private List<RequirementLinkModel> findRequirementLinkModels(List<Long> versionIds) {
        // get the models
        List<RequirementLinkModel> models =
                ExportDaoUtils.loadModels(
                        em,
                        "requirementVersion.excelExportRequirementLinks",
                        versionIds,
                        VERSION_IDS,
                        RequirementLinkModel.class);

        /*
         * more complex part : computing the pathes. The steps are the following :
         * - gather all the requirement ids involved
         * - gather the paths for all these requirements and map them by id
         * - for each model, assign the paths.
         */
        List<Long> reqIds = gatherRequirementIdsFromLinkModels(models);
        Map<Long, String> pathById = gatherRequirementPaths(reqIds);
        assignPaths(models, pathById);

        return models;
    }

    private List<LinkedLowLevelRequirementModel> findLinkedLowLevelReqModels(List<Long> versionIds) {
        List<LinkedLowLevelRequirementModel> models = new ArrayList<>();
        Map<Long, List<Long>> map =
                highLevelRequirementService.findLinkedLowLevelReqIdsMappedByHighLevelReqIdFromVersionIds(
                        versionIds);
        for (Entry<Long, List<Long>> mapped : map.entrySet()) {
            mapped
                    .getValue()
                    .forEach(
                            it -> {
                                LinkedLowLevelRequirementModel model =
                                        new LinkedLowLevelRequirementModel(mapped.getKey(), it, null, null);
                                models.add(model);
                            });
        }

        /*
         * more complex part : computing the pathes. The steps are the following :
         * - gather all the requirement ids involved
         * - gather the paths for all these requirements and map them by id
         * - for each model, assign the paths.
         */
        List<Long> reqIds = gatherRequirementIdsFromLinkedLowLevelReqModels(models);
        Map<Long, String> pathById = gatherRequirementPaths(reqIds);
        assignLinkedLowReqPaths(models, pathById);

        return models;
    }

    private List<RequirementModel> findRequirementModel(List<Long> versionIds) {
        List<RequirementModel> requirementModels =
                ExportDaoUtils.loadModels(
                        em, "requirement.findVersionsModels", versionIds, VERSION_IDS, RequirementModel.class);
        getOtherProperties(requirementModels);
        return requirementModels;
    }

    private void getOtherProperties(List<RequirementModel> requirementModels) {
        for (RequirementModel requirementModel : requirementModels) {
            requirementModel.setPath(getPathAsString(requirementModel));
            getModelRequirementPosition(requirementModel);
            getModelRequirementCUF(requirementModel);
        }
    }

    public String getPathAsString(RequirementModel exportedRequirement) {
        return ExportDaoUtils.getRequirementPath(
                em, exportedRequirement.getRequirementId(), exportedRequirement.getProjectName());
    }

    @SuppressWarnings("unchecked")
    private void getModelRequirementCUF(RequirementModel requirementModel) {
        Session session = ExportDaoUtils.getStatelessSession(em);
        Query q = session.getNamedQuery("requirement.excelRequirementExportCUF");
        q.setLong("requirementVersionId", requirementModel.getId());
        q.setResultTransformer(new EasyConstructorResultTransformer(CustomField.class));
        requirementModel.setCufs(q.list());
    }

    private void getModelRequirementPosition(RequirementModel requirementModel) {
        Long reqId = requirementModel.getRequirementId();
        int index = getRequirementPositionInLibrary(reqId);
        if (index == 0) {
            index = getRequirementPositionInFolder(reqId);
        }
        if (index == 0) {
            index = getPositionChildrenRequirement(reqId);
        }
        requirementModel.setRequirementIndex(index);
    }

    private int getPositionChildrenRequirement(Long reqId) {
        return ExportDaoUtils.requirementVersionQuery(
                em, "requirement.findVersionsModelsIndexChildrenRequirement", reqId, 0);
    }

    private int getRequirementPositionInFolder(Long reqId) {
        return ExportDaoUtils.requirementVersionQuery(
                em, "requirement.findVersionsModelsIndexInFolder", reqId, 0);
    }

    private int getRequirementPositionInLibrary(Long reqId) {
        return ExportDaoUtils.requirementVersionQuery(
                em, "requirement.findVersionsModelsIndexInLibrary", reqId, 0);
    }

    private List<Long> gatherRequirementIdsFromLinkModels(List<RequirementLinkModel> models) {
        Set<Long> ids = new HashSet<>(models.size());
        for (RequirementLinkModel model : models) {
            ids.add(model.getReqId());
            ids.add(model.getRelReqId());
        }
        return new ArrayList<>(ids);
    }

    private List<Long> gatherRequirementIdsFromLinkedLowLevelReqModels(
            List<LinkedLowLevelRequirementModel> models) {
        Set<Long> ids = new HashSet<>(models.size());
        for (LinkedLowLevelRequirementModel model : models) {
            ids.add(model.getHighLevelReqId());
            ids.add(model.getLinkedLowLevelReqId());
        }
        return new ArrayList<>(ids);
    }

    private Map<Long, String> gatherRequirementPaths(List<Long> requirementIds) {

        int nbReqs = requirementIds.size();

        List<String> pathes = pathService.buildRequirementsPaths(requirementIds);
        Map<Long, String> pathById = new HashMap<>(nbReqs);
        for (int i = 0; i < nbReqs; i++) {
            pathById.put(requirementIds.get(i), pathes.get(i));
        }

        return pathById;
    }

    private void assignPaths(List<RequirementLinkModel> models, Map<Long, String> pathById) {
        for (RequirementLinkModel model : models) {
            String reqPath = pathById.get(model.getReqId());
            String relPath = pathById.get(model.getRelReqId());
            model.setReqPath(reqPath);
            model.setRelReqPath(relPath);
        }
    }

    private void assignLinkedLowReqPaths(
            List<LinkedLowLevelRequirementModel> models, Map<Long, String> pathById) {
        for (LinkedLowLevelRequirementModel model : models) {
            String highLevelReqPath = pathById.get(model.getHighLevelReqId());
            String linkedLowLevelReqPath = pathById.get(model.getLinkedLowLevelReqId());
            model.setHighLevelReqPath(highLevelReqPath);
            model.setLinkedLowLevelReqPath(linkedLowLevelReqPath);
        }
    }

    @SuppressWarnings("unchecked")
    private List<RequirementModelFromJooq> findRequirementModelFromJooq(List<Long> versionIds) {

        RequirementVersion innerVersions =
                REQUIREMENT_VERSION.as(RequestAliasesConstants.INNER_VERSIONS);
        String versionCountAlias = RequestAliasesConstants.VERSIONS_COUNT;

        return dsl.select(
                        REQUIREMENT_VERSION.RES_ID.as(RequestAliasesConstants.ID),
                        REQUIREMENT_VERSION.REFERENCE,
                        REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS),
                        REQUIREMENT_VERSION.CRITICALITY,
                        INFO_LIST_ITEM.LABEL.as(ColumnIds.REQUIREMENT_CATEGORY.getColumnId()),
                        REQUIREMENT_VERSION.VERSION_NUMBER.as(
                                RequestAliasesConstants.REQUIREMENT_VERSION_NUMBER),
                        REQUIREMENT_VERSION.REQUIREMENT_ID,
                        RESOURCE.NAME,
                        RESOURCE.CREATED_BY,
                        RESOURCE.LAST_MODIFIED_BY,
                        RESOURCE.LAST_MODIFIED_ON,
                        RESOURCE.DESCRIPTION,
                        REQUIREMENT_LIBRARY_NODE.PROJECT_ID,
                        RESOURCE.CREATED_ON,
                        PROJECT.NAME.as(ColumnIds.PROJECT_NAME.getColumnId()),
                        countDistinct(MILESTONE_REQ_VERSION.MILESTONE_ID).as(MILESTONES_ALIAS),
                        selectCount()
                                .from(innerVersions)
                                .where(innerVersions.REQUIREMENT_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                                .asField(versionCountAlias),
                        when(HIGH_LEVEL_REQUIREMENT.RLN_ID.isNotNull(), true)
                                .otherwise(false)
                                .as(IS_HIGH_LEVEL),
                        selectCount()
                                .from(ATTACHMENT)
                                .where(ATTACHMENT.ATTACHMENT_LIST_ID.eq(RESOURCE.ATTACHMENT_LIST_ID))
                                .asField(ColumnIds.ATTACHMENTS.getColumnId()),
                        when(
                                        DSL.exists(
                                                DSL.selectOne()
                                                        .from(RLN_RELATIONSHIP)
                                                        .innerJoin(REQUIREMENT)
                                                        .on(RLN_RELATIONSHIP.ANCESTOR_ID.eq(REQUIREMENT.RLN_ID))
                                                        .where(
                                                                RLN_RELATIONSHIP.DESCENDANT_ID.eq(
                                                                        REQUIREMENT_VERSION.REQUIREMENT_ID))),
                                        true)
                                .otherwise(false)
                                .as(ColumnIds.REQUIREMENT_VERSION_HAS_PARENT.getColumnId()),
                        selectCount()
                                .from(RLN_RELATIONSHIP_CLOSURE)
                                .where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                                .and(RLN_RELATIONSHIP_CLOSURE.DEPTH.ne((short) 0))
                                .asField(RequestAliasesConstants.CHILD_OF_REQUIREMENT),
                        selectCount()
                                .from(REQUIREMENT)
                                .where(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
                                .asField(RequestAliasesConstants.LINKED_STANDARD_REQUIREMENT),
                        when(
                                        DSL.exists(
                                                DSL.selectOne()
                                                        .from(REQUIREMENT_VERSION_LINK)
                                                        .where(
                                                                REQUIREMENT_VERSION_LINK.RELATED_REQUIREMENT_VERSION_ID.eq(
                                                                        REQUIREMENT_VERSION.RES_ID))),
                                        true)
                                .otherwise(false)
                                .as(RequestAliasesConstants.HAS_LINK_TYPE),
                        when(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.isNotNull(), true)
                                .otherwise(false)
                                .as(REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT),
                        countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID)
                                .as(ColumnIds.COVERAGE_OF_ASSOCIATED_TEST_CASE.getColumnId()),
                        DSL.select(groupConcat(MILESTONE.LABEL).orderBy(MILESTONE.LABEL).separator(", "))
                                .from(MILESTONE)
                                .join(MILESTONE_REQ_VERSION)
                                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
                                .where(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                                .asField(RequestAliasesConstants.MILESTONE_LABELS),
                        DSL.select(groupConcat(MILESTONE.STATUS).orderBy(MILESTONE.LABEL).separator("|"))
                                .from(MILESTONE)
                                .join(MILESTONE_REQ_VERSION)
                                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
                                .where(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                                .asField(RequestAliasesConstants.MILESTONE_STATUS),
                        DSL.select(groupConcat(MILESTONE.END_DATE).orderBy(MILESTONE.LABEL).separator("|"))
                                .from(MILESTONE)
                                .join(MILESTONE_REQ_VERSION)
                                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
                                .where(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                                .asField(RequestAliasesConstants.MILESTONE_END_DATE),
                        countDistinct(MILESTONE_REQ_VERSION.MILESTONE_ID).as(MILESTONES_ALIAS))
                .from(REQUIREMENT_VERSION)
                .innerJoin(RESOURCE)
                .on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
                .leftJoin(REQUIREMENT_VERSION_COVERAGE)
                .on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .leftJoin(HIGH_LEVEL_REQUIREMENT)
                .on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .leftJoin(MILESTONE_REQ_VERSION)
                .on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                .leftJoin(MILESTONE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
                .leftJoin(INFO_LIST_ITEM)
                .on(REQUIREMENT_VERSION.CATEGORY.eq((INFO_LIST_ITEM.ITEM_ID)))
                .where(REQUIREMENT_VERSION.RES_ID.in(versionIds))
                .groupBy(
                        REQUIREMENT_VERSION.RES_ID,
                        REQUIREMENT_VERSION.REFERENCE,
                        REQUIREMENT_VERSION.REQUIREMENT_STATUS,
                        REQUIREMENT_VERSION.CRITICALITY,
                        INFO_LIST_ITEM.LABEL,
                        REQUIREMENT_VERSION.VERSION_NUMBER,
                        REQUIREMENT_VERSION.REQUIREMENT_ID,
                        RESOURCE.NAME,
                        RESOURCE.CREATED_BY,
                        RESOURCE.LAST_MODIFIED_BY,
                        RESOURCE.LAST_MODIFIED_ON,
                        RESOURCE.DESCRIPTION,
                        REQUIREMENT_LIBRARY_NODE.PROJECT_ID,
                        RESOURCE.CREATED_ON,
                        PROJECT.NAME,
                        HIGH_LEVEL_REQUIREMENT.RLN_ID,
                        REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID,
                        RESOURCE.ATTACHMENT_LIST_ID)
                .fetchInto(RequirementModelFromJooq.class);
    }
}
