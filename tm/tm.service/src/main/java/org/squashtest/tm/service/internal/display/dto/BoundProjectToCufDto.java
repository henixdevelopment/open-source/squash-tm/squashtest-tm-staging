/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.customfield.BindableEntity;

public class BoundProjectToCufDto {
    private Long projectId;
    private BindableEntity bindableEntity;
    private String projectName;
    private Long customFieldBindingId;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public BindableEntity getBindableEntity() {
        return bindableEntity;
    }

    public void setBindableEntity(BindableEntity bindableEntity) {
        this.bindableEntity = bindableEntity;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getCustomFieldBindingId() {
        return customFieldBindingId;
    }

    public void setCustomFieldBindingId(Long customFieldBindingId) {
        this.customFieldBindingId = customFieldBindingId;
    }
}
