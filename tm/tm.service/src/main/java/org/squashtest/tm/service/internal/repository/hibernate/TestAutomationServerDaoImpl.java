/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.AutomationEnvironmentTag.AUTOMATION_ENVIRONMENT_TAG;
import static org.squashtest.tm.jooq.domain.tables.Project.PROJECT;
import static org.squashtest.tm.jooq.domain.tables.TestAutomationServer.TEST_AUTOMATION_SERVER;

import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.DSLContext;
import org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTagHolder;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.internal.repository.CustomTestAutomationServerDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;

public class TestAutomationServerDaoImpl implements CustomTestAutomationServerDao {

    @PersistenceContext private EntityManager em;

    @Inject private DSLContext dsl;

    /**
     * @see
     *     org.squashtest.tm.service.internal.repository.TestAutomationServerDao#hasBoundProjects(long)
     */
    @Override
    public boolean hasBoundProjects(long serverId) {
        Query q = em.createNamedQuery("testAutomationServer.hasBoundProjects");
        q.setParameter(ParameterNames.SERVER_ID, serverId);
        Long count = (Long) q.getSingleResult();
        return count > 0;
    }

    /**
     * @see org.squashtest.tm.service.internal.repository.TestAutomationServerDao#deleteServer(long)
     */
    @Override
    public void deleteServer(long serverId) {
        dereferenceProjects(serverId);
        em.flush();
        deleteServerById(serverId);
        em.flush();
    }

    @Override
    public List<String> getEnvironmentTags(long testAutomationServerId) {
        return dsl.select(AUTOMATION_ENVIRONMENT_TAG.VALUE)
                .from(AUTOMATION_ENVIRONMENT_TAG)
                .where(
                        AUTOMATION_ENVIRONMENT_TAG
                                .ENTITY_TYPE
                                .eq(AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER.name())
                                .and(AUTOMATION_ENVIRONMENT_TAG.ENTITY_ID.eq(testAutomationServerId)))
                .fetchInto(String.class);
    }

    public String getAdditionalConfigurationByProjectId(long projectId) {
        return dsl.select(TEST_AUTOMATION_SERVER.ADDITIONAL_CONFIGURATION)
                .from(TEST_AUTOMATION_SERVER)
                .join(PROJECT)
                .on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
                .and(PROJECT.PROJECT_ID.eq(projectId))
                .fetchOneInto(String.class);
    }

    @Override
    public Map<Long, Long> findIdsByIterationId(Long iterationId) {
        return dsl.selectDistinct(PROJECT.PROJECT_ID, TEST_AUTOMATION_SERVER.SERVER_ID)
                .from(TEST_AUTOMATION_SERVER)
                .join(PROJECT)
                .on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
                .join(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .join(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .join(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .join(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .where(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(iterationId))
                .and(TEST_AUTOMATION_SERVER.KIND.eq("squashOrchestrator"))
                .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
                .and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
                .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull())
                .fetchMap(PROJECT.PROJECT_ID, TEST_AUTOMATION_SERVER.SERVER_ID);
    }

    @Override
    public Map<Long, Long> findIdsByTestSuiteId(Long testSuiteId) {
        return dsl.selectDistinct(PROJECT.PROJECT_ID, TEST_AUTOMATION_SERVER.SERVER_ID)
                .from(TEST_AUTOMATION_SERVER)
                .join(PROJECT)
                .on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
                .join(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .join(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .join(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .join(TEST_SUITE_TEST_PLAN_ITEM)
                .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(testSuiteId))
                .and(TEST_AUTOMATION_SERVER.KIND.eq(TestAutomationServerKind.squashOrchestrator.name()))
                .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
                .and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
                .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull())
                .fetchMap(PROJECT.PROJECT_ID, TEST_AUTOMATION_SERVER.SERVER_ID);
    }

    @Override
    public Long findTestAutomationServerIdByGenericProjectId(Long genericProjectId) {
        return dsl.select(PROJECT.TA_SERVER_ID)
                .from(PROJECT)
                .where(PROJECT.PROJECT_ID.eq(genericProjectId))
                .fetchOneInto(Long.class);
    }

    // ***************** private stuffs ***************

    private void dereferenceProjects(long serverId) {
        Query q = em.createNamedQuery("testAutomationServer.dereferenceProjects");
        q.setParameter(ParameterNames.SERVER_ID, serverId);
        q.executeUpdate();
    }

    private void deleteServerById(long serverId) {
        Query q = em.createNamedQuery("testAutomationServer.deleteServer");
        q.setParameter(ParameterNames.SERVER_ID, serverId);
        q.executeUpdate();
    }
}
