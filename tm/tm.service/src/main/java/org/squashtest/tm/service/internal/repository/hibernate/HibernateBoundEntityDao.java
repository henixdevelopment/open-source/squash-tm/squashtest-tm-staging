/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.service.internal.repository.BoundEntityDao;

@Repository
public class HibernateBoundEntityDao implements BoundEntityDao {

    @PersistenceContext private EntityManager em;

    @Override
    public BoundEntity findBoundEntity(CustomFieldValue customFieldValue) {
        return findBoundEntity(
                customFieldValue.getBoundEntityId(), customFieldValue.getBoundEntityType());
    }

    @Override
    public BoundEntity findBoundEntity(Long boundEntityId, BindableEntity entityType) {
        if (entityType == BindableEntity.CUSTOM_REPORT_FOLDER) {
            Query query = em.createNamedQuery("BoundEntityDao.findCurrentCustomReportFoldersId");
            query.setParameter("clnId", boundEntityId);
            boundEntityId = (Long) query.getSingleResult();
        }
        Class<?> entityClass = entityType.getReferencedClass();
        return (BoundEntity) em.getReference(entityClass, boundEntityId);
    }

    @Override
    public boolean hasCustomField(Long boundEntityId, BindableEntity entityType) {

        Query query = em.createNamedQuery("BoundEntityDao.hasCustomFields");
        query.setParameter("boundEntityId", boundEntityId);
        query.setParameter("boundEntityType", entityType);

        return (Long) query.getSingleResult() != 0;
    }
}
