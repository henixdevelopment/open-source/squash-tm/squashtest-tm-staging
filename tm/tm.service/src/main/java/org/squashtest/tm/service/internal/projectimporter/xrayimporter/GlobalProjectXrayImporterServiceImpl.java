/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.xrayimporter;

import com.fasterxml.jackson.core.JsonFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.CustomFieldTable;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.ItemTable;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayImportModel;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayInfoModel;
import org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception.ImportXrayException;
import org.squashtest.tm.service.projectimporter.xrayimporter.GlobalProjectXrayImporterService;
import org.squashtest.tm.service.projectimporter.xrayimporter.ProjectImporterXrayToTableService;
import org.squashtest.tm.service.projectimporter.xrayimporter.XrayTablesDao;
import org.squashtest.tm.service.projectimporter.xrayimporter.topivot.ExecutionWorkspaceToPivotImporterService;
import org.squashtest.tm.service.projectimporter.xrayimporter.topivot.TestCaseToPivotImporterService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

@Service("GlobalProjectXrayImporterServiceImpl")
@Transactional
public class GlobalProjectXrayImporterServiceImpl implements GlobalProjectXrayImporterService {
    private final XrayTablesDao xrayTablesDao;
    private final ProjectImporterXrayToTableService projectImporterXrayToTableService;
    private final TestCaseToPivotImporterService testCaseToPivotImporterService;
    private final ExecutionWorkspaceToPivotImporterService executionWorkspaceToPivotImporterService;
    private final PermissionEvaluationService permissionEvaluationService;

    public GlobalProjectXrayImporterServiceImpl(
            XrayTablesDao xrayTablesDao,
            ProjectImporterXrayToTableService projectImporterXrayToTableService,
            TestCaseToPivotImporterService testCaseToPivotImporterService,
            ExecutionWorkspaceToPivotImporterService executionWorkspaceToPivotImporterService,
            PermissionEvaluationService permissionEvaluationService) {
        this.xrayTablesDao = xrayTablesDao;
        this.projectImporterXrayToTableService = projectImporterXrayToTableService;
        this.testCaseToPivotImporterService = testCaseToPivotImporterService;
        this.executionWorkspaceToPivotImporterService = executionWorkspaceToPivotImporterService;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    @Override
    public XrayImportModel generatePivotFromXml(long projectId, MultipartFile[] multipartFiles)
            throws IOException {
        checkCanImportPermission(projectId);
        try {
            int idTransaction = TransactionAspectSupport.currentTransactionStatus().hashCode();
            generateTmpTables(idTransaction);
            for (MultipartFile multipartFile : multipartFiles) {
                parsingXmlToTmpTables(multipartFile.getInputStream());
            }
            return generatePivotWithTmpTables(idTransaction);
        } finally {
            xrayTablesDao.dropTablesIfExist();
        }
    }

    @Override
    public XrayInfoModel generateXrayInfoDtoFromXml(long projectId, MultipartFile[] multipartFiles)
            throws IOException {
        checkCanImportPermission(projectId);
        XrayInfoModel xrayInfoDto = new XrayInfoModel();
        for (MultipartFile multipartFile : multipartFiles) {
            try (InputStream inputStream = multipartFile.getInputStream()) {
                projectImporterXrayToTableService.getXrayInfoFromXMLParsing(
                        inputStream, xrayInfoDto, multipartFile.getOriginalFilename());
            }
        }
        return xrayInfoDto;
    }

    @Override
    public File verifyXrayFile(long projectId, String fileName) throws IllegalAccessException {
        checkCanImportPermission(projectId);
        String fileNameMatch =
                String.format(
                        "^(%s|%s)\\d+\\.[a-z]+", XrayField.PIVOT_FILENAME, XrayField.PIVOT_LOG_FILENAME);
        Matcher matcher = Pattern.compile(fileNameMatch).matcher(fileName);
        if (!matcher.find()) {
            throw new IllegalAccessException("File " + fileName + " is not a valid pivot file.");
        }
        File file = new File(FileUtils.getTempDirectory(), fileName);
        if (!file.exists()) {
            throw new IllegalArgumentException("File " + fileName + " does not exist.");
        }
        return file;
    }

    private void checkCanImportPermission(long projectId) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService,
                Collections.singletonList(projectId),
                Permissions.IMPORT.name(),
                Project.class.getName());
    }

    private void generateTmpTables(int idTransaction) {
        xrayTablesDao.createTables(new ItemTable(idTransaction), new CustomFieldTable(idTransaction));
    }

    private void parsingXmlToTmpTables(InputStream inputStream) throws IOException {
        try (inputStream) {
            projectImporterXrayToTableService.convertXrayToTable(inputStream);
        } catch (XMLStreamException xmlStreamException) {
            throw new RuntimeException(xmlStreamException);
        }
    }

    private XrayImportModel generatePivotWithTmpTables(int idTransaction) {
        File pivotZipFile;
        File pivotLogFile;
        try {
            pivotZipFile = File.createTempFile(XrayField.PIVOT_FILENAME + idTransaction, ".zip");
            pivotLogFile = File.createTempFile(XrayField.PIVOT_LOG_FILENAME + idTransaction, ".txt");
            pivotZipFile.deleteOnExit();
            pivotLogFile.deleteOnExit();
        } catch (IOException e) {
            throw new ImportXrayException(
                    "Error during the generation of the pivot format while creating the temporary files in the temp directory: ",
                    e);
        }
        JsonFactory jsonFactory = new JsonFactory();
        XrayImportModel xrayImportModel = new XrayImportModel();
        try (ArchiveOutputStream<ZipArchiveEntry> archive =
                new ArchiveStreamFactory()
                        .createArchiveOutputStream(
                                ArchiveStreamFactory.ZIP, new FileOutputStream(pivotZipFile))) {
            try (PrintWriter logWriter = new PrintWriter(pivotLogFile)) {
                logWriter.write("Pivot generation log");
                testCaseToPivotImporterService.writeTestCaseFolder(
                        jsonFactory, logWriter, archive, xrayImportModel);
                testCaseToPivotImporterService.writeTestCase(
                        jsonFactory, logWriter, archive, xrayImportModel);
                testCaseToPivotImporterService.writeCalledTestCase(
                        jsonFactory, logWriter, archive, xrayImportModel);
                executionWorkspaceToPivotImporterService.writeCampaign(
                        jsonFactory, logWriter, archive, xrayImportModel);
                executionWorkspaceToPivotImporterService.writeIteration(
                        jsonFactory, logWriter, archive, xrayImportModel);
            }
        } catch (ArchiveException | IOException e) {
            throw new ImportXrayException(
                    "Error during the generation of the pivot format while writing to the ZIP file: ", e);
        }
        xrayImportModel.setPivotFileName(pivotZipFile.getName());
        xrayImportModel.setPivotLogFileName(pivotLogFile.getName());
        return xrayImportModel;
    }
}
