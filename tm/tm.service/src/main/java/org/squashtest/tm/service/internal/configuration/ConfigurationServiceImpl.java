/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.configuration;

import static org.squashtest.tm.jooq.domain.Tables.CORE_CONFIG;

import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.impl.DSL;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.event.ConfigUpdateEvent;
import org.squashtest.tm.exception.MaxSizeException;
import org.squashtest.tm.service.configuration.ConfigurationService;

@Service("squashtest.core.configuration.ConfigurationService")
@Transactional
public class ConfigurationServiceImpl implements ConfigurationService {

    // TODO make these named queries
    private static final String INSERT_KEY_SQL =
            "insert into CORE_CONFIG (STR_KEY, VALUE) values (?1, ?2)";
    private static final String UPDATE_KEY_SQL =
            "update CORE_CONFIG set VALUE = ?1 where STR_KEY = ?2";

    @PersistenceContext private EntityManager em;

    private DSLContext dsl;

    private ApplicationEventPublisher eventPublisher;

    public ConfigurationServiceImpl(DSLContext dsl, ApplicationEventPublisher eventPublisher) {
        this.dsl = dsl;
        this.eventPublisher = eventPublisher;
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, String> findAllConfiguration() {
        return this.dsl
                .select()
                .from(CORE_CONFIG)
                .fetch()
                .intoMap(CORE_CONFIG.STR_KEY, CORE_CONFIG.VALUE);
    }

    @Override
    public void createNewConfiguration(String key, String value) {
        Query sqlQuery = em.createNativeQuery(INSERT_KEY_SQL);
        sqlQuery.setParameter(1, key);
        sqlQuery.setParameter(2, value);
        sqlQuery.executeUpdate();
        eventPublisher.publishEvent(new ConfigUpdateEvent(key));
    }

    @Override
    public void updateConfiguration(String key, String value) {
        Query sqlQuery = em.createNativeQuery(UPDATE_KEY_SQL);
        sqlQuery.setParameter(1, value);
        sqlQuery.setParameter(2, key);
        sqlQuery.executeUpdate();
        eventPublisher.publishEvent(new ConfigUpdateEvent(key));
    }

    @Override
    @Transactional(readOnly = true)
    public String findConfiguration(String key) {
        return dsl.select(CORE_CONFIG.VALUE)
                .from(CORE_CONFIG)
                .where(CORE_CONFIG.STR_KEY.eq(key))
                .fetchOne(CORE_CONFIG.VALUE);
    }

    /**
     * As per interface spec, when stored value is "true" (ignoring case), this returns <code>true
     * </code>, otherwise it returns <code>false</code>
     *
     * @see org.squashtest.tm.service.configuration.ConfigurationService#getBoolean(java.lang.String)
     */
    @Override
    @Transactional(readOnly = true)
    public boolean getBoolean(String key) {
        return Boolean.parseBoolean(findConfiguration(key));
    }

    /**
     * @see org.squashtest.tm.service.configuration.ConfigurationService#set(java.lang.String,
     *     boolean)
     */
    @Override
    public void set(String key, boolean value) {
        String strVal = Boolean.toString(value);
        set(key, strVal);
    }

    /**
     * @see org.squashtest.tm.service.configuration.ConfigurationService#set(java.lang.String,
     *     java.lang.String)
     */
    @Override
    public void set(String key, String value) {
        if (findConfiguration(key) == null) {
            createNewConfiguration(key, value);
        } else {
            updateConfiguration(key, value);
        }
    }

    @Override
    public void checkSizeValidity(String key, String value) {
        Field<Long> characterMaxLengthField = DSL.field("character_maximum_length", Long.class);
        Field<Long> characterMaxOctets = DSL.field("character_octet_length", Long.class);

        Record2<Long, Long> result =
                dsl.select(characterMaxLengthField, characterMaxOctets)
                        .from(DSL.table("information_schema.columns"))
                        .where(
                                DSL.field("table_name")
                                        .equalIgnoreCase(CORE_CONFIG.getName())
                                        .and(DSL.field("column_name").equalIgnoreCase(CORE_CONFIG.VALUE.getName())))
                        .fetchAny();

        if (result != null) {
            Long maxLength = result.component1();
            Long maxOctets = result.component2();

            if ((maxLength != null && value.length() > maxLength)
                    || (maxOctets != null && value.getBytes().length >= maxOctets)) {
                throw new MaxSizeException("The maximum size is exceeded for the key : " + key);
            }
        }
    }
}
