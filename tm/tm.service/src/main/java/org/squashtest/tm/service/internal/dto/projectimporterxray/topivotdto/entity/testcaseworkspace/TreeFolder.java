/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.jooq.tools.StringUtils;

public class TreeFolder {
    private final Set<TreeFolder> children = new LinkedHashSet<>();
    private final String data;
    private final String fullPath;
    private final boolean rootFolder;
    private Integer id;
    private Integer parentId;

    public TreeFolder() {
        this.data = "";
        this.fullPath = "";
        this.rootFolder = true;
    }

    public TreeFolder(
            String data, String fullPath, boolean rootFolder, Integer id, Integer parentId) {
        this.data = data;
        this.fullPath = fullPath;
        this.rootFolder = rootFolder;
        this.parentId = parentId;
        this.id = id;
    }

    // Create Tree with recursive
    public TreeFolder child(String data, AtomicInteger idGenerator) {
        for (TreeFolder child : children) {
            if (child.data.equals(data)) {
                return child;
            }
        }
        return child(
                new TreeFolder(
                        data,
                        String.format("%s/%s", this.fullPath, data),
                        StringUtils.isEmpty(this.fullPath),
                        idGenerator.incrementAndGet(),
                        this.id));
    }

    private TreeFolder child(TreeFolder child) {
        children.add(child);
        return child;
    }

    // Getter
    public String getFullPath() {
        return fullPath;
    }

    public boolean isRootFolder() {
        return rootFolder;
    }

    public Set<TreeFolder> getChildren() {
        return children;
    }

    public String getData() {
        return data;
    }

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    // Setter
    public void setId(Integer id) {
        this.id = id;
    }
}
