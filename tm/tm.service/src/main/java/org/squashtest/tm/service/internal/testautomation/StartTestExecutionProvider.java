/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.internal.testautomation.httpclient.BusClientFactory;
import org.squashtest.tm.service.internal.testautomation.httpclient.WorkflowClientFactory;
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext;
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;

/**
 * Simple injectable factory that makes TestAutomationSquashAutomConnector and StartTestExecution
 * less coupled and thus easier to test independently.
 */
@Component
public class StartTestExecutionProvider {
    @Value("${squashtm.testautomationserver.timeout:15}")
    private int requestTimeoutSeconds;

    public StartTestExecution create(
            BuildDef buildDef,
            Collection<SquashAutomExecutionConfiguration> configurations,
            String suiteId,
            TestPlanContext testPlanContext,
            String callbackUrl,
            String automatedServerToken,
            WorkflowClientFactory workflowClientFactory,
            BusClientFactory busClientFactory,
            EntityPathHeaderDao entityPathHeaderDao,
            boolean isUltimateLicenseAvailable) {
        return new StartTestExecution(
                buildDef,
                configurations,
                suiteId,
                testPlanContext,
                callbackUrl,
                automatedServerToken,
                workflowClientFactory,
                busClientFactory,
                entityPathHeaderDao,
                requestTimeoutSeconds,
                isUltimateLicenseAvailable);
    }
}
