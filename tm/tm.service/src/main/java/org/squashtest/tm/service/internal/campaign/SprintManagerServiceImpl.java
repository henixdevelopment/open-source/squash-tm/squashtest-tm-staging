/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.LINK_REQUIREMENT_TO_SPRINT;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_SPRINT_OR_ADMIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintReqVersion;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.campaign.SprintManagerService;
import org.squashtest.tm.service.internal.deletion.JdbcSprintReqVersionDeletionHandlerFactory;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionDto;
import org.squashtest.tm.service.internal.dto.SprintReqVersionsBindingExceptionSummary;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;
import org.squashtest.tm.service.internal.repository.SprintDao;
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao;
import org.squashtest.tm.service.internal.repository.SprintRequirementSyncExtenderDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Transactional
@Service("SprintManagerService")
public class SprintManagerServiceImpl implements SprintManagerService {

    @Inject private SprintReqVersionDao sprintReqVersionDao;

    @Inject private SprintDao sprintDao;

    @Inject private RequirementDao requirementDao;

    @Inject private RequirementVersionDao requirementVersionDao;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject
    @Qualifier("squashtest.tm.repository.RequirementLibraryNodeDao")
    private LibraryNodeDao<RequirementLibraryNode> requirementLibraryNodeDao;

    @Inject SprintRequirementSyncExtenderDao sprintRequirementSyncExtenderDao;

    @Inject SprintDisplayDao sprintDisplayDao;

    @Inject CampaignNodeDeletionHandler deletionHandler;

    @Inject private JdbcSprintReqVersionDeletionHandlerFactory sprintReqVersionDeletionHandler;

    /**
     * Find all the requirement versions linked to the rlnIds and return a list of
     * SprintReqVersionDtos.
     *
     * @param sprintId The id of the sprint that needs the binding
     * @param rlnIds The rlnIds to be bound
     * @param summary Possible exceptions summary that will be filled in this method
     * @return The List of SprintReqVersionDtos that have been saved
     */
    @Override
    @PreAuthorize(LINK_REQUIREMENT_TO_SPRINT + OR_HAS_ROLE_ADMIN)
    public List<SprintReqVersionDto> bindRequirementsByRlnIds(
            Long sprintId, List<Long> rlnIds, SprintReqVersionsBindingExceptionSummary summary) {
        List<RequirementVersion> reqVersions = findRequirementVersionsFromRlnIds(rlnIds);
        return bindRequirements(sprintId, reqVersions, summary);
    }

    /**
     * Fetch all the requirement versions corresponding to the ids and return a list of
     * SprintReqVersionDtos.
     *
     * @param sprintId The id of the sprint that needs the binding
     * @param reqVersionIds The requirement version ids to be bound
     * @param summary Possible exceptions summary that will be filled in this method
     * @return The List of SprintReqVersionDtos that have been saved
     */
    @Override
    @PreAuthorize(LINK_REQUIREMENT_TO_SPRINT + OR_HAS_ROLE_ADMIN)
    public List<SprintReqVersionDto> bindRequirementsByReqVersionIds(
            Long sprintId, List<Long> reqVersionIds, SprintReqVersionsBindingExceptionSummary summary) {
        List<RequirementVersion> reqVersions = findRequirementVersions(reqVersionIds);
        return bindRequirements(sprintId, reqVersions, summary);
    }

    /**
     * This method checks : - first, if the sprintId and the requirement versions are already bound in
     * the sprint_req_version table. If so, adds to the summary a list of
     * existingSprintReqVersionNames and removes these requirement versions to the reqVersionsToSave -
     * secondly, if any requirement version corresponds to a high-level requirement. If so, adds to
     * the summary a list of highLevelReqVersionNames and removes these requirement versions to the
     * reqVersionsToSave - third, if any requirement version is obsolete. If so,adds to the summary a
     * list of obsoleteReqVersionNames and removes these requirement versions to the
     * reqVersionsToSave. Finally, binds all the reqVersionsToSave to the sprintId.
     *
     * @param sprintId The id of the sprint that needs the binding
     * @param reqVersions The list of sprintReqVersions to be bound
     * @param summary Possible exceptions summary that will be filled in this method
     * @return List<SprintReqVersionDto> : a list of SprintReqVersionDtos representing the
     *     SprintReqVersions that have been saved
     */
    private List<SprintReqVersionDto> bindRequirements(
            Long sprintId,
            List<RequirementVersion> reqVersions,
            SprintReqVersionsBindingExceptionSummary summary) {
        List<RequirementVersion> reqVersionsToSave = new ArrayList<>(reqVersions);

        List<String> existingSprintReqVersionNames =
                retrieveExistingSprintReqVersions(sprintId, reqVersions, reqVersionsToSave);
        summary.setReqVersionsAlreadyLinkedToSprint(existingSprintReqVersionNames);

        List<String> highLevelReqVersionNames =
                retrieveHighLevelReqVersions(reqVersions, reqVersionsToSave);
        summary.setHighLevelReqVersionsInSelection(highLevelReqVersionNames);

        List<String> obsoleteReqVersionNames =
                retrieveObsoleteReqVersions(reqVersions, reqVersionsToSave);
        summary.setObsoleteReqVersionsInSelection(obsoleteReqVersionNames);

        Sprint sprint = sprintDao.findById(sprintId);
        sprint.checkLinkable();

        List<SprintReqVersion> sprintReqVersions =
                reqVersionsToSave.stream()
                        .map(reqVersion -> new SprintReqVersion(reqVersion, sprint))
                        .toList();
        if (!reqVersionsToSave.isEmpty()) {
            sprint.updateLastModificationWithCurrentUser();
        }
        List<SprintReqVersion> savedSprintReqVersions = sprintReqVersionDao.saveAll(sprintReqVersions);
        return convertToSprintReqVersionDto(savedSprintReqVersions);
    }

    /**
     * Deletes the required sprint versions based on their executions and user permissions. This
     * method first retrieves the sprint corresponding to the given sprint ID and verifies if it can
     * be linked. Then, it retrieves the required sprint versions (SprintReqVersion) associated with
     * the sprint and distinguishes between those that have executions and those that do not. Required
     * sprint versions without executions are deleted with the {@code LINK} permission, while those
     * with executions are deleted with the {@code EXTENDED_DELETE} permission.
     *
     * @param sprintId the ID of the sprint from which the required sprint versions will be deleted
     * @param sprintReqVersionIds a collection of IDs of the required sprint versions to be deleted
     */
    @Override
    public void deleteSprintReqVersions(
            final long sprintId, final Collection<Long> sprintReqVersionIds) {
        Sprint sprint = sprintDao.findById(sprintId);
        sprint.checkLinkable();

        Set<Long> sprintReqVersionsWithExecutions =
                new HashSet<>(sprintReqVersionDao.findSprintReqVersionWithExecutions(sprintReqVersionIds));

        List<Long> allowedSprintReqVersionIds =
                sprintReqVersionIds.stream()
                        .filter(
                                id ->
                                        sprintReqVersionsWithExecutions.contains(id)
                                                ? permissionEvaluationService.hasRoleOrPermissionOnObject(
                                                        Roles.ROLE_ADMIN,
                                                        Permissions.EXTENDED_DELETE.name(),
                                                        sprint.getId(),
                                                        Sprint.SIMPLE_CLASS_NAME)
                                                : permissionEvaluationService.hasRoleOrPermissionOnObject(
                                                        Roles.ROLE_ADMIN,
                                                        Permissions.LINK.name(),
                                                        sprint.getId(),
                                                        Sprint.SIMPLE_CLASS_NAME))
                        .toList();

        if (!allowedSprintReqVersionIds.isEmpty()) {
            deleteSprintReqVersions(allowedSprintReqVersionIds, sprint);
        }
    }

    private void deleteSprintReqVersions(List<Long> sprintReqVersionIds, Sprint sprint) {

        sprintReqVersionDeletionHandler.build(sprintReqVersionIds).deleteSprintRequirementVersions();
        sprint.updateLastModificationWithCurrentUser();
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize(READ_SPRINT_OR_ADMIN)
    public Sprint findById(final long sprintId) {
        return sprintDao.findById(sprintId);
    }

    /**
     * Retrieves a list of nodes whose ids are passed as parameters. Checks if these nodes are folders
     * or requirements. If folders, returns the requirement versions linked to the requirements
     * contained in the folders. Otherwise, returns the requirement versions linked to the
     * requirements.
     *
     * @param rlnIds
     * @return a list of requirement versions
     */
    private List<RequirementVersion> findRequirementVersionsFromRlnIds(List<Long> rlnIds) {

        List<Requirement> requirements =
                requirementDao.findRequirementsNodeRootAndInFolderByNodeIds(rlnIds);

        if (!requirements.isEmpty()) {
            return requirements.stream().map(Requirement::getCurrentVersion).toList();
        }

        return Collections.emptyList();
    }

    private List<RequirementVersion> findRequirementVersions(List<Long> ids) {
        return requirementVersionDao.findAllById(ids);
    }

    /**
     * Identifies requirement versions already bound to the sprintId and removes them from
     * reqVersionsToSave. Returns the names of already bound requirement versions.
     *
     * @param sprintId
     * @param reqVersions
     * @param reqVersionsToSave
     * @return a List<String>
     */
    private List<String> retrieveExistingSprintReqVersions(
            Long sprintId,
            List<RequirementVersion> reqVersions,
            List<RequirementVersion> reqVersionsToSave) {
        List<String> existingSprintReqVersionNames = new ArrayList<>();
        List<Long> reqVersionIds = reqVersions.stream().map(RequirementVersion::getId).toList();
        List<SprintReqVersion> existingSprintReqVersions =
                sprintReqVersionDao.findAllBySprintIdAndRequirementVersionIdInOrderByRequirementVersionId(
                        sprintId, reqVersionIds);
        existingSprintReqVersions.forEach(
                existingSprintReqVersion -> {
                    RequirementVersion reqVersion = existingSprintReqVersion.getRequirementVersion();
                    reqVersionsToSave.remove(reqVersion);
                    existingSprintReqVersionNames.add(reqVersion.getName());
                });
        return existingSprintReqVersionNames;
    }

    /**
     * Identifies requirement versions which are high level requirements and removes them from the
     * reqVersionsToSave. Returns the names of these high level requirements.
     *
     * @param reqVersions
     * @param reqVersionsToSave
     * @return a List<String>
     */
    private List<String> retrieveHighLevelReqVersions(
            List<RequirementVersion> reqVersions, List<RequirementVersion> reqVersionsToSave) {
        List<String> highLevelReqNames = new ArrayList<>();
        List<Long> reqVersionIds = reqVersions.stream().map(RequirementVersion::getId).toList();
        List<Long> highLevelReqVersionIds =
                requirementDao.fetchHighLevelRequirementVersionIds(reqVersionIds);
        reqVersions.forEach(
                reqVersion -> {
                    boolean isHighLevelRequirement = highLevelReqVersionIds.contains(reqVersion.getId());
                    if (isHighLevelRequirement) {
                        String reqVersionName = reqVersion.getName();
                        highLevelReqNames.add(reqVersionName);
                        reqVersionsToSave.remove(reqVersion);
                    }
                });
        return highLevelReqNames;
    }

    /**
     * Identifies requirement versions which are obsolete and removes them from the reqVersionsToSave.
     * Returns the names of these obsolete requirements.
     *
     * @param reqVersions
     * @param reqVersionsToSave
     * @return a List<String>
     */
    private List<String> retrieveObsoleteReqVersions(
            List<RequirementVersion> reqVersions, List<RequirementVersion> reqVersionsToSave) {
        return reqVersions.stream()
                .filter(reqVersion -> !reqVersion.isNotObsolete())
                .map(
                        obsoleteReqVersion -> {
                            reqVersionsToSave.remove(obsoleteReqVersion);
                            return obsoleteReqVersion.getName();
                        })
                .toList();
    }

    /**
     * Converts a list of SprintReqVersions into a list of SprintReqVersionDtos
     *
     * @param sprintReqVersions
     * @return
     */
    private List<SprintReqVersionDto> convertToSprintReqVersionDto(
            List<SprintReqVersion> sprintReqVersions) {
        return sprintReqVersions.stream().map(SprintReqVersionDto::new).toList();
    }

    @Override
    public SprintStatus getSprintStatusByExecutionId(long testPlanItemId) {
        return sprintDisplayDao.getSprintStatusByExecutionId(testPlanItemId);
    }

    @Override
    public SprintStatus getSprintStatusBySprintReqVersionId(long sprintReqVersionId) {
        return sprintDisplayDao.getSprintStatusBySprintReqVersionId(sprintReqVersionId);
    }

    @Override
    public void deleteSprintSynchronisation(long sprintId) {
        sprintReqVersionDao.setAllSprintReqVersionsToNativeBySprintId(sprintId);
        sprintDao.deleteRemoteSynchronisationFromSprintId(sprintId);
        sprintRequirementSyncExtenderDao.deleteAllSprintReqSyncExtendersBySprintId(sprintId);
    }
}
