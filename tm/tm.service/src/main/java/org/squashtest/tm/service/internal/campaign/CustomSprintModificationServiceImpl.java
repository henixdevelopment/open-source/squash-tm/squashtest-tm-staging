/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.WRITE_SPRINT_OR_ADMIN;

import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.campaign.CustomSprintModificationService;
import org.squashtest.tm.service.campaign.SprintModificationService;
import org.squashtest.tm.service.display.sprint.SprintDisplayService;
import org.squashtest.tm.service.execution.ExploratoryExecutionService;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionDto;
import org.squashtest.tm.service.internal.library.NodeManagementService;
import org.squashtest.tm.service.internal.repository.SprintDao;
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao;

@Transactional
@Service("CustomSprintModificationService")
public class CustomSprintModificationServiceImpl implements CustomSprintModificationService {

    @Inject private SprintReqVersionDao sprintReqVersionDao;

    @Inject private SprintDisplayService sprintDisplayService;

    @Inject private SprintModificationService sprintModificationService;

    @Inject private ExploratoryExecutionService exploratoryExecutionService;

    @Inject private SprintDao sprintDao;

    @Inject
    @Named("squashtest.tm.service.internal.SprintManagementService")
    private NodeManagementService<Sprint, CampaignLibraryNode, CampaignFolder>
            sprintManagementService;

    public CustomSprintModificationServiceImpl() {
        super();
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_OR_ADMIN)
    public void rename(@Id long sprintId, String newName) {
        sprintManagementService.renameNode(sprintId, newName);
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_OR_ADMIN)
    public void updateStatusBySprintId(Long sprintId, SprintStatus newStatus) {
        sprintModificationService.changeStatus(sprintId, newStatus);
        if (newStatus == SprintStatus.CLOSED) {
            denormalizeSprintReqVersions(sprintId);
            exploratoryExecutionService.pauseRunningExploratoryExecutions(sprintId);
        }
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_OR_ADMIN)
    public void denormalizeSprintReqVersions(Long sprintId) {
        List<SprintReqVersionDto> sprintReqVersionDtos =
                sprintDisplayService.findSprintReqVersionDtosForDenormalization(sprintId);
        for (SprintReqVersionDto dto : sprintReqVersionDtos) {
            sprintReqVersionDao.updateForDenormalization(
                    dto.getId(),
                    dto.getReference(),
                    dto.getName(),
                    dto.getStatus(),
                    dto.getCriticality(),
                    dto.getCategoryLabel(),
                    dto.getDescription());
        }
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_OR_ADMIN)
    public void updateDescription(@Id long sprintId, String newReference) {
        Sprint sprint = sprintDao.findById(sprintId);
        sprint.checkLinkable();
        sprintModificationService.changeDescription(sprintId, newReference);
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_OR_ADMIN)
    public void updateStartDate(@Id long sprintId, Date newStartDate) {
        Sprint sprint = sprintDao.findById(sprintId);
        sprint.checkLinkable();
        sprintModificationService.changeStartDate(sprintId, newStartDate);
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_OR_ADMIN)
    public void updateEndDate(@Id long sprintId, Date newStartDate) {
        Sprint sprint = sprintDao.findById(sprintId);
        sprint.checkLinkable();
        sprintModificationService.changeEndDate(sprintId, newStartDate);
    }
}
