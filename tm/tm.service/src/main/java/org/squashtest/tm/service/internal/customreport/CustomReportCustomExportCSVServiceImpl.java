/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customreport;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.max;
import static org.jooq.impl.DSL.nullif;
import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.domain.customreport.CustomExportColumnLabel.CAMPAIGN_MILESTONE;
import static org.squashtest.tm.domain.customreport.CustomExportColumnLabel.ISSUE_EXECUTION_STEP_ISSUES_IDS;
import static org.squashtest.tm.domain.customreport.CustomExportColumnLabel.ISSUE_EXECUTION_STEP_ISSUES_NUMBER;
import static org.squashtest.tm.domain.customreport.CustomExportColumnLabel.TEST_CASE_DESCRIPTION;
import static org.squashtest.tm.domain.customreport.CustomExportColumnLabel.TEST_CASE_LABEL;
import static org.squashtest.tm.domain.customreport.CustomExportColumnLabel.getSelectableEntityTypeToIdTableFieldMap;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.VERIFYING_STEPS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SelectJoinStep;
import org.jooq.SelectSelectStep;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.customreport.CustomExportColumnLabel;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportCustomExportColumn;
import org.squashtest.tm.jooq.domain.tables.ItemTestPlanExecution;
import org.squashtest.tm.jooq.domain.tables.Iteration;
import org.squashtest.tm.service.customreport.CustomReportCustomExportCSVService;

@Service
public class CustomReportCustomExportCSVServiceImpl implements CustomReportCustomExportCSVService {

    private static final String ES_VS = "es_vs";
    private static final String CAMPAIGN_ITPI_DONE = "campaign_itpi_done";
    private static final String CAMPAIGN_ITPI_TOTAL = "campaign_itpi_total";

    @Inject private DSLContext dslContext;

    @Override
    public Map<Long, Result<Record>> getRowsData(
            CustomReportCustomExport customExport,
            Set<EntityType> cufEntityList,
            boolean restrainToLastExecution) {
        List<CustomReportCustomExportColumn> selectedColumns = customExport.getColumns();

        List<EntityType> fullEntityList = getEntityTypeList(restrainToLastExecution, selectedColumns);

        List<Field<?>> fieldsList = getFieldsList(cufEntityList, selectedColumns);
        // Fetch data from database
        List<EntityReference> entities = customExport.getScope();

        return fetchData(
                entities, fieldsList, fullEntityList, selectedColumns, restrainToLastExecution);
    }

    private List<EntityType> getEntityTypeList(
            boolean restrainToLastExecution, List<CustomReportCustomExportColumn> selectedColumns) {
        // Extract EntityTypes from the selected columns
        List<EntityType> fullEntityList =
                selectedColumns.stream()
                        .map(column -> column.getLabel().getEntityType())
                        .distinct()
                        .collect(Collectors.toList());

        // If the user only wants the last execution, we must make sure this column gets selected
        if (restrainToLastExecution && !fullEntityList.contains(EntityType.EXECUTION)) {
            fullEntityList.add(EntityType.EXECUTION);
        }
        return fullEntityList;
    }

    private List<Field<?>> getFieldsList(
            Set<EntityType> cufEntityList, List<CustomReportCustomExportColumn> selectedColumns) {
        // Extract jooqTableFields from the selected columns
        List<Field<?>> fieldsList =
                selectedColumns.stream()
                        .filter(column -> column.getLabel().getJooqTableField() != null)
                        .map(column -> column.getLabel().getJooqTableField())
                        .collect(Collectors.toList());

        // We need to include the Entity id field in the List if at least one CustomField is requested
        // for this Entity
        for (EntityType entityType : cufEntityList) {
            Field<Long> entityIdTableField =
                    CustomExportColumnLabel.getEntityTypeToIdTableFieldMap().get(entityType);
            if (!fieldsList.contains(entityIdTableField)) {
                fieldsList.add(entityIdTableField);
            }
        }

        // With multiple export we need to make sure this column is selected in order to group results
        // by campaign id
        if (!fieldsList.contains(CAMPAIGN.CLN_ID)) {
            fieldsList.add(CAMPAIGN.CLN_ID);
        }

        return fieldsList;
    }

    private Map<Long, Result<Record>> fetchData(
            List<EntityReference> entities,
            Collection<Field<?>> fieldList,
            List<EntityType> fullEntityList,
            List<CustomReportCustomExportColumn> selectedColumns,
            boolean printLastExec) {

        int queryDepth = getQueryDepth(fullEntityList);

        boolean isTestSuiteRequested = fullEntityList.contains(EntityType.TEST_SUITE);
        boolean isGroupByAndOrderByExecutionStepIdNeeded =
                checkGroupByAndOrderByExecutionStepIdNeeded(selectedColumns, queryDepth, fullEntityList);

        Map<EntityType, Boolean> selectedEntityTypeMap =
                getSelectedEntityTypeMap(entities.stream().map(EntityReference::getType).toList());

        SelectSelectStep<Record> selectQuery = dslContext.select(fieldList);

        SelectJoinStep<Record> fromQuery =
                buildFromClauseOfMainQuery(
                        fieldList, isTestSuiteRequested, selectedEntityTypeMap, queryDepth, selectQuery);

        Map<TableField<?, Long>, List<Long>> selectedTableFieldMap = getTableFieldListMap(entities);

        Condition condition = buildCondition(selectedTableFieldMap, printLastExec);

        Set<Field<?>> defaultGroupAndOrderByFieldList =
                buildDefaultGroupAndOrderByFieldList(
                        queryDepth, isTestSuiteRequested, isGroupByAndOrderByExecutionStepIdNeeded);
        fromQuery
                .where(condition)
                .groupBy(appendRequiredGroupByFields(defaultGroupAndOrderByFieldList, selectedColumns))
                .orderBy(defaultGroupAndOrderByFieldList);

        return fromQuery.fetchGroups(CAMPAIGN.CLN_ID);
    }

    private Map<TableField<?, Long>, List<Long>> getTableFieldListMap(
            List<EntityReference> entities) {
        Map<EntityType, TableField<?, Long>> entityMap = getSelectableEntityTypeToIdTableFieldMap();
        Map<TableField<?, Long>, List<Long>> selectedTableFieldMap = new HashMap<>();

        entities.forEach(
                entityReference -> appendTableField(entityMap, selectedTableFieldMap, entityReference));

        return selectedTableFieldMap;
    }

    private void appendTableField(
            Map<EntityType, TableField<?, Long>> entityMap,
            Map<TableField<?, Long>, List<Long>> selectedTableFieldMap,
            EntityReference entityReference) {
        TableField<?, Long> field = entityMap.getOrDefault(entityReference.getType(), null);
        if (Objects.isNull(field)) {
            throw new IllegalArgumentException(
                    "Entity of type " + entityReference.getType().name() + " is not supported");
        }
        selectedTableFieldMap
                .computeIfAbsent(field, k -> new ArrayList<>())
                .add(entityReference.getId());
    }

    private Condition buildCondition(
            Map<TableField<?, Long>, List<Long>> fields, boolean printLastExec) {
        Condition condition = null;

        for (Map.Entry<TableField<?, Long>, List<Long>> entry : fields.entrySet()) {
            if (condition == null) {
                condition = entry.getKey().in(entry.getValue());
            } else {
                condition = condition.or(entry.getKey().in(entry.getValue()));
            }
        }

        if (printLastExec) {
            ItemTestPlanExecution itpe = ITEM_TEST_PLAN_EXECUTION.as("itpe");
            condition =
                    (condition)
                            .and(
                                    (ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER)
                                            .eq(
                                                    dslContext
                                                            .select(max(itpe.EXECUTION_ORDER))
                                                            .from(itpe)
                                                            .where(
                                                                    (ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID)
                                                                            .eq(itpe.ITEM_TEST_PLAN_ID)))
                                            .or(((ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON).isNull())))
                            .or((condition).and((ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON).isNull()));
        }

        return condition;
    }

    private Map<EntityType, Boolean> getSelectedEntityTypeMap(List<EntityType> type) {
        Map<EntityType, Boolean> result = new HashMap<>();
        result.put(EntityType.ITERATION, type.contains(EntityType.ITERATION));
        result.put(EntityType.TEST_SUITE, type.contains(EntityType.TEST_SUITE));
        result.put(EntityType.CAMPAIGN_FOLDER, type.contains(EntityType.CAMPAIGN_FOLDER));
        return result;
    }

    /**
     * Build the From clause of the main Query.
     *
     * @param fieldList The List of all the requested Fields in the Query
     * @param isTestSuiteRequested Whether Test Suites are requested in the Query
     * @param queryDepth The depth of the Query
     * @param selectQuery The previously built Select clause of the Query
     * @return The From clause of the Query
     */
    private SelectJoinStep<Record> buildFromClauseOfMainQuery(
            Collection<Field<?>> fieldList,
            boolean isTestSuiteRequested,
            Map<EntityType, Boolean> selectedEntityTypeMap,
            int queryDepth,
            SelectSelectStep<Record> selectQuery) {
        SelectJoinStep<Record> fromQuery = selectQuery.from(CAMPAIGN);

        if (Boolean.TRUE.equals(selectedEntityTypeMap.get(EntityType.CAMPAIGN_FOLDER))) {
            joinCampaignFolder(fromQuery);
        }

        fromQuery.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN.CLN_ID));

        if (fieldList.contains(CAMPAIGN_MILESTONE.getJooqTableField())) {
            // only if CAMPAIGN_MILESTONE was selected
            joinCampaignMilestone(fromQuery);
        }

        if (isJoinOnIterationNeeded(queryDepth, selectedEntityTypeMap)) {
            joinIteration(fromQuery);
        }

        if (isJoinOnTestCaseNeeded(queryDepth, selectedEntityTypeMap)) {
            joinTestCase(fromQuery);
            joinInfoListItem(fromQuery);

            if (isJoinOnTestSuiteNeeded(isTestSuiteRequested, selectedEntityTypeMap)) {
                // only if TEST_SUITE attributes were selected
                joinTestSuite(fromQuery);
            }
        }
        if (queryDepth > 3) {
            joinExecution(fromQuery);
        }
        if (queryDepth > 4) {
            joinExecutionStep(fromQuery);
        }

        return fromQuery;
    }

    private void joinCampaignFolder(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(CLN_RELATIONSHIP_CLOSURE)
                .on(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(CAMPAIGN.CLN_ID))
                .leftJoin(CAMPAIGN_FOLDER)
                .on(CAMPAIGN_FOLDER.CLN_ID.eq(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID));
    }

    private void joinCampaignMilestone(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(MILESTONE_CAMPAIGN)
                .on(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .leftJoin(MILESTONE.as("camp_milestone"))
                .on(MILESTONE.as("camp_milestone").MILESTONE_ID.eq(MILESTONE_CAMPAIGN.MILESTONE_ID));
    }

    private void joinIteration(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
                .leftJoin(ITERATION)
                .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID));
    }

    private void joinExecutionStep(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(EXECUTION_EXECUTION_STEPS)
                .on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .leftJoin(EXECUTION_STEP)
                .on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                .leftJoin(VERIFYING_STEPS.as(ES_VS))
                .on(VERIFYING_STEPS.as(ES_VS).TEST_STEP_ID.eq(EXECUTION_STEP.TEST_STEP_ID));
    }

    private void joinExecution(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(
                        ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .leftJoin(EXECUTION)
                .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID));
    }

    private void joinTestSuite(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .leftJoin(TEST_SUITE)
                .on(TEST_SUITE.ID.eq(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID));
    }

    private void joinInfoListItem(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(INFO_LIST_ITEM.as("type_list"))
                .on(INFO_LIST_ITEM.as("type_list").ITEM_ID.eq(TEST_CASE.TC_TYPE))
                .leftJoin(INFO_LIST_ITEM.as("type_nature"))
                .on(INFO_LIST_ITEM.as("type_nature").ITEM_ID.eq(TEST_CASE.TC_NATURE));
    }

    private void joinTestCase(SelectJoinStep<Record> fromQuery) {
        fromQuery
                .leftJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                .leftJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .leftJoin(CORE_USER)
                .on(CORE_USER.PARTY_ID.eq(ITERATION_TEST_PLAN_ITEM.USER_ID))
                .leftJoin(DATASET)
                .on(DATASET.DATASET_ID.eq(ITERATION_TEST_PLAN_ITEM.DATASET_ID))
                .leftJoin(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID))
                .leftJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(PROJECT)
                .on(PROJECT.TCL_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID));
    }

    private boolean isJoinOnTestSuiteNeeded(
            boolean isTestSuiteRequested, Map<EntityType, Boolean> selectedEntityTypeMap) {
        return isTestSuiteRequested || selectedEntityTypeMap.get(EntityType.TEST_SUITE);
    }

    private boolean isJoinOnTestCaseNeeded(
            int queryDepth, Map<EntityType, Boolean> selectedEntityTypeMap) {
        return queryDepth > 2 || selectedEntityTypeMap.get(EntityType.TEST_SUITE);
    }

    private boolean isJoinOnIterationNeeded(
            int queryDepth, Map<EntityType, Boolean> selectedEntityTypeMap) {
        return queryDepth > 1
                || selectedEntityTypeMap.get(EntityType.ITERATION)
                || selectedEntityTypeMap.get(EntityType.TEST_SUITE);
    }

    /**
     * Return the depth of the Query.
     *
     * @param entityTypeList The list of the EntityTypes involved in the Query
     * @return The depth of the Query
     */
    private int getQueryDepth(List<EntityType> entityTypeList) {
        if (entityTypeList.contains(EntityType.ISSUE)) return 6;
        if (entityTypeList.contains(EntityType.EXECUTION_STEP)
                || entityTypeList.contains(EntityType.TEST_STEP)) return 5;
        if (entityTypeList.contains(EntityType.EXECUTION)) return 4;
        if (entityTypeList.contains(EntityType.TEST_CASE)
                || entityTypeList.contains(EntityType.TEST_SUITE)) return 3;
        if (entityTypeList.contains(EntityType.ITERATION)) return 2;
        // default
        return 1;
    }

    private boolean checkGroupByAndOrderByExecutionStepIdNeeded(
            List<CustomReportCustomExportColumn> selectedColumns,
            int queryDepth,
            List<EntityType> entityTypeList) {
        if (queryDepth > 4) {
            boolean columnsContainsIssueExecutionStep =
                    selectedColumns.stream()
                            .map(CustomReportCustomExportColumn::getLabel)
                            .anyMatch(
                                    label ->
                                            label.equals(ISSUE_EXECUTION_STEP_ISSUES_NUMBER)
                                                    || label.equals(ISSUE_EXECUTION_STEP_ISSUES_IDS));

            boolean entityListContainExecutionStepOrTestStep =
                    entityTypeList.contains(EntityType.EXECUTION_STEP)
                            || entityTypeList.contains(EntityType.TEST_STEP);

            return columnsContainsIssueExecutionStep || entityListContainExecutionStepOrTestStep;
        }
        return false;
    }

    /**
     * Build the default List of Fields that compose the Order By and the Group by clauses of the
     * Query.
     *
     * @param queryDepth The depth of the Query
     * @param isTestSuiteRequested Whether Test Suites are requested in the Query
     * @param isGroupByAndOrderByExecutionStepIdNeeded Whether we need execution step id in field list
     * @return The List of Fields composing the Order By and the Group by clauses of the Query
     */
    private Set<Field<?>> buildDefaultGroupAndOrderByFieldList(
            int queryDepth,
            boolean isTestSuiteRequested,
            boolean isGroupByAndOrderByExecutionStepIdNeeded) {
        Set<Field<?>> orderByFieldList = new HashSet<>();

        orderByFieldList.add(CAMPAIGN_LIBRARY_NODE.PROJECT_ID);
        orderByFieldList.add(CAMPAIGN.CLN_ID);

        if (queryDepth > 1) {
            orderByFieldList.add(ITERATION.ITERATION_ID);
        }
        if (queryDepth > 2) {
            if (isTestSuiteRequested) {
                orderByFieldList.add(TEST_SUITE.ID);
            }
            orderByFieldList.add(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);
        }
        if (queryDepth > 3) {
            orderByFieldList.add(EXECUTION.EXECUTION_ID);
        }
        if (isGroupByAndOrderByExecutionStepIdNeeded) {
            orderByFieldList.add(EXECUTION_STEP.EXECUTION_STEP_ID);
        }
        return orderByFieldList;
    }

    /**
     * Append the necessary fields to the Group by clause of the Query.
     *
     * @param selectedColumns All the selected columns in the CustomExport
     * @param defaultGroupByFieldList The default fields needed in the group by built previously
     * @return The Set of Fields composing the Group By clause of the Query
     */
    private Set<Field<?>> appendRequiredGroupByFields(
            Set<Field<?>> defaultGroupByFieldList, List<CustomReportCustomExportColumn> selectedColumns) {
        // Add required primary keys to the group by clause according to the selected columns
        for (CustomReportCustomExportColumn column : selectedColumns) {
            if (column.getLabel().getJooqTablePkField() != null) {
                defaultGroupByFieldList.add(column.getLabel().getJooqTablePkField());
            }
            // Some corner cases:
            // - If TEST_CASE_LABEL or TEST_CASE_DESCRIPTION are requested but no TEST_CASE columns are,
            // and since they are linked to TEST_CASE_LIBRARY_NODE table, we must add TEST_CASE_ID in the
            // Group By
            if (TEST_CASE_LABEL.equals(column.getLabel())
                    || TEST_CASE_DESCRIPTION.equals(column.getLabel())) {
                defaultGroupByFieldList.add(TEST_CASE.TCLN_ID);
            }
        }
        return defaultGroupByFieldList;
    }

    @Override
    public Map<Long, Object> computeCampaignProgressRate(Set<Long> campaignIds) {
        Map<Long, Object> result = new HashMap<>();
        campaignIds.forEach(
                campaignRowId -> result.put(campaignRowId, getCampaignProgressRateData(campaignRowId)));
        return result;
    }

    /**
     * Compute the Campaign progress rate. Formula is (nbrOfItpiDone / totalNbrOfItpi) in the
     * Campaign.
     *
     * @param campaignId The Campaign id
     * @return The value of the Campaign progress rate
     */
    private Object getCampaignProgressRateData(Long campaignId) {
        return dslContext
                .select(getCampaignProgressRateField(campaignId))
                .fetchOne(getCampaignProgressRateField(campaignId));
    }

    /**
     * Build the Jooq Field that computes the given Campaign progress rate.
     *
     * @param campaignId The Campaign id
     * @return The Jooq Field of the Campaign progress rate
     */
    private Field<?> getCampaignProgressRateField(Long campaignId) {
        return concat(
                DSL.round(
                                getItpiDoneCountField(campaignId)
                                        .div(nullif(getItpiTotalCountField(campaignId), 0.0))
                                        .mul(100L),
                                2)
                        .cast(SQLDataType.VARCHAR(5)),
                val(" "),
                val("%"));
    }

    /**
     * Build the Jooq Field that computes the Number of Itpis Done in the Campaign.
     *
     * @param campaignId The Campaign id
     * @return The Jooq Field computing the Number of Itpis Done in the Campaign
     */
    private Field<Double> getItpiDoneCountField(Long campaignId) {
        return DSL.select(count(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID).cast(SQLDataType.DOUBLE))
                .from(CAMPAIGN.as(CAMPAIGN_ITPI_DONE))
                .leftJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.as(CAMPAIGN_ITPI_DONE).CLN_ID))
                .leftJoin(Iteration.ITERATION)
                .on(Iteration.ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .leftJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(Iteration.ITERATION.ITERATION_ID))
                .leftJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .where(CAMPAIGN.as(CAMPAIGN_ITPI_DONE).CLN_ID.eq(campaignId))
                .and(
                        ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.in(
                                "SETTLED", "UNTESTABLE", "BLOCKED", "FAILURE", "SUCCESS"))
                .asField()
                .cast(Double.class);
    }

    /**
     * Build the Jooq Field that computes the Total Number of Itpis in the Campaign.
     *
     * @param campaignId The Campaign id list
     * @return The Jooq Field computing the Total Number of Itpis in the Campaign
     */
    private Field<Double> getItpiTotalCountField(Long campaignId) {
        return DSL.select(count(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID).cast(SQLDataType.DOUBLE))
                .from(CAMPAIGN.as(CAMPAIGN_ITPI_TOTAL))
                .leftJoin(CAMPAIGN_ITERATION)
                .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.as(CAMPAIGN_ITPI_TOTAL).CLN_ID))
                .leftJoin(Iteration.ITERATION)
                .on(Iteration.ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                .leftJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(Iteration.ITERATION.ITERATION_ID))
                .leftJoin(ITERATION_TEST_PLAN_ITEM)
                .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .where(CAMPAIGN.as(CAMPAIGN_ITPI_TOTAL).CLN_ID.eq(campaignId))
                .asField()
                .cast(Double.class);
    }
}
