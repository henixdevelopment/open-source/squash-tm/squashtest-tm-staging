/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.profile;

import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.List;
import java.util.Set;
import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.feature.ExperimentalFeature;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.profile.CannotDeleteProfileException;
import org.squashtest.tm.service.annotation.IsUltimateLicenseAvailable;
import org.squashtest.tm.service.audit.AuditModificationService;
import org.squashtest.tm.service.feature.experimental.ExperimentalFeatureManager;
import org.squashtest.tm.service.internal.display.dto.NewProfileDto;
import org.squashtest.tm.service.internal.display.dto.ProfileActivePermissionsRecord;
import org.squashtest.tm.service.internal.repository.ProfileDao;
import org.squashtest.tm.service.profile.ProfileManagerService;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;

@Transactional
@Service
public class ProfileManagerServiceImpl implements ProfileManagerService {

    private static final String PROFILE_WITH_ID = "Profile with id";

    private final ProfileDao profileDao;
    private final DSLContext dslContext;
    private final ObjectAclService aclService;
    private final AuditModificationService auditModificationService;
    private final ExperimentalFeatureManager experimentalFeatureManager;

    public ProfileManagerServiceImpl(
            ProfileDao profileDao,
            DSLContext dslContext,
            ObjectAclService aclService,
            AuditModificationService auditModificationService,
            ExperimentalFeatureManager experimentalFeatureManager) {
        this.profileDao = profileDao;
        this.dslContext = dslContext;
        this.aclService = aclService;
        this.auditModificationService = auditModificationService;
        this.experimentalFeatureManager = experimentalFeatureManager;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public long persist(NewProfileDto profileDto) {
        final String profileName = profileDto.getQualifiedName();
        if (nameInUse(profileName)) {
            throw new NameAlreadyInUseException(AclGroup.class.getSimpleName(), profileName);
        }

        AclGroup newProfile = profileDto.toProfile();
        profileDao.save(newProfile);

        insertPermissionsFromReferenceProfile(newProfile.getId(), profileDto.getReferenceProfileId());

        return newProfile.getId();
    }

    private void insertPermissionsFromReferenceProfile(long profileId, long referenceProfileId) {
        dslContext
                .insertInto(
                        ACL_GROUP_PERMISSION,
                        ACL_GROUP_PERMISSION.ACL_GROUP_ID,
                        ACL_GROUP_PERMISSION.PERMISSION_MASK,
                        ACL_GROUP_PERMISSION.CLASS_ID)
                .select(
                        dslContext
                                .select(
                                        DSL.val(profileId),
                                        ACL_GROUP_PERMISSION.PERMISSION_MASK,
                                        ACL_GROUP_PERMISSION.CLASS_ID)
                                .from(ACL_GROUP_PERMISSION)
                                .where(ACL_GROUP_PERMISSION.ACL_GROUP_ID.eq(referenceProfileId)))
                .execute();
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteProfile(long profileId) {
        AclGroup profileToDelete =
                profileDao
                        .findById(profileId)
                        .orElseThrow(
                                () ->
                                        new EntityNotFoundException(
                                                String.format("%S %d %S", PROFILE_WITH_ID, profileId, "doesn't exist.")));

        profileToDelete.checkAndForbidOperationForDefaultSystemProfile();

        if (profileDao.isProfileUsed(profileId)) {
            throw new CannotDeleteProfileException();
        }

        dslContext
                .deleteFrom(ACL_GROUP_PERMISSION)
                .where(ACL_GROUP_PERMISSION.ACL_GROUP_ID.eq(profileId))
                .execute();

        profileDao.delete(profileToDelete);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void changeDescription(long profileId, String newDescription) {
        AclGroup profile = findById(profileId);

        profile.setDescription(newDescription);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void changeName(long profileId, @NotNull String newName) {
        AclGroup profile = findById(profileId);

        if (newName.equals(profile.getQualifiedName())) {
            return;
        }
        if (nameInUse(newName)) {
            throw new NameAlreadyInUseException(AclGroup.class.getSimpleName(), newName);
        }
        profile.setQualifiedName(newName);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deactivateProfile(long profileId) {
        AclGroup profile = findById(profileId);

        profile.setActive(false);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void activateProfile(long profileId) {
        AclGroup profile = findById(profileId);

        profile.setActive(true);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void changePermissions(long profileId, List<ProfileActivePermissionsRecord> records) {
        AclGroup profile = findById(profileId);
        profile.checkAndForbidOperationForDefaultSystemProfile();

        if (!experimentalFeatureManager.isEnabled(ExperimentalFeature.CONFIGURE_PROFILES)) {
            final Set<String> allowedClasses =
                    Set.of(
                            Project.class.getName(),
                            RequirementLibrary.class.getName(),
                            TestCaseLibrary.class.getName(),
                            ActionWordLibrary.class.getName());
            records =
                    records.stream().filter(record -> allowedClasses.contains(record.className())).toList();
        }

        profileDao.updatePermissions(profileId, records);
        aclService.refreshAcls();
        auditModificationService.updateAuditable(profile);
    }

    private AclGroup findById(long profileId) {
        return profileDao
                .findById(profileId)
                .orElseThrow(
                        () ->
                                new EntityNotFoundException(
                                        String.format("%s %d %S", PROFILE_WITH_ID, profileId, "doesn't exist.")));
    }

    private boolean nameInUse(String profileName) {
        return dslContext.fetchExists(
                dslContext
                        .select(ACL_GROUP.ID)
                        .from(ACL_GROUP)
                        .where(ACL_GROUP.QUALIFIED_NAME.eq(profileName)));
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public void transferAuthorizations(long profileId, long targetProfileId) {
        dslContext
                .update(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .set(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID, targetProfileId)
                .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(profileId))
                .execute();
        aclService.refreshAcls();
    }
}
