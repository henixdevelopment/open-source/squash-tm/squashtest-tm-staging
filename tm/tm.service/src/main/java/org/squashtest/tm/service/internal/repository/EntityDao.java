/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.internal.copier.ChildEntityDtoResult;

public interface EntityDao<ENTITY_TYPE> extends GenericDao<ENTITY_TYPE> {

    ENTITY_TYPE findById(long id);

    List<ENTITY_TYPE> findAll();

    List<ENTITY_TYPE> findAllByIds(Collection<Long> id);

    default ENTITY_TYPE loadContainerForPaste(long id) {
        return findById(id);
    }

    default List<ENTITY_TYPE> loadContainersForPaste(Collection<Long> ids) {
        return findAllByIds(ids);
    }

    default List<ENTITY_TYPE> loadNodeForPaste(Collection<Long> ids) {
        return findAllByIds(ids);
    }

    default ChildEntityDtoResult loadChildForPaste(
            Collection<Long> ids, int maxResult, int offset, ClipboardPayload clipboardPayload) {
        return new ChildEntityDtoResult(Collections.emptyList(), true);
    }
}
