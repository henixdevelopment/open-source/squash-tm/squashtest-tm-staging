/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.sprint;

import java.util.Date;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.campaign.SprintReqVersion;
import org.squashtest.tm.domain.campaign.SprintReqVersionValidationStatus;
import org.squashtest.tm.domain.requirement.ManagementMode;
import org.squashtest.tm.domain.requirement.RemoteRequirementPerimeterStatus;
import org.squashtest.tm.domain.requirement.RequirementVersion;

public class SprintReqVersionDto {

    private Long id;
    private String projectName;
    private String createdBy;
    private Date createdOn;
    private String lastModifiedBy;
    private Date lastModifiedOn;
    private Long projectId;
    private Long campaignLibraryId;
    private String requirementVersionProjectName;
    private Long requirementVersionProjectId;
    private String reference;
    private String name;
    private Long categoryId;
    private String categoryLabel;
    private String criticality;
    private String status;
    private SprintReqVersionValidationStatus validationStatus;
    private Long versionId;
    private int nbTests;
    private Long requirementId;
    private Long sprintId;
    private String sprintName;
    private String description;
    private String path;
    private Long testPlanId;
    private RemoteRequirementPerimeterStatus remotePerimeterStatus;
    private String remoteReqUrl;
    private ManagementMode mode;
    private int nbIssues;
    private int nbExecutions;
    private String remoteReqState;
    private String remoteReqStatusAndState;

    public SprintReqVersionDto(SprintReqVersion sprintReqVersion) {
        RequirementVersion requirementVersion = sprintReqVersion.getRequirementVersion();
        this.id = sprintReqVersion.getId();
        this.projectName = sprintReqVersion.getProject().getName();
        this.projectId = sprintReqVersion.getProject().getId();
        this.requirementVersionProjectName = requirementVersion.getProject().getName();
        this.requirementVersionProjectId = requirementVersion.getProject().getId();
        this.reference = requirementVersion.getReference();
        this.name = requirementVersion.getName();
        this.categoryId = requirementVersion.getCategory().getId();
        this.categoryLabel = requirementVersion.getCategory().getLabel();
        this.criticality = requirementVersion.getCriticality().getCode();
        this.status = requirementVersion.getStatus().name();
        this.validationStatus = sprintReqVersion.getValidationStatus();
        this.versionId = requirementVersion.getId();
        this.requirementId = requirementVersion.getRequirement().getId();
        this.description = sprintReqVersion.getSprint().getDescription();
        this.sprintId = sprintReqVersion.getSprint().getId();
        this.sprintName = sprintReqVersion.getSprint().getName();
        this.testPlanId = sprintReqVersion.getTestPlan().getId();
        this.createdBy = sprintReqVersion.getCreatedBy();
        this.createdOn = sprintReqVersion.getCreatedOn();
        this.lastModifiedBy = sprintReqVersion.getLastModifiedBy();
        this.lastModifiedOn = sprintReqVersion.getLastModifiedOn();
        this.nbTests = sprintReqVersion.getTestPlan().getTestPlanItems().size();
    }

    public SprintReqVersionDto() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getCampaignLibraryId() {
        return campaignLibraryId;
    }

    public void setCampaignLibraryId(Long campaignLibraryId) {
        this.campaignLibraryId = campaignLibraryId;
    }

    public String getRequirementVersionProjectName() {
        return requirementVersionProjectName;
    }

    public void setRequirementVersionProjectName(String requirementVersionProjectName) {
        this.requirementVersionProjectName = requirementVersionProjectName;
    }

    public Long getRequirementVersionProjectId() {
        return requirementVersionProjectId;
    }

    public void setRequirementVersionProjectId(Long requirementVersionProjectId) {
        this.requirementVersionProjectId = requirementVersionProjectId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SprintReqVersionValidationStatus getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(SprintReqVersionValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public Long getSprintId() {
        return sprintId;
    }

    public void setSprintId(Long sprintId) {
        this.sprintId = sprintId;
    }

    public String getSprintName() {
        return sprintName;
    }

    public void setSprintName(String sprintName) {
        this.sprintName = sprintName;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public Long getTestPlanId() {
        return testPlanId;
    }

    public void setTestPlanId(Long testPlanId) {
        this.testPlanId = testPlanId;
    }

    public RemoteRequirementPerimeterStatus getRemotePerimeterStatus() {
        return remotePerimeterStatus;
    }

    public void setRemotePerimeterStatus(RemoteRequirementPerimeterStatus remotePerimeterStatus) {
        this.remotePerimeterStatus = remotePerimeterStatus;
    }

    public String getRemoteReqUrl() {
        return remoteReqUrl;
    }

    public void setRemoteReqUrl(String remoteReqUrl) {
        this.remoteReqUrl = remoteReqUrl;
    }

    public ManagementMode getMode() {
        return mode;
    }

    public void setMode(ManagementMode mode) {
        this.mode = mode;
    }

    public void setNbIssues(int nbIssues) {
        this.nbIssues = nbIssues;
    }

    public int getNbIssues() {
        return nbIssues;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public int getNbExecutions() {
        return nbExecutions;
    }

    public void setNbExecutions(int nbExecutions) {
        this.nbExecutions = nbExecutions;
    }

    public int getNbTests() {
        return nbTests;
    }

    public void setNbTests(int nbTests) {
        this.nbTests = nbTests;
    }

    public String getRemoteReqState() {
        return remoteReqState;
    }

    public void setRemoteReqState(String remoteReqState) {
        this.remoteReqState = remoteReqState;
    }

    public String getRemoteReqStatusAndState() {
        return remoteReqStatusAndState;
    }

    public void setRemoteReqStatusAndState(String remoteReqStatusAndState) {
        this.remoteReqStatusAndState = remoteReqStatusAndState;
    }
}
