/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.tables.AutomationEnvironmentTag.AUTOMATION_ENVIRONMENT_TAG;

import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.jooq.DSLContext;
import org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTagHolder;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.service.internal.repository.CustomGenericProjectDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;

/**
 * @author Gregory Fouquet
 */
public class GenericProjectDaoImpl implements CustomGenericProjectDao {
    @PersistenceContext private EntityManager em;

    @Inject private DSLContext dslContext;

    /**
     * @return the coerced project
     * @see
     *     org.squashtest.tm.service.internal.repository.CustomGenericProjectDao#coerceProjectIntoTemplate(long)
     *     (long)
     */
    @Override
    public ProjectTemplate coerceProjectIntoTemplate(long projectId) {
        Session session = getCurrentSession();

        Project project = (Project) session.load(Project.class, projectId);
        session.flush();
        session.evict(project);

        NativeQuery query =
                session.createNativeQuery(
                        "update PROJECT set PROJECT_TYPE = 'T',  TEMPLATE_ID = null where PROJECT_ID = :id");
        query.setParameter("id", projectId);
        final int changedRows = query.executeUpdate();
        if (changedRows != 1) {
            throw new HibernateException("Expected 1 changed row but got " + changedRows + " instead");
        }
        session.flush();

        return (ProjectTemplate) session.load(ProjectTemplate.class, projectId);
    }

    @Override
    public boolean isProjectTemplate(long projectId) {

        Query query = em.createNamedQuery("GenericProject.findProjectTypeOf");
        query.setParameter(ParameterNames.PROJECT_ID, projectId);

        String type = (String) query.getSingleResult();

        return "T".equals(type);
    }

    @Override
    public boolean isBoundToATemplate(long genericProjectId) {
        Query query = em.createNamedQuery("GenericProject.findBoundTemplateId");
        query.setParameter(ParameterNames.PROJECT_ID, genericProjectId);
        List<Long> templateIdss = query.getResultList();
        return !templateIdss.isEmpty();
    }

    @Override
    public boolean oneIsBoundToABoundProject(Collection<Long> bindingIds) {
        Query query = em.createNamedQuery("GenericProject.findBoundTemplateIdsFromBindingIds");
        query.setParameter("bindingIds", bindingIds);
        List<Long> templateIds = query.getResultList();
        return !templateIds.isEmpty();
    }

    @Override
    public GenericProject getProjectWithBugtrackerProjects(long projectId) {
        return em.createQuery(
                        "select p from GenericProject p left join fetch p.bugtrackerProjects where p.id = :projectId group by p",
                        GenericProject.class)
                .setParameter("projectId", projectId)
                .getSingleResult();
    }

    @Override
    public List<String> getEnvironmentTags(long genericProjectId) {
        return dslContext
                .select(AUTOMATION_ENVIRONMENT_TAG.VALUE)
                .from(AUTOMATION_ENVIRONMENT_TAG)
                .where(
                        AUTOMATION_ENVIRONMENT_TAG
                                .ENTITY_TYPE
                                .eq(AutomationEnvironmentTagHolder.PROJECT.name())
                                .and(AUTOMATION_ENVIRONMENT_TAG.ENTITY_ID.eq(genericProjectId)))
                .fetchInto(String.class);
    }

    @Override
    public boolean isInheritsEnvironmentTags(long genericProjectId) {
        return dslContext
                .select(PROJECT.INHERITS_ENVIRONMENT_TAGS)
                .from(PROJECT)
                .where(PROJECT.PROJECT_ID.eq(genericProjectId))
                .fetchOptional()
                .map(inheritsEnvironmentTags -> inheritsEnvironmentTags.into(Boolean.class))
                .orElse(false);
    }

    private Session getCurrentSession() {
        return em.unwrap(Session.class);
    }
}
