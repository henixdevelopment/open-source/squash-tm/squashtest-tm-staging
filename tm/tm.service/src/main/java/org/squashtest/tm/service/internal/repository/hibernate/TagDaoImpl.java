/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.customfield.TagsValue;
import org.squashtest.tm.service.internal.repository.TagDao;

/**
 * @author akatz
 */
@Repository
public class TagDaoImpl implements TagDao {

    @PersistenceContext private EntityManager em;

    @Override
    public List<TagsValue> getTagsValuesByTagLabelAndTestCaseIds(
            List<Long> testCasesIds, String tagLabel) {
        Query query =
                em.createQuery(
                        "SELECT tv FROM TagsValue tv "
                                + "JOIN FETCH tv.selectedOptions "
                                + "WHERE tv.boundEntityId IN (:testCasesIds) "
                                + "AND tv.boundEntityType = :entityType "
                                + "AND tv.binding.customField.inputType = :cufType "
                                + "AND tv.binding.customField.label = :tagLabel ");

        query.setParameter("testCasesIds", testCasesIds);
        query.setParameter("entityType", BindableEntity.TEST_CASE);
        query.setParameter("cufType", InputType.TAG);
        query.setParameter("tagLabel", tagLabel);
        return query.getResultList();
    }
}
