/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

public final class EntityGraphName {

    public static final String ACTION_WORD_LIBRARY = "actionWordLibrary";
    public static final String AUTOMATED_EXECUTION_EXTENDER = "automatedExecutionExtender";
    public static final String AUTOMATED_SUITES = "automatedSuites";
    public static final String AUTOMATED_TEST = "automatedTest";
    public static final String AUTOMATED_TEST_REFERENCE = "automatedTestReference";
    public static final String AUTOMATED_TEST_TECHNOLOGY = "automatedTestTechnology";
    public static final String AUTOMATION_REQUEST = "automationRequest";
    public static final String AUTOMATION_REQUEST_LIBRARY = "automationRequestLibrary";
    public static final String ATTACHMENTS = "attachments";
    public static final String ATTACHMENT_LIST = "attachmentList";
    public static final String BINDING = "binding";
    public static final String BUGTRACKER_PROJECTS = "bugtrackerProjects";
    public static final String CAMPAIGN = "campaign";
    public static final String CAMPAIGN_LIBRARY = "campaignLibrary";
    public static final String CHILDREN = "children";
    public static final String CONTENT = "content";
    public static final String CUSTOM_FIELD = "customField";
    public static final String CUSTOM_REPORT_LIBRARY = "customReportLibrary";
    public static final String DATASET = "dataset";
    public static final String DATASETS = "datasets";
    public static final String ENABLED_PLUGINS = "enabledPlugins";
    public static final String EXECUTION = "execution";
    public static final String EXECUTIONS = "executions";
    public static final String EXPLORATORY_SESSION_OVERVIEW = "exploratorySessionOverview";
    public static final String INFO_LIST = "infoList";
    public static final String ISSUE_LIST = "issueList";
    public static final String ITERATION = "iteration";
    public static final String ITERATIONS = "iterations";
    public static final String MILESTONES = "milestones";
    public static final String NATURE = "nature";
    public static final String PARAMETER = "parameter";
    public static final String PARAMETERS = "parameters";
    public static final String PARAMETER_VALUES = "parameterValues";
    public static final String PROJECT = "project";
    public static final String REFERENCED_DATASET = "referencedDataset";
    public static final String REFERENCED_TEST_CASE = "referencedTestCase";
    public static final String REQUIREMENT_LIBRARY = "requirementLibrary";
    public static final String REQUIREMENT_VERSION = "requirementVersion";
    public static final String SCM_REPOSITORY = "scmRepository";
    public static final String SCM_SERVER = "scmServer";
    public static final String SPRINT_REQ_VERSION = "sprintReqVersions";
    public static final String STEPS = "steps";
    public static final String SYNC_EXTENDER = "syncExtender";
    public static final String TEST_AUTOMATION_PROJECTS = "testAutomationProjects";
    public static final String TEST_CASE_LIBRARY = "testCaseLibrary";
    public static final String TEST_PLAN = "testPlan";
    public static final String TEST_PLANS = "testPlans";
    public static final String TEST_PLAN_ITEMS = "testPlanItems";
    public static final String TEST_SUITES = "testSuites";
    public static final String TYPE = "type";
    public static final String VERSIONS = "versions";

    private EntityGraphName() {}
}
