/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static java.util.Objects.requireNonNull;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.partitionBy;
import static org.jooq.impl.DSL.rowNumber;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.AUTOMATION_REQUEST_STATUS;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.CAMPAIGN_MILESTONE_END_DATE;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.CAMPAIGN_MILESTONE_ID;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.CAMPAIGN_MILESTONE_STATUS;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.CAMPAIGN_PROJECT_NAME;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.EXECUTION_EXECUTION_MODE;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.ITEM_TEST_PLAN_TESTER;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.TEST_CASE_AUTOMATABLE;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanList.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.tables.Iteration.ITERATION;
import static org.squashtest.tm.jooq.domain.tables.IterationTestPlanItem.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.grid.ColumnIds.MILESTONES_END_DATE;
import static org.squashtest.tm.service.internal.display.search.filter.ItpiExecutionScopeFilterHandler.extractEntityIdsFromScope;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CAMPAIGN_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITERATION_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LABEL;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LATEST_EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LATEST_ITPI_IDS_SUBQUERY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ROW_NUMBER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_IMPORTANCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.USER;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record8;
import org.jooq.SelectJoinStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.jooq.domain.tables.IterationTestPlanItem;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridSort;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.filters.GridFilterConditionBuilder;
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class CustomItpiLastExecutionFilterDaoImpl implements CustomItpiLastExecutionFilterDao {
    private static final List<EntityType> scopableEntityList =
            Arrays.asList(
                    EntityType.ITERATION,
                    EntityType.CAMPAIGN,
                    EntityType.CAMPAIGN_FOLDER,
                    EntityType.CAMPAIGN_LIBRARY,
                    EntityType.PROJECT,
                    EntityType.TEST_SUITE,
                    EntityType.EXECUTION);

    private static final String RANKED_IDS = "ranked_ids";
    private static final String EXECUTION_ON = "executionOn";
    private static final String ITEM_TEST_PLAN_EXECUTION_SCOPE = "itemTestPlanExecutionScope";

    @Inject HibernateCampaignDao campaignDao;

    private final DSLContext dsl;

    public CustomItpiLastExecutionFilterDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    /**
     * find the list of itpi in campaign scope. For each distinct tc/dataset pair, counts only the
     * last execution. If the last execution is null, it will return one of the unexecuted itpi for
     * that pair
     *
     * @param campaignIds campaign ids
     * @return list of itpi in campaign scope filtered by last executed and grouped by test case id
     *     and dataset id
     */
    @Override
    public List<Long> gatherLatestItpiIdsForTCInScopeForCampaign(List<Long> campaignIds) {
        IterationTestPlanItem itp =
                ITERATION_TEST_PLAN_ITEM.as(RequestAliasesConstants.ITERATION_TEST_PLAN_ITEM);

        Table<Record2<Long, Integer>> rankedIds =
                dsl.select(
                                itp.ITEM_TEST_PLAN_ID.as(ID),
                                rowNumber()
                                        .over(
                                                partitionBy(itp.TCLN_ID, itp.DATASET_ID)
                                                        .orderBy(
                                                                itp.LAST_EXECUTED_ON.desc().nullsLast(),
                                                                itp.CREATED_ON.desc(),
                                                                itp.ITEM_TEST_PLAN_ID.desc()))
                                        .as(ROW_NUMBER))
                        .from(CAMPAIGN)
                        .innerJoin(CAMPAIGN_ITERATION)
                        .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
                        .innerJoin(ITERATION)
                        .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                        .innerJoin(itp)
                        .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(itp.ITEM_TEST_PLAN_ID))
                        .where(CAMPAIGN.CLN_ID.in(campaignIds))
                        .asTable(RANKED_IDS);

        return dsl.select(rankedIds.field(ID, Long.class))
                .from(rankedIds)
                .where(rankedIds.field(ROW_NUMBER, int.class).eq(1))
                .fetchInto(Long.class);
    }

    /**
     * find the list of itpi in iteration scope. For each distinct tc/dataset pair, counts only the
     * last execution. If the last execution is null, it will return one of the unexecuted itpi for
     * that pair
     *
     * @param iterationIds iteration ids
     * @return list of itpi in iteration scope filtered by last executed and grouped by test case id
     *     and dataset id
     */
    @Override
    public List<Long> gatherLatestItpiIdsForTCInScopeForIteration(List<Long> iterationIds) {
        IterationTestPlanItem itp =
                ITERATION_TEST_PLAN_ITEM.as(RequestAliasesConstants.ITERATION_TEST_PLAN_ITEM);

        Table<Record2<Long, Integer>> rankedIds =
                dsl.select(
                                itp.ITEM_TEST_PLAN_ID.as(ID),
                                rowNumber()
                                        .over(
                                                partitionBy(itp.TCLN_ID, itp.DATASET_ID)
                                                        .orderBy(
                                                                itp.LAST_EXECUTED_ON.desc().nullsLast(),
                                                                itp.CREATED_ON.desc(),
                                                                itp.ITEM_TEST_PLAN_ID.desc()))
                                        .as(ROW_NUMBER))
                        .from(ITERATION)
                        .innerJoin(ITEM_TEST_PLAN_LIST)
                        .on(ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                        .innerJoin(itp)
                        .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(itp.ITEM_TEST_PLAN_ID))
                        .where(ITERATION.ITERATION_ID.in(iterationIds))
                        .asTable(RANKED_IDS);

        return dsl.select(rankedIds.field(ID, Long.class))
                .from(rankedIds)
                .where(rankedIds.field(ROW_NUMBER, int.class).eq(1))
                .fetchInto(Long.class);
    }

    /**
     * Given a dynamic scope that can contain various entity types, find and filter the last executed
     * itpi. If the itpi for a given tc/dataset pair has never been executed, it returns the id of the
     * last created itpi in the parameters.
     *
     * @param scopableEntitiesMap a map containing the list of ids in scope separated by EntityType
     * @return the list of last executed itpi ids in scope
     */
    @Override
    public List<Long> gatherLatestItpiIdsForTCInDynamicScope(
            Map<EntityType, List<Long>> scopableEntitiesMap) {

        SelectJoinStep<Record2<Long, Integer>> subquery =
                dsl.select(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ID),
                                rowNumber()
                                        .over(
                                                partitionBy(
                                                                ITERATION_TEST_PLAN_ITEM.TCLN_ID,
                                                                ITERATION_TEST_PLAN_ITEM.DATASET_ID)
                                                        .orderBy(
                                                                ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON.desc().nullsLast(),
                                                                ITERATION_TEST_PLAN_ITEM.CREATED_ON.desc(),
                                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.desc()))
                                        .as(ROW_NUMBER))
                        .from(ITERATION_TEST_PLAN_ITEM);

        if (!scopableEntitiesMap.isEmpty()) {
            Condition whereClause =
                    appendJoinClausesByScopeAndBuildWhereCondition(scopableEntitiesMap, subquery);
            subquery.where(whereClause);
        }

        return dsl.select(subquery.field(ID, Long.class))
                .from(subquery)
                .where(field(name(ROW_NUMBER)).eq(1))
                .fetchInto(Long.class);
    }

    @Override
    public Table getTableForTCInDynamicScope(GridRequest request) {
        Map<EntityType, List<Long>> scopableEntitiesMap = new EnumMap<>(EntityType.class);
        scopableEntityList.forEach(
                entityType ->
                        extractEntityIdsFromScope(request.getScope(), scopableEntitiesMap, entityType));

        if (!request.getExcludedScope().isEmpty()) {
            extractEntityIdsFromScope(
                    request.getExcludedScope(), scopableEntitiesMap, EntityType.TEST_SUITE);
        }

        return gatherLatestItpiIdsForTCInDynamicScopeAsTable(scopableEntitiesMap, request);
    }

    private Table gatherLatestItpiIdsForTCInDynamicScopeAsTable(
            Map<EntityType, List<Long>> scopableEntitiesMap, GridRequest request) {

        SelectJoinStep<Record8<Long, Long, Long, Long, String, Timestamp, String, Integer>> subquery =
                dsl.select(
                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(LATEST_EXECUTION_ID),
                                ITERATION_TEST_PLAN_ITEM.USER_ID.as(USER),
                                ITERATION_TEST_PLAN_ITEM.TCLN_ID.as(TEST_CASE_ID),
                                ITERATION_TEST_PLAN_ITEM.DATASET_ID.as(DATASET_ID),
                                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.as(EXECUTION_STATUS),
                                ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON.as(LAST_EXECUTED_ON),
                                ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY.as(LAST_EXECUTED_BY),
                                rowNumber()
                                        .over(
                                                partitionBy(
                                                                ITERATION_TEST_PLAN_ITEM.TCLN_ID,
                                                                ITERATION_TEST_PLAN_ITEM.DATASET_ID)
                                                        .orderBy(
                                                                ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON.desc().nullsLast(),
                                                                ITERATION_TEST_PLAN_ITEM.CREATED_ON.desc(),
                                                                ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.desc()))
                                        .as(ROW_NUMBER))
                        .from(ITERATION_TEST_PLAN_ITEM);

        if (!scopableEntitiesMap.isEmpty()) {
            Condition whereClause =
                    appendJoinClausesByScopeAndBuildWhereCondition(scopableEntitiesMap, subquery);
            subquery.where(whereClause);
        }

        Table<?> latestItpiIdsSubquery = subquery.asTable(LATEST_ITPI_IDS_SUBQUERY);
        // Apply optional filters and variable filters to the final query
        latestItpiIdsSubquery = addOptionalFilterToQuery(latestItpiIdsSubquery, request);
        Condition combinedCondition =
                craftVariableFiltersConditionsForQuery(latestItpiIdsSubquery, request);

        // Create the final selection on this derived table
        return dsl.select(buildDynamicFields(latestItpiIdsSubquery, request))
                .from(latestItpiIdsSubquery)
                .where(latestItpiIdsSubquery.field(ROW_NUMBER, Integer.class).eq(1))
                .and(combinedCondition)
                .asTable(RANKED_IDS);
    }

    private Condition craftVariableFiltersConditionsForQuery(
            Table<?> latestItpiIdsSubquery, GridRequest request) {

        // Build a combined condition from filters
        return request.getFilterValues().stream()
                .filter(filter -> !ITEM_TEST_PLAN_EXECUTION_SCOPE.equals(filter.getId()))
                .map(
                        filter ->
                                convertFilterToConditionTest(
                                        latestItpiIdsSubquery, filter)) // Map each filter to a condition
                .reduce(Condition::and) // Combine conditions with AND
                .orElse(null); // If no conditions, return null
    }

    protected Condition convertFilterToConditionTest(
            Table<?> latestItpiIdsSubquery, GridFilterValue gridFilterValue) {
        String id = gridFilterValue.getId();
        GridColumn gridColumn =
                requireNonNull(createAliasToFieldDictionary(latestItpiIdsSubquery).get(id));
        Field<?> field = gridColumn.getNativeField();

        if (EXECUTION_ON.equals(id) || MILESTONES_END_DATE.getColumnId().equals(id)) {
            field = DSL.timestamp((Field<? extends Timestamp>) field);
        }

        return GridFilterConditionBuilder.getConditionBuilder(field, gridFilterValue).build();
    }

    protected Map<String, GridColumn> createAliasToFieldDictionary(Table<?> latestItpiIdsSubquery) {
        return this.getGridColumnsForFilters(latestItpiIdsSubquery).stream()
                .collect(Collectors.toMap(GridColumn::findRuntimeAlias, column -> column));
    }

    private Condition appendJoinClausesByScopeAndBuildWhereCondition(
            Map<EntityType, List<Long>> scopableEntitiesMap, SelectJoinStep<?> subquery) {
        Condition whereCondition = DSL.noCondition();
        for (Map.Entry<EntityType, List<Long>> entityScope : scopableEntitiesMap.entrySet()) {
            appendJoinClausesByEntityType(entityScope, subquery);
            whereCondition = appendWhereClause(entityScope, whereCondition);
        }
        return whereCondition;
    }

    private void appendJoinClausesByEntityType(
            Map.Entry<EntityType, List<Long>> entityScope, SelectJoinStep<?> subquery) {
        // get index of entity type in ordered entity list to add all necessary preceding join clauses
        int index = scopableEntityList.indexOf(entityScope.getKey());
        for (int i = 0; i <= index; i++) {
            appendJoinClauseIfMissing(subquery, scopableEntityList.get(i));
        }
    }

    // by entity type, append join and where clauses if missing
    private void appendJoinClauseIfMissing(SelectJoinStep<?> subquery, EntityType entityType) {
        switch (entityType) {
            case ITERATION:
                if (!subquery.getSQL().toUpperCase().contains(Tables.ITERATION.ITERATION_ID.getName())) {
                    subquery
                            .leftJoin(Tables.ITEM_TEST_PLAN_LIST)
                            .on(
                                    Tables.ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(
                                            ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID));
                    subquery
                            .leftJoin(Tables.ITERATION)
                            .on(Tables.ITERATION.ITERATION_ID.eq(Tables.ITEM_TEST_PLAN_LIST.ITERATION_ID));
                }
                break;
            case CAMPAIGN, CAMPAIGN_FOLDER:
                if (!subquery.getSQL().toUpperCase().contains(CAMPAIGN_ITERATION.getName())) {
                    subquery
                            .leftJoin(CAMPAIGN_ITERATION)
                            .on(CAMPAIGN_ITERATION.ITERATION_ID.eq(Tables.ITERATION.ITERATION_ID));
                }
                break;
            case CAMPAIGN_LIBRARY, PROJECT:
                if (!subquery.getSQL().toUpperCase().contains(CAMPAIGN_LIBRARY_NODE.getName())) {
                    subquery
                            .leftJoin(CAMPAIGN_LIBRARY_NODE)
                            .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID));
                }
                break;
            case TEST_SUITE:
                if (!subquery.getSQL().toUpperCase().contains(TEST_SUITE_TEST_PLAN_ITEM.getName())) {
                    subquery
                            .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                            .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID));
                }
                break;
            default:
                break;
        }
    }

    private Condition appendWhereClause(
            Map.Entry<EntityType, List<Long>> entityScope, Condition whereCondition) {
        List<Long> ids = entityScope.getValue();

        return switch (entityScope.getKey()) {
            case ITERATION -> whereCondition.or(Tables.ITERATION.ITERATION_ID.in(ids));
            case CAMPAIGN -> whereCondition.or(CAMPAIGN_ITERATION.CAMPAIGN_ID.in(ids));
            case CAMPAIGN_FOLDER -> getFolderWhereCondition(whereCondition, ids);
            case CAMPAIGN_LIBRARY, PROJECT -> whereCondition.or(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(ids));
            case TEST_SUITE -> whereCondition.or(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.in(ids));
            default -> whereCondition;
        };
    }

    private Condition getFolderWhereCondition(Condition whereCondition, List<Long> ids) {
        List<Long> campaignIdsInFolder = campaignDao.findAllCampaignIdsByNodeIds(ids);
        return whereCondition.or(CAMPAIGN_ITERATION.CAMPAIGN_ID.in(campaignIdsInFolder));
    }

    private Table<?> addOptionalFilterToQuery(
            Table<?> latestItpiIdsSubquery, GridRequest gridRequest) {

        boolean campaignJoinsApplied = false;
        boolean commonJoinsApplied = false;
        boolean milestoneJoinsApplied = false;
        boolean coreUserJoinsApplied = false;

        if (isTestCaseJoinRequired(gridRequest, true)) {
            latestItpiIdsSubquery =
                    latestItpiIdsSubquery
                            .leftJoin(TEST_CASE)
                            .on(latestItpiIdsSubquery.field(TEST_CASE_ID, Long.class).eq(TEST_CASE.TCLN_ID));
        }

        Set<String> columnPrototypes = new LinkedHashSet<>();

        for (GridSort sort : gridRequest.getSort()) {
            columnPrototypes.add(sort.getColumnPrototype());
        }

        for (GridFilterValue filter : gridRequest.getFilterValues()) {
            columnPrototypes.add(filter.getColumnPrototype());
        }

        for (String value : columnPrototypes) {
            switch (value) {
                case TEST_CASE_NAME:
                    latestItpiIdsSubquery =
                            latestItpiIdsSubquery
                                    .leftJoin(TEST_CASE_LIBRARY_NODE)
                                    .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID));
                    break;

                case AUTOMATION_REQUEST_STATUS:
                    latestItpiIdsSubquery =
                            latestItpiIdsSubquery
                                    .leftJoin(AUTOMATION_REQUEST)
                                    .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID));
                    break;

                case DATASET_NAME:
                    latestItpiIdsSubquery =
                            latestItpiIdsSubquery
                                    .leftJoin(DATASET)
                                    .on(latestItpiIdsSubquery.field(DATASET_ID, Long.class).eq(DATASET.DATASET_ID));
                    break;

                case EXECUTION_EXECUTION_MODE:
                    latestItpiIdsSubquery =
                            latestItpiIdsSubquery
                                    .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                                    .on(
                                            latestItpiIdsSubquery
                                                    .field(LATEST_EXECUTION_ID, Long.class)
                                                    .eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                                    .leftJoin(EXECUTION)
                                    .on((ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID).eq(EXECUTION.EXECUTION_ID));
                    break;

                case ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN, ITEM_TEST_PLAN_TESTER:
                    if (!coreUserJoinsApplied) {
                        latestItpiIdsSubquery =
                                latestItpiIdsSubquery
                                        .leftJoin(CORE_USER)
                                        .on(latestItpiIdsSubquery.field(USER, Long.class).eq(CORE_USER.PARTY_ID));
                        coreUserJoinsApplied = true;
                    }
                    break;
                case ITERATION_NAME:
                case CAMPAIGN_NAME:
                case CAMPAIGN_PROJECT_NAME:
                case CAMPAIGN_MILESTONE_ID:
                case CAMPAIGN_MILESTONE_STATUS:
                case CAMPAIGN_MILESTONE_END_DATE:
                    if (!commonJoinsApplied) {
                        latestItpiIdsSubquery =
                                latestItpiIdsSubquery
                                        .leftJoin(ITEM_TEST_PLAN_LIST)
                                        .on(
                                                latestItpiIdsSubquery
                                                        .field(LATEST_EXECUTION_ID, Long.class)
                                                        .eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                                        .leftJoin(ITERATION)
                                        .on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
                                        .leftJoin(CAMPAIGN_ITERATION)
                                        .on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
                                        .leftJoin(CAMPAIGN)
                                        .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID));
                        commonJoinsApplied = true;
                    }

                    if (value.equals(CAMPAIGN_MILESTONE_ID)
                            || value.equals(CAMPAIGN_MILESTONE_STATUS)
                            || value.equals(CAMPAIGN_MILESTONE_END_DATE)) {
                        if (!milestoneJoinsApplied) {
                            latestItpiIdsSubquery =
                                    latestItpiIdsSubquery
                                            .leftJoin(MILESTONE_CAMPAIGN)
                                            .on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(MILESTONE_CAMPAIGN.CAMPAIGN_ID))
                                            .leftJoin(MILESTONE)
                                            .on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID));
                            milestoneJoinsApplied = true;
                        }
                    }

                    if (value.equals(CAMPAIGN_NAME) || value.equals(CAMPAIGN_PROJECT_NAME)) {
                        if (!campaignJoinsApplied) {
                            latestItpiIdsSubquery =
                                    latestItpiIdsSubquery
                                            .leftJoin(CAMPAIGN_LIBRARY_NODE)
                                            .on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                            .leftJoin(PROJECT)
                                            .on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID));
                            campaignJoinsApplied = true;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return latestItpiIdsSubquery;
    }

    private boolean isTestCaseJoinRequired(
            GridRequest gridRequest, boolean addAutomationRequestStatus) {
        List<String> columnsThatNeedJoinToTestCase =
                new ArrayList<>(
                        List.of(
                                TEST_CASE_ID,
                                TEST_CASE_REFERENCE,
                                TEST_CASE_NAME,
                                TEST_CASE_IMPORTANCE,
                                TEST_CASE_AUTOMATABLE));
        if (addAutomationRequestStatus) {
            columnsThatNeedJoinToTestCase.add(AUTOMATION_REQUEST_STATUS);
        }
        return Stream.concat(
                        gridRequest.getFilterValues().stream().map(GridFilterValue::getColumnPrototype),
                        gridRequest.getSort().stream().map(GridSort::getColumnPrototype))
                .anyMatch(columnsThatNeedJoinToTestCase::contains);
    }

    private List<Field<?>> buildDynamicFields(Table<?> latestItpiIdsSubquery, GridRequest request) {
        Field<Integer> customOrderField =
                DSL.case_()
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.READY.name()),
                                1)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.RUNNING.name()),
                                2)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.SUCCESS.name()),
                                3)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.WARNING.name()),
                                4)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.SETTLED.name()),
                                5)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.SKIPPED.name()),
                                6)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.CANCELLED.name()),
                                7)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.FAILURE.name()),
                                8)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.BLOCKED.name()),
                                10)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.NOT_RUN.name()),
                                11)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.ERROR.name()),
                                12)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.UNTESTABLE.name()),
                                13)
                        .when(
                                latestItpiIdsSubquery
                                        .field(EXECUTION_STATUS, String.class)
                                        .eq(ExecutionStatus.NOT_FOUND.name()),
                                14)
                        .otherwise(-1000);

        Field<Integer> customImportancemOrderField =
                DSL.case_()
                        .when(TEST_CASE.IMPORTANCE.eq(TestCaseImportance.VERY_HIGH.name()), 1)
                        .when(TEST_CASE.IMPORTANCE.eq(TestCaseImportance.HIGH.name()), 2)
                        .when(TEST_CASE.IMPORTANCE.eq(TestCaseImportance.MEDIUM.name()), 3)
                        .when(TEST_CASE.IMPORTANCE.eq(TestCaseImportance.LOW.name()), 4)
                        .otherwise(-1000);

        // Base fields that are always included
        List<Field<?>> fields = new ArrayList<>();
        fields.add(latestItpiIdsSubquery.field(LATEST_EXECUTION_ID, Long.class));
        fields.add(latestItpiIdsSubquery.field(USER, Long.class));
        fields.add(latestItpiIdsSubquery.field(TEST_CASE_ID, Long.class));
        fields.add(latestItpiIdsSubquery.field(ROW_NUMBER, Long.class));
        fields.add(latestItpiIdsSubquery.field(EXECUTION_STATUS, Long.class));
        fields.add(latestItpiIdsSubquery.field(LAST_EXECUTED_ON, Long.class));
        fields.add(latestItpiIdsSubquery.field(LAST_EXECUTED_BY, Long.class));
        fields.add(customOrderField.as("EXECUTION_STATUS_PRIORITY"));

        Set<String> columnPrototypes = new LinkedHashSet<>();
        boolean coreUserJoinsApplied = false;

        for (GridSort sort : request.getSort()) {
            columnPrototypes.add(sort.getColumnPrototype());
        }

        for (GridFilterValue filter : request.getFilterValues()) {
            columnPrototypes.add(filter.getColumnPrototype());
        }

        if (isTestCaseJoinRequired(request, false)) {
            fields.add(TEST_CASE.TCLN_ID);
            fields.add(TEST_CASE.REFERENCE.as(TEST_CASE_REFERENCE));
            fields.add(TEST_CASE.AUTOMATABLE.as(TEST_CASE_AUTOMATABLE));
            fields.add(customImportancemOrderField.as(TEST_CASE_IMPORTANCE));
        }

        for (String value : columnPrototypes) {
            switch (value) {
                case TEST_CASE_NAME:
                    fields.add(TEST_CASE_LIBRARY_NODE.NAME.as(TEST_CASE_NAME));
                    break;
                case DATASET_NAME:
                    fields.add(DATASET.NAME.as(DATASET_NAME));
                    break;
                case EXECUTION_EXECUTION_MODE:
                    fields.add(EXECUTION.EXECUTION_MODE.as(EXECUTION_EXECUTION_MODE));
                    break;
                case AUTOMATION_REQUEST_STATUS:
                    fields.add(AUTOMATION_REQUEST.REQUEST_STATUS.as(AUTOMATION_REQUEST_STATUS));
                    break;
                case ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN, ITEM_TEST_PLAN_TESTER:
                    if (!coreUserJoinsApplied) {
                        fields.add(CORE_USER.LOGIN.as(ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN));
                        coreUserJoinsApplied = true;
                    }
                    break;
                case ITERATION_NAME:
                    fields.add(ITERATION.NAME.as(ITERATION_NAME));
                    break;
                case CAMPAIGN_NAME:
                    fields.add(CAMPAIGN_LIBRARY_NODE.NAME.as(CAMPAIGN_NAME));
                    break;
                case CAMPAIGN_PROJECT_NAME:
                    fields.add(PROJECT.NAME.as(CAMPAIGN_PROJECT_NAME));
                    break;
                case CAMPAIGN_MILESTONE_ID:
                    fields.add(MILESTONE_CAMPAIGN.CAMPAIGN_ID.as(CAMPAIGN_MILESTONE_ID));
                    break;
                case CAMPAIGN_MILESTONE_STATUS:
                    fields.add(MILESTONE.STATUS.as(CAMPAIGN_MILESTONE_STATUS));
                    break;
                case CAMPAIGN_MILESTONE_END_DATE:
                    fields.add(MILESTONE.END_DATE.as(CAMPAIGN_MILESTONE_END_DATE));
                    break;
                default:
                    break;
            }
        }
        return fields;
    }

    protected List<GridColumn> getGridColumnsForFilters(Table<?> latestItpiIdsSubquery) {
        return List.of(
                new GridColumn(ITERATION_TEST_PLAN_ITEM.TCLN_ID.as(ID), TEST_CASE.TCLN_ID),
                new GridColumn(TEST_CASE.REFERENCE),
                new GridColumn(TEST_CASE_LIBRARY_NODE.NAME),
                new GridColumn(TEST_CASE.AUTOMATABLE),
                new GridColumn(TEST_CASE.IMPORTANCE),
                new GridColumn(
                        AUTOMATION_REQUEST.REQUEST_STATUS.as(AUTOMATION_REQUEST_STATUS),
                        AUTOMATION_REQUEST.REQUEST_STATUS),
                new GridColumn(ITERATION_TEST_PLAN_ITEM.USER_ID.as(ASSIGNEE), CORE_USER.LOGIN),
                new GridColumn(
                        ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON.as(RequestAliasesConstants.EXECUTION_ON),
                        latestItpiIdsSubquery.field(LAST_EXECUTED_ON)),
                new GridColumn(
                        ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY.as(EXECUTED_BY),
                        latestItpiIdsSubquery.field(LAST_EXECUTED_BY)),
                new GridColumn(
                        EXECUTION.EXECUTION_MODE.as(INFERRED_EXECUTION_MODE), EXECUTION.EXECUTION_MODE),
                new GridColumn(
                        ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS,
                        latestItpiIdsSubquery.field(EXECUTION_STATUS)),
                new GridColumn(
                        MILESTONE.MILESTONE_ID.as(RequestAliasesConstants.MILESTONE_LABEL),
                        MILESTONE.MILESTONE_ID),
                new GridColumn(
                        MILESTONE.STATUS.as(RequestAliasesConstants.MILESTONE_STATUS), MILESTONE.STATUS),
                new GridColumn(
                        MILESTONE.END_DATE.as(RequestAliasesConstants.MILESTONE_END_DATE), MILESTONE.END_DATE));
    }

    protected List<GridColumn> getColumnsForSort(Table<?> rankedIds) {
        return List.of(
                new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as(LABEL), rankedIds.field(TEST_CASE_NAME)),
                new GridColumn((TEST_CASE.REFERENCE), rankedIds.field(TEST_CASE_REFERENCE)),
                new GridColumn((TEST_CASE.AUTOMATABLE), rankedIds.field(TEST_CASE_AUTOMATABLE)),
                new GridColumn((TEST_CASE.IMPORTANCE), rankedIds.field(TEST_CASE_IMPORTANCE)),
                new GridColumn(
                        AUTOMATION_REQUEST.REQUEST_STATUS, rankedIds.field(AUTOMATION_REQUEST_STATUS)),
                new GridColumn(DATASET.NAME.as(DATASET_NAME), rankedIds.field(DATASET_NAME)),
                new GridColumn(
                        CAMPAIGN_LIBRARY_NODE.NAME.as(CAMPAIGN_NAME), rankedIds.field(CAMPAIGN_NAME)),
                new GridColumn(PROJECT.NAME.as(PROJECT_NAME), rankedIds.field(CAMPAIGN_PROJECT_NAME)),
                new GridColumn(ITERATION.NAME.as(ITERATION_NAME), rankedIds.field(ITERATION_NAME)),
                new GridColumn(
                        ITERATION_TEST_PLAN_ITEM.USER_ID.as(ASSIGNEE_LOGIN),
                        rankedIds.field(ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN)),
                new GridColumn(
                        EXECUTION.EXECUTION_MODE.as(EXECUTION_EXECUTION_MODE),
                        rankedIds.field(EXECUTION_EXECUTION_MODE)),
                new GridColumn(
                        ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS,
                        rankedIds.field("EXECUTION_STATUS_PRIORITY")),
                new GridColumn(rankedIds.field(LAST_EXECUTED_ON)),
                new GridColumn(rankedIds.field(LAST_EXECUTED_BY)));
    }

    public SortField<?> convertGridSortToOrderBy(Table<?> rankedIds, GridSort gridSort) {
        GridSort.SortDirection direction = gridSort.getDirection();
        String id = gridSort.getProperty();
        Map<String, GridColumn> aliasToFieldMap = createAliasToFieldDictionaryForOrder(rankedIds);

        GridColumn gridColumn = requireNonNull(aliasToFieldMap.get(id));

        Field<?> nativeField = gridColumn.getNativeField();

        return direction == GridSort.SortDirection.ASC ? nativeField.asc() : nativeField.desc();
    }

    private Map<String, GridColumn> createAliasToFieldDictionaryForOrder(Table<?> rankedIds) {
        return this.getColumnsForSort(rankedIds).stream()
                .collect(Collectors.toMap(GridColumn::findRuntimeAlias, column -> column));
    }
}
