/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher.deserializer

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.IOUtils
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState
import spock.lang.Specification

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

/**
 * @author akatz
 *
 * This class tests the deserializer as used as an annotation on the Attachment DTO.
 */
class StreamDeserializerTest extends Specification {

    def "should correctly deserialize the content of the stream located inside an attachment of an AutomatedExecutionState"() {

        given:
        def jsonContent = Files.readString(Paths.get('src/test/resources/resultpublisher/successStatusUpdate.json'))


        when:
            AutomatedExecutionState state = new ObjectMapper(new JsonFactory()).readValue(jsonContent, AutomatedExecutionState.class)

        then:
            def content = normalizeEOL(IOUtils.toString(state.getAttachments().get(0).getContent(), StandardCharsets.UTF_8))
            def fileContent = normalizeEOL(new String(Files.readAllBytes(Paths.get("src/test/resources/resultpublisher/hello-world-report.bintxt"))))

            assert content == fileContent
    }

    def normalizeEOL(String text) {
        text.replaceAll("\\r\\n", "\n").replaceAll("\\r", "\n")
    }


}
