/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot

import org.springframework.context.MessageSource
import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.project.AutomationWorkflowType
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.KeywordTestCase
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest
import spock.lang.Specification

import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuildersTestUtils.createActionWordStep

class SettingSectionBuilderTest extends Specification {

	MessageSource messageSource = Mock()

	void setup() {
		messageSource.getMessage("testcase.bdd.script.label.test-case-importance", null, _ as Locale) >> "Test case importance"
		messageSource.getMessage("testcase.bdd.script.label.automation-priority", null, _ as Locale) >> "Automation priority"
		messageSource.getMessage("test-case.importance.LOW", null, _ as Locale) >> "Low"
		messageSource.getMessage("label.id", null, _ as Locale) >> "ID"
		messageSource.getMessage("test-case.reference.label", null, _ as Locale) >> "Reference"
	}

	def "with minimal settings"() {
		given:
		def keywordTestCase = Spy(KeywordTestCase)
		keywordTestCase.notifyAssociatedWithProject(new Project())
		keywordTestCase.getName() >> "My minimal test case"
		keywordTestCase.getId() >> 341L

		when:
		String result = SettingSectionBuilder.buildSettingsSection(keywordTestCase, messageSource)

		then:
		result ==
			"""*** Settings ***
Documentation    My minimal test case
Metadata         ID                           341
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


"""
	}

	def "with dataset"() {
		given:
		def keywordTestCase = Spy(KeywordTestCase)
		keywordTestCase.notifyAssociatedWithProject(new Project())
		keywordTestCase.getName() >> "My minimal test case"
		keywordTestCase.getId() >> 341L

		and:
		keywordTestCase.addStep(createActionWordStep(Keyword.GIVEN, "I say <hey>"))
		keywordTestCase.addStep(createActionWordStep(Keyword.THEN, "you say <yo>"))
		keywordTestCase.addDataset(new Dataset("my dataset", keywordTestCase))

		when:
		String result = SettingSectionBuilder.buildSettingsSection(keywordTestCase, messageSource)

		then:
		result ==
			"""*** Settings ***
Documentation    My minimal test case
Metadata         ID                           341
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


"""
	}

	def "with tc description"() {
		given:
		def keywordTestCase = Spy(KeywordTestCase)
		keywordTestCase.notifyAssociatedWithProject(new Project())
		keywordTestCase.getName() >> "Greetings test"
		keywordTestCase.getId() >> 341L
		keywordTestCase.getDescription() >> "<p>Hello, <b>world!</b></p><p>How are you today?</p>"

		when:
		String result = SettingSectionBuilder.buildSettingsSection(keywordTestCase, messageSource)

		then:
		result ==
			"""*** Settings ***
Documentation    Greetings test
...
...              Hello, world!
...
...              How are you today?
Metadata         ID                           341
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


"""
	}

	def "with tc reference"() {
		given:
		def keywordTestCase = Spy(KeywordTestCase)
		keywordTestCase.notifyAssociatedWithProject(new Project())
		keywordTestCase.getName() >> "Disconnection test"
		keywordTestCase.getId() >> 341L
		keywordTestCase.getReference() >> "MyRef"

		when:
		String result = SettingSectionBuilder.buildSettingsSection(keywordTestCase, messageSource)

		then:
		result ==
			"""*** Settings ***
Documentation    Disconnection test
Metadata         ID                           341
Metadata         Reference                    MyRef
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


"""
	}

	def "with automation priority"() {
		given:
		def keywordTestCase = Spy(KeywordTestCase)
		def project = new Project()
		keywordTestCase.notifyAssociatedWithProject(project)
		keywordTestCase.getName() >> "Disconnection test"
		keywordTestCase.getId() >> 341L
		keywordTestCase.getReference() >> "MyRef"
		keywordTestCase.automationRequest = new AutomationRequest()
		keywordTestCase.automationRequest.automationPriority = 42

		project.setAutomationWorkflowType(AutomationWorkflowType.NATIVE)

		when:
		String result = SettingSectionBuilder.buildSettingsSection(keywordTestCase, messageSource)

		then:
		result ==
			"""*** Settings ***
Documentation    Disconnection test
Metadata         ID                           341
Metadata         Reference                    MyRef
Metadata         Automation priority          42
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


"""
	}

	def "without automation workflow"() {
		given:
		def keywordTestCase = Spy(KeywordTestCase)
		def project = new Project()
		keywordTestCase.notifyAssociatedWithProject(project)
		keywordTestCase.getName() >> "Disconnection test"
		keywordTestCase.getId() >> 341L
		keywordTestCase.automationRequest = new AutomationRequest()
		keywordTestCase.automationRequest.automationPriority = 42

		// Automation priority should not be shown
		project.setAutomationWorkflowType(AutomationWorkflowType.NONE)

		when:
		String result = SettingSectionBuilder.buildSettingsSection(keywordTestCase, messageSource)

		then:
		result ==
			"""*** Settings ***
Documentation    Disconnection test
Metadata         ID                           341
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


"""
	}
}
