/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.messages

import org.apache.commons.io.IOUtils
import org.opentestfactory.dto.v1.JobDTO
import org.squashtest.squash.automation.tm.testplan.library.model.Credentials
import org.squashtest.squash.automation.tm.testplan.library.model.CredentialsType
import org.squashtest.squash.automation.tm.testplan.library.model.ExecutionConfiguration
import org.squashtest.squash.automation.tm.testplan.library.model.Project
import org.squashtest.squash.automation.tm.testplan.library.model.SourceVersion
import org.squashtest.squash.automation.tm.testplan.library.model.TestExecution
import org.squashtest.squash.automation.tm.testplan.library.model.TestPlan
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext
import org.squashtest.tm.service.internal.testautomation.model.messages.Workflow
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.ArrayExpectedInAdditionalConfigurationException
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.ObjectExpectedInAdditionalConfigurationException
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.EmptySectionInAdditionalConfigurationException
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.UnknownKeyInAdditionalConfigurationException
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException
import spock.lang.Specification

import java.util.stream.Collectors

class WorkflowTest extends Specification{

    private TestPlan testPlanMock
    private TestPlanContext testPlanContextMock

    private static final String PLACEHOLDER_TEST_ID = "placeholderTestId"

    def setup() {
        testPlanMock = Mock(TestPlan)
        testPlanContextMock = Mock(TestPlanContext)
    }

    def "should not create a workflow without TestPlan"() {
        when:
        new Workflow("apiVersion", "name", null, testPlanContextMock, null)

        then:
        thrown NullPointerException
    }

    def "should add name in metadata map"() {
        given:
        testPlanMock.getProjects() >> getProjectList()
        testPlanMock.getConfigurations() >> createExecutionConfigurations()

        when:
        Workflow res = new Workflow("apiVersion", "aName", testPlanMock, testPlanContextMock, null)

        then:
        res.name() == "aName"
    }

    def "should create workflow with testcase metadata"() {
        given:
        testPlanMock.getProjects() >> [createProjectWithTestMetadata()]
        testPlanMock.getConfigurations() >> createExecutionConfigurations()

        when:
        Workflow wf = new Workflow("apiVersion", "wfName", testPlanMock, testPlanContextMock, null)

        then:
        Map testCaseMetadata = wf.getMetadata().get("managedTests").get("testCases")
        testCaseMetadata.keySet().size() == 1
        String execStepUuid = testCaseMetadata.keySet().getAt(0)
        UUID.fromString(execStepUuid)
        def expectedEntries = [path:["path", "to", "test"], nature:"BUSINESS", importance:"MEDIUM", name:"testTestCase", technology:"RobotFramework"]
        expectedEntries.each {entry -> testCaseMetadata[execStepUuid][entry.key] == entry.value}

    }

    def "should create workflow with test plan metadata"() {
        given:
        testPlanMock.getProjects() >> []
        testPlanMock.getConfigurations() >> createExecutionConfigurations()
        testPlanContextMock.getEntityUuid() >> "enTityUuid"
        testPlanContextMock.getEntityPathAsList() >> ["entity", "path"]
        testPlanContextMock.getEntityTypeAsString() >> "iteRation"

        when:
        Workflow wf = new Workflow("apiVersion", "wfName", testPlanMock, testPlanContextMock, null)

        then:
        wf.getMetadata().get("managedTests").get("testPlan") == [path: ["entity", "path"], type: "iteRation", uuid: "enTityUuid"]
    }

    def "should create workflow with valid additional configuration"() {
        given:
        testPlanMock.getProjects() >> []
        testPlanMock.getConfigurations() >> createExecutionConfigurations()
        testPlanContextMock.getEntityUuid() >> "entityUuid"
        testPlanContextMock.getEntityPathAsList() >> ["entity", "path"]
        testPlanContextMock.getEntityTypeAsString() >> "iteration"

        when:
        InputStream addConfInputStream =  this.getClass().getClassLoader().getResourceAsStream("squashautom/validAdditionalConfiguration.yaml")
        Workflow workflow = new Workflow("apiVersion", "workflowName", testPlanMock, testPlanContextMock,
            addConfInputStream.getText('UTF-8'))

        then:
        workflow.getHooks() ==  [[name:"The Hello World hook!", events:[[categoryPrefix:"actions", category:"checkout"]], before:[[run:"echo Hello World !"]]]]
        workflow.getVariables() == [VAR1:"iAmVar1", VAR2:"iAmVar2", VAR3:"iAmVar3"]
        workflow.getMetadata() == [managedTests:[testCases:[:], testPlan:[path:["entity", "path"], type:"iteration", uuid:"entityUuid"]], name:"workflowName", annotations:["squashtest.org/originating-projects":["1", "2"]], blob:"blib", labels:[foo:"bar"]]
    }

    def "should throw error when invalid yaml syntax in additional configuration"() {
        given:
        testPlanMock.getProjects() >> []
        testPlanMock.getConfigurations() >> createExecutionConfigurations()
        testPlanContextMock.getEntityUuid() >> "entityUuid"
        testPlanContextMock.getEntityPathAsList() >> ["entity", "path"]
        testPlanContextMock.getEntityTypeAsString() >> "iteration"

        when:
        InputStream addConfInputStream =  this.getClass().getClassLoader().getResourceAsStream("squashautom/invalidYamlSyntaxInAdditionalConfiguration.yaml")
        Workflow workflow = new Workflow("apiVersion", "workflowName", testPlanMock, testPlanContextMock,addConfInputStream.getText('UTF-8'))

        then:
        workflow == null
        thrown InvalidSquashOrchestratorConfigurationException
    }

    def "should throw error when a key is unknown in additional configuration"() {
        given:
        testPlanMock.getProjects() >> []
        testPlanMock.getConfigurations() >> createExecutionConfigurations()
        testPlanContextMock.getEntityUuid() >> "entityUuid"
        testPlanContextMock.getEntityPathAsList() >> ["entity", "path"]
        testPlanContextMock.getEntityTypeAsString() >> "iteration"

        when:
        InputStream addConfInputStream =  this.getClass().getClassLoader().getResourceAsStream("squashautom/unknownKeyInAdditionalConfigurationObjectExpected.yaml")
        Workflow workflow = new Workflow("apiVersion", "workflowName", testPlanMock, testPlanContextMock,addConfInputStream.getText('UTF-8'))

        then:
        workflow == null
        thrown UnknownKeyInAdditionalConfigurationException
    }

    def "should throw error when an object is empty in additional configuration"() {
        given:
        testPlanMock.getProjects() >> []
        testPlanMock.getConfigurations() >> createExecutionConfigurations()
        testPlanContextMock.getEntityUuid() >> "entityUuid"
        testPlanContextMock.getEntityPathAsList() >> ["entity", "path"]
        testPlanContextMock.getEntityTypeAsString() >> "iteration"

        when:
        InputStream addConfInputStream =  this.getClass().getClassLoader().getResourceAsStream("squashautom/emptyObjectInAdditionalConfigurationObjectExpected.yaml")
        Workflow workflow = new Workflow("apiVersion", "workflowName", testPlanMock, testPlanContextMock,addConfInputStream.getText('UTF-8'))

        then:
        workflow == null
        thrown EmptySectionInAdditionalConfigurationException
    }

    def "should throw error when an array is expected in additional configuration"() {
        given:
        testPlanMock.getProjects() >> []
        testPlanMock.getConfigurations() >> createExecutionConfigurations()
        testPlanContextMock.getEntityUuid() >> "entityUuid"
        testPlanContextMock.getEntityPathAsList() >> ["entity", "path"]
        testPlanContextMock.getEntityTypeAsString() >> "iteration"

        when:
        InputStream addConfInputStream =  this.getClass().getClassLoader().getResourceAsStream("squashautom/invalidAdditionalConfigurationArrayExpected.yaml")
        Workflow workflow = new Workflow("apiVersion", "workflowName", testPlanMock, testPlanContextMock,addConfInputStream.getText('UTF-8'))

        then:
        workflow == null
        thrown ArrayExpectedInAdditionalConfigurationException
    }

    def "should throw error when an object is expected in additional configuration"() {
        given:
        testPlanMock.getProjects() >> []
        testPlanMock.getConfigurations() >> createExecutionConfigurations()
        testPlanContextMock.getEntityUuid() >> "entityUuid"
        testPlanContextMock.getEntityPathAsList() >> ["entity", "path"]
        testPlanContextMock.getEntityTypeAsString() >> "iteration"

        when:
        InputStream addConfInputStream =  this.getClass().getClassLoader().getResourceAsStream("squashautom/invalidAdditionalConfigurationObjectExpected.yaml")
        Workflow workflow = new Workflow("apiVersion", "workflowName", testPlanMock, testPlanContextMock,addConfInputStream.getText('UTF-8'))

        then:
        workflow == null
        thrown ObjectExpectedInAdditionalConfigurationException
    }

    def "should generate all jobs"() {
        given:
        testPlanMock.getProjects() >> getProjectList()
        testPlanMock.getConfigurations() >> createExecutionConfigurations()

        when:
        Workflow res = new Workflow("apiVersion", "aName", testPlanMock, testPlanContextMock, null)

        then:
        res.getJobs().size() == 2
    }

    def "should generate jobs with params"() {
        given:
        TestExecution exec1 = createExecution("TestSuite1.robot#testCase1", "CUF_Value")
        TestExecution exec2 = createExecution("TestSuite1.robot#testCase1", "CUF_Value2")

        and:
        Project projectWithParam = createProjectWithParms("https://bitbucket.org/squadhstest/aProjectWithParms", exec1, exec2)
        testPlanMock.getProjects() >> [projectWithParam]
        testPlanMock.getConfigurations() >> createExecutionConfigurations()

        when:
        Workflow res = new Workflow("apiVersion", "aName", testPlanMock, testPlanContextMock, null)

        then:
        res.getJobs().size() == 1
        JobDTO jobDTO = res.getJobs().entrySet().iterator().next().getValue()
        jobDTO.getSteps().size() == 5
    }

    def "should generate jobs with runsOn"() {
        given:
            Project projectWithEnvironmentTags =
                    createSampleProjectWithEnvironmentTags(inputProjectTags, inputProjectCategory)
        and:
            testPlanMock.getProjects() >> [projectWithEnvironmentTags]
            testPlanMock.getConfigurations() >> createExecutionConfigurations()
        when:
            Workflow result = new Workflow("apiVersion", "workflowName", testPlanMock, testPlanContextMock, null)
        then:
            JobDTO jobDto = result.getJobs().entrySet().first().getValue()
            jobDto.runsOn() == expectedJobTags
        where:
            inputProjectTags                    | inputProjectCategory           | expectedJobTags
            ["linux", "robotframework", "ssh"]  | "robotframework/execute@v1"    | ["linux", "robotframework", "ssh"]
            ["linux"]                           | "robotframework/execute@v1"    | ["linux", "robotframework"]
            []                                  | "robotframework/execute@v1"    | ["robotframework"]
    }

    private static def getProjectList(){
        Project firstProject = createProject(
                "https://bitbucket.org/squashtest/testExample",
                "Category1/firstSuite.robot/#testCase1",
                "Category1/second_suite.robot#testCase2")

        Project secondProject = createProject(
                "https://bitbucket.org/squadhstest/anotherProject",
                "TestSuite1.robot#testCase1")

        return [firstProject, secondProject]
    }

    private static def createProject (String projectUrl, String... testReferences) {
        try {
            List<TestExecution> tests=
                    Arrays.asList(testReferences)
                            .stream()
                            .map({ ref -> new TestExecution(ref, Collections.emptyMap(), UUID.randomUUID().toString()) })
                            .toList();
            return new Project(new SourceVersion(new URL(projectUrl),"master"), tests, "toto/execute@v1", new Credentials(CredentialsType.BASIC_AUTH, "suer", "password"));
        } catch (MalformedURLException ex) {
            throw new RuntimeException("Failed to build test URL", ex);
        }
    }

    private static def createProjectWithTestMetadata(){
        return new Project(new SourceVersion(new URL("https://bitbucket.org/squashtest/testExample"),"master"), [createTestExecutionWithMetadata()], "titi/execute@v1", new Credentials(CredentialsType.BASIC_AUTH, "suer", "password"));
    }

    private static def createTestExecutionWithMetadata(){
        TestExecution testExecution = new TestExecution("CategoryN/thirdSuite.robot#testCase1", Collections.emptyMap(), "testId");
        testExecution.addMetadata("importance", "MEDIUM");
        testExecution.addMetadata("name", "testTestCase");
        testExecution.addMetadata("nature", "BUSINESS");
        testExecution.addMetadata("technology", "RobotFramework");
        testExecution.addMetadata("path", ["path", "to", "test"]);
        return testExecution
    }


    private static def createProjectWithParms (String projectUrl, TestExecution... testExecs) {
        try {
            List<TestExecution> tests=
                    Arrays.asList(testExecs);
            return new Project(new SourceVersion(new URL(projectUrl),"master"), tests, "toto/execute@1", new Credentials(CredentialsType.BASIC_AUTH, "suer", "password"))
        } catch (MalformedURLException ex) {
            throw new RuntimeException("Failed to build test URL", ex);
        }
    }

    private static def createExecution (String testRef, String cufValue) {
        final HashMap<String, String> testParmsMap = new HashMap<>();
        testParmsMap.put("TC_CUF_1", cufValue);
        return new TestExecution(testRef, testParmsMap, PLACEHOLDER_TEST_ID);
    }

    private static createSampleProjectWithEnvironmentTags(List<String> environmentTags, String projectCategory) {
        final HashMap<String, String> environmentVariableMap = new HashMap<>();
        environmentVariableMap.put("variableName", "value");
        return new Project(
                new SourceVersion(new URL("http://monUrl.org"), "main"),
                new Credentials(CredentialsType.BASIC_AUTH, "user", "password"),
                projectCategory,
                [],
                environmentTags,
                environmentVariableMap)
    }

    private static createExecutionConfigurations() {
        return [
            new ExecutionConfiguration(1, Collections.emptyList(), Collections.emptyMap(), ""),
            new ExecutionConfiguration(2, Collections.emptyList(), Collections.emptyMap(), ""),
        ]
    }
}
