/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd

import org.springframework.context.MessageSource
import org.squashtest.tm.domain.bdd.ActionWord
import org.squashtest.tm.domain.bdd.ActionWordParameter
import org.squashtest.tm.domain.bdd.ActionWordParameterValue
import org.squashtest.tm.domain.bdd.ActionWordText
import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.project.AutomationWorkflowType
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.DatasetParamValue
import org.squashtest.tm.domain.testcase.KeywordTestCase
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.domain.testcase.Parameter
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest
import org.squashtest.tm.service.internal.testcase.bdd.robot.ActionWordParameterMock
import spock.lang.Specification
import spock.lang.Unroll

import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuildersTestUtils.createBasicActionWord

/**
 * Note : the tests on the whole script output are expensive to maintain. Their purpose is to check the overall structure
 * of a Robot file generated from Squash TM. More specific and detailed tests about sections of the generated script
 * should go in their respective section builder tests (e.g. SettingSectionBuilderTest).
 */
class RobotScriptWriterTest extends Specification {

	def robotScriptWriter = new RobotScriptWriter()

	Project project = new Project()

	KeywordTestCase keywordTestCase

	MessageSource messageSource = Mock()

	def "setup"() {
		keywordTestCase = Spy(KeywordTestCase)
		keywordTestCase.getId() >> 341L
		keywordTestCase.notifyAssociatedWithProject(project)

		keywordTestCase.automationRequest = new AutomationRequest()
		keywordTestCase.automationRequest.automationPriority = 42

		project.setAutomationWorkflowType(AutomationWorkflowType.NATIVE)

		def locale = project.getBddScriptLanguage().getLocale()
		messageSource.getMessage("testcase.bdd.script.label.test-case-importance", null, locale) >> "Test case importance"
		messageSource.getMessage("testcase.bdd.script.label.automation-priority", null, locale) >> "Automation priority"
		messageSource.getMessage("test-case.importance.LOW", null, locale) >> "Low"
		messageSource.getMessage("label.id", null, locale) >> "ID"
		messageSource.getMessage("test-case.reference.label", null, locale) >> "Reference"
	}

	/* ----- Test Case Script ----- */

	def "with minimal test case"() {
		given:
		keywordTestCase.setName("Minimal test")
		when:
		String result = robotScriptWriter.writeBddScript(keywordTestCase, messageSource, true)
		then:
		result ==
			"""*** Settings ***
Documentation    Minimal test
Metadata         ID                           341
Metadata         Automation priority          42
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Minimal test
    [Documentation]    Minimal test


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}

	/* ----- Test Step Script Generation ----- */

	def "step script with no parameters"() {
		given:
			KeywordTestStep step = new KeywordTestStep(
				Keyword.GIVEN,
				createBasicActionWord("Today is Monday"))
		when:
			String result = robotScriptWriter.writeBddStepScript(step, 0, 0)
		then:
			result == "Given Today is Monday"

	}

	def "step script with a parameter value as free text"() {
		given:
			def fragment1 = new ActionWordText("It is ")
			def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
			def value = new ActionWordParameterValue("10 o'clock")
			value.setActionWordParam(fragment2)
			ActionWord actionWord = new ActionWord([fragment1, fragment2] as List)

			KeywordTestStep step = new KeywordTestStep(Keyword.WHEN, actionWord)
			List<ActionWordParameterValue> paramValues = [value]
			step.setParamValues(paramValues)
		when:
			String result = robotScriptWriter.writeBddStepScript(step, 0, 0)
		then:
			result == "When It is \"10 o'clock\""
	}

	def "step script with two side by side parameters valued as free text"() {
		given:
			def fragment1 = new ActionWordText("I am in ")
			def fragment2 = new ActionWordParameterMock(-1L, "param1", "Paris")
			def value2 = new ActionWordParameterValue("Los Angeles")
			value2.setActionWordParam(fragment2)
			def fragment3 = new ActionWordParameterMock(-2L, "param2", "France")
			def value3 = new ActionWordParameterValue("United States")
			value3.setActionWordParam(fragment3)

			ActionWord actionWord = new ActionWord([fragment1, fragment2, fragment3] as List)

			KeywordTestStep step = new KeywordTestStep(Keyword.GIVEN, actionWord)
			List<ActionWordParameterValue> paramValues = [value2, value3]
			step.setParamValues(paramValues)
		when:
			String result = robotScriptWriter.writeBddStepScript(step, 0, 0)
		then:
			result == "Given I am in \"Los Angeles\"\"United States\""
	}

	@Unroll
	def "step script with a parameter value as a number"() {
		given:
			def fragment1 = new ActionWordText("It is ")
			def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
			def value = new ActionWordParameterValue(number)
			value.setActionWordParam(fragment2)
			ActionWord actionWord = new ActionWord([fragment1, fragment2] as List)

			KeywordTestStep step = new KeywordTestStep(Keyword.WHEN, actionWord)
			List<ActionWordParameterValue> paramValues = [value]
			step.setParamValues(paramValues)
		when:
			String result = robotScriptWriter.writeBddStepScript(step, 0, 0)
		then:
			result == "When It is \"${number}\""
		where:
			number << ["10", "10.5", "10,5", "-10.5", "-10,5"]
	}

	def "step script with several parameters"() {
		given:
			def fragment1 = new ActionWordText("it is ")
			def fragment2 = new ActionWordParameterMock(-1L, "param1", "12")
			def value2 = new ActionWordParameterValue("9")
			value2.setActionWordParam(fragment2)
			def fragment3 = new ActionWordText(" o'clock in ")
			def fragment4 = new ActionWordParameterMock(-2L, "param2", "Paris")
			def value4 = new ActionWordParameterValue("London")
			def fragment5 = new ActionWordText(" with a ")
			def fragment6 = new ActionWordParameterMock(-3L, "param3", "sunny")
			def value6 = new ActionWordParameterValue("<weather>")
			value6.setActionWordParam(fragment6)
			def fragment7 = new ActionWordText(" weather.")
			value4.setActionWordParam(fragment4)

			ActionWord actionWord = new ActionWord(
				[fragment1, fragment2, fragment3, fragment4, fragment5, fragment6, fragment7] as List)

			KeywordTestStep step = new KeywordTestStep(Keyword.THEN, actionWord)
			List<ActionWordParameterValue> paramValues = [value2, value4, value6]
			step.setParamValues(paramValues)
		when:
			String result = robotScriptWriter.writeBddStepScript(step, 0, 0)
		then:
			result == "Then it is \"9\" o'clock in \"London\" with a \"\${dataset}[weather]\" weather."
	}

	def "step script using a datatable"() {
		given:
			def fragment1 = new ActionWordText("the following users are listed")
			ActionWord actionWord = new ActionWord([fragment1] as List)
			KeywordTestStep step = new KeywordTestStep(Keyword.THEN, actionWord)
			step.setDatatable("| user1 | user1@mail.com |\n| user2 | user2@mail.com |")
		when:
			String result = robotScriptWriter.writeBddStepScript(step, 2, 0)
		then:
			result == "Then the following users are listed \"\${datatables}[datatable_2]\""
	}

	def "step script using a docstring"() {
		given:
			def fragment1 = new ActionWordText("the following letter is displayed")
			ActionWord actionWord = new ActionWord([fragment1] as List)
			KeywordTestStep step = new KeywordTestStep(Keyword.THEN, actionWord)
			step.setDocstring("\tDear Santa,\n I want a lot of present this year.")
		when:
			String result = robotScriptWriter.writeBddStepScript(step, 0, 2)
		then:
			result == "Then the following letter is displayed \"\${docstrings}[docstring_2]\""
	}
}
