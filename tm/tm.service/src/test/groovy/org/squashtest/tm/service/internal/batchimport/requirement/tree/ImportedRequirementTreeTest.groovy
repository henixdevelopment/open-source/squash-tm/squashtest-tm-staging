/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.tree

import org.squashtest.tm.service.internal.batchimport.Existence
import org.squashtest.tm.service.internal.batchimport.TargetStatus
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget
import org.squashtest.tm.service.internal.batchimport.requirement.dto.ImportRequirementFinder
import org.squashtest.tm.service.internal.repository.RequirementImportDao
import spock.lang.Specification

class ImportedRequirementTreeTest extends Specification {

	ImportedRequirementTree tree

    ImportRequirementFinder finder

    RequirementImportDao dao = Mock()

	def setup() {
		tree = new ImportedRequirementTree()
	}

	def setupSpec() {
		String.metaClass.toReqTarget = {
			new RequirementTarget(delegate)
		}
	}

	def "should insert a requirement and all its hierarchy in the model"(){

		given :
		RequirementTarget target = "/project/home/me/requirement".toReqTarget()

        def targets = [target] as Set

        finder = new ImportRequirementFinder(dao, targets)

		when :
		tree.addNodes(targets, finder,"project", -1L, )

		then :
        tree.nodes.size() == 4

        ImportedRequirementNode node1 = tree.getNode("project".toReqTarget())
		ImportedRequirementNode node2 = tree.getNode("/project/home".toReqTarget())
		ImportedRequirementNode node3 = tree.getNode("/project/home/me".toReqTarget())
		ImportedRequirementNode node4 = tree.getNode("/project/home/me/requirement".toReqTarget())

        node1.isLibrary()
		checkConnections node1, null, [node2]

        node2.isRequirementFolder()
		checkConnections node2, node1, [node3]

        node3.isRequirementFolder()
		checkConnections node3, node2, [node4]

        node4.isRequirement()
		checkConnections node4, node3, []

	}


    def "should insert a requirement only in the model"(){

        given :
        RequirementTarget target = "/project/home/me/requirement".toReqTarget()

        def targets = [target] as Set

        finder = new ImportRequirementFinder(dao, targets)

        when :
        tree.addNodes(targets, finder)

        then :
        tree.nodes.size() == 1

        ImportedRequirementNode node = tree.getNode("/project/home/me/requirement".toReqTarget())
        node != null

    }


def "should insert two requirements that totally different ancestry"(){

		given :
		RequirementTarget target1 = "/project/home/requirement".toReqTarget()
		RequirementTarget target2 = "/anotherproject/home/requirement".toReqTarget()

        finder = new ImportRequirementFinder(dao, [target1, target2] as Set)

        def targets = [target1] as Set
        def targets2 = [target2] as Set

		when :
		tree.addNodes(targets, finder, "project", -1L)
		tree.addNodes(targets2, finder, "anotherproject", -2L)

		then :
		tree.nodes.size() == 6

		def node1 = tree.getNode("project".toReqTarget())
		def node2 = tree.getNode("/project/home".toReqTarget())
		def node3 = tree.getNode("/project/home/requirement".toReqTarget())
		def node4 = tree.getNode("anotherproject".toReqTarget())
		def node5 = tree.getNode("/anotherproject/home".toReqTarget())
		def node6 = tree.getNode("/anotherproject/home/requirement".toReqTarget())


		checkConnections(node1, null, [node2])
		checkConnections(node2, node1, [node3])
		checkConnections(node3, node2, [])
		checkConnections(node4, null, [node5])
		checkConnections node5, node4, [node6]
		checkConnections node6, node5, []

	}


	def "should attach a subhierarchy of a requirement to an existing branch of the tree"(){

		given :
		RequirementTarget target1 = "/project/home/me/requirement".toReqTarget()
		RequirementTarget target2 = "/project/home/somewhere/deep/anotherrequirement".toReqTarget()

		and :
        def targets = [target1, target2] as Set

        finder = new ImportRequirementFinder(dao, targets)

		when :
		tree.addNodes(targets, finder,"project", -1L, )

		then :

		tree.nodes.size() == 7

		def node1 = tree.getNode("project".toReqTarget())
		def node2 = tree.getNode("/project/home".toReqTarget())
		def node3 = tree.getNode("/project/home/me".toReqTarget())
		def node4 = tree.getNode("/project/home/me/requirement".toReqTarget())
		def node5 = tree.getNode("/project/home/somewhere".toReqTarget())
		def node6 = tree.getNode("/project/home/somewhere/deep".toReqTarget())
		def node7 = tree.getNode("/project/home/somewhere/deep/anotherrequirement".toReqTarget())

		checkConnections node1, null, [node2]
		checkConnections node2, node1, [node3, node5]
		checkConnections node3, node2, [node4]
		checkConnections node4, node3, []
		checkConnections node5, node2, [node6]
		checkConnections node6, node5, [node7]
		checkConnections node7, node6, []

	}


	def "should create a requirement hierarchy nested under an existing requirement"(){

		given :
		RequirementTarget target1 = "/project/home/requirement".toReqTarget()
		RequirementTarget target2 = "/project/home/requirement/nestedrequirement/subrequirement".toReqTarget()

        and :
        def targets = [target1, target2] as Set

        finder = new ImportRequirementFinder(dao, targets)

        when :
        tree.addNodes(targets, finder, "project", -1L)

		then :
		def nested = tree.getNode("/project/home/requirement/nestedrequirement".toReqTarget())
		def sub = tree.getNode("/project/home/requirement/nestedrequirement/subrequirement".toReqTarget())

        nested.isRequirement()
        sub.isRequirement()

	}

	def "should update an existing node"(){

		given :
		RequirementTarget target1 = "/project/home/requirement".toReqTarget()

        def targets = [target1] as Set

        finder = new ImportRequirementFinder(dao, targets)

		RequirementTarget target2 = "/project/home/requirement".toReqTarget()
		TargetStatus status2 = new TargetStatus(Existence.EXISTS, 5l)

		and :
		tree.addNodes(targets, finder, "project", -1L)

		when :
		tree.updateStatus(target2, status2)

		then :

		def node = tree.getNode("/project/home/requirement".toReqTarget())
		node.getStatus().getStatus() == Existence.EXISTS

	}

	def "a node that was though to be a folder, happens to be a requirement. All its children become requirements."(){

		given :
		RequirementTarget deepTarget = "/project/home/requirement/nested/deep".toReqTarget()
		RequirementTarget middleTarget = "/project/home/requirement".toReqTarget()

        and :
        def targets = [deepTarget, middleTarget] as Set
        finder = new ImportRequirementFinder(dao, targets)


		when :
		tree.addNodes(targets, finder, "project", -1L, )

		then :
		def nested = tree.getNode("/project/home/requirement/nested".toReqTarget())
        nested.isRequirement()
	}


    boolean checkConnections(ImportedRequirementNode node, ImportedRequirementNode parent, List<ImportedRequirementNode> children) {
        def parentTrue = node.parent == parent
        def childTrue = node.getContents().containsAll(children)

        return parentTrue && childTrue
    }

}
