/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher

import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStep
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseKind
import org.squashtest.tm.domain.testcase.TestStep
import org.squashtest.tm.service.execution.automatedexecution.AutomatedExecutionFailureDetailService
import org.squashtest.tm.service.internal.dto.AutomatedExecutionUpdateData
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao
import org.squashtest.tm.service.internal.repository.ExecutionDao
import org.squashtest.tm.service.plugin.PluginFinderService
import org.squashtest.tm.service.testautomation.AutomatedExecutionManagerService
import org.squashtest.tm.service.testautomation.model.Attachment
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState
import org.squashtest.tm.service.testautomation.model.TfTestExecutionStatus
import spock.lang.Specification
import spock.lang.Unroll

import javax.persistence.EntityManager

class ExecutionServiceTest extends Specification {

    ExecutionService service = new ExecutionService()

    EntityManager entityManager = Mock(EntityManager)
    ExecutionDao executionDao = Mock(ExecutionDao)
    AutomatedSuitePublisherService suiteManagerService = Mock(AutomatedSuitePublisherService)
    AutomatedExecutionManagerService automatedExecutionManager= Mock(AutomatedExecutionManagerService)
    AutomatedExecutionFailureDetailService failureDetailService = Mock(AutomatedExecutionFailureDetailService)
    AutomatedExecutionExtenderDao automatedExecutionExtenderDao = Mock(AutomatedExecutionExtenderDao)
    PluginFinderService pluginFinderService = Mock(PluginFinderService);

    def "setup"() {
        service.entityManager = entityManager
        service.executionDao = executionDao
        service.automatedSuitePublisherService = suiteManagerService
        service.automatedExecutionManager = automatedExecutionManager
        service.failureDetailService = failureDetailService
        service.automatedExecutionExtenderDao = automatedExecutionExtenderDao
        service.pluginFinderService = pluginFinderService
    }

    @Unroll
    def "should throw when no execution data"() {
        given:
        executionDao.findAutomatedExecutionUpdateData(_, _) >> null

        when:
        service.updateExecutionAndClearSession(1L, "4028800e8d7dc047018d7dc7dbff0002", null)

        then:
        thrown(NoSuchElementException)

    }

    def "should update execution"() {
        given:
        def attachment = Mock(Attachment)
        List<Attachment> attachments = List.of(attachment)

        AutomatedExecutionUpdateData data = new AutomatedExecutionUpdateData(1L, 2L, 3L)

        and:
        def status = Mock(TfTestExecutionStatus) {
            tfTestExecutionStatusToTmTestExecutionStatus() >> Mock(TestExecutionStatus)
        }

        and:
        def executionStep = Mock(ExecutionStep) {
            getReferencedTestStep() >> Mock(TestStep) {
                getTestCase() >> Mock(TestCase) {
                    getKind() >> TestCaseKind.GHERKIN
                }
            }
        }
        List<ExecutionStep> steps = List.of(executionStep)

        ExecutionStatus stepStatus = ExecutionStatus.SUCCESS
        def TfTestExecutionStatus = Mock(TfTestExecutionStatus) {
            getStatus() >> stepStatus
        }

        Map<Integer, TfTestExecutionStatus> testStepStatuses = Map.of(1, TfTestExecutionStatus)

        and:
        def stateChange = Mock(AutomatedExecutionState) {
            getAttachments() >> attachments
            getTfTestExecutionStatus() >> status
            getTestStepExecutionStatuses() >> testStepStatuses
        }

        and:
        def execution = new Execution()
        def extender = new AutomatedExecutionExtender()
        execution.testPlan = new IterationTestPlanItem()
        extender.setExecution(execution)
        automatedExecutionExtenderDao.findById(_) >> Optional.of(extender)

        when:
        service.updateExecutionAndClearSession(1L, "4028800e8d7dc047018d7dc7dbff0002", stateChange)

        then:
        1 * executionDao.findAutomatedExecutionUpdateData(_, _) >> data
        1 * suiteManagerService.updateAttachments(_,_,_)
        1 * executionDao.findSteps(_) >> steps
        1 * automatedExecutionManager.changeExecutionState(_, _)
        steps.get(0).getExecutionStatus() >> stepStatus
    }
}
