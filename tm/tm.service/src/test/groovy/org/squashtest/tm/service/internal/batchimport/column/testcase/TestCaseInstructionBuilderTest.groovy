/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.column.testcase

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.squashtest.tm.core.foundation.lang.DateUtils
import org.squashtest.tm.domain.infolist.ListItemReference
import org.squashtest.tm.domain.testcase.TestCaseImportance
import org.squashtest.tm.domain.testcase.TestCaseStatus
import org.squashtest.tm.service.importer.ImportMode
import org.squashtest.tm.service.importer.ImportStatus
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction
import org.squashtest.tm.service.internal.batchimport.column.CustomFieldColumnDef
import org.squashtest.tm.service.internal.batchimport.column.StdColumnDef
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet
import org.squashtest.tm.service.internal.batchimport.column.WorksheetDef
import org.squashtest.tm.service.internal.batchimport.excel.CannotCoerceException
import spock.lang.Specification
import spock.lang.Unroll


/**
 * @author Gregory Fouquet
 *
 */
class TestCaseInstructionBuilderTest extends Specification {
	WorksheetDef wd = Mock()
	Row row = Mock()
	TestCaseInstructionBuilder builder
	Cell pathCell = Mock()
	Cell orderCell = Mock()

	def setup() {
		wd.getWorksheetType() >> TemplateWorksheet.TEST_CASES_SHEET
		builder = new TestCaseInstructionBuilder(wd)
	}

	private setupTestCaseTargetSpec() {
		wd.getImportableColumnDefs() >> [
            new StdColumnDef(TestCaseSheetColumn.TC_PATH, 10),
            new StdColumnDef(TestCaseSheetColumn.TC_NUM, 20)
		]

		wd.getCustomFieldDefs() >> []

		row.getCell(10, _) >> pathCell
		row.getCell(20, _) >> orderCell
	}

	@Unroll
	def "should create target from row with path #path and order #order"() {
		given:
		setupTestCaseTargetSpec()

		and:
		pathCell.getCellType() >> CellType.STRING
		pathCell.getStringCellValue() >> path
		orderCell.getNumericCellValue() >> order
		orderCell.getStringCellValue() >> order
		orderCell.getCellType() >> cellType

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		instruction.target.path == path
		instruction.target.order == intOrder

		where:
		path 	| order 		| intOrder 	| cellType
		"foo" 	| 30.0 			| 29       	| CellType.NUMERIC
		"foo" 	| 29.9999996	| 29 		| CellType.NUMERIC
		"foo" 	| 75.359 		| 74     	| CellType.NUMERIC
		"foo" 	| "30" 			| 29       	| CellType.STRING
		"foo" 	| null 			| null     	| CellType.BLANK
	}


	@Unroll
	def "should log fatal error on blank path from cell type #cellType"() {
		given:
		setupTestCaseTargetSpec()

		and:
		pathCell.getCellType() >> cellType
		orderCell.getCellType() >> CellType.NUMERIC

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		instruction.logTrain.hasCriticalErrors()

		where:
		cellType << [CellType.BLANK, CellType.ERROR, CellType.FORMULA, CellType.STRING]
	}

	def "not sure what we should do when order not a number"() {
		given:
		setupTestCaseTargetSpec()

		and:
		pathCell.getStringCellValue() >> "foo"
		pathCell.getCellType() >> CellType.STRING
		orderCell.getNumericCellValue() >> { throw new RuntimeException("not a number, lol") }
		orderCell.getStringCellValue() >> "not a number, lol"
		orderCell.getCellType() >> CellType.STRING

		when:
		TestCaseInstruction target = builder.build(row)

		then: "no exceptions raised not to break processing"
		notThrown(CannotCoerceException)
		target != null
	}

	@Unroll
	def "should create test case from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		instruction.testCase[propName] == propValue

		where:
		col				                    | cellType					| cellValue			| propName			| propValue
        TestCaseSheetColumn.TC_REFERENCE	| CellType.STRING		    | "yeah"			| "reference"		| "yeah"
        TestCaseSheetColumn.TC_REFERENCE	| CellType.BLANK		    | ""				| "reference"		| null

        TestCaseSheetColumn.TC_NAME			| CellType.STRING		    | "yeah"			| "name"			| "yeah"
        TestCaseSheetColumn.TC_NAME			| CellType.STRING		    | ""				| "name"			| ""
        TestCaseSheetColumn.TC_NAME		 	| CellType.BLANK		    | ""				| "name"			| null

        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.NUMERIC	        | 1					| "importanceAuto"	| true
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.BOOLEAN	        | true				| "importanceAuto"	| true
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.STRING		    | "1"				| "importanceAuto"	| true
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.NUMERIC	        | 0					| "importanceAuto"	| false
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.BOOLEAN	        | false				| "importanceAuto"	| false
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.STRING		    | "0"				| "importanceAuto"	| false
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.BLANK		    | ""				| "importanceAuto"	| null

        TestCaseSheetColumn.TC_WEIGHT		| CellType.STRING		    | "VERY_HIGH"		| "importance"		| TestCaseImportance.VERY_HIGH
        TestCaseSheetColumn.TC_WEIGHT		| CellType.BLANK		    | ""				| "importance"		| null

        TestCaseSheetColumn.TC_STATUS		| CellType.STRING		    | "APPROVED"		| "status"			| TestCaseStatus.APPROVED
        TestCaseSheetColumn.TC_STATUS		| CellType.BLANK		    | ""				| "status"			| null

        TestCaseSheetColumn.TC_DESCRIPTION	| CellType.STRING		    | "yeah"			| "description"		| "yeah"
        TestCaseSheetColumn.TC_DESCRIPTION	| CellType.BLANK		    | ""				| "description"		| null

        TestCaseSheetColumn.TC_PRE_REQUISITE| CellType.STRING		    | "yeah"			| "prerequisite"	| "yeah"
        TestCaseSheetColumn.TC_PRE_REQUISITE| CellType.BLANK		    | ""				| "prerequisite"	| null

        TestCaseSheetColumn.TC_CREATED_ON	| CellType.STRING		    | "2010-01-12"		| "createdOn"		| DateUtils.parseIso8601Date("2010-01-12")
        TestCaseSheetColumn.TC_CREATED_ON	| CellType.NUMERIC	        | DateUtils.parseIso8601Date("2019-03-17") | "createdOn"		| DateUtils.parseIso8601Date("2019-03-17")
        TestCaseSheetColumn.TC_CREATED_ON	| CellType.BLANK		    | ""				| "createdOn"		| null

        TestCaseSheetColumn.TC_CREATED_BY	| CellType.STRING		    | "your mom"		| "createdBy"		| "your mom"
        TestCaseSheetColumn.TC_CREATED_BY	| CellType.BLANK		    | ""				| "createdBy"		| null
	}

	@Unroll
	def "should create test case from row with this bunch of data (info lists) : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		propValue == null || propValue.references(instruction.testCase[propName])

		where:
		col				                    | cellType					| cellValue			| propName			| propValue

        TestCaseSheetColumn.TC_NATURE		| CellType.STRING		    | "USER_TESTING"	| "nature"			| new ListItemReference("NAT_USER_TESTING")
        TestCaseSheetColumn.TC_NATURE		| CellType.BLANK		    | ""				| "nature"			| null

        TestCaseSheetColumn.TC_TYPE			| CellType.STRING		    | "PARTNER_TESTING"	| "type"			| new ListItemReference("TYP_PARTNER_TESTING")
        TestCaseSheetColumn.TC_TYPE			| CellType.BLANK		    | ""				| "type"			| null


	}


	private Cell mockCell(cellType, cellValue) {
		Cell cell = Mock()

		cell.getCellType() >> cellType

		cell.getNumericCellValue() >> cellValue
		cell.getStringCellValue() >> cellValue
		cell.getBooleanCellValue() >> cellValue
		cell.getDateCellValue() >> cellValue

		return cell
	}

	@Unroll
	def "should create instruction from row with this bunch of data : #col #cellType #cellValue #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		instruction.mode == propValue

		where:
		col				                    | cellType					| cellValue			| propValue
        TestCaseSheetColumn.ACTION			| CellType.STRING		    | "CREATE"			| ImportMode.CREATE
        TestCaseSheetColumn.ACTION			| CellType.STRING		    | "C"				| ImportMode.CREATE
        TestCaseSheetColumn.ACTION			| CellType.BLANK		    | ""				| ImportMode.UPDATE
	}

	@Unroll
	def "should log error on illegal action value : #col #cellType #cellValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		instruction.logTrain.entries[0].status == ImportStatus.WARNING
		instruction.mode == propValue

		where:
		col				                    | cellType					| cellValue		| propValue
        TestCaseSheetColumn.ACTION			| CellType.STRING		    | "PROBLEM?"	| ImportMode.getDefault()
        TestCaseSheetColumn.ACTION			| CellType.FORMULA	        | null			| ImportMode.getDefault()
	}

	@Unroll
	def "should add custom field to instruction from row with this bunch of data : #cellType #cellValue #fieldCode"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> []
		wd.getCustomFieldDefs() >> [
			new CustomFieldColumnDef(fieldCode, 30)
		]

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		instruction.customFields[fieldCode] == cellValue

		where:
		cellType			 	| fieldCode	| cellValue
		CellType.STRING	        | "FOO"		| "bar"
		CellType.BLANK	        | "FOO"		| null
	}

	def "should set the row number on the instruction"() {
		given:
		wd.getImportableColumnDefs() >> []
		wd.getCustomFieldDefs() >> []

		and:
		row.getRowNum() >> 150

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		instruction.line == 151
	}

	@Unroll
	def "should log warning for broken data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		TestCaseInstruction instruction = builder.build(row)

		then:
		!instruction.logTrain.hasCriticalErrors()
		instruction.logTrain.entries.size() == 1
		instruction.testCase[propName] == propValue

		where:
		col				                    | cellType					| cellValue			| propName			| propValue
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.NUMERIC	        | 2					| "importanceAuto"	| null
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.STRING		    | "PROBLEM?"		| "importanceAuto"	| null
        TestCaseSheetColumn.TC_WEIGHT_AUTO	| CellType.FORMULA	        | ""				| "importanceAuto"	| null

        TestCaseSheetColumn.TC_WEIGHT		| CellType.STRING		    | "PROBLEM?"		| "importance"		| null
        TestCaseSheetColumn.TC_WEIGHT		| CellType.NUMERIC	        | 1					| "importance"		| null

        TestCaseSheetColumn.TC_STATUS		| CellType.STRING		    | "PROBLEM?"		| "nature"			| null
        TestCaseSheetColumn.TC_STATUS		| CellType.NUMERIC	        | 1					| "nature"			| null
	}

}
