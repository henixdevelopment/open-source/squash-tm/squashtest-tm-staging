/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker

import org.squashtest.tm.domain.bugtracker.BugTracker
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation
import org.squashtest.tm.exception.bugtracker.CannotDeleteBugtrackerLinkedToSynchronisationException
import org.squashtest.tm.service.internal.repository.BugTrackerDao
import org.squashtest.tm.service.internal.repository.RemoteSynchronisationDao
import spock.lang.Specification

class BugTrackerManagerServiceImplTest extends Specification  {

    BugTrackerManagerServiceImpl service = new BugTrackerManagerServiceImpl()
    BugTrackerDao bugTrackerDao = Mock()
    RemoteSynchronisationDao remoteSynchronisationDao = Mock()


    def setup(){
        service.bugTrackerDao = bugTrackerDao
        service.remoteSynchronisationDao = remoteSynchronisationDao
    }

    def "should not delete bugtracker if there are remote synchronisations linked to it"(){
        given :

        BugTracker bugTracker = Mock()
        bugTracker.getId() >> 52L
        bugTracker.getName() >> "azureBT"

        Project project = Mock()
        project.getName() >> "project1"

        def bugtrackerIds = [52L]
        bugTrackerDao.findById(_) >> Optional.of(bugTracker)

        RemoteSynchronisation remoteSynchro = Mock()
        remoteSynchro.getName() >> "remoteSynchro1"
        remoteSynchro.getServer() >> bugTracker
        remoteSynchro.getProject() >> project

        remoteSynchronisationDao.findByServers(_) >> [remoteSynchro]

        when:
        service.deleteBugTrackers(bugtrackerIds)

        then:
        thrown(CannotDeleteBugtrackerLinkedToSynchronisationException)
    }
}
