/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion

import org.squashtest.tm.domain.NamedReference
import org.squashtest.tm.domain.library.structures.LibraryGraph
import org.squashtest.tm.domain.milestone.Milestone
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.deletion.*
import org.squashtest.tm.service.internal.campaign.LockedTestCaseNodeDetectionService
import org.squashtest.tm.service.internal.repository.TestCaseDao
import org.squashtest.tm.service.internal.repository.TestCaseDeletionDao
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import spock.lang.Specification

class LockedTestCaseNodeDetectionServiceImplTest extends Specification {
    LockedTestCaseNodeDetectionService handler
    TestCaseDeletionDao deletionDao = Mock()
    TestCaseDao testCaseDao = Mock();
    TestCaseCallTreeFinder calltreeFinder = Mock()
    ActiveMilestoneHolder activeMilestoneHolder = Mock()

	def setup() {
        handler = new LockedTestCaseNodeDetectionServiceImpl(
            activeMilestoneHolder,
            deletionDao,
            testCaseDao,
            calltreeFinder
        )
	}

    def "should detect locked test case by executions associated in the simulation"() {
        given:
        def ids = [1L, 2L, 3L]
        testCaseDao.findAllLinkedToIteration(ids) >> [createTestCase(1L)]

        when:
        def reportList = handler.detectLockedByExecutedTestCases(ids)

        then:
        reportList.size() == 1
        reportList[0] instanceof LinkedToIterationPreviewReport
        LinkedToIterationPreviewReport report = reportList[0] as LinkedToIterationPreviewReport
        report.getLockedNodes().size() == 0
    }

    def "should detect locked nodes by milestone"() {
        given:
        def ids = [1L, 2L]
        deletionDao.findTestCasesWhichMilestonesForbidsDeletion(ids) >> [1L, 2L]

        when:
        def result = handler.detectLockedByMilestone(ids)

        then:
        result.size() == 1
        result[0] instanceof BoundToLockedMilestonesReport
        BoundToLockedMilestonesReport report = result[0] as BoundToLockedMilestonesReport
        report.getLockedNodes() == [1L, 2L] as Set
    }

    def "should not detect locked nodes by milestone"() {
        given:
        def ids = [1L, 2L]
        deletionDao.findTestCasesWhichMilestonesForbidsDeletion(ids) >> []

        when:
        def result = handler.detectLockedByMilestone(ids)

        then:
        result.size() == 0
    }

    def "should detect locked nodes with active milestone -- SingleOrMultiple"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.of(createMilestone(1L))
        deletionDao.separateFolderFromTestCaseIds(ids) >> [[1L], [2L, 3L, 4L]]
        testCaseDao.findNodeIdsHavingMultipleMilestones([2L, 3L, 4L]) >> [2L]
        testCaseDao.findNonBoundTestCases([2L, 3L, 4L], 1L) >> [3L, 4L]

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 3
        result[0] instanceof MilestoneModeNoFolderDeletion
        MilestoneModeNoFolderDeletion reportFolder = result[0] as MilestoneModeNoFolderDeletion
        reportFolder.getLockedNodes() == [1L] as Set

        and:
        result[1] instanceof SingleOrMultipleMilestonesReport
        SingleOrMultipleMilestonesReport reportBinding = result[1] as SingleOrMultipleMilestonesReport
        reportBinding.getLockedNodes() == [2L] as Set

        and:
        result[2] instanceof NotBoundToActiveMilestonesReport
        NotBoundToActiveMilestonesReport reportNotBound = result[2] as NotBoundToActiveMilestonesReport
        reportNotBound.getLockedNodes() == [3L, 4L] as Set
    }

    def "should detect locked nodes with active milestone -- BoundToMultiple"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.of(createMilestone(1L))
        deletionDao.separateFolderFromTestCaseIds(ids) >> [[1L], [2L, 3L, 4L]]
        testCaseDao.findNodeIdsHavingMultipleMilestones([2L, 3L, 4L]) >> [2L, 3L, 4L]
        testCaseDao.findNonBoundTestCases([2L, 3L, 4L], 1L) >> []

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 2
        result[0] instanceof MilestoneModeNoFolderDeletion
        MilestoneModeNoFolderDeletion reportFolder = result[0] as MilestoneModeNoFolderDeletion
        reportFolder.getLockedNodes() == [1L] as Set

        and:
        result[1] instanceof BoundToMultipleMilestonesReport
        BoundToMultipleMilestonesReport reportBinding = result[1] as BoundToMultipleMilestonesReport
        reportBinding.getLockedNodes() == [2L, 3L, 4L] as Set
    }

    def "should not detect locked nodes with no active milestone"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.empty()
        deletionDao.separateFolderFromTestCaseIds(ids) >> [[1L], [2L, 3L, 4L]]
        testCaseDao.findNodeIdsHavingMultipleMilestones([2L, 3L, 4L]) >> [2L, 3L, 4L]

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 0
    }

    def "should detect locked test case by call steps 1"(){
        given :
        calltreeFinder.getCallerGraph(_) >> testGraph1()

        when :
        def reportList = handler.detectLockedByCallSteps([21l, 22l, 23l, 24l, 25l], [])

        then :
        reportList.size() == 1
        reportList[0] instanceof NotDeletablePreviewReport
        NotDeletablePreviewReport report = reportList[0] as NotDeletablePreviewReport
        report.nodeNames == ["21", "22", "23", "24"] as Set
        report.why == ["11", "12", "1"] as Set


    }


    def "should detect locked test case by call steps 2"(){
        given :
        calltreeFinder.getCallerGraph(_) >> testGraph2()

        when :
        def reportList = handler.detectLockedByCallSteps([11l, 12l], [])

        then :
        reportList.size() == 1;
        reportList[0] instanceof NotDeletablePreviewReport
        NotDeletablePreviewReport report = reportList[0] as NotDeletablePreviewReport
        report.nodeNames == ["11", "12"] as Set
        report.why == ["1"] as Set
    }


    def "should not detect locked test case by call steps"(){
        given :
        calltreeFinder.getCallerGraph(_) >> testGraph2()

        when :
        def reportList = handler.detectLockedByCallSteps([11l, 12l, 1l], [])

        then :
        reportList.size() == 0
    }


    def createMilestone(Long id) {
        Milestone milestone = Mock(Milestone)
        milestone.getId() >> id
        return milestone
    }

    def createTestCase(id) {
        TestCase tc = Mock(TestCase)
        tc.getId() >> id
        tc.getName() >> "tc"+id
        return tc
    }

    NamedReference ref(id){
        return new NamedReference(id, id?.toString())
    }

    LibraryGraph.SimpleNode node(id){
        return (id != null) ? new LibraryGraph.SimpleNode(ref(id)) : null
    }



    LibraryGraph testGraph1(){

        def layer0 = [ [ null, 1l ] ]
        def layer1 = [ [ 1l, 11l ], [ 1l, 12l ]  ]
        def layer2 = [
            [ 11l, 21l ], [ 11l,  22l ], [ 11l, 23l ],
            [ 12l, 22l ], [ 12l,  23l ], [ 12l, 24l ],
            [ 23l, 24l ],
            [ null, 25l ]
        ]

        def allData = layer0 + layer1 + layer2


        LibraryGraph g = new LibraryGraph()
        allData.each{ g.addEdge(node(it[0]), node(it[1])) }

        return g
    }

    LibraryGraph testGraph2(){

        def layer0 = [ [ null, 1l ] ]
        def layer1 = [ [ 1l, 11l ], [ 1l, 12l ]  ]

        def allData = layer0 + layer1


        LibraryGraph g = new LibraryGraph()
        allData.each{ g.addEdge(node(it[0]), node(it[1])) }

        return g
    }
}
