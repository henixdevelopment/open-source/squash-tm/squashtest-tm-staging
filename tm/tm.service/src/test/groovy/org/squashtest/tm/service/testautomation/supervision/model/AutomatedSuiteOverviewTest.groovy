/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.supervision.model

import org.squashtest.tm.domain.execution.ExecutionStatus
import spock.lang.Specification

class AutomatedSuiteOverviewTest extends Specification {

    def "should create an AutomatedSuiteOverview"() {
        given:
        def tfExecutionView = new TfExecutionView(
            7L,
            "test case 7",
            ExecutionStatus.RUNNING,
            "node_1",
            "jenkins_server")
        def squashAutomExecutionView = new SquashAutomExecutionView(
            7L,
            "test case 7",
            "dataset_1",
            ExecutionStatus.RUNNING,
            "orchestrator")

        when:
        AutomatedSuiteOverview overview = new AutomatedSuiteOverview(
            "22d6d286-3467-4c5d-a487-5cd5f158c053",
            19,
            [tfExecutionView],
            3,
            [squashAutomExecutionView, squashAutomExecutionView])
        overview.setWorkflowsUUIDs(["4f232ab2-8891-49fb-ba85-aff5539c400d"])
        overview.setErrorMessage("error message")

        then:
        overview.getSuiteId() == "22d6d286-3467-4c5d-a487-5cd5f158c053"
        overview.getTfPercentage() == 19
        overview.getTfExecutions() == [tfExecutionView]
        overview.getAutomTerminatedCount() == 3
        overview.getAutomExecutions() == [squashAutomExecutionView, squashAutomExecutionView]
        overview.getWorkflowsUUIDs() == ["4f232ab2-8891-49fb-ba85-aff5539c400d"]
        overview.getErrorMessage() == "error message"

    }
}
