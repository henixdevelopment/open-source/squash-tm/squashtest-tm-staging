/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.requirement


import org.squashtest.tm.exception.requirement.link.CodesAndRolesInconsistentException
import org.squashtest.tm.service.internal.repository.RequirementVersionLinkTypeDao
import org.squashtest.tm.service.internal.requirement.RequirementVersionLinkTypeManagerServiceImpl
import spock.lang.Specification

class RequirementVersionLinkTypeManagerServiceImplTest extends Specification {

    RequirementVersionLinkTypeManagerServiceImpl service = new RequirementVersionLinkTypeManagerServiceImpl()
    RequirementVersionLinkTypeDao linkTypeDao = Mock()

    def setup() {
        service.linkTypeDao = linkTypeDao
        service.doesLinkTypeCodeAlreadyExist(_, _) >> false
    }

    def "should not update requirement link type if roles and role codes are inconsistent"() {
        given:
        RequirementVersionLinkTypeRecord newLinkType = new RequirementVersionLinkTypeRecord("code1", "roleCode", "code2", "roleCode")

        when:
        service.editLinkType(1, newLinkType)

        then:
        thrown CodesAndRolesInconsistentException
    }
}
