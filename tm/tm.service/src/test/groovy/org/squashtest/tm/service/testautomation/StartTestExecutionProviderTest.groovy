/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import org.squashtest.tm.service.internal.library.EntityPathHeaderService
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao
import org.squashtest.tm.service.internal.testautomation.BuildDef
import org.squashtest.tm.service.internal.testautomation.StartTestExecutionProvider
import org.squashtest.tm.service.internal.testautomation.httpclient.BusClientFactory
import org.squashtest.tm.service.internal.testautomation.httpclient.WorkflowClientFactory
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration
import spock.lang.Specification

class StartTestExecutionProviderTest extends Specification {
    def "should do its factory job"() {
        given:
        def provider = new StartTestExecutionProvider()
            BuildDef buildDef = Mock()
            WorkflowClientFactory workflowClientFactory = Mock()
            BusClientFactory busClientFactory = Mock()
            TestPlanContext testPlanContext = Mock()
        EntityPathHeaderDao entityPathHeaderDao = Mock()

        TestAutomationServer server = Mock()
        server.getUrl() >> "http://localhost:8089"
        buildDef.getServer() >> server

        TokenAuthCredentials credentials = Mock()
        credentials.getToken() >> "token"
        buildDef.getCredentials() >> credentials

        when:
        def startTestExecution = provider.create(buildDef,
            [new SquashAutomExecutionConfiguration(7L, ["default"], ["linux", "ssh"], new HashMap<String, String>(),  null)],
            "suiteId",
            testPlanContext,
            "callbackUrl",
            "automatedServerToken",
            workflowClientFactory,
            busClientFactory,
            entityPathHeaderDao,
        true)

        then:
        startTestExecution != null
    }
}
