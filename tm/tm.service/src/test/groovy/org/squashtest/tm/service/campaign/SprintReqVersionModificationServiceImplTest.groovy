/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.squashtest.tm.domain.campaign.SprintReqVersionValidationStatus
import org.squashtest.tm.domain.campaign.SprintStatus
import org.squashtest.tm.exception.campaign.SprintClosedException
import org.squashtest.tm.service.internal.campaign.SprintReqVersionModificationServiceImpl
import spock.lang.Specification

import javax.persistence.EntityManager

class SprintReqVersionModificationServiceImplTest extends Specification {

    SprintManagerService sprintManagerService = Mock()
    EntityManager entityManager = Mock()
    SprintReqVersionModificationServiceImpl sprintReqVersionModificationService =
        new SprintReqVersionModificationServiceImpl(sprintManagerService, entityManager)

    def "should get sprint by id"() {
        given:
        sprintManagerService.getSprintStatusBySprintReqVersionId(_) >> SprintStatus.CLOSED

        when:
        sprintReqVersionModificationService.changeValidationStatus(-1L, SprintReqVersionValidationStatus.IN_PROGRESS)

        then:
        thrown(SprintClosedException)
    }
}
