/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.model

import org.apache.commons.io.IOUtils
import spock.lang.Specification

class AutomatedExecutionStateTest extends Specification {

    def "should construct well"() {
        given:
            TfTestExecutionStatus status = new TfTestExecutionStatus()
            Map<Integer, TfTestExecutionStatus> stepStatuses = Map.of(1, status)
            List<String> tags = List.of("linux, ssh, robotframework")
            Map<String, String> variables = Map.of("browser", "firefox")
            List<Attachment> attachments = List.of(
                    new Attachment("logs", IOUtils.toInputStream("content")))
            String testTechnology = "robotframework"
        when:
            AutomatedExecutionState state = new AutomatedExecutionState(status, stepStatuses, tags, variables, attachments, testTechnology, true)
        then:
            status == state.getTfTestExecutionStatus()
            stepStatuses == state.getTestStepExecutionStatuses()
            tags == state.getEnvironmentTags()
            variables == state.getEnvironmentVariables()
            attachments == state.getAttachments()
            testTechnology == state.getTestTechnology()
    }

    def "should not have null tags or variables"() {
        given:
        TfTestExecutionStatus status = new TfTestExecutionStatus()
        List<Attachment> attachments = List.of(
                new Attachment("logs", IOUtils.toInputStream("content")))
        String testTechnology = "robotframework"

        when:
            AutomatedExecutionState state = new AutomatedExecutionState(status,Map.of(), null, null, attachments, testTechnology, true)
        then:
            [] == state.getEnvironmentTags()
            [:] == state.getEnvironmentVariables()
    }
}

