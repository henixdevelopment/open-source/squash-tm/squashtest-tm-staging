/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.artificialintelligence.testcasegeneration

import org.squashtest.tm.service.internal.repository.AiTestCaseGenerationDao
import org.squashtest.tm.service.internal.repository.ProjectDao
import org.squashtest.tm.service.internal.repository.RequirementVersionDao
import org.squashtest.tm.service.internal.repository.TestCaseDao
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.squashtest.tm.service.servers.StoredCredentialsManager
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService
import spock.lang.Specification

import javax.persistence.EntityManager

class AiTestCaseGenerationServiceImplTest extends Specification {

    EntityManager entityManager = Mock()
    RequirementVersionDao requirementVersionDao = Mock()
    TestCaseLibraryNavigationService testCaseLibraryNavigationService = Mock()
    AiTestCaseGenerationDao aiTestCaseGenerationDao = Mock()
    ActiveMilestoneHolder activeMilestoneHolder = Mock()
    StoredCredentialsManager credentialsManager = Mock()

    AiTestCaseGenerationServiceImpl aiTestCaseGenerationServiceImpl = new AiTestCaseGenerationServiceImpl(
        entityManager, requirementVersionDao, testCaseLibraryNavigationService,
        aiTestCaseGenerationDao, activeMilestoneHolder, credentialsManager
    )

    def "should replace mustache with requirement"() {
        given:
        def requirement = "This replace the mustache in payloadTemplate"
        def payloadTemplate = "This is a Mustache template. Your requirement: {{ requirement }}"

        when:
        def result = aiTestCaseGenerationServiceImpl.getPayloadAsJsonString(requirement, payloadTemplate)

        then:
        result == "This is a Mustache template. Your requirement: This replace the mustache in payloadTemplate"
    }

    def "should transform the jsonPath to a json pointer"() {
        given:
        def path1 = "[0].message.generated_text"
        def path2 = "choices[0].message.content"

        when:
        def transformedPath1 = aiTestCaseGenerationServiceImpl.transformToJsonPointer(path1)
        def transformedPath2 = aiTestCaseGenerationServiceImpl.transformToJsonPointer(path2)

        then:
        transformedPath1 == "/0/message/generated_text"
        transformedPath2 == "/choices/0/message/content"
    }

    def "should extract the server response"() {
        given:
        def jsonPath = "choices[0].message.content"
        def serverResponse = "{\"id\":\"7bad0bb649aa4fbbbd88614d006a2379\",\"object\":\"chat.completion\",\"created\":1712327678,\"model\":\"mistral-medium\",\"choices\":[{\"index\":0,\"message\":{\"role\":\"assistant\",\"content\":\"{\\\"testCases\\\":[{\\\"name\\\":\\\"Cas de test passant - Affichage du message de bienvenue après connexion\\\",\\\"description\\\":\\\"L'utilisateur se connecte et le message de bienvenue s'affiche correctement\\\",\\\"prerequisites\\\":\\\"Un compte utilisateur valide est disponible\\\",\\\"testSteps\\\":[{\\\"index\\\":0,\\\"action\\\":\\\"Ouvrir l'application Squash\\\",\\\"expectedResult\\\":\\\"L'écran de connexion s'affiche correctement\\\"},{\\\"index\\\":1,\\\"action\\\":\\\"Saisir les identifiants de connexion valides\\\",\\\"expectedResult\\\":\\\"Les identifiants sont acceptés\\\"},{\\\"index\\\":2,\\\"action\\\":\\\"Cliquer sur le bouton de connexion\\\",\\\"expectedResult\\\":\\\"L'utilisateur est connecté avec succès\\\"},{\\\"index\\\":3,\\\"action\\\":\\\"Vérifier l'affichage du message de bienvenue\\\",\\\"expectedResult\\\":\\\"Le message 'Bienvenue sur Squash !' s'affiche en police Arial 12 et en couleur vert\\\"}]},{\\\"name\\\":\\\"Cas de test non passant - Message de bienvenue incorrect\\\",\\\"description\\\":\\\"L'utilisateur se connecte et le message de bienvenue s'affiche de manière incorrecte\\\",\\\"prerequisites\\\":\\\"Un compte utilisateur valide est disponible\\\",\\\"testSteps\\\":[{\\\"index\\\":0,\\\"action\\\":\\\"Ouvrir l'application Squash\\\",\\\"expectedResult\\\":\\\"L'écran de connexion s'affiche correctement\\\"},{\\\"index\\\":1,\\\"action\\\":\\\"Saisir les identifiants de connexion valides\\\",\\\"expectedResult\\\":\\\"Les identifiants sont acceptés\\\"},{\\\"index\\\":2,\\\"action\\\":\\\"Cliquer sur le bouton de connexion\\\",\\\"expectedResult\\\":\\\"L'utilisateur est connecté avec succès\\\"},{\\\"index\\\":3,\\\"action\\\":\\\"Vérifier l'affichage du message de bienvenue\\\",\\\"expectedResult\\\":\\\"Le message 'Bienvenue sur Squash !' s'affiche en police différente de Arial 12 ou en couleur différente de vert\\\"}]}\"},\"tool_calls\":null}],\"finish_reason\":\"stop\",\"logprobs\":null,\"usage\":{\"prompt_tokens\":190,\"total_tokens\":657,\"completion_tokens\":467}}"

        when:
        def result = aiTestCaseGenerationServiceImpl.extractServerResponse(jsonPath, serverResponse)

        then:
        result == "{\"testCases\":[{\"name\":\"Cas de test passant - Affichage du message de bienvenue après connexion\",\"description\":\"L'utilisateur se connecte et le message de bienvenue s'affiche correctement\",\"prerequisites\":\"Un compte utilisateur valide est disponible\",\"testSteps\":[{\"index\":0,\"action\":\"Ouvrir l'application Squash\",\"expectedResult\":\"L'écran de connexion s'affiche correctement\"},{\"index\":1,\"action\":\"Saisir les identifiants de connexion valides\",\"expectedResult\":\"Les identifiants sont acceptés\"},{\"index\":2,\"action\":\"Cliquer sur le bouton de connexion\",\"expectedResult\":\"L'utilisateur est connecté avec succès\"},{\"index\":3,\"action\":\"Vérifier l'affichage du message de bienvenue\",\"expectedResult\":\"Le message 'Bienvenue sur Squash !' s'affiche en police Arial 12 et en couleur vert\"}]},{\"name\":\"Cas de test non passant - Message de bienvenue incorrect\",\"description\":\"L'utilisateur se connecte et le message de bienvenue s'affiche de manière incorrecte\",\"prerequisites\":\"Un compte utilisateur valide est disponible\",\"testSteps\":[{\"index\":0,\"action\":\"Ouvrir l'application Squash\",\"expectedResult\":\"L'écran de connexion s'affiche correctement\"},{\"index\":1,\"action\":\"Saisir les identifiants de connexion valides\",\"expectedResult\":\"Les identifiants sont acceptés\"},{\"index\":2,\"action\":\"Cliquer sur le bouton de connexion\",\"expectedResult\":\"L'utilisateur est connecté avec succès\"},{\"index\":3,\"action\":\"Vérifier l'affichage du message de bienvenue\",\"expectedResult\":\"Le message 'Bienvenue sur Squash !' s'affiche en police différente de Arial 12 ou en couleur différente de vert\"}]}"
    }

    def "should clean the server response"() {
        given:
        def serverResponseWithTestCases = "```json\n" +
            "{\n" +
            "  \"testCases\": [\n" +
            "    {\n" +
            "      \"name\": \"Greeting User On Login\",\n" +
            "      \"description\": \"Verify that upon login, the user is greeted with 'Bienvenue sur Squash!' in Arial font size 16 and blue color. Additionally, an 'Administration' button should be present for administrator users and absent for non-administrator users.\",\n" +
            "      \"prerequisites\": \"User has an account on Squash and is able to login.\",\n" +
            "      \"testSteps\": [\n" +
            "        {\n" +
            "          \"index\": 0,\n" +
            "          \"action\": \"Login to Squash with a non-administrator account.\",\n" +
            "          \"expectedResult\": \"User is greeted with 'Bienvenue sur Squash!' displayed in Arial font size 16 and blue color. There should be no 'Administration' button visible on the page.\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"index\": 1,\n" +
            "          \"action\": \"Login to Squash with an administrator account.\",\n" +
            "          \"expectedResult\": \"User is greeted with 'Bienvenue sur Squash!' displayed in Arial font size 16 and blue color. An 'Administration' button should be present on the page.\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}\n" +
            "```"

        when:
        def result = aiTestCaseGenerationServiceImpl.cleanServerResponse(null, serverResponseWithTestCases)

        then:
        result == "{\n" +
            "  \"testCases\": [\n" +
            "    {\n" +
            "      \"name\": \"Greeting User On Login\",\n" +
            "      \"description\": \"Verify that upon login, the user is greeted with 'Bienvenue sur Squash!' in Arial font size 16 and blue color. Additionally, an 'Administration' button should be present for administrator users and absent for non-administrator users.\",\n" +
            "      \"prerequisites\": \"User has an account on Squash and is able to login.\",\n" +
            "      \"testSteps\": [\n" +
            "        {\n" +
            "          \"index\": 0,\n" +
            "          \"action\": \"Login to Squash with a non-administrator account.\",\n" +
            "          \"expectedResult\": \"User is greeted with 'Bienvenue sur Squash!' displayed in Arial font size 16 and blue color. There should be no 'Administration' button visible on the page.\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"index\": 1,\n" +
            "          \"action\": \"Login to Squash with an administrator account.\",\n" +
            "          \"expectedResult\": \"User is greeted with 'Bienvenue sur Squash!' displayed in Arial font size 16 and blue color. An 'Administration' button should be present on the page.\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}"
    }
}
