/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher

import org.apache.commons.io.IOUtils
import org.springframework.security.access.AccessDeniedException
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus
import org.squashtest.tm.domain.attachment.AttachmentList
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.service.attachment.AttachmentManagerService
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.testautomation.model.Attachment
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState
import org.squashtest.tm.service.testautomation.model.TfTestExecutionStatus
import spock.lang.Specification

import javax.persistence.EntityManager

class AutomatedSuitePublisherServiceTest extends Specification {

    EntityManager entityManager = Mock()
    AutomatedSuiteDao autoSuiteDao = Mock()
    AttachmentManagerService attachmentManagerServiceMock = Mock()
    RestITPIManagerService restITPIManagerServiceMock = Mock()
    PermissionEvaluationService permissionEvaluationService = Mock()
    AutomatedSuitePublisherService testee = new AutomatedSuitePublisherService(
            entityManager: entityManager,
            autoSuiteDao: autoSuiteDao,
            attachmentManagerService: attachmentManagerServiceMock,
            restITPIManagerService: restITPIManagerServiceMock,
            permissionEvaluationService: permissionEvaluationService
    )


    def "should change automated suite status for role admin"() {
        given:
        String suiteId = "1234"
        String attachment1Blob = "<test>test1<test>"
        String attachment2Blob = "<test>test2<test>"
        String attachment3Blob = "<test>test3<test>"
        InputStream stream1 = IOUtils.toInputStream(attachment1Blob, "UTF-8")
        InputStream stream2 = IOUtils.toInputStream(attachment2Blob, "UTF-8")
        InputStream stream3 = IOUtils.toInputStream(attachment3Blob, "UTF-8")
            Attachment attachment1 = new Attachment("Test1 - attachment1.txt", stream1)
        Attachment attachment2 = new Attachment("Test1 - attachment1.txt", stream2)
        Attachment attachment3 = new Attachment("Test1 - attachment1.txt", stream3)
        List<Attachment> attachments = new ArrayList<>()
        attachments.add(attachment1)
        attachments.add(attachment2)
        attachments.add(attachment3)

        and:
            TfTestExecutionStatus tfTestExecutionStatus = Mock()

        TestExecutionStatus stateChange = Mock(TestExecutionStatus) {
            getStatus() >> org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.FAILURE
        }

            AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getAttachments() >> attachments
            getTfTestExecutionStatus() >> tfTestExecutionStatus
        }

        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus() >> stateChange

        AttachmentList attachmentList = Mock(AttachmentList) {
            getId() >> 42L
        }

        Iteration iteration = Mock(Iteration) {
            getId() >> 42L
        }

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> ExecutionStatus.SUCCESS
            getAttachmentList() >> attachmentList
            getIteration() >> iteration
        }
        autoSuiteDao.findById(_) >> suite
        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus(_) >> stateChange
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> true
        permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT") >> false

        when:
        testee.forceExecutionStatus(suiteId, automatedExecutionState)

        then:
        1 * suite.setLastModifiedOn(_)
        1 * suite.setExecutionStatus(_)
        3 * attachmentManagerServiceMock.addAttachment(42L,_,_)
    }

    def "should change automated suite status for role ta_api_client"() {
        given:
        String suiteId = "1234"
        String attachment1Blob = "<test>test1<test>"
        String attachment2Blob = "<test>test2<test>"
        String attachment3Blob = "<test>test3<test>"
        InputStream stream1 = IOUtils.toInputStream(attachment1Blob, "UTF-8")
        InputStream stream2 = IOUtils.toInputStream(attachment2Blob, "UTF-8")
        InputStream stream3 = IOUtils.toInputStream(attachment3Blob, "UTF-8")
        Attachment attachment1 = new Attachment("Test1 - attachment1.txt", stream1)
        Attachment attachment2 = new Attachment("Test1 - attachment1.txt", stream2)
        Attachment attachment3 = new Attachment("Test1 - attachment1.txt", stream3)
        List<Attachment> attachments = new ArrayList<>()
        attachments.add(attachment1)
        attachments.add(attachment2)
        attachments.add(attachment3)

        and:
        TfTestExecutionStatus tfTestExecutionStatus = Mock()

        TestExecutionStatus stateChange = Mock(TestExecutionStatus) {
            getStatus() >> org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.FAILURE
        }

        AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getAttachments() >> attachments
            getTfTestExecutionStatus() >> tfTestExecutionStatus
        }

        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus() >> stateChange

        AttachmentList attachmentList = Mock(AttachmentList) {
            getId() >> 42L
        }

        Iteration iteration = Mock(Iteration) {
            getId() >> 42L
        }

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> ExecutionStatus.SUCCESS
            getAttachmentList() >> attachmentList
            getIteration() >> iteration
        }

        autoSuiteDao.findById(_) >> suite
        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus(_) >> stateChange
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> false
        permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT") >> true

        when:
        testee.forceExecutionStatus(suiteId, automatedExecutionState)

        then:
        1 * suite.setLastModifiedOn(_)
        1 * suite.setExecutionStatus(_)
        3 * attachmentManagerServiceMock.addAttachment(42L,_,_)
    }

    def "should change automated suite status for permission on iteration"() {
        given:
        String suiteId = "1234"
        String attachment1Blob = "<test>test1<test>"
        String attachment2Blob = "<test>test2<test>"
        String attachment3Blob = "<test>test3<test>"
        InputStream stream1 = IOUtils.toInputStream(attachment1Blob, "UTF-8")
        InputStream stream2 = IOUtils.toInputStream(attachment2Blob, "UTF-8")
        InputStream stream3 = IOUtils.toInputStream(attachment3Blob, "UTF-8")
        Attachment attachment1 = new Attachment("Test1 - attachment1.txt", stream1)
        Attachment attachment2 = new Attachment("Test1 - attachment1.txt", stream2)
        Attachment attachment3 = new Attachment("Test1 - attachment1.txt", stream3)
        List<Attachment> attachments = new ArrayList<>()
        attachments.add(attachment1)
        attachments.add(attachment2)
        attachments.add(attachment3)

        and:
        Iteration iteration = Mock(Iteration) {
            getId() >> 42L
        }

        TfTestExecutionStatus tfTestExecutionStatus = Mock()

        TestExecutionStatus stateChange = Mock(TestExecutionStatus) {
            getStatus() >> org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.FAILURE
        }

        AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getAttachments() >> attachments
            getTfTestExecutionStatus() >> tfTestExecutionStatus
        }

        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus() >> stateChange

        AttachmentList attachmentList = Mock(AttachmentList) {
            getId() >> 42L
        }

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> ExecutionStatus.SUCCESS
            getAttachmentList() >> attachmentList
            getIteration() >> iteration
        }
        autoSuiteDao.findById(_) >> suite
        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus(_) >> stateChange
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> false
        permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT") >> false
        permissionEvaluationService.hasPermissionOnObject("EXECUTE", iteration) >> true

        when:
        testee.forceExecutionStatus(suiteId, automatedExecutionState)

        then:
        1 * suite.setLastModifiedOn(_)
        1 * suite.setExecutionStatus(_)
        3 * attachmentManagerServiceMock.addAttachment(42L,_,_)
    }

    def "should not update status if status priority is lower than current"() {
        given:
        def currentStatus = ExecutionStatus.SUCCESS
        // Target status has inferior priority than the current so it should be ignored
        def desiredStatus = org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.RUNNING

        and:
        TfTestExecutionStatus tfTestExecutionStatus = Mock()
        TestExecutionStatus stateChange = Mock(TestExecutionStatus) { getStatus() >> desiredStatus }

        AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getAttachments() >> new ArrayList<>()
            getTfTestExecutionStatus() >> tfTestExecutionStatus
        }

        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus() >> stateChange

        AttachmentList attachmentList = Mock(AttachmentList) { getId() >> 42L }
        Iteration iteration = Mock(Iteration) { getId() >> 42L }

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> currentStatus
            getAttachmentList() >> attachmentList
            getIteration() >> iteration
        }
        autoSuiteDao.findById(_) >> suite
        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus(_) >> stateChange
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> true

        when:
        testee.forceExecutionStatus("whatever", automatedExecutionState)

        then:
        0 * suite.setExecutionStatus(_)
    }

    def "should change automated suite status for permission on testsuite"() {
        given:
        String suiteId = "1234"
        String attachment1Blob = "<test>test1<test>"
        InputStream stream1 = IOUtils.toInputStream(attachment1Blob, "UTF-8")
        Attachment attachment1 = new Attachment("Test1 - attachment1.txt", stream1)
        List<Attachment> attachments = new ArrayList<>()
        attachments.add(attachment1)

        and:
        TestSuite testSuite = Mock(TestSuite)  { getId() >> 42L }
        TfTestExecutionStatus tfTestExecutionStatus = Mock(TfTestExecutionStatus)
        TestExecutionStatus stateChange = Mock(TestExecutionStatus) {
            getStatus() >> org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.FAILURE
        }

        AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getAttachments() >> attachments
            getTfTestExecutionStatus() >> tfTestExecutionStatus
        }

        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus() >> stateChange

        AttachmentList attachmentList = Mock(AttachmentList) { getId() >> 42L }

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> ExecutionStatus.SUCCESS
            getAttachmentList() >> attachmentList
            getTestSuite() >> testSuite
        }
        autoSuiteDao.findById(_) >> suite
        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus(_) >> stateChange
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> false
        permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT") >> false
        permissionEvaluationService.hasPermissionOnObject("EXECUTE", testSuite) >> true

        when:
        testee.forceExecutionStatus(suiteId, automatedExecutionState)

        then:
        1 * suite.setLastModifiedOn(_)
        1 * suite.setExecutionStatus(_)
        1 * attachmentManagerServiceMock.addAttachment(42L,_,_)
    }

    def "should deny access instead of changing automated suite status for incorrect role"() {
        given:
        AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState)
        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> ExecutionStatus.SUCCESS
        }
        autoSuiteDao.findById(_) >> suite
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> false
        permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT") >> false

        when:
        testee.forceExecutionStatus("whatev", automatedExecutionState)

        then:
        final AccessDeniedException exception = thrown()
        exception.message == "Access is denied."
        0 * suite.setLastModifiedOn(_)
        0 * suite.setExecutionStatus(_)
        0 * attachmentManagerServiceMock.addAttachment(42L,_,_)
    }

    def "should add ITPI label and Id to attachment name"() {
        given:
        Long testId = 123
        String attachment1Blob = "<test>test1<test>"
        String attachment2Blob = "<test>test2<test>"
        String attachment3Blob = "<test>test3<test>"
        InputStream stream1 = IOUtils.toInputStream(attachment1Blob, "UTF-8")
        InputStream stream2 = IOUtils.toInputStream(attachment2Blob, "UTF-8")
        InputStream stream3 = IOUtils.toInputStream(attachment3Blob, "UTF-8")
        Attachment attachment1 = new Attachment("report.xml", stream1)
        Attachment attachment2 = new Attachment("report.html", stream2)
        Attachment attachment3 = new Attachment("report.json", stream3)
        List<Attachment> attachments = new ArrayList<>()
        attachments.add(attachment1)
        attachments.add(attachment2)
        attachments.add(attachment3)

        and:
        AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getAttachments() >> attachments
        }

        IterationTestPlanItem itpiMock = Mock(IterationTestPlanItem)

        restITPIManagerServiceMock.getById(testId) >> itpiMock

        itpiMock.getLabel() >> "itpiLabel"
        itpiMock.getId() >> 456

        when:
        testee.addItpiLabelAndIdToAttachmentName(testId, automatedExecutionState)

        then:
        attachments.get(0).name == "itpiLabel[456]-report.xml"
        attachments.get(1).name == "itpiLabel[456]-report.html"
        attachments.get(2).name == "itpiLabel[456]-report.json"
    }

    def "should attach allure report to automated suite"() {
        given:
        String suiteId = "1234"
        String attachment1Blob = "<test>test1<test>"
        String attachment2Blob = "<test>test2<test>"
        String attachment3Blob = "<test>test3<test>"
        InputStream stream1 = IOUtils.toInputStream(attachment1Blob, "UTF-8")
        InputStream stream2 = IOUtils.toInputStream(attachment2Blob, "UTF-8")
        InputStream stream3 = IOUtils.toInputStream(attachment3Blob, "UTF-8")
        Attachment attachment1 = new Attachment("Test1 - attachment1.txt", stream1)
        Attachment attachment2 = new Attachment("Test1 - attachment1.txt", stream2)
        Attachment attachment3 = new Attachment("Test1 - attachment1.txt", stream3)
        List<Attachment> attachments = new ArrayList<>()
        attachments.add(attachment1)
        attachments.add(attachment2)
        attachments.add(attachment3)

        String allureBlob = "<tar>allureReportProject<tar>"
        InputStream allureStream = IOUtils.toInputStream(allureBlob, "UTF-8")
        Attachment allureTar = new Attachment("allureReport.tar", allureStream)

        AttachmentList attachmentList = Mock(AttachmentList) {
            getId() >> 42L
        }

        Iteration iteration = Mock(Iteration) {
            getId() >> 42L
        }

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> ExecutionStatus.SUCCESS
            getAttachmentList() >> attachmentList
            getIteration() >> iteration
        }
        autoSuiteDao.findById(_) >> suite
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> false
        permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT") >> true

        when:
        testee.attachReportToAutomatedSuite(suiteId, allureTar)

        then:
        1 * attachmentManagerServiceMock.addAttachment(42L,_,_)
    }

    def "should change automated suite status for role admin with test suite attachment holder"() {
        given:
        String suiteId = "1234"
        List<Attachment> attachments = new ArrayList<>()

        and:
        TfTestExecutionStatus tfTestExecutionStatus = Mock()

        TestExecutionStatus stateChange = Mock(TestExecutionStatus) {
            getStatus() >> org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus.FAILURE
        }

        AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getAttachments() >> attachments
            getTfTestExecutionStatus() >> tfTestExecutionStatus
        }

        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus() >> stateChange

        AttachmentList attachmentList = Mock(AttachmentList) {
            getId() >> 42L
        }

        TestSuite testSuite = Mock(TestSuite) {
            getId() >> 42L
        }

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getExecutionStatus() >> ExecutionStatus.SUCCESS
            getAttachmentList() >> attachmentList
            getTestSuite() >> testSuite
        }
        autoSuiteDao.findById(_) >> suite
        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus(_) >> stateChange
        permissionEvaluationService.hasRole("ROLE_ADMIN") >> true
        permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT") >> false

        when:
        testee.forceExecutionStatus(suiteId, automatedExecutionState)

        then:
        1 * suite.setLastModifiedOn(_)
        1 * suite.setExecutionStatus(_)
        0 * attachmentManagerServiceMock.addAttachment(42L,_,_)
    }
}
