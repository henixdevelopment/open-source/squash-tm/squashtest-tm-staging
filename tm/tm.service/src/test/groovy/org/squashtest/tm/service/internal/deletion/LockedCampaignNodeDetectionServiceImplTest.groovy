/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion

import org.squashtest.tm.domain.NamedReference
import org.squashtest.tm.domain.milestone.Milestone
import org.squashtest.tm.service.deletion.*
import org.squashtest.tm.service.internal.repository.CampaignDao
import org.squashtest.tm.service.internal.repository.CampaignDeletionDao
import org.squashtest.tm.service.internal.repository.display.CampaignDisplayDao
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.squashtest.tm.service.security.PermissionEvaluationService
import spock.lang.Specification

class LockedCampaignNodeDetectionServiceImplTest extends Specification {
    LockedCampaignNodeDetectionServiceImpl handler
    CampaignDeletionDao deletionDao = Mock()
    CampaignDao campaignDao = Mock()
    CampaignDisplayDao campaignDisplayDao = Mock()
    IterationDisplayDao iterationDisplayDao = Mock()
    TestSuiteDisplayDao suiteDisplayDao = Mock()
    PermissionEvaluationService permissionEvaluationService = Mock()
    ActiveMilestoneHolder activeMilestoneHolder = Mock()
    SprintDisplayDao sprintDisplayDao = Mock()

	def setup() {
        handler = new LockedCampaignNodeDetectionServiceImpl(
            activeMilestoneHolder,
            deletionDao,
            campaignDao,
            campaignDisplayDao,
            iterationDisplayDao,
            suiteDisplayDao,
            permissionEvaluationService,
            sprintDisplayDao
        )
	}

    def "should detect campaign with locked nodes"() {
        given:
        def ids = [1L, 2L, 3L]
        campaignDisplayDao.findNamedReferences(_) >> ids.collect { createNameReference(it, "t"+it) }
        campaignDisplayDao.findExecutionIdsByCampaignIds(_) >> [1L: [1L], 2L: [2L], 3L: [], 4L: [1L]]
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 1L, _) >> true
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 2L, _) >> false

        when:
        def result = handler.detectLockedCampaigns(ids)

        then:
        detectEntityLockedNodesResult(result)
    }

    def "should detect sprint with locked nodes"() {
        given:
        def ids = [1L, 2L, 3L]
        sprintDisplayDao.findNamedReferences(_) >> ids.collect { createNameReference(it, "t"+it) }
        sprintDisplayDao.findExecutionIdsBySprintIds(_) >> [1L: [1L], 2L: [2L], 3L: [], 4L: [1L]]
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 1L, _) >> true
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 2L, _) >> false

        when:
        def result = handler.detectLockedSprints(ids)

        then:
        detectEntityLockedNodesResult(result)
    }

    def "should detect iteration with locked nodes"() {
        given:
        def ids = [1L, 2L, 3L]
        iterationDisplayDao.findNamedReferences(_) >> ids.collect { createNameReference(it, "t"+it) }
        iterationDisplayDao.findExecutionIdsByIterationIds(_) >> [1L: [1L], 2L: [2L], 3L: [], 4L: [1L]]
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 1L, _) >> true
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 2L, _) >> false

        when:
        def result = handler.detectLockedIteration(ids)

        then:
        detectEntityLockedNodesResult(result)
    }

    def "should detect test suite with locked nodes"() {
        given:
        def ids = [1L, 2L, 3L]
        suiteDisplayDao.findNamedReferences(_) >> ids.collect { createNameReference(it, "t"+it) }
        suiteDisplayDao.findExecutionIdsBySuiteIds(_) >> [1L: [1L], 2L: [2L], 3L: [], 4L: [1L]]
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 1L, _) >> true
        permissionEvaluationService.hasRoleOrPermissionOnObject(_, _, 2L, _) >> false

        when:
        def result = handler.detectLockedSuites(ids)

        then:
        detectEntityLockedNodesResult(result)
    }

    def detectEntityLockedNodesResult(List<SuppressionPreviewReport> result) {
        result.size() == 2
        result.every { it instanceof NotDeletableCampaignsPreviewReport }
        NotDeletableCampaignsPreviewReport reportWithRights = result[0] as NotDeletableCampaignsPreviewReport;
        reportWithRights.isHasRights()
        reportWithRights.getLockedNodes().isEmpty()

        NotDeletableCampaignsPreviewReport reportWithoutRights = result[1] as NotDeletableCampaignsPreviewReport;
        !reportWithoutRights.isHasRights()
        reportWithoutRights.getLockedNodes() == [2L] as Set
    }

    def "should detect test plan item bound to multiple test suite"() {
        given:
        def ids = [1L, 2L, 3L]
        suiteDisplayDao.findLinkedSuites(_) >> [1L: [1L, 2L], 2L: [2L], 3L: [3L, 4L]]

        when:
        def result = handler.detectTestPlanItemBoundMultipleTestSuite(ids)

        then:
        result.size() == 1
        result[0] instanceof BoundToNotSelectedTestSuite
        BoundToNotSelectedTestSuite report = result[0] as BoundToNotSelectedTestSuite
        report.getLockedNodes().isEmpty()
    }

    def "should detect locked nodes by milestone for campaign"() {
        given:
        def ids = [1L, 2L]
        deletionDao.findCampaignsWhichMilestonesForbidsDeletion(ids) >> [1L, 2L]

        when:
        def result = handler.detectLockedByMilestone(ids)

        then:
        result.size() == 1
        result[0] instanceof BoundToLockedMilestonesReport
        BoundToLockedMilestonesReport report = result[0] as BoundToLockedMilestonesReport
        report.getLockedNodes() == [1L, 2L] as Set
    }

    def "should detect locked nodes by milestone for iteration"() {
        given:
        def ids = [1L, 2L]
        iterationDisplayDao.findCampaignIdsByIterationIds(_) >> [1L, 2L]
        deletionDao.findCampaignsWhichMilestonesForbidsDeletion(ids) >> [1L]

        when:
        def result = handler.detectLockedByMilestoneIteration(ids)

        then:
        result.size() == 1
        result[0] instanceof BoundToLockedMilestonesReport
        BoundToLockedMilestonesReport report = result[0] as BoundToLockedMilestonesReport
        report.getLockedNodes() == [1L] as Set
    }

    def "should detect locked nodes by milestone for test suite"() {
        given:
        def ids = [1L, 2L]
        suiteDisplayDao.findCampaignIdsBySuiteIds(_) >> [1L, 2L]
        deletionDao.findCampaignsWhichMilestonesForbidsDeletion(ids) >> [1L]

        when:
        def result = handler.detectLockedByMilestoneTestSuite(ids)

        then:
        result.size() == 1
        result[0] instanceof BoundToLockedMilestonesReport
        BoundToLockedMilestonesReport report = result[0] as BoundToLockedMilestonesReport
        report.getLockedNodes() == [1L] as Set
    }

    def "should detect locked nodes with active milestone -- SingleOrMultiple"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.of(createMilestone(1L))
        deletionDao.separateFolderFromCampaignIds(ids) >> [[1L], [2L, 3L, 4L]]
        campaignDao.findCampaignIdsHavingMultipleMilestones([2L, 3L, 4L]) >> [2L]
        campaignDao.findNonBoundCampaign([2L, 3L, 4L], 1L) >> [3L, 4L]

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 3
        result[0] instanceof MilestoneModeNoFolderDeletion
        MilestoneModeNoFolderDeletion reportFolder = result[0] as MilestoneModeNoFolderDeletion
        reportFolder.getLockedNodes() == [1L] as Set

        and:
        result[1] instanceof SingleOrMultipleMilestonesReport
        SingleOrMultipleMilestonesReport reportBinding = result[1] as SingleOrMultipleMilestonesReport
        reportBinding.getLockedNodes() == [2L] as Set

        and:
        result[2] instanceof NotBoundToActiveMilestonesReport
        NotBoundToActiveMilestonesReport reportNotBound = result[2] as NotBoundToActiveMilestonesReport
        reportNotBound.getLockedNodes() == [3L, 4L] as Set
    }

    def "should detect locked nodes with active milestone -- BoundToMultiple"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.of(createMilestone(1L))
        deletionDao.separateFolderFromCampaignIds(ids) >> [[1L], [2L, 3L, 4L]]
        campaignDao.findCampaignIdsHavingMultipleMilestones([2L, 3L, 4L]) >> [2L, 3L, 4L]
        campaignDao.findNonBoundCampaign([2L, 3L, 4L], 1L) >> []

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 2
        result[0] instanceof MilestoneModeNoFolderDeletion
        MilestoneModeNoFolderDeletion reportFolder = result[0] as MilestoneModeNoFolderDeletion
        reportFolder.getLockedNodes() == [1L] as Set

        and:
        result[1] instanceof BoundToMultipleMilestonesReport
        BoundToMultipleMilestonesReport reportBinding = result[1] as BoundToMultipleMilestonesReport
        reportBinding.getLockedNodes() == [2L, 3L, 4L] as Set
    }

    def "should not detect locked nodes with no active milestone"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.empty()
        deletionDao.separateFolderFromCampaignIds(ids) >> [[1L], [2L, 3L, 4L]]
        campaignDao.findCampaignIdsHavingMultipleMilestones([2L, 3L, 4L]) >> [2L, 3L, 4L]

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 0
    }

    def createNameReference(Long id, String name) {
        NamedReference ref = Mock(NamedReference)
        ref.getId() >> id
        ref.getName() >> name
        return ref
    }

    def createMilestone(Long id) {
        Milestone milestone = Mock(Milestone)
        milestone.getId() >> id
        return milestone
    }
}
