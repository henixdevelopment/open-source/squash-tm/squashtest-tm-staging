/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter

import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import spock.lang.Specification

class FullTextPostgresFilterValueHandlerTest extends Specification {

    def handler = new FullTextPostgresFilterValueHandler()

    def "should remove unnecessary spaces from filter values"() {
        given:
        def gridFilterValue = new GridFilterValue()
        gridFilterValue.values << "  test   avec  "

        when:
        handler.handleGridFilterValue(gridFilterValue)

        then:
        gridFilterValue.values == ["test & avec"]
    }

    def "should transform html special characters and escape the '&' generated"() {
        given:
        def gridFilterValue = new GridFilterValue()
        gridFilterValue.values << "à bientôt"

        when:
        handler.handleGridFilterValue(gridFilterValue)

        then:
        gridFilterValue.values == ["\\&agrave; & bient\\&ocirc;t"]
    }
}
