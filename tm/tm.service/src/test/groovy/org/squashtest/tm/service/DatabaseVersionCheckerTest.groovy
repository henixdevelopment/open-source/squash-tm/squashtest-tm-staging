/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service

import org.squashtest.tm.exception.DatabaseVersionMismatchException
import org.squashtest.tm.service.configuration.ConfigurationService
import spock.lang.Specification

class DatabaseVersionCheckerTest extends Specification {

    DatabaseVersionChecker databaseVersionChecker = new DatabaseVersionChecker()
    ConfigurationService configurationService = Mock()
    String expectedVersion = "10.0.0"


    def setup() {
        databaseVersionChecker.configurationService = configurationService
    }

    def "testCheckDatabaseVersion_NullDatabaseVersion"() {
        configurationService.findConfiguration(_) >> null

        when:
        databaseVersionChecker.checkDatabaseVersion()

        then:
        def e = thrown(DatabaseVersionMismatchException)
        e.message == "Unable to find the database version."
    }

    def "testCheckDatabaseVersion_IncorrectDatabaseVersion"() {
        configurationService.findConfiguration(_) >> "3.0.0"

        when:
        databaseVersionChecker.checkDatabaseVersion()

        then:
        def e = thrown(DatabaseVersionMismatchException)
        e.message == "The database version is not compatible. Required version : $expectedVersion, current version : 3.0.0"
    }

    def "testCheckDatabaseVersion_CorrectDatabaseVersion"() {
        configurationService.findConfiguration(_) >> expectedVersion

        when:
        databaseVersionChecker.checkDatabaseVersion()

        then:
        noExceptionThrown()
    }
}
