/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion

import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.service.attachment.AttachmentManagerService
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService
import org.squashtest.tm.service.deletion.*
import org.squashtest.tm.service.internal.campaign.LockedCampaignNodeDetectionService
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService
import org.squashtest.tm.service.internal.repository.CampaignDeletionDao
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao
import org.squashtest.tm.service.internal.repository.TestSuiteDao
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService
import spock.lang.Specification

class CampaignDeletionHandlerImplTest extends Specification {
	CampaignDeletionHandlerImpl handler = new CampaignDeletionHandlerImpl()
    LockedCampaignNodeDetectionService lockedCampaignNodeDetectionService = Mock()
	TestSuiteDao suiteDao = Mock()
    TestSuiteDisplayDao suiteDisplayDao = Mock()
	PrivateCustomFieldValueService customValueService = Mock()
	CampaignDeletionDao deletionDao = Mock()
	IterationTestPlanManagerService iterationTestPlanManagerService = Mock()
    IterationTestPlanDao iterationTestPlanDao = Mock()
	AttachmentManagerService attachmentManagerService = Mock()
    AutomatedSuiteManagerService automatedSuiteManagerService = Mock()
    ActiveMilestoneHolder activeMilestoneHolder = Mock()

	def setup() {
        handler.lockedCampaignNodeDetectionService = lockedCampaignNodeDetectionService
        handler.activeMilestoneHolder = activeMilestoneHolder
		handler.suiteDao = suiteDao
		handler.customValueService = customValueService
		handler.deletionDao = deletionDao
		handler.iterationTestPlanManagerService = iterationTestPlanManagerService
		handler.attachmentManager = attachmentManagerService
        handler.iterationTestPlanDao = iterationTestPlanDao
        handler.autoSuiteManagerService = automatedSuiteManagerService
	}

	def "should simulate test suite deletion with no error"() {
		given:
        lockedCampaignNodeDetectionService.detectLockedSuites(_) >> []
        lockedCampaignNodeDetectionService.detectLockedByMilestoneTestSuite(_) >> []
        lockedCampaignNodeDetectionService.detectTestPlanItemBoundMultipleTestSuite(_) >> []

        when:
		def result = handler.simulateSuiteDeletion(_)

        then:
		result == []
	}

	def "should simulate test suite deletion with errors"() {
		given:
        lockedCampaignNodeDetectionService.detectLockedSuites(_) >> [Mock(NotDeletableCampaignsPreviewReport)]
        lockedCampaignNodeDetectionService.detectLockedByMilestoneTestSuite(_) >> [Mock(BoundToLockedMilestonesReport)]
        lockedCampaignNodeDetectionService.detectTestPlanItemBoundMultipleTestSuite(_) >> [Mock(BoundToNotSelectedTestSuite)]

		when:
		def result = handler.simulateSuiteDeletion(_)

		then:
		result.size() == 3
	}

	def "should delete test suite "() {

		given:
        def ids = [1L, 2L, 5L]
		def cmp = createCampaign(1)
		def iteration = createIteration(1, cmp)
		def ts = (1..5).collect { createTestSuite(it, iteration) }
		def itpi = (1..5).collect { createItpi(it, ts) }
		ts.each { it.getTestPlan() >> itpi }
        suiteDao.findAllByIds(ids) >> ts
        attachmentManagerService.getListIDbyContentIdForAttachmentLists(_) >> new ArrayList<>()

        and:
        lockedCampaignNodeDetectionService.detectLockedSuites(_) >> []
        lockedCampaignNodeDetectionService.detectLockedByMilestoneTestSuite(_) >> []
        lockedCampaignNodeDetectionService.detectTestPlanItemBoundMultipleTestSuite(_) >> []

		when:
		def result = handler.deleteSuites(ids, false)
		then:
		result.removed.resid == ids

	}


	def "should delete test suite and remove iteration test plan item from iteration "() {

		given:
        def ids = [1L, 2L, 3L, 4L, 5L]
		def cmp = createCampaign(1)
		def iteration = createIteration(1, cmp)
		def ts = (1..5).collect { createTestSuite(it, iteration) }
		def itpi = (1..5).collect { createItpi(it, ts) }
		ts.each { it.getTestPlan() >> itpi }
        suiteDao.findAllByIds(ids) >> ts
        attachmentManagerService.getListIDbyContentIdForAttachmentLists(_) >> new ArrayList<>()
        iterationTestPlanDao.findIterationTestPlanItemsToRemoveInDeleteTestSuite(ts, ids) >> itpi
        suiteDisplayDao.findExecutionIdsBySuiteIds(_) >> [1L: null]

        and:
        lockedCampaignNodeDetectionService.detectLockedSuites(_) >> []
        lockedCampaignNodeDetectionService.detectLockedByMilestoneTestSuite(_) >> []
        lockedCampaignNodeDetectionService.detectTestPlanItemBoundMultipleTestSuite(_) >> []

        when:
		def result = handler.deleteSuites(ids, true)

		then:
        result.removed.resid == ids
		5 * iterationTestPlanManagerService.removeTestPlanFromIteration(_)

	}

	def "should delete test suite and not remove iteration test plan item from iteration if the item is in another not deleted suite"() {

		given:
        def ids = [1L, 3L, 4L, 5L]
		def cmp = createCampaign(1)
		def iteration = createIteration(1, cmp)
		def ts = (1..5).collect { createTestSuite(it, iteration) }
		def itpi = (1..5).collect { createItpi(it, ts) }
		ts.each { it.getTestPlan() >> itpi }
        suiteDao.findAllByIds(ids) >> ts
        attachmentManagerService.getListIDbyContentIdForAttachmentLists(_) >> new ArrayList<>()
        iterationTestPlanDao.findIterationTestPlanItemsToRemoveInDeleteTestSuite(ts, ids) >> null
        suiteDisplayDao.findExecutionIdsBySuiteIds(_) >> [1L: null]

        and:
        lockedCampaignNodeDetectionService.detectLockedSuites(_) >> []
        lockedCampaignNodeDetectionService.detectLockedByMilestoneTestSuite(_) >> []
        lockedCampaignNodeDetectionService.detectTestPlanItemBoundMultipleTestSuite(_) >> [new BoundToNotSelectedTestSuite()]

		when:
		def result = handler.deleteSuites(ids, true)

		then:
		0 * iterationTestPlanManagerService.removeTestPlanFromIteration(_)

	}

    def "should simulate libraryNode deletion without warning"() {
        given:
        lockedCampaignNodeDetectionService.detectLockedCampaigns(_) >> []
        lockedCampaignNodeDetectionService.detectLockedSprints(_) >> []
        lockedCampaignNodeDetectionService.detectLockedByMilestone(_) >> []
        lockedCampaignNodeDetectionService.detectLockedWithActiveMilestone(_) >> []

        when:
        def reportList = handler.diagnoseSuppression(_)

        then:
        reportList.size() == 0
    }

    def "should simulate sprint deletion with warning"() {
        given:
        lockedCampaignNodeDetectionService.detectLockedCampaigns(_) >> [Mock(NotDeletableCampaignsPreviewReport)]
        lockedCampaignNodeDetectionService.detectLockedSprints(_) >> [Mock(NotDeletableCampaignsPreviewReport)]
        lockedCampaignNodeDetectionService.detectLockedByMilestone(_) >> [Mock(BoundToLockedMilestonesReport)]
        lockedCampaignNodeDetectionService.detectLockedWithActiveMilestone(_) >> [Mock(MilestoneModeNoFolderDeletion), Mock(BoundToMultipleMilestonesReport)]

        when:
        def reportList = handler.diagnoseSuppression(_)

        then:
        reportList.size() == 5
    }

    def "should convert simulation report to a list of locked node ids"() {
        given:
        def ids = [1L, 2L, 3L, 4L, 5L]
        lockedCampaignNodeDetectionService.detectLockedCampaigns(ids) >> [createLockedNodeIdsInReport(Mock(NotDeletableCampaignsPreviewReport), [1L])]
        lockedCampaignNodeDetectionService.detectLockedSprints(ids) >> [createLockedNodeIdsInReport(Mock(NotDeletableCampaignsPreviewReport), [2L])]
        lockedCampaignNodeDetectionService.detectLockedByMilestone(ids) >> []
        lockedCampaignNodeDetectionService.detectLockedWithActiveMilestone(ids) >> [
            createLockedNodeIdsInReport(Mock(BoundToMultipleMilestonesReport), [3L]),
            createLockedNodeIdsInReport(Mock(NotBoundToActiveMilestonesReport), [2L])
        ]

        when:
        def lockedNodeIds = handler.detectLockedNodes(ids)

        then:
        lockedNodeIds == [1L, 2L, 3L]
    }

	def createCampaign(id) {
		Campaign cmp = Mock(Campaign)
		cmp.getId() >> id
		cmp.getMilestones() >> []
		return cmp
	}

	def createIteration(id, cmp) {
		Iteration iteration = Mock(Iteration)
		iteration.getId() >> id
		iteration.getCampaign() >> cmp
		return iteration
	}

	def createTestSuite = { id, iteration ->
		TestSuite ts = Mock(TestSuite)
		ts.getId() >> id
		ts.getAttachmentList() >> []
		ts.getAttachmentList().getId() >> 0
		ts.getIteration() >> iteration
		ts.getMilestones() >> []
		return ts
	}

	def createItpi = { id, testSuites ->
		IterationTestPlanItem itpi = Mock(IterationTestPlanItem)
		itpi.getId() >> id
		itpi.getTestSuites() >> testSuites.collect { it }
		itpi.getExecutions() >> []
		return itpi
	}

    def createLockedNodeIdsInReport = { report, ids ->
        report.getLockedNodes() >> ids
        return report
    }

}
