/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion


import org.squashtest.tm.service.deletion.BoundToLockedMilestonesReport
import org.squashtest.tm.service.deletion.LinkedToIterationPreviewReport
import org.squashtest.tm.service.deletion.NotBoundToActiveMilestonesReport
import org.squashtest.tm.service.deletion.SingleOrMultipleMilestonesReport
import org.squashtest.tm.service.internal.campaign.LockedTestCaseNodeDetectionService
import org.squashtest.tm.service.internal.repository.TestCaseDeletionDao
import org.squashtest.tm.service.internal.repository.TestCaseFolderDao
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import spock.lang.Specification

class TestCaseNodeDeletionHandlerTest extends Specification {
    TestCaseNodeDeletionHandlerImpl handler = new TestCaseNodeDeletionHandlerImpl()
    LockedTestCaseNodeDetectionService lockedTestCaseNodeDetectionService = Mock()
	TestCaseFolderDao fDao = Mock()
	TestCaseDeletionDao deletionDao = Mock()
	ActiveMilestoneHolder activeMilestoneHolder = Mock()


	def setup(){
		handler.folderDao = fDao
		handler.deletionDao = deletionDao
		handler.activeMilestoneHolder = activeMilestoneHolder
        handler.lockedTestCaseNodeDetectionService = lockedTestCaseNodeDetectionService
		activeMilestoneHolder.getActiveMilestone() >> Optional.empty()

		deletionDao.findTestCasesWhichMilestonesForbidsDeletion(_) >> []
	}

    def "should simulate test case deletion with no error"() {
        given:
        lockedTestCaseNodeDetectionService.detectLockedByExecutedTestCases(_) >> []
        lockedTestCaseNodeDetectionService.detectLockedByMilestone(_) >> []
        lockedTestCaseNodeDetectionService.detectLockedWithActiveMilestone(_) >> []
        lockedTestCaseNodeDetectionService.detectLockedByCallSteps(_, _) >> []

        when:
        def result = handler.diagnoseSuppression(_)

        then:
        result == []
    }

    def "should simulate test case deletion with error"() {
        given:
        lockedTestCaseNodeDetectionService.detectLockedByExecutedTestCases(_) >> [Mock(LinkedToIterationPreviewReport)]
        lockedTestCaseNodeDetectionService.detectLockedByMilestone(_) >> [Mock(BoundToLockedMilestonesReport)]
        lockedTestCaseNodeDetectionService.detectLockedWithActiveMilestone(_) >> [Mock(BoundToLockedMilestonesReport)]
        lockedTestCaseNodeDetectionService.detectLockedByCallSteps(_, _) >> [Mock(SingleOrMultipleMilestonesReport)]

        when:
        def result = handler.diagnoseSuppression(_)

        then:
        result.size() == 4
    }

    def "should convert simulation report to a list of locked node ids"() {
        given:
        def ids = [1L, 2L, 3L, 4L, 5L]
        lockedTestCaseNodeDetectionService.detectLockedByExecutedTestCases(_) >> []
        lockedTestCaseNodeDetectionService.detectLockedByMilestone(_) >> [createLockedNodeIdsInReport(Mock(BoundToLockedMilestonesReport), [1L])]
        lockedTestCaseNodeDetectionService.detectLockedWithActiveMilestone(_) >> [
            createLockedNodeIdsInReport(Mock(SingleOrMultipleMilestonesReport), [2L]),
            createLockedNodeIdsInReport(Mock(NotBoundToActiveMilestonesReport), [2L]),
        ]
        lockedTestCaseNodeDetectionService.detectLockedByCallSteps(_, _) >> []

        when:
        def lockedNodeIds = handler.detectLockedNodes(ids);

        then:
        lockedNodeIds == [1L, 2L]
    }

    def createLockedNodeIdsInReport = { report, ids ->
        report.getLockedNodes() >> ids
        return report
    }
}
