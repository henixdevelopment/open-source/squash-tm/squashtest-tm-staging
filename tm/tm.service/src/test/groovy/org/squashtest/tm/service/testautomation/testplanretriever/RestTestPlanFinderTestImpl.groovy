/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever

import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.service.internal.repository.IterationDao
import org.squashtest.tm.service.internal.repository.TestSuiteDao
import org.squashtest.tm.service.internal.testautomation.testplanretriever.RestTestPlanFinderImpl
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NotFoundTargetUUIDException
import spock.lang.Specification

import javax.persistence.NoResultException


class RestTestPlanFinderTest extends Specification {

    IterationDao iterationDaoMock = Mock()
    TestSuiteDao testSuiteDaoMock = Mock()

    RestTestPlanFinderImpl testee = new RestTestPlanFinderImpl(
            iterationDao: iterationDaoMock,
            testSuiteDao: testSuiteDaoMock
    )

    def "Should find an iteration given its UUID"() {

        given:
        Iteration targetIteration = Mock()

        and:
        iterationDaoMock.findByUUID("346615e0-0b5b-4ade-a5f6-671ce972138c") >> targetIteration

        when:
        Iteration iteration = testee.findIterationByUuid("346615e0-0b5b-4ade-a5f6-671ce972138c")

        then:
        iteration == targetIteration
    }

    def "Should not find any iteration given an incorrect UUID"() {

        given:
        iterationDaoMock.findByUUID("346615e0-0b5b-4ade-a5f6-671ce972138c") >> { throw new NoResultException() }

        when:
        testee.findIterationByUuid("346615e0-0b5b-4ade-a5f6-671ce972138c")

        then:
        thrown NotFoundTargetUUIDException
    }

    def "Should find a test suite given its UUID"() {

        given:
        TestSuite targetTestSuite = Mock()

        and:
        testSuiteDaoMock.findByUUID("346615e0-0b5b-4ade-a5f6-671ce972138c") >> targetTestSuite

        when:
        TestSuite testSuite = testee.findTestSuiteByUuid("346615e0-0b5b-4ade-a5f6-671ce972138c")

        then:
        testSuite == targetTestSuite
    }

    def "Should not find any test suite given an incorrect UUID"() {

        given:
        testSuiteDaoMock.findByUUID("346615e0-0b5b-4ade-a5f6-671ce972138c") >> { throw new NoResultException() }

        when:
        testee.findTestSuiteByUuid("346615e0-0b5b-4ade-a5f6-671ce972138c")

        then:
        thrown NotFoundTargetUUIDException
    }
}
