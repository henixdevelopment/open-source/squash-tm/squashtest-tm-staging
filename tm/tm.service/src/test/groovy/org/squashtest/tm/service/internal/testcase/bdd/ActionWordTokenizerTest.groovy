/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd

import org.squashtest.tm.exception.actionword.InvalidActionWordTextException
import spock.lang.Specification

import java.util.stream.Collectors

class ActionWordTokenizerTest extends Specification {
	def "should scan a single token"() {
		given:
		def tokenizer = new ActionWordTokenizer(rawText, true)

		when:
		def tokens = tokenizer.tokenize()

		then:
		tokens.size() == 1
		tokens[0].type == expectedType
		tokens[0].lexeme == rawText

		where:
		rawText						| expectedType
		" " 						| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"\t" 						| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"Some text to tokenize." 	| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"a\\\\bc" 					| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"a\\\"bc" 					| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"a\\<bc" 					| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"a\\>bc" 					| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"\"some free value\"" 		| ActionWordTokenizer.AWTokenType.QUOTED_VALUE
		"\"free\\\\value\"" 		| ActionWordTokenizer.AWTokenType.QUOTED_VALUE
		"\"free \\\"value\\\"\""	| ActionWordTokenizer.AWTokenType.QUOTED_VALUE
		"\"\\<free\\> value\""		| ActionWordTokenizer.AWTokenType.QUOTED_VALUE
        "\"<free> value\""		    | ActionWordTokenizer.AWTokenType.QUOTED_VALUE
		"123" 						| ActionWordTokenizer.AWTokenType.NUMBER
		"1.23" 						| ActionWordTokenizer.AWTokenType.NUMBER
		"123abc"					| ActionWordTokenizer.AWTokenType.PLAIN_TEXT
		"<123abc>"					| ActionWordTokenizer.AWTokenType.ANGLED_VALUE
	}

	def "should reject unterminated free values"() {
		given:
		def tokenizer = new ActionWordTokenizer(rawText, true)

		when:
		tokenizer.tokenize()

		then:
		thrown(ActionWordTokenizer.UnclosedQuotedValue.class)

		where:
		rawText << [
			"\"start free value",
			"and \"start",
			"\"almost\\\"",
			"not \"closed\\\" yet",
		]
	}

	def "should allow escaped quotes in text and free values"() {
		given:
		def raw = "123\tsome mixed \"bag\" of \\\"few\\\" combinations : \"do not \\\"mess\\\" with escaped characters!\""
		def expectedTypes = [
			ActionWordTokenizer.AWTokenType.NUMBER,
			ActionWordTokenizer.AWTokenType.PLAIN_TEXT,
			ActionWordTokenizer.AWTokenType.QUOTED_VALUE,
			ActionWordTokenizer.AWTokenType.PLAIN_TEXT,
			ActionWordTokenizer.AWTokenType.QUOTED_VALUE,
		]
		def expectedTexts = [
			"123 ",
			": some mixed ",
			"\"bag\"",
			" of \\\"few\\\" combinations : ",
			"\"do not \\\"mess\\\" with escaped characters!\"",
		]
		def tokenizer = new ActionWordTokenizer(raw, true)

		when:
		def tokens = tokenizer.tokenize()

		then:
		tokens.size() == expectedTypes.size()
		for (int i = 0; i < expectedTypes.size(); ++i) {
			tokens[i].type == expectedTypes[i]
			tokens[i].lexeme == expectedTexts[i]
		}
	}

	def "should detect parameter names"() {
		given:
		def tokenizer = new ActionWordTokenizer(raw, true)

		when:
		def tokens = tokenizer.tokenize()

		then:
		tokens.stream()
			.filter({ token -> token.getType() == ActionWordTokenizer.AWTokenType.ANGLED_VALUE })
			.count() == expectedParameters.size()

		int idx = 0

		for (int i = 0; i < tokens.size(); ++i) {
			if (tokens[i].type == ActionWordTokenizer.AWTokenType.ANGLED_VALUE) {
				tokens[i].lexeme == expectedParameters[idx]
				idx++
			}
		}

		where:
		raw 					| expectedParameters
		"" 						| []
		"abc" 					| []
		"abc\\<123\\>def"		| []
		"abc1.23def"			| []
		" .1"					| []
		" 1. "					| []
		"_<__>_"	 			| ["<__>"]
		"<hola>"	 			| ["<hola>"]
		"<hola> 123.321" 		| ["<hola>"]
		"0<hola>" 				| ["<hola>"]
		" <hola> "				| ["<hola>"]
		"<hola><hello>" 		| ["<hola>", "<hello>"]
		"\\<<hola>\\>  <hello>" | ["<hola>", "<hello>"]
	}

	def "should reject parameters when they are disallowed"() {
		given:
		def tokenizer = new ActionWordTokenizer(rawText, false)

		when:
		tokenizer.tokenize()

		then:
		thrown(ActionWordTokenizer.ForbiddenAngleException.class)

		where:
		rawText << [
			"<boom>",
			" hey <you> ",
			"\\< <wow> \\>"
		]
	}

	def "should reject close parameter character alone"() {
		given:
		def tokenizer = new ActionWordTokenizer(rawText, true)

		when:
		tokenizer.tokenize()

		then:
		thrown(InvalidActionWordTextException.class)

		where:
		rawText << [
			">",
			" >",
			"\\<>"
		]
	}

	def "should reject unterminated parameter names"() {
		given:
		def tokenizer = new ActionWordTokenizer(rawText, true)

		when:
		tokenizer.tokenize()

		then:
		thrown(ActionWordTokenizer.UnclosedAngledValue.class)

		where:
		rawText << [
			"<start_free_value",
			"and <start",
		]
	}

	def "should discriminate valid numbers"() {
		given:
		def tokenizer = new ActionWordTokenizer(raw, true)

		when:
		def tokens = tokenizer.tokenize()

		then:
		tokens.stream()
			.filter({ tok -> tok.type == ActionWordTokenizer.AWTokenType.NUMBER })
			.count() == expectedNumbers.size()

		int numberIndex = 0

		for (int i = 0; i < tokens.size(); ++i) {
			if (tokens[i].type == ActionWordTokenizer.AWTokenType.NUMBER) {
				tokens[i].lexeme == expectedNumbers[numberIndex]
				numberIndex++
			}
		}

		where:
		raw 			| expectedNumbers
		"" 				| []
		"abc" 			| []
		"abc123def"		| []
		"abc1.23def"	| []
		" .1"			| []
		" 1. "			| []
		"1.1.1"			| []
		".12-3"			| []
		"123.321"	 	| ["123.321"]
		" 123.321" 		| ["123.321"]
		"0" 			| ["0"]
		" 1"			| ["1"]
		"123" 			| ["123"]
		" 123" 			| ["123"]
		"123\t" 		| ["123"]
		"123 321" 		| ["123", "321"]
		"-12"			| ["-12"]
		"1.2-1.56" 		| []
		"1.2 -1.56"		| ["1.2", "-1.56"]
		"1.2 - 1.56"	| ["1.2", "1.56"]
		"1.2-3.4"		| []
		"\"12\"13"		| ["13"]
	}

	def "should preserve whitespaces as is"() {
		given:
		def tokenizer = new ActionWordTokenizer(raw, true)

		when:
		def tokens = tokenizer.tokenize()
		def joined = tokens.stream()
			.map({ tok -> tok.lexeme })
			.collect(Collectors.joining())

		then:
		joined == raw

		where:
		raw << [
			"abc",
			"123",
			" ",
			" abc ",
			" 123 ",
			"a,b,c   d,e,f\t123   321 ",
			"     \t\t\t\t    ",
			" hello    \"   there   \"   :)"
		]
	}
}
