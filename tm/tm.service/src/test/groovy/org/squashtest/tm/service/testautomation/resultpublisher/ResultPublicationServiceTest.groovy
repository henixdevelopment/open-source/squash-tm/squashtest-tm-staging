/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher

import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.service.internal.repository.ApiTokenDao
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.testautomation.model.Attachment
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState
import org.squashtest.tm.service.testautomation.model.TfTestExecutionStatus
import spock.lang.Specification

class ResultPublicationServiceTest extends Specification {

    AutomatedSuiteDao autoSuiteDao = Mock(AutomatedSuiteDao)
    ExecutionService executionService = Mock(ExecutionService)
    ApiTokenDao apiTokenDao = Mock(ApiTokenDao)

    ResultPublicationService testee = new ResultPublicationServiceImpl(
        autoSuiteDao: autoSuiteDao,
        executionService: executionService,
        apiTokenDao: apiTokenDao)

    TfTestExecutionStatus tfTestExecutionStatus = new TfTestExecutionStatus()
    Map<Integer, TfTestExecutionStatus> testStepExecutionStatuses = new HashMap<>()
    def environmentTags = ["robotframework", "linux"]
    Map<String, Object> environmentVariables = new HashMap<>()
    List<Attachment> attachments = new ArrayList<>()
    def testTechnology = "Robot Framework"

    AutomatedExecutionState state

    def "setup"() {
        tfTestExecutionStatus.setStartTime(new Date(2024,8,21,6,25,14))
        tfTestExecutionStatus.setEndTime(new Date(2024,8,21,8,37,7))
        tfTestExecutionStatus.setDuration(14)
        tfTestExecutionStatus.setResultUrl("resultUrl")
    }

    def "Should not try to delete token if is not last status update"() {
        given:
            def isLastUpdate = false

            state = new AutomatedExecutionState(
                tfTestExecutionStatus,
                testStepExecutionStatuses,
                environmentTags,
                environmentVariables,
                attachments,
                testTechnology,
                isLastUpdate
            )
        autoSuiteDao.findById("suiteId") >> new AutomatedSuite()

        when:
            testee.publishResult("suiteId",-42L,state)

        then:
           0 * autoSuiteDao.isAutomatedSuiteCreatedByTestAutomationServerUser(_)
           1 * executionService.updateExecutionAndClearSession(-42L, "suiteId", state)
    }

    def "Should try to delete token if last status update and no schedule deletion if devops launch"() {
        given:
            def isLastUpdate = true

            state = new AutomatedExecutionState(
                tfTestExecutionStatus,
                testStepExecutionStatuses,
                environmentTags,
                environmentVariables,
                attachments,
                testTechnology,
                isLastUpdate
            )
            autoSuiteDao.findById("suiteId") >> new AutomatedSuite();
            autoSuiteDao.isAutomatedSuiteCreatedByTestAutomationServerUser("suiteId") >> true

        when:
            testee.publishResult("suiteId",-42L,state)

        then:
            0 * apiTokenDao.findAllByNameContains("-suiteId-")
            1 * executionService.updateExecutionAndClearSession(-42L, "suiteId", state)
    }


    def "Should try to delete token if last status update and schedule deletion if autom launch"() {
        given:
           def isLastUpdate = true

            state = new AutomatedExecutionState(
                tfTestExecutionStatus,
                testStepExecutionStatuses,
                environmentTags,
                environmentVariables,
                attachments,
                testTechnology,
                isLastUpdate
            )
            autoSuiteDao.findById("suiteId") >> new AutomatedSuite();
            autoSuiteDao.isAutomatedSuiteCreatedByTestAutomationServerUser("suiteId") >> false

        when:
            testee.publishResult("suiteId",-42L,state)

        then:

            1 * apiTokenDao.findAllByNameContains("-suiteId-")
            1 * executionService.updateExecutionAndClearSession(-42L, "suiteId", state)
    }


}
