/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution

import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.execution.*
import org.squashtest.tm.exception.exploratoryexecution.IllegalExploratoryExecutionStateTransitionException
import org.squashtest.tm.service.internal.repository.ExploratoryExecutionDao
import org.squashtest.tm.service.internal.repository.ExploratoryExecutionEventDao
import org.squashtest.tm.service.internal.repository.UserDao
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao
import org.squashtest.tm.service.security.UserContextService
import spock.lang.Specification

import javax.persistence.EntityManager

class ExploratoryExecutionServiceImplTest extends Specification {

    ExploratoryExecutionEventDao exploratoryExecutionEventDao = Mock()
    UserContextService userContextService = Mock()
    UserDao userDao = Mock()
    SprintDisplayDao sprintDisplayDao = Mock()
    ExploratoryExecutionDao exploratoryExecutionDao = Mock()

    ExploratoryExecutionServiceImpl exploratoryExecutionService = new ExploratoryExecutionServiceImpl(
        exploratoryExecutionEventDao, userDao, userContextService, sprintDisplayDao, exploratoryExecutionDao
    )

    static def START = ExploratoryExecutionEventType.START
    static def PAUSE = ExploratoryExecutionEventType.PAUSE
    static def RESUME = ExploratoryExecutionEventType.RESUME
    static def STOP = ExploratoryExecutionEventType.STOP

    static def NEVER_STARTED = ExploratoryExecutionRunningState.NEVER_STARTED
    static def RUNNING = ExploratoryExecutionRunningState.RUNNING
    static def STOPPED = ExploratoryExecutionRunningState.STOPPED
    static def PAUSED = ExploratoryExecutionRunningState.PAUSED

    def setup() {
        exploratoryExecutionService.entityManager = Mock(EntityManager)
    }

    def "should find the running state of a exploratory test execution"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> events

        when:
        ExploratoryExecutionRunningState state = exploratoryExecutionService.findExploratoryExecutionRunningState(1L)

        then:
        state == expectedState

        where:
        events                                                     | expectedState
        []                                                         | NEVER_STARTED
        buildEventSequenceDesc(START)                              | RUNNING
        buildEventSequenceDesc(START, STOP)                        | STOPPED
        buildEventSequenceDesc(START, PAUSE)                       | PAUSED
        buildEventSequenceDesc(START, PAUSE, STOP)                 | STOPPED
        buildEventSequenceDesc(START, PAUSE, RESUME)               | RUNNING
        buildEventSequenceDesc(START, PAUSE, RESUME, STOP)         | STOPPED
        buildEventSequenceDesc(START, PAUSE, RESUME, STOP, RESUME) | RUNNING
    }

    def "should compute the elapsed time for an exploratory execution"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDate(1L) >> events

        when:
        LatestExploratoryExecutionEvent event = exploratoryExecutionService.fetchLatestExploratoryExecutionEvent(1L)

        then:
        event.timeElapsed == expactedTimeElapsed

        where:
        events                                                        | expactedTimeElapsed
        buildEventSequence(START)                                     | 0
        buildEventSequence(START, STOP)                               | 1
        buildEventSequence(START, PAUSE)                              | 1
        buildEventSequence(START, PAUSE, RESUME)                      | 1
        buildEventSequence(START, PAUSE, RESUME, PAUSE, RESUME, STOP) | 3
    }

    def "should start an exploratory execution"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> []
        exploratoryExecutionService.entityManager = Mock(EntityManager)
        exploratoryExecutionService.entityManager.find(ExploratoryExecution.class, 1L) >> mockExploratoryExecution()

        when:
        exploratoryExecutionService.startOrResumeExploratoryExecution(1L, "admin")

        then:
        noExceptionThrown()
    }

    def "should resume an exploratory execution"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> eventSequence
        exploratoryExecutionService.entityManager = Mock(EntityManager)
        exploratoryExecutionService.entityManager.find(ExploratoryExecution.class, 1L) >> mockExploratoryExecution()

        when:
        exploratoryExecutionService.startOrResumeExploratoryExecution(1L, "admin")

        then:
        noExceptionThrown()

        where:
        eventSequence << [
            buildEventSequenceDesc(START, PAUSE),
            buildEventSequenceDesc(START, STOP),
            buildEventSequenceDesc(START, PAUSE, STOP),
        ]
    }

    def "should throw when trying to run an exploratory execution that is already running"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> eventSequence
        exploratoryExecutionService.entityManager = Mock(EntityManager)
        exploratoryExecutionService.entityManager.find(ExploratoryExecution.class, 1L) >> mockExploratoryExecution()

        when:
        exploratoryExecutionService.startOrResumeExploratoryExecution(1L, "admin")

        then:
        thrown(IllegalExploratoryExecutionStateTransitionException.AlreadyRunning)

        where:
        eventSequence << [
            buildEventSequenceDesc(START),
            buildEventSequenceDesc(START, STOP, RESUME),
            buildEventSequenceDesc(START, PAUSE, RESUME),
        ]
    }

    def "should pause an exploratory execution"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> eventSequence
        exploratoryExecutionService.entityManager = Mock(EntityManager)
        exploratoryExecutionService.entityManager.find(ExploratoryExecution.class, 1L) >> mockExploratoryExecution()

        when:
        exploratoryExecutionService.pauseExploratoryExecution(1L, "admin")

        then:
        noExceptionThrown()

        where:
        eventSequence << [
            buildEventSequenceDesc(START),
            buildEventSequenceDesc(START, STOP, RESUME),
            buildEventSequenceDesc(START, PAUSE, RESUME),
        ]
    }

    def "should throw when trying to pause an exploratory execution that is not running"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> eventSequence
        exploratoryExecutionService.entityManager = Mock(EntityManager)
        exploratoryExecutionService.entityManager.find(ExploratoryExecution.class, 1L) >> mockExploratoryExecution()

        when:
        exploratoryExecutionService.pauseExploratoryExecution(1L, "admin")

        then:
        thrown(IllegalExploratoryExecutionStateTransitionException.AlreadyPaused)

        where:
        eventSequence << [
            buildEventSequenceDesc(),
            buildEventSequenceDesc(START, STOP),
            buildEventSequenceDesc(START, PAUSE),
        ]
    }

    def "should stop an exploratory execution"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> eventSequence
        exploratoryExecutionService.entityManager = Mock(EntityManager)
        exploratoryExecutionService.entityManager.find(ExploratoryExecution.class, 1L) >> mockExploratoryExecution()

        when:
        exploratoryExecutionService.stopExploratoryExecution(1L, "admin")

        then:
        noExceptionThrown()

        where:
        eventSequence << [
            buildEventSequenceDesc(START),
            buildEventSequenceDesc(START, STOP, RESUME),
            buildEventSequenceDesc(START, PAUSE, RESUME),
        ]
    }

    def "should throw when trying to stop an exploratory execution that is not running"() {
        given:
        exploratoryExecutionEventDao.findByExploratoryExecution_IdOrderByDateDesc(1L) >> eventSequence
        exploratoryExecutionService.entityManager = Mock(EntityManager)
        exploratoryExecutionService.entityManager.find(ExploratoryExecution.class, 1L) >> mockExploratoryExecution()

        when:
        exploratoryExecutionService.stopExploratoryExecution(1L, "admin")

        then:
        thrown(IllegalExploratoryExecutionStateTransitionException.AlreadyStopped)

        where:
        eventSequence << [
            buildEventSequenceDesc(),
            buildEventSequenceDesc(START, STOP),
        ]
    }

    def buildEventSequence(ExploratoryExecutionEventType... eventTypes) {
        long seconds = 0
        return Arrays.asList(eventTypes).stream()
            .map({
                seconds++
                Date date = new Date(seconds * 1000)
                return new ExploratoryExecutionEvent(null, date, "admin", it)
            })
            .toList()
    }

    def buildEventSequenceDesc(ExploratoryExecutionEventType... eventTypes) {
        def sequence = buildEventSequence(eventTypes)
        Collections.reverse(sequence)
        return sequence
    }

    def mockExploratoryExecution() {
        ExploratoryExecution exploratoryExecution = Mock(ExploratoryExecution)
        exploratoryExecution.getTestPlan() >> Mock(IterationTestPlanItem)
        return exploratoryExecution
    }
}
