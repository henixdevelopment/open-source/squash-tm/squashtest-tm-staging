/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever

import org.apache.commons.lang3.tuple.Triple
import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.CustomField
import org.squashtest.tm.domain.customfield.CustomFieldBinding
import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.domain.infolist.SystemListItem
import org.squashtest.tm.domain.scm.ScmRepository
import org.squashtest.tm.domain.scm.ScmServer
import org.squashtest.tm.domain.servers.AuthenticationProtocol
import org.squashtest.tm.domain.servers.BasicAuthenticationCredentials
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.DatasetParamValue
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.testautomation.TaParametersBuilder
import org.squashtest.tm.service.internal.testautomation.testplanretriever.AutomatedTestBuilderService
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService
import spock.lang.Specification

import javax.inject.Provider
import java.nio.charset.StandardCharsets

class AutomatedTestBuilderServiceTest extends Specification{

    CustomFieldValueFinderService customFieldValueFinderMock = Mock()
    Provider<TaParametersBuilder> providerMock = Mock()
    UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService = Mock();

    AutomatedTestBuilderService testee = new AutomatedTestBuilderService(
        providerMock,
        customFieldValueFinderMock,
        ultimateLicenseAvailabilityService
    )

    TestCase testCase = Mock()
    Dataset dataset = Mock()
    Iteration iteration = Mock()
    Campaign campaign = Mock()
    TestSuite testSuite = Mock()
    IterationTestPlanItem item = Mock()
    List<TestCase> testCases = new ArrayList<>()
    List<Iteration> iterations = new ArrayList<>()
    List<Campaign> campaigns = new ArrayList<>()
    List<TestSuite> testSuites = new ArrayList<>()
    List<IterationTestPlanItem> items = new ArrayList<>()

    CustomFieldValue tcValue = Mock()
    CustomFieldValue itValue = Mock()
    CustomFieldValue cpgValue = Mock()
    CustomFieldValue tsValue = Mock()

    ScmRepository basicAuthRepository = Mock()
    ScmServer basicAuthServer = Mock()
    ScmRepository tokenAuthRepository = Mock()
    ScmServer tokenAuthServer = Mock()
    BasicAuthenticationCredentials basicAuthCredentials = Mock()
    TokenAuthCredentials tokenAuthCredentials = Mock()

    def setup() {
        testCase.getUuid() >> "testUUID"
        testCase.getBoundEntityId() >> 42L
        testCase.getBoundEntityType() >> BindableEntity.TEST_CASE

        basicAuthRepository.getScmServer() >> basicAuthServer
        basicAuthRepository.getFullUrl() >> "http://github.com/myServer/myRepo"
        basicAuthRepository.getWorkingBranch() >> "master"

        tokenAuthRepository.getScmServer() >> tokenAuthServer
        tokenAuthRepository.getFullUrl() >> "http://github.com/myServer/myRepo"
        tokenAuthRepository.getWorkingBranch() >> "master"

        basicAuthCredentials.getUsername() >> "login"
        basicAuthCredentials.getPassword() >> "password".toCharArray()

        tokenAuthCredentials.getToken() >> "ARandomToken"

        basicAuthServer.getKind() >> "git"
        basicAuthServer.getAuthenticationProtocol() >> AuthenticationProtocol.BASIC_AUTH

        tokenAuthServer.getKind() >> "git"
        tokenAuthServer.getAuthenticationProtocol() >> AuthenticationProtocol.TOKEN_AUTH

        testCases.add(testCase)

        dataset.getParameterValues() >> new HashSet<DatasetParamValue>()
        dataset.getName() >> "DSname"

        item.getReferencedTestCase() >> testCase
        item.getReferencedDataset() >> dataset

        items.add(item)

        tcValue.getBoundEntityId() >> 42L

        //Ultimate:

        itValue.getBoundEntityId() >> 43L
        cpgValue.getBoundEntityId() >> 44L
        tsValue.getBoundEntityId() >> 45L
        iteration.getBoundEntityId() >> 43L
        iterations.add(iteration)
        campaign.getBoundEntityId() >> 44L
        campaigns.add(campaign)
        testSuite.getBoundEntityId() >> 45L
        testSuites.add(testSuite)
        item.getIteration() >> iteration
        item.getCampaign() >> campaign
        item.getTestSuites() >> testSuites
    }


    def "should fetch test case custom fields given a list of ITPIs"() {

        given:
        item.getReferencedTestCase() >> testCase
        item.getReferencedDataset() >> dataset
        items.add(item)
        customFieldValueFinderMock.findAllCustomFieldValues(_) >> [tcValue]

        when:
        Map<Long, List<CustomFieldValue>> testCaseCfv = testee.fetchTestCaseCfv(items)

        then:
        testCaseCfv.get(42L) == [tcValue]
    }

    //Community

    def "should create test with params OK (Basic Auth Credentials)"() {
        given:
            testCase.getScmRepository() >> basicAuthRepository
            testCase.getName() >> "oneTestCase"
            testCase.getAutomatedTestReference() >> "ref#test"
            testCase.getUuid() >> "uUid"
            def nature = new SystemListItem()
            nature.setCode("NATURE")
            testCase.getNature() >> nature
            testCase.getImportance() >> "LOW"
            def type = new SystemListItem()
            type.setCode("TYPE")
            testCase.getType() >> type
            def technology = new AutomatedTestTechnology()
            technology.setName("Cypress")
            testCase.getAutomatedTestTechnology() >> technology
            testCase.getId() >> 42
            def path = "path > to > oneTestCase"

            byte[] expectedPassword = new String(basicAuthCredentials.getPassword()).getBytes(StandardCharsets.UTF_8)
            Map<String, Object> paramz = [
                DSNAME:"DSname",
                TC_EXECUTION_ID:100,
                TC_UUID:"testUUID",
                TC_CUF_FIELD:"VALUE",
                AUTOM_AUTOMATED_TEST_REFERENCE:"ref#test",
                AUTOM_AUTOMATED_TEST_TECHNOLOGY_ACTION_PROVIDER_KEY:null,
                AUTOM_SOURCE_CODE_REPOSITORY_TYPE:basicAuthServer.getKind(),
                AUTOM_SOURCE_CODE_REPOSITORY_URL:basicAuthRepository.getFullUrl(),
                AUTOM_SOURCE_CODE_REPOSITORY_BRANCH:basicAuthRepository.getWorkingBranch(),
                AUTOM_SOURCE_CODE_REPOSITORY_AUTHENTICATION_TYPE:basicAuthServer.getAuthenticationProtocol().name(),
                AUTOM_SOURCE_CODE_REPOSITORY_AUTHENTICATION:String.format("%s:%s", basicAuthCredentials.getUsername(), Base64.getEncoder().encodeToString(expectedPassword))
            ]
            Map<String,Object> metadata = [
                "name": "oneTestCase",
                "type": "TYPE",
                "reference": "ref#test",
                "path": ["path", "to", "oneTestCase"],
                "nature": "NATURE",
                "importance": "LOW",
                "technology": "Cypress",
                "uuid": "testUUID"
            ]

        and:

            tcValue.value >> "VALUE"

            CustomFieldBinding binding = Mock()
            tcValue.binding >> binding

            CustomField field = Mock()
            field.code >> "FIELD"
            binding.customField >> field

            CustomFieldValuesForExec cfv = Mock() {
                getValueForTestCase(_) >> [tcValue]
            }

            providerMock.get() >> { return new TaParametersBuilder() }
            testee.paramBuilder = providerMock
            def itemExecutionId = 100L
            ultimateLicenseAvailabilityService.isAvailable() >> false

        when:
            Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>> test = testee.createTestWithParams(item, cfv, basicAuthCredentials,itemExecutionId, path)

        then:
            test.getLeft() == item
            test.getMiddle() == paramz
            test.getRight() == metadata
    }

    def "should create test with params OK (Token Auth Credentials)"() {
        given:
            testCase.getScmRepository() >> tokenAuthRepository
            testCase.getName() >> "twoTestCase"
            testCase.getAutomatedTestReference() >> "ref#test"
            testCase.getUuid() >> "uUid"
            def nature = new SystemListItem()
            nature.setCode("NATURE")
            testCase.getNature() >> nature
            testCase.getImportance() >> "LOW"
            def type = new SystemListItem()
            type.setCode("TYPE")
            testCase.getType() >> type
            def technology = new AutomatedTestTechnology()
            technology.setName("Cypress")
            testCase.getAutomatedTestTechnology() >> technology
            testCase.getId() >> 42
            def path = "path > to > twoTestCase"

            Map<String, Object> paramz = [
                DSNAME:"DSname",
                TC_EXECUTION_ID:100,
                TC_UUID:"testUUID",
                TC_CUF_FIELD:"VALUE",
                AUTOM_AUTOMATED_TEST_REFERENCE:"ref#test",
                AUTOM_AUTOMATED_TEST_TECHNOLOGY_ACTION_PROVIDER_KEY:null,
                AUTOM_SOURCE_CODE_REPOSITORY_TYPE:tokenAuthServer.getKind(),
                AUTOM_SOURCE_CODE_REPOSITORY_URL:tokenAuthRepository.getFullUrl(),
                AUTOM_SOURCE_CODE_REPOSITORY_BRANCH:tokenAuthRepository.getWorkingBranch(),
                AUTOM_SOURCE_CODE_REPOSITORY_AUTHENTICATION_TYPE:tokenAuthServer.getAuthenticationProtocol().name(),
                AUTOM_SOURCE_CODE_REPOSITORY_AUTHENTICATION:tokenAuthCredentials.getToken()
            ]
            Map<String,Object> metadata = [
                "name": "twoTestCase",
                "type": "TYPE",
                "reference": "ref#test",
                "path": ["path", "to", "twoTestCase"],
                "nature": "NATURE",
                "importance": "LOW",
                "technology": "Cypress",
                "uuid": "testUUID"
            ]

        and:

            tcValue.value >> "VALUE"

            CustomFieldBinding binding = Mock()
            tcValue.binding >> binding

            CustomField field = Mock()
            field.code >> "FIELD"
            binding.customField >> field

            CustomFieldValuesForExec cfv = Mock() {
                getValueForTestCase(_) >> [tcValue]
            }

            providerMock.get() >> { return new TaParametersBuilder() }
            testee.paramBuilder = providerMock
            def itemExecutionId = 100L
            ultimateLicenseAvailabilityService.isAvailable() >> false

        when:
            Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>> test = testee.createTestWithParams(item, cfv, tokenAuthCredentials, itemExecutionId, path)

        then:
            test.getLeft() == item
            test.getMiddle() == paramz
            test.getRight() == metadata
    }

    def "should find custom field values"() {
        given:
            customFieldValueFinderMock.findAllCustomFieldValues(testCases) >> [tcValue]
            ultimateLicenseAvailabilityService.isAvailable() >> false

        when:
            CustomFieldValuesForExec result = testee.fetchCustomFieldValues(items)

        then:
            result.getValueForTestCase(42L) == [tcValue]
    }

    // Ultimate

    def "should create test with params OK - ultimate "() {
        given:
            byte[] expectedPassword = new String(basicAuthCredentials.getPassword()).getBytes(StandardCharsets.UTF_8)
            testCase.getName() >> "threeTestCase"
            testCase.getAutomatedTestReference() >> "ref#test#ref"
            testCase.getUuid() >> "uUid"
            testCase.getScmRepository() >> basicAuthRepository

            def nature = new SystemListItem()
            nature.setCode("NAT")
            testCase.getNature() >> nature
            testCase.getImportance() >> "LOW"
            def type = new SystemListItem()
            type.setCode("TYPE")
            testCase.getType() >> type
            def technology = new AutomatedTestTechnology()
            technology.setName("Cypress")
            testCase.getAutomatedTestTechnology() >> technology
            testCase.getId() >> 42
            def path =  "path > to > threeTestCase"

            Map<String, Object> paramz = [
                DSNAME:"DSname",
                TC_EXECUTION_ID:100,
                TC_UUID:"testUUID",
                TC_CUF_FIELD:"TC_VALUE",
                IT_CUF_FIELD:"IT_VALUE",
                CPG_CUF_FIELD:"CPG_VALUE",
                TS_CUF_FIELD:"TS_VALUE",
                AUTOM_AUTOMATED_TEST_REFERENCE:"ref#test#ref",
                AUTOM_AUTOMATED_TEST_TECHNOLOGY_ACTION_PROVIDER_KEY:null,
                AUTOM_SOURCE_CODE_REPOSITORY_TYPE:basicAuthServer.getKind(),
                AUTOM_SOURCE_CODE_REPOSITORY_URL:basicAuthRepository.getFullUrl(),
                AUTOM_SOURCE_CODE_REPOSITORY_BRANCH:basicAuthRepository.getWorkingBranch(),
                AUTOM_SOURCE_CODE_REPOSITORY_AUTHENTICATION_TYPE:basicAuthServer.getAuthenticationProtocol().name(),
                AUTOM_SOURCE_CODE_REPOSITORY_AUTHENTICATION:String.format("%s:%s", basicAuthCredentials.getUsername(), Base64.getEncoder().encodeToString(expectedPassword))
            ]
            Map<String,Object> metadata = [
                "name": "threeTestCase",
                "type": "TYPE",
                "reference": "ref#test#ref",
                "path": ["path", "to", "threeTestCase"],
                "nature": "NAT",
                "importance": "LOW",
                "technology": "Cypress",
                "uuid": "testUUID"
            ]

        and:

            tcValue.value >> "TC_VALUE"
            itValue.value >> "IT_VALUE"
            cpgValue.value >> "CPG_VALUE"
            tsValue.value >> "TS_VALUE"

            CustomFieldBinding binding = Mock()
            tcValue.binding >> binding
            itValue.binding >> binding
            cpgValue.binding >> binding
            tsValue.binding >> binding

            CustomField field = Mock()
            field.code >> "FIELD"
            binding.customField >> field

            AllCustomFieldValuesForExec cfv = Mock() {
                getValueForTestCase(_) >> [tcValue]
                getValueForIteration(_) >> [itValue]
                getValueForCampaign(_) >> [cpgValue]
                getValueForTestSuite(_) >> [tsValue]
            }

            providerMock.get() >> { return new TaParametersBuilder() }
            testee.paramBuilder = providerMock
            def itemExecutionId = 100L
            ultimateLicenseAvailabilityService.isAvailable() >> true

        when:
            Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>> test = testee.createTestWithParams(item, cfv, basicAuthCredentials, itemExecutionId, path)

        then:
            test.getLeft() == item
            test.getMiddle() == paramz
            test.getRight() == metadata
    }

    def "should find custom field values - ultimate "() {
        given:
            customFieldValueFinderMock.findAllCustomFieldValues(testCases) >> [tcValue]
            customFieldValueFinderMock.findAllCustomFieldValues(iterations) >> [itValue]
            customFieldValueFinderMock.findAllCustomFieldValues(campaigns) >> [cpgValue]
            customFieldValueFinderMock.findAllCustomFieldValues(testSuites) >> [tsValue]
            ultimateLicenseAvailabilityService.isAvailable() >> true

        when:
            AllCustomFieldValuesForExec result = testee.fetchCustomFieldValues(items)

        then:
            result.getValueForTestCase(42L) == [tcValue]
            result.getValueForIteration(43L) == [itValue]
            result.getValueForCampaign(44L) == [cpgValue]
            result.getValueForTestSuite(45L) == [tsValue]
    }
}
