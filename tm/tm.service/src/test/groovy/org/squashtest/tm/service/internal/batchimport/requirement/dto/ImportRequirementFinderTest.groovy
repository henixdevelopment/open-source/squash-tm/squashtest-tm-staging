/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.dto

import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget
import org.squashtest.tm.service.internal.repository.RequirementImportDao
import spock.lang.Specification


class ImportRequirementFinderTest extends Specification {

    RequirementImportDao dao = Mock()

    ImportRequirementFinder finder

    def "should find basic requirement target"() {
        given:
        def target = new RequirementTarget("/project/basic/req1")
        finder = new ImportRequirementFinder(dao, [target] as Set)

        and:
        def node = new ImportRequirementFinder.RequirementNode(1L, false, null, null)
        finder.requirements.put("project/basic/req1", List.of(node))

        when:
        def result = finder.searchRequirementNode(target)

        then:
        result != null
        result.id == 1L
        !result.isSynchronized()
    }

    def "should not find basic node with sync target"() {
        given:
        def target = new RequirementTarget("/project/basic/req1")
        target.setRemoteKey("remote_key")
        finder = new ImportRequirementFinder(dao, [target] as Set)

        and:
        def node = new ImportRequirementFinder.RequirementNode(1L, false, null, null)
        finder.requirements.put("project/basic/req1", [node])

        when:
        def result = finder.searchRequirementNode(target)

        then:
        result == null
    }

    def "should find sync node"() {
        given:
        RequirementTarget target = new RequirementTarget("/project/sync/req_sync1")
        target.setRemoteKey("sync_1")

        finder = new ImportRequirementFinder(dao, [target] as Set)

        and:
        def node = new ImportRequirementFinder.RequirementNode(3L, false, "sync_1", null)
        finder.synchronizedRequirements.put("project/sync/req_sync1", [node])

        when:
        def result = finder.searchRequirementNode(target)

        then:
        result != null
        result.id == 3L
        result.isSynchronized()
    }

    def "should not find sync node with basic target"() {
        given:
        RequirementTarget target = new RequirementTarget("/project/sync/req_sync1")

        finder = new ImportRequirementFinder(dao, [target] as Set)

        and:
        def node = new ImportRequirementFinder.RequirementNode(3L, false, "sync_1", null)
        finder.synchronizedRequirements.put("project/sync/req_sync1", [node])

        when:
        def result = finder.searchRequirementNode(target)

        then:
        result == null
    }

    def "should not find remote sync node with no remote sync target"() {
        given:
        RequirementTarget target = new RequirementTarget("/project/remote/remote_1")
        target.setRemoteKey("remote_1")

        finder = new ImportRequirementFinder(dao, [target] as Set)

        and:
        def node = new ImportRequirementFinder.RequirementNode(4L, false, "remote_1", 1L)
        finder.synchronizedRequirements.put("project/sync/remote_1", [node])

        when:
        def result = finder.searchRequirementNode(target)

        then:
        result == null
    }

    def "should find basic parent"() {
        given:
        RequirementTarget child = new RequirementTarget("/project/parent/child")

        finder = new ImportRequirementFinder(dao, [child] as Set)

        def a = new ImportRequirementFinder.RequirementNode(8L, false, null, null)
        def b = new ImportRequirementFinder.RequirementNode(9L, false, "sync", null)

        finder.requirements.put("project/parent", [a, b])


        when:
        def result = finder.searchBasicRequirementParent("/project/parent")

        then:
        result != null
        result.id == 8L
        !result.isSynchronized()
    }

    def "should find sync parent"() {
        given:
        RequirementTarget child = new RequirementTarget("/project/parent/child")

        finder = new ImportRequirementFinder(dao, [child] as Set)

        def a = new ImportRequirementFinder.RequirementNode(9L, false, "sync", null)
        def b = new ImportRequirementFinder.RequirementNode(10L, false, "sync", 1L)

        finder.synchronizedRequirements.put("project/parent", [a, b])

        when:
        def result = finder.searchSynchronizedRequirementParent("/project/parent", null)

        then:
        result != null
        result.id == 9L
        result.isSynchronized()
    }

    def "should find remote sync parent"() {
        given:
        RequirementTarget child = new RequirementTarget("/project/parent/child")

        finder = new ImportRequirementFinder(dao, [child] as Set)

        def a = new ImportRequirementFinder.RequirementNode(9L, false, "sync", null)
        def b = new ImportRequirementFinder.RequirementNode(10L, false, "sync", 1L)

        finder.synchronizedRequirements.put("project/parent", [a, b])

        when:
        def result = finder.searchSynchronizedRequirementParent("/project/parent", 1L)

        then:
        result != null
        result.id == 10L
        result.isSynchronized()
    }

    def"should find target parent"() {
        given:
        RequirementTarget child = new RequirementTarget("/project/parent/child")
        RequirementTarget parent = new RequirementTarget("/project/parent")

        finder = new ImportRequirementFinder(dao, [child, parent] as Set)

        def a = new ImportRequirementFinder.RequirementNode(9L, false, null, null)
        finder.requirements.put("project/parent/child", [a])

        and:
        def childNode = finder.searchRequirementNode(child)

        when:
        def result = finder.searchParentTarget("/project/parent", childNode)

        then:
        result != null
        result == parent

    }
}
