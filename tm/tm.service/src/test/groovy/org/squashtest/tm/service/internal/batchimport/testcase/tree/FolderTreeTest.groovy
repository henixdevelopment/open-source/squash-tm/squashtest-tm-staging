/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase.tree

import org.squashtest.tm.service.internal.batchimport.Batches
import org.squashtest.tm.service.internal.batchimport.testcase.dto.TestCaseImportData
import spock.lang.Specification

class FolderTreeTest extends Specification {
    def "should create folder tree with multiple paths"() {
        given:
        def testCaseImportDataA = List.of(Mock(TestCaseImportData))
        def testCaseImportDataB = List.of(Mock(TestCaseImportData))
        def testCaseImportDataC = List.of(Mock(TestCaseImportData))
        def paths = Map.of(
            "/project/folder1/subfolder1", testCaseImportDataA,
            "/project/folder1/subfolder2", testCaseImportDataB,
            "/project/folder2", testCaseImportDataC,
        )


        when:
        def folderTree = new FolderTree("/project", paths)

        then:
        folderTree.getRoot().getChildren().size() == 2
        folderTree.getRoot().getChild("folder1") != null
        folderTree.getRoot().getChild("folder2") != null

        folderTree.getRoot().getChild("folder1").getChild("subfolder1") != null
        folderTree.getRoot().getChild("folder1").getChild("subfolder2") != null
        folderTree.getRoot().getChild("folder2").getChildren().isEmpty()
    }



    def "should create folder tree with folders containing slashes"() {
        given:
        def testCaseImportDataA = List.of(Mock(TestCaseImportData))
        def testCaseImportDataB = List.of(Mock(TestCaseImportData))
        def testCaseImportDataC = List.of(Mock(TestCaseImportData))
        def paths = Map.of(
            "/project/folder/middle \\/slash", testCaseImportDataA,
            "/root/folder/\\/ begin slash", testCaseImportDataB,
            "/root/folder/end slash \\/", testCaseImportDataC,
        )

        when:
        def folderTree = new FolderTree("root", paths)

        then:
        folderTree.getRoot().getChildren().size() == 1
        folderTree.getRoot().getChild("folder").getChildren().size() == 3
        folderTree.getRoot().getChild("folder").getChild("middle /slash") != null
        folderTree.getRoot().getChild("folder").getChild("/ begin slash") != null
        folderTree.getRoot().getChild("folder").getChild("end slash /") != null
    }


    def "should collect missing nodes"() {
         given:
         def testCaseImportDataA = List.of(Mock(TestCaseImportData))
         def testCaseImportDataB = List.of(Mock(TestCaseImportData))
         def paths = Map.of(
             "/project/folder1/subfolder1", testCaseImportDataA,
             "/project/folder2/subfolder2", testCaseImportDataB,
         )

         FolderTree folderTree = new FolderTree("project", paths)

         and:
         NavigableMap<String, Long> existingPaths = new TreeMap<>()
         existingPaths.put("project/folder1", 1L)
         existingPaths.put("project/folder1/subfolder1", 2L)

         Long testCaseLibraryId = 10L

         when:
         List<MissingNode> missingNodes = folderTree.collectMissingNodes(existingPaths, testCaseLibraryId)

         then:
         missingNodes.size() == 1
         MissingNode missingNode = missingNodes.get(0)
         missingNode.isLibraryContent()
         missingNode.parentId() == testCaseLibraryId
         missingNode.node().getPath() == "project/folder2"
     }

     def "should collect missing nodes with slash in node name"() {
         given:
         def testCaseImportDataA = List.of(Mock(TestCaseImportData))
         def testCaseImportDataB = List.of(Mock(TestCaseImportData))
         def testCaseImportDataC = List.of(Mock(TestCaseImportData))
         def paths = Map.of(
             "/project/folder/middle \\/slash", testCaseImportDataA,
             "/project/folder/end slash \\/", testCaseImportDataB,
             "/project/folder/\\/ begin slash", testCaseImportDataC
         )
         FolderTree folderTree = new FolderTree("project", paths)

         and:
         NavigableMap<String, Long> existingPaths = new TreeMap<>()
         existingPaths.put("project/folder", 1L)
         existingPaths.put("project/folder/middle /slash", 2L)

         Long testCaseLibraryId = 123L

         when:
         List<MissingNode> missingNodes = folderTree.collectMissingNodes(existingPaths, testCaseLibraryId)

         then:
         missingNodes.size() == 2
     }

    def "should collect existing nodes"() {
        given:
        Long existingNodeId = 2L
        def testCaseImportDataA = List.of(Mock(TestCaseImportData))
        def testCaseImportDataB = List.of(Mock(TestCaseImportData))
        def paths = Map.of(
            "/project/folder1/subfolder1", testCaseImportDataA,
            "/project/folder2/subfolder2", testCaseImportDataB,
        )

        FolderTree folderTree = new FolderTree("project", paths)

        and:
        NavigableMap<String, Long> existingPaths = new TreeMap<>()
        existingPaths.put("project/folder1", 1L)
        existingPaths.put("project/folder1/subfolder1", existingNodeId)

        when:
        Batches<TestCaseImportData> existingNodes = folderTree.collectExistingNodes(existingPaths)

        then:
        existingNodes.contains(existingNodeId)
        existingNodes.getEntitiesByTargetId(existingNodeId) == testCaseImportDataA
    }
}
