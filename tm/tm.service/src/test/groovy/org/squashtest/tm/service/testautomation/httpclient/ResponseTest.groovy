/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.httpclient

import org.apache.http.Header
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.StatusLine
import org.apache.http.entity.ContentType
import org.squashtest.tm.service.internal.testautomation.httpclient.Response
import spock.lang.Specification

class ResponseTest extends Specification {
    private HttpResponse httpResponseNotEmpty

    private HttpResponse httpResponseEmpty

    def setup() {
        httpResponseNotEmpty = Mock(HttpResponse)
        httpResponseNotEmpty.getStatusLine() >> Mock(StatusLine)
        HttpEntity entity = Mock()
        Header header = Mock()
        header.getValue() >> ContentType.APPLICATION_JSON.getMimeType()
        entity.getContentType() >> header
        httpResponseNotEmpty.getEntity() >> entity

        httpResponseEmpty = Mock(HttpResponse)
    }

    def "should return status line"() {
        given:

            Response testee = new Response(httpResponseNotEmpty)

        when:

        StatusLine res = testee.getStatusLine()

        then:

        res != null
    }

    def "should return entity"() {
        given:

        Response testee = new Response(httpResponseNotEmpty)

        when:

        HttpEntity res = testee.getEntity()

        then:

        res != null
    }

    def "should return mime type if content"() {
        given:

        Response testee = new Response(httpResponseNotEmpty)

        when:

        String res = testee.mimeType()

        then:

        res == ContentType.APPLICATION_JSON.getMimeType()
    }

    def "should return mime type null if no content"() {
        given:

        Response testee = new Response(httpResponseEmpty)

        when:

        String res = testee.mimeType()

        then:

        res == null
    }
}
