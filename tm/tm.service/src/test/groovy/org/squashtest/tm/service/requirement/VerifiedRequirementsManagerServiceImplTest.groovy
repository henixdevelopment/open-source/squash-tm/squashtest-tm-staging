/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.requirement

import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder
import org.squashtest.tm.core.foundation.collection.PagingAndSorting
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementLibraryNode
import org.squashtest.tm.domain.requirement.RequirementStatus
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.domain.resource.Resource
import org.squashtest.tm.domain.testcase.ActionTestStep
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode
import org.squashtest.tm.domain.testcase.TestStep
import org.squashtest.tm.service.internal.repository.LibraryNodeDao
import org.squashtest.tm.service.internal.repository.RequirementDao
import org.squashtest.tm.service.internal.repository.RequirementVersionCoverageDao
import org.squashtest.tm.service.internal.repository.RequirementVersionDao
import org.squashtest.tm.service.internal.repository.TestCaseDao
import org.squashtest.tm.service.internal.repository.TestStepDao
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader
import org.squashtest.tm.service.internal.requirement.VerifiedRequirementsManagerServiceImpl
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder
import org.squashtest.tm.service.internal.testcase.TestCaseImportanceManagerServiceImpl
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.tools.unittest.assertions.CollectionAssertions
import org.squashtest.tm.tools.unittest.reflection.ReflectionCategory
import spock.lang.Specification

class VerifiedRequirementsManagerServiceImplTest extends Specification {
	VerifiedRequirementsManagerServiceImpl service = new VerifiedRequirementsManagerServiceImpl()
	TestCaseDao testCaseDao = Mock()
	TestStepDao testStepDao = Mock()
    RequirementVersionDao requirementVersionDao = Mock()
	LibraryNodeDao<RequirementLibraryNode> nodeDao = Mock()
	TestCaseCallTreeFinder callTreeFinder = Mock()
	RequirementVersionCoverageDao requirementVersionCoverageDao = Mock()
	TestCaseImportanceManagerServiceImpl testCaseImportanceManagerService = Mock()
    PermissionEvaluationService permissionEvaluationService = Mock()
	ActiveMilestoneHolder activeMilestoneHolder = Mock()
    RequirementDao requirementDao = Mock()
    TestCaseLoader testCaseLoader = Mock()

    TestCase testCase
    RequirementVersion rv5
    RequirementVersion rv15
    RequirementVersion rv25
    Requirement req5
    Requirement req25

	def setup() {
		CollectionAssertions.declareContainsExactly()
        // setup service
		service.testCaseDao = testCaseDao
		service.testStepDao = testStepDao
		service.requirementVersionDao = requirementVersionDao
		service.requirementLibraryNodeDao = nodeDao
		service.testCaseImportanceManagerService = testCaseImportanceManagerService
		service.requirementVersionCoverageDao = requirementVersionCoverageDao
		service.callTreeFinder = callTreeFinder
		service.permissionEvaluationService = permissionEvaluationService
		service.activeMilestoneHolder = activeMilestoneHolder
        service.requirementDao = requirementDao
        service.testCaseLoader = testCaseLoader
		activeMilestoneHolder.getActiveMilestone() >> Optional.empty()
        permissionEvaluationService.hasRole(_) >> true

        // setup test case
        testCase = new TestCase()

        // setup requirementVersions
        rv5 = new RequirementVersion()
        rv15 = new RequirementVersion()
        rv25 = new RequirementVersion()
        use (ReflectionCategory) {
            Resource.set field: "id", of: rv5, to: 5L
            Resource.set field: "id", of: rv15, to: 15L
            Resource.set field: "id", of: rv25, to: 25L
        }
        rv15.setVersionNumber(2)

        // setup requirements
        req5 = new Requirement(rv5)
        req25 = new Requirement(rv25)
        use (ReflectionCategory) {
            RequirementLibraryNode.set field: "id", of: req5, to: 1L
            RequirementLibraryNode.set field: "id", of: req25, to: 2L
            RequirementVersion.set field: "requirement", of: rv5, to: req5
            RequirementVersion.set field: "requirement", of: rv15, to: req5
            RequirementVersion.set field: "requirement", of: rv25, to: req25
        }

        // mock dao responses
        testCaseLoader.load(10L) >> testCase
        testCaseLoader.load(10L, _) >> testCase
        requirementVersionDao.findAllById([5L, 15L]) >> [rv5, rv15]
        requirementVersionDao.findAllById([5L, 25L]) >> [rv5, rv25]
        requirementVersionDao.findAllById([5L, 15L, 25L]) >> [rv5, rv15, rv25]
        nodeDao.findAllByIds([5L, 25L]) >> [req5, req25]
    }

    void cleanup() {
        testCase = null
        rv5 = null
        rv15 = null
        rv25 = null
        req5 = null
        req25 = null
    }

	def "should add requirements to test case's verified requirements"() {
		when:
        def req1
        def reqIds = [5L, 25L]
		service.addVerifiedRequirementsToTestCase(reqIds, 10L)

		then:
        1 * requirementDao.findRequirementsNodeRootAndInFolderByNodeIds([5L, 25L]) >> [req5, req25]
		testCase.verifiedRequirementVersions.containsExactly([rv5, rv25])
	}

	def "should not add requirement version to test case's verified requirements if a sister version is already verified"() {
        given: "rv5 is covered in testCase"
        RequirementVersionCoverage rvc = new RequirementVersionCoverage(rv5, testCase)
        testCase.addRequirementCoverage(rvc)

		when: "Trying to add 3 reqVersions to testCase"
		service.addRequirementVersionsToTestCase([5L, 15L, 25L], 10L)

		then: "rv15 should not be added because it is a sister version of rv5, which is already verified"
		testCase.verifiedRequirementVersions.containsExactly([rv5, rv25])
	}

	def "should add requirements to test step's verified requirements"() {
		given:"a testCase having a testStep of id=10L"
		ActionTestStep testStep = new ActionTestStep()
		TestCase testCase = new TestCase()
		use (ReflectionCategory){
			TestStep.set field:"id", of:testStep, to:10L
			TestCaseLibraryNode.set field:"id", of:testCase, to:16L
		}
		testStepDao.findActionTestStepById(10L)>>testStep
		testCase.addStep(testStep)

		and:"the test case covers the requirement version 5L"
        def reqIds = [5L, 25L]
		RequirementVersionCoverage coverage = new RequirementVersionCoverage(rv5, testCase)
		requirementVersionCoverageDao.byRequirementVersionAndTestCase(5, 16L)>>coverage
		requirementVersionCoverageDao.byRequirementVersionAndTestCase(25, 16L)>>null

		when:
		service.addVerifiedRequirementsToTestStep(reqIds, 10L)

		then:
        1 * requirementDao.findRequirementsNodeRootAndInFolderByNodeIds(reqIds) >> [req5, req25]
		testStep.verifiedRequirementVersions.containsExactly([rv5, rv25])
		1 * requirementVersionCoverageDao.persist(_)
	}

	def "should not add requirements with no verifiable version to test case's verified requirements"() {
		given: "reqVersion 5 is Obsolete"
        def reqIds = [5L, 25L]
		use (ReflectionCategory) {
			RequirementVersion.set field: "status", of: rv5, to: RequirementStatus.OBSOLETE
		}

		when: "trying to add it to test case"
		service.addVerifiedRequirementsToTestCase(reqIds, 10L)

		then: "it should not be added"
        1 * requirementDao.findRequirementsNodeRootAndInFolderByNodeIds(reqIds) >> [req5, req25]
		testCase.verifiedRequirementVersions.containsExactly([rv25])
	}

	def "should remove requirements from test case's verified requirements"() {
		given: "a test case which verifies these requirements"
        RequirementVersionCoverage rvc5 = new RequirementVersionCoverage(req5, testCase)
        RequirementVersionCoverage rvc25 = new RequirementVersionCoverage(req25, testCase)
        requirementVersionCoverageDao.byTestCaseAndRequirementVersions([5L, 25L], 10L)>>[rvc5.id, rvc25.id]

        when:
		service.removeVerifiedRequirementVersionsFromTestCase([5L, 25L], 10L)

		then:
		1 * requirementVersionCoverageDao.delete(_)
	}

	def "should remove single requirement from test case's verified requirements"() {
		given: "a test case which verifies requirement 5"
		RequirementVersionCoverage rvc5 =  new RequirementVersionCoverage(rv5, testCase)
        use (ReflectionCategory) {
            RequirementVersionCoverage.set field: "id", of: rvc5, to: 20L
        }

		requirementVersionCoverageDao.byRequirementVersionAndTestCase(5L, 10L) >> rvc5

		when:
		service.removeVerifiedRequirementVersionFromTestCase(5L, 10L)

		then:
		1 * requirementVersionCoverageDao.delete(_)
	}

	def "should find directly when search all verified requirements for test case"() {
		given: "sorting directives"
		PagingAndSorting sorting = Mock()

		and: "the looked up test case with 1 verified requirement"
		TestCase testCase = Mock()
        testCaseLoader.load(_, _) >> testCase

		RequirementVersion directlyVerified = Mock()
		directlyVerified.id >> 100L

		RequirementVersionCoverage coverage = Mock()
		coverage.verifiedRequirementVersion >> directlyVerified

		testCase.verifies(directlyVerified) >> true

		testCase.getRequirementVersionCoverages() >> [coverage]

		and:
		requirementVersionCoverageDao.findDistinctRequirementVersionsByTestCases({ [20L].containsAll(it) }, _) >> [directlyVerified]

		and : "the looked up test case calls no test case"
		callTreeFinder.getTestCaseCallTree(_) >> []

		when:
		PagedCollectionHolder verifieds = service.findAllVerifiedRequirementsByTestCaseId(20L, sorting)

		then:
		verifieds.pagedItems.collect {it.id} == [100L]
		verifieds.pagedItems.collect { it.directVerification } == [true]
	}

	def "should find 1st level indirectly verified requirements in verified list"() {
		given: "sorting directives"
		PagingAndSorting sorting = Mock()

		and: "the looked up test case with no verified requirement"
		TestCase testCase = Mock()
        testCaseLoader.load(_, _) >> testCase

		testCase.getRequirementVersionCoverages() >> []

		and : "the looked up test case calls a test case"
		long callee = 20L
		callTreeFinder.getTestCaseCallTree(_) >> [callee]

		and: "the callee verifies a requirement"
		RequirementVersionCoverage rvc = Mock()
		RequirementVersion verified = Mock()
		rvc.verifiedRequirementVersion >> verified
		verified.id >> 100L
		requirementVersionCoverageDao.findDistinctRequirementVersionsByTestCases({[10L, 20L].containsAll(it) }, _) >> [verified]

		when:
		PagedCollectionHolder verifieds = service.findAllVerifiedRequirementsByTestCaseId(10, sorting)

		then:
		verifieds.pagedItems.collect{it.id} == [100L]
		verifieds.pagedItems.collect { it.directVerification } == [false]
	}

	def "should find 2nd level indirectly verified requirements in verified list"() {
		given: "sorting directives"
		PagingAndSorting sorting = Mock()

		and: "the looked up test case with no verified requirement"
		TestCase testCase = Mock()
        testCaseLoader.load(_, _) >> testCase

		testCase.getRequirementVersionCoverages() >> []

		and : "the looked up test case calls a test case that calls a test case (L2)"
		long firstLevelCallee = 20L
		long secondLevelCallee = 30L
		callTreeFinder.getTestCaseCallTree(_) >> [
			firstLevelCallee,
			secondLevelCallee
		]

		and: "the L2 callee verifies a requirement"
		RequirementVersionCoverage rvc = Mock()
		RequirementVersion verified = Mock()
		rvc.verifiedRequirementVersion >> verified
		verified.id >> 100L
		requirementVersionCoverageDao.findDistinctRequirementVersionsByTestCases({[10L, 20L, 30L].containsAll(it) }, _) >> [verified]

		when:
		PagedCollectionHolder verifieds = service.findAllVerifiedRequirementsByTestCaseId(10, sorting)

		then:
		verifieds.pagedItems.collect{it.id}==[100L]
		verifieds.pagedItems.collect { it.directVerification } == [false]
	}

	def "should count verified requirements in verified list"() {
		given: "sorting directives"
		PagingAndSorting sorting = Mock()

		and: "the looked up test case"
		TestCase testCase = Mock()
		testCaseDao.findById(10L) >> testCase

		testCase.getRequirementVersionCoverages() >> []

		and: "the looked up test case calls no test case"
		callTreeFinder.getTestCaseCallTree(10L) >> []

		and:
		requirementVersionCoverageDao.findDistinctRequirementVersionsByTestCases({ [10L].containsAll(it) }, _) >> []

		and:
		requirementVersionCoverageDao.numberDistinctVerifiedByTestCases({ [10L].containsAll(it) }) >> 666

        when:
		PagedCollectionHolder verifieds = service.findAllVerifiedRequirementsByTestCaseId(10, sorting)

		then:
		verifieds.totalNumberOfItems == 666
	}
}
