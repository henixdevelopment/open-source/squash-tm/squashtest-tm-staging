/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.column.testcase

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.squashtest.tm.service.importer.ImportMode
import org.squashtest.tm.service.internal.batchimport.instruction.DatasetParamValueInstruction
import org.squashtest.tm.service.internal.batchimport.column.StdColumnDef
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet
import org.squashtest.tm.service.internal.batchimport.column.WorksheetDef
import spock.lang.Specification
import spock.lang.Unroll

/**
 * @author Gregory Fouquet
 *
 */
class DatasetParamValueInstructionBuilderTest extends Specification {
	WorksheetDef wd = Mock();
	Row row = Mock()
	DatasetParamValueInstructionBuilder builder

	def setup() {
		wd.getWorksheetType() >> TemplateWorksheet.DATASET_PARAM_VALUES_SHEET
		builder = new DatasetParamValueInstructionBuilder(wd)
	}

	private Cell mockCell(cellType, cellValue) {
		Cell cell = Mock()

		cell.getCellType() >> cellType

		cell.getNumericCellValue() >> cellValue
		cell.getStringCellValue() >> cellValue
		cell.getBooleanCellValue() >> cellValue
		cell.getDateCellValue() >> cellValue

		return cell
	}

	@Unroll
	def "should populate a dataset target from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		DatasetParamValueInstruction instruction = builder.build(row)

		then:
		instruction.target[propName] == propValue

		where:
		col				                                | cellType             | cellValue    | propName | propValue
        DatasetParamValuesSheetColumn.TC_OWNER_PATH	    | CellType.STRING      | "/here/i/am" | "path"   | "/here/i/am/datasets/null"
        DatasetParamValuesSheetColumn.TC_OWNER_PATH	    | CellType.BLANK       | null         | "path"   | "null/datasets/null"

        DatasetParamValuesSheetColumn.TC_DATASET_NAME	| CellType.STRING      | "my name"    | "name"   | "my name"
        DatasetParamValuesSheetColumn.TC_DATASET_NAME	| CellType.BLANK       | null         | "name"   | null
	}

	@Unroll
	def "should create test step instruction from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		DatasetParamValueInstruction instruction = builder.build(row)

		then:
		instruction[propName] == propValue

		where:
		col				                                | cellType					| cellValue			| propName			| propValue
        DatasetParamValuesSheetColumn.ACTION			| CellType.STRING	     	| "CREATE"			| "mode"			| ImportMode.CREATE
        DatasetParamValuesSheetColumn.ACTION			| CellType.STRING		    | "C"				| "mode"			| ImportMode.CREATE
        DatasetParamValuesSheetColumn.ACTION			| CellType.BLANK		    | null				| "mode"			| ImportMode.UPDATE

	}

	@Unroll
	def "should create action test step from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		DatasetParamValueInstruction instruction = builder.build(row)

		then:
		instruction.datasetValue[propName] == propValue

		where:
		col						                                | cellType					| cellValue								| propName				| propValue
        DatasetParamValuesSheetColumn.TC_DATASET_PARAM_NAME	    | CellType.STRING		    | "my name is luka"						| "parameterName"		| "my name is luka"
        DatasetParamValuesSheetColumn.TC_DATASET_PARAM_NAME	    | CellType.BLANK		    | null									| "parameterName"		| null

        DatasetParamValuesSheetColumn.TC_DATASET_PARAM_VALUE	| CellType.STRING		    | "i live on the 2nd floor"				| "value"				| "i live on the 2nd floor"
        DatasetParamValuesSheetColumn.TC_DATASET_PARAM_VALUE	| CellType.BLANK		    | null									| "value"				| ""

        DatasetParamValuesSheetColumn.TC_PARAM_OWNER_PATH		| CellType.STRING		    | "we/re/here"							| "parameterOwnerPath"	| "we/re/here"
        DatasetParamValuesSheetColumn.TC_PARAM_OWNER_PATH		| CellType.BLANK		    | null									| "parameterOwnerPath"	| null

	}

}
