/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot

import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.KeywordTestCase
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest
import spock.lang.Specification

import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuildersTestUtils.createActionWordStep
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuildersTestUtils.createBasicActionWord

class KeywordSectionBuilderTest extends Specification {

	KeywordTestCase keywordTestCase

	def "setup"() {
		keywordTestCase = Spy(KeywordTestCase)
		keywordTestCase.getId() >> 341L
		keywordTestCase.getName() >> "My test case"
		keywordTestCase.notifyAssociatedWithProject(new Project())

		keywordTestCase.automationRequest = new AutomationRequest()
		keywordTestCase.automationRequest.automationPriority = 42
	}

	def "with minimal test case"() {
		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)

		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}

	def "with dataset"() {
		given:
		keywordTestCase.addStep(createActionWordStep(Keyword.GIVEN, "the login form is displayed"))
		keywordTestCase.addStep(createActionWordStep(Keyword.WHEN, "the login is set to <login>"))
		keywordTestCase.addStep(createActionWordStep(Keyword.AND, "the last name is set to <lastName>"))
		keywordTestCase.addStep(createActionWordStep(Keyword.AND, "the email is set to <email>"))
		keywordTestCase.addStep(createActionWordStep(Keyword.AND, "the phone number is set to <phoneNumber>"))
		keywordTestCase.addStep(createActionWordStep(Keyword.AND, "the birth date is set to <birthDate>"))
		keywordTestCase.addStep(createActionWordStep(Keyword.AND, "the login form is displayed"))
		keywordTestCase.addStep(createActionWordStep(Keyword.AND, "the card number is set to <cardNumber>"))
		keywordTestCase.addStep(createActionWordStep(Keyword.THEN, "the website crashes"))
		keywordTestCase.addStep(createActionWordStep(Keyword.AND, "the form content is lost forever."))

		def dataset =  new Dataset()
		dataset.setName("dataset1")
		keywordTestCase.addDataset(dataset)

		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)

		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                \${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=\${city}    country=\${country}    currency=\${currency}

    \${login} =          Get Test Param    DS_login
    \${lastName} =       Get Test Param    DS_lastName
    \${email} =          Get Test Param    DS_email
    \${phoneNumber} =    Get Test Param    DS_phoneNumber
    \${birthDate} =      Get Test Param    DS_birthDate
    \${cardNumber} =     Get Test Param    DS_cardNumber

    &{dataset} =    Create Dictionary    login=\${login}            lastName=\${lastName}        email=\${email}    phoneNumber=\${phoneNumber}
    ...                                  birthDate=\${birthDate}    cardNumber=\${cardNumber}

    RETURN    &{dataset}
"""
	}

	def "with 2 datatables"() {
		given:
		keywordTestCase.setName("User table test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.WHEN, createBasicActionWord("I am on user page"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I can see the users"))
		step2.setDatatable("| Henry | Dupond | henry.dupond@mail.com |\n" +
			"| Louis | Dupont | louis.dupont@mail.com |\n" +
			"| Charles | Martin | charles.martin@mail.com |")
		KeywordTestStep step3 = new KeywordTestStep(Keyword.AND, createBasicActionWord("I see the administrator"))
		step3.setDatatable("| Bruce | Wayne | batman@mail.com |\n" +
			"| Peter | Parker | spiderman@mail.com |\n" +
			"| Clark | Kent | superman@mail.com |\n" +
			"| Tony  | Stark | ironman@mail.com |\n" +
			"| Bruce | Banner | hulk@mail.com |\n" +
			"| Antonio | Diego | bane@mail.com |\n" +
			"| Eduardo | Dorrance | therealbane@mail.com |\n" +
			"| Erik | Lehnsherr | magneto@mail.com |")

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)
		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)
		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}    datatable_2=\${datatable_2}

    @{row_1_1} =    Create List    Henry      Dupond    henry.dupond\\@mail.com
    @{row_1_2} =    Create List    Louis      Dupont    louis.dupont\\@mail.com
    @{row_1_3} =    Create List    Charles    Martin    charles.martin\\@mail.com
    @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}    \${row_1_3}

    @{row_2_1} =    Create List    Bruce      Wayne        batman\\@mail.com
    @{row_2_2} =    Create List    Peter      Parker       spiderman\\@mail.com
    @{row_2_3} =    Create List    Clark      Kent         superman\\@mail.com
    @{row_2_4} =    Create List    Tony       Stark        ironman\\@mail.com
    @{row_2_5} =    Create List    Bruce      Banner       hulk\\@mail.com
    @{row_2_6} =    Create List    Antonio    Diego        bane\\@mail.com
    @{row_2_7} =    Create List    Eduardo    Dorrance     therealbane\\@mail.com
    @{row_2_8} =    Create List    Erik       Lehnsherr    magneto\\@mail.com
    @{datatable_2} =    Create List    \${row_2_1}    \${row_2_2}    \${row_2_3}    \${row_2_4}    \${row_2_5}    \${row_2_6}    \${row_2_7}    \${row_2_8}

    &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}    datatable_2=\${datatable_2}

    RETURN    &{datatables}
"""
	}

	def "with 1 datatable containing escaped pipes"() {
		given:
		keywordTestCase.setName("User table test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.WHEN, createBasicActionWord("I am on user page"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I can see the users"))
		step2.setDatatable("| Henry | Dupond | henry.dupond@mail.com |\n" +
			"| Louis \\|\\| | Dupont | louis.dupont@mail.com |\n" +
			"| Charles | Martin | charles.martin@mail.com |")

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)
		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}    datatable_2=\${datatable_2}

    @{row_1_1} =    Create List    Henry       Dupond    henry.dupond\\@mail.com
    @{row_1_2} =    Create List    Louis ||    Dupont    louis.dupont\\@mail.com
    @{row_1_3} =    Create List    Charles     Martin    charles.martin\\@mail.com
    @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}    \${row_1_3}

    &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}

    RETURN    &{datatables}
"""
	}

	def "with 1 datatable containing special characters"() {
		given:
		keywordTestCase.setName("Automatic characters escaping")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.WHEN, createBasicActionWord("I create an action word containing escaped special characters"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I can see the escape characters are kept"))

		// Extracted from Robot documentation.
		// The official documentation also lists the pipe "|" as an escapable character, which is only useful when using
		// the pipe separated format. TM users need to escape them in datatables (because these are pipe separated) but
		// the escape character won't be rendered in the resulting script (which is space separated).
		step2.setDatatable("""
			| ch | description                                         | escaped           |
			| \$ | Dollar sign, never starts a scalar variable.        | \\\${notvar}        |
			| @  | At sign, never starts a list variable.              | \\@{notvar}         |
			| &  | Ampersand, never starts a dictionary variable.      | \\&{notvar}         |
			| %  | Percent sign, never starts an environment variable. | \\%{notvar}         |
			| #  | Hash sign, never starts a comment.                  | \\# not comment     |
			| =  | Equal sign, never part of named argument syntax.    | not\\=named         |
			| \\ | Backslash character, never escapes anything.        | c:\\\\temp, \\\${var} |""")

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)
		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}    datatable_2=\${datatable_2}

    @{row_1_1} =    Create List
    @{row_1_2} =    Create List    ch    description                                            escaped
    @{row_1_3} =    Create List    \\\$    Dollar sign, never starts a scalar variable.           \\\\\\\${notvar}
    @{row_1_4} =    Create List    \\@    At sign, never starts a list variable.                 \\\\\\@{notvar}
    @{row_1_5} =    Create List    \\&    Ampersand, never starts a dictionary variable.         \\\\\\&{notvar}
    @{row_1_6} =    Create List    \\%    Percent sign, never starts an environment variable.    \\\\\\%{notvar}
    @{row_1_7} =    Create List    \\#    Hash sign, never starts a comment.                     \\\\\\# not comment
    @{row_1_8} =    Create List    \\=    Equal sign, never part of named argument syntax.       not\\\\\\=named
    @{row_1_9} =    Create List    \\\\    Backslash character, never escapes anything.           c:\\\\\\\\temp, \\\\\\\${var}
    @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}    \${row_1_3}    \${row_1_4}    \${row_1_5}    \${row_1_6}    \${row_1_7}    \${row_1_8}    \${row_1_9}

    &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}

    RETURN    &{datatables}
"""
	}

	def "with both datatable and docstring on 1 step"() {
		given:
		keywordTestCase.setName("User table test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.WHEN, createBasicActionWord("I am on user page"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I can see the users"))
		step2.setDatatable("| Henry | Dupond | henry.dupond@mail.com |\n" +
			"| Louis | Dupont | louis.dupont@mail.com |\n" +
			"| Charles | Martin | charles.martin@mail.com |")
		step2.setDocstring("\tDear Mommy,\nI am fine, thank You!\n\tYour Son.")
		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)
		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}    datatable_2=\${datatable_2}

    @{row_1_1} =    Create List    Henry      Dupond    henry.dupond\\@mail.com
    @{row_1_2} =    Create List    Louis      Dupont    louis.dupont\\@mail.com
    @{row_1_3} =    Create List    Charles    Martin    charles.martin\\@mail.com
    @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}    \${row_1_3}

    &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}

    RETURN    &{datatables}
"""
	}

	def "with 1 docstring"() {
		given:
		keywordTestCase.setName("Letter test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("following letter is displayed"))
		step1.setDocstring("\tDear Jack,\n" +
			"I have arrived in London this morning. Everything went well!\n" +
			"Looking forward to seeing you on Friday.\n" +
			"\n\tYour friend, John.")

		keywordTestCase.addStep(step1)
		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)
		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Docstrings
    [Documentation]    Retrieves Squash TM's docstrings and stores them in a dictionary.
    ...
    ...                For instance, two docstrings have been defined in Squash TM,
    ...                the first one containing the string
    ...                "I am the
    ...                FIRST    docstring",
    ...                the second one containing the string "I am the second docstring"
    ...
    ...                First, this keyword retrieves values and converts them to an inline string :
    ...                \${docstring_1} =    Set Variable    I am the\\nFIRST\\tdocstring"
    ...
    ...                Then, this keyword stores the docstrings into the &{docstrings} dictionary
    ...                with each docstring name as key, and each docstring value as value :
    ...                \${docstrings} =    Create Dictionary    docstring_1=\${docstring_1}    docstring_2=\${docstring_2}

    \${docstring_1} =    Set Variable    \\tDear Jack,\\nI have arrived in London this morning. Everything went well!\\nLooking forward to seeing you on Friday.\\n\\n\\tYour friend, John.

    &{docstrings} =    Create Dictionary    docstring_1=\${docstring_1}

    RETURN    &{docstrings}
"""
	}

	def "with 2 docstrings"() {
		given:
		keywordTestCase.setName("Letter test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("following letter is displayed"))
		step1.setDocstring("\tDear Jack,\n" +
			"I have arrived in London this morning. Everything went well!\n" +
			"Looking forward to seeing you on Friday.\n" +
			"\n\tYour friend, John.")
		KeywordTestStep step2 = new KeywordTestStep(Keyword.AND, createBasicActionWord("following letter is displayed"))
		step2.setDocstring("\tDear Jack,\n" +
			"I have arrived in London this morning. Everything went well!\n" +
			"Looking forward to seeing you on Tuesday.\n" +
			"\n\tYour friend, John.")

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		when:
		String result = KeywordSectionBuilder.buildKeywordsSection(keywordTestCase)
		then:
		result ==
			"""*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_341_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_341_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_341_SETUP_VALUE} =    Get Variable Value    \${TEST_341_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_341_SETUP_VALUE is not None
        Run Keyword    \${TEST_341_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_341_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_341_TEARDOWN}.

    \${TEST_341_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_341_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_341_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_341_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Docstrings
    [Documentation]    Retrieves Squash TM's docstrings and stores them in a dictionary.
    ...
    ...                For instance, two docstrings have been defined in Squash TM,
    ...                the first one containing the string
    ...                "I am the
    ...                FIRST    docstring",
    ...                the second one containing the string "I am the second docstring"
    ...
    ...                First, this keyword retrieves values and converts them to an inline string :
    ...                \${docstring_1} =    Set Variable    I am the\\nFIRST\\tdocstring"
    ...
    ...                Then, this keyword stores the docstrings into the &{docstrings} dictionary
    ...                with each docstring name as key, and each docstring value as value :
    ...                \${docstrings} =    Create Dictionary    docstring_1=\${docstring_1}    docstring_2=\${docstring_2}

    \${docstring_1} =    Set Variable    \\tDear Jack,\\nI have arrived in London this morning. Everything went well!\\nLooking forward to seeing you on Friday.\\n\\n\\tYour friend, John.
    \${docstring_2} =    Set Variable    \\tDear Jack,\\nI have arrived in London this morning. Everything went well!\\nLooking forward to seeing you on Tuesday.\\n\\n\\tYour friend, John.

    &{docstrings} =    Create Dictionary    docstring_1=\${docstring_1}    docstring_2=\${docstring_2}

    RETURN    &{docstrings}
"""
	}
}
