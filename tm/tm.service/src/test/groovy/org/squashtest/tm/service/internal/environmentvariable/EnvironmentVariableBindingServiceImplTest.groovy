/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable

import org.squashtest.tm.domain.environmentvariable.EVBindableEntity
import org.squashtest.tm.domain.environmentvariable.EVInputType
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingValueService
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto
import org.squashtest.tm.service.internal.repository.CustomEnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao
import org.squashtest.tm.service.internal.repository.display.EnvironmentVariableDisplayDao
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao
import org.squashtest.tm.service.security.PermissionEvaluationService
import spock.lang.Specification

class EnvironmentVariableBindingServiceImplTest extends Specification {

	EnvironmentVariableBindingServiceImpl service
	EnvironmentVariableDao environmentVariableDao = Mock()
	EnvironmentVariableBindingDao environmentVariableBindingDao = Mock()
	CustomEnvironmentVariableBindingDao customEnvironmentVariableBindingDao = Mock()
	EnvironmentVariableBindingValueService bindingValueService = Mock()
	EnvironmentVariableDisplayDao environmentVariableDisplayDao = Mock()
	ProjectDisplayDao projectDisplayDao = Mock()
    PermissionEvaluationService permissionEvaluationService = Mock()

	def setup() {
		service = new EnvironmentVariableBindingServiceImpl(
                environmentVariableDao,
                environmentVariableBindingDao,
				bindingValueService,
                customEnvironmentVariableBindingDao,
                environmentVariableDisplayDao,
                projectDisplayDao,
                permissionEvaluationService
        )
	}

	def "should create new binding from server view"() {
		given:
		Long serverId = -1L
		EVBindableEntity serverType = EVBindableEntity.TEST_AUTOMATION_SERVER
		List<Long> variableIds = Arrays.asList(-1L, -2L)

		EnvironmentVariable ev1 = new EnvironmentVariable("ev1", EVInputType.PLAIN_TEXT)
		EnvironmentVariable ev2 = new EnvironmentVariable("ev2", EVInputType.PLAIN_TEXT)

		List<EnvironmentVariable> variables = Arrays.asList(ev1, ev2)

		when:
		service.createNewBindings(serverId, serverType, variableIds)

		then:
		1 * environmentVariableDao.findAllByIdIn(variableIds) >> variables
		4 * environmentVariableBindingDao.save(_)
		2 * customEnvironmentVariableBindingDao.findProjectsLinkedToServerWhereVariableIsNotBound(serverId, _) >> Arrays.asList(-1L)

	}

	def "should create new binding from project view"() {
		given:
		Long projectId = -1L
		EVBindableEntity projectType = EVBindableEntity.PROJECT
		List<Long> variableIds = Arrays.asList(-1L, -2L)

		EnvironmentVariable ev1 = new EnvironmentVariable("ev1", EVInputType.PLAIN_TEXT)
		EnvironmentVariable ev2 = new EnvironmentVariable("ev2", EVInputType.PLAIN_TEXT)

		List<EnvironmentVariable> variables = Arrays.asList(ev1, ev2)

		when:
		service.createNewBindings(projectId, projectType, variableIds)

		then:
		1 * environmentVariableDao.findAllByIdIn(variableIds) >> variables
		2 * environmentVariableBindingDao.save(_)
		0 * customEnvironmentVariableBindingDao.findProjectsLinkedToServerWhereVariableIsNotBound(projectId, _)
	}


	def "should get bound environment variable by entity"() {
		given:
		Long serverId = -1L
		Long evId = -5L
		EVBindableEntity serverType = EVBindableEntity.TEST_AUTOMATION_SERVER
		BoundEnvironmentVariableDto evDto = new BoundEnvironmentVariableDto()
		evDto.setId(evId)
		evDto.setInputType(EVInputType.DROPDOWN_LIST.name())

		BoundEnvironmentVariableDto evDto2 = new BoundEnvironmentVariableDto()
		evDto2.setInputType(EVInputType.PLAIN_TEXT.name())

		List<BoundEnvironmentVariableDto> dtos = Arrays.asList(evDto, evDto2)

		EnvironmentVariableOption option = new EnvironmentVariableOption("option1", 0)


		when:
		def result = service.getBoundEnvironmentVariablesByEntity(serverId, serverType)

		then:
		1 * customEnvironmentVariableBindingDao.findAllBoundEnvironmentVariables(serverId, serverType) >> dtos
		1 * environmentVariableDisplayDao.getEnvironmentVariableOptionListByEvId(evId) >> Arrays.asList(option)
		result.size() == 2
	}

	def "should bind server environment variables to project"() {
		given:
		Long serverId = -2L
		Long projectId = -5L

		EnvironmentVariable ev1 = new EnvironmentVariable("ev1", EVInputType.PLAIN_TEXT)
		EnvironmentVariable ev2 = new EnvironmentVariable("ev2", EVInputType.PLAIN_TEXT)

		EnvironmentVariableBinding projectBinding = new EnvironmentVariableBinding()
		projectBinding.setEnvironmentVariable(ev1)
		projectBinding.setValue("test")

		EnvironmentVariableBinding serverBinding = new EnvironmentVariableBinding()
		serverBinding.setEnvironmentVariable(ev1)
		serverBinding.setValue("test ev1")

		EnvironmentVariableBinding serverBinding2 = new EnvironmentVariableBinding()
		serverBinding2.setEnvironmentVariable(ev2)
		serverBinding.setValue("test ev2")

		List<EnvironmentVariableBinding> projectBindings = Arrays.asList(projectBinding)
		List<EnvironmentVariableBinding> serverBindings = Arrays.asList(serverBinding, serverBinding2)

		when:
		service.bindServerEnvironmentVariablesToProject(serverId, projectId)

		then:
		1 * environmentVariableBindingDao.findAllByEntityIdAndEntityType(projectId, _) >> projectBindings
		1 * environmentVariableBindingDao.findAllByEntityIdAndEntityType(serverId, _) >> serverBindings
		1 * environmentVariableBindingDao.saveAll(_)
	}

	def "should get bound environment variable from project view"() {
		given:
		Long projectId = -8L
		Long serverId = -4L
		Long evId = -5L
		Long evId2 = -7L


		BoundEnvironmentVariableDto evDto = new BoundEnvironmentVariableDto()
		evDto.setId(evId)
		evDto.setInputType(EVInputType.PLAIN_TEXT.name())

		BoundEnvironmentVariableDto evDto2 = new BoundEnvironmentVariableDto()
		evDto2.setId(evId2)
		evDto2.setInputType(EVInputType.PLAIN_TEXT.name())

		List<BoundEnvironmentVariableDto> projectDtos = Arrays.asList(evDto, evDto2)
		List<BoundEnvironmentVariableDto> serverDtos = Arrays.asList(evDto2)

		when:
		def result = service.getBoundEnvironmentVariableFromProjectView(projectId)

		then:
		1 * customEnvironmentVariableBindingDao.findAllBoundEnvironmentVariables(projectId, _) >> projectDtos
		1 * projectDisplayDao.getTaServerIdByProjectId(projectId) >> serverId
		1 * customEnvironmentVariableBindingDao.findAllBoundEnvironmentVariables(serverId, _) >> serverDtos
		result.size() == 2
		def boundServer = result.find {it -> it.isBoundToServer() === true }
		boundServer != null
	}
}
