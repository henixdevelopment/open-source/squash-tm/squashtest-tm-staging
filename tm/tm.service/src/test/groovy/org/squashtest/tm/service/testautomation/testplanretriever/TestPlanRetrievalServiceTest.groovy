/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever

import org.apache.commons.lang3.tuple.Triple
import org.hibernate.Session
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.customfield.CustomField
import org.squashtest.tm.domain.customfield.TagsValue
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.campaign.AutomatedExecutionCreationService
import org.squashtest.tm.service.internal.dto.AutomatedTestPlanDTO
import org.squashtest.tm.service.internal.dto.TriggerRequestDTO
import org.squashtest.tm.service.internal.dto.TriggerRequestExtendedDTO
import org.squashtest.tm.service.internal.repository.CustomFieldDao
import org.squashtest.tm.service.internal.repository.ExecutionDao
import org.squashtest.tm.service.internal.repository.IterationDao
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao
import org.squashtest.tm.service.internal.repository.ProjectDao
import org.squashtest.tm.service.internal.repository.TagDao
import org.squashtest.tm.service.internal.repository.TestSuiteDao
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao
import org.squashtest.tm.service.internal.testautomation.testplanretriever.RestAutomatedSuiteManagerService
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NoITPIFoundException
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NotFoundCustomFieldException
import spock.lang.Specification

import javax.persistence.EntityManager

/**
 *
 * @author akatz
 */
class TestPlanRetrievalServiceTest extends Specification {

    IterationTestPlanDao itpiDaoMock = Mock()
    RestAutomatedSuiteManagerService restAutomatedSuiteManagerServiceMock = Mock()
    RestTestPlanFinder restTestPlanFinderMock = Mock()
    EntityPathHeaderDao entityPathHeaderMock = Mock()
    ProjectDao projectDao = Mock()
    CustomFieldDao customFieldDao = Mock()
    TagDao tagDao= Mock()
    UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService = Mock()
    AutomatedExecutionCreationService automatedExecutionCreationServiceMock = Mock()
    EntityManager entityManagerMock = Mock()

    TestPlanRetrievalService testee = new TestPlanRetrievalService(
            itpiDao: itpiDaoMock,
            restService: restAutomatedSuiteManagerServiceMock,
            restTestPlanFinder: restTestPlanFinderMock,
            pathHeaderDao: entityPathHeaderMock,
            projectDao: projectDao,
            squashTMVersion: "7.0.0",
            cufDao: customFieldDao,
            tagDao: tagDao,
            ultimateLicenseService: ultimateLicenseAvailabilityService,
            automatedExecutionCreationService: automatedExecutionCreationServiceMock,
            entityManager: entityManagerMock
    )

    def "should get an iteration test plan from a valid trigger request"() {

        given:

        TriggerRequestDTO triggerRequest = new TriggerRequestDTO("7a687c51-9ca9-4da5-89f6-37a6e8c69879")

        Long projectId = 5L
        Project project = Mock() {
            getId() >> projectId
        }

        Iteration targetIteration = Mock(){
            getUuid() >> "1111-2222-4444"
            getId() >> 1L
            getName() >> "iterName"
            getProject() >> project
        }

        TestCase testCase = Mock()
        Execution execution = Mock() {
            getProject() >> project
            getSteps() >> Collections.emptyList()
        }

        IterationTestPlanItem callBackITPI = Mock() {
            getId() >> 42L
            getReferencedTestCase() >> testCase
            createExecution(_,_) >> execution
        }

        List<Long> itemIds = List.of(callBackITPI.getId())

        List<IterationTestPlanItem> items = List.of(callBackITPI)

        Map<String, Object> paramMap = [
                "TC_EXECUTION_ID":123L,
                "TC_REFERENCE":"",
                "TC_UUID":"346615e0-0b5b-4ade-a5f6-671ce972138c",
                "TC_SOURCE_CODE_REPOSITORY_URL":"http://repo",
                "TC_AUTOMATED_TEST_REFERENCE":"test"
        ]
        Map<String, Object> metadata = [
            "name": "TestCase",
            "reference": "test#case2",
            "uuid": "testUuid",
            "importance": "LOW"
        ]
        Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>> tuple = Mock() {
            getLeft() >> callBackITPI
            getMiddle() >> paramMap
            getRight() >> metadata
        }
        Collection<Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>> testListWithParams = [tuple]
        AutomatedSuite suite = Mock() {
            getId() >> "1234"
        }

        Map<Long, Long> map = Map.of(callBackITPI.getId(), 1L)

        and:
        ultimateLicenseAvailabilityService.isAvailable() > false
        itpiDaoMock.fetchForAutomatedExecutionCreation(_) >> items
        entityManagerMock.unwrap(_) >> Mock(Session)
        automatedExecutionCreationServiceMock.createAutomatedExecutions(_,_,_) >> map
        itpiDaoMock.fetchWithServerByIds(_) >> items
        restAutomatedSuiteManagerServiceMock.prepareExecutionOrder(items,_) >> testListWithParams
        restAutomatedSuiteManagerServiceMock.createAutomatedSuiteLinkedToIteration(targetIteration) >> suite
        entityPathHeaderMock.buildIterationPathHeader(_) >> "path > to > test"
        projectDao.fetchByIterationIdForAutomatedExecutionCreation(_) >> project

        when:
            AutomatedTestPlanDTO testPlanDTO = testee.getIterationTestPlan(triggerRequest, targetIteration, itemIds)

        then:
        testPlanDTO.executionOrderDTO.testList.size() == 1
        def test = testPlanDTO.executionOrderDTO.testList[0]
        def actualParamMap = test.paramMap
        def actualMetadata = test.metadata
        test.id == 42L
        actualParamMap.size() == 5
        actualParamMap["TC_EXECUTION_ID"] == 123L
        actualParamMap["TC_REFERENCE"] == ""
        actualParamMap["TC_UUID"] == "346615e0-0b5b-4ade-a5f6-671ce972138c"
        actualParamMap["TC_SOURCE_CODE_REPOSITORY_URL"] == "http://repo"
        actualParamMap["TC_AUTOMATED_TEST_REFERENCE"] == "test"
        actualMetadata["name"] == "TestCase"
        actualMetadata["reference"] == "test#case2"
        actualMetadata["uuid"] == "testUuid"
        actualMetadata["importance"] == "LOW"
        testPlanDTO.getTestPlanContext().getSquashTMVersion() == "7.0.0"
    }

    def "should get a test suite test plan from a valid trigger request"() {

        given:
        TriggerRequestDTO triggerRequest = new TriggerRequestDTO("7a687c51-9ca9-4da5-89f6-37a6e8c69879")
        Project project = Mock() {
            getId() >> 5L
        }

        def itemIds = List.of(42L)

        TestSuite targetTestSuite = Mock(){
            getUuid() >> "1111-2222-4444"
            getId() >> 1L
            getName() >> "suiteName"
            getProject() >> project
        }
        Execution execution = Mock() {
            getProject() >> project
            getSteps() >> Collections.emptyList()
        }
        List<IterationTestPlanItem> items = new ArrayList<>()
        TestCase testCase = Mock()
        IterationTestPlanItem callBackITPI = Mock() {
            getId() >> 42L
            getReferencedTestCase() >> testCase
            createExecution(_,_) >> execution
        }
        items.add(callBackITPI)
        Map<String, Object> paramMap = [
                "TC_EXECUTION_ID":123L,
                "TC_REFERENCE":"",
                "TC_UUID":"346615e0-0b5b-4ade-a5f6-671ce972138c",
                "TC_SOURCE_CODE_REPOSITORY_URL":"http://repo",
                "TC_AUTOMATED_TEST_REFERENCE":"test"
        ]
        Map<String, Object> metadata = [
            "name": "TestCase",
            "reference": "test#case1",
            "uuid": "testUuid",
            "importance": "MEDIUM"
        ]
        Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>> tuple = Mock() {
            getLeft() >> callBackITPI
            getMiddle() >> paramMap
            getRight() >> metadata
        }
        Collection<Triple<IterationTestPlanItem, Map<String, Object>, Map<String, Object>>> testListWithParams = [tuple]
        AutomatedSuite suite = Mock() {
            getId() >> "1234"
        }


        Map<Long, Long> map = Map.of(callBackITPI.getId(), 1L)

        and:
        restTestPlanFinderMock.findTestSuiteByUuid(_) >> targetTestSuite
        itpiDaoMock.fetchForAutomatedExecutionCreation(_) >> items
        entityManagerMock.unwrap(_) >> Mock(Session)
        automatedExecutionCreationServiceMock.createAutomatedExecutions(_,_,_) >> map
        itpiDaoMock.fetchWithServerByIds(_) >> items
        restAutomatedSuiteManagerServiceMock.prepareExecutionOrder(items,_) >> testListWithParams
        restAutomatedSuiteManagerServiceMock.createAutomatedSuiteLinkedToTestSuite(targetTestSuite) >> suite
        entityPathHeaderMock.buildTestSuitePathHeader(_) >> "path > to > suite > case"
        projectDao.fetchByTestSuiteForAutomatedExecutionCreation(_) >> project

        when:
        AutomatedTestPlanDTO testPlanDTO = testee.getTestSuiteTestPlan(triggerRequest, targetTestSuite, itemIds)

        then:
        testPlanDTO.executionOrderDTO.testList.size() == 1
        def test = testPlanDTO.executionOrderDTO.testList[0]
        def actualParamMap = test.paramMap
        def actualMetadata = test.metadata
        test.id == 42L
        actualParamMap.size() == 5
        actualParamMap["TC_EXECUTION_ID"] == 123L
        actualParamMap["TC_REFERENCE"] == ""
        actualParamMap["TC_UUID"] == "346615e0-0b5b-4ade-a5f6-671ce972138c"
        actualParamMap["TC_SOURCE_CODE_REPOSITORY_URL"] == "http://repo"
        actualParamMap["TC_AUTOMATED_TEST_REFERENCE"] == "test"
        actualMetadata["name"] == "TestCase"
        actualMetadata["reference"] == "test#case1"
        actualMetadata["uuid"] == "testUuid"
        actualMetadata["importance"] == "MEDIUM"
        testPlanDTO.getTestPlanContext().getSquashTMVersion() == "7.0.0"
    }

    // Community

    def "should get items"() {

        given:
            TriggerRequestDTO triggerRequest = new TriggerRequestDTO(
                "7a687c51-9ca9-4da5-89f6-37a6e8c69879"
            )
            List<IterationTestPlanItem> items = new ArrayList<>()
            IterationTestPlanItem callBackITPI = Mock()
            items.add(callBackITPI)
            ultimateLicenseAvailabilityService.isAvailable() >> false

        when:
            def result = testee.fetchIterationTestPlanItems(triggerRequest, _)

        then:
            result.size() == 1
            1 * itpiDaoMock.fetchForAutomatedExecutionCreation(_) >> items
    }

     //Ultimate

     def "should get iteration items from a valid trigger request, filtered by tags including an empty one"() {

         given:
         TriggerRequestExtendedDTO triggerRequest = new TriggerRequestExtendedDTO(
                 "7a687c51-9ca9-4da5-89f6-37a6e8c69879",
                 "aTagLabel",
                 " |aTagValue"
             )
             List<IterationTestPlanItem> items = new ArrayList<>()
             TestCase testCase = Mock()
             IterationTestPlanItem callBackITPI = Mock() {
                 getId() >> 42L
                 getReferencedTestCase() >> testCase
             }
             items.add(callBackITPI)
            CustomField customField = Mock()
             List<TagsValue> tagsValues= new ArrayList<>()
             TagsValue tagsValue = Mock() {
                 getValues() >> ["aTagValue"]
             }
             tagsValues.add(tagsValue)
             ultimateLicenseAvailabilityService.isAvailable() >> true


         when:
             def result = testee.fetchIterationTestPlanItems(triggerRequest, _)

         then:
             result.size() == 1
             1 * itpiDaoMock.fetchForAutomatedExecutionCreation(_) >> items
             1 * customFieldDao.findByLabelAndInputType(_,_) >> customField
             1 * tagDao.getTagsValuesByTagLabelAndTestCaseIds(_,_) >> tagsValues
     }

     def "should get iteration items order from a valid trigger request, unfiltered by any tag"() {

         given:
             TriggerRequestExtendedDTO triggerRequest = new TriggerRequestExtendedDTO(
                 "7a687c51-9ca9-4da5-89f6-37a6e8c69879",
                 "",
                 ""
             )
             List<IterationTestPlanItem> items = new ArrayList<>()
             TestCase testCase = Mock()
             IterationTestPlanItem callBackITPI = Mock() {
                 getId() >> 42L
                 getReferencedTestCase() >> testCase
             }
             items.add(callBackITPI)

             CustomField customField = Mock()
             List<TagsValue> tagsValues= new ArrayList<>()
             TagsValue tagsValue = Mock() {
                 getValues() >> ["aTagValue"]
             }
             tagsValues.add(tagsValue)
             ultimateLicenseAvailabilityService.isAvailable() >> true

         when:
             def result = testee.fetchIterationTestPlanItems(triggerRequest, _)

         then:
             result.size() == 1
             1 * itpiDaoMock.fetchForAutomatedExecutionCreation(_) >> items
             0 * customFieldDao.findByLabelAndInputType(_,_) >> customField
             0 * tagDao.getTagsValuesByTagLabelAndTestCaseIds(_,_) >> tagsValues
     }

     def "should not find any custom field corresponding to the ones specified in the trigger request"() {

         given:
             TriggerRequestExtendedDTO triggerRequest = new TriggerRequestExtendedDTO(
                 "7a687c51-9ca9-4da5-89f6-37a6e8c69879",
                 "aTagLabel",
                 "aTagValue"
             )
             List<IterationTestPlanItem> items = List.of(Mock(IterationTestPlanItem))
             ultimateLicenseAvailabilityService.isAvailable() >> true

         when:
             testee.fetchIterationTestPlanItems(triggerRequest, _)

         then:
             1 * itpiDaoMock.fetchForAutomatedExecutionCreation(_) >> items
             1 * customFieldDao.findByLabelAndInputType(_,_) >> null
             thrown NotFoundCustomFieldException
     }

}
