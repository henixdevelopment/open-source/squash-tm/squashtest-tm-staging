/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.attachment

import org.squashtest.tm.service.attachment.AttachmentFinderService
import org.squashtest.tm.service.internal.display.dto.AttachmentDto
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao
import spock.lang.Specification

class AttachmentDisplayServiceImplTest extends Specification {

	AttachmentDisplayServiceImpl attachmentDisplayService
	AttachmentDisplayDao attachmentDisplayDaoMock = Mock(AttachmentDisplayDao)
	AttachmentFinderService attachmentFinderServiceMock = Mock(AttachmentFinderService)

	def setup() {
		attachmentDisplayService = new AttachmentDisplayServiceImpl(
				attachmentDisplayDaoMock,
				attachmentFinderServiceMock
		)
	}

	def "getAttachmentList(long): [Nominal] Should get the required AttachmentList with some Attachments"() {
		given:
		long requestedId = 8

		def expectedAttachments = [new AttachmentDto(), new AttachmentDto(), new AttachmentDto()]
		def expectedAttachmentList = new AttachmentListDto(id: requestedId, attachments: expectedAttachments)

		and:
		attachmentDisplayDaoMock.findAttachmentListById(requestedId) >> expectedAttachmentList

		when:
		def resultAttachmentList = attachmentDisplayService.getAttachmentList(requestedId)

		then:
		resultAttachmentList != null
		resultAttachmentList.getId() == requestedId
		resultAttachmentList.getAttachments() != null
		resultAttachmentList.getAttachments() == expectedAttachments
	}

	def "getAttachmentList(long): [Empty] Should get the required AttachmentList with no Attachments"() {
		given:
		long requestedId = 8
		def expectedAttachments = []
		def expectedAttachmentList = new AttachmentListDto(id: requestedId, attachments: expectedAttachments)

		and:
		attachmentDisplayDaoMock.findAttachmentListById(requestedId) >> expectedAttachmentList

		when:
		def resultAttachmentList = attachmentDisplayService.getAttachmentList(requestedId)

		then:
		resultAttachmentList != null
		resultAttachmentList.getId() == requestedId
		resultAttachmentList.getAttachments() != null
		resultAttachmentList.getAttachments() == expectedAttachments
	}

	def "getAttachmentList(long): [Null] Should return null because the AttachmentList does not exist"() {
		given:
		long nonExistentId = 404

		and:
		attachmentDisplayDaoMock.findAttachmentListById(nonExistentId) >> null

		when:
		def resultAttachmentList = attachmentDisplayService.getAttachmentList(nonExistentId)

			then:
		resultAttachmentList == null
	}

}
