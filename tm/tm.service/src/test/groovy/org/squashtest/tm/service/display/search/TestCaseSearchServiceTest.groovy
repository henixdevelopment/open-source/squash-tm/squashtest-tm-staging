/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.jooq.DSLContext
import org.springframework.security.access.AccessDeniedException
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.search.TestCaseSearchServiceImpl
import org.squashtest.tm.service.internal.display.search.filter.FilterHandlers
import org.squashtest.tm.service.internal.display.search.filter.FilterValueHandlers
import org.squashtest.tm.service.internal.repository.ColumnPrototypeDao
import org.squashtest.tm.service.project.ProjectFinder
import org.squashtest.tm.service.query.QueryProcessingService
import spock.lang.Specification

import javax.persistence.EntityManager

class TestCaseSearchServiceTest extends Specification {

	QueryProcessingService queryService = Mock()
	ColumnPrototypeDao columnPrototypeDao = Mock()
	EntityManager entityManager = Mock()
  	ProjectFinder projectFinder = Mock()
	FilterHandlers filterHandlers = Mock()
	FilterValueHandlers gridFilterValueHandlers = Mock()
	DSLContext dslContext = Mock()

	TestCaseSearchService testCaseSearchService = new TestCaseSearchServiceImpl(
		queryService,
		columnPrototypeDao,
		entityManager,
		projectFinder,
		filterHandlers,
		gridFilterValueHandlers,
		dslContext
	)

	def "should denied search access to user without permissions"() {
		given:
		projectFinder.canReadAtLeastOneProject() >> false

		when:
		testCaseSearchService.search(new GridRequest())

		then:
		thrown(AccessDeniedException)
	}
}
