/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.annotation

import org.aspectj.lang.ProceedingJoinPoint
import spock.lang.Specification
import org.springframework.security.access.AccessDeniedException

class IsUltimateLicenseAvailableAspectTest extends Specification {

    IsUltimateLicenseAvailableAspect isUltimateLicenseAvailableAspect = Spy()

    def "should throw AccessDeniedException when ultimate license is not available"() {
        given:
        ProceedingJoinPoint joinPoint = Mock()
        isUltimateLicenseAvailableAspect.isUltimateLicenseAvailable() >> false

        when:
        isUltimateLicenseAvailableAspect.checkUltimateLicense(joinPoint)

        then:
        def thrownException = thrown(AccessDeniedException)
        thrownException.message == "Forbidden 403 Ultimate License Not Available"
    }

    def "should proceed without throwing exception when ultimate license is available"() {
        given:
        ProceedingJoinPoint joinPoint = Mock()

        isUltimateLicenseAvailableAspect.isUltimateLicenseAvailable() >> true

        when:
        isUltimateLicenseAvailableAspect.checkUltimateLicense(joinPoint)

        then:
        notThrown(AccessDeniedException)
    }
}
