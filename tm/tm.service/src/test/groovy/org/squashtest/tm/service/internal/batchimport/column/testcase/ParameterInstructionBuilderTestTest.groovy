/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.column.testcase

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.squashtest.tm.service.importer.ImportMode
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction
import org.squashtest.tm.service.internal.batchimport.column.StdColumnDef
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet
import org.squashtest.tm.service.internal.batchimport.column.WorksheetDef
import spock.lang.Specification
import spock.lang.Unroll

/**
 * @author Gregory Fouquet
 *
 */
class ParameterInstructionBuilderTest extends Specification {
	WorksheetDef wd = Mock();
	Row row = Mock()
	ParameterInstructionBuilder builder

	def setup() {
		wd.getWorksheetType() >> TemplateWorksheet.PARAMETERS_SHEET
		builder = new ParameterInstructionBuilder(wd)
	}

	private Cell mockCell(cellType, cellValue) {
		Cell cell = Mock()

		cell.getCellType() >> cellType

		cell.getNumericCellValue() >> cellValue
		cell.getStringCellValue() >> cellValue
		cell.getBooleanCellValue() >> cellValue
		cell.getDateCellValue() >> cellValue

		return cell
	}

	@Unroll
	def "should create test step target from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		ParameterInstruction instruction = builder.build(row)

		then:
		instruction.target[propName] == propValue

		where:
		col				                    | cellType					| cellValue			| propName			| propValue
        ParameterSheetColumn.TC_OWNER_PATH	| CellType.STRING		    | "here/i/am"		| "path"			| "here/i/am/parameters/null"
        ParameterSheetColumn.TC_OWNER_PATH	| CellType.BLANK		    | null				| "path"			| "null/parameters/null"

	}

	@Unroll
	def "should create test step instruction from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		ParameterInstruction instruction = builder.build(row)

		then:
		instruction[propName] == propValue

		where:
		col				                    | cellType					| cellValue			| propName			| propValue
        ParameterSheetColumn.ACTION			| CellType.STRING		    | "CREATE"			| "mode"			| ImportMode.CREATE
        ParameterSheetColumn.ACTION			| CellType.STRING		    | "C"				| "mode"			| ImportMode.CREATE
        ParameterSheetColumn.ACTION			| CellType.BLANK		    | null				| "mode"			| ImportMode.UPDATE

	}

	@Unroll
	def "should create action test step from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		ParameterInstruction instruction = builder.build(row)

		then:
		instruction.parameter[propName] == propValue

		where:
		col						                    | cellType           | cellValue                 | propName      | propValue
        ParameterSheetColumn.TC_PARAM_NAME			| CellType.STRING    | "my name is luka"         | "name"        | "my name is luka"
        ParameterSheetColumn.TC_PARAM_NAME			| CellType.BLANK     | null                      | "name"        | null

        ParameterSheetColumn.TC_PARAM_DESCRIPTION	| CellType.STRING    | "i live on the 2nd floor" | "description" | "i live on the 2nd floor"
        ParameterSheetColumn.TC_PARAM_DESCRIPTION	| CellType.BLANK     | null                      | "description" | null

	}

}
