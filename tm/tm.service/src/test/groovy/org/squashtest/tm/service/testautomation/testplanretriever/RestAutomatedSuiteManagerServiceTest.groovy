/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever

import org.apache.commons.lang3.tuple.Triple
import org.springframework.security.access.AccessDeniedException
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao
import org.squashtest.tm.service.internal.testautomation.testplanretriever.AutomatedTestBuilderService
import org.squashtest.tm.service.internal.testautomation.testplanretriever.RestAutomatedSuiteManagerService
import org.squashtest.tm.service.security.PermissionEvaluationService
import spock.lang.Specification

class RestAutomatedSuiteManagerServiceTest<C extends CustomFieldValuesForExec> extends Specification {

    AutomatedSuiteDao restAutomatedSuiteDaoMock = Mock()
    PermissionEvaluationService permissionServiceMock = Mock()
    AutomatedTestBuilderService<C> testBuilderMock = Mock()
    EntityPathHeaderDao pathHeaderDaoMock = Mock()

    RestAutomatedSuiteManagerService<C> testee = new RestAutomatedSuiteManagerService<>(
            automatedSuiteDao: restAutomatedSuiteDaoMock,
            permissionService: permissionServiceMock,
            testBuilder: testBuilderMock,
            pathHeaderDao: pathHeaderDaoMock
    )

    def "should create a list of tests with parameters given a list of ITPIs"() {

        given:
        Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>> testWithParam = Mock()
        Collection<Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>>> targetTestListWithParams =
                new ArrayList<Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>>>()
        targetTestListWithParams.add(testWithParam)
        def id = 1L
        IterationTestPlanItem item = Mock()

        item.getReferencedTestCase() >> Mock(TestCase) {
            getId() >> id
        }
        List<IterationTestPlanItem> items = new ArrayList<>()
        items.add(item)
        C customFieldValuesForExec = Mock()
        Map<Long, Long> itemExecutionMap = Mock()

        and:
        permissionServiceMock.hasRole(_) >> true
        testBuilderMock.fetchCustomFieldValues(_) >> customFieldValuesForExec
        testBuilderMock.createTestWithParams(_,_,_,_, _) >> testWithParam
        pathHeaderDaoMock.buildTestCasePathByIds(_) >> Map.of(id, "path > foo")

        when:
        Collection<Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>>> testListWithParams =
                testee.prepareExecutionOrder(items, itemExecutionMap)

        then:
        testListWithParams == targetTestListWithParams
    }

    def "should be denied access for lack of permission"() {

        given:
        Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>> testWithParam = Mock()
        Collection<Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>>> targetTestListWithParams =
                new ArrayList<Triple<IterationTestPlanItem, Map<String, Object>,Map<String, Object>>>()
        targetTestListWithParams.add(testWithParam)
        IterationTestPlanItem item = Mock()
        List<IterationTestPlanItem> items = new ArrayList<>()
        items.add(item)
        Map<Long, Long> itemExecutionMap = Mock()

        and:
        permissionServiceMock.hasRole(_) >> false

        when:
        testee.prepareExecutionOrder(items, itemExecutionMap)

        then:
        thrown AccessDeniedException
    }

    def "should create an automated suite linked to an iteration"() {

        given:
        AutomatedSuite targetSuite = Mock()
        Iteration iteration = Mock()

        and:
        restAutomatedSuiteDaoMock.createNewSuiteWithLinkToIteration(_) >> targetSuite
        restAutomatedSuiteDaoMock.findByIdWithExtenders(_) >> targetSuite

        when:
        AutomatedSuite suite = testee.createAutomatedSuiteLinkedToIteration(iteration)

        then:
        suite == targetSuite
    }

    def "should create an automated suite linked to a test suite"() {

        given:
        AutomatedSuite targetSuite = Mock()
        TestSuite testSuite = Mock()

        and:
        restAutomatedSuiteDaoMock.createNewSuiteWithLinkToTestSuite(_) >> targetSuite
        restAutomatedSuiteDaoMock.findByIdWithExtenders(_) >> targetSuite

        when:
        AutomatedSuite suite = testee.createAutomatedSuiteLinkedToTestSuite(testSuite)

        then:
        suite == targetSuite
    }
}
