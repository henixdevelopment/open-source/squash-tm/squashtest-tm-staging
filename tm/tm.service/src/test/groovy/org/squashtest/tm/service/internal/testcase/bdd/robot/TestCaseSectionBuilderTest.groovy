/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot

import org.squashtest.tm.domain.bdd.ActionWord
import org.squashtest.tm.domain.bdd.ActionWordParameterValue
import org.squashtest.tm.domain.bdd.ActionWordText
import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.project.AutomationWorkflowType
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.DatasetParamValue
import org.squashtest.tm.domain.testcase.KeywordTestCase
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.domain.testcase.Parameter
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest
import spock.lang.Specification
import spock.lang.Unroll

import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuildersTestUtils.createActionWordStep
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuildersTestUtils.createBasicActionWord

class TestCaseSectionBuilderTest extends Specification {

	Project project = new Project()

	KeywordTestCase keywordTestCase

	def "setup"() {
		keywordTestCase = Spy(KeywordTestCase)
		keywordTestCase.getId() >> 341L
		keywordTestCase.notifyAssociatedWithProject(project)

		keywordTestCase.automationRequest = new AutomationRequest()
		keywordTestCase.automationRequest.automationPriority = 42

		project.setAutomationWorkflowType(AutomationWorkflowType.NATIVE)
	}

	def "without test steps"() {
		given:
		keywordTestCase.setName("Disconnection test")

		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)

		then:
		result ==
			"""*** Test Cases ***
Disconnection test
    [Documentation]    Disconnection test


"""
	}

	def "with only text from a KeywordTestCase and no automation workflow"() {
		given:
		keywordTestCase.setName("Disconnection test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("I am connécted"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, createBasicActionWord("I sign oùt"))
		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am dîsconnect&d"))

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)

		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Disconnection test
    [Documentation]    Disconnection test

    Given I am connécted
    When I sign oùt
    Then I am dîsconnect&d


"""
	}

	def "with parameter value as free text from a KeywordTestCase"() {
		given:
		keywordTestCase.setName("Daily test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))

		def fragment1 = new ActionWordText("It is ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
		def value1 = new ActionWordParameterValue("10 o'clock")
		value1.setActionWordParam(fragment2)
		ActionWord actionWord2 = new ActionWord([fragment1, fragment2] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues = [value1]
		step2.setParamValues(paramValues)

		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am working"))
		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    Given Today is Monday
    When It is "10 o'clock"
    Then I am working


"""
	}

	@Unroll
	def "with parameter value as a number"() {
		given:
		keywordTestCase.setName("Daily test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))

		def fragment1 = new ActionWordText("It is ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
		def value1 = new ActionWordParameterValue(word)
		value1.setActionWordParam(fragment2)
		ActionWord actionWord2 = new ActionWord([fragment1, fragment2] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues = [value1]
		step2.setParamValues(paramValues)

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    Given Today is Monday
    When It is \"${word}\"


"""
		where:
		word << ["10", "10.5", "10,5", "-10.5", "-10,5"]
	}

	def "with TC param but no dataset"() {
		given:
		keywordTestCase.setName("Daily test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))

		def fragment1 = new ActionWordText("It is ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
		def value1 = new ActionWordParameterValue("<time>")
		value1.setActionWordParam(fragment2)
		ActionWord actionWord2 = new ActionWord([fragment1, fragment2] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues = [value1]
		step2.setParamValues(paramValues)

		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am working"))

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]"
    Then I am working


"""
	}

	def "with dataset but no param between <>"() {
		given:
		keywordTestCase.setName("Daily test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))

		def fragment1 = new ActionWordText("It is ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
		def value1 = new ActionWordParameterValue("time")
		value1.setActionWordParam(fragment2)
		ActionWord actionWord2 = new ActionWord([fragment1, fragment2] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues = [value1]
		step2.setParamValues(paramValues)

		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am working"))

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)

		def tcParam =  new Parameter("tcParam")
		keywordTestCase.getParameters() >> [tcParam]
		def dataset =  new Dataset()
		dataset.setName("dataset1")

		keywordTestCase.addDataset(dataset)

		def value =  new DatasetParamValue(tcParam, dataset,"9 AM")
		dataset.addParameterValue(value)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    Given Today is Monday
    When It is "time"
    Then I am working


"""
	}

	def "with dataset, but no parameter at all"() {
		given:
		keywordTestCase.setName("Daily test")
		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am working"))
		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		and:
		def tcParam =  new Parameter("tcParam")
		keywordTestCase.getParameters() >> [tcParam]
		def dataset =  new Dataset()
		dataset.setName("dataset1")
		def value =  new DatasetParamValue(tcParam, dataset,"9 AM")
		dataset.addParameterValue(value)
		keywordTestCase.addDataset(dataset)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    Given Today is Monday
    Then I am working


"""
	}

	def "with 1 dataset and 1 param between <>"() {
		given:
		keywordTestCase.setName("Daily test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))

		def fragment1 = new ActionWordText("It is ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
		def value1 = new ActionWordParameterValue("<time>")
		value1.setActionWordParam(fragment2)
		ActionWord actionWord2 = new ActionWord([fragment1, fragment2] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues = [value1]
		step2.setParamValues(paramValues)

		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am working"))

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)

		def tcParam =  new Parameter("time", keywordTestCase)
		def dataset =  new Dataset()
		dataset.setName("dataset1")

		keywordTestCase.addDataset(dataset)

		def value =  new DatasetParamValue(tcParam, dataset,"9 AM")
		dataset.addParameterValue(value)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]"
    Then I am working


"""
	}

	def "with 1 dataset and 1 param between <> without escaping the arrow symbols"() {
		given:
		keywordTestCase.setName("Daily test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))

		def fragment1 = new ActionWordText("It is ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
		def value1 = new ActionWordParameterValue("<time>")
		value1.setActionWordParam(fragment2)
		ActionWord actionWord2 = new ActionWord([fragment1, fragment2] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues = [value1]
		step2.setParamValues(paramValues)

		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am working"))

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)

		def tcParam =  new Parameter("time", keywordTestCase)
		def dataset =  new Dataset()
		dataset.setName("dataset1")

		keywordTestCase.addDataset(dataset)

		def value =  new DatasetParamValue(tcParam, dataset,"9 AM")
		dataset.addParameterValue(value)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]"
    Then I am working


"""
	}

	def "with 1 dataset and 2 param between <>"() {
		given:
		keywordTestCase.setName("Daily test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("Today is Monday"))

		def fragment1 = new ActionWordText("It is ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "12 o'clcock")
		def value1 = new ActionWordParameterValue("<time>")
		value1.setId(-2L)
		value1.setActionWordParam(fragment2)
		def fragment3 = new ActionWordText(" in ")
		def fragment4 = new ActionWordParameterMock(-2L, "param2", "Paris")
		def value2 = new ActionWordParameterValue("<place>")
		value2.setId(-1L)
		value2.setActionWordParam(fragment4)
		ActionWord actionWord2 = new ActionWord([fragment1, fragment2, fragment3, fragment4] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues = [value1, value2]
		step2.setParamValues(paramValues)

		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I am working"))

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)

		def tcParam1 =  new Parameter("time", keywordTestCase)
		def tcParam2 =  new Parameter("place", keywordTestCase)
		def dataset =  new Dataset()
		dataset.setName("dataset1")

		keywordTestCase.addDataset(dataset)

		def paramValue1 =  new DatasetParamValue(tcParam1, dataset,"9 AM")
		def paramValue2 =  new DatasetParamValue(tcParam2, dataset,"London")
		dataset.parameterValues = [paramValue1, paramValue2]
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]" in "\${dataset}[place]"
    Then I am working


"""
	}

	def "with 1 dataset and 2 param between <> with values as number"() {
		given:
		keywordTestCase.setName("Count test")

		def fragment1 = new ActionWordText("I buy ")
		def fragment2 = new ActionWordParameterMock(-1L, "param1", "10")
		def value1 = new ActionWordParameterValue("<total>")
		value1.setId(-1L)
		value1.setActionWordParam(fragment2)
		def fragment3 = new ActionWordText(" tickets")

		ActionWord actionWord1 = new ActionWord([fragment1, fragment2, fragment3] as List)
		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, actionWord1)
		List<ActionWordParameterValue> paramValues1 = [value1]
		step1.setParamValues(paramValues1)

		def fragment4 = new ActionWordText("I give ")
		def fragment5 = new ActionWordParameterMock(-2L, "param1", "10")
		def value2 = new ActionWordParameterValue("<less>")
		value2.setId(-2L)
		value2.setActionWordParam(fragment5)
		def fragment6 = new ActionWordText(" to my friend")

		ActionWord actionWord2 = new ActionWord([fragment4, fragment5, fragment6] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues2 = [value2]
		step2.setParamValues(paramValues2)

		def fragment7 = new ActionWordText("I still have ")
		def fragment8 = new ActionWordParameterMock(-3L, "param1", "10")
		def value3 = new ActionWordParameterValue("<left>")
		value3.setId(-3L)
		value3.setActionWordParam(fragment8)
		def fragment9 = new ActionWordText(" tickets")

		ActionWord actionWord3 = new ActionWord([fragment7, fragment8, fragment9] as List)
		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, actionWord3)
		List<ActionWordParameterValue> paramValues3 = [value3]
		step3.setParamValues(paramValues3)

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)

		def tcParam1 =  new Parameter("total", keywordTestCase)
		def tcParam2 =  new Parameter("less", keywordTestCase)
		def tcParam3 =  new Parameter("left", keywordTestCase)
		def dataset =  new Dataset()
		dataset.setName("dataset1")

		keywordTestCase.addDataset(dataset)

		def paramValue1 =  new DatasetParamValue(tcParam1, dataset,"5")
		def paramValue2 =  new DatasetParamValue(tcParam2, dataset,"3")
		def paramValue3 =  new DatasetParamValue(tcParam3, dataset,"two")
		dataset.parameterValues = [paramValue1, paramValue2, paramValue3]
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Count test
    [Documentation]    Count test

    &{dataset} =    Retrieve Dataset

    Given I buy "\${dataset}[total]" tickets
    When I give "\${dataset}[less]" to my friend
    Then I still have "\${dataset}[left]" tickets


"""
	}

	def "with 1 dataset and 2 param between <> used twice"() {
		given:
		keywordTestCase.setName("Working test")

		def fragment1 = new ActionWordText("it is Monday")

		ActionWord actionWord1 = new ActionWord([fragment1] as List)
		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, actionWord1)

		def fragment4 = new ActionWordText("it is ")
		def fragment5 = new ActionWordParameterMock(-2L, "param1", "10")
		def value2 = new ActionWordParameterValue("<time>")
		value2.setId(-2L)
		value2.setActionWordParam(fragment5)
		def fragment6 = new ActionWordText(" in ")
		def fragment2 = new ActionWordParameterMock(-1L, "param2", "Paris")
		def value1 = new ActionWordParameterValue("<place>")
		value1.setId(-1L)
		value1.setActionWordParam(fragment2)

		ActionWord actionWord2 = new ActionWord([fragment4, fragment5, fragment6, fragment2] as List)
		KeywordTestStep step2 = new KeywordTestStep(Keyword.WHEN, actionWord2)
		List<ActionWordParameterValue> paramValues2 = [value2, value1]
		step2.setParamValues(paramValues2)

		def fragment7 = new ActionWordText("I work at ")
		def fragment8 = new ActionWordParameterMock(-3L, "param1", "10")
		def value3 = new ActionWordParameterValue("<time>")
		value3.setActionWordParam(fragment8)
		value3.setId(-3L)
		def fragment9 = new ActionWordText(" in ")
		def fragment10 = new ActionWordParameterMock(-4L, "param2", "Paris")
		def value4 = new ActionWordParameterValue("<place>")
		value4.setActionWordParam(fragment10)
		value4.setId(-4L)

		ActionWord actionWord3 = new ActionWord([fragment7, fragment8, fragment9, fragment10] as List)
		KeywordTestStep step3 = new KeywordTestStep(Keyword.THEN, actionWord3)
		List<ActionWordParameterValue> paramValues3 = [value3, value4]
		step3.setParamValues(paramValues3)

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)

		def tcParam1 =  new Parameter("time", keywordTestCase)
		def tcParam2 =  new Parameter("place", keywordTestCase)
		def dataset =  new Dataset()
		dataset.setName("  dataset   1    ")

		keywordTestCase.addDataset(dataset)

		def paramValue1 =  new DatasetParamValue(tcParam1, dataset,"6AM")
		def paramValue2 =  new DatasetParamValue(tcParam2, dataset,"London")
		dataset.parameterValues = [paramValue1, paramValue2]
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Working test
    [Documentation]    Working test

    &{dataset} =    Retrieve Dataset

    Given it is Monday
    When it is "\${dataset}[time]" in "\${dataset}[place]"
    Then I work at "\${dataset}[time]" in "\${dataset}[place]"


"""
	}

	def "with escaped delimiters in steps"() {
		given:
		keywordTestCase.setName("Escaped delimiters in steps")

		KeywordTestStep step1 = createActionWordStep(Keyword.WHEN, "I put a \\<br/\\> in my HTML page")
		KeywordTestStep step2 = createActionWordStep(Keyword.THEN, "my lead tech says \"Don't use \\<br/\\>, that's \\\"bad_practice\\\"\"")
		KeywordTestStep step3 = createActionWordStep(Keyword.AND, "I reply \\\"No problem, I can use a <paragraph> instead\\\"")

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Escaped delimiters in steps
    [Documentation]    Escaped delimiters in steps

    &{dataset} =    Retrieve Dataset

    When I put a <br/> in my HTML page
    Then my lead tech says "Don't use <br/>, that's "bad_practice""
    And I reply "No problem, I can use a "\${dataset}[paragraph]" instead"


"""
	}

	def "with 2 datatables"() {
		given:
		keywordTestCase.setName("User table test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.WHEN, createBasicActionWord("I am on user page"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I can see the users"))
		step2.setDatatable("| Henry | Dupond | henry.dupond@mail.com |\n" +
			"| Louis | Dupont | louis.dupont@mail.com |\n" +
			"| Charles | Martin | charles.martin@mail.com |")
		KeywordTestStep step3 = new KeywordTestStep(Keyword.AND, createBasicActionWord("I see the administrator"))
		step3.setDatatable("| Bruce | Wayne | batman@mail.com |\n" +
			"| Peter | Parker | spiderman@mail.com |\n" +
			"| Clark | Kent | superman@mail.com |")

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		keywordTestCase.addStep(step3)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
User table test
    [Documentation]    User table test

    &{datatables} =    Retrieve Datatables

    When I am on user page
    Then I can see the users "\${datatables}[datatable_1]"
    And I see the administrator "\${datatables}[datatable_2]"


"""
	}

	def "with 1 docstring"() {
		given:
		keywordTestCase.setName("Letter test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("following letter is displayed"))
		step1.setDocstring("\tDear Jack,\n" +
			"I have arrived in London this morning. Everything went well!\n" +
			"Looking forward to seeing you on Friday.\n" +
			"\n\tYour friend, John.")

		keywordTestCase.addStep(step1)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Letter test
    [Documentation]    Letter test

    &{docstrings} =    Retrieve Docstrings

    Given following letter is displayed "\${docstrings}[docstring_1]"


"""
	}

	def "with 2 docstrings"() {
		given:
		keywordTestCase.setName("Letter test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("following letter is displayed"))
		step1.setDocstring("\tDear Jack,\n" +
			"I have arrived in London this morning. Everything went well!\n" +
			"Looking forward to seeing you on Friday.\n" +
			"\n\tYour friend, John.")
		KeywordTestStep step2 = new KeywordTestStep(Keyword.AND, createBasicActionWord("following letter is displayed"))
		step2.setDocstring("\tDear Jack,\n" +
			"I have arrived in London this morning. Everything went well!\n" +
			"Looking forward to seeing you on Tuesday.\n" +
			"\n\tYour friend, John.")

		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Letter test
    [Documentation]    Letter test

    &{docstrings} =    Retrieve Docstrings

    Given following letter is displayed "\${docstrings}[docstring_1]"
    And following letter is displayed "\${docstrings}[docstring_2]"


"""
	}

	def "with both datatable and docstring on 1 step"() {
		given:
		keywordTestCase.setName("User table test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.WHEN, createBasicActionWord("I am on user page"))
		KeywordTestStep step2 = new KeywordTestStep(Keyword.THEN, createBasicActionWord("I can see the users"))
		step2.setDatatable("| Henry | Dupond | henry.dupond@mail.com |\n" +
			"| Louis | Dupont | louis.dupont@mail.com |\n" +
			"| Charles | Martin | charles.martin@mail.com |")
		step2.setDocstring("\tDear Mommy,\nI am fine, thank You!\n\tYour Son.")
		keywordTestCase.addStep(step1)
		keywordTestCase.addStep(step2)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
User table test
    [Documentation]    User table test

    &{datatables} =    Retrieve Datatables

    When I am on user page
    Then I can see the users "\${datatables}[datatable_1]"


"""
	}

	def "with 1 comment"() {
		given:
		keywordTestCase.setName("Comment test")

		KeywordTestStep step1 = new KeywordTestStep(Keyword.GIVEN, createBasicActionWord("I do something"))
		step1.setComment("the action can be anything here\nANYTHING !")

		keywordTestCase.addStep(step1)
		when:
		String result = TestCaseSectionBuilder.buildTestCasesSection(keywordTestCase)
		then:
		result ==
			"""*** Test Cases ***
Comment test
    [Documentation]    Comment test

    Given I do something
    # the action can be anything here
    # ANYTHING !


"""
	}
}
