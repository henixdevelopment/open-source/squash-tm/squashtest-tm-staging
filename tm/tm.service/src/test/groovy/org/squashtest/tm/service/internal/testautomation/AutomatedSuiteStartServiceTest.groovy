/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation

import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testautomation.*
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.servers.CredentialsProvider
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration
import org.squashtest.tm.service.testautomation.model.SuiteExecutionConfiguration
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector
import org.squashtest.tm.service.testautomation.spi.UnknownConnectorKind
import spock.lang.Specification

import javax.inject.Provider

class AutomatedSuiteStartServiceTest extends Specification {

    PermissionEvaluationService permissionService = Mock()
    TestAutomationConnectorRegistry connectorRegistry = Mock()
    AutomatedSuiteDao automatedSuiteDao = Mock()
    Provider<TaParametersBuilder> parametersBuilderProvider = Mock()
    CustomFieldValueFinderService customFieldValueFinder = Mock()
    CredentialsProvider credentialsProvider = Mock()

    AutomatedSuiteStartService service = new AutomatedSuiteStartService(
        permissionService,
        connectorRegistry,
        automatedSuiteDao,
        parametersBuilderProvider,
        customFieldValueFinder,
        credentialsProvider
    )

    def setup() {
        parametersBuilderProvider.get() >> { return new TaParametersBuilder() }
        permissionService.hasRoleOrPermissionOnObject(_, _, _) >> true
        permissionService.hasRoleOrPermissionOnObject(_, _, _, _) >> true
        permissionService.hasRole(_) >> true
    }

    def "should start some tests"(){
        given :
        AutomatedSuite suite = mockAutomatedSuite()

        and :
        def jenConnector = Mock(TestAutomationConnector)
        def automConnector = Mock(TestAutomationConnector)

        and:
        automatedSuiteDao.findAndFetchForAutomatedExecutionCreation(_) >> []
        customFieldValueFinder.findAllCustomFieldValues(_) >> []

        when :
        service.start(suite, [])

        then :

        1 * connectorRegistry.getConnectorForKind(TestAutomationServerKind.jenkins) >> jenConnector
        1 * connectorRegistry.getConnectorForKind(TestAutomationServerKind.squashOrchestrator) >> automConnector
        1 * jenConnector.executeParameterizedTests(_, "12345")
        1 * automConnector.executeParameterizedTests(_, "12345")

    }

    def "should start some tests with Squash TF and Squash AUTOM executions"(){
        given :
        AutomatedSuiteWithSquashAutomAutomatedITPIs suite = mockAutomatedSuiteWithSquashAutomAutomatedITPIs()

        and :
        def jenConnector = Mock(TestAutomationConnector)
        def squashautomConnector = Mock(TestAutomationConnector)

        and:
        Collection<SuiteExecutionConfiguration> jenkinsConfiguration = []
        Collection<SquashAutomExecutionConfiguration> squashAutomConfiguration = []

        and:
        automatedSuiteDao.findAndFetchForAutomatedExecutionCreation(_) >> []
        customFieldValueFinder.findAllCustomFieldValues(_) >> []
        def workflows = Mock(AutomatedSuiteWorkflow)

        when :
        service.start(suite, jenkinsConfiguration, squashAutomConfiguration)

        then :
        1 * connectorRegistry.getConnectorForKind(TestAutomationServerKind.jenkins) >> jenConnector
        2 * connectorRegistry.getConnectorForKind(TestAutomationServerKind.squashOrchestrator) >> squashautomConnector
        1 * jenConnector.executeParameterizedTests(_, "12345")
        1 * squashautomConnector.executeParameterizedTestsBasedOnITPICollection(_, "12345", _) >> List.of(workflows)
    }

    def "should notify some executions that an error occured before they could start"(){
        given :
        AutomatedSuite suite = mockAutomatedSuite()

        suite.executionExtenders.each{
            def exec = new Execution()
            exec.automatedExecutionExtender = it
        }

        and :
        def jenConnector = Mock(TestAutomationConnector)

        connectorRegistry.getConnectorForKind(TestAutomationServerKind.jenkins) >> jenConnector
        connectorRegistry.getConnectorForKind(TestAutomationServerKind.squashOrchestrator) >> { throw new UnknownConnectorKind("connector unknown") }

        and:
        automatedSuiteDao.findAndFetchForAutomatedExecutionCreation(_) >> suite.executionExtenders
        customFieldValueFinder.findAllCustomFieldValues(_) >> []

        and:
        def errors = 0
        suite.executionExtenders.each { it.setExecutionStatus(_) >> { st -> st == ExecutionStatus.ERROR ?: errors++ } }

        when :
        service.start(suite, [])

        then :
        1 * jenConnector.executeParameterizedTests(_, "12345")
        errors == 6
    }

    def mockAutomatedSuiteWithSquashAutomAutomatedITPIs(){
        AutomatedSuite suite = mockAutomatedSuite()
        List<IterationTestPlanItem> squashAutomItems = mockITPIListSquashAutom()
        return new AutomatedSuiteWithSquashAutomAutomatedITPIs(suite, squashAutomItems)
    }

    def mockAutomatedSuite() {

        AutomatedSuite suite = new AutomatedSuite()
        suite.id = "12345"
        suite.iteration = mockIteration()

        TestAutomationServer serverJenkins = new TestAutomationServer(TestAutomationServerKind.jenkins)
        serverJenkins.setName("thejenkins")
        serverJenkins.setUrl("http://jenkins-ta")
        TestAutomationServer serverAutom = new TestAutomationServer(TestAutomationServerKind.squashOrchestrator)
        serverAutom.setName("theAutom")
        serverAutom.setUrl("http://autom")

        TestAutomationProject projectJ1 = new TestAutomationProject("project-jenkins-1", serverJenkins)
        TestAutomationProject projectAutom1 = new TestAutomationProject("project-autom-1", serverAutom)
        TestAutomationProject projectJ2 = new TestAutomationProject("project-jenkins-2", serverJenkins)

        def allTests = []

        def projects = [
            projectJ1,
            projectAutom1,
            projectJ2
        ]

        projects.each { proj ->

            5.times { num ->

                AutomatedTest test = new AutomatedTest("${proj.jobName} - test $num", proj)
                allTests << test
            }
        }

        suite.addExtenders(
            projects.collect { proj ->
                // returns list of lists of exts
                return (0..5).collect { // returns list of exts
                    mockExtender()
                }
                    .eachWithIndex { extender, num ->

                        // performs stuff on exts and returns exts
                        extender.getAutomatedProject() >> proj
                        def autotest = extender.getAutomatedTest()
                        autotest.getProject() >> proj
                        autotest.getName() >> "${proj.jobName} - test $num"
                    }
            }.flatten()
        )

        return suite
    }

    private List<IterationTestPlanItem> mockITPIListSquashAutom() {
        List<IterationTestPlanItem> itpiList = new ArrayList<>()
        5.times { num -> itpiList.add(mockITPISquashAutom()) }
        return itpiList
    }

    private AutomatedExecutionExtender mockExtender() {
        AutomatedExecutionExtender extender = Mock()

        AutomatedTest automatedTest = Mock()
        extender.getAutomatedTest() >> automatedTest
        Execution exec = Mock()
        Iteration iteration = Mock()

        exec.iteration >> iteration
        def iterationTestPlanItem = Mock(IterationTestPlanItem)
        exec.testPlan >> iterationTestPlanItem
        exec.campaign >> Mock(Campaign)
        iterationTestPlanItem.getIteration() >> iteration
        iterationTestPlanItem.getTestSuites() >> new ArrayList<TestSuite>()
        iteration.getTestSuites() >> new ArrayList<TestSuite>()

        extender.getExecution() >> exec
        extender.getIterationTestPlanItem() >> iterationTestPlanItem

        TestCase tc = Mock()
        tc.uuid >> "44d63d7e-11dd-44b0-b584-565b6f791fa9"

        exec.referencedTestCase >> tc

        return extender
    }

    private IterationTestPlanItem mockITPISquashAutom() {
        IterationTestPlanItem itpi = Mock()

        TestAutomationServer serverSquashAutom = new TestAutomationServer(TestAutomationServerKind.squashOrchestrator)
        serverSquashAutom.setName("theSquashAutom")
        serverSquashAutom.setUrl("http://squashautom-ta")

        Project mockProject = Mock()
        mockProject.getId() >> 2L
        mockProject.getTestAutomationServer() >> serverSquashAutom
        itpi.getProject() >> mockProject
        itpi.isAutomated() >> true
        itpi.getReferencedTestCase() >> Mock(TestCase) {
            isSquashTFAutomated() >> false
            isSquashAutomAutomated() >> true
            getProject() >> mockProject
        }
        itpi.getIteration() >> mockIteration()
        itpi.getTestSuites() >> mockTestSuiteList()
        itpi.getCampaign() >> Mock(Campaign)

        return itpi
    }

    private Iteration mockIteration() {
        Iteration iter = Mock()
        iter.getId() >> 1L
        iter.getCampaign() >> Mock(Campaign)
        return iter
    }

    private List<TestSuite> mockTestSuiteList() {
        List<TestSuite> testSuiteList = new ArrayList<>()
        testSuiteList.add(mockTestSuite())
        return testSuiteList
    }

    private TestSuite mockTestSuite() {
        TestSuite testSuite = Mock()
        testSuite.getId() >> 1L
        return testSuite
    }
}
