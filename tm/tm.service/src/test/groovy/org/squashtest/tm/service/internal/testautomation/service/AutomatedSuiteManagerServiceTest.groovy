/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.service


import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.domain.testautomation.*
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService
import org.squashtest.tm.service.internal.denormalizedfield.PrivateDenormalizedFieldValueService
import org.squashtest.tm.service.internal.repository.*
import org.squashtest.tm.service.internal.testautomation.AutomatedSuiteManagerServiceImpl
import org.squashtest.tm.service.internal.testautomation.TestAutomationConnectorRegistry
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.servers.CredentialsProvider
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector
import spock.lang.Specification

import javax.persistence.EntityManager

class AutomatedSuiteManagerServiceTest extends Specification {
    TestAutomationConnectorRegistry connectorRegistry
    AutomatedSuiteManagerServiceImpl service
    PermissionEvaluationService permService
    AutomatedSuiteDao autoSuiteDao

    PrivateCustomFieldValueService customFieldValuesService = Mock()
    PrivateDenormalizedFieldValueService denormalizedFieldValueService = Mock()
    ExecutionDao executionDaoMock = Mock()
    EntityManager entityManager = Mock()
    ProjectDao projectDao = Mock()
    IterationTestPlanDao testPlanDao = Mock()
    GenericProjectDao genericProjectDao = Mock()
    CredentialsProvider credentialsProvider = Mock()

    def setup() {
        connectorRegistry = Mock()
        permService = Mock()
        autoSuiteDao = Mock()
        permService.hasRoleOrPermissionOnObject(_, _, _) >> true
        permService.hasRoleOrPermissionOnObject(_, _, _, _) >> true
        permService.hasRole(_) >> true

        service = new AutomatedSuiteManagerServiceImpl(autoSuiteDao: autoSuiteDao,
            executionDao: executionDaoMock,
            customFieldValuesService: customFieldValuesService,
            denormalizedFieldValueService: denormalizedFieldValueService)
        service.connectorRegistry = connectorRegistry
        service.permissionService = permService
        service.entityManager = entityManager
        service.projectDao = projectDao
        service.testPlanDao = testPlanDao
        service.genericProjectDao = genericProjectDao
        service.credentialsProvider = credentialsProvider
    }

    def "should stop running workflows"() {
        given:
        def suiteId = "4028800e8d549ac2018d54fae2970002"
        def workflow = "b794241e-03c6-4af8-a8ca-dc315795e268"
        def workflows = List.of(workflow)

        and:
        def suiteWorkflow = Mock(AutomatedSuiteWorkflow) {
            getProjectId() >> 1L
            getWorkflowId() >> workflow
        }

        def server = Mock(TestAutomationServer) {
            getKind() >> TestAutomationServerKind.squashOrchestrator
            getId() >> 1L
        }
        def connector = Mock(TestAutomationConnector)
        connector.killWorkflow(_, _, _, _) >> Mock(OrchestratorResponse) { isReachable() >> true }

        and:
        List<AutomatedSuiteWorkflow> suiteWorkflows = new ArrayList<>()
        suiteWorkflows.add(suiteWorkflow)

        AutomatedSuite suite = Mock(AutomatedSuite) {
            getWorkflows() >> suiteWorkflows
        }

        when:
        def result = service.stopWorkflows(suiteId, workflows)

        then:
        1 * autoSuiteDao.getAutomatedSuiteWorkflowByWorkflowId(_) >> suiteWorkflow
        1 * genericProjectDao.findTestAutomationServer(_) >> server
        1 * connectorRegistry.getConnectorForKind(_) >> connector
        1 * credentialsProvider.getProjectLevelCredentials(_, _) >> Optional.ofNullable(Mock(TokenAuthCredentials))
        1 * entityManager.find(_, _) >> suite
        suiteWorkflows.size() == 0
        !result
    }

    def mockAutomatedSuite() {

        AutomatedSuite suite = new AutomatedSuite()
        suite.id = "12345"
        suite.iteration = mockIteration()

        TestAutomationServer serverJenkins = new TestAutomationServer(TestAutomationServerKind.jenkins)
        serverJenkins.setName("thejenkins")
        serverJenkins.setUrl("http://jenkins-ta")
        TestAutomationServer serverAutom = new TestAutomationServer(TestAutomationServerKind.squashOrchestrator)
        serverAutom.setName("theAutom")
        serverAutom.setUrl("http://autom")

        TestAutomationProject projectJ1 = new TestAutomationProject("project-jenkins-1", serverJenkins)
        TestAutomationProject projectAutom1 = new TestAutomationProject("project-autom-1", serverAutom)
        TestAutomationProject projectJ2 = new TestAutomationProject("project-jenkins-2", serverJenkins)

        def allTests = []

        def projects = [
            projectJ1,
            projectAutom1,
            projectJ2
        ]

        projects.each { proj ->

            5.times { num ->

                AutomatedTest test = new AutomatedTest("${proj.jobName} - test $num", proj)
                allTests << test
            }
        }

        suite.addExtenders(
            projects.collect { proj ->
                // returns list of lists of exts
                return (0..5).collect { // returns list of exts
                    mockExtender()
                }
                    .eachWithIndex { extender, num ->

                        // performs stuff on exts and returns exts
                        extender.getAutomatedProject() >> proj
                        def autotest = extender.getAutomatedTest()
                        autotest.getProject() >> proj
                        autotest.getName() >> "${proj.jobName} - test $num"
                    }
            }.flatten()
        )

        return suite
    }

    private Iteration mockIteration() {
        Iteration iter = Mock()
        iter.getId() >> 1L
        iter.getCampaign() >> Mock(Campaign)
        return iter
    }

    private TestSuite mockTestSuite() {
        TestSuite testSuite = Mock()
        testSuite.getId() >> 1L
        return testSuite
    }

    private AutomatedExecutionExtender mockExtender() {
        AutomatedExecutionExtender extender = Mock()

        AutomatedTest automatedTest = Mock()
        extender.getAutomatedTest() >> automatedTest
        Execution exec = Mock()
        Iteration iteration = Mock()

        exec.iteration >> iteration
        def iterationTestPlanItem = Mock(IterationTestPlanItem)
        exec.testPlan >> iterationTestPlanItem
        exec.campaign >> Mock(Campaign)
        iterationTestPlanItem.getIteration() >> iteration
        iterationTestPlanItem.getTestSuites() >> new ArrayList<TestSuite>()
        iteration.getTestSuites() >> new ArrayList<TestSuite>()

        extender.getExecution() >> exec

        TestCase tc = Mock()
        tc.uuid >> "44d63d7e-11dd-44b0-b584-565b6f791fa9"

        exec.referencedTestCase >> tc

        return extender
    }
}
