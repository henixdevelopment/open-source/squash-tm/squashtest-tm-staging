/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.springframework.context.MessageSource
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException
import org.squashtest.tm.core.foundation.lang.Couple
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.servers.AuthenticationProtocol
import org.squashtest.tm.domain.servers.Credentials
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow
import org.squashtest.tm.domain.testautomation.AutomatedTest
import org.squashtest.tm.domain.testautomation.TestAutomationProject
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.users.ApiToken
import org.squashtest.tm.service.internal.configuration.CallbackUrlProvider
import org.squashtest.tm.service.internal.dto.ApiTokenDto
import org.squashtest.tm.service.internal.library.EntityPathHeaderService
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao
import org.squashtest.tm.service.internal.testautomation.BuildDef
import org.squashtest.tm.service.internal.testautomation.StartTestExecution
import org.squashtest.tm.service.internal.testautomation.StartTestExecutionProvider
import org.squashtest.tm.service.internal.testautomation.TestAutomationSquashAutomConnector
import org.squashtest.tm.service.internal.testautomation.model.IterationTestPlanItemWithCustomFields
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService
import org.squashtest.tm.service.servers.CredentialsProvider
import org.squashtest.tm.service.servers.StoredCredentialsManager
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration
import org.squashtest.tm.service.testautomation.spi.TestAutomationServerNoCredentialsException
import org.squashtest.tm.service.user.ApiTokenService
import spock.lang.Specification

class TestAutomationSquashAutomConnectorTest extends Specification{

    private TestAutomationSquashAutomConnector testAutomationSquashAutomConnector;

    def setup(){
        testAutomationSquashAutomConnector = new TestAutomationSquashAutomConnector()
    }

    def "check credentials with credentials should not be a supported operation"(){
        given:
        TestAutomationServer server = Mock()
        Credentials credentials = Mock()

        when:
        testAutomationSquashAutomConnector.checkCredentials(server, credentials)

        then:
        thrown(UnsupportedOperationException)
    }

    def "check credentials with login and password should not be a supported operation"(){
        given:
        TestAutomationServer server = Mock()
        String login = "login"
        String password = "password"

        when:
        testAutomationSquashAutomConnector.checkCredentials(server, login, password)

        then:
        thrown(UnsupportedOperationException)
    }

    def "list projects on automation server with credentials should not be a supported operation"(){
        given:
        TestAutomationServer server = Mock()
        Credentials credentials = Mock()

        when:
        testAutomationSquashAutomConnector.listProjectsOnServer(server, credentials)

        then:
        thrown(UnsupportedOperationException)
    }

    def "list tests in project should not be a supported operation"(){
        given:
        TestAutomationProject project = Mock()
        String username = "login"

        when:
        testAutomationSquashAutomConnector.listTestsInProject(project, username)

        then:
        thrown(UnsupportedOperationException)
    }

    def "execute parameterized test should not be a supported operation"(){
        given:
        Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests = Mock()
        String externalId= "id"

        when:
        testAutomationSquashAutomConnector.executeParameterizedTests(tests, externalId)

        then:
        thrown(UnsupportedOperationException)
    }

    def "should map list of items by automation server"(){
        given:

        List<IterationTestPlanItemWithCustomFields> tests = mockTests()

        when:

        Map<TestAutomationServer, List<IterationTestPlanItemWithCustomFields>> res =
                testAutomationSquashAutomConnector.mapParametrizedItemsByServerAndNamespace(
                        tests,
                        createProjectIdToNamespaceMap())

        then:
        res.entrySet().size() == 2

    }

    def "should transform multi value map to list of BuildDef"(){
        given:
        StoredCredentialsManager storedCredentialsManager = Mock()
        storedCredentialsManager.findProjectCredentials(_,_) >> null
        testAutomationSquashAutomConnector.storedCredentialsManager = storedCredentialsManager

        CredentialsProvider credentialsProvider = Mock()
        Credentials credentials = new TokenAuthCredentials("someToken");
        credentialsProvider.getAppLevelCredentials(_) >> Optional.of(credentials)
        testAutomationSquashAutomConnector.credentialsProvider = credentialsProvider

        Authentication authentication = Mock()
        authentication.getName() >> "toto"
        authentication.getCredentials() >> "totoPassword"

        and:

        Map<TestAutomationServer, List<IterationTestPlanItemWithCustomFields>> multValueMap =
                testAutomationSquashAutomConnector.mapParametrizedItemsByServerAndNamespace(
                        mockTests(),
                        createProjectIdToNamespaceMap())

        when:

        List<BuildDef> res = testAutomationSquashAutomConnector.mapToBuildDef(multValueMap)

        then:
        res.size() == 2
        res.get(0).getCredentials().getToken() == "someToken"
        res.get(0).getServer().getName() == "Autom Server 1"

    }

    def "find test automation project url should not be a supported operation"(){
        given:
        TestAutomationProject project = Mock()

        when:
        testAutomationSquashAutomConnector.findTestAutomationProjectURL(project)

        then:
        thrown(UnsupportedOperationException)
    }

    def "testListIsOrderGuaranteed should not be a supported operation"(){
        given:
        Collection<AutomatedTest> tests = Mock()
        when:
        testAutomationSquashAutomConnector.testListIsOrderGuaranteed(tests)
        then:
        thrown(UnsupportedOperationException)
    }

    def "should get connector kind"(){
        given:
        TestAutomationServerKind connectorKind = TestAutomationServerKind.squashOrchestrator

        when:
        TestAutomationServerKind result = testAutomationSquashAutomConnector.getConnectorKind()

        then:
        result == connectorKind
    }

    def "should say if authentication protocol #protocol is supported"(){
        when:
        boolean result = testAutomationSquashAutomConnector.supports(protocol)

        then:

        result == isSupported

        where:
        protocol | isSupported
        AuthenticationProtocol.BASIC_AUTH | false
        AuthenticationProtocol.TOKEN_AUTH | true

    }

    def "should whine if not a supported protocol"(){
        given:
            CredentialsProvider credentialsProvider = Mock()
            Credentials credentials = Mock();
            credentials.getImplementedProtocol() >> AuthenticationProtocol.BASIC_AUTH
            credentialsProvider.getAppLevelCredentials(_) >> Optional.of(credentials)
            testAutomationSquashAutomConnector.credentialsProvider = credentialsProvider
        and:
            TestAutomationServer taServer=Mock()
            taServer.getName() >> "dummyServer"
        and:
            MessageSource i18nHelper=Mock()
            i18nHelper.getMessage(_,_,_) >> "Pas content, Pas content!"
            testAutomationSquashAutomConnector.i18nHelper=i18nHelper
        when:
            testAutomationSquashAutomConnector.getAutomationServerCredentials(taServer)
        then:
            thrown(UnsupportedAuthenticationModeException)

    }

    def "should get supported protocols"(){
        when:
        AuthenticationProtocol[] result = testAutomationSquashAutomConnector.getSupportedProtocols()

        then:
        result.length == 1
        result[0] == AuthenticationProtocol.TOKEN_AUTH
    }

    def "Should bark the right insult when no credentials"(){
        given:
            CredentialsProvider credentialsProvider = Mock()
            credentialsProvider.getAppLevelCredentials(_) >> Optional.empty()
            testAutomationSquashAutomConnector.credentialsProvider = credentialsProvider
        and:
            TestAutomationServer taServer=Mock()
            taServer.getName() >> "dummyServer"
        and:
            MessageSource i18nHelper=Mock()
            i18nHelper.getMessage(_,_,_) >> "Pas content, Pas content!"
            testAutomationSquashAutomConnector.i18nHelper=i18nHelper
        when:
            testAutomationSquashAutomConnector.getAutomationServerCredentials(taServer)
        then:
            thrown(TestAutomationServerNoCredentialsException)
    }


    def "should compute map of namespace by project id"() {
        given:
            Collection<SquashAutomExecutionConfiguration> configurations = createConfigurationsSample();
        when:
            def result = testAutomationSquashAutomConnector.computeNamespaceByTmProjectId(configurations)
        then:
            result.size() == 3
            result.get(1L) == "default"
            result.get(3L) == "namespace-b"
            result.get(7L) == "namespace-b"
    }

    private def mockTests(){
        //First Test Case
        TestAutomationServer automServer1 = Mock()
        automServer1.getId() >> 1L
        automServer1.getName() >> "Autom Server 1"
        automServer1.getUrl() >> "http://127.0.64.5:8189"
        Project project1 = Mock()
        project1.getId() >> 1L
        project1.getTestAutomationServer() >> automServer1
        project1.getName() >> "Project 1"
        TestCase testCase1 = Mock()
        testCase1.getProject() >> project1
        testCase1.getName() >> "Test Case 1"
        IterationTestPlanItem item1 = Mock()
        item1.getReferencedTestCase() >> testCase1

        Map<String, Object> item1ParamMap = [TC_REF : "", TC_CUF_CUF1 : "value1"]

        //Second Test Case
        TestAutomationServer automServer2 = Mock()
        automServer2.getId() >> 2L
        automServer2.getName() >> "Autom Server 2"
        Project project2 = Mock()
        project2.getId() >> 2L
        project2.getTestAutomationServer() >> automServer2
        project2.getName() >> "Project 2"
        TestCase testCase2 = Mock()
        testCase2.getProject() >> project2
        testCase2.getName() >> "Test Case 2"
        IterationTestPlanItem item2 = Mock()
        item2.getReferencedTestCase() >> testCase2

        Map<String, Object> item2ParamMap = [TC_REF : "", TC_CUF_CUF1 : "value2"]

        return [new IterationTestPlanItemWithCustomFields(item1, item1ParamMap),
                new IterationTestPlanItemWithCustomFields(item2, item2ParamMap)]

    }

    private static Map<Long, String> createProjectIdToNamespaceMap() {
        return new HashMap<Long, String>() {{
            put(1L, "default");
            put(2L, "extended")
        }}
    }

    private static Collection<SquashAutomExecutionConfiguration> createConfigurationsSample() {

        SquashAutomExecutionConfiguration conf1 =
                new SquashAutomExecutionConfiguration(
                        1L,
                        Arrays.asList("default"),
                        Arrays.asList("cucumber"),
                        new HashMap<String, String>(),
                        null)
        SquashAutomExecutionConfiguration conf2 =
                new SquashAutomExecutionConfiguration(
                        3L,
                        Arrays.asList("namespace-a", "namespace-b"),
                        Arrays.asList("cucumber"),
                        new HashMap<String, String>(),
                        null)
        SquashAutomExecutionConfiguration conf3 =
                new SquashAutomExecutionConfiguration(
                        7L,
                        Arrays.asList("namespace-b"),
                        Arrays.asList("cucumber"),
                        new HashMap<String, String>(),
                    null)
        return [conf1, conf2, conf3]
    }
}
