/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.scmserver

import org.squashtest.tm.domain.scm.ScmServer
import org.squashtest.tm.domain.servers.ThirdPartyServer
import org.squashtest.tm.exception.NameAlreadyInUseException
import org.squashtest.tm.service.internal.repository.ScmRepositoryDao
import org.squashtest.tm.service.internal.repository.ScmServerDao
import org.squashtest.tm.service.internal.repository.display.ScmServerDisplayDao
import org.squashtest.tm.service.scmserver.ScmRepositoryFilesystemService
import org.squashtest.tm.tools.unittest.reflection.ReflectionCategory
import spock.lang.Specification

class ScmServerManagerServiceTest extends Specification {
	private ScmServerDao scmServerDao = Mock()
	private ScmRepositoryDao scmRepositoryDao = Mock()
	private ScmServerDisplayDao scmServerDisplayDao = Mock()
    private ScmRepositoryFilesystemService scmRepositoryFilesystemService = Mock()
	private ScmServerManagerServiceImpl scmServerManagerService = new ScmServerManagerServiceImpl(scmServerDao, scmRepositoryDao, scmServerDisplayDao, scmRepositoryFilesystemService)

	def "#createNewScmServer(ScmServer) - [Nominal] Should create a new ScmServer"() {
		given: "Mock data"
			ScmServer newScmServer = Mock()
		and: "Mock expected result"
			ScmServer expectedScmServer = Mock()
		and: "Mock Dao methods"
			scmServerDao.isServerNameAlreadyInUse(newScmServer) >> false
			scmServerDao.save(newScmServer) >> expectedScmServer
		when:
			ScmServer createdScmServer = scmServerManagerService.createNewScmServer(newScmServer)
		then:
			createdScmServer == expectedScmServer
	}

	def "#createNewScmServer(ScmServer) - [Exception] Should try to create a new ScmServer with a name already used and throw a NameAlreadyInUseException"() {
		given: "Mock data"
			ScmServer newScmServer = Mock()
			newScmServer.getName() >> "Github_Server"
		and: "Mock Dao method"
			scmServerDao.isServerNameAlreadyInUse(newScmServer.getName()) >> true
		when:
			scmServerManagerService.createNewScmServer(newScmServer)
		then:
			thrown NameAlreadyInUseException
	}

	def "#updateName(long, String) - [Nominal] Should update the name of a ScmServer"() {
		given: "Mock data"
			long serverId = 90
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.name = "GitHub Server"
		and:
			String newName = "GitLab Server"
		and: "Mock Dao methods"
			scmServerDao.getReferenceById(serverId) >> server
			scmServerDao.isServerNameAlreadyInUse(newName) >> false
		when:
			String resultName = scmServerManagerService.updateName(serverId, newName)
		then:
			server.id == serverId
			server.name == newName
			1 * scmServerDao.save(server)
			resultName == newName
	}

	def "#updateName(long, String) - [Nothing] Should try to update the name of a ScmServer with the same name and do nothing"() {
		given: "Mock data"
			long serverId = 90
			String serverName = "GitHub Server"
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.name = serverName
		and: "Mock Dao method"
			scmServerDao.getReferenceById(serverId) >> server
		when:
			String resultName = scmServerManagerService.updateName(serverId, serverName)
		then:
			server.id == serverId
			server.name == serverName
			0 * scmServerDao.isServerNameAlreadyInUse()
			0 * scmServerDao.save(server)
			resultName == serverName
	}

	def "#updateName(long, String) - [Exception] Should try to update the name of a ScmServer with an already used name and throw a NameAlreadyInUseException"() {
		given: "Mock data"
			long serverId = 90
			String serverName = "GitHub Server"
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.name = serverName
		and:
			String newName ="GitLab Server"
		and: "Mock Dao methods"
			scmServerDao.getReferenceById(serverId) >> server
			scmServerDao.isServerNameAlreadyInUse(newName) >> true
		when:
			scmServerManagerService.updateName(serverId, newName)
		then:
			server.id == serverId
			server.name == serverName
			0 * scmServerDao.isServerNameAlreadyInUse()
			0 * scmServerDao.save(server)
			thrown NameAlreadyInUseException
	}

	def "#updateUrl(long, String) - [Nominal] Should update the Url of a ScmServer"() {
		given: "Mock data"
			long serverId = 50
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.url = "http://github.com"
		and:
			String newUrl = "http://gitlab.com"
		and: "Mock Dao method"
			scmServerDao.getReferenceById(serverId) >> server
		when:
			String resultUrl = scmServerManagerService.updateUrl(serverId, newUrl)
		then:
			server.id == serverId
			server.url == newUrl
			1 * scmServerDao.save(server)
			resultUrl == newUrl
	}

	def "#updateUrl(long, String) - [Nothing] Should try to update the Url of a ScmServer with the same Url and do nothing"() {
		given: "Mock data"
			long serverId = 50
			String serverUrl = "http://github.com"
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.url = serverUrl
		and: "Mock Dao method"
			scmServerDao.getReferenceById(serverId) >> server
		when:
			String resultUrl = scmServerManagerService.updateUrl(serverId, serverUrl)
		then:
			server.id == serverId
			server.url == serverUrl
			0 * scmServerDao.save(server)
			resultUrl == serverUrl
	}

	def "#updateUrl(long, String) - [Exception] Should try to update the Url of a ScmServer with an malformed Url and throw an Exception"() {
		given: "Mock data"
			long serverId = 50
			String serverUrl = "http://github.com"
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.url = serverUrl
		and:
			String malformedUrl = "malformedUrl"
		and: "Mock Dao methods"
			scmServerDao.getReferenceById(serverId) >> server
			scmServerDao.save(server) >> { throw new Exception() }
		when:
			scmServerManagerService.updateUrl(serverId, malformedUrl)
		then:
			thrown Exception
	}

	def "#updateCommiterMail(long, String) - [Nominal] Should update the committer mail of a ScmServer"() {
		given: "Mock data"
			long serverId = 2
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.committerMail = "committer@mail.com"
		and:
			String newCommitterMail = "new.committer@mail.com"
		and: "Mock Dao method"
			scmServerDao.getReferenceById(serverId) >> server
		when:
			String resultCommitterMail = scmServerManagerService.updateCommitterMail(serverId, newCommitterMail)
		then:
			server.id == serverId
			server.committerMail == newCommitterMail
			1 * scmServerDao.save(server)
			resultCommitterMail == newCommitterMail
	}

	def "#updateCommitterMail(long, String) - [Nothing] Should try to update the committer mail of a ScmServer with the same mail and do nothing"() {
		given: "Mock data"
			long serverId = 3
			String serverCommitterMail = "committer@mail.com"
			ScmServer server = new ScmServer()
			setServerId(server, serverId)
			server.committerMail = serverCommitterMail
		and: "Mock Dao method"
			scmServerDao.getReferenceById(serverId) >> server
		when:
			String resultCommitterMail = scmServerManagerService.updateCommitterMail(serverId, serverCommitterMail)
		then:
			server.id == serverId
			server.committerMail == serverCommitterMail
			0 * scmServerDao.save(server)
			resultCommitterMail == serverCommitterMail
	}

	def '#deleteScmServers(Collection<Long>) - [Nominal] Should delete several ScmServers'() {
		given: "Mock data"
			Collection<Long> serverIds = [1L, 5L, 15L]
		and:
			ScmServer s1 = Mock()
			ScmServer s2 = Mock()
			ScmServer s3 = Mock()
		and: "Mock Dao methods"
			scmServerDao.getReferenceById(1L) >> s1
			scmServerDao.getReferenceById(5L) >> s2
			scmServerDao.getReferenceById(15L) >> s3
		when:
			scmServerManagerService.deleteScmServers(serverIds)
		then:
			1 * scmServerDao.releaseContainedScmRepositoriesFromProjects(serverIds)
			1 * scmServerDao.delete(s1)
			1 * scmServerDao.delete(s2)
			1 * scmServerDao.delete(s3)
            3 * scmRepositoryFilesystemService.deleteLocalRepositories(_)
	}


	def setServerId(server, id){
		use(ReflectionCategory){
			ThirdPartyServer.set field: "id", of: server, to:id
		}
	}

    def "#updateCredentialsNotShared(long, boolean) - [Nominal] Should update the credentialsNotShared of a ScmServer"() {
        given: "Mock data"
        long serverId = 1
        ScmServer server = new ScmServer()
        setServerId(server, serverId)
        server.credentialsNotShared = false
        and:
        boolean newCredentialsNotShared = true
        and: "Mock Dao methods"
        scmServerDao.findById(serverId) >> Optional.of(server)
        when:
        boolean resultCredentialsNotShared = scmServerManagerService.updateCredentialsNotShared(serverId, newCredentialsNotShared)
        then:
        server.id == serverId
        server.credentialsNotShared == newCredentialsNotShared
        resultCredentialsNotShared == newCredentialsNotShared
    }

    def "#getCredentialsNotShared(long) - [Nominal] Should get the credentialsNotShared of a ScmServer"() {
        given: "Mock data"
        long serverId = 1
        ScmServer server = new ScmServer()
        setServerId(server, serverId)
        server.credentialsNotShared = true
        and: "Mock Dao methods"
        scmServerDao.findById(serverId) >> Optional.of(server)
        when:
        boolean resultCredentialsNotShared = scmServerManagerService.getCredentialsNotShared(serverId)
        then:
        resultCredentialsNotShared == server.credentialsNotShared
    }
}
