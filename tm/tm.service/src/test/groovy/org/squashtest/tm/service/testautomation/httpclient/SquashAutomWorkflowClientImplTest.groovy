/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.httpclient

import org.apache.commons.io.IOUtils
import org.apache.http.HttpEntity
import org.apache.http.StatusLine
import org.apache.http.entity.ContentType
import org.opentestfactory.messages.OTFMessage
import org.opentestfactory.messages.Status
import org.squashtest.tm.service.internal.testautomation.httpclient.MessageRefusedException
import org.squashtest.tm.service.internal.testautomation.httpclient.Response
import org.squashtest.tm.service.internal.testautomation.httpclient.SquashAutomWorkflowClientImpl
import org.squashtest.tm.service.internal.testautomation.httpclient.UnexpectedServerResponseException
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowHandle
import spock.lang.Specification

class SquashAutomWorkflowClientImplTest extends Specification{
    private static final String ENDPOINT = "http://someEndpoint.com"
    private static final String TOKEN = "someToken"

    private SquashAutomWorkflowClientImpl client

    def setup() {
        client = new SquashAutomWorkflowClientImpl(ENDPOINT, ENDPOINT, ENDPOINT, TOKEN, 15)
    }

    def "should handle correct response"() {
        given:

        URI uri = new URI(ENDPOINT + "/extension")

        StatusLine statusLine = Mock()
        statusLine.getStatusCode() >> 200

            Response response = Mock()
        response.getStatusLine() >> statusLine

        HttpEntity entity = Mock()

        InputStream content =  this.getClass().getClassLoader().getResourceAsStream("squashautom/workflowHandle.json")
        entity.getContent() >> content

        response.getEntity() >> entity

        when:

            WorkflowHandle workflowHandle = client.handleStatusResponse(response, uri, WorkflowHandle.class)

        then:
        workflowHandle != null
        workflowHandle.getStatus() == Status.StatusValue.Success
        workflowHandle.getWorkflowId() == "thisIsAnId"
    }

    def "should handle incorrect response (code > 400) with json content"() {
        given:

        URI uri = new URI(ENDPOINT + "/extension")

        StatusLine statusLine = Mock()
        statusLine.getStatusCode() >> 404

        Response response = Mock()
        response.getStatusLine() >> statusLine
        response.mimeType() >> ContentType.APPLICATION_JSON.getMimeType()

        HttpEntity entity = Mock()

        InputStream content =  this.getClass().getClassLoader().getResourceAsStream("squashautom//workflowHandle.json")
        entity.getContent() >> content

        response.getEntity() >> entity

        when:

        client.handleStatusResponse(response, uri, WorkflowHandle.class)

        then:
        thrown MessageRefusedException
    }

    def "should handle incorrect response (code > 400) with no content"() {
        given:

        URI uri = new URI(ENDPOINT + "/extension")

        StatusLine statusLine = Mock()
        statusLine.getStatusCode() >> 404

        Response response = Mock()
        response.getStatusLine() >> statusLine

        HttpEntity entity = Mock()

        InputStream content =  this.getClass().getClassLoader().getResourceAsStream("squashautom//workflowHandle.json")
        entity.getContent() >> content

        response.getEntity() >> entity

        when:

        client.handleStatusResponse(response, uri, WorkflowHandle.class)

        then:
        thrown UnexpectedServerResponseException
    }

    private static class TestMessage extends OTFMessage{
        TestMessage() {
            super("v1");
        }

        TestMessage(String apiVersion){
            super(apiVersion);
        }
    }

    def "should build channels endpoint url"() {
        given:
        def receptionistUrl = "http://receptionist.url"
        def killSwitchUrl = "http://killSwitch.url"
        def workflowClient = new SquashAutomWorkflowClientImpl(receptionistUrl, observerUrl, killSwitchUrl, TOKEN, 15)

        when:
        def builtUrl = workflowClient.buildChannelsUrl()

        then:
        builtUrl == expectedUrl

        where:
        observerUrl                         ||  expectedUrl
        "http://127.0.0.1"                  |   "http://127.0.0.1/channels"
        "http://127.0.0.1/"                 |   "http://127.0.0.1/channels"
        "http://127.0.0.1//"                |   "http://127.0.0.1/channels"
        "http://127.0.0.1:7775"             |   "http://127.0.0.1:7775/channels"
        "http://127.0.0.1:7775/"            |   "http://127.0.0.1:7775/channels"
        "http://127.0.0.1:7775//"           |   "http://127.0.0.1:7775/channels"
        "http://127.0.0.1:7775/blabla/"     |   "http://127.0.0.1:7775/blabla/channels"
        "http://127.0.0.1:7775//blabla/"    |   "http://127.0.0.1:7775/blabla/channels"
        "http://127.0.0.1:7775/blabla//"    |   "http://127.0.0.1:7775/blabla/channels"
        ""                                  |   "http://receptionist.url/channels"
        null                                |   "http://receptionist.url/channels"
    }
}
