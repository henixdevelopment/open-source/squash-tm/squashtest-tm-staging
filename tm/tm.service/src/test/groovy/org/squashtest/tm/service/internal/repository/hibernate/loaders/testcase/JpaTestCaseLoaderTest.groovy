/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders.testcase

import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader
import spock.lang.Specification

import javax.persistence.EntityGraph
import javax.persistence.EntityManager
import javax.persistence.Subgraph
import javax.persistence.TypedQuery

class JpaTestCaseLoaderTest extends Specification {
    JpaTestCaseLoader testCaseLoader
    EntityManager entityManager = Mock(EntityManager)

    void setup() {
        testCaseLoader = new JpaTestCaseLoader(entityManager)
    }

    def "should load test cases"() {

        given:
        Collection<Long> ids = [1L, 2L, 3L]
        EnumSet<TestCaseLoader.Options> options = EnumSet.of(
            TestCaseLoader.Options.FETCH_ATTACHMENT_LIST,
            TestCaseLoader.Options.FETCH_STEPS
        )
        TypedQuery<TestCase> query = Mock(TypedQuery)
        EntityGraph<TestCase> entityGraph = Mock(EntityGraph)
        Subgraph subGraph = Mock(Subgraph)
        def tc = Mock(TestCase)

        when:
        List<TestCase> result = testCaseLoader.load(ids, options)

        then:
        1 * entityManager.createQuery(_, _) >> query
        2 * query.setHint(_, _) >> query
        1 * query.getResultList() >> [tc]
        1 * entityManager.createEntityGraph(_) >> entityGraph
        1 * entityGraph.addAttributeNodes(_)
        1 * entityGraph.addSubgraph(TestCaseLoader.Options.FETCH_ATTACHMENT_LIST.field) >> subGraph
        1 * subGraph.addAttributeNodes("attachments")
        result.contains(tc)

    }
}
