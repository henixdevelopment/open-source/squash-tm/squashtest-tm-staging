/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.servers

import org.squashtest.tm.domain.bugtracker.BugTracker
import org.squashtest.tm.service.spi.DefaultOAuth2ConfigurationHandler
import org.squashtest.csp.core.bugtracker.spi.DefaultOAuth2FormValues
import org.squashtest.tm.domain.servers.ThirdPartyServer
import spock.lang.Specification

class OAuth2ConsumerServiceImplTest extends Specification{
	DefaultOAuth2ConfigurationHandler handler = new DefaultOAuth2ConfigurationHandler()


	def "should get oauth2 authorization url"(){

		given:

		ServerOAuth2Conf conf = new ServerOAuth2Conf()
		conf.setAuthorizationUrl("https://test-squash.com/rest/oauth2/latest/authorize")
		conf.setCallbackUrl("https://test-squash.com/oauth2/authentication")
		conf.setGrantType("authorization_url")
		conf.setClientId("a1b9c9d6e")
		conf.setScope("WRITE")

		ThirdPartyServer bt = new BugTracker()
		bt.setUrl("https://test-squash.com/")
		when:
		def url =  handler.getOauth2AuthenticationUrl(bt,conf)

		then:
		url.equalsIgnoreCase("https://test-squash.com/rest/oauth2/latest/authorize?client_id=a1b9c9d6e&redirect_uri=https://test-squash.com/oauth2/authentication&response_type=authorization_url&scope=WRITE")

	}

	def "should get oauth2 request token url"(){
		given:
		ServerOAuth2Conf conf = new ServerOAuth2Conf()
		conf.setAuthorizationUrl("https://test-squash.com/rest/oauth2/latest/authorize")
		conf.setRequestTokenUrl("https://test-squash.com/rest/oauth2/latest/token")
		conf.setCallbackUrl("https://test-squash.com/oauth2/authentication")
		conf.setGrantType("authorization_url")
		conf.setClientId("a1b9c9d6e")
		conf.setClientSecret("a4b9c9a4b9c9d6ed6e")
		conf.setScope("WRITE")

		ThirdPartyServer bt = new BugTracker()
		bt.setUrl("https://test-squash.com/")

		def code = "dehedeuhd"
		def btId =10L
		when:
		def url =  handler.getOauth2RequestTokenUrl(btId,code,conf)

		then:
		url.equalsIgnoreCase("https://test-squash.com/rest/oauth2/latest/token?client_id=a1b9c9d6e&client_secret=a4b9c9a4b9c9d6ed6e&code=dehedeuhd&grant_type=authorization_code&redirect_uri=https://test-squash.com/oauth2/authentication")

	}

	def "should get oauth2 default form value"(){
		when:
		DefaultOAuth2FormValues defaultValue =  handler.getDefaultValueForOauth2Form("https://test-squash.com")

		then:
        defaultValue.authorizationUrl.equalsIgnoreCase("https://test-squash.com/rest/oauth2/latest/authorize")
		defaultValue.requestTokenUrl.equalsIgnoreCase("https://test-squash.com/rest/oauth2/latest/token")
		defaultValue.scope.equalsIgnoreCase("")


	}



}
