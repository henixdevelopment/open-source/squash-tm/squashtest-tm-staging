/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder
import spock.lang.Specification

import javax.persistence.EntityGraph
import javax.persistence.EntityManager
import javax.persistence.Subgraph
import javax.persistence.TypedQuery

class EntityGraphQueryBuilderTest extends Specification {
    EntityManager entityManager = Mock()
    EntityGraph entityGraph = Mock(EntityGraph)
    TypedQuery query = Mock(TypedQuery)
    String baseQuery = "SELECT t FROM TestEntity t"

    EntityGraphQueryBuilder<TestEntity> entityGraphQueryBuilder

    def setup() {
        entityManager.createEntityGraph(TestEntity) >> entityGraph
        entityManager.createQuery(_,_) >> query
        entityGraphQueryBuilder = new EntityGraphQueryBuilder<>(entityManager, TestEntity, baseQuery)
    }

    def "should add attributes"() {
        when:
        entityGraphQueryBuilder.addAttributeNodes("attr1", "attr2")

        then:
        1 * entityGraph.addAttributeNodes("attr1", "attr2")
    }

    def "should add subgraph with attributes"() {
        given:
        Subgraph subGraph = Mock(Subgraph)
        entityGraph.addSubgraph("attr1") >> subGraph

        when:
        entityGraphQueryBuilder.addSubGraph("attr1", "subAttr1", "subAttr2")

        then:
        1 * subGraph.addAttributeNodes("subAttr1", "subAttr2")
        entityGraphQueryBuilder.subgraphs.size() == 1
        entityGraphQueryBuilder.subgraphs.get("attr1") == subGraph

    }

    def "should add subgraph to subgraph"() {
        given:
        Subgraph subGraph = Mock()
        entityGraphQueryBuilder.subgraphs.put("attr1", subGraph)

        Subgraph subSubGraph = Mock()

        when:
        entityGraphQueryBuilder.addSubgraphToSubgraph("attr1", "subAttr1", "subSubAttr1")

        then:
        1 * subGraph.addSubgraph("subAttr1") >> subSubGraph
        1 * subSubGraph.addAttributeNodes("subSubAttr1")
        entityGraphQueryBuilder.subgraphs.size() == 2
    }


    def "should execute for single"() {
        given:
        Map<String, Object> parameters = ["param1": "value1", "param2": "value2"]

        when:
        entityGraphQueryBuilder.execute(parameters)

        then:
        2 * query.setParameter(_, _)
        1 * query.getSingleResult()
    }

    def "should execute for list"() {
        given:
        Map<String, Object> parameters = ["param1": "value1", "param2": "value2"]

        when:
        entityGraphQueryBuilder.executeList(parameters)

        then:
        2 * query.setParameter(_, _)
        1 * query.getResultList()
    }

    def "should execute for page"() {
        given:
        Map<String, Object> parameters = ["param1": "value1"]
        def mockOrders = List.of(Mock(Sort.Order) {
            getProperty() >> "name"
            getDirection() >> Sort.Direction.ASC
        })

        Pageable pageable = Mock() {
            toOptional() >> Optional.empty()
            getSort() >> Mock(Sort) {
                orders >> mockOrders
            }
        }

        String countSql = "SELECT COUNT(t) FROM TestEntity t"
        def countQuery = Mock(TypedQuery)

        when:
        entityGraphQueryBuilder.executePageable(parameters, pageable, countSql)

        then:
        1 * query.setParameter(_, _)
        1 * query.getResultList() >> new ArrayList<>()
        1 * entityManager.createQuery(countSql, Long.class) >> countQuery
        1 * countQuery.setParameter(_, _)
        1 * countQuery.getSingleResult() >> 1l
    }

}

class TestEntity {}
