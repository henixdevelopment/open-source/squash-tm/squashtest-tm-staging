/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.user

import org.springframework.security.core.session.SessionRegistry
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.domain.users.UsersGroup
import org.squashtest.tm.exception.IllegalUserGroupTransitionException
import org.squashtest.tm.service.configuration.ConfigurationService
import org.squashtest.tm.service.feature.FeatureManager
import org.squashtest.tm.service.internal.repository.UserDao
import org.squashtest.tm.service.internal.repository.UsersGroupDao
import org.squashtest.tm.service.license.LicenseHelperService
import org.squashtest.tm.service.security.AdministratorAuthenticationService
import org.squashtest.tm.service.security.acls.model.ObjectAclService
import org.squashtest.tm.service.user.UserAdministrationService
import spock.lang.Specification

class UserAdministrationServiceImplTest extends Specification {

	UserAdministrationService service = new UserAdministrationServiceImpl()
	UserDao userDao = Mock()
	UsersGroupDao groupDao = Mock()
	AdministratorAuthenticationService adminService = Mock()
	FeatureManager features  = Mock()
	ObjectAclService aclService = Mock()
	ConfigurationService configurationService = Mock()
	LicenseHelperService licenseHelperService = Mock()
	SessionRegistry sessionRegistry = Mock()

	def setup(){
		service.userDao = userDao
		service.groupDao = groupDao
		service.adminAuthentService = adminService
		service.features = features
		service.aclService = aclService
		service.configurationService = configurationService
		service.licenseHelperService = licenseHelperService
		service.sessionRegistry = sessionRegistry
		features.isEnabled(_) >> false
	}

    def "should not modify group for Test automation server"() {
        given:
        UsersGroup usersGroup = Mock(UsersGroup)
        usersGroup.getId() >> 1L
        usersGroup.getQualifiedName() >> UsersGroup.ADMIN

        User user = Mock(User)
        user.getGroup() >> usersGroup
        userDao.getReferenceById(10L) >> user

        and:
        UsersGroup group = Mock(UsersGroup)
        group.getQualifiedName() >> UsersGroup.TEST_AUTOMATION_SERVER
        groupDao.getReferenceById(4L) >> group

        and:
        sessionRegistry.getAllPrincipals() >> []

        when:
        service.setUserGroupAuthority(10L, 4L)

        then:
        thrown(IllegalUserGroupTransitionException)
    }

    def "should not modify group from Test automation server"() {
        given:
        UsersGroup usersGroup = Mock(UsersGroup)
        usersGroup.getId() >> 4L
        usersGroup.getQualifiedName() >> UsersGroup.TEST_AUTOMATION_SERVER

        User user = Mock(User)
        user.getGroup() >> usersGroup
        userDao.getReferenceById(10L) >> user

        and:
        UsersGroup group = Mock(UsersGroup)
        group.getQualifiedName() >> UsersGroup.USER
        groupDao.getReferenceById(2L) >> group

        and:
        sessionRegistry.getAllPrincipals() >> []

        when:
        service.setUserGroupAuthority(10L, 2L)

        then:
        thrown(IllegalUserGroupTransitionException)
    }

    def "should modify group from User to Admin"() {
        given:
        UsersGroup usersGroup = Mock(UsersGroup)
        usersGroup.getId() >> 2L
        usersGroup.getQualifiedName() >> UsersGroup.USER

        User user = Mock(User)
        user.getGroup() >> usersGroup
        userDao.getReferenceById(10L) >> user

        and:
        UsersGroup group = Mock(UsersGroup)
        group.getQualifiedName() >> UsersGroup.ADMIN
        groupDao.getReferenceById(1L) >> group

        and:
        sessionRegistry.getAllPrincipals() >> []

        when:
        service.setUserGroupAuthority(10L, 1L)

        then:
        1 * user.setGroup(group)
    }

	def "should check login availability"() {
		given:
		User user = new User()
		user.setLogin("login")

		when:
		service.addUser(user, 2L, "password")

		then:
		1 * userDao.findUserByLogin("login")
	}

	def "should check case insensitive login availability"() {
		given:
		User user = new User()
		user.setLogin("login")
		String login = "login"

		and:
		FeatureManager feats = Mock()
		feats.isEnabled(FeatureManager.Feature.CASE_INSENSITIVE_LOGIN) >> true
		service.features = feats

		when:
		service.addUser(user, 2L, "password")

		then:
		1 * userDao.findUserByCiLogin("login")
	}
}
