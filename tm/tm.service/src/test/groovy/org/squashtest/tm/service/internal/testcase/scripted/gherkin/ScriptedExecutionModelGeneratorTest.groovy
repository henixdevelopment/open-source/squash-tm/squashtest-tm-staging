/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.scripted.gherkin

import gherkin.AstBuilder
import gherkin.Parser
import gherkin.ast.GherkinDocument
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import spock.lang.Specification

import static org.apache.commons.io.FileUtils.readFileToString

class ScriptedExecutionModelGeneratorTest extends Specification {

	def "should parse a document" () {
		given:
		GherkinDocument gherkinDocument = getGherkinDocument(file)
		ScriptedExecutionModelGenerator generator = new ScriptedExecutionModelGenerator()

		when:
		def model = generator.buildModel(gherkinDocument)

		then:
		// Next line fails because of character encoding
		// model.scriptName == scriptName
		model.steps.size() == stepCount
		model.hasPrerequisite() == hasPrerequisite

		where:
		file                              || scriptName 					|| hasPrerequisite  || stepCount
		"simple_script.feature"           || "Aller chercher un café"  		|| false 			|| 2
		"scenario_outline_script.feature" || "Aller chercher un café" 		|| false 			|| 6 // 4 examples from outline + 2 scenarios
		"background_script.feature"       || "verifier la machine encore" 	|| true  			|| 2
	}

	GherkinDocument getGherkinDocument(String file) {
		Resource resource = new ClassPathResource("testcase/scripted/gherkin/" + file)
		String script = readFileToString(resource.getFile())
		Parser<GherkinDocument> parser = new Parser<>(new AstBuilder())
		GherkinDocument gherkinDocument = parser.parse(script)
		gherkinDocument
	}
}
