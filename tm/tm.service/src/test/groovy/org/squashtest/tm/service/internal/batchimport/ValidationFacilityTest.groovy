/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport


import org.springframework.security.access.AccessDeniedException
import org.squashtest.tm.domain.infolist.InfoListItem
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementLibrary
import org.squashtest.tm.domain.requirement.RequirementStatus
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseLibrary
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.service.importer.Target
import org.squashtest.tm.service.infolist.InfoListItemFinderService
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageTarget
import org.squashtest.tm.service.internal.batchimport.instruction.TestCaseInstruction
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget
import org.squashtest.tm.service.internal.repository.ProjectDao
import org.squashtest.tm.service.internal.repository.RequirementImportDao
import org.squashtest.tm.service.internal.repository.RequirementVersionCoverageDao
import org.squashtest.tm.service.internal.repository.TestCaseDao
import org.squashtest.tm.service.internal.repository.UserDao
import org.squashtest.tm.service.plugin.PluginFinderService
import org.squashtest.tm.service.requirement.HighLevelRequirementService
import org.squashtest.tm.service.requirement.RequirementLibraryFinderService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.security.UserContextService
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService
import org.squashtest.tm.service.user.UserAccountService
import spock.lang.Issue
import spock.lang.Specification
import spock.lang.Unroll

import static org.squashtest.tm.service.importer.ImportStatus.FAILURE

/**
 * @author Gregory Fouquet
 *
 */
class ValidationFacilityTest extends Specification {
	ValidationFacility facility
	Model model = Mock()
	UserAccountService userAccount = Mock()
	UserContextService userContextService = Mock();
	PermissionEvaluationService permissionService = Mock()
	UserDao userDao = Mock()
	TargetStatus status = Mock()
    EntityValidator entityValidator = Mock()

	RequirementLibraryFinderService requirementFinder = Mock()
	ProjectDao projectDao = Mock()
    InfoListItemFinderService infoListItemService = Mock()

	def setup() {
        facility = new ValidationFacility(permissionService,
            infoListItemService,
            model,
            userDao,
            Mock(MilestoneImportHelper),
            requirementFinder,
            projectDao,
            userContextService,
            Mock(TestCaseDao),
            entityValidator,
            Mock(RequirementImportDao)
        )

		model.getTestCaseCufs(_) >> Collections.emptyList()
		model.getProjectStatus(_) >> Mock(ProjectTargetStatus) {
            getId() >> 1L
            getTestCaseLibraryId() >> 1L
            getRequirementLibraryId() >> 1L
            getStatus() >> Existence.EXISTS
        }

		userAccount.findCurrentUser() >> Mock(User)
		userContextService.getUsername() >> ""
	}

	def mockAnyStatus() {
		model.getStatus(_) >> status
	}

	@Unroll
	def "should validate new test case with inconsistent path #path and name #name"() {
		given:
		LogTrain logTrain = new LogTrain()
		entityValidator.createTestCaseChecks(_, _, _) >> logTrain

        and:
        Project project = Mock() {
            getId() >> 1L
            getTestCaseLibrary() >> Mock(TestCaseLibrary) {
                getId() >> 1L
            }
        }

		and:
        TestCaseTarget target = Mock() {
            getProject() >> "project"
            getPath() >> path
        }

		and:
		TestCase testCase = Mock() {
            getName() >> name
        }

		and:
        TestCaseInstruction instr = new TestCaseInstruction(target, testCase);

        and:
		mockAnyStatus()
		status.status >> Existence.NOT_EXISTS

        and:
        userContextService.getUsername() >> "user"
        infoListItemService.findDefaultTestCaseNature(_) >> Mock(InfoListItem)
        infoListItemService.findDefaultTestCaseType(_) >> Mock(InfoListItem)
        permissionService.hasRoleOrPermissionOnObject(_, _, _, _) >> true

		when:
		facility.createTestCases(List.of(instr), project)

		then:
        def createLog = instr.getLogTrain()
		!createLog.hasCriticalErrors()

		where:
		path					| name
		"/the/path/is/straight"	| "deviant"
		"/the/path/is/straight"	| ""
	}

	def "should not validate old test case with inconsistent path and name"() {
		given:
		LogTrain logTrain = new LogTrain()
		entityValidator.updateTestCaseChecks(_, _, _) >> logTrain

		and:
		TestCaseTarget target = Mock()
		target.path >> "/the/path/is/straight"

		and:
		TestCase testCase = Mock()
		testCase.name >> "deviant"

		and:
		TestCaseInstruction instr = new TestCaseInstruction(target, testCase);

		and:
		mockAnyStatus()
		status.status >> Existence.EXISTS

		when:
		LogTrain createLog = facility.updateTestCase(instr)

		then:
		createLog.criticalErrors
	}

	@Unroll
	def "should validate old test case with without name '#name'"() {
		given:
		LogTrain logTrain = new LogTrain()
		entityValidator.updateTestCaseChecks(_, _, _) >> logTrain

		and:
		TestCaseTarget target = Mock()
		target.path >> path

		and:
		TestCase testCase = Mock() {
            getName() >> name
        }

		and:
		TestCaseInstruction instr = new TestCaseInstruction(target, testCase);

		and:
		mockAnyStatus()
		status.status >> Existence.EXISTS

        and:
        userContextService.getUsername() >> "user"
        infoListItemService.findDefaultTestCaseNature(_) >> Mock(InfoListItem)
        infoListItemService.findDefaultTestCaseType(_) >> Mock(InfoListItem)
        permissionService.hasRoleOrPermissionOnObject(_, _, _, _) >> true

		when:
		LogTrain createLog = facility.updateTestCase(instr)

		then:

        createLog.getEntries().forEach {  it -> println("Foo bar " + it.i18nError) }
		createLog.criticalErrors == fails

		where:
		path					| name		| fails
		"/the/path/is/straight"	| ""		| false
		"/the/path/is/straight"	| null		| false
	}

	def "should validate reqver for coverage"() {
		given:
		CoverageTarget target = wellFormedCoverageTarget()
        CoverageInstruction instruction = new CoverageInstruction(target, Mock(RequirementVersionCoverage))


		and:
        RequirementTarget reqTarget = Mock() {
            getId() >> 10L
        }
        RequirementVersionTarget versionTarget = new RequirementVersionTarget(reqTarget, 1)

        TestCaseTarget tcTarget = Mock() {
            getId() >> 20L
        }

        model.getStatus(_) >> Mock(TargetStatus) {
            getStatus() >> Existence.EXISTS
            getId() >> 1L
        }
        target.setTestCase(tcTarget)
        target.setRequirementVersion(versionTarget)

		and:
        def reqStatus = Map.of(1L, RequirementStatus.WORK_IN_PROGRESS)
        model.getRequirementVersionId(_) >> 1L
		permissionService.hasRoleOrPermissionOnObject(_, _, _, _) >> true

		when:
        facility.checkCoverageCreation(instruction, new HashMap<Long, List<Long>>(), reqStatus, Collections.emptyMap())

		then:
		!instruction.hasCriticalErrors()
}

	@Issue("#6257")
	def "should not validate unreadble reqver for coverage"() {
        given:
        CoverageTarget target = wellFormedCoverageTarget()
        CoverageInstruction instruction = new CoverageInstruction(target, Mock(RequirementVersionCoverage))


        and:
        RequirementTarget reqTarget = Mock()
        RequirementVersionTarget versionTarget = new RequirementVersionTarget(reqTarget, 1)

        TestCaseTarget tcTarget = Mock()

        model.getStatus(_) >> Mock(TargetStatus) {
            getStatus() >> Existence.EXISTS
            getId() >> 1L
        }

        target.setTestCase(tcTarget)
        target.setRequirementVersion(versionTarget)

        and:
        def reqStatus = Map.of(1L, RequirementStatus.WORK_IN_PROGRESS)
        permissionService.hasRoleOrPermissionOnObject(_, _, _, _) >> false

        when:
        facility.checkCoverageCreation(instruction, new HashMap<Long, List<Long>>(), reqStatus, Collections.emptyMap())

        then:
        instruction.hasCriticalErrors()
	}

	def wellFormedCoverageTarget() {
		CoverageTarget target = Mock()
		target.reqPathWellFormed >> true
		target.reqVersion >> 1L

		return new CoverageTarget(reqPath: "/ouate/de/phoque", tcPath: "/sweet/fanny/adams", reqVersion: 1);
	}


	@Issue("#6255")
	@Unroll
	def "should check project with authz tc: #testCaseLibAuthorized, req: #requirementLibAuthorized"() {
		given:
		CoverageTarget coverageTarget = wellFormedCoverageTarget()
        RequirementTarget requirementTarget = new RequirementTarget(coverageTarget.getReqPath())
        RequirementVersionTarget versionTarget = new RequirementVersionTarget(requirementTarget, coverageTarget.getReqVersion())
        coverageTarget.setRequirementVersion(versionTarget)

        TestCaseTarget testCaseTarget = new TestCaseTarget(coverageTarget.getTcPath())
        coverageTarget.setTestCase(testCaseTarget)

        CoverageInstruction instruction = new CoverageInstruction(coverageTarget, Mock(RequirementVersionCoverage))

		and:
		permissionService.hasRoleOrPermissionOnObject("ROLE_ADMIN", "IMPORT", 1L, "org.squashtest.tm.domain.testcase.TestCaseLibrary") >> testCaseLibAuthorized
		permissionService.hasRoleOrPermissionOnObject("ROLE_ADMIN", "IMPORT", 1L, "org.squashtest.tm.domain.requirement.RequirementLibrary") >> requirementLibAuthorized

		when:
        def res = facility.hasPermissions(instruction)

		then:
		 notThrown(UnsupportedOperationException) // as thrown in ish #6255
        res == check

		where:
		testCaseLibAuthorized 	| requirementLibAuthorized	| check
		true 					| true						| true
		false					| false						| false
		false 					| true						| false
		true 					| false						| false
	}

}
