/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion


import org.squashtest.tm.domain.milestone.Milestone
import org.squashtest.tm.service.deletion.*
import org.squashtest.tm.service.internal.campaign.LockedRequirementNodeDetectionService
import org.squashtest.tm.service.internal.repository.RequirementDao
import org.squashtest.tm.service.internal.repository.RequirementDeletionDao
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import spock.lang.Specification

class LockedRequirementNodeDetectionServiceImplTest extends Specification {
    LockedRequirementNodeDetectionService handler
    ActiveMilestoneHolder activeMilestoneHolder = Mock()
    RequirementDeletionDao deletionDao = Mock()
    RequirementDao requirementDao = Mock()

	def setup() {
        handler = new LockedRequirementNodeDetectionServiceImpl(
            activeMilestoneHolder,
            deletionDao,
            requirementDao
        )
	}

    def "should detect locked nodes by milestone"() {
        given:
        def ids = [1L, 2L]
        deletionDao.filterRequirementsIdsWhichMilestonesForbidsDeletion(ids) >> [1L, 2L]

        when:
        def result = handler.detectLockedByMilestone(ids)

        then:
        result.size() == 1
        result[0] instanceof BoundToLockedMilestonesReport
        BoundToLockedMilestonesReport report = result[0] as BoundToLockedMilestonesReport
        report.getLockedNodes() == [1L, 2L] as Set
    }

    def "should not detect locked nodes by milestone"() {
        given:
        def ids = [1L, 2L]
        deletionDao.filterRequirementsIdsWhichMilestonesForbidsDeletion(ids) >> []

        when:
        def result = handler.detectLockedByMilestone(ids)

        then:
        result.size() == 0
    }

    def "should detect locked nodes with active milestone -- SingleOrMultiple"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.of(createMilestone(1L))
        deletionDao.separateFolderFromRequirementIds(ids) >> [[1L], [2L, 3L, 4L]]
        deletionDao.filterRequirementHavingMultipleMilestones([2L, 3L, 4L]) >> [2L]
        requirementDao.findNonBoundRequirement([2L, 3L, 4L], 1L) >> [3L, 4L]
        requirementDao.filterRequirementHavingManyVersions([2L, 3L, 4L]) >> [3L]

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 4
        result[0] instanceof MilestoneModeNoFolderDeletion
        MilestoneModeNoFolderDeletion reportFolder = result[0] as MilestoneModeNoFolderDeletion
        reportFolder.getLockedNodes() == [1L] as Set

        and:
        result[1] instanceof SingleOrMultipleMilestonesReport
        SingleOrMultipleMilestonesReport reportBinding = result[1] as SingleOrMultipleMilestonesReport
        reportBinding.getLockedNodes() == [2L] as Set

        and:
        result[2] instanceof NotBoundToActiveMilestonesReport
        NotBoundToActiveMilestonesReport reportNotBound = result[2] as NotBoundToActiveMilestonesReport
        reportNotBound.getLockedNodes() == [3L, 4L] as Set

        result[3] instanceof MultipleReqVersionWithActiveMilestonesReport
        MultipleReqVersionWithActiveMilestonesReport reportReqVersion = result[3] as MultipleReqVersionWithActiveMilestonesReport
        reportReqVersion.getLockedNodes() == [3L] as Set
    }

    def "should detect locked nodes with active milestone -- BoundToMultiple"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.of(createMilestone(1L))
        deletionDao.separateFolderFromRequirementIds(ids) >> [[1L], [2L, 3L, 4L]]
        deletionDao.filterRequirementHavingMultipleMilestones([2L, 3L, 4L]) >> [2L, 3L, 4L]
        requirementDao.findNonBoundRequirement([2L, 3L, 4L], 1L) >> []
        requirementDao.filterRequirementHavingManyVersions([2L, 3L, 4L]) >> []

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 2
        result[0] instanceof MilestoneModeNoFolderDeletion
        MilestoneModeNoFolderDeletion reportFolder = result[0] as MilestoneModeNoFolderDeletion
        reportFolder.getLockedNodes() == [1L] as Set

        and:
        result[1] instanceof BoundToMultipleMilestonesReport
        BoundToMultipleMilestonesReport reportBinding = result[1] as BoundToMultipleMilestonesReport
        reportBinding.getLockedNodes() == [2L, 3L, 4L] as Set
    }

    def "should detect locked nodes with active milestone -- SingleMilestones"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.of(createMilestone(1L))
        deletionDao.separateFolderFromRequirementIds(ids) >> [[1L], [2L, 3L, 4L]]
        deletionDao.filterRequirementHavingMultipleMilestones([2L, 3L, 4L]) >> []
        requirementDao.findNonBoundRequirement([2L, 3L, 4L], 1L) >> []
        requirementDao.filterRequirementHavingManyVersions([2L, 3L, 4L]) >> []

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 2
        result[0] instanceof MilestoneModeNoFolderDeletion
        MilestoneModeNoFolderDeletion reportFolder = result[0] as MilestoneModeNoFolderDeletion
        reportFolder.getLockedNodes() == [1L] as Set

        and:
        result[1] instanceof SingleMilestonesReport
        SingleMilestonesReport reportBinding = result[1] as SingleMilestonesReport
        reportBinding.getLockedNodes() == [] as Set
    }

    def "should not detect locked nodes with no active milestone"() {
        given:
        def ids = [1L, 2L, 3L, 4L]
        activeMilestoneHolder.getActiveMilestone() >> Optional.empty()
        deletionDao.separateFolderFromRequirementIds(ids) >> [[1L], [2L, 3L, 4L]]
        deletionDao.filterRequirementHavingMultipleMilestones([2L, 3L, 4L]) >> [2L, 3L, 4L]
        requirementDao.findNonBoundRequirement([2L, 3L, 4L], 1L) >> []
        requirementDao.filterRequirementHavingManyVersions([2L, 3L, 4L]) >> []

        when:
        def result = handler.detectLockedWithActiveMilestone(ids)

        then:
        result.size() == 0
    }


    def createMilestone(Long id) {
        Milestone milestone = Mock(Milestone)
        milestone.getId() >> id
        return milestone
    }
}
