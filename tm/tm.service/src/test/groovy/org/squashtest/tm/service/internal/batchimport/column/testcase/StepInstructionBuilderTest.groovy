/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.column.testcase

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.squashtest.tm.service.importer.ImportMode
import org.squashtest.tm.service.internal.batchimport.instruction.StepInstruction
import org.squashtest.tm.service.internal.batchimport.column.CustomFieldColumnDef
import org.squashtest.tm.service.internal.batchimport.column.StdColumnDef
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet
import org.squashtest.tm.service.internal.batchimport.column.WorksheetDef
import spock.lang.Specification
import spock.lang.Unroll

/**
 * @author Gregory Fouquet
 *
 */
class StepInstructionBuilderTest extends Specification {
	WorksheetDef wd = Mock();
	Row row = Mock()
	StepInstructionBuilder builder

	def setup() {
		wd.getWorksheetType() >> TemplateWorksheet.STEPS_SHEET
		builder = new StepInstructionBuilder(wd)
	}

	private Cell mockCell(cellType, cellValue) {
		Cell cell = Mock()

		cell.getCellType() >> cellType

		cell.getNumericCellValue() >> cellValue
		cell.getStringCellValue() >> cellValue
		cell.getBooleanCellValue() >> cellValue
		cell.getDateCellValue() >> cellValue

		return cell
	}

	@Unroll
	def "should create test step target from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		StepInstruction instruction = builder.build(row)

		then:
		instruction.target[propName] == propValue

		where:
		col				                | cellType           | cellValue   | propName | propValue
        StepSheetColumn.TC_OWNER_PATH	| CellType.STRING    | "here/i/am" | "path"   | "here/i/am/steps/null"
        StepSheetColumn.TC_OWNER_PATH	| CellType.BLANK     | null        | "path"   | "null/steps/null"

        StepSheetColumn.TC_STEP_NUM 	| CellType.NUMERIC   | 20          | "index"  | 19
        StepSheetColumn.TC_STEP_NUM 	| CellType.STRING    | "20"        | "index"  | 19
        StepSheetColumn.TC_STEP_NUM		| CellType.BLANK     | null        | "index"  | null

	}

	@Unroll
	def "should create test step instruction from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30)]
		wd.getCustomFieldDefs() >> []

		when:
		StepInstruction instruction = builder.build(row)

		then:
		instruction[propName] == propValue

		where:
		col				                | cellType					| cellValue			| propName			| propValue
        StepSheetColumn.ACTION			| CellType.STRING		    | "DELETE"			| "mode"			| ImportMode.DELETE
        StepSheetColumn.ACTION			| CellType.STRING		    | "D"				| "mode"			| ImportMode.DELETE
        StepSheetColumn.ACTION			| CellType.BLANK		    | null				| "mode"			| ImportMode.UPDATE

	}

	@Unroll
	def "should create action test step from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		Cell typeCell = mockCell(CellType.NUMERIC, 0)
		row.getCell(40, _) >> typeCell
		def typeCellDef = new StdColumnDef(StepSheetColumn.TC_STEP_IS_CALL_STEP, 40)

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30), typeCellDef]
		wd.getColumnDef(StepSheetColumn.TC_STEP_IS_CALL_STEP) >> typeCellDef
		wd.getCustomFieldDefs() >> []

		and: builder = new StepInstructionBuilder(wd);

		when:
		StepInstruction instruction = builder.build(row)

		then:
		instruction.testStep[propName] == propValue

		where:
		col						                | cellType					| cellValue								| propName			| propValue
        StepSheetColumn.TC_STEP_ACTION			| CellType.STRING	    	| "i just want a lover like any other"	| "action"			| "i just want a lover like any other"
        StepSheetColumn.TC_STEP_ACTION			| CellType.BLANK		    | ""									| "action"			| ""

        StepSheetColumn.TC_STEP_EXPECTED_RESULT	| CellType.STRING		    | "what do i get"						| "expectedResult"	| "what do i get"
        StepSheetColumn.TC_STEP_EXPECTED_RESULT	| CellType.BLANK		    | ""									| "expectedResult"	| ""

	}
	@Unroll
	def "should create call test step from row with this bunch of data : #col #cellType #cellValue #propName #propValue"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		Cell typeCell = mockCell(CellType.BOOLEAN, true)
		row.getCell(40, _) >> typeCell
		def typeCellDef = new StdColumnDef(StepSheetColumn.TC_STEP_IS_CALL_STEP, 40)

		and:
		wd.getImportableColumnDefs() >> [new StdColumnDef(col, 30), typeCellDef]
		wd.getColumnDef(StepSheetColumn.TC_STEP_IS_CALL_STEP) >> typeCellDef
		wd.getCustomFieldDefs() >> []

		and: builder = new StepInstructionBuilder(wd);

		when:
		StepInstruction instruction = builder.build(row)

		then:
		instruction.calledTC[propName] == propValue

		where:
		col						                | cellType					| cellValue		| propName			| propValue
        StepSheetColumn.TC_STEP_ACTION			| CellType.STRING		    | "here/i/am"	| "path"			| "here/i/am"
        StepSheetColumn.TC_STEP_ACTION			| CellType.BLANK	    	| null			| "path"			| null

	}

	@Unroll
	def "should add custom field to instruction from row with this bunch of data : #cellType #cellValue #fieldCode"() {
		given:
		Cell cell = mockCell(cellType, cellValue)
		row.getCell(30, _) >> cell

		and:
		wd.getImportableColumnDefs() >> []
		wd.getCustomFieldDefs() >> [new CustomFieldColumnDef(fieldCode, 30)]

		when:
		StepInstruction instruction = builder.build(row)

		then:
		instruction.customFields[fieldCode] == cellValue

		where:
		cellType			 	| fieldCode	| cellValue
		CellType.STRING	        | "FOO"		| "bar"
		CellType.BLANK	        | "FOO"		| null
	}

}
