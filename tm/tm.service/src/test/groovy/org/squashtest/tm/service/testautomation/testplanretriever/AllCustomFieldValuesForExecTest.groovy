/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever

import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.service.testautomation.testplanretriever.AllCustomFieldValuesForExec
import spock.lang.Specification

class AllCustomFieldValuesForExecTest extends Specification {

    CustomFieldValue tcValue = Mock()
    CustomFieldValue itValue = Mock()
    CustomFieldValue cpgValue = Mock()
    CustomFieldValue tsValue = Mock()
    List<CustomFieldValue> tcValues = new ArrayList<>()
    List<CustomFieldValue> itValues = new ArrayList<>()
    List<CustomFieldValue> cpgValues = new ArrayList<>()
    List<CustomFieldValue> tsValues = new ArrayList<>()
    Map<Long, List<CustomFieldValue>> testCaseCfv = new HashMap<>()
    Map<Long, List<CustomFieldValue>> iterationCfv = new HashMap<>()
    Map<Long, List<CustomFieldValue>> campaignCfv = new HashMap<>()
    Map<Long, List<CustomFieldValue>> testSuiteCfv = new HashMap<>()

    AllCustomFieldValuesForExec testee

    def setup() {
        tcValues.add(tcValue)
        testCaseCfv.put(42L, tcValues)
        itValues.add(itValue)
        iterationCfv.put(43L, itValues)
        cpgValues.add(cpgValue)
        campaignCfv.put(44L, cpgValues)
        tsValues.add(tsValue)
        testSuiteCfv.put(45L, tsValues)

        testee = new AllCustomFieldValuesForExec(testCaseCfv, iterationCfv, campaignCfv, testSuiteCfv)
    }

    def "get value for iteration"() {
        when:
        List<CustomFieldValue> value = testee.getValueForIteration(43L)

        then:
        value == itValues
    }

    def "get value for campaign"() {
        when:
        List<CustomFieldValue> value = testee.getValueForCampaign(44L)

        then:
        value == cpgValues
    }

    def "get value for test suite"() {
        when:
        List<CustomFieldValue> value = testee.getValueForTestSuite(45L)

        then:
        value == tsValues
    }
}
