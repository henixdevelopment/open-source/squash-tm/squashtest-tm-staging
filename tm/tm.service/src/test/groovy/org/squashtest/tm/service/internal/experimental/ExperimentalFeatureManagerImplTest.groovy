/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.experimental

import org.squashtest.tm.core.foundation.exception.FeatureFlagNotEnabledException
import org.squashtest.tm.core.foundation.feature.ExperimentalFeature
import org.squashtest.tm.service.internal.feature.experimental.ExperimentalFeatureConfig
import org.squashtest.tm.service.internal.feature.experimental.ExperimentalFeatureManagerImpl
import spock.lang.Specification

// These tests will fail if the ExperimentalFeature enum is empty.
// A workaround could be to add a dummy value to ExperimentalFeature when that's the case.
class ExperimentalFeatureManagerImplTest extends Specification {
    def feature = ExperimentalFeature.values()[0]

    def "isEnabled should return true if the feature is enabled"() {
        given:
        def manager = new ExperimentalFeatureManagerImpl(withFeatureOn())

        expect:
        manager.isEnabled(feature)
    }

    def "isEnabled should return false if the feature is disabled"() {
        given:
        def manager = new ExperimentalFeatureManagerImpl(withFeatureOff())

        expect:
        !manager.isEnabled(feature)
    }

    def "isEnabled should return false if the feature is absent"() {
        given:
        def manager = new ExperimentalFeatureManagerImpl(withFeatureAbsent())

        expect:
        !manager.isEnabled(feature)
    }

    def "checkIfEnabled should not throw if the feature is enabled"() {
        given:
        def manager = new ExperimentalFeatureManagerImpl(withFeatureOn())

        when:
        manager.checkIfEnabled(feature)

        then:
        noExceptionThrown()
    }

    def "checkIfEnabled should throw an exception if the feature is disabled"() {
        given:
        def manager = new ExperimentalFeatureManagerImpl(withFeatureOff())

        when:
        manager.checkIfEnabled(feature)

        then:
        thrown(FeatureFlagNotEnabledException)
    }

    def "checkIfEnabled should throw an exception if the feature is absent"() {
        given:
        def manager = new ExperimentalFeatureManagerImpl(withFeatureAbsent())

        when:
        manager.checkIfEnabled(feature)

        then:
        thrown(FeatureFlagNotEnabledException)
    }

    ExperimentalFeatureConfig withFeatureOn() {
        def configuration = new ExperimentalFeatureConfig()
        configuration.setExperimental(Collections.singletonMap(feature.name(), true))
        configuration
    }

    ExperimentalFeatureConfig withFeatureOff() {
        def configuration = new ExperimentalFeatureConfig()
        configuration.setExperimental(Collections.singletonMap(feature.name(), false))
        configuration
    }

    ExperimentalFeatureConfig withFeatureAbsent() {
        def configuration = new ExperimentalFeatureConfig()
        configuration.setExperimental(Collections.emptyMap())
        configuration
    }
}
