/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.httpclient

import org.apache.http.HttpEntity
import org.apache.http.StatusLine
import org.apache.http.entity.ContentType
import org.opentestfactory.messages.OTFMessage
import org.opentestfactory.messages.Status
import org.squashtest.tm.service.internal.testautomation.httpclient.MessageRefusedException
import org.squashtest.tm.service.internal.testautomation.httpclient.Request
import org.squashtest.tm.service.internal.testautomation.httpclient.Response
import org.squashtest.tm.service.internal.testautomation.httpclient.SquashAutomBusClientImpl
import org.squashtest.tm.service.internal.testautomation.httpclient.UnexpectedServerResponseException
import org.squashtest.tm.service.internal.testautomation.model.messages.PublicationStatus
import spock.lang.Specification

class SquashAutomBusClientImplTest extends Specification{
    private static final String ENDPOINT = "http://someEndpoint.com"
    private static final String TOKEN = "someToken"

    private SquashAutomBusClientImpl client

    def setup() {
        client = new SquashAutomBusClientImpl(ENDPOINT, TOKEN, 15)
    }

    def "should create request"() {
        given:

        TestMessage message = new TestMessage("v1")

        when:

        URI uri = new URI(ENDPOINT + "/extension")
            Request request = client.createRequest(uri, message)

        then:
        request != null
    }

    def "should handle correct response"() {
        given:

        URI uri = new URI(ENDPOINT + "/extension")

        StatusLine statusLine = Mock()
        statusLine.getStatusCode() >> 200

            Response response = Mock()
        response.getStatusLine() >> statusLine

        HttpEntity entity = Mock()

        InputStream content =  this.getClass().getClassLoader().getResourceAsStream("squashautom/runningWorkflowStatus.json")
        entity.getContent() >> content

        response.getEntity() >> entity

        when:

        PublicationStatus finalStatus = client.handleResponse(response, uri)

        then:
        finalStatus != null
        finalStatus.getStatus() == Status.StatusValue.Success
    }

    def "should handle incorrect response (code > 400) with json content"() {
        given:

        URI uri = new URI(ENDPOINT + "/extension")

        StatusLine statusLine = Mock()
        statusLine.getStatusCode() >> 404

        Response response = Mock()
        response.getStatusLine() >> statusLine
        response.mimeType() >> ContentType.APPLICATION_JSON.getMimeType()

        HttpEntity entity = Mock()

        InputStream content =  this.getClass().getClassLoader().getResourceAsStream("squashautom/runningWorkflowStatus.json")
        entity.getContent() >> content

        response.getEntity() >> entity

        when:

        client.handleResponse(response, uri)

        then:
        thrown MessageRefusedException
    }

    def "should handle incorrect response (code > 400) with no content"() {
        given:

        URI uri = new URI(ENDPOINT + "/extension")

        StatusLine statusLine = Mock()
        statusLine.getStatusCode() >> 404

        Response response = Mock()
        response.getStatusLine() >> statusLine

        HttpEntity entity = Mock()

        InputStream content =  this.getClass().getClassLoader().getResourceAsStream("squashautom/runningWorkflowStatus.json")
        entity.getContent() >> content

        response.getEntity() >> entity

        when:

        client.handleResponse(response, uri)

        then:
        thrown UnexpectedServerResponseException
    }

    private static class TestMessage extends OTFMessage{
        TestMessage() {
            super("v1");
        }

        TestMessage(String apiVersion){
            super(apiVersion);
        }
    }
}
