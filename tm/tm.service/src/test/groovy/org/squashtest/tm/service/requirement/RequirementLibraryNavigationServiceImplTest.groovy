/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.requirement

import org.squashtest.tm.domain.attachment.AttachmentList
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.CustomField
import org.squashtest.tm.domain.customfield.CustomFieldBinding
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.requirement.HighLevelRequirementConverter
import org.squashtest.tm.domain.requirement.NewRequirementVersionDto
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.HighLevelRequirementCreator
import org.squashtest.tm.domain.requirement.RequirementFolder
import org.squashtest.tm.domain.requirement.RequirementLibrary
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.domain.resource.Resource
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService
import org.squashtest.tm.service.infolist.InfoListItemFinderService
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService
import org.squashtest.tm.service.internal.library.AbstractLibraryNavigationService
import org.squashtest.tm.service.internal.repository.RequirementDao
import org.squashtest.tm.service.internal.repository.RequirementFolderDao
import org.squashtest.tm.service.internal.repository.RequirementLibraryDao
import org.squashtest.tm.service.internal.requirement.RequirementFactory
import org.squashtest.tm.service.internal.requirement.RequirementLibraryNavigationServiceImpl
import org.squashtest.tm.service.milestone.MilestoneMembershipManager
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.testutils.MockFactory
import org.squashtest.tm.tools.unittest.reflection.ReflectionCategory
import spock.lang.Specification

class RequirementLibraryNavigationServiceImplTest extends Specification {

	RequirementLibraryNavigationServiceImpl service = new RequirementLibraryNavigationServiceImpl()
	RequirementLibraryDao requirementLibraryDao = Mock()
	RequirementFolderDao requirementFolderDao = Mock()
	RequirementDao requirementDao = Mock()
	PermissionEvaluationService permissionService = Mock()
	InfoListItemFinderService infoListItemService = Mock()
	MilestoneMembershipManager milestoneService = Mock()
	PrivateCustomFieldValueService customValueService = Mock()
	CustomFieldBindingFinderService customFieldBindingFinderService = Mock()
	MockFactory mockFactory = new MockFactory()

	RequirementVersion version;    // used in some hacks

	def setup() {
		NewRequirementVersionDto.metaClass.sameAs = {
			it.name == delegate.name &&
				it.description == delegate.description &&
				it.criticality &&
				it.reference == delegate.reference
		}

		service.requirementLibraryDao = requirementLibraryDao
		service.requirementFolderDao = requirementFolderDao
		service.requirementDao = requirementDao
		permissionService.hasRoleOrPermissionOnObject(_, _, _) >> true
		service.infoListItemService = infoListItemService
		service.milestoneService = milestoneService
		service.customFieldBindingFinderService = customFieldBindingFinderService
		service.customFieldValueService = customValueService
		service.requirementFactory = new RequirementFactory(new ArrayList<HighLevelRequirementCreator>(), new ArrayList<HighLevelRequirementConverter>())

		use(ReflectionCategory) {
			AbstractLibraryNavigationService.set(field: "permissionService", of: service, to: permissionService)
			AbstractLibraryNavigationService.set(field: "customFieldValuesService", of: service, to: customValueService)
		}

		customValueService.findAllCustomFieldValues(_) >> []
		customValueService.findAllCustomFieldValues(_, _) >> []

	}

	def "should add folder to library and persist the folder"() {
		given:
		RequirementFolder newFolder = Mock()

		and:
		Project project = Mock()
		newFolder.getProject() >> project
		project.getId() >> 10L
        AttachmentList attachmentList = Mock() {
            getId() >> 10L
        }
        newFolder.getAttachmentList() >> attachmentList

		and:
		CustomField cuf = Mock()
		cuf.getId() >> 4L

		BindableEntity entity1 = BindableEntity.CAMPAIGN
		BindableEntity entity2 = BindableEntity.CAMPAIGN

		CustomFieldBinding binding1 = Mock()
		CustomFieldBinding binding2 = Mock()

		binding1.getBoundEntity() >> entity1
		binding1.getCustomField() >> cuf

		binding2.getBoundEntity() >> entity2
		binding2.getCustomField() >> cuf

		List<CustomFieldBinding> bindings = [binding1, binding2]
		customFieldBindingFinderService.findCustomFieldsForProjectAndEntity(10L, BindableEntity.REQUIREMENT_FOLDER) >> bindings
        customValueService.findAllCustomFieldValues(newFolder) >> []

		and:
		RequirementLibrary lib = Mock()
		requirementLibraryDao.findById(10) >> lib
		newFolder.getContent() >> []
		when:
		service.addFolderToLibrary(10, newFolder)

		then:
		1 * lib.addContent(newFolder)
		1 * requirementFolderDao.persist(newFolder)
	}

	def "should create a Requirement in a RootContent"() {

		given:
		RequirementLibrary lib = Spy(new RequirementLibrary())
		def proj = mockFactory.createBasicProject()
		proj.setRequirementLibrary(lib)
		requirementLibraryDao.findById(1) >> lib

		and:
		def req = new NewRequirementVersionDto(name: "name", reference: "ref", category: "CAT_BUSINESS")

		when:
		def res = service.addRequirementToRequirementLibrary(1, req, [])

		then:
		1 * lib.addContent(_ as Requirement)
		1 * requirementDao.persist(_ as Requirement) >> {
			this.version = it.get(0).currentVersion
			use(ReflectionCategory) {
				Resource.set(field: 'id', of: version, to: 1l)
			}
		}
		res.getCurrentVersion().getName() == "name"
		res.getCurrentVersion().getReference() == "ref"
		res.getCurrentVersion().getId() == 1L
		res.getCategory().code == "CAT_BUSINESS"
	}


	def "should create a Requirement in a Folder"() {

		given:
		RequirementFolder folder = new RequirementFolder()
		def proj = mockFactory.createBasicProject()
		folder.notifyAssociatedWithProject(proj)
		requirementFolderDao.findByIdWithContent(1) >> folder

		and:

		def req = new NewRequirementVersionDto(name: "name", category: "CAT_BUSINESS")

		when:
		def res = service.addRequirementToRequirementFolder(1, req, [])

		then:
		1 * requirementDao.persist(_ as Requirement) >> {
			this.version = it.get(0).currentVersion
			use(ReflectionCategory) {
				Resource.set(field: 'id', of: version, to: 1l)
			}
		}
		res.getCurrentVersion().getName() == "name"
		res.getCurrentVersion().getId() == 1L
		res.getCategory().code == "CAT_BUSINESS"
		folder.getContent().size() == 1
	}

	def "should find library"() {
		given:
		RequirementLibrary l = Mock()
		requirementLibraryDao.findById(10) >> l

		when:
		def found = service.findLibrary(10)

		then:
		found == l
	}

    def "should create a Requirement in a requirement"() {

        given:
        Requirement requirement = new Requirement()
        def proj = mockFactory.createBasicProject()
        requirement.notifyAssociatedWithProject(proj)
        requirementDao.findByIdWithChildren(1) >> requirement

        and:

        def req = new NewRequirementVersionDto(name: "name", category: "CAT_BUSINESS")

        when:
        def res = service.addRequirementToRequirement(1, req, [])

        then:
        1 * requirementDao.persist(_ as Requirement) >> {
            this.version = it.get(0).currentVersion
            use(ReflectionCategory) {
                Resource.set(field: 'id', of: version, to: 1l)
            }
        }
        res.getCurrentVersion().getName() == "name"
        res.getCurrentVersion().getId() == 1L
        res.getCategory().code == "CAT_BUSINESS"
        requirement.getContent().size() == 1
    }

}
