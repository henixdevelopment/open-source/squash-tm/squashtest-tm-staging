/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion


import org.squashtest.tm.service.deletion.BoundToLockedMilestonesReport
import org.squashtest.tm.service.deletion.LinkedToIterationPreviewReport
import org.squashtest.tm.service.deletion.MultipleReqVersionWithActiveMilestonesReport
import org.squashtest.tm.service.deletion.NotBoundToActiveMilestonesReport
import org.squashtest.tm.service.deletion.SingleMilestonesReport
import org.squashtest.tm.service.deletion.SingleOrMultipleMilestonesReport
import org.squashtest.tm.service.internal.campaign.LockedRequirementNodeDetectionService
import org.squashtest.tm.service.internal.campaign.LockedTestCaseNodeDetectionService
import org.squashtest.tm.service.internal.repository.TestCaseDeletionDao
import org.squashtest.tm.service.internal.repository.TestCaseFolderDao
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import spock.lang.Specification

class RequirementNodeDeletionHandlerTest extends Specification {
    RequirementDeletionHandlerImpl handler = new RequirementDeletionHandlerImpl();
    LockedRequirementNodeDetectionService lockedNodeDetectionService = Mock();


	def setup(){
		handler.lockedNodeDetectionService = lockedNodeDetectionService;
	}

    def "should simulate requirement deletion with no error"() {
        given:
        lockedNodeDetectionService.detectLockedByMilestone(_) >> []
        lockedNodeDetectionService.detectLockedWithActiveMilestone(_) >> []

        when:
        def result = handler.diagnoseSuppression(_)

        then:
        result == []
    }

    def "should simulate test case deletion with error"() {
        given:
        lockedNodeDetectionService.detectLockedByMilestone(_) >> [Mock(BoundToLockedMilestonesReport)]
        lockedNodeDetectionService.detectLockedWithActiveMilestone(_) >> [Mock(BoundToLockedMilestonesReport)]

        when:
        def result = handler.diagnoseSuppression(_)

        then:
        result.size() == 2
    }

    def "should convert simulation report to a list of locked node ids"() {
        given:
        def ids = [1L, 2L, 3L, 4L, 5L]
        lockedNodeDetectionService.detectLockedByMilestone(_) >> [createLockedNodeIdsInReport(Mock(BoundToLockedMilestonesReport), [1L])]
        lockedNodeDetectionService.detectLockedWithActiveMilestone(_) >> [
            createLockedNodeIdsInReport(Mock(SingleOrMultipleMilestonesReport), [2L]),
            createLockedNodeIdsInReport(Mock(NotBoundToActiveMilestonesReport), [2L]),
        ]

        when:
        def lockedNodeIds = handler.detectLockedNodes(ids);

        then:
        lockedNodeIds == [1L, 2L]
    }

    def createLockedNodeIdsInReport = { report, ids ->
        report.getLockedNodes() >> ids
        return report
    }
}
