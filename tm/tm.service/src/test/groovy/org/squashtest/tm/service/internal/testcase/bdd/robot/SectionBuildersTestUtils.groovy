/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot

import org.squashtest.tm.domain.bdd.ActionWord
import org.squashtest.tm.domain.bdd.ActionWordParameter
import org.squashtest.tm.domain.bdd.ActionWordParameterValue
import org.squashtest.tm.domain.bdd.ActionWordText
import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.service.internal.testcase.bdd.KeywordTestStepActionWordParser

class ActionWordParameterMock extends ActionWordParameter {
	private Long id

	ActionWordParameterMock(Long id, String name, String defaultValue) {
		this.setName(name)
		this.setDefaultValue(defaultValue)
		this.id = id
	}

	Long getId() {
		return id
	}

	void setId(Long id) {
		this.id = id
	}
}

static ActionWord createBasicActionWord(String singleFragment) {
	def fragment = new ActionWordText(singleFragment)
	return new ActionWord([fragment] as List)
}

// Yeah, that's quite messy. That's revamped from the actual app code with a few changes to avoid interacting
// with the database. I think the code for creating and updating KeywordTestSteps would benefit from a good refactoring
static KeywordTestStep createActionWordStep(Keyword keyword, String rawText) {
	def parser = new KeywordTestStepActionWordParser()
	ActionWord actionWord = parser.createActionWordFromKeywordTestStep(rawText)
	def keywordTestStep = new KeywordTestStep(keyword, actionWord)

	for (int i = 0; i < actionWord.getActionWordParams().size(); ++i) {
		ActionWordParameter parameter = actionWord.getActionWordParams().get(i)
		parameter.setId(42L)
		ActionWordParameterValue newValue = new ActionWordParameterValue()
		newValue.setValue(parser.getParameterValues().get(i).getValue())

		newValue.setActionWordParam(parameter)
		newValue.setKeywordTestStep(keywordTestStep)

		if (newValue.isLinkedToTestCaseParam()) {
			String valueStr = newValue.getValue().trim()
			newValue.setValue(valueStr)
		}

		keywordTestStep.addParamValues(newValue)
	}

	return keywordTestStep
}
