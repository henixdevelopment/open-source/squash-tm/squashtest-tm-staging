/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher

import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao
import org.squashtest.tm.service.internal.repository.UserDao
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState
import org.squashtest.tm.service.testautomation.model.TfTestExecutionStatus
import spock.lang.Specification

class RestITPIManagerServiceTest extends Specification {

    IterationTestPlanDao itpiDaoMock = Mock()
    UserDao userDaoMock = Mock()
    CustomTestSuiteModificationService customTestSuiteModificationServiceMock = Mock()
    RestITPIManagerService testee = new RestITPIManagerService(
            itpiDao: itpiDaoMock,
            userDao: userDaoMock,
            customTestSuiteModificationService: customTestSuiteModificationServiceMock
    )

    def "forceExecutionStatus interactions test"() {

        given:
        IterationTestPlanItem item = Mock(IterationTestPlanItem) {
            getTestSuites() >> new ArrayList<TestSuite>()
        }
        String login = "login"
        User user = Mock(User) {
            getLogin() >> login
        }

        TestExecutionStatus testStatus = Mock(TestExecutionStatus) {
            getStatus() >> ExecutionStatus.SUCCESS
        }

            TfTestExecutionStatus tfTestExecutionStatus = Mock(TfTestExecutionStatus) {
            getStatus() >> ExecutionStatus.SUCCESS
        }

        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus() >> testStatus

            AutomatedExecutionState automatedExecutionState = Mock(AutomatedExecutionState) {
            getTfTestExecutionStatus() >> tfTestExecutionStatus
        }

        itpiDaoMock.findByIdWithTestCase(_) >> item
        userDaoMock.findUserByLogin(_) >> user
        tfTestExecutionStatus.tfTestExecutionStatusToTmTestExecutionStatus(_) >> testStatus

        when:
        testee.forceExecutionStatus(42L, automatedExecutionState)

        then:
        1 * customTestSuiteModificationServiceMock.updateExecutionStatus(_)
        1 * item.setExecutionStatus(org.squashtest.tm.domain.execution.ExecutionStatus.SUCCESS)
        1 * item.setLastExecutedBy(user.getLogin())
        1 * item.setLastExecutedOn(_)
        1 * item.setUser(user)
    }
}
