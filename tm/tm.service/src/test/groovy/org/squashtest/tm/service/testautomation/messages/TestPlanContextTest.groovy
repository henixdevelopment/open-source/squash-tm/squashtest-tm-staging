/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.messages

import org.squashtest.tm.api.plugin.EntityType
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext
import spock.lang.Specification
import spock.lang.Unroll

class TestPlanContextTest extends Specification {
    def "should return path"(){
        given:
            TestPlanContext testPlanContext = new TestPlanContext(EntityType.ITERATION, "name", "path", "uuid", "7.0.0")
        when:
            String path = testPlanContext.getEntityPath()
        then:
            path=="path > name"
    }

    def "cannot create TestPlanContext with invalid EntityType"(){
        when:
            new TestPlanContext(EntityType.CAMPAIGN, "name", "path", "uuid", "7.0.0")
        then:
            def e = thrown IllegalArgumentException
            e.getMessage() == "Unsupported type "+EntityType.CAMPAIGN
    }

    @Unroll
    def "cannot create TestPlanContext with blank path"(){
        when:
            new TestPlanContext(EntityType.TEST_SUITE, "name", path, "uuid", "7.0.0")
        then:
            def e = thrown IllegalArgumentException
            e.getMessage() == "Path cannot be blank."
        where:
            path << ["",null]
    }

    def "cannot create TestPlanContext with blank name"(){
        when:
        new TestPlanContext(EntityType.TEST_SUITE, name, "path", "uuid","7.0.0")
        then:
        def e = thrown IllegalArgumentException
        e.getMessage() == "Entity name cannot be blank."
        where:
        name << ["",null]
    }

    def "should return EntityType"(){
        given:
            TestPlanContext testPlanContext = new TestPlanContext(EntityType.TEST_SUITE, "name", "path", "uuid","7.0.0")
        when:
            String testSuite = testPlanContext.getEntityTypeAsString()
        then:
            testSuite=="test suite"
    }

    def "should return EntityUuid"(){
        given:
            TestPlanContext testPlanContext = new TestPlanContext(EntityType.TEST_SUITE, "name", "path", "tEstUuId","7.0.0")
        when:
            String uuid = testPlanContext.getEntityUuid()
        then:
            uuid == "tEstUuId"
    }
}
