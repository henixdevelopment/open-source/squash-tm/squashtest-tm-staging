/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.attachmentlist.querybuilder

import org.squashtest.tm.domain.EntityType
import spock.lang.Specification
import spock.lang.Unroll

class AttachmentListQueryBuilderTest extends Specification {

    def service = new AttachmentListQueryBuilder()

    @Unroll
    def "getLockedMilestonesQuery should return correct query for entityType"() {
        given:
        EntityType entityType = EntityType.valueOf(entityTypeValue)

        when:
        def result = service.getLockedMilestonesQuery(entityType)

        then:
        result != null

        where:
        entityTypeValue << AttachmentListQueryBuilder.STRATEGY_MAP.keySet().collect { it.name() }
    }

    def "should throw IllegalArgumentException for unsupported entity type"() {
        when:
        service.getLockedMilestonesQuery(EntityType.SESSION_NOTE)

        then:
        thrown(IllegalArgumentException)
    }

}
