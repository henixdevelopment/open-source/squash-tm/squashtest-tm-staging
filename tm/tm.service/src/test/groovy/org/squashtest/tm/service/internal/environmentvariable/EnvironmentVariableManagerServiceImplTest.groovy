/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable


import org.squashtest.tm.domain.environmentvariable.EVInputType
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable
import org.squashtest.tm.exception.NameAlreadyInUseException
import org.squashtest.tm.exception.RequiredFieldException
import org.squashtest.tm.exception.OptionAlreadyExistException
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentVariableManagerService
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingValueService
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao
import spock.lang.Specification

class EnvironmentVariableManagerServiceImplTest extends Specification {

	EnvironmentVariableManagerServiceImpl service
	EnvironmentVariableDao environmentVariableDao = Mock()
	EnvironmentVariableBindingService environmentVariableBindingService = Mock()
	EnvironmentVariableBindingValueService environmentVariableValueService = Mock()
	DenormalizedEnvironmentVariableManagerService denormalizedEnvironmentVariableManagerService = Mock()

	def setup() {
		service =
			new EnvironmentVariableManagerServiceImpl(environmentVariableDao,
                    environmentVariableBindingService,
                    environmentVariableValueService,
					denormalizedEnvironmentVariableManagerService)
	}

	def "should delete environment variable"() {
		given:
		EnvironmentVariable environmentVariable = Mock()
		List<Long> environmentVariableIds = Arrays.asList(1L)
		environmentVariableDao.getReferenceById(1L) >> environmentVariable

		when:
		service.deleteEnvironmentVariable(environmentVariableIds)

		then:
		1 * environmentVariableDao.delete(environmentVariable)
		noExceptionThrown()
	}

	def "should change name"() {
		given:
		String newName = "newName"
		String oldName = "oldName"
		EnvironmentVariable environmentVariable = new EnvironmentVariable(oldName, EVInputType.PLAIN_TEXT)
		environmentVariableDao.getReferenceById(1L) >> environmentVariable

		when:
		service.changeName(1L, newName)

		then:
		1 * environmentVariableDao.findByName(newName) >> null
		environmentVariable.getName() == newName
		noExceptionThrown()
	}

	def "should fail when change name"() {
		given:
		String newName = "newName"
		String oldName = "oldName"

		EnvironmentVariable existingEnvironmentVariable = new EnvironmentVariable(newName, EVInputType.PLAIN_TEXT)
		EnvironmentVariable environmentVariable = new EnvironmentVariable(oldName, EVInputType.PLAIN_TEXT)
		environmentVariableDao.getReferenceById(1L) >> environmentVariable

		when:
		service.changeName(1L, newName)

		then:
		1 * environmentVariableDao.findByName(newName) >> existingEnvironmentVariable
		thrown(NameAlreadyInUseException)
	}

	def "should change option label"() {
		given:
		String newOptionLabel = "newLabel"
		String oldOptionLabel = "oldLabel"
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption(oldOptionLabel, 0)
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("opt2", 1)
		options.addAll(option1, option2)
		environmentVariable.setOptions(options)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionLabel(1L, oldOptionLabel, newOptionLabel)

		then:
		environmentVariable.getOptions().get(0).label == newOptionLabel
		noExceptionThrown()
	}

	def "should fail when change option label with empty label"() {
		given:
		String newOptionLabel = ""
		String oldOptionLabel = "oldLabel"

		when:
		service.changeOptionLabel(1L, oldOptionLabel, newOptionLabel)

		then:
		thrown(RequiredFieldException)
	}

	def "should fail change option label when label already exist"() {
		given:
		String existOptionLabel = "existLabel"
		String oldOptionLabel = "existLabel"
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption(oldOptionLabel)
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("opt2")
		options.addAll(option1, option2)
		environmentVariable.setOptions(options)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionLabel(1L, oldOptionLabel, existOptionLabel)

		then:
		thrown(OptionAlreadyExistException)
	}

	def "should change option position"() {
		given:
		Long environmentVariableId = 1L
		Integer position = 2
		List<String> labelToMove = Arrays.asList("option1")

		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption("option1", 0)
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("option2", 1)
		EnvironmentVariableOption option3 = new EnvironmentVariableOption("option3", 2)
		options.addAll(option1, option2, option3)
		environmentVariable.setOptions(options)
		Long indexBeforeChange = environmentVariable.getOptions().indexOf(option1)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionsPosition(environmentVariableId, position, labelToMove)

		then:
		indexBeforeChange == 0
		environmentVariable.getOptions().indexOf(option1) == 2

	}


}
