/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics

import org.squashtest.tm.domain.Level
import org.squashtest.tm.domain.LevelUtils
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.domain.testcase.TestCaseImportance
import spock.lang.Specification
import spock.lang.Unroll

import static org.squashtest.tm.domain.execution.ExecutionStatus.*

class CountOnEnumTest extends Specification {


	def "should create a count on enum"(){
		expect:
		def countOnEnum = new CountOnEnum<>(ExecutionStatus.class)
		countOnEnum
	}

	def "should refuse null class"(){
		when:
		new CountOnEnum<>(null)

		then:
		thrown NullPointerException
	}

	def "should add a count and return stats"() {
		given:
		CountOnEnum<ExecutionStatus> countOnEnum = new CountOnEnum<>(ExecutionStatus.class)

		when:
		countOnEnum.add(READY, 2)
		countOnEnum.add(BLOCKED, 4)
		countOnEnum.add(READY, 5)

		then:
		def statistics = countOnEnum.getStatistics()
		statistics.size() == 13
		statistics.get(READY) == 7
		statistics.get(BLOCKED) == 4
		statistics.get(RUNNING) == 0
		statistics.get(SKIPPED) == 0
		statistics.get(CANCELLED) == 0
	}

	def "should add a count and return stats on subset of enum"() {
		given:
		CountOnEnum<ExecutionStatus> countOnEnum = new CountOnEnum<>(getCanonicalStatusSet())

		when:
		countOnEnum.add(READY, 2)
		countOnEnum.add(BLOCKED, 4)
		countOnEnum.add(READY, 5)

		then:
		def statistics = countOnEnum.getStatistics()
		statistics.size() == 9
		statistics.get(READY) == 7
		statistics.get(BLOCKED) == 4
		statistics.get(RUNNING) == 0
		statistics.get(SKIPPED) == 0
		statistics.get(CANCELLED) == 0
		!statistics.containsKey(NOT_RUN)
	}

	def "should order stats"() {
		expect:
		CountOnEnum<TestCaseImportance> countOnEnum = new CountOnEnum<>(TestCaseImportance.class)
		LinkedHashMap statistics = countOnEnum.getStatistics()
		statistics.size() == 4
		statistics.keySet() == new LinkedHashSet([TestCaseImportance.VERY_HIGH, TestCaseImportance.HIGH, TestCaseImportance.MEDIUM, TestCaseImportance.LOW])
	}

	@Unroll
	def "should create from tuples"() {
		when:
		def countOnEnum = CountOnEnum.fromTuples(tuple, ExecutionStatus.class)

		def statistics = countOnEnum.getStatistics()

		then:
		statistics.get(BLOCKED) == expectedBlocked
		statistics.get(RUNNING) == expectedRunning

		where:
		tuple                                                | expectedBlocked | expectedRunning
		[[BLOCKED, 2L].toArray(), ["BLOCKED", 1].toArray()]  | 3               | 0
		[["BLOCKED", 2].toArray(), ["RUNNING", 1].toArray()] | 2               | 1
	}

	@Unroll
	def "should reject tuples with null values"() {
		when:
		CountOnEnum.fromTuples(tuple, ExecutionStatus.class)

		then:
		thrown NullPointerException

		where:
		tuple                       | _
		[[null, 2].toArray()]       | _
		[[BLOCKED, null].toArray()] | _
		null                        | _
	}

	@Unroll
	def "should reject tuples with incorrect values"() {
		when:
		CountOnEnum.fromTuples(tuple, ExecutionStatus.class)

		then:
		thrown IllegalArgumentException

		where:
		tuple                                                            | _
		[[BLOCKED, 3].toArray(), [TestCaseImportance.HIGH, 2].toArray()] | _
		[[BLOCKED, 3].toArray(), ["HIGH", 2].toArray()]                  | _
		[[BLOCKED, "5"].toArray()]                                       | _
	}


	def "should map results with a key mapper"() {
		given:
		def aggregateOnEnum = new CountOnEnum(ExecutionStatus.class)

		when:
		aggregateOnEnum.add(BLOCKED,5)
		aggregateOnEnum.add(NOT_RUN,5)
		aggregateOnEnum.add(READY,5)
		aggregateOnEnum.add(RUNNING, 3)

		then:
		def statistics = aggregateOnEnum.getStatistics(DummyEnum.&fromExecutionStatus, DummyEnum.class)
		statistics.get(DummyEnum.ONE_VALUE) == 15
		statistics.get(DummyEnum.TWO_VALUE) == 3
	}

	enum DummyEnum implements Level {
		ONE_VALUE(1),
		TWO_VALUE(2);

		private final int level;

		DummyEnum(int level) {
			this.level = level;
		}

		@Override
		public String getI18nKey() {
			return null;
		}

		@Override
		public int getLevel() {
			return 0;
		}

		public static DummyEnum fromExecutionStatus(ExecutionStatus executionStatus) {
			def canonicalStatus = executionStatus.getCanonicalStatus()
			switch (canonicalStatus) {
				case BLOCKED:
				case READY:
					return ONE_VALUE;
				default:
					return TWO_VALUE;
			}
		}
	}
}
