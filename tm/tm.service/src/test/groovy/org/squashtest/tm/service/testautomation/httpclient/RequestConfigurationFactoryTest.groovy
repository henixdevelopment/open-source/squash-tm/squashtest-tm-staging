/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.httpclient

import org.apache.http.client.config.RequestConfig
import org.squashtest.tm.service.internal.testautomation.httpclient.ClientRuntimeException
import org.squashtest.tm.service.internal.testautomation.httpclient.RequestConfigurationFactory
import spock.lang.Specification

/**
 *
 * @author ericdegenetais
 */
class RequestConfigurationFactoryTest extends Specification{
    RequestConfigurationFactory testee=new RequestConfigurationFactory();

    def "should return default if not local"(){
        given:
            def defaultConfig = testee.defaultConfigBuilder(15).build()
        when:
            RequestConfig actual=testee.getRequestConfig(new URI("http://henix.fr"), 15);
        then:
            actual.toString() == defaultConfig.toString() // RequestConfig.equals doesn't compare by value...
    }

    def "should set source adresse ioloop if local"(){
        when:
            RequestConfig actual=testee.getRequestConfig(new URI("http://localhost"), 15);
        then:
            actual.getLocalAddress().getHostAddress() == "127.0.0.1"
    }

    def "should throw ClientRuntimeException when unknown host"(){
        when:
            RequestConfig actual=testee.getRequestConfig(new URI("http://domain-that-we-wont-create.demo.squashtest.org"), 15);
        then:
            thrown(ClientRuntimeException)
    }
}

