/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable

import org.squashtest.tm.domain.environmentvariable.EVBindableEntity
import org.squashtest.tm.domain.environmentvariable.EVInputType
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption

import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable
import org.squashtest.tm.exception.environmentvariable.ValueDoesNotMatchesPatternException
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao

import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao
import spock.lang.Specification

class EnvironmentVariableBindingValueServiceImplTest extends Specification {

	EnvironmentVariableBindingValueServiceImpl service
	ProjectDisplayDao projectDisplayDao = Mock()
	EnvironmentVariableBindingDao environmentVariableBindingDao = Mock()
	EnvironmentVariableDao environmentVariableDao = Mock()

	def setup() {
		service =
			new EnvironmentVariableBindingValueServiceImpl(
				projectDisplayDao,
				environmentVariableBindingDao,
				environmentVariableDao)
	}


	def "should update environment variable value from server view"() {
		given:
		Long bindingId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		EnvironmentVariable environmentVariable = new EnvironmentVariable("ev1", EVInputType.PLAIN_TEXT)
		binding.setEnvironmentVariable(environmentVariable)
		environmentVariable.getId() >> -1L
		environmentVariableBindingDao.getReferenceById(bindingId) >> binding

		when:
		service.editEnvironmentVariableValue(bindingId, "value")

		then:
		binding.getValue() === "value"
		noExceptionThrown()
	}

	def "should update environment variable value from project view "() {
		given:
		Long bindingId = -1L
		Long projectId= -1L

		EnvironmentVariable environmentVariable = Mock(EnvironmentVariable) {
			it.getId() >> -1L
			it.setName("ev1")
			it.setInputType(EVInputType.PLAIN_TEXT)
		}

		EnvironmentVariableBinding binding = new EnvironmentVariableBinding(environmentVariable, projectId, EVBindableEntity.PROJECT)
		environmentVariableBindingDao.getReferenceById(bindingId) >> binding

		when:
		service.editEnvironmentVariableValue(bindingId, "value")

		then:
		1 * environmentVariableBindingDao.getReferenceById(bindingId) >> binding
		binding.value == "value"
		noExceptionThrown()
	}

	def"should fail when updating environment variable with incorrect value"() {
		given:
		Long bindingId = -1L
		Long serverId = -1L
		EnvironmentVariable variable = new EnvironmentVariable("test", EVInputType.PLAIN_TEXT)
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding(variable, serverId, EVBindableEntity.TEST_AUTOMATION_SERVER, )
		environmentVariableBindingDao.getReferenceById(bindingId) >> binding

		when:
		service.editEnvironmentVariableValue(bindingId, "vâlue")

		then:
		thrown(ValueDoesNotMatchesPatternException)
	}

	def "should throw IllegalArgumentException when edit environment variable value if option value is invalid"() {
		given:
		Long bindingId = -1L
		Long serverId = -1L
		Long environmentVariableId = -1L

		EnvironmentVariableOption option1 = new EnvironmentVariableOption("option1")
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("option2")
		SingleSelectEnvironmentVariable environmentVariable = Mock(SingleSelectEnvironmentVariable) {
			it.getId() >> environmentVariableId
			it.getOptions() >> Arrays.asList(option1, option2)
		}

		EnvironmentVariableBinding binding = new EnvironmentVariableBinding(environmentVariable, serverId, EVBindableEntity.TEST_AUTOMATION_SERVER)

		when:
		service.editEnvironmentVariableValue(bindingId, "invalidValue")

		then:
		1 * environmentVariableBindingDao.getReferenceById(bindingId) >> binding
		1 * environmentVariableDao.findSingleSelectEnvironmentVariableById(environmentVariableId) >> environmentVariable
		thrown(IllegalArgumentException)
	}

	def "should reset default value from project view"() {
		given:
		Long bindingId = -1L
		String entityType = "PROJECT"
		Long projectId= -1L
		Long environmentVariableId = -1L
		Long testAutomationServerId = -1L
		EnvironmentVariableBinding serverBinding = new EnvironmentVariableBinding()
		serverBinding.setValue("serverValue")

		EnvironmentVariable environmentVariable = Mock(EnvironmentVariable) {
			it.getId() >> environmentVariableId
			it.setName("ev1")
			it.setInputType(EVInputType.PLAIN_TEXT)
		}

		EnvironmentVariableBinding binding = new EnvironmentVariableBinding(environmentVariable, projectId, EVBindableEntity.PROJECT)
		binding.setValue("projectValue")

		when:
		def result = service.resetDefaultValue(bindingId, entityType)

		then:
		1 * environmentVariableBindingDao.getReferenceById(bindingId) >> binding
		1 * projectDisplayDao.getTaServerIdByProjectId(projectId) >> testAutomationServerId
		1 * environmentVariableBindingDao.findByEntityIdTypeAndEvId(projectId, EVBindableEntity.TEST_AUTOMATION_SERVER, environmentVariableId) >> serverBinding
		result == serverBinding.value
		noExceptionThrown()
	}

	def "should reset default value from execution popup view"() {
		given:
		Long bindingId = -1L
		Long projectId= -1L
		String entityType = "EXECUTION_DIALOG"
		Long testAutomationServerId = -1L
		EnvironmentVariableBinding projectBinding = new EnvironmentVariableBinding()
		projectBinding.setValue("projectValue")

		when:
		def result = service.resetDefaultValue(bindingId, entityType)

		then:
		1 * environmentVariableBindingDao.getReferenceById(bindingId) >> projectBinding
		0 * projectDisplayDao.getTaServerIdByProjectId(projectId) >> testAutomationServerId
		result == projectBinding.value
		noExceptionThrown()
	}

	def "should reinitialize environment variable values by value and by environment variable id"() {
		given:
		List<String> values = Arrays.asList("oldValue")
		Long evId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		binding.setValue("oldValue")
		List<EnvironmentVariableBinding> bindings = Arrays.asList(binding)

		when:
		service.reinitializeEnvironmentVariableValuesByValueAndEvId(values, evId)

		then:
		1 * environmentVariableBindingDao.findAllByEvIdAndValues(evId, values) >> bindings
		binding.getValue() == ""
	}

	def "should replace environment variable values by evId"() {
		given:
		String oldValue = "oldValue"
		String newValue = "newValue"
		Long evId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		binding.setValue("oldValue")
		List<EnvironmentVariableBinding> bindings = Arrays.asList(binding)

		when:
		service.replaceAllExistingValuesByEvId(evId, oldValue, newValue)

		then:
		1 * environmentVariableBindingDao.findAllByEvIdAndValues(evId, Collections.singletonList(oldValue)) >> bindings
		binding.getValue() == newValue

	}
}

