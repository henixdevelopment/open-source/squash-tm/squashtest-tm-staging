/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign

import org.jooq.DSLContext
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStep
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testcase.ScriptedTestCase
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseVisitor
import org.squashtest.tm.domain.testcase.TestStep
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao
import org.squashtest.tm.service.internal.repository.TestSuiteDao
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.user.UserAccountService
import org.squashtest.tm.tools.unittest.reflection.ReflectionCategory
import spock.lang.Specification
import spock.lang.Unroll

class TestSuiteExecutionProcessingServiceImplTest  extends Specification {
	TestSuiteExecutionProcessingServiceImpl manager
	TestSuiteDao testSuiteDao = Mock()
	IterationTestPlanManager testPlanManager = Mock()
	UserAccountService userService = Mock()
	PermissionEvaluationService permissionEvaluationService = Mock()
	CampaignNodeDeletionHandler campaignNodeDeletionHandler = Mock()
	IterationTestPlanDao iterationTestPlanDao = Mock()
	DSLContext dslContext = Mock()

	def setup() {
		manager = new TestSuiteExecutionProcessingServiceImpl(
			campaignNodeDeletionHandler,
            testPlanManager,
			userService,
            permissionEvaluationService,
			iterationTestPlanDao,
            dslContext,
            testSuiteDao,
            Mock(TestSuiteDisplayDao))

		User user = Mock()
		user.getLogin() >> "admin"
		userService.findCurrentUser() >> user
	}

	def "should start new execution of test suite"() {
		given:
        Project project = Mock() {
            getId() >> 1L
        }

        TestCase test = new TestCase()
        test.steps = []

        IterationTestPlanItem item = Mock() {
            getId() >> 1L
            isTestCaseDeleted() >> false
            isExecutableThroughTestSuite() >> true
            getExecutions() >> []
            getReferencedTestCase() >> test
        }

		TestSuite suite = Mock() {
            getProject() >> project
            findFirstExecutableTestPlanItem(_) >> item
            getTestPlan() >> [item]
        }

		and:
		testSuiteDao.loadForExecutionResume(10) >> suite

		and:
        Execution exec = Mock() {
            getId() >> 1L
            getTestPlan() >> item
            getSteps() >> [Mock(ExecutionStep)]
        }

        testPlanManager.addExecution(_) >> exec

		when:
		def res = manager.startResume(10)

		then:
		res.executionId == exec.id
        res.testSuiteId == 10
	}

	@Unroll("should there have more items in test plan ? #moreExecutable !")
	def "should have more items in test plan"() {
		given:
		def suite = aSuiteWithExecutableItems(10L, 20L)

        Execution execution = Mock(Execution) {
            getTestPlan() >> Mock(IterationTestPlanItem) {
                getId() >> 10L
            }
        }

		when:
		def more = manager.hasNextTestCase(suite, "admin", execution)

		then:
		more

	}

	@Unroll("should there have more items in test plan ? #more !")
	def "should not have more items in test plan"() {
		given:
        def suite = aSuiteWithExecutableItems(10L, 20L)

        Execution execution = Mock(Execution) {
            getTestPlan() >> Mock(IterationTestPlanItem) {
                getId() >> 20L
            }
        }

        when:
        def more = manager.hasNextTestCase(suite, "admin", execution)

		then:
		!more

	}

	def "item should be the last executable of test plan"() {
		given:
		TestSuite testSuite = aSuiteWithExecutableItems(10L, 20L)

		and:
		TestCase testCase = new TestCase()
		IterationTestPlanItem item = new IterationTestPlanItem(testCase)
		User user = Mock()
		user.getLogin() >> "admin"
		use (ReflectionCategory) {
			IterationTestPlanItem.set field: "id", of: item, to: 30L
			IterationTestPlanItem.set field: "user", of: item, to: user
		}
		testSuite.iteration.addTestPlan(item)
		testSuite.bindTestPlanItem(item)

		and:
		IterationTestPlanItem otherItem = new IterationTestPlanItem(Mock(TestCase))
		testSuite.iteration.addTestPlan(otherItem)
		testSuite.bindTestPlanItem(otherItem)
		use (ReflectionCategory) {
			IterationTestPlanItem.set field: "id", of: otherItem, to: 40L
			IterationTestPlanItem.set field: "referencedTestCase", of: otherItem, to: null
			IterationTestPlanItem.set field: "user", of: item, to: user
		}

        and:
        Execution execution = Mock(Execution) {
            getTestPlan() >> testSuite.getTestPlan().get(1)
        }

		when:
        def res = manager.hasNextTestCase(testSuite, "admin", execution)

		then:
		!res
	}

	def "wrong item should not be the last of test plan"() {
		given:
		TestSuite testSuite = new TestSuite()
		Iteration iteration = new Iteration()
		testSuite.setIteration(iteration)

		and:
		TestCase testCase = new TestCase()
		IterationTestPlanItem item = new IterationTestPlanItem(testCase)
		User user = Mock()
		user.getLogin() >> "admin"
		use (ReflectionCategory) {
			IterationTestPlanItem.set field: "id", of: item, to: 10L
			IterationTestPlanItem.set field: "user", of: item, to: user
		}
		iteration.addTestPlan(item)
		testSuite.bindTestPlanItem(item)

        and:
        Execution execution = Mock(Execution) {
            getTestPlan() >> Mock(IterationTestPlanItem) {
                getId() >> 30L
            }
        }

		when:
		def res = manager.hasNextTestCase(testSuite, "admin", execution)

		then:
		res
	}

	def "item linked to Gherkin test should be the last executable of test plan if script have scenarios"() {
		given:
		TestSuite testSuite = aSuiteWithExecutableItems(10L, 20L)

		and:
		ScriptedTestCase testCase = new ScriptedTestCase()
		testCase.setScript("# language: fr\n" +
			"Fonctionnalité: migration\n" +
			"  \n" +
			"  Scénario: Vérifier les produits disponibles.")

		and:
		IterationTestPlanItem item = new IterationTestPlanItem(testCase)
		User user = Mock()
		user.getLogin() >> "admin"
		use (ReflectionCategory) {
			IterationTestPlanItem.set field: "id", of: item, to: 30L
			IterationTestPlanItem.set field: "user", of: item, to: user
		}
		testSuite.iteration.addTestPlan(item)
		testSuite.bindTestPlanItem(item)

        and:
        Execution execution = Mock(Execution) {
            getTestPlan() >> testSuite.getTestPlan().get(1)
        }

		when:
		def res = manager.hasNextTestCase(testSuite, "admin", execution)

		then:
		res
	}

	def "item linked to Gherkin test should not be the last executable of test plan if script doesn't have scenarios"() {
		given:
		TestSuite testSuite = aSuiteWithExecutableItems(10L, 20L)

		and:
		ScriptedTestCase testCase = new ScriptedTestCase()
		testCase.setScript("")

		and:
		IterationTestPlanItem item = new IterationTestPlanItem(testCase)
		User user = Mock()
		user.getLogin() >> "admin"
		use (ReflectionCategory) {
			IterationTestPlanItem.set field: "id", of: item, to: 30L
			IterationTestPlanItem.set field: "user", of: item, to: user
		}
		testSuite.iteration.addTestPlan(item)
		testSuite.bindTestPlanItem(item)

        and:
        Execution execution = Mock(Execution) {
            getTestPlan() >> testSuite.getTestPlan().get(1)
        }

		when:
		def res = manager.hasNextTestCase(testSuite, "admin",execution)

		then:
		!res
	}

	def aSuiteWithExecutableItems(Long... ids) {
		TestSuite testSuite = new TestSuite()
		Iteration iteration = new Iteration()
		testSuite.setIteration(iteration)

		ids.each { id ->
			TestCase testCase = Mock()
			testCase.accept(_) >> { TestCaseVisitor visitor -> visitor.visit(testCase) }
			TestStep testStep = Mock()
			testCase.getSteps() >> [testStep]
			IterationTestPlanItem item = new IterationTestPlanItem(testCase)
			User user = Mock()
			user.getLogin() >> "admin"
			use (ReflectionCategory) {
				IterationTestPlanItem.set field: "id", of: item, to: id
				IterationTestPlanItem.set field: "user", of: item, to: user
			}
			iteration.addTestPlan(item)
			testSuite.bindTestPlanItem(item)
		}

		return testSuite
	}

	def "should start next execution of test suite"() {
		given:
        Project project = Mock() {
            getId() >> 1L
        }

        TestCase test = new TestCase()
        test.steps = []

        IterationTestPlanItem nextItem = Mock() {
            isExecutableThroughTestSuite() >> true
            getId() >> 1L
            getReferencedTestCase() >> test
        }

        TestSuite suite = Mock() {
            getProject() >> project
            getTestPlan() >> [nextItem]
            findNextExecutableTestPlanItem(100, _) >> nextItem
        }

		and:
		testSuiteDao.loadForExecutionResume(10) >> suite

		and:
        Execution exec = Mock() {
            getId() >> 1L
            getTestPlan() >> nextItem
            getSteps() >> [Mock(ExecutionStep)]
        }
        testPlanManager.addExecution(_) >> exec

		when:
		def res = manager.startResumeNextExecution(10, 100)

		then:
        res.getExecutionId() == exec.id
        res.testSuiteId == 10
	}
}
