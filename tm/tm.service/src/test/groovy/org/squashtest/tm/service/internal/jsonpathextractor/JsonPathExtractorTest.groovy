/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.jsonpathextractor

import spock.lang.Specification

class JsonPathExtractorTest extends Specification {

    def "validPath test"() {
        given:
        def extractor = new JsonPathExtractorImpl()

        when:
        def paths = [
            "a", "abcde", "a2cd3", "a.bc.def", "salut.à.toi.le.Français",
            "[0]", "[1][0]", "[1][0][1984]", "node[1]", "node[1][0][1984]",
            "node[1].subnode[0][1984]", "node[1].subnode[0][1984].truc",
            "node.field[1].subnode[0][1984].truc", "[7].node.field[1].subnode[0][1984].truc",
            "[7].node.field[1].subn0de[0][1984].truc"
        ]

        then:
        paths.every { extractor.isPathValid(it) }
    }

    def "invalidPath test"() {
        given:
        def extractor = new JsonPathExtractorImpl()

        when:
        def paths = [
            "", "salut.à.toi.l'Ukrainien", "salut.", ".salut", "salut..toi",
            "[-0]", "[]", "[-1]", ".[0]", "[0].", "[2][-1]", "[1].[1]", "[1][1].",
            "[7]node.field[1].subnode[0][1984].truc", "[7].node.field[1]subnode[0][1984].truc",
            "[7].node.field[1].subnode[0][1984]truc", "node.[1].subnode[0][1984]truc",
            "node[1].subnode[0][1984].truc.", "node[1", "node[-1]", "node[]"
        ]

        then:
        !paths.every { extractor.isPathValid(it) }
    }

    def "basicScalarString test"() {
        given:
        def result = JsonPathExtractorImpl.extract("{\"a\": \"b\"}", "a")

        expect:
        result == "b"
    }

    def "basicScalarInteger test"() {
        given:
        def result = JsonPathExtractorImpl.extract("{\"a\": 62}", "a")

        expect:
        result == "62"
    }

    def "basicArrayString test"() {
        given:
        def result = JsonPathExtractorImpl.extract("[ \"aa\", \"bb\", \"cc\" ]", "[1]")

        expect:
        result == "bb"
    }

    def "basicArrayInteger test"() {
        given:
        def result = JsonPathExtractorImpl.extract("[ 11, 22, 33 ]", "[1]")

        expect:
        result == "22"
    }

    def "arrayInArray test"() {
        given:
        def result = JsonPathExtractorImpl.extract("[ \"aa\", [ \"bb\", \"cc\", \"dd\", \"ee\" ], \"ff\" ]", "[1][2]")

        expect:
        result == "dd"
    }

    def "dictInDict test"() {
        given:
        def result = JsonPathExtractorImpl.extract("{ \"aa\": { \"bb\": \"cc\", \"dd\": \"ee\", \"ff\": \"gg\" }, \"hh\": \"ii\" }", "aa.dd")

        expect:
        result == "ee"
    }

    def "dictInArray test"() {
        given:
        def result = JsonPathExtractorImpl.extract("[ { \"aa\": \"bb\", \"cc\": \"dd\", \"ee\": \"ff\" }, { \"gg\": \"hh\", \"ii\": \"jj\", \"kk\": \"ll\" } ]", "[1].ii")

        expect:
        result == "jj"
    }

    def "arrayInDict test"() {
        given:
        def result = JsonPathExtractorImpl.extract("{ \"aa\": \"bb\", \"cc\": [ \"dd\", \"ee\", \"ff\" ], \"gg\": \"hh\" }", "cc[2]")

        expect:
        result == "ff"
    }

    def "realWorld test"() {
        given:
        def result = JsonPathExtractorImpl.extract("{\"name\":\"John\", \"age\":30, \"cars\":[\"Ford\", \"BMW\", \"Fiat\"]}", "cars[2]")

        expect:
        result == "Fiat"
    }

    def "realWorld2 test"() {
        given:
        def result = JsonPathExtractorImpl.extract("{\"name\":\"John\", \"age\":30, \"cars\":[ { \"model\":\"Ford\", \"year\":2017 }, { \"model\":\"BMW\", \"year\":2018 }, { \"model\":\"Fiat\", \"year\":2019 } ]}", "cars[0].year")

        expect:
        result == "2017"
    }
}
