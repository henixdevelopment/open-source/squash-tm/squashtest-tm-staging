/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.remotesynchronisation

import org.squashtest.tm.exception.sync.PathAlreadyInUseException
import org.squashtest.tm.exception.sync.PathContainsASprintGroupException
import org.squashtest.tm.service.internal.repository.CustomRemoteSynchronisationDao
import spock.lang.Specification

class RemoteSynchronisationServiceImplTest extends Specification {

    RemoteSynchronisationServiceImpl service = new RemoteSynchronisationServiceImpl()

    CustomRemoteSynchronisationDao customRemoteSynchronisationDao = Mock()

    def setup() {
        service.customRemoteSynchronisationDao = customRemoteSynchronisationDao
    }

    def "should do nothing if path is available"() {
        given:
        def projectName = "Project 1"
        def pathToCheck = "This/Path/Is/Available"

        and:
        customRemoteSynchronisationDao.findAllRemoteSynchronisationOptionsFromProjectName(projectName) >> Collections.emptyList()

        when:
        service.checkPathAvailability(projectName, pathToCheck)

        then:
        noExceptionThrown()
    }

    def "should throw PathAlreadyInUseException if path is unavailable"() {
        given:
        def existingOptions = ["{\"synchronisationPath\":\"Already/Existing Path\"}"]
        def projectName = "Project 1"

        and:
        customRemoteSynchronisationDao.findAllRemoteSynchronisationOptionsFromProjectName(projectName) >> existingOptions

        when:
        service.checkPathAvailability(projectName, pathToCheck)

        then:
        thrown(PathAlreadyInUseException)

        where:
        pathToCheck << [
            "Already/Existing Path",
            " Already/Existing Path ",
            "Already  / Existing Path",
            "Already/Existing     Path",
            "  Already  /  Existing  Path "
        ]
    }

    def "should do nothing if sprint path is available"() {
        given:
        def projectName = "Project 1"
        def pathToCheck = "This/Sprint Path/Is/Available"

        and:
        customRemoteSynchronisationDao.findAllRemoteSynchronisationOptionsFromProjectName(projectName) >> Collections.emptyList()

        when:
        service.checkSprintPathAvailability(projectName, pathToCheck)

        then:
        noExceptionThrown()
    }

    def "should throw an exception if sprint path is unavailable"() {
        given:
        def existingOptions = ["{\"sprintSynchronisationPath\":\"Already/Existing/Sprint Path\"}"]
        def projectName = "Project 1"

        and:
        customRemoteSynchronisationDao.findAllRemoteSynchronisationOptionsFromProjectName(projectName) >> existingOptions

        when:
        service.checkSprintPathAvailability(projectName, pathToCheck)

        then:
        thrown(expectedException)

        where:
        pathToCheck                                  | expectedException
        "Already/Existing/Sprint Path"               | PathAlreadyInUseException
        " Already/Existing/Sprint Path "             | PathAlreadyInUseException
        "Already  /Existing / Sprint   Path"         | PathAlreadyInUseException
        "Already/Existing/Sprint Path/and more"      | PathContainsASprintGroupException
        " Already/Existing/Sprint Path/and more "    | PathContainsASprintGroupException
        "Already /Existing / Sprint   Path/and more" | PathContainsASprintGroupException
    }
}
