/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.opentestfactory.messages.Status
import org.squashtest.squash.automation.tm.commons.ExpectedSuiteDefinition
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import org.squashtest.tm.service.internal.library.EntityPathHeaderService
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao
import org.squashtest.tm.service.internal.testautomation.BuildDef
import org.squashtest.tm.service.internal.testautomation.StartTestExecution
import org.squashtest.tm.service.internal.testautomation.httpclient.BusClientFactory
import org.squashtest.tm.service.internal.testautomation.httpclient.SquashAutomBusClient
import org.squashtest.tm.service.internal.testautomation.httpclient.SquashAutomWorkflowClient
import org.squashtest.tm.service.internal.testautomation.httpclient.WorkflowClientFactory
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext
import org.squashtest.tm.service.internal.testautomation.model.messages.ExecutionError
import org.squashtest.tm.service.internal.testautomation.model.messages.PublicationStatus
import org.squashtest.tm.service.internal.testautomation.model.messages.WorkflowHandle
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration
import spock.lang.Specification

/**
 *
 * @author ericdegenetais
 */
class StartTestExecutionTest extends Specification {

    private BusClientFactory busClientFactory=Mock()
    private SquashAutomBusClient busClient=Mock()
    private TestPlanContext testPlanContext=Mock()

    private WorkflowClientFactory workflowClientFactory=Mock()
    private SquashAutomWorkflowClient workflowClient=Mock()

    private BuildDef buildDef=Mock()

    private PublicationStatus status=Mock()
    private EntityPathHeaderDao entityPathHeaderDao = Mock()
    private StartTestExecution testee

    def setup(){
        busClientFactory.getClient(_,_,_) >> busClient
        workflowClientFactory.getClient(_,_,_,_,_) >> workflowClient

        TestAutomationServer server=Mock()
        server.getUrl() >> "https://tf.exemple.com:8665/"
        TokenAuthCredentials credentials=Mock()
        credentials.getToken() >> "Hush !!! Tell no one!"

        buildDef.getServer() >> server
        buildDef.getCredentials() >> credentials
        buildDef.getParameterizedItems() >> new java.util.ArrayList()

        status.getDetails() >> Collections.emptyMap()
        def handle = Mock(WorkflowHandle)
        workflowClient.launch(_) >> handle

        testee=new StartTestExecution(
                buildDef,
                [new SquashAutomExecutionConfiguration(3L, ["default"], ["robotframework", "ssh"], new HashMap<String, String>(),  null),
                 new SquashAutomExecutionConfiguration(5L, ["default"], ["cucumber", "linux"], new HashMap<String, String>(), null)],
                "suiteId",
                testPlanContext,
                "https://tm.exemple.com/callback",
                "tmUserToken",
                workflowClientFactory,
                busClientFactory,
                entityPathHeaderDao,
                15,
            true
        )
    }

    def "Publish Error when expected suite data not transmitted."(){
        given:
            status.getStatus() >> Status.StatusValue.Failure
        when:
            testee.run()
        then:
            1 * busClient.publish({event -> event instanceof ExpectedSuiteDefinition}) >> status
            1 * busClient.publish({event -> event instanceof ExecutionError})
    }

    def "No Error when expected suite data transmitted."(){
        given:
            status.getStatus() >> Status.StatusValue.Success
        when:
            testee.run()
        then:
            1 * busClient.publish({event -> event instanceof ExpectedSuiteDefinition}) >> status
            0 * busClient.publish({event -> event instanceof ExecutionError})
    }
}

