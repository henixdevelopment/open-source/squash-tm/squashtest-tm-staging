/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.report.spring.view.docxtemplater;

import static org.squashtest.tm.api.report.DocxTemplaterReport.CUSTOM_TEMPLATE_FOLDER_PATH;
import static org.squashtest.tm.api.report.DocxTemplaterReport.DEFAULT_TEMPLATE_FILE_PATH;
import static org.squashtest.tm.api.report.DocxTemplaterReport.TEMPLATE_FILE_NAME;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.view.AbstractView;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;

public class DocxTemplaterDocxView extends AbstractView {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocxTemplaterDocxView.class);

    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }
    ;

    @Override
    protected void renderMergedOutputModel(
            Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {

        final String templateFilePath = retrieveTemplateFilePath(model);

        Resource resource = getApplicationContext().getResource(templateFilePath);

        try {
            InputStream inputStream = resource.getInputStream();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=" + resource.getFilename());
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
            inputStream.close();
            // WARNING!! it previously caught all Exceptions
        } catch (IOException e) {
            LOGGER.debug("file doesn't exist {}", resource.getFilename(), e);
        }
    }

    public Resource downloadDefaultTemplate(String templateFileName) {
        return getApplicationContext().getResource(templateFileName);
    }

    private String retrieveTemplateFilePath(Map<String, Object> model) {
        final String templateFromModel = (String) model.get(TEMPLATE_FILE_NAME);

        if (StringUtils.hasText(templateFromModel)) {
            final String customTemplateFolderPath = (String) model.get(CUSTOM_TEMPLATE_FOLDER_PATH);
            final String customTemplateFilePath = customTemplateFolderPath.concat(templateFromModel);
            if (new File(customTemplateFilePath).exists()) {
                return String.format("File:%s", customTemplateFilePath);
            }
        }

        return (String) model.get(DEFAULT_TEMPLATE_FILE_PATH);
    }
}
