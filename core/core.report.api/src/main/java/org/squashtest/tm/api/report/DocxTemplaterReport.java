/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.report;

import org.springframework.context.MessageSource;

/**
 * DocxTemplater report. Has essentially the same behavior as BasicReport, but now the template is
 * dynamic, so it sets: - the folder path that can contain custom templates for this docx report -
 * and the default template file path.
 *
 * @author bsiri
 * @author Gregory Fouquet
 */
public class DocxTemplaterReport extends BasicReport {
    public static final String DEFAULT_TEMPLATE_FILE_PATH = "defaultTemplateFilePath";
    public static final String TEMPLATE_FILE_NAME = "templateFileName";
    public static final String CUSTOM_TEMPLATE_FOLDER_PATH = "customTemplateFolderPath";
    public static final String DOCX_FILE_EXTENSION = "docx";

    private String customTemplateFolderPath;
    private String defaultTemplateFilePath;
    private MessageSource messageSource;

    public String getCustomTemplateFolderPath() {
        return customTemplateFolderPath;
    }

    // customTemplateFolderPath value is set by the plugin
    public void setCustomTemplateFolderPath(String customTemplateFolderPath) {
        this.customTemplateFolderPath = customTemplateFolderPath;
    }

    public String getDefaultTemplateFilePath() {
        return defaultTemplateFilePath;
    }

    // defaultTemplateFilePath value is set by the plugin
    public void setDefaultTemplateFilePath(String defaultTemplateFilePath) {
        this.defaultTemplateFilePath = defaultTemplateFilePath;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * @see
     *     org.squashtest.tm.core.foundation.i18n.ContextBasedInternationalized#initializeMessageSource(org.springframework.context.MessageSource)
     */
    @Override
    protected void initializeMessageSource(MessageSource messageSource) {
        super.initializeMessageSource(messageSource);
        this.messageSource = messageSource;
    }
}
