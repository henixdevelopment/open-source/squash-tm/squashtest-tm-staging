/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.report.form;

import static org.squashtest.tm.api.report.DocxTemplaterReport.DOCX_FILE_EXTENSION;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import org.apache.commons.io.FilenameUtils;

public class TemplateDropdownList extends OptionsGroup {
    private String folderPath;
    private String fileDoesNotExistMessageKey;

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public String getFileDoesNotExistMessage() {
        return getMessage(fileDoesNotExistMessageKey);
    }

    public void setFileDoesNotExistMessageKey(String fileDoesNotExistMessageKey) {
        this.fileDoesNotExistMessageKey = fileDoesNotExistMessageKey;
    }

    /**
     * @see Input#getType()
     */
    @Override
    public InputType getType() {
        return InputType.TEMPLATE_DROPDOWN_LIST;
    }

    @Override
    public List<OptionInput> getOptions() {
        if (Objects.nonNull(folderPath)) {
            File[] customTemplates = new File(folderPath).listFiles();
            if (Objects.nonNull(customTemplates) && customTemplates.length > 0) {
                List<OptionInput> opts = new ArrayList<>();
                opts.add(super.getOptions().get(0));
                opts.addAll(retrieveTemplateOptionInputs(customTemplates));
                return opts;
            }
        }

        return super.getOptions();
    }

    private List<OptionInput> retrieveTemplateOptionInputs(File[] customTemplates) {
        return Stream.of(customTemplates)
                .filter(customTemplate -> !customTemplate.isDirectory())
                .map(File::getName)
                .filter(
                        customTemplateName ->
                                DOCX_FILE_EXTENSION.equals(FilenameUtils.getExtension(customTemplateName)))
                .map(
                        customTemplateName -> {
                            OptionInput opt = new OptionInput(customTemplateName);
                            opt.setLabelKey(customTemplateName);
                            return opt;
                        })
                .toList();
    }

    @Override
    public void accept(InputVisitor visitor) {
        visitor.visit(this);
    }
}
