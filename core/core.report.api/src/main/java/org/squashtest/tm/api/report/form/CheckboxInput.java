/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.report.form;

/**
 * @author Gregory
 */
public class CheckboxInput extends OptionInput implements Input {

    private String helpI18nKey;

    /**
     * @see org.squashtest.tm.api.report.form.Input#getType()
     */
    @Override
    public InputType getType() {
        return InputType.CHECKBOX;
    }

    @Override
    public void accept(OptionInputVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void accept(InputVisitor inputVisitor) {
        inputVisitor.visit(this);
    }

    @Override
    public String getHelpI18nKey() {
        return helpI18nKey;
    }

    public void setHelpI18nKey(String helpI18nKey) {
        this.helpI18nKey = helpI18nKey;
    }

    public String getHelpMessage() {
        return getMessage(helpI18nKey);
    }
}
