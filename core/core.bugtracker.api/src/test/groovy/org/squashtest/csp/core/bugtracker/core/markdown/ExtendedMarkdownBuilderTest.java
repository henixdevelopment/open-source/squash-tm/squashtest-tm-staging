/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.csp.core.bugtracker.core.markdown;

import org.junit.Assert;
import org.junit.Test;

public class ExtendedMarkdownBuilderTest {

    @Test
    public void buildMarkdown() {
        ExtendedMarkdownBuilder builder = new ExtendedMarkdownBuilder();

        String result =
                builder
                        .appendHeader1("Testing builder")
                        .appendPlainText("Plain test")
                        .appendCodeBlock("var hello = 'world';")
                        .appendQuoteBlock(
                                new ExtendedMarkdownBuilder()
                                        .appendHeader2("Block title")
                                        .appendParagraph("First paragraph")
                                        .appendParagraph("Second paragraph")
                                        .appendPlainText("some text followed by ")
                                        .appendPlainText(builder.link("a link", "url"))
                                        .appendCodeBlock(
                                                new ExtendedMarkdownBuilder()
                                                        .appendHeader3("small title inside code block")
                                                        .appendPlainText("// some code")))
                        .appendPlainText("And that's all, folks !")
                        .build();

        String expected =
                "# Testing builder\n"
                        + "\n"
                        + "Plain test\n"
                        + "\n"
                        + "```\n"
                        + "var hello = 'world';\n"
                        + "```\n"
                        + "\n"
                        + ">>>\n"
                        + "\n"
                        + "## Block title\n"
                        + "\n"
                        + "First paragraph\n"
                        + "\n"
                        + "Second paragraph\n"
                        + "\n"
                        + "some text followed by \n"
                        + "[a link](url)\n"
                        + "\n"
                        + "```\n"
                        + "\n"
                        + "### small title inside code block\n"
                        + "\n"
                        + "// some code\n"
                        + "```\n"
                        + "\n"
                        + ">>>\n"
                        + "\n"
                        + "And that's all, folks !";

        Assert.assertEquals(expected, result);
    }
}
