/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context;

public class StandardExecutionStepInfo extends ExecutionStepInfo {

    private final String action;
    private final String htmlAction;
    private final String expectedResult;
    private final String htmlExpectedResult;

    public StandardExecutionStepInfo(
            Long id,
            int executionStepOrder,
            String action,
            String htmlAction,
            String expectedResult,
            String htmlExpectedResult) {
        super(TestCaseInfo.Kind.STANDARD, id, executionStepOrder);
        this.action = action;
        this.htmlAction = htmlAction;
        this.expectedResult = expectedResult;
        this.htmlExpectedResult = htmlExpectedResult;
    }

    /* No args constructor for serialization only ! */
    public StandardExecutionStepInfo() {
        action = null;
        expectedResult = null;
        htmlAction = null;
        htmlExpectedResult = null;
    }

    public String getAction() {
        return action;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public String getHtmlAction() {
        return htmlAction;
    }

    public String getHtmlExpectedResult() {
        return htmlExpectedResult;
    }
}
