/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SelectorField extends Field {

    private List<Field> fields;

    public SelectorField(String id, String label) {
        super(id, label);
        Rendering rendering = new Rendering();
        rendering.setInputType(
                new InputType(
                        InputType.TypeName.SELECTOR_FIELD.value, InputType.TypeName.SELECTOR_FIELD.value));
        this.setRendering(rendering);
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        Set<String> fieldIds = fields.stream().map(Field::getId).collect(Collectors.toSet());
        if (fields.size() != fieldIds.size()) {
            throw new IllegalArgumentException("Violation of unicity constraint.");
        }

        this.fields = fields;
    }
}
