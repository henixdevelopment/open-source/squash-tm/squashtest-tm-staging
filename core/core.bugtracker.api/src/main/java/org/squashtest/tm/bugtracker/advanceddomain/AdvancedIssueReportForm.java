/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;

/**
 * Advanced BT connectors can return this object instead of a regular RemoteIssue or AdvancedIssue,
 * which in turn allows the connector to work with the additional context when, for example,
 * executing delegate commands.
 *
 * <p>Note: this class inherits AdvancedIssue only to avoid breaking compatibility with existing
 * connectors. A composition would be better suited because what we're returning IS NOT an issue,
 * it's a form for creating one. But doing such a refactoring would have too much impacts.
 */
public class AdvancedIssueReportForm extends AdvancedIssue {
    private RemoteIssueContext remoteIssueContext;
    private boolean hasCacheError;

    public RemoteIssueContext getRemoteIssueContext() {
        return remoteIssueContext;
    }

    public void setRemoteIssueContext(RemoteIssueContext remoteIssueContext) {
        this.remoteIssueContext = remoteIssueContext;
    }

    public boolean isHasCacheError() {
        return hasCacheError;
    }

    public void setHasCacheError(boolean hasCacheError) {
        this.hasCacheError = hasCacheError;
    }
}
