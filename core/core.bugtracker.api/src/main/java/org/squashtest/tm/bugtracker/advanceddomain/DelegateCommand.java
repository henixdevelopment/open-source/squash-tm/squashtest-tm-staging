/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import java.util.Map;

/**
 * This class represents an arbitrary command that a widget of Squash UI may ask the bugtracker
 * connector to perform in order to enhance its behaviour. It may happen for instance if {@link
 * InputType#getConfiguration()} contains certain values (such as {@link
 * CommonConfigurationKey#ONCHANGE}), that native Squash widget can handle (that are listed in
 * {@link InputType})
 *
 * @author bsiri
 */
public class DelegateCommand {

    private String command;

    private Object argument;

    private InvocationContext context;

    public DelegateCommand() {
        super();
    }

    public DelegateCommand(String command, Object argument) {
        super();
        this.command = command;
        this.argument = argument;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Object getArgument() {
        return argument;
    }

    public void setArgument(Object argument) {
        this.argument = argument;
    }

    public InvocationContext getContext() {
        return context;
    }

    public void setContext(InvocationContext context) {
        this.context = context;
    }

    /** Provide additional context for a delegate command such as the remote project name. */
    public static class InvocationContext {
        /** Currently selected remote project in the report form. */
        private String remoteProject;

        /** The report form used. */
        private AdvancedIssueReportForm reportForm;

        private Map<String, Object> remoteIssueCurrentFormRequest;

        private Map<String, Object> boundEntityInfo;

        public InvocationContext() {
            // NOOP
        }

        public String getRemoteProject() {
            return remoteProject;
        }

        public void setRemoteProject(String remoteProject) {
            this.remoteProject = remoteProject;
        }

        public AdvancedIssueReportForm getReportForm() {
            return reportForm;
        }

        public void setReportForm(AdvancedIssueReportForm reportForm) {
            this.reportForm = reportForm;
        }

        public Map<String, Object> getRemoteIssueCurrentFormRequest() {
            return remoteIssueCurrentFormRequest;
        }

        public void setRemoteIssueCurrentFormRequest(
                Map<String, Object> remoteIssueCurrentFormRequest) {
            this.remoteIssueCurrentFormRequest = remoteIssueCurrentFormRequest;
        }

        public Map<String, Object> getBoundEntityInfo() {
            return boundEntityInfo;
        }

        public void setBoundEntityInfo(Map<String, Object> boundEntityInfo) {
            this.boundEntityInfo = boundEntityInfo;
        }
    }
}
