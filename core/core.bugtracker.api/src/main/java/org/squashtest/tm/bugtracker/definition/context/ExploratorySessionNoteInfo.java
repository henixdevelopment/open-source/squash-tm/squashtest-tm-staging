/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context;

public class ExploratorySessionNoteInfo extends ExecutionSubItemInfo {

    public enum Kind {
        COMMENT,
        SUGGESTION,
        BUG,
        QUESTION,
        POSITIVE
    }

    private final String content;
    private final String htmlContent;
    private final Kind kind;

    public ExploratorySessionNoteInfo(long id, String content, String htmlContent, Kind kind) {
        super(TestCaseInfo.Kind.EXPLORATORY, id);
        this.content = content;
        this.htmlContent = htmlContent;
        this.kind = kind;
    }

    /* No args constructor for serialization only ! */
    public ExploratorySessionNoteInfo() {
        content = null;
        htmlContent = null;
        kind = null;
    }

    public String getContent() {
        return content;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public Kind getKind() {
        return kind;
    }
}
