/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.util.Assert;

/**
 * Describe a set of changes to be applied to a report form. It can be used by connectors as a
 * response to a delegate command. Needs to be fully serializable.
 *
 * <p>Please note that the front-end makes use of the full qualified name of this class. If it gets
 * renamed or moved to another package, the changes must be reflected front-side.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "class")
public class ChangeSet {

    private final List<Change> changes = new ArrayList<>();

    public static ChangeSet forNewValue(String fieldId, FieldValue newValue) {
        ChangeSet changeSet = new ChangeSet();
        changeSet.changes.add(new Change(ChangeKind.REPLACE_VALUE, fieldId, newValue, null));
        return changeSet;
    }

    public static ChangeSet forNewPossibleValues(String fieldId, FieldValue... newValues) {
        ChangeSet changeSet = new ChangeSet();
        changeSet.changes.add(
                new Change(
                        ChangeKind.REPLACE_POSSIBLE_VALUES, fieldId, null, Arrays.asList(newValues.clone())));
        return changeSet;
    }

    public static ChangeSet forAppendedPossibleValues(String fieldId, FieldValue... newValues) {
        ChangeSet changeSet = new ChangeSet();
        changeSet.changes.add(
                new Change(
                        ChangeKind.APPEND_POSSIBLE_VALUES, fieldId, null, Arrays.asList(newValues.clone())));
        return changeSet;
    }

    public List<Change> getChanges() {
        return changes;
    }

    public List<Change> getChangesForField(String fieldId) {
        return this.changes.stream().filter(change -> change.fieldId.equals(fieldId)).toList();
    }

    public enum ChangeKind {
        REPLACE_VALUE,
        REPLACE_POSSIBLE_VALUES,
        APPEND_POSSIBLE_VALUES
    }

    public static class Change {
        public final ChangeKind kind;
        public final String fieldId;
        public final FieldValue newValue;
        public final List<FieldValue> newPossibleValues;

        public Change(
                @NotNull ChangeKind kind,
                @NotNull @NotBlank String fieldId,
                @Nullable FieldValue newValue,
                @Nullable List<FieldValue> newPossibleValues) {
            Assert.notNull(kind, "Change kind cannot be null");
            Assert.notNull(fieldId, "Field ID cannot be null");

            this.kind = kind;
            this.fieldId = fieldId;
            this.newValue = newValue;
            this.newPossibleValues = newPossibleValues;
        }
    }
}
