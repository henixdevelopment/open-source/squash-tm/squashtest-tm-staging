/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import java.util.Collections;
import java.util.List;

/**
 * Defines the fields to display when searching for a remote issue in Squash. This class needs to be
 * fully serializable as it'll be sent to the client.
 */
public class RemoteIssueSearchForm {

    private final List<Field> fields;

    private final String focusedField;

    /** Select which fields should retain their state when "Add another" is clicked. */
    private List<String> fieldsToRetain;

    public RemoteIssueSearchForm(List<Field> fields, String focusedField) {
        this.fields = fields;
        this.focusedField = focusedField;
    }

    public RemoteIssueSearchForm(
            List<Field> fields, String focusedField, List<String> fieldsToRetain) {
        this.fields = fields;
        this.focusedField = focusedField;
        this.fieldsToRetain = fieldsToRetain;
    }

    public List<Field> getFields() {
        return fields;
    }

    public String getFocusedField() {
        return focusedField;
    }

    public List<String> getFieldsToRetain() {
        return fieldsToRetain;
    }

    public static RemoteIssueSearchForm defaultForm() {
        return new RemoteIssueSearchForm(
                Collections.singletonList(
                        buildRequiredTextField(
                                "key", "sqtm-core.entity.issue.key.label")), // Front-side translation
                "key",
                null);
    }

    public static Field textField(String fieldId, String label) {
        final String textFieldType = InputType.TypeName.TEXT_FIELD.value;
        InputType inputType = new InputType(textFieldType, textFieldType);

        Rendering rendering = new Rendering();
        rendering.setInputType(inputType);

        Field field = new Field();
        field.setId(fieldId);
        field.setLabel(label);
        field.setRendering(rendering);

        return field;
    }

    public static Field buildRequiredTextField(String fieldId, String label) {
        Field requiredTextField = textField(fieldId, label);
        requiredTextField.getRendering().setRequired(true);
        return requiredTextField;
    }
}
