/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import java.util.HashMap;
import java.util.Map;

/**
 * An input type defines basically what widget should be used when rendered in a UI. Squash proposes
 * several native widgets, but is open to extension (ask on the forum on that topic if you're
 * interested). An InputType maps the original name of a widget proposed on the remote bugtracker to
 * the name of a native Squash Widget. It is the role of the bugtracker connector to define such
 * mapping and provide InputType for each fields that needs rendering on the Squash UI.
 *
 * <p>A widget represents a {@link Field} and produces a {@link FieldValue}. The original field name
 * must contain only printable characters, digits, dots, underscore and dash, any non supported
 * character will be replaced by '_'. See the static fields for the list of known fields.
 *
 * <p>special fields :
 *
 * <ul>
 *   <li>If a remote widget cannot be coerced to a Squash widget, it must use {@link
 *       TypeName#UNKNOWN} as name, and specify the original name anyway.
 *   <li>One or several widgets may set the flag {@link #isFieldSchemeSelector()}. If set, when
 *       their value change a new field scheme will be selected using {@link
 *       AdvancedProject#getFieldScheme(String)}, using "&lt;id&gt;>:&lt;scalar&gt;" as argument,
 *       where id is the id of the field and scalar the value of the field value. They also behave
 *       like a normal field.
 * </ul>
 *
 * <p>an InputType also accepts metadata that will be transmitted to the Squash UI, as a map. These
 * metadata should be stuffed in the attribute {@link #configuration}. See {@link
 * CommonConfigurationKey} enum for supported configurations. *
 *
 * @author bsiri
 */
public class InputType {
    public enum TypeName {
        UNKNOWN("unknown"),
        TEXT_FIELD("text_field"),
        TEXT_AREA("text_area"),
        DATE_PICKER("date_picker"),
        DATE_TIME("date_time"),
        TAG_LIST("tag_list"),
        FREE_TAG_LIST("free_tag_list"),
        DROPDOWN_LIST("dropdown_list"),
        CHECKBOX("checkbox"),
        CHECKBOX_LIST("checkbox_list"),
        RADIO_BUTTON("radio_button"),
        FILE_UPLOAD("file_upload"),
        CASCADING_SELECT("cascading_select"),
        MULTI_SELECT("multi_select"),
        COMBO_BOX("combo_box"),
        RICH_TEXT("rich_text"),
        TREE_SELECT("tree_select"),
        INTEGER("integer"),
        DECIMAL("decimal"),
        SELECTOR_FIELD("selector_field"),
        FIELD_GROUP("field_group");

        public final String value;

        TypeName(String value) {
            this.value = value;
        }
    }

    public static final String EXCLUDED_CHARACTERS = "[^\\w-_.0-9]";

    // ***** attributes ******

    private String name = TypeName.UNKNOWN.value;

    private String original = TypeName.UNKNOWN.value;

    private String dataType;

    private boolean fieldSchemeSelector = false;

    private Map<String, String> configuration = new HashMap<>();

    public InputType() {
        super();
    }

    public InputType(String name, String original) {
        super();
        this.name = name;
        this.original = original;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the original name, escaped using {@link #formatName(String)}
     */
    public String getOriginal() {
        return InputType.formatName(original);
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public boolean isFieldSchemeSelector() {
        return fieldSchemeSelector;
    }

    public void setFieldSchemeSelector(boolean fieldSchemeSelector) {
        this.fieldSchemeSelector = fieldSchemeSelector;
    }

    public Map<String, String> getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Map<String, String> conf) {
        this.configuration = conf;
    }

    public void addConfiguration(String key, String value) {
        this.configuration.put(key, value);
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String inputType) {
        this.dataType = inputType;
    }

    /**
     * Escapes illegal characters and make that string comply to the rules specified at the
     * documentation at the class level.
     *
     * @param original string to escape
     * @return the escaped string
     */
    public static String formatName(String original) {
        return original.replaceAll(EXCLUDED_CHARACTERS, "_");
    }
}
