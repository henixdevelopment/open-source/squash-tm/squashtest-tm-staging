/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context;

public class TestCaseInfo {
    public enum Kind {
        STANDARD,
        GHERKIN,
        KEYWORD,
        EXPLORATORY
    }

    private final Long id;
    private final String reference;
    private final String name;
    private final Kind kind;

    public TestCaseInfo(Long id, String reference, String name, Kind kind) {
        this.id = id;
        this.reference = reference;
        this.name = name;
        this.kind = kind;
    }

    /* No-args constructor for serialization only ! */
    public TestCaseInfo() {
        id = null;
        reference = null;
        name = null;
        kind = null;
    }

    public Long getId() {
        return id;
    }

    public String getReference() {
        return reference;
    }

    public String getName() {
        return name;
    }

    public Kind getKind() {
        return kind;
    }
}
