/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context;

import java.util.List;

/**
 * Holds information about a bug tracker binding with a TM project.
 *
 * <p>Its main purpose is to pass data in between layers without dealing with hibernate objects.
 */
public class BugTrackerBindingInfo {
    private final Long projectId;
    private final Long bugTrackerId;
    private final List<String> remoteProjectNames;

    public BugTrackerBindingInfo(Long projectId, Long bugTrackerId, List<String> remoteProjectNames) {
        this.bugTrackerId = bugTrackerId;
        this.projectId = projectId;
        this.remoteProjectNames = remoteProjectNames;
    }

    /* No args constructor for serialization only ! */
    public BugTrackerBindingInfo() {
        projectId = null;
        bugTrackerId = null;
        remoteProjectNames = null;
    }

    public Long getProjectId() {
        return projectId;
    }

    public Long getBugTrackerId() {
        return bugTrackerId;
    }

    public List<String> getRemoteProjectNames() {
        return remoteProjectNames;
    }
}
