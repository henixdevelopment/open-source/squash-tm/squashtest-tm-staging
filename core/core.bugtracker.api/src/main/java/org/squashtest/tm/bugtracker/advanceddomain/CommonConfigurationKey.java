/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

/** Commonly used keys for InputType configuration. */
public enum CommonConfigurationKey {
    /**
     * If set, when the widget on the Squash UI changes its value, it will emit a {@link
     * DelegateCommand} to the bugtracker connector. Not all widgets supports this, as of 3.0.0 only
     * text_field and tags (free_tag_list, tag_list and multi_select) can do so. Native squash widgets
     * will emit a DelegateCommand, using the value you supplied for 'onchange' as command name and
     * its {@link FieldValue#getName()} as argument. This mechanism is used for instance by the
     * text_fields for autocompletion.
     *
     * @since 1.5.1.
     */
    ONCHANGE("onchange"),

    /** Allow to define a confirmation message to show before the value changes get applied. */
    CONFIRM_MESSAGE("confirmMessage"),

    /** Allow to define some text to be displayed as a tooltip. */
    HELP_MESSAGE("help-message"),

    /**
     * Allow defining the field for which the help message should be attached when a field is selected
     * using a selector selector.
     */
    FIELD_LINKED_TO_HELP_MESSAGE("field_linked_to_help_message"),

    /** If present, allow to filter among possible values in dropdown lists. */
    FILTER_POSSIBLE_VALUES("filter-possible-values"),

    /** Boolean to define if the field needs to be return as pure HTML. */
    RENDER_AS_HTML("render-as-html"),

    /** Enable search issue upon clicking an autocomplete option, targeting a specific json key. */
    DIRECT_SEARCH_FIELD_KEY("direct-search-field-key");

    public final String value;

    CommonConfigurationKey(String value) {
        this.value = value;
    }
}
