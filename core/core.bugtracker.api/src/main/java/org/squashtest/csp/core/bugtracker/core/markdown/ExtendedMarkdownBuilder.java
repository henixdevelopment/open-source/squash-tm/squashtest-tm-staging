/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.csp.core.bugtracker.core.markdown;

/**
 * Builder for extended markdown formatted text.
 *
 * <p>This build allows for extended markdown features such as multi-line code and quote blocks with
 * fences.
 */
public class ExtendedMarkdownBuilder extends AbstractMarkdownBuilder<ExtendedMarkdownBuilder> {

    public ExtendedMarkdownBuilder appendQuoteBlock(String content) {
        return appendQuoteBlock(createInnerBuilder().appendPlainText(content));
    }

    public ExtendedMarkdownBuilder appendQuoteBlock(ExtendedMarkdownBuilder innerBuilder) {
        instructions.add(createQuoteBlockInstruction(innerBuilder));
        return getThis();
    }

    public ExtendedMarkdownBuilder appendCodeBlock(String content) {
        return appendCodeBlock(createInnerBuilder().appendPlainText(content));
    }

    public ExtendedMarkdownBuilder appendCodeBlock(ExtendedMarkdownBuilder innerBuilder) {
        instructions.add(createCodeBlockInstruction(innerBuilder));
        return getThis();
    }

    @Override
    protected ExtendedMarkdownBuilder getThis() {
        return this;
    }

    @Override
    protected ExtendedMarkdownBuilder createInnerBuilder() {
        return new ExtendedMarkdownBuilder();
    }

    protected AbstractInstruction<ExtendedMarkdownBuilder> createQuoteBlockInstruction(
            ExtendedMarkdownBuilder innerBuilder) {
        return new QuoteBlockInstruction<>(innerBuilder);
    }

    protected AbstractInstruction<ExtendedMarkdownBuilder> createCodeBlockInstruction(
            ExtendedMarkdownBuilder innerBuilder) {
        return new CodeBlockInstruction<>(innerBuilder);
    }

    private static class QuoteBlockInstruction<T extends AbstractMarkdownBuilder<T>>
            extends AbstractFencedInstruction<T> {
        QuoteBlockInstruction(T innerBuilder) {
            super(">>>", ">>>", innerBuilder);
        }
    }

    private static class CodeBlockInstruction<T extends AbstractMarkdownBuilder<T>>
            extends AbstractFencedInstruction<T> {
        CodeBlockInstruction(T innerBuilder) {
            super("```", "```", innerBuilder);
        }
    }
}
