/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.security.acls;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;

/**
 * Enum of Squash permissions. Should match the one defined as Spring Security base / custom
 * permissions {@link CustomPermission}
 *
 * @author Gregory Fouquet
 */
public enum Permissions {
    READ(BasePermission.READ), // 1
    WRITE(BasePermission.WRITE), // 2
    CREATE(BasePermission.CREATE), // 4
    DELETE(BasePermission.DELETE), // 8
    ADMIN(BasePermission.ADMINISTRATION), // 16
    MANAGE_PROJECT(CustomPermission.MANAGE_PROJECT), // 32
    EXPORT(CustomPermission.EXPORT), // 64
    EXECUTE(CustomPermission.EXECUTE), // 128
    LINK(CustomPermission.LINK), // 256
    IMPORT(CustomPermission.IMPORT), // 512
    ATTACH(CustomPermission.ATTACH), // 1024
    EXTENDED_DELETE(CustomPermission.EXTENDED_DELETE), // 2048
    READ_UNASSIGNED(CustomPermission.READ_UNASSIGNED), // 4096
    WRITE_AS_FUNCTIONAL(CustomPermission.WRITE_AS_FUNCTIONAL), // 8192
    WRITE_AS_AUTOMATION(CustomPermission.WRITE_AS_AUTOMATION), // 16384
    MANAGE_MILESTONE(CustomPermission.MANAGE_MILESTONE), // 32768
    MANAGE_PROJECT_CLEARANCE(CustomPermission.MANAGE_PROJECT_CLEARANCE), // 65536
    DELETE_EXECUTION(CustomPermission.DELETE_EXECUTION); // 131072

    private static final Map<Integer, Permissions> MASK_TO_PERMISSION =
            Arrays.stream(Permissions.values())
                    .collect(Collectors.toMap(Permissions::getMask, Function.identity()));

    private final Permission permissionWithMask;

    Permissions(Permission permissionWithMask) {
        this.permissionWithMask = permissionWithMask;
    }

    /**
     * @return the quality
     */
    public Permission getPermission() {
        return permissionWithMask;
    }

    public int getMask() {
        return permissionWithMask.getMask();
    }

    public static Set<Permissions> findByMasks(List<Integer> masks) {
        return masks.stream()
                .map(MASK_TO_PERMISSION::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toUnmodifiableSet());
    }

    public static Permissions findByMask(int mask) {
        return MASK_TO_PERMISSION.get(mask);
    }
}
