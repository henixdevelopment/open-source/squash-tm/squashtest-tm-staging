/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.plugin;

/**
 * Base interface for SquashTM plugins. It is responsible for plugin identification and validation.
 *
 * @author Gregory Fouquet
 */
public interface Plugin {

    /**
     * @return This plugin persistent, globally unique identifier. Should not return null.
     */
    String getId();

    /**
     * @return the type of the plugin. This is a free string that should help the user to know what
     *     this plugin is about. It would be courteous to make it translated in the locale used in the
     *     current thread.
     */
    String getType();

    /**
     * @return it is an internal data, which serves to identify the type of the plugin and to
     *     differentiate the plugins used.
     */
    PluginType getPluginType();

    /**
     * Asks the plugin to validate its current configuration stored in the database. As the
     * configuration may be context-dependant, as in a per-project configuration for instance, an
     * EntityReference to that context is supplied. The EntityReference can be null, or a project, a
     * node etc. Must either succeed, or throw a {@link PluginValidationException}.
     *
     * @param reference to a given object
     */
    void validate(EntityReference reference) throws PluginValidationException;
}
