/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.security.acls;

import java.io.Serial;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;

/**
 * Any new permission should be added to the corresponding enum of Squash permissions {@link
 * Permissions}
 */
public class CustomPermission extends BasePermission {
    @Serial private static final long serialVersionUID = -4326675754746846198L;

    public static final Permission MANAGE_PROJECT = new CustomPermission(1 << 5, 'M'); // 32
    public static final Permission EXPORT = new CustomPermission(1 << 6, 'X'); // 64
    public static final Permission EXECUTE = new CustomPermission(1 << 7, 'E'); // 128
    public static final Permission LINK = new CustomPermission(1 << 8, 'L'); // 256
    public static final Permission IMPORT = new CustomPermission(1 << 9, 'I'); // 512
    public static final Permission ATTACH = new CustomPermission(1 << 10, 'T'); // 1024
    public static final Permission EXTENDED_DELETE = new CustomPermission(1 << 11, 'S'); // 2048
    public static final Permission READ_UNASSIGNED = new CustomPermission(1 << 12, 'U'); // 4096
    public static final Permission WRITE_AS_FUNCTIONAL = new CustomPermission(1 << 13, 'F'); // 8192
    public static final Permission WRITE_AS_AUTOMATION = new CustomPermission(1 << 14, 'O'); // 16384
    public static final Permission MANAGE_MILESTONE = new CustomPermission(1 << 15, 'J'); // 32768
    public static final Permission MANAGE_PROJECT_CLEARANCE =
            new CustomPermission(1 << 16, 'P'); // 65536
    public static final Permission DELETE_EXECUTION = new CustomPermission(1 << 17, 'N'); // 131072

    public CustomPermission(int mask, char code) {
        super(mask, code);
    }
}
