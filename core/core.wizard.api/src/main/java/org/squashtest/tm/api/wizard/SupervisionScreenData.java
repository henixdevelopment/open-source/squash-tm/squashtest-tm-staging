/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.wizard;

import org.squashtest.tm.api.plugin.UsedInPlugin;

@UsedInPlugin("Xsquash4GitLab, Xsquash4Jira, WorkflowAutomJira")
public class SupervisionScreenData {
    private boolean hasSyncNameColumn;
    private boolean hasPerimeterColumn;
    private boolean hasRemoteSelectTypeColumn;
    private boolean hasSynchronizedRequirementsRatioColumn;
    private boolean hasSynchronizedSprintTicketsRatioColumn;

    public static SupervisionScreenData withNoAdditionalColumn() {
        return new SupervisionScreenData();
    }

    public boolean isHasSyncNameColumn() {
        return hasSyncNameColumn;
    }

    public void setHasSyncNameColumn(boolean hasSyncNameColumn) {
        this.hasSyncNameColumn = hasSyncNameColumn;
    }

    public boolean isHasPerimeterColumn() {
        return hasPerimeterColumn;
    }

    public void setHasPerimeterColumn(boolean hasPerimeterColumn) {
        this.hasPerimeterColumn = hasPerimeterColumn;
    }

    public boolean isHasRemoteSelectTypeColumn() {
        return hasRemoteSelectTypeColumn;
    }

    public void setHasRemoteSelectTypeColumn(boolean hasRemoteSelectTypeColumn) {
        this.hasRemoteSelectTypeColumn = hasRemoteSelectTypeColumn;
    }

    public boolean isHasSynchronizedRequirementsRatioColumn() {
        return hasSynchronizedRequirementsRatioColumn;
    }

    public void setHasSynchronizedRequirementsRatioColumn(
            boolean hasSynchronizedRequirementsRatioColumn) {
        this.hasSynchronizedRequirementsRatioColumn = hasSynchronizedRequirementsRatioColumn;
    }

    public boolean isHasSynchronizedSprintTicketsRatioColumn() {
        return hasSynchronizedSprintTicketsRatioColumn;
    }

    public void setHasSynchronizedSprintTicketsRatioColumn(
            boolean hasSynchronizedSprintTicketsRatioColumn) {
        this.hasSynchronizedSprintTicketsRatioColumn = hasSynchronizedSprintTicketsRatioColumn;
    }
}
