/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.sanitizehtml;

import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.ParseError;
import org.jsoup.parser.ParseErrorList;
import org.jsoup.parser.Parser;
import org.jsoup.safety.Safelist;

public final class HTMLSanitizeUtils {
    private static final String[] allowedTags = {
        "a",
        "b",
        "blockquote",
        "br",
        "caption",
        "center",
        "cite",
        "code",
        "col",
        "colgroup",
        "dd",
        "del",
        "div",
        "dl",
        "dt",
        "em",
        "figure",
        "h1",
        "h2",
        "h3",
        "h4",
        "h5",
        "h6",
        "hr",
        "i",
        "img",
        "ins",
        "li",
        "ol",
        "p",
        "pre",
        "q",
        "s",
        "small",
        "span",
        "strike",
        "strong",
        "sub",
        "sup",
        "table",
        "tbody",
        "td",
        "tfoot",
        "th",
        "thead",
        "tr",
        "u",
        "ul"
    };

    private static final String[] allowedAttributes = {
        "align",
        "aria-hidden",
        "border",
        "cellpadding",
        "cellspacing",
        "class",
        "dir",
        "height",
        "id",
        "lang",
        "rel",
        "role",
        "style",
        "tabindex",
        "title",
        "width"
    };

    private HTMLSanitizeUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Check if the HTML is valid and return a list of errors.
     *
     * @param unsecureHtml the HTML to check
     * @return a list of errors
     */
    public static List<String> checkHtml(String unsecureHtml) {
        if (StringUtils.isBlank(unsecureHtml)) {
            return Collections.emptyList();
        }

        final List<String> parseErrors = findParseErrors(unsecureHtml);

        if (!parseErrors.isEmpty()) {
            return parseErrors;
        }

        if (Jsoup.isValid(unsecureHtml, getSafelist())) {
            return Collections.emptyList();
        } else {
            return Collections.singletonList("HTML contains unsafe elements or attributes.");
        }
    }

    private static List<String> findParseErrors(String unsecureHtml) {
        final Parser parser = Parser.htmlParser().setTrackErrors(5);

        Jsoup.parse(unsecureHtml, "", parser);

        final ParseErrorList parserErrorList = parser.getErrors();

        return parserErrorList.stream().map(ParseError::toString).toList();
    }

    public static String cleanHtml(String unsecureHtml) {
        if (unsecureHtml == null) {
            return null;
        }

        if (StringUtils.isBlank(unsecureHtml)) {
            return StringUtils.EMPTY;
        }

        final Document.OutputSettings outputSettings = new Document.OutputSettings();
        outputSettings.prettyPrint(false);
        outputSettings.escapeMode(Entities.EscapeMode.extended);
        outputSettings.outline(false);

        // String that will be used to substitute unicode escaped characters to prevent JSoup to
        // unescape them
        final String substitute = "à@-";
        String finalHtml = unsecureHtml.replaceAll("&([^;]+?);", substitute + "$1;");

        finalHtml = Jsoup.clean(finalHtml, "", getSafelist(), outputSettings);

        // Substitute back characters that had been changed before Jsoup.clean()
        return finalHtml.replaceAll(substitute + "([^;]+?);", "&$1;");
    }

    private static Safelist getSafelist() {
        return new Safelist()
                .addTags(allowedTags)
                .addAttributes(":all", allowedAttributes)
                .addAttributes("a", "accesskey", "charset", "name", "target", "type", "href")
                .addAttributes("blockquote", "cite")
                .addAttributes("col", "span")
                .addAttributes("colgroup", "span")
                .addAttributes("del", "cite")
                .addAttributes("figure", "longdesc")
                .addAttributes("font", "color", "face", "size")
                .addAttributes("img", "longdesc", "alt", "src")
                .addAttributes("ins", "cite")
                .addAttributes("ol", "start", "type")
                .addAttributes("q", "cite")
                .addAttributes("table", "border", "cellpadding", "cellspacing", "summary")
                .addAttributes("td", "abbr", "axis", "colspan", "rowspan")
                .addAttributes("th", "abbr", "axis", "colspan", "rowspan", "scope")
                .addAttributes("ul", "type")
                .addProtocols("a", "href", "ftp", "http", "https", "mailto")
                .addProtocols("blockquote", "cite", "http", "https")
                .addProtocols("del", "cite", "http", "https")
                .addProtocols("img", "src", "cid", "data", "http", "https")
                .addProtocols("ins", "cite", "http", "https")
                .addProtocols("q", "cite", "http", "https");
    }
}
