/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.lang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

/**
 * Used internally mostly for operations on paths. Much like an URL instance can check if the
 * protocol, host, path etc are corrects.
 *
 * @author bsiri
 */
// TODO : why this class in a package .lang ? (so is UrlUtils etc)
// TODO : refactor with
// https://gitlab.com/henixdevelopment/squash/squash-tm/core/squashtest-tm-staging/-/issues/3372
public final class PathUtils {

    private static final String SPLIT = "(?<!\\\\)/";

    private static final String CORRECT_MULTIPLE_SLASHES = "(?<!\\\\)\\/+";

    /**
     * A Slash with its surrounding trailing spaces. Note: If the slash is preceded by a backslash,
     * then it is part of the name and has not to be cleaned.
     */
    private static final String REGEX_SLASH_WITH_TRAILING_SPACES = " *(?<!\\\\)/ *";

    private PathUtils() {
        super();
    }

    /**
     * Checks if a path is well-formed. A well-formed path:
     * <li>Starts with a slash
     * <li>Doesn't end with a slash
     * <li>Contains at least two elements (project name and element name)
     * <li>Has no empty segments between slashes
     * <li>Properly handles escaped slashes and special characters
     *
     * @param path The path to check
     * @return true if the path is well-formed
     */
    public static boolean isPathWellFormed(String path) {
        if (path == null || path.isEmpty()) {
            return false;
        }

        // Must start with a slash
        if (!path.startsWith("/")) {
            return false;
        }

        boolean escaped = false;
        boolean hasContent = false; // Track if current segment has content
        int segmentCount = 0; // Count path segments (should be >= 2)

        // Skip the first character since we already checked it's a slash
        for (int i = 1; i < path.length(); i++) {
            char c = path.charAt(i);

            if (c == '\\') {
                escaped = !escaped;
                hasContent = true;
            } else if (c == '/' && !escaped) {
                if (!hasContent) {
                    return false; // Empty segment between slashes
                }
                segmentCount++;
                hasContent = false;
            } else {
                hasContent = true;
                escaped = false;
            }
        }

        // Must not end with a slash and must have content in the last segment
        if (!hasContent) {
            return false;
        }

        // Count the final segment if it has content
        segmentCount++;

        // Must have at least 2 segments (project name + element name)
        return segmentCount >= 2;
    }

    public static String cleanMultipleSlashes(String path) {
        return path.replaceAll(CORRECT_MULTIPLE_SLASHES, "/");
    }

    /**
     * Removes spaces between words if there are more than one, and at the beginning or end of the
     * String. Also removes spaces close to '/'.
     *
     * @param path The String representing the path to clean
     * @return A clean path with no unnecessary space.
     */
    public static String removeAllUnnecessarySpaces(String path) {
        String cleanPath = removeTrailingSpacesSurroundingSlashes(path);
        return StringUtils.normalizeSpace(cleanPath);
    }

    /**
     * Removes trailing spaces around the slashes in the given String and returns the result String.
     *
     * @param path The String representing the path to clean.
     * @return The given path whose trailing spaces surrounding slashes were removed.
     */
    public static String removeTrailingSpacesSurroundingSlashes(String path) {
        return path.replaceAll(REGEX_SLASH_WITH_TRAILING_SPACES, "/");
    }

    /**
     * Extracts the <strong>escaped</strong> projet name from a path. Note (GRF) : a path such as
     * "/token" will return a null project name. Not too sure if that's what we want but it seem to
     * work this way
     *
     * @see #extractUnescapedProjectName(String)
     * @param path the non null path to extract the project name from.
     * @return the <strong>escaped</strong> project name <strong>which might be null</strong>
     */
    public static String extractProjectName(String path) {
        if (path == null) {
            return null;
        }

        // Find the second forward slash that isn't escaped
        int start = path.startsWith("/") ? 1 : 0;
        int end = -1;
        boolean escaped = false;

        for (int i = start; i < path.length(); i++) {
            char c = path.charAt(i);
            if (c == '\\') {
                escaped = !escaped;
            } else if (c == '/' && !escaped) {
                end = i;
                break;
            } else {
                escaped = false;
            }
        }

        if (end == -1 || start >= end) {
            return null;
        }

        return path.substring(start, end);
    }

    /**
     * Same as {@link #extractProjectName(String)} but unescapes the result. Project-less paths
     * ("/foo") return null project name.
     *
     * @param path the non null path to extract the project name from.
     * @return the project name <strong>which might be null</strong>
     */
    public static String extractUnescapedProjectName(String path) {
        String esc = extractProjectName(path);
        return esc == null ? null : unescapePathPartSlashes(esc);
    }

    public static Set<String> extractProjectNames(Collection<String> pathes) {
        Set<String> res = new HashSet<>();
        for (String path : pathes) {
            res.add(extractProjectName(path));
        }

        return res;
    }

    /**
     * Will build a valid path from splits. Throw {@link IllegalArgumentException} if names
     * concatenation lead to an ill formed path
     *
     * @param names names
     * @return String
     */
    public static String buildPathFromParts(String[] names) {
        StringBuilder builder = new StringBuilder();
        for (String name : names) {
            builder.append("/");
            builder.append(name);
        }
        String path = builder.toString();
        if (!isPathWellFormed(path)) {
            throw new IllegalArgumentException();
        }
        return path;
    }

    /**
     * Extracts the entity name from a path. The entity name is the last part of the path.
     *
     * @param path path
     * @return String
     */
    public static String extractEntityName(String path) {
        if (path == null) {
            throw new IllegalArgumentException("Path cannot be null");
        }

        // Find the last unescaped slash
        int lastSlashIndex = -1;
        boolean escaped = false;

        for (int i = 0; i < path.length(); i++) {
            char c = path.charAt(i);
            if (c == '\\') {
                escaped = !escaped;
            } else if (c == '/' && !escaped) {
                lastSlashIndex = i;
            } else {
                escaped = false;
            }
        }

        if (lastSlashIndex == -1) {
            throw new IllegalArgumentException("No slash found in path '" + path + "'");
        }

        String name = path.substring(lastSlashIndex + 1);

        if (name.isEmpty()) {
            throw new IllegalArgumentException("Slash at the end of path '" + path + "'");
        }

        return name;
    }

    public static boolean arePathsAndNameConsistents(String path, String name) {
        try {
            String pathName = extractEntityName(path);
            return pathName.equals(name);

        } catch (IllegalArgumentException ex) {
            return false;
        }
    }

    /**
     * Returns the path with a different test case name. You can't change directory that way (using
     * "..")
     *
     * @param path path
     * @param name name
     * @return String
     */
    public static String rename(String path, String name) {
        String oldname = extractEntityName(path);
        String oldpatt = "\\Q" + oldname + "\\E$";
        return path.replaceAll(oldpatt, name);
    }

    /**
     * Transforms a split path into a single String representing a tree path.
     *
     * <p>Example:
     *
     * <ul>
     *   <li>effectiveSplits = <code>["project", "rootNode", "rootNodeChild", "rootNodeChildChild"]
     *       </code>
     *   <li>result = <code>"rootNode/rootNodeChild/rootNodeChildChild"</code>
     * </ul>
     */
    public static String getPathWithoutProject(List<String> effectiveSplits) {
        StringBuilder result = new StringBuilder();

        for (int i = 1; i < effectiveSplits.size(); i++) {
            if (i == 1) {
                result.append(effectiveSplits.get(i));
            } else {
                result.append("/").append(effectiveSplits.get(i));
            }
        }

        return result.toString();
    }

    public static String appendPathToProjectName(String projectName, String path) {
        if (Objects.isNull(projectName) || projectName.isEmpty()) {
            throw new IllegalArgumentException("Project name can't be null or empty");
        }

        if (Objects.isNull(path) || path.isEmpty()) {
            throw new IllegalArgumentException("Path can't be null or empty");
        }

        if (projectName.contains("/")) {
            projectName = projectName.replace("/", "\\/");
        }

        return removeAllUnnecessarySpaces("/" + projectName + StringUtils.prependIfMissing(path, "/"));
    }

    public static List<String> unescapeSlashes(List<String> paths) {
        List<String> unescaped = new ArrayList<>(paths.size());
        for (String orig : paths) {
            unescaped.add(orig.replace("\\/", "/"));
        }
        return unescaped;
    }

    /**
     * Transforms a path into a list representing the path without project name.
     *
     * <p>Example:
     *
     * <ul>
     *   <li>path = <code>["/project/rootNode/rootNodeChild"]</code>
     *   <li>result = <code>["rootNode", "rootNodeChild"]</code>
     * </ul>
     */
    public static List<String> getSplitPathWithoutProjectNameFromAPath(String path) {
        List<String> splits = Arrays.asList(PathUtils.splitPath(path));
        List<String> effectiveSplits = PathUtils.unescapeSlashes(splits);

        if (effectiveSplits.size() < 2) {
            throw new IllegalArgumentException(
                    "Folder path must contain at least a valid /projectName/node.");
        }

        return effectiveSplits.subList(1, effectiveSplits.size());
    }

    /**
     * a well formed path starts with a '/' and we remove it right away before splitting (or else a
     * false positive empty string would appear before it)
     *
     * @param path path
     * @return String[]
     */
    public static String[] splitPath(String path) {
        return path.replaceFirst("^/+", "").split(SPLIT);
    }

    /**
     * Accepts a well formed path and returns the list of paths of all of its ancestors, and then
     * itself.
     *
     * <p>example : scanPath("/project/folder1/folder2/element") => ["/project", "/project/folder1/",
     * "/project/folder1/folder2" ,"/project/folder1/folder2/element"]
     *
     * <p>Some Scala guy would think of it as path.split("/").scanLeft("")(_ + "/" + _)
     */
    public static List<String> scanPath(String path) {

        String[] split = splitPath(path);
        List<String> paths = new ArrayList<>(split.length);
        StringBuilder buffer = new StringBuilder();

        // build all the paths on the way.
        for (String aSplit : split) {
            buffer.append("/");
            buffer.append(aSplit);
            paths.add(buffer.toString());
        }

        return paths;
    }

    public static boolean isPathSyntaxValid(String path) {
        if (path.isEmpty()) {
            return true;
        }

        char last = path.charAt(path.length() - 1);

        if (last == '/') {
            return false;
        }

        boolean hasBlankPart = Arrays.stream(path.split("/")).anyMatch(String::isBlank);
        return !hasBlankPart;
    }

    /**
     * Unescape a path. Beware that it will change the path structure by adding "/" so it should be
     * use only with parts...
     *
     * @param pathPart pathPart
     * @return String
     */
    public static String unescapePathPartSlashes(String pathPart) {
        return pathPart.replace("\\/", "/");
    }

    /**
     * Unescape a path. Beware that it will change the path structure by adding "/" so it should be
     * use only with parts...
     *
     * @param pathParts pathParts
     * @return List<String>
     */
    public static List<String> unescapePathPartSlashes(Collection<String> pathParts) {
        List<String> unescapedParts = new ArrayList<>();
        for (String part : pathParts) {
            unescapedParts.add(PathUtils.unescapePathPartSlashes(part));
        }
        return unescapedParts;
    }

    public static String asSquashPath(String path) {
        return PathUtils.unescapePathPartSlashes(
                path.replaceFirst("^/", "").replaceFirst("(?<!\\\\)/$", ""));
    }
}
