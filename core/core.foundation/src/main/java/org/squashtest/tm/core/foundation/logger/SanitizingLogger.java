/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.logger;

/** A wrapper around slf4j logger that sanitizes the messages before logging them. */
public class SanitizingLogger implements Logger {
    private final org.slf4j.Logger logger;

    public SanitizingLogger(org.slf4j.Logger logger) {
        this.logger = logger;
    }

    @Override
    public boolean isTraceEnabled() {
        return logger.isTraceEnabled();
    }

    @Override
    public void trace(String message, Object... args) {
        logger.trace(sanitize(message), getSanitizedArgs(args));
    }

    @Override
    public void trace(String msg, Throwable t) {
        logger.trace(msg, t);
    }

    @Override
    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    @Override
    public void debug(String message, Object... args) {
        logger.debug(sanitize(message), getSanitizedArgs(args));
    }

    @Override
    public void debug(String msg, Throwable t) {
        logger.debug(msg, t);
    }

    @Override
    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    @Override
    public void info(String message, Object... args) {
        logger.info(sanitize(message), getSanitizedArgs(args));
    }

    @Override
    public void info(String msg, Throwable t) {
        logger.info(msg, t);
    }

    @Override
    public boolean isWarnEnabled() {
        return logger.isWarnEnabled();
    }

    @Override
    public void warn(String message, Object... args) {
        logger.warn(sanitize(message), getSanitizedArgs(args));
    }

    @Override
    public void warn(String msg, Throwable t) {
        logger.warn(msg, t);
    }

    @Override
    public boolean isErrorEnabled() {
        return logger.isErrorEnabled();
    }

    @Override
    public void error(String message, Object... args) {
        logger.error(sanitize(message), getSanitizedArgs(args));
    }

    @Override
    public void error(String msg, Throwable t) {
        logger.error(msg, t);
    }

    @Override
    public String getName() {
        return logger.getName();
    }

    private Object[] getSanitizedArgs(Object[] args) {
        Object[] sanitizedArgs = new Object[args.length];

        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof String) {
                sanitizedArgs[i] = sanitize((String) args[i]);
            } else {
                sanitizedArgs[i] = args[i];
            }
        }

        return sanitizedArgs;
    }

    public static String sanitize(String input) {
        if (input == null) {
            return null;
        }

        return input.replaceAll("\\r?+\\n+", " ").trim();
    }
}
