/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.logger;

public interface Logger {

    boolean isTraceEnabled();

    void trace(String message, Object... args);

    void trace(String msg, Throwable t);

    boolean isDebugEnabled();

    void debug(String message, Object... args);

    void debug(String msg, Throwable t);

    boolean isInfoEnabled();

    void info(String message, Object... args);

    void info(String msg, Throwable t);

    boolean isWarnEnabled();

    void warn(String message, Object... args);

    void warn(String msg, Throwable t);

    boolean isErrorEnabled();

    void error(String message, Object... args);

    void error(String msg, Throwable t);

    String getName();
}
