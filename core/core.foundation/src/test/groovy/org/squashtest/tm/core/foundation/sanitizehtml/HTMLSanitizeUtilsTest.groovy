/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.sanitizehtml

import spock.lang.Specification;

class HTMLSanitizeUtilsTest extends Specification {

    def "should return the same string as input"() {
        when:
        def result = HTMLSanitizeUtils.cleanHtml(unsecureHtml)

        then:
        result == unsecureHtml

        where:
        unsecureHtml << [
            "<p>A simple sentence with basic characters and numbers 1234567890.</p>",
            "<p>Phrase réaliste avec accents non echappés, à bientôt !</p>",
            "<p>Phrase r&eacute;aliste avec accents, &agrave; bient&ocirc;t !</p>",
            "<p>V&eacute;rifier que 5 &gt; 1.</p>",
            "<p>Many encoded characters: &amp;&quot;&#39;&micro;&uml;&pound;&curren;&sect;&deg;</p>",
            "<p>Many characters with accents: &ccedil;u&eacute;&egrave;&ecirc;&acirc;&ocirc;&ucirc;&icirc;&iuml;&euml;&uuml;&ouml;&auml;&ntilde;&ograve;&igrave;</p>",
            "<p>Many non encoded characters: #{[(^`%*^\$!:/@;.,=_-~)]}</p>"
        ]
    }

    def "should handle invalid HTML"() {
        when:
        def result = HTMLSanitizeUtils.cleanHtml(unsecureHtml)

        then:
        result == expected

        where:
        unsecureHtml | expected
        "<p>Unbalanced tag" | "<p>Unbalanced tag</p>"
        "<p>Phrase r&eacute;aliste avec accents, &agrave; bient&ocirc;t !" | "<p>Phrase r&eacute;aliste avec accents, &agrave; bient&ocirc;t !</p>"
        "</br>" | "<br>"
        "<br></br><br />" | "<br><br><br>"
        "❤🦄" | "❤🦄"
        "l'élève est créé et le message suivant s'affiche : [L'élève <nom de l'élève> a bien été créé.]" | "l'élève est créé et le message suivant s'affiche : [L'élève  a bien été créé.]"

    }
}
