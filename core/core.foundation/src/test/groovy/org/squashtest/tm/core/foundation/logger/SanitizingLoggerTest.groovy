/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.logger

import spock.lang.Specification

class SanitizingLoggerTest extends Specification {

    def "should sanitize string argument"() {
        when:
        def result = SanitizingLogger.sanitize(input)

        then:
        result == expected

        where:
        input          | expected
        "foo"          | "foo"
        "foo\nbar"     | "foo bar"
        "foo\n\n\nbar" | "foo bar"
    }

    def "should call logger with sanitized string"() {
        given:
        def logger = Mock(org.slf4j.Logger)
        def sut = new SanitizingLogger(logger)

        when:
        sut.info("Planet Earth is {}", [input].toArray())

        then:
        1 * logger.info("Planet Earth is {}", [expected].toArray())

        where:
        input                                            | expected
        "blue"                                           | "blue"
        "blue\nand there's nothing I can do"             | "blue and there's nothing I can do"
        "flat\n\n\n\n\n\n\n\n\n\n...when drawn on a map" | "flat ...when drawn on a map"
    }

    def "should call logger with 2 sanitized strings"() {
        given:
        def logger = Mock(org.slf4j.Logger)
        def sut = new SanitizingLogger(logger)

        when:
        sut.debug("Don't you wonder sometimes\nAbout {} and {}?", "sound\n", "\nvision")

        then:
        1 * logger.debug("Don't you wonder sometimes About {} and {}?", ["sound", "vision"].toArray())
    }

    def "should call logger with sanitized string and untouched number"() {
        given:
        def logger = Mock(org.slf4j.Logger)
        def sut = new SanitizingLogger(logger)

        when:
        sut.warn("Check ignition and may God's love be with you ({}, {})", 1L, "lift\noff")

        then:
        1 * logger.warn("Check ignition and may God's love be with you ({}, {})", [1L, "lift off"].toArray())
    }

    def "should call with exception"() {
        given:
        def logger = Mock(org.slf4j.Logger)
        def sut = new SanitizingLogger(logger)
        def exception = new Exception("there's something wrong")

        when:
        sut.error("Your circuit's dead", exception)

        then:
        1 * logger.error("Your circuit's dead", exception)
    }
}
