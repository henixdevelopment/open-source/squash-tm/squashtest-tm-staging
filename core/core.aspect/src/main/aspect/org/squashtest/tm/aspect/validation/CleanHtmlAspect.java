/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.aspect.validation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.core.foundation.sanitizehtml.HTMLSanitizeUtils;

@Aspect
@Component
public class CleanHtmlAspect {
    @Around("@annotation(CleanHtml) || execution(* *(.., @CleanHtml (*), ..))")
    public Object aroundCleanHtmlMethodExecution(ProceedingJoinPoint joinPoint) throws Throwable {
        final Object[] args = joinPoint.getArgs();
        final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        final Method method = signature.getMethod();
        final boolean isMethodAnnotated = method.isAnnotationPresent(CleanHtml.class);

        // Sanitize method arguments
        final Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            for (Annotation annotation : parameterAnnotations[i]) {
                if (annotation instanceof CleanHtml && args[i] instanceof String) {
                    args[i] = HTMLSanitizeUtils.cleanHtml((String) args[i]);
                }
            }
        }

        // Call the method
        Object returnValue = joinPoint.proceed(args);

        // Sanitize the return value if the method is annotated
        if (isMethodAnnotated && returnValue instanceof String) {
            returnValue = HTMLSanitizeUtils.cleanHtml((String) returnValue);
        }

        return returnValue;
    }
}
