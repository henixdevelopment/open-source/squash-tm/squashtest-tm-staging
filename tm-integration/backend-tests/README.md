SQTM Backend Tests
======================

This project is dedicated to automate backend testing of SquashTM (Version 2.0+). The goal is to test the full featured Squash TM backend server with all layers started.
We test the input/output of the whole server.

JSON in/JSON out

## RestAssured

The requests are made with the RestAssured Library

## Database

### Initial Setup

The initial database setup is done with liquibase with liquibase-maven-plugin

### Test Setup

The setup of each test is done with dbunit.
