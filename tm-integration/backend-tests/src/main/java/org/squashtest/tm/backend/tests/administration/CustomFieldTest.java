/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CustomFieldTest extends DatabaseTest {

	@Test
	public void shouldShowAllCustomFields() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/custom-fields")
			.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 2 custom fields
		assertEquals(2, dataRows.size());

		assertEquals("CustomField1", jsonPath.getString("dataRows[0].data.name"));
		assertEquals("CUF1", jsonPath.getString("dataRows[0].data.label"));
		assertEquals("1234", jsonPath.getString("dataRows[0].data.code"));
		assertEquals("PLAIN_TEXT", jsonPath.getString("dataRows[0].data.inputType"));
		assertTrue(jsonPath.getBoolean("dataRows[0].data.optional"));
	}

	@Test
	public void shouldRemoveCustomFields() {
		Cookie session = login(Users.ADMIN);

		given()
			.cookie(session)
			.contentType("application/json")
		.when()
			.delete("backend/custom-fields/-1,-2")
		.then()
			.statusCode(200);

		assertCustomFieldCount(session, 0);
	}

	@Test
	public void shouldAddCustomFields() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> cf1 = new HashMap<>();
		cf1.put("name", "cf1");
		cf1.put("label", "cf1");
		cf1.put("code", "cf1");
		cf1.put("inputType", "PLAIN_TEXT");
		cf1.put("optional", false);
		cf1.put("defaultValue", "blabla");
		addCustomField(session, cf1);

		Map<String, Object> cf2 = new HashMap<>();
		cf2.put("name", "cf2");
		cf2.put("label", "cf2");
		cf2.put("code", "cf2");
		cf2.put("inputType", "RICH_TEXT");
		cf2.put("optional", false);
		cf2.put("defaultValue", "<p>Hey</p>");
		addCustomField(session, cf2);

		Map<String, Object> cf3 = new HashMap<>();
		cf3.put("name", "cf3");
		cf3.put("label", "cf3");
		cf3.put("code", "cf3");
		cf3.put("inputType", "NUMERIC");
		cf3.put("optional", false);
		cf3.put("defaultValue", 123);
		addCustomField(session, cf3);

		Map<String, Object> cf4 = new HashMap<>();
		cf4.put("name", "cf4");
		cf4.put("label", "cf4");
		cf4.put("code", "cf4");
		cf4.put("inputType", "CHECKBOX");
		cf4.put("optional", true);
		cf4.put("defaultValue", false);
		addCustomField(session, cf4);

		Map<String, Object> cf5 = new HashMap<>();
		cf5.put("name", "cf5");
		cf5.put("label", "cf5");
		cf5.put("code", "cf5");
		cf5.put("inputType", "TAG");
		cf5.put("optional", false);
		cf5.put("defaultValue", "hello|goodbye");
		addCustomField(session, cf5);

		Map<String, Object> cf6 = new HashMap<>();
		cf6.put("name", "cf6");
		cf6.put("label", "cf6");
		cf6.put("code", "cf6");
		cf6.put("inputType", "DATE_PICKER");
		cf6.put("optional", false);
		cf6.put("defaultValue", "2020-05-19T09:49:47.882Z");
		addCustomField(session, cf6);

		// TODO: dropdown lists

		assertCustomFieldCount(session, 2 + 6);
	}

	@Test
	public void shouldPreventAddingCustomFieldsWithBlankFields() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> customField = new HashMap<>();
		customField.put("name", "");
		customField.put("label", "cf");
		customField.put("code", "cf");
		customField.put("inputType", "PLAIN_TEXT");
		customField.put("optional", false);
		customField.put("defaultValue", "blabla");

		String response1 = addCustomField(session, customField, 412);
		JsonPath jsonPath = from(response1);
		assertEquals("name", jsonPath.getString("fieldValidationErrors[0].fieldName"));

		customField.put("name", "cf");
		customField.put("label", "");

		String response2 = addCustomField(session, customField, 412);
		JsonPath jsonPath2 = from(response2);
		assertEquals("label", jsonPath2.getString("fieldValidationErrors[0].fieldName"));

		customField.put("label", "cf");
		customField.put("code", "");

		String response3 = addCustomField(session, customField, 412);
		JsonPath jsonPath3 = from(response3);
		assertEquals("code", jsonPath3.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldPreventAddingCustomFieldsWithIllegalCodeCharacters() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> customField = new HashMap<>();
		customField.put("name", "custom field");
		customField.put("label", "cf");
		customField.put("code", "é_à");
		customField.put("inputType", "PLAIN_TEXT");
		customField.put("optional", false);
		customField.put("defaultValue", "blabla");
		String response = addCustomField(session, customField, 412);

		JsonPath jsonPath = from(response);
		assertEquals("code", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldPreventAddingCustomFieldsWithDuplicateCode() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> customField = new HashMap<>();
		customField.put("name", "custom field");
		customField.put("label", "cf");
		customField.put("code", "1234");
		customField.put("inputType", "PLAIN_TEXT");
		customField.put("optional", false);
		customField.put("defaultValue", "blabla");
		String response = addCustomField(session, customField, 412);

		JsonPath jsonPath = from(response);
		assertEquals("code", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
	}

	private String addCustomField(Cookie session, Map<String, Object> formModel) {
		return addCustomField(session, formModel, 201);
	}

	private String addCustomField(Cookie session, Map<String, Object> formModel, int expectedStatusCode) {
		String response = given()
			.cookie(session)
			.contentType("application/json")
			.body(formModel)
		.when()
			.post("backend/custom-fields/new")
		.then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();

		if (expectedStatusCode == 201) {
			JsonPath jsonPath = from(response);
			assertNotNull(jsonPath.getJsonObject("id"));
		}

		return response;
	}

	private void assertCustomFieldCount(Cookie session, int expectedCount) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(expectedCount + 5);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
		.when()
			.post("backend/custom-fields")
		.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");
		assertEquals(expectedCount, dataRows.size());
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"administration/custom-field-data-test.xml");
	}
}
