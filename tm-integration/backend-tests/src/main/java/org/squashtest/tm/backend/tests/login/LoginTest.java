/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.login;

import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class LoginTest extends DatabaseTest {

	@Test
	public void shouldLogin() {
		Map<String, String> credentials = new HashMap<>();
		credentials.put("username", "admin");
		credentials.put("password", "admin");
		// @formatter:off
		given().
			formParams(credentials).
		when().
			post("backend/login").
		then().
			statusCode(200).
			body("authenticated", is(true));
		// @formatter:on
	}

	@Test
	public void shouldRejectUnknownUser() {
		Map<String, String> credentials = new HashMap<>();
		credentials.put("username", "unknown");
		credentials.put("password", "nope");

		// @formatter:off
		given().
			formParams(credentials).
		when().
			post("backend/login").
		then().
			statusCode(200).
			body("authenticated", is(false));
		// @formatter:on
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"referential/referential-data-test.xml");
	}
}
