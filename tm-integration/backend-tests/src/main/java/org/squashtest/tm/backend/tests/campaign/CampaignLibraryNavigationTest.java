/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.campaign;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;

public class CampaignLibraryNavigationTest extends DatabaseTest {

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER", "TEST_EDITOR"})
	public void addCampaignToLibrary(Users user) {
		Cookie cookie = login(user);

		Map<String, Object> formModel = createCampaignFormModel("CampaignLibrary--1", "Campaign 1", "Ref-001", "");

		String response = given()
			.cookie(cookie)
			.contentType(ContentType.JSON)
			.body(formModel).
		when()
			.post("backend/campaign-tree/new-campaign").
		then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/campaign/create-response-schema.json"))
			.extract().body().asString();

		JsonPath jsonPath = JsonPath.from(response);
		assertEquals("leaf", jsonPath.getString("state"));
		assertEquals(-1, jsonPath.getInt("projectId"));
		assertEquals("Ref-001 - Campaign 1", jsonPath.getString("data.NAME"));
		assertEquals("Ref-001", jsonPath.getString("data.REFERENCE"));
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER", "TEST_EDITOR"})
	public void addCampaignToFolder(Users user) {
		Cookie cookie = login(user);

		Map<String, Object> formModel = createCampaignFormModel("CampaignFolder--301", "Campaign 1", "Ref-001", "");

		String response = given()
			.cookie(cookie)
			.contentType(ContentType.JSON)
			.body(formModel).
		when()
			.post("backend/campaign-tree/new-campaign").
		then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/campaign/create-response-schema.json"))
			.extract().body().asString();

		JsonPath jsonPath = JsonPath.from(response);
		assertEquals("leaf", jsonPath.getString("state"));
		assertEquals(-1, jsonPath.getInt("projectId"));
		assertEquals("Ref-001 - Campaign 1", jsonPath.getString("data.NAME"));
		assertEquals("Ref-001", jsonPath.getString("data.REFERENCE"));
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"VIEWER", "VALIDATOR", "TEST_RUNNER", "TEST_DESIGNER", "AUTOMATED_TEST_WRITER", "ADVANCE_TESTER"})
	public void cannotAddCampaignToLibrary(Users user) {
		Cookie cookie = login(user);

		Map<String, Object> formModel = createCampaignFormModel("CampaignLibrary--1", "Campaign 1", "Ref-001", "");

		given()
			.cookie(cookie)
			.contentType(ContentType.JSON)
			.body(formModel).
		when()
			.post("backend/campaign-tree/new-campaign").
		then()
			.statusCode(403);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"VIEWER", "VALIDATOR", "TEST_RUNNER", "TEST_DESIGNER", "AUTOMATED_TEST_WRITER", "ADVANCE_TESTER"})
	public void cannotAddCampaignToFolder(Users user) {
		Cookie cookie = login(user);

		Map<String, Object> formModel = createCampaignFormModel("CampaignFolder--301", "Campaign 1", "Ref-001", "");

		given()
			.cookie(cookie)
			.contentType(ContentType.JSON)
			.body(formModel).
		when()
			.post("backend/campaign-tree/new-campaign").
		then()
			.statusCode(403);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER", "TEST_EDITOR"})
	public void addIterationToCampaign(Users user) {
		Cookie cookie = login(user);

		Map<String, Object> formModel = createIterationFormModel("Campaign--302", "Iteration 1", "1", "");

		String response = given()
			.cookie(cookie)
			.contentType(ContentType.JSON)
			.body(formModel).
		when()
			.post("backend/campaign-tree/campaign/-302/new-iteration").
		then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/iteration/create-response-schema.json"))
			.extract().body().asString();

		JsonPath jsonPath = JsonPath.from(response);
		assertEquals("leaf", jsonPath.getString("state"));
		assertEquals(-1, jsonPath.getInt("projectId"));
		assertEquals("1 - Iteration 1", jsonPath.getString("data.NAME"));
		assertEquals("1", jsonPath.getString("data.REFERENCE"));
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"VIEWER", "VALIDATOR", "TEST_RUNNER", "TEST_DESIGNER", "AUTOMATED_TEST_WRITER", "ADVANCE_TESTER"})
	public void cannotAddIterationToCampaign(Users user) {
		Cookie cookie = login(user);

		Map<String, Object> formModel = createIterationFormModel("Campaign--302", "Iteration 1", "1", "");

		given()
			.cookie(cookie)
			.contentType(ContentType.JSON)
			.body(formModel).
		when()
			.post("backend/campaign-tree/campaign/-302/new-iteration").
		then()
			.statusCode(403);
	}

	@Test
	public void shouldForbidCampaignCreationWithDuplicateName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createCampaignFormModel("CampaignLibrary--1", "Campaign", "", "");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/campaign-tree/new-campaign").
				then()
			.statusCode(412)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/errors/field-validation-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("sqtm-core.error.generic.duplicate-name", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidCampaignCreationWithEmptyName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createCampaignFormModel("CampaignLibrary--1", "", "", "");

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
			when()
			.post("backend/campaign-tree/new-campaign").
			then()
			.statusCode(412)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/errors/empty-field-validation-schema.json"));
	}

	@Test
	public void shouldForbidIterationCreationWithDuplicateName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createIterationFormModel("Campaign-302", "Iteration", "", "");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/campaign-tree/campaign/-302/new-iteration").
				then()
			.statusCode(412)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/errors/field-validation-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("sqtm-core.error.generic.duplicate-name", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidIterationCreationWithEmptyName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createIterationFormModel("Campaign-302", "", "", "");

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
			when()
			.post("backend/campaign-tree/campaign/-302/new-iteration").
			then()
			.statusCode(412)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/errors/empty-field-validation-schema.json"));
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"referential/referential-data-test.xml",
			"campaign/campaign-library-navigation-data-test.xml"
		);
	}

	private Map<String, Object> createCampaignFormModel(String parentRef, String name, String reference, String description) {
		return createBaseFormModel(parentRef, name, reference, description);
	}

	private Map<String, Object> createIterationFormModel(String parentRef, String name, String reference, String description) {
		return createBaseFormModel(parentRef, name, reference, description);
	}

	private Map<String, Object> createBaseFormModel(String parentRef, String name, String reference, String description) {
		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", name);
		formModel.put("reference", reference);
		formModel.put("description", description);
		formModel.put("parentEntityReference", parentRef);
		formModel.put("customFields", new HashMap<Long, Object>());
		return formModel;
	}
}
