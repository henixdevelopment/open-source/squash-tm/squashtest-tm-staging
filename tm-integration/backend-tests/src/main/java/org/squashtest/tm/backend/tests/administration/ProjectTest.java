/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ProjectTest extends DatabaseTest {

	@Test
	public void shouldShowAllProjects() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/generic-projects").
				then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/grid/grid-response-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(3, 				dataRows.size());
		assertEquals("Project-101", 	jsonPath.getString("dataRows[0].data.name"));
		assertEquals("Label-101", 		jsonPath.getString("dataRows[0].data.label"));
		assertEquals("2020-04-08T22:00:00.000+0000", 	jsonPath.getString("dataRows[0].data.createdOn"));
		assertEquals("admin", 			jsonPath.getString("dataRows[0].data.createdBy"));
		assertNull(jsonPath.getString("dataRows[0].data.lastModifiedOn"));
		assertNull(jsonPath.getString("dataRows[0].data.lastModifiedBy"));
		assertTrue(jsonPath.getJsonObject("dataRows[0].data.hasPermissions"));
		assertEquals("Server-1", 		jsonPath.getString("dataRows[0].data.bugtrackerName"));
		assertEquals("TA01", 			jsonPath.getString("dataRows[0].data.executionServer"));

		assertEquals("Project-102", 	jsonPath.getString("dataRows[1].data.name"));
		assertNull(jsonPath.getString("dataRows[1].data.bugtrackerName"));
		assertNull(jsonPath.getString("dataRows[1].data.executionServer"));

		assertEquals("Project-103", 	jsonPath.getString("dataRows[2].data.name"));
	}

	@Test
	public void shouldOnlyShowManagedProjects() {
		Cookie session = login(Users.PROJECT_MANAGER);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/generic-projects").
				then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/grid/grid-response-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(1, 				dataRows.size());
		assertEquals("Project-101", 	jsonPath.getString("dataRows[0].data.name"));
	}

	@Test
	public void shouldGetTemplatesList() {
		Cookie session = login(Users.ADMIN);

		String response = given()
			.cookie(session)
			.contentType("application/json").
				when()
			.get("backend/generic-projects/templates").
				then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<NamedReference> templates = jsonPath.getList("templates");
		assertEquals(1, 				templates.size());
		assertEquals("-101", 			jsonPath.getString("templates[0].id"));
		assertEquals("Project-101", 	jsonPath.getString("templates[0].name"));
	}

	@Test
	public void shouldAddProject() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> project = new HashMap<>();

		project.put("name", "project");
		project.put("label", "P1");
		project.put("description", "description");

		String response = addWithStatus(session, project, "backend/projects/new", 201);

		JsonPath json = new JsonPath(response);
		assertNotNull(json.getJsonObject("id"));
		assertProjectCount(4);
	}

	@Test
	public void shouldForbidAddProjectWhenNameAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> project = new HashMap<>();

		project.put("name", "Project-101");
		project.put("label", "P1");
		project.put("description", "description");

		addWithStatus(session, project, "backend/projects/new", 412);
	}

	@Test
	public void shouldAddTemplate() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> template = new HashMap<>();

		template.put("name", "template");
		template.put("label", "T01");
		template.put("description", "description");

		String response = addWithStatus(session, template, "backend/projects/new-template", 201);

		JsonPath json = new JsonPath(response);
		assertNotNull(json.getJsonObject("id"));
		assertProjectCount(4);
	}

	@Test
	public void shouldForbidAddTemplateWhenNameAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> project = new HashMap<>();

		project.put("name", "Project-101");
		project.put("label", "P1");
		project.put("description", "description");

		addWithStatus(session, project, "backend/projects/new-template", 412);
	}

	@Test
	public void shouldAddProjectFromTemplate() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> template = new HashMap<>();

		template.put("name", "project from template");
		template.put("label", "T01");
		template.put("description", "description");
		template.put("fromTemplate", true);
		template.put("templateId", -101);
		template.put("keepTemplateBinding", true);
		template.put("copyCUF", true);
		template.put("copyPermissions", true);
		template.put("copyAllowTcModifFromExec", true);
		template.put("copyInfolists", true);
		template.put("copyBugtrackerBinding", true);
		template.put("copyOptionalExecStatuses", true);
		template.put("copyMilestone", true);
		template.put("copyPlugins", true);
		template.put("copyAutomatedProjects", true);

		String response = addWithStatus(session, template, "backend/projects/new", 201);

		JsonPath json = new JsonPath(response);
		assertNotNull(json.getJsonObject("id"));
		assertProjectCount(4);
	}

	@Test
	public void shouldForbidAddProjectFromTemplateWhenNameAlreadyExist() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> template = new HashMap<>();

		template.put("name", "Project-101");
		template.put("fromTemplate", true);
		template.put("templateId", -101);

		addWithStatus(session, template, "backend/projects/new", 412);
	}

	@Test
	public void shouldAddTemplateFromProject() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> template = new HashMap<>();

		template.put("name", "template");
		template.put("label", "T01");
		template.put("description", "description");
		template.put("templateId", -101);
		template.put("copyCUF", true);
		template.put("copyPermissions", true);
		template.put("copyAllowTcModifFromExec", true);
		template.put("copyInfolists", true);
		template.put("copyBugtrackerBinding", true);
		template.put("copyOptionalExecStatuses", true);
		template.put("copyMilestone", true);
		template.put("copyPlugins", true);
		template.put("copyAutomatedProjects", true);

		String response = addWithStatus(session, template, "backend/project-templates/new", 201);

		JsonPath json = new JsonPath(response);
		assertNotNull(json.getJsonObject("id"));
		assertProjectCount(4);
	}

	@Test
	public void shouldForbidAddTemplateFromProjectWhenNameAlreadyExist() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> template = new HashMap<>();

		template.put("name", "Project-101");
		addWithStatus(session, template, "backend/project-templates/new", 412);
	}

	@Test
	public void shouldDeleteProject() {
		Cookie session = login(Users.ADMIN);

		given()
			.cookie(session).
				when()
			.delete("backend/projects/-103").
				then()
			.statusCode(200);
	}

	@Test
	public void shouldForbidDeleteProjectWhenProjectHasData() {
		Cookie session = login(Users.ADMIN);

		given()
			.cookie(session).
			when()
			.delete("backend/projects/-101").
			then()
			.statusCode(412);
	}

	private void assertProjectCount(int expectedCount) {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(50);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/generic-projects").
				then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");
		assertEquals(expectedCount, dataRows.size());
	}

	private String addWithStatus(Cookie session, Map<String, Object> request, String url, int statusCode) {
		return given()
			.cookie(session)
			.contentType("application/json")
			.body(request).
				when()
			.post(url).
				then()
			.statusCode(statusCode)
			.extract().response().asString();
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"administration/project-data-test.xml");
	}

	@Override
	protected List<String> getPreTearDownUpdateDatasetPaths() {
		// On MariaDB, we need to clean the self-referencing FKs (project.template_id)
		return Collections.singletonList("administration/project-clean.xml");
	}
}
