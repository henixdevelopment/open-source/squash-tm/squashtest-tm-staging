/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;

public class ConnectionLogTest extends DatabaseTest {

	@Test
	public void shouldShowAllConnectionLogs() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/users/connection-logs")
			.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 6 connection logs (1 connexion log for "admin" user logging in at test initialization)
		assertEquals(3, dataRows.size());

		//Login tests
		assertEquals("ONE", jsonPath.getString("dataRows[2].data.login"));
		assertEquals("TWO", jsonPath.getString("dataRows[1].data.login"));


		//Connection dates tests
		assertEquals("2020-04-10T22:00:00.000+0000", jsonPath.getString("dataRows[2].data.connectionDate"));
		assertEquals("2020-04-12T10:39:39.000+0000", jsonPath.getString("dataRows[1].data.connectionDate"));

		//Connexion successful
		assertEquals(true, jsonPath.getBoolean("dataRows[2].data.success"));

		//Connexion failed
		assertEquals(false, jsonPath.getBoolean("dataRows[1].data.success"));
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"referential/referential-data-test.xml",
			"administration/connection-log-data-test.xml"
		);
	}
}
