/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestAutomationServerTest extends DatabaseTest {

	@Test
	public void shouldShowAllTestAutomationServers() {
		Cookie session = login(Users.ADMIN);

		String response = getTestAutomationServerGrid(session, 10, 0);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 2 automation servers
		assertEquals(2, dataRows.size());

		assertEquals("testServer1", jsonPath.getString("dataRows[0].data.name"));
		assertEquals("http:192.168.0.03:9090/testServer1", jsonPath.getString("dataRows[0].data.baseUrl"));
	}

	@Test
	public void shouldAddTestAutomationServer() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("testServer15", "http://localhost:8080", "login",
			"password", true, "description");

		String response = addTestAutomationServer(session, formModel, 201);

		JsonPath jsonPath = from(response);
		assertNotNull(jsonPath.getJsonObject("id"));
	}

	@Test
	public void shouldForbidServerCreationIfNameAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("testServer1", "http://localhost:8080", "login",
			"password", true, "description");

		String response = addTestAutomationServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("sqtm-core.error.generic.name-already-in-use", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidServerCreationWithEmptyName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("", "http://localhost:8080", "login",
			"password", true, "description");

		String response = addTestAutomationServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("name", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidServerCreationWithEmptyUrl() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("server name", "", "login",
			"password", true, "description");

		String response = addTestAutomationServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("baseUrl", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidServerCreationWithEmptyLogin() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("server name", "http://localhost:8080", "",
			"password", true, "description");

		String response = addTestAutomationServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("login", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidServerCreationWithEmptyPassword() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("server name", "http://localhost:8080", "login",
			"", true, "description");

		String response = addTestAutomationServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("password", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidServerCreationWithMalformedUrl() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("server name", "localhost", "login",
			"password", true, "description");

		String response = addTestAutomationServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		assertEquals("baseUrl", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
		assertEquals("sqtm-core.exception.wrong-url", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidServerCreationIfUrlAndLoginAreAlreadyRegistered() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("server name", "http:192.168.0.03:9090/testServer1",
			"server1", "", true, "description");

		String response = addTestAutomationServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("login", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
		assertEquals("sqtm-core.exception.test-automation-server.server-and-user-already-registered",
			jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldDeleteTestAutomationServer() {
		Cookie session = login(Users.ADMIN);

		// Do delete
		given()
			.cookie(session)
		.when()
			.delete("backend/test-automation-servers/-2,-3,-1") // Add non existing ID in purpose
		.then()
			.statusCode(200)
			.extract().response().asString();

		// check that new count is 0
		String fetchResponse = getTestAutomationServerGrid(session, 10, 0);
		JsonPath jsonPath = from(fetchResponse);
		List<Object> dataRows = jsonPath.getList("dataRows");
		assertEquals(0, dataRows.size());
	}

	private Map<String, Object> createFormModel(String name, String url, String login, String password,
										boolean manualSlaveSelection, String description) {
		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", name);
		formModel.put("baseUrl", url);
		formModel.put("login", login);
		formModel.put("password", password);
		formModel.put("manualSlaveSelection", manualSlaveSelection);
		formModel.put("description", description);
		return formModel;
	}

	private String getTestAutomationServerGrid(Cookie session, int gridSize, int gridPage) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(gridSize);
		gridRequest.setPage(gridPage);

		return given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/test-automation-servers")
			.then()
			.statusCode(200)
			.extract().response().asString();
	}

	private String addTestAutomationServer(Cookie session, Map<String, Object> serverToAdd, int expectedStatusCode) {
		return given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(serverToAdd).
				when()
			.post("backend/test-automation-servers/new").
				then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"administration/test-automation-server-data-test.xml");
	}
}
