/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MilestoneTest extends DatabaseTest {
	@Test
	public void shouldShowAllMilestones() {
		Cookie session = login(Users.ADMIN);
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 4 milestones :
		// - 1 GLOBAL
		// - 2 owned by project_manager
		// - 1 owned by test_designer
		assertEquals(4, dataRows.size());

		assertEquals(-2L, 							 	jsonPath.getLong("dataRows[0].data.milestoneId"));
		assertEquals("Milestone 2", 				 	jsonPath.getString("dataRows[0].data.label"));
		assertEquals("PLANNED", 					 	jsonPath.getString("dataRows[0].data.status"));
		assertEquals("2011-04-16T22:00:00.000+0000", 	jsonPath.getString("dataRows[0].data.endDate"));
		assertEquals(0, 							 	jsonPath.getInt("dataRows[0].data.projectCount"));
		assertEquals("GLOBAL", 					 	jsonPath.getString("dataRows[0].data.range"));
		assertEquals("admin", 						 	jsonPath.getString("dataRows[0].data.ownerLogin"));
		assertEquals("2020-04-16T22:00:00.000+0000", 	jsonPath.getString("dataRows[0].data.createdOn"));
		assertEquals("admin", 							jsonPath.getString("dataRows[0].data.createdBy"));
		assertNull(jsonPath.getString("dataRows[0].data.description"));

		assertEquals(-1L, 								jsonPath.getLong("dataRows[1].data.milestoneId"));
		assertEquals("Milestone 1", 					jsonPath.getString("dataRows[1].data.label"));
		assertEquals("FINISHED", 						jsonPath.getString("dataRows[1].data.status"));
		assertEquals("2010-04-16T22:00:00.000+0000", 	jsonPath.getString("dataRows[1].data.endDate"));
		assertEquals(1, 								jsonPath.getInt("dataRows[1].data.projectCount"));
		assertEquals("RESTRICTED", 					jsonPath.getString("dataRows[1].data.range"));
		assertEquals("project_manager", 				jsonPath.getString("dataRows[1].data.ownerLogin"));
		assertEquals("description", 					jsonPath.getString("dataRows[1].data.description"));
		assertEquals("2020-04-16T22:00:00.000+0000", 	jsonPath.getString("dataRows[1].data.createdOn"));
		assertEquals("project_manager", 				jsonPath.getString("dataRows[1].data.createdBy"));

		assertEquals(-3L, 								jsonPath.getLong("dataRows[2].data.milestoneId"));
		assertEquals("Milestone 3", 					jsonPath.getString("dataRows[2].data.label"));
		assertEquals("PLANNED", 						jsonPath.getString("dataRows[2].data.status"));
		assertEquals("2002-04-16T22:00:00.000+0000", 	jsonPath.getString("dataRows[2].data.endDate"));
		assertEquals(0, 								jsonPath.getInt("dataRows[2].data.projectCount"));
		assertEquals("RESTRICTED", 					jsonPath.getString("dataRows[2].data.range"));
		assertEquals("test_designer", 					jsonPath.getString("dataRows[2].data.ownerLogin"));
		assertEquals("2020-04-16T22:00:00.000+0000", 	jsonPath.getString("dataRows[2].data.createdOn"));
		assertEquals("test_designer", 					jsonPath.getString("dataRows[2].data.createdBy"));
		assertNull(jsonPath.getString("dataRows[2].data.description"));

		assertEquals(-4L, 								jsonPath.getLong("dataRows[3].data.milestoneId"));
		assertEquals("Milestone 4", 					jsonPath.getString("dataRows[3].data.label"));
		assertEquals("PLANNED", 						jsonPath.getString("dataRows[3].data.status"));
		assertEquals("2000-04-16T22:00:00.000+0000", 	jsonPath.getString("dataRows[3].data.endDate"));
		assertEquals(1, 								jsonPath.getInt("dataRows[3].data.projectCount"));
		assertEquals("RESTRICTED", 					jsonPath.getString("dataRows[3].data.range"));
		assertEquals("project_manager", 				jsonPath.getString("dataRows[3].data.ownerLogin"));
		assertEquals("project_manager", 				jsonPath.getString("dataRows[3].data.createdBy"));
		assertNull(jsonPath.getString("dataRows[2].data.description"));
	}

	@Test
	public void shouldRestrictShownMilestonesForNonAdminUser() {
		Cookie session = login(Users.PROJECT_MANAGER);
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 3 milestones visible for this user (GLOBAL + 2 owned)
		assertEquals(3, dataRows.size());

		assertEquals(-2L, 					jsonPath.getLong("dataRows[0].data.milestoneId"));
		assertEquals("GLOBAL", 			jsonPath.getString("dataRows[0].data.range"));
		assertEquals("admin", 				jsonPath.getString("dataRows[0].data.ownerLogin"));

		assertEquals(-1L, 					jsonPath.getLong("dataRows[1].data.milestoneId"));
		assertEquals("RESTRICTED", 		jsonPath.getString("dataRows[1].data.range"));
		assertEquals("project_manager", 	jsonPath.getString("dataRows[1].data.ownerLogin"));

		assertEquals(-4L, 					jsonPath.getLong("dataRows[2].data.milestoneId"));
		assertEquals("RESTRICTED", 		jsonPath.getString("dataRows[2].data.range"));
		assertEquals("project_manager", 	jsonPath.getString("dataRows[2].data.ownerLogin"));
	}

	@Test
	public void shouldShowMilestonesLinkedToManagedProjects() {
		Cookie session = login(Users.TEST_DESIGNER);
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 3 milestones visible for this user :
		// - 1 GLOBAL
		// - 1 owned
		// - 1 associated with Project 2 (for which this user is project manager)
		assertEquals(3, dataRows.size());

		assertEquals(-2L, 					jsonPath.getLong("dataRows[0].data.milestoneId"));
		assertEquals("GLOBAL", 			jsonPath.getString("dataRows[0].data.range"));
		assertEquals("admin", 				jsonPath.getString("dataRows[0].data.ownerLogin"));

		assertEquals(-3L, 					jsonPath.getLong("dataRows[1].data.milestoneId"));
		assertEquals("RESTRICTED", 		jsonPath.getString("dataRows[1].data.range"));
		assertEquals("test_designer", 		jsonPath.getString("dataRows[1].data.ownerLogin"));

		assertEquals(-4L, 					jsonPath.getLong("dataRows[2].data.milestoneId"));
		assertEquals("RESTRICTED", 		jsonPath.getString("dataRows[2].data.range"));
		assertEquals("project_manager", 	jsonPath.getString("dataRows[2].data.ownerLogin"));
	}

	@Test
	public void shouldAddMilestoneAndGiveItGlobalRange() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> milestoneToAdd = new HashMap<>();

		milestoneToAdd.put("label", "my new milestone");
		milestoneToAdd.put("status", "FINISHED");
		milestoneToAdd.put("endDate", new Date());
		milestoneToAdd.put("description", "description");

		// Add milestones
		addMilestone(session, milestoneToAdd, 200);

		// TODO: here we could just fetch one milestone once the appropriate methods are added
		// Re-fetch grid to check its size and content
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(5, dataRows.size());

		// We know the first row is the newly created milestone because they are sorted in descending end date order
		// and all previously added milestones (in milestone-data-test.xml) have past end dates.
		assertEquals("my new milestone", 				jsonPath.getString("dataRows[0].data.label"));
		assertEquals("FINISHED", 						jsonPath.getString("dataRows[0].data.status"));
		assertEquals(0, 								jsonPath.getInt("dataRows[0].data.projectCount"));
		assertEquals("GLOBAL", 							jsonPath.getString("dataRows[0].data.range"));
		assertEquals(-1L,	 							jsonPath.getLong("dataRows[0].data.ownerId"));
		assertEquals("admin", 							jsonPath.getString("dataRows[0].data.ownerLogin"));
		assertEquals("admin", 							jsonPath.getString("dataRows[0].data.createdBy"));
	}

	@Test
	public void shouldAddMilestoneAndGiveItRestrictedRange() {
		Cookie session = login(Users.PROJECT_MANAGER);

		Map<String, Object> milestoneToAdd = new HashMap<>();

		milestoneToAdd.put("label", "my new milestone");
		milestoneToAdd.put("status", "FINISHED");
		milestoneToAdd.put("endDate", new Date());
		milestoneToAdd.put("description", "description");

		// Add milestones
		addMilestone(session, milestoneToAdd, 200);

		// TODO: here we could just fetch one milestone once the appropriate methods are added
		// Re-fetch grid to check its size and content
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// This user can see the global milestone, the 2 owned previously added and the newly created
		assertEquals(4, dataRows.size());

		// We know the first row is the newly created milestone because they are sorted in descending end date order
		// and all previously added milestones (in milestone-data-test.xml) have past end dates.
		assertEquals("my new milestone", 		jsonPath.getString("dataRows[0].data.label"));
		assertEquals("FINISHED", 				jsonPath.getString("dataRows[0].data.status"));
		assertEquals(0, 						jsonPath.getInt("dataRows[0].data.projectCount"));
		assertEquals("RESTRICTED", 			jsonPath.getString("dataRows[0].data.range"));
		assertEquals(-5L,	 					jsonPath.getLong("dataRows[0].data.ownerId"));
		assertEquals("project_manager", 		jsonPath.getString("dataRows[0].data.ownerLogin"));
		assertEquals("project_manager", 		jsonPath.getString("dataRows[0].data.createdBy"));
	}

	@Test
	public void shouldForbidCreationWithDuplicateName() {
		Cookie session = login(Users.PROJECT_MANAGER);

		Map<String, Object> milestoneToAdd = new HashMap<>();

		milestoneToAdd.put("label", "Milestone 3");
		milestoneToAdd.put("status", "FINISHED");
		milestoneToAdd.put("endDate", new Date());
		milestoneToAdd.put("description", "description");

		// Add milestones
		addMilestone(session, milestoneToAdd, 412);	// expect PRECONDITION_FAILED
	}

	@Test
	public void shouldForbidCreationWithEmptyName() {
		Cookie session = login(Users.PROJECT_MANAGER);

		Map<String, Object> milestoneToAdd = new HashMap<>();

		milestoneToAdd.put("label", "");
		milestoneToAdd.put("status", "FINISHED");
		milestoneToAdd.put("endDate", new Date());
		milestoneToAdd.put("description", "description");

		// Add milestones
		addMilestone(session, milestoneToAdd, 412);	// expect PRECONDITION_FAILED
	}

	@Test
	public void shouldDeleteMilestones() {
		Cookie session = login(Users.ADMIN);

		// Delete milestones
		deleteAllMilestones(session, 200);

		// Check how many are left
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(0, dataRows.size());
	}

	// TODO : this test won't work right now as NO CHECKS ON PERMISSIONS ARE MADE WHEN DELETING MILESTONES
	public void shouldDeleteOnlyOwnedMilestones() {
		Cookie session = login(Users.PROJECT_MANAGER);

		// Delete milestones
		deleteAllMilestones(session, 200);

		// Check how many are left
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// The project manager should only see the global milestone which can't be deleted
		assertEquals(1, dataRows.size());
		assertEquals(-1L, dataRows.size());
	}

	private void deleteAllMilestones(Cookie session, int expectedStatusCode) {
		given()
			.cookie(session)
			.contentType("application/json").
		when()
			.delete("backend/milestones/-1,-2,-3,-4").
		then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	private void addMilestone(Cookie session, Map<String, Object> milestone, int expectedStatusCode) {
		given()
			.cookie(session)
			.contentType("application/json")
			.body(milestone).
		when()
			.post("backend/milestones/new").
		then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	private String fetchGrid(Cookie session) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		return given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
		when()
			.post("backend/milestones").
		then()
			.statusCode(200)
			.extract().response().asString();
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"referential/info-list-system.xml",
			"administration/milestone-data-test.xml");
	}
}
