INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES (-159753);

INSERT INTO TEST_CASE_LIBRARY_NODE (TCLN_ID, NAME, CREATED_BY, CREATED_ON, PROJECT_ID, ATTACHMENT_LIST_ID)
VALUES (-1592, 'tc1', 'admin', '2023-09-21', 2, -159753),
       (-1593, 'tc2', 'admin', '2023-09-21', 2, -159753),
       (-1594, 'tc3', 'admin', '2023-09-21', 2, -159753),
       (-1595, 'tc4', 'admin', '2023-09-21', 2, -159753);

INSERT INTO TEST_CASE (TCLN_ID, TC_NATURE, TC_TYPE, PREREQUISITE, UUID)
VALUES (-1592, 12, 20, '', 'acb3f620-7540-4db1-892d-8b774db23ab5'),
       (-1593, 12, 20, '', 'acb3f620-7540-4db1-892d-8b774db23ab6'),
       (-1594, 12, 20, '', 'acb3f620-7540-4db1-892d-8b774db23ab7'),
       (-1595, 12, 20, '', 'acb3f620-7540-4db1-892d-8b774db23ab8');

INSERT INTO PARAMETER(PARAM_ID, NAME, TEST_CASE_ID, DESCRIPTION)
VALUES (-100, 'Chien', -1592, ''),
       (-108, 'Chat', -1592, ''),
       (-159, 'Loup', -1592, ''),

       (-160, 'First', -1593, ''),
       (-168, 'Second', -1593, ''),
       (-169, 'Third', -1593, ''),
       (-162, 'Fourth', -1593, ''),

       (-171, 'Alpha', -1595, ''),
       (-172, 'Beta', -1595, ''),
       (-173, 'Gamma', -1595, ''),
       (-174, 'Delta', -1595, ''),
       (-175, 'Epsilon', -1595, '');
