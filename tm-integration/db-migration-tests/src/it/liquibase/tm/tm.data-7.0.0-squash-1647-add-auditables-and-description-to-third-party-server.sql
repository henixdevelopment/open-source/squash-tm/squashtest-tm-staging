INSERT INTO THIRD_PARTY_SERVER (SERVER_ID, NAME, URL)
VALUES (-1, 'automation server 1', 'toto'),
       (-3, 'automation server 3', 'toto'),
       (-5, 'automation server 4', 'toto'),
       (-8, 'automation server 5', 'toto'),
       (-11, 'automation server 6', 'toto'),
       (-12, 'not automation 1', 'toto'),
       (-22, 'not automation 2', 'toto'),
       (-1987, 'automation server 7', 'toto');

INSERT INTO TEST_AUTOMATION_SERVER (SERVER_ID, CREATED_BY, CREATED_ON, DESCRIPTION, LAST_MODIFIED_BY, LAST_MODIFIED_ON)
VALUES (-3, 'Byron Kelleher', '2023-02-24 14:18:53','', 'Antoine Dupont','2023-02-24 14:22:53'),
       (-8, 'Garen', '1987-03-27 01:01:01','It is a terrible matchup', 'Darius','2023-02-24 14:18:53'),
       (-11, 'Thierry Dusautoir', '2010-02-07 22:45:53','Grand chelem incroyable!', 'Julien Bonnaire','2010-03-20 23:00:53');

INSERT INTO TEST_AUTOMATION_SERVER (SERVER_ID, CREATED_BY, CREATED_ON, DESCRIPTION)
VALUES (-1, 'admin', '2023-02-24 14:18:53',''),
       (-5, 'admin', '2021-01-21 14:18:53','Pas forcément mieux avant'),
       (-1987, 'admin', '2023-02-24 14:18:53','');
