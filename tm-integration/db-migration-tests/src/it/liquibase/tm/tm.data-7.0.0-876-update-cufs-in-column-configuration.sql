INSERT INTO CORE_PARTY (PARTY_ID)
VALUES (-1),
       (-2);

INSERT INTO CORE_USER(PARTY_ID, LOGIN, FIRST_NAME, LAST_NAME, ACTIVE, CREATED_BY, CREATED_ON, CAN_DELETE_FROM_FRONT)
VALUES (-1, 'user1', 'titi', 'titi', true, 'admin', '2023-02-24 14:18:53', true),
       (-2, 'user2', 'toto', 'toto', true, 'admin', '2023-02-24 14:18:53', true);

INSERT INTO CUSTOM_FIELD (CF_ID, FIELD_TYPE, NAME, LABEL, INPUT_TYPE, CODE)
VALUES (-1, 'CF', 'cuf1', 'cuf1', 'PLAIN_TEXT', 'cuf1'),
       (-2, 'CF', 'cuf2', 'cuf2', 'CHECKBOX', '-2'),
       (-3, 'CF', 'cuf3', 'cuf3', 'DATE_PICKER', 'cuf|3'),
       (-4, 'NUM', 'cuf4', 'cuf4', 'NUMERIC', 'toto'),
       (-5, 'MSF', 'cuf5', 'cuf5', 'TAG', '|test'),
       (-6, 'SSF', 'cuf6', 'cuf6', 'DROPDOWN_LIST', '_newtest123'),
       (-7, 'RTF', 'cuf7', 'cuf7', 'RICH_TEXT', 'cuf7'),
       (-8, 'SSF', 'cuf8', 'cuf8', 'DROPDOWN_LIST', 'TATA_TITI_10'),
       (-9, 'CF', 'cuf9', 'cuf9', 'PLAIN_TEXT', 'cuf9'),
       (-10, 'CF', 'cuf10', 'cuf10', 'PLAIN_TEXT', 'titi'),
       (-11, 'CF', 'cuf11', 'cuf11', 'PLAIN_TEXT', 'cuf111'),
       (-12, 'CF', 'cuf12', 'cuf12', 'PLAIN_TEXT', 'cuf112');

INSERT INTO GRID_COLUMN_DISPLAY_REFERENCE (GCDR_ID, PARTY_ID, GRID_ID)
VALUES (-1, -1, 'requirement-search'),
       (-2, -1, 'test-case-search'),
       (-3, -2, 'requirement-search'),
       (-4, -2, 'test-case-search');

INSERT INTO GRID_COLUMN_DISPLAY_CONFIGURATION (GCDC_ID, GCDR_ID, ACTIVE_COLUMN_ID)
VALUES (-1, -1, 'cuf|cuf1'),
       (-2, -1, 'cuf|-2'),
       (-3, -1, 'cuf|cuf|3'),
       (-4, -1, 'name'),
       (-5, -1, 'projectName'),
       (-6, -1, 'requirementId'),
       (-7, -2, 'cuf|toto'),
       (-8, -2, 'cuf||test'),
       (-9, -2, 'cuf|_newtest123'),
       (-10, -2, 'projectName'),
       (-11, -2, 'reference'),
       (-12, -2, 'status'),
       (-13, -3, 'cuf|cuf7'),
       (-14, -3, 'cuf|TATA_TITI_10'),
       (-15, -3, 'cuf|cuf9'),
       (-16, -3, 'criticality'),
       (-17, -3, 'category'),
       (-18, -3, 'description'),
       (-19, -4, 'cuf|titi'),
       (-20, -4, 'cuf|cuf11'),
       (-21, -4, 'cuf|cuf12'),
       (-22, -4, 'automatable'),
       (-23, -4, 'lastModifiedBy'),
       (-24, -4, 'kind');
