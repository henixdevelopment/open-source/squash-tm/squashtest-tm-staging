INSERT INTO THIRD_PARTY_SERVER (SERVER_ID, NAME, URL, AUTH_POLICY, AUTH_PROTOCOL)
VALUES (-75, 'server1', 'http://jenkins', 'USER', 'BASIC_AUTH'),
       (-77, 'server2', 'http://orchestrator', 'APP_LEVEL', 'TOKEN_AUTH'),
       (-78, 'server3', 'http://orchestrator2', 'APP_LEVEL', 'TOKEN_AUTH'),
       (-79, 'server4', 'http://orchestrator3', 'APP_LEVEL', 'TOKEN_AUTH');

INSERT INTO TEST_AUTOMATION_SERVER (SERVER_ID, KIND, CREATED_ON, MANUAL_SLAVE_SELECTION)
VALUES (-75, 'jenkins', '2019-05-16', false),
       (-77, 'squashAutom', '2019-05-16', false),
       (-78, 'squashAutom', '2019-05-16', false),
       (-79, 'squashAutom', '2019-05-16', false);
