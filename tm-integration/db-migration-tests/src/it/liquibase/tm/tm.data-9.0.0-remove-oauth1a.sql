INSERT INTO THIRD_PARTY_SERVER (SERVER_ID, NAME, URL, AUTH_PROTOCOL, CREATED_BY, CREATED_ON)
VALUES (-10, 'First oauth1a server', 'http://fake-oauth1a.server.org', 'OAUTH_1A', 'admin', '2024-10-18 00:00:00'),
       (-20, 'Second oauth1a server', 'http://fake-oauth1a.server.org', 'OAUTH_1A', 'admin', '2024-10-18 00:00:00');

INSERT INTO STORED_CREDENTIALS (CREDENTIAL_ID, AUTHENTICATED_SERVER, CONTENT_TYPE)
VALUES (-1, -10, 'CONF'),
       (-2, -10, 'CRED'),
       (-3, -20, 'CONF');
