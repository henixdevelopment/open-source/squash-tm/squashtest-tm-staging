INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES (-9000);
INSERT INTO EXECUTION (EXECUTION_ID, ATTACHMENT_LIST_ID, PREREQUISITE, CREATED_BY, CREATED_ON, NAME)
VALUES (-90, -9000, '</br><span class=''step-keyword step-keyword-given''>Soit </span>une machine à café.</br>',
        'dbunit', '2024-04-17', 'Execution-1');

INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES (-9001);
INSERT INTO EXECUTION_STEP (EXECUTION_STEP_ID, ATTACHMENT_LIST_ID, ACTION, EXPECTED_RESULT, CREATED_BY, CREATED_ON)
VALUES (-99, -9001,
        '<span class=''scenario-keyword''>Plan du Scénario </span>Vérifier la livraison des produits.</br><span class=''step-keyword step-keyword-given''>Etant donné que </span>la machine est en marche.</br><span class=''step-keyword step-keyword-given''>Et que </span>mon solde est au moins de 0.40.</br></br><span class=''step-keyword step-keyword-when''>Quand </span>je sélectionne le Expresso.</br></br><span class=''step-keyword step-keyword-then''>Alors </span>la machine me sert un Expresso et mon compte est débité de 0.40.</br>',
        '', 'dbunit', '2024-04-17');
INSERT INTO EXECUTION_EXECUTION_STEPS (EXECUTION_ID, EXECUTION_STEP_ID, EXECUTION_STEP_ORDER) VALUES (-90, -99, 0);

INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES (-9002);
INSERT INTO EXECUTION_STEP (EXECUTION_STEP_ID, ATTACHMENT_LIST_ID, ACTION, EXPECTED_RESULT, CREATED_BY, CREATED_ON)
VALUES (-100, -9002,
        '</br>',
        '', 'dbunit', '2024-04-17');
INSERT INTO EXECUTION_EXECUTION_STEPS (EXECUTION_ID, EXECUTION_STEP_ID, EXECUTION_STEP_ORDER) VALUES (-90, -100, 1);
