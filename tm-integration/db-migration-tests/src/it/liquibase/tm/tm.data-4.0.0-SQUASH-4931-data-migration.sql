INSERT INTO ACTION_WORD (ACTION_WORD_ID, LAST_IMPLEMENTATION_TECHNOLOGY, PROJECT_ID, TOKEN, CREATED_BY, CREATED_ON)
	VALUES (1, 'CUCUMBER', 2, 'T-bonjour', 'liquibase',  '2022-04-08'),
	       (2, 'CUCUMBER', 2, 'T-hello', 'liquibase', '2022-04-08'),
	       (3, 'ROBOT', 2, 'T-ola', 'liquibase', '2022-04-08');
