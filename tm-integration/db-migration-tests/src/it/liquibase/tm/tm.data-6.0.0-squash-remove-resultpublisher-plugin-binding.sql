INSERT INTO ATTACHMENT_LIST(ATTACHMENT_LIST_ID) VALUES
    (1),
    (2),
    (3),
    (4),
    (5),
    (6),
    (7),
    (8),
    (9),
    (10),
    (11),
    (12);

INSERT INTO CAMPAIGN_LIBRARY(CL_ID, ATTACHMENT_LIST_ID) VALUES
    (1, 1),
    (2, 2),
    (3, 3);

INSERT INTO TEST_CASE_LIBRARY(TCL_ID, ATTACHMENT_LIST_ID) VALUES
    (1, 4),
    (2, 5),
    (3, 6);

INSERT INTO REQUIREMENT_LIBRARY(RL_ID, ATTACHMENT_LIST_ID) VALUES
    (1, 7),
    (2, 8),
    (3, 9);

INSERT INTO PROJECT(PROJECT_ID, NAME, DESCRIPTION, LABEL, ACTIVE, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, CL_ID, TCL_ID, RL_ID, ATTACHMENT_LIST_ID) VALUES
    (14, 'Test Project-1', '<p>TEST</p>', 'Lorem ipsum dolor sit amet', true, 'admin', '2011-09-30 10:24:47.0', null, null, 1, 1, 1, 10);
INSERT INTO PROJECT(PROJECT_ID, NAME, DESCRIPTION, LABEL, ACTIVE, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, CL_ID, TCL_ID, RL_ID, ATTACHMENT_LIST_ID) VALUES
    (15, 'Test Project-2', '<p>TEST</p>', 'Lorem ipsum dolor sit amet', true, 'admin', '2011-09-30 10:24:47.0', null, null, 2, 2, 2, 11);
INSERT INTO PROJECT(PROJECT_ID, NAME, DESCRIPTION, LABEL, ACTIVE, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, CL_ID, TCL_ID, RL_ID, ATTACHMENT_LIST_ID) VALUES
    (16, 'Test Project-3', '<p>TEST</p>', 'Lorem ipsum dolor sit amet', true, 'admin', '2011-09-30 10:24:47.0', null, null, 3, 3, 3, 12);

INSERT INTO LIBRARY_PLUGIN_BINDING(PLUGIN_BINDING_ID, LIBRARY_ID, LIBRARY_TYPE, PLUGIN_ID, ACTIVE) VALUES
   (1, 1, 'C', 'squash.tm.plugin.resultpublisher', true),
   (2, 2, 'C', 'squash.tm.plugin.resultpublisher', true),
   (3, 3, 'C', 'squash.tm.plugin.resultpublisher', true);

INSERT INTO LIBRARY_PLUGIN_BINDING_PROPERTY(PLUGIN_BINDING_ID, PLUGIN_BINDING_KEY, PLUGIN_BINDING_VALUE) VALUES
   (1, 'automatedExecutionType', 'COMPLETE'),
   (2, 'automatedExecutionType', 'LIGHT'),
   (3, 'automatedExecutionType', 'COMPLETE');
