INSERT INTO EXECUTION (EXECUTION_ID, NAME, PREREQUISITE, CREATED_BY, CREATED_ON)
VALUES (-635, 'jplepuise', 'jplepuise', 'dbunit', '2024-04-17');

INSERT INTO EXECUTION_STEP (EXECUTION_STEP_ID, ACTION, EXPECTED_RESULT, CREATED_BY, CREATED_ON)
VALUES (-10, '</br>', '', 'dbunit', '2024-04-17'),
       (-11, '</br>', '', 'dbunit', '2024-04-17'),
       (-12, '</br>', '', 'dbunit', '2024-04-17'),
       (-13, '</br>', '', 'dbunit', '2024-04-17'),
       (-14, '</br>', '', 'dbunit', '2024-04-17'),
       (-15, '</br>', '', 'dbunit', '2024-04-17'),
       (-16, '</br>', '', 'dbunit', '2024-04-17');

INSERT INTO EXECUTION_EXECUTION_STEPS (EXECUTION_ID, EXECUTION_STEP_ID, EXECUTION_STEP_ORDER)
VALUES (-635, -10, 0);

INSERT INTO DENORMALIZED_FIELD_VALUE (DFV_ID, CFV_ID, DENORMALIZED_FIELD_HOLDER_ID, DENORMALIZED_FIELD_HOLDER_TYPE, INPUT_TYPE, FIELD_TYPE)
VALUES (-3, null, -10, 'EXECUTION', 'PLAIN_TEXT', 'CF'),
       (-4, null, -11, 'EXECUTION', 'TAG', 'MSF'),
       (-2000, null, -12, 'EXECUTION', 'DROPDOWN_LIST', 'SSF'),
       (-42, null, -13, 'EXECUTION', 'DROPDOWN_LIST', 'SSF'),
       (-1984, null, -14, 'EXECUTION', 'DROPDOWN_LIST', 'SSF'),
       (-6, null, -15, 'EXECUTION', 'DROPDOWN_LIST', 'SSF'),
       (-6454545454545, null, -16, 'EXECUTION', 'RICH_TEXT', 'RTF');
