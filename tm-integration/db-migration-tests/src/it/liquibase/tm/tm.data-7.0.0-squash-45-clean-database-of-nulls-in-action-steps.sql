INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES (-2658);
INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES (-2659);

INSERT INTO PROJECT(PROJECT_ID, NAME, DESCRIPTION, LABEL, ACTIVE, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, CL_ID, TCL_ID, RL_ID, ATTACHMENT_LIST_ID)
VALUES (-1, 'Test Project-1', '<p>TEST</p>', 'Lorem ipsum dolor sit amet', true, 'admin', '2011-09-30 10:24:47.0', null, null, null, null, null, -2658);

INSERT INTO TEST_CASE_LIBRARY_NODE (TCLN_ID, NAME, CREATED_BY, CREATED_ON, PROJECT_ID, ATTACHMENT_LIST_ID)
VALUES (-1, 'tc1', 'admin', '2023-09-21', -1, -2659);

INSERT INTO TEST_CASE (TCLN_ID, TC_NATURE, TC_TYPE, PREREQUISITE, UUID)
VALUES (-1, 12, 20, '', 'acb3f620-7540-4db1-892d-8b778db23ab5');

INSERT INTO TEST_STEP (TEST_STEP_ID) VALUES (-1), (-2), (-3), (-4);

INSERT INTO ACTION_TEST_STEP (TEST_STEP_ID, ACTION, EXPECTED_RESULT, ATTACHMENT_LIST_ID)
VALUES (-1, 'I do this', null, -2659),
       (-2, null, null, -2659),
       (-3, null, 'The result is correct', -2659),
       (-4, 'I do this', 'It does that', -2659);

INSERT INTO ISSUE_LIST (ISSUE_LIST_ID) VALUES (-2);

INSERT INTO EXECUTION (EXECUTION_ID, TCLN_ID, PREREQUISITE, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, DESCRIPTION, NAME, EXECUTION_MODE, EXECUTION_STATUS, LAST_EXECUTED_BY, LAST_EXECUTED_ON, ATTACHMENT_LIST_ID, ISSUE_LIST_ID)
VALUES (-1, -1, '', 'admin', '2011-09-30 10:24:47.0', null, null, null, 'exec1', 'MANUAL', 'READY', null, null, -2658, -2);

INSERT INTO EXECUTION_STEP (EXECUTION_STEP_ID, EXPECTED_RESULT, ACTION, EXECUTION_STATUS, LAST_EXECUTED_BY, LAST_EXECUTED_ON, COMMENT, TEST_STEP_ID, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, ATTACHMENT_LIST_ID, ISSUE_LIST_ID)
VALUES (-1, null, 'I do this', 'READY', null, null, '', -1, 'admin', '2011-09-30 10:24:47.0', null, null, -2658, -2),
       (-2, null, '', 'READY', null, null, '', -2, 'admin', '2011-09-30 10:24:47.0', null, null, -2658, -2),
       (-3, 'The result is correct', '', 'READY', null, null, '', -1, 'admin', '2011-09-30 10:24:47.0', null, null, -2658, -2),
       (-4, 'It does that', 'I do this', 'READY', null, null, '', -1, 'admin', '2011-09-30 10:24:47.0', null, null, -2658, -2);
