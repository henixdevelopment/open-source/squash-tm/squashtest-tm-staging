INSERT INTO ENVIRONMENT_VARIABLE (EV_ID, FIELD_TYPE, NAME, INPUT_TYPE)
VALUES (-1, 'EV', 'SIMPLE1', 'PLAIN_TEXT'), (-2, 'EV', 'SIMPLE2', 'PLAIN_TEXT'), (-3, 'SSEV', 'LIST1', 'DROPDOWN_LIST');

INSERT INTO ENVIRONMENT_VARIABLE_OPTION (EV_ID, LABEL, POSITION)
VALUES (-3, 'OPT1', 0), (-3, 'OPT2', 1);

INSERT INTO THIRD_PARTY_SERVER (SERVER_ID, NAME, URL, AUTH_POLICY, AUTH_PROTOCOL)
VALUES (-76, 'AUTOM', 'http://test.fr', 'USER', 'BASIC_AUTH');

INSERT INTO TEST_AUTOMATION_SERVER (SERVER_ID, KIND, DESCRIPTION, CREATED_BY, CREATED_ON, MANUAL_SLAVE_SELECTION)
VALUES (-76, 'squashAutom', '', 'admin', '2022-12-11 15:54:21', false);

INSERT INTO PROJECT (PROJECT_ID, NAME, ATTACHMENT_LIST_ID, CREATED_BY, CREATED_ON, TA_SERVER_ID)
VALUES (-56, 'Project_bms', 753, 'liquibase',  '2022-04-08', -76);

INSERT INTO ENVIRONMENT_VARIABLE_BINDING (EVB_ID, EV_ID, BOUND_SERVER_ID)
VALUES 	(-5, -1, -76), (-2, -2, -76), (-3, -3, -76);

INSERT INTO ENVIRONMENT_VARIABLE_VALUE (EVV_ID, BOUND_ENTITY_ID, BOUND_ENTITY_TYPE, EVB_ID, VALUE)
VALUES (-1, -76, 'TEST_AUTOMATION_SERVER', -5, 'Server value ev1'),
       (-2, -76, 'TEST_AUTOMATION_SERVER', -2, 'Server value ev2'),
       (-3, -76, 'TEST_AUTOMATION_SERVER', -3, 'OPT1'),
       (-4, -56, 'PROJECT', -5, 'Project value ev1');

