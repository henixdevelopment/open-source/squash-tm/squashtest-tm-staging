/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign

import org.jooq.DSLContext
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@UnitilsSupport
class AutomatedSuiteExecutionGridIT extends DbunitServiceSpecification {

    @Inject
    DSLContext dslContext

    def "should fetch executions grid"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 10
        gridRequest.page = 0
        def grid = new AutomatedSuiteExecutionGrid("a2201092-6be7-4bae-89fa-60b8b94e6ca2")

        when:
        def response = grid.getRows(gridRequest, dslContext)

        then:
        response.count == 3
        response.dataRows.size() == 3

        checkRow(response.dataRows.get(0), "ERROR", "update data", "", null, ["Robotframework"], ["BROWSER : firefox", "VERSION : 6.0.0"])
        checkRow(response.dataRows.get(1), "BLOCKED", "logout", "", null, ["linux", "ssh", "Cucumber 5+"], null)
        checkRow(response.dataRows.get(2), "SUCCESS", "login", "", "dataset_1", ["linux", "Robotframework"], ["BROWSER : chrome", "VERSION : 5.1.0"])
    }

    def checkRow(row, executionStatus, name, reference, dataset, tags, variables) {
        with(row) {
            data["executionStatus"] == executionStatus
            data["name"] == name
            data["reference"] == reference
            data["datasetLabel"] == dataset
            if (Objects.isNull(tags)) {
                Objects.isNull(data["environmentTags"])
            } else {
                tags.every {
                    data["environmentTags"].contains(it)
                }
            }
            if (Objects.isNull(variables)) {
                Objects.isNull(data["environmentVariables"])
            } else {
                variables.every {
                    data["environmentVariables"].contains(it)
                }
            }
        }
        true // to avoid false failure in "then:" block
    }
}
