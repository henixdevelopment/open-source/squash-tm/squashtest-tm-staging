/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.query.Operation
import org.squashtest.tm.domain.query.QueryColumnPrototypeReference
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Ignore
import spock.lang.IgnoreIf
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class TestCaseSearchServiceIT extends DbunitServiceSpecification {

	static boolean isH2() {
		return System.properties['jooq.sql.dialect'] == null || System.properties['jooq.sql.dialect'] == 'H2'
	}

	@Inject
	private TestCaseSearchService service

	@Unroll
	def "should find test case by name"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_NAME"
		filterValue.values = [researchedName]
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedName | operation      || expectedIds              | expectedCount
		"sts"          | Operation.LIKE || [-14L, -13L, -12L, -11L] | 4
		"sts 130"      | Operation.LIKE || [-11L]                   | 1
		"sts 13"       | Operation.LIKE || [-14L, -13L, -12L, -11L] | 4
		"a"            | Operation.LIKE || [-16L, -15L]             | 2
	}

	@Unroll
	def "should find test case by reference"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_REFERENCE"
		filterValue.values = [researchedName]
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedName | operation      || expectedIds | expectedCount
		"a"            | Operation.LIKE || [-12L]      | 1
		"f"            | Operation.LIKE || [-15L]      | 1
		"five"         | Operation.LIKE || [-15L]      | 1
	}

	@Unroll
	def "should find test case created by"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CREATED_BY"
		filterValue.values = researchedNames
		filterValue.operation = 'IN'
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames  || expectedIds        | expectedCount
		["john"]         || [-16L]             | 1
		["mike"]         || [-12L, -11L]       | 2
		["mike", "john"] || [-16L, -12L, -11L] | 3
	}

	@Unroll
	def "should find test case by attachment count"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_ATTCOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["0"]            | Operation.EQUALS        || [-16L, -15L, -14L, -13L, -12L]       | 5
		["1"]            | Operation.LOWER         || [-16L, -15L, -14L, -13L, -12L]       | 5
		["1"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["1"]            | Operation.EQUALS        || [-11L]                               | 1
		["0", "1"]       | Operation.BETWEEN       || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["0"]            | Operation.GREATER       || [-11L]                               | 1
		["0"]            | Operation.GREATER_EQUAL || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["2"]            | Operation.GREATER       || []                                   | 0
	}

	@Unroll
	def "should find test case by tag cuf"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CUF_TAG"
		filterValue.values = researchedNames
		filterValue.operation = operation
		filterValue.cufId = -6L
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames                   | operation || expectedIds                          | expectedCount
		["Success"]                       | "IN"      || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["BFR"]                           | "IN"      || [-15L]                               | 1
		["Space shuttle mission", "BFR"]  | "IN"      || [-15L, -14L, -13L, -12L, -11L]       | 5
		["NOOP"]                          | "IN"      || []                                   | 0
		["Space shuttle mission", "BFR"]  | "AND"     || []                                   | 0
		["To The Moon", "Success"]        | "AND"     || [-16L, -15L]                         | 2
		["To The Moon", "Success", "BFR"] | "AND"     || [-15L]                               | 1
	}

	@Unroll
	def "should find test case by text cuf"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CUF_TEXT"
		filterValue.values = researchedNames
		filterValue.operation = operation
		filterValue.cufId = -1L
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames | operation || expectedIds  | expectedCount
		["last"]        | "LIKE"    || [-14L]       | 1
		["la"]          | "LIKE"    || [-16L, -14L] | 2
	}


	@Unroll
	def "should order test case by reference"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = QueryColumnPrototypeReference.TEST_CASE_NAME
		filterValue.values = [researchedName]
		filterValue.operation = 'LIKE'
		def sort = new GridSort()
		sort.columnPrototype = QueryColumnPrototypeReference.TEST_CASE_REFERENCE;
		sort.direction = GridSort.SortDirection.DESC;
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]
		request.sort = [sort]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedName || expectedIds              | expectedCount
		"sts"          || [-14L, -13L, -11L, -12L] | 4
		"a"            || [-16L, -15L]             | 2
	}

	@Unroll
	def "should find test case modified by"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_MODIFIED_BY"
		filterValue.values = researchedNames
		filterValue.operation = 'IN'
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames  || expectedIds        | expectedCount
		["john"]         || [-12L]             | 1
		["mike"]         || [-16L, -11L]       | 2
		["mike", "john"] || [-16L, -12L, -11L] | 3
	}

	@Unroll
	def "should find test case by id"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_ID"
		filterValue.values = ids
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		ids            | operation               || expectedIds                    | expectedCount
		["-12"]        | Operation.EQUALS        || [-12L]                         | 1
		["-16"]        | Operation.EQUALS        || [-16L]                         | 1
		["-8"]         | Operation.EQUALS        || []                             | 0
		["-12"]        | Operation.LOWER         || [-16L, -15L, -14L, -13L]       | 4
		["-12"]        | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L] | 5
		["-13"]        | Operation.GREATER       || [-12L, -11L]                   | 2
		["-13"]        | Operation.GREATER_EQUAL || [-13L, -12L, -11L]             | 3
		["-15", "-13"] | Operation.BETWEEN       || [-15L, -14L, -13L]             | 3
	}

	@IgnoreIf({ TestCaseSearchServiceIT.isH2() })
	@Unroll
	def "should find test case by createdOn"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CREATED_ON"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues             | operation               || expectedIds                          | expectedCount
		["2012-11-13"]               | Operation.EQUALS        || [-13L, -12L, -11L]                   | 3
		["2012-11-13"]               | Operation.LOWER         || []                                   | 0
		["2012-11-16"]               | Operation.LOWER_EQUAL   || [-14L, -13L, -12L, -11L]             | 4
		["2012-12-13"]               | Operation.EQUALS        || [-16L, -15L]                         | 2
		["2012-11-13", "2012-12-13"] | Operation.BETWEEN       || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["2012-12-13", "2012-11-13"] | Operation.BETWEEN       || []                                   | 0
		["2012-11-16"]               | Operation.GREATER       || [-16L, -15L]                         | 2
		["2012-11-16"]               | Operation.GREATER_EQUAL || [-16L, -15L, -14L]                   | 3
		["2012-12-13"]               | Operation.GREATER       || []                                   | 0
	}

	@IgnoreIf({ TestCaseSearchServiceIT.isH2() })
	@Unroll
	def "should find test case by modifiedOn"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_MODIFIED_ON"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues             | operation               || expectedIds                    | expectedCount
		["2012-11-24"]               | Operation.EQUALS        || [-12L]                         | 1
		["2012-11-24"]               | Operation.LOWER         || [-13L]                         | 1
		["2012-11-24"]               | Operation.LOWER_EQUAL   || [-13L, -12L]                   | 2
		["2012-12-18"]               | Operation.EQUALS        || [-16L, -15L]                   | 2
		["2012-11-13", "2012-12-13"] | Operation.BETWEEN       || [-13L, -12L, -11L]             | 3
		["2012-12-13", "2012-11-13"] | Operation.BETWEEN       || []                             | 0
		["2012-11-16"]               | Operation.GREATER       || [-16L, -15L, -14L, -12L, -11L] | 5
		["2012-11-16"]               | Operation.GREATER_EQUAL || [-16L, -15L, -14L, -12L, -11L] | 5
		["2012-12-13"]               | Operation.GREATER       || [-16L, -15L, -14L]             | 3
	}

	@Unroll
	def "should find test case by kind"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_KIND"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues                                       | operation    || expectedIds                          | expectedCount
		["org.squashtest.tm.domain.testcase.TestCase"]         | Operation.IN || [-15L, -13L, -12L, -11L]             | 4
		["org.squashtest.tm.domain.testcase.ScriptedTestCase"] | Operation.IN || [-16L, -14]                          | 2
		["org.squashtest.tm.domain.testcase.TestCase",
		 "org.squashtest.tm.domain.testcase.ScriptedTestCase"] | Operation.IN || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
	}

	@Unroll
	def "should find test case by status"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_STATUS"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues                 | operation    || expectedIds              | expectedCount
		["UNDER_REVIEW"]                 | Operation.IN || [-16L]                   | 1
		["WORK_IN_PROGRESS"]             | Operation.IN || [-12L, -11L]             | 2
		["APPROVED"]                     | Operation.IN || [-14L, -13L]             | 2
		["APPROVED", "WORK_IN_PROGRESS"] | Operation.IN || [-14L, -13L, -12L, -11L] | 4
		["APPROVED", "UNDER_REVIEW"]     | Operation.IN || [-16L, -14L, -13L]       | 3
	}

	@Unroll
	def "should find test case by automationRequest status"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "AUTOMATION_REQUEST_STATUS"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues                  | operation    || expectedIds        | expectedCount
		["WORK_IN_PROGRESS"]              | Operation.IN || [-12L, -11L]       | 2
		["AUTOMATED"]                     | Operation.IN || [-13L]             | 1
		["REJECTED"]                      | Operation.IN || [-15L]             | 1
		["WORK_IN_PROGRESS", "AUTOMATED"] | Operation.IN || [-13L, -12L, -11L] | 3
		["REJECTED", "AUTOMATED"]         | Operation.IN || [-15L, -13L]       | 2
		["WORK_IN_PROGRESS", "REJECTED"]  | Operation.IN || [-15L, -12L, -11L] | 3
		["READY_TO_TRANSMIT"]             | Operation.IN || []                 | 0
	}

	@Unroll
	def "should find test case by milestone id"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_MILESTONE_ID"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation    || expectedIds  | expectedCount
		["-1"]           | Operation.IN || [-12L, -11L] | 2
		["-2"]           | Operation.IN || [-12L]       | 1
		["-1", "-2"]     | Operation.IN || [-12L, -11L] | 2
		["-3"]           | Operation.IN || []           | 0
	}

	@Unroll
	def "should find test case by milestone status"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_MILESTONE_STATUS"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues           | operation    || expectedIds  | expectedCount
		["PLANNED"]                | Operation.IN || [-12L, -11L] | 2
		["PLANNED", "IN_PROGRESS"] | Operation.IN || [-12L, -11L] | 2
		["IN_PROGRESS"]            | Operation.IN || []           | 0
	}

	@IgnoreIf({ TestCaseSearchServiceIT.isH2() })
	@Unroll
	def "should find test case by milestone endDate"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_MILESTONE_END_DATE"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues             | operation             || expectedIds  | expectedCount
		["2014-10-10"]               | Operation.EQUALS      || [-12L, -11L] | 2
		["2014-10-11"]               | Operation.EQUALS      || [-12L]       | 1
		["2014-10-11"]               | Operation.LOWER       || [-11L]       | 1
		["2014-10-11"]               | Operation.LOWER_EQUAL || [-12L, -11L] | 2
		["2014-10-10", "2014-10-11"] | Operation.BETWEEN     || [-12L, -11L] | 2
		["2014-10-10"]               | Operation.GREATER     || [-12L]       | 1
		["2014-10-11"]               | Operation.GREATER     || []           | 0
	}

	@Unroll
	def "should find test case by stepCount"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_STEPCOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["3"]            | Operation.EQUALS        || [-12L]                               | 1
		["1"]            | Operation.EQUALS        || [-15L, -11L]                         | 2
		["0"]            | Operation.EQUALS        || [-16L, -14L, -13L]                   | 3
		["3"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["3"]            | Operation.LOWER         || [-16L, -15L, -14L, -13L, -11L]       | 5
		["1"]            | Operation.GREATER       || [-12L]                               | 1
		["1"]            | Operation.GREATER_EQUAL || [-15L, -12L, -11L]                   | 3
	}

	@Unroll
	def "should find test case by parameterCount"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_PARAMCOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["3"]            | Operation.EQUALS        || []                                   | 0
		["2"]            | Operation.EQUALS        || [-15L, -12L]                         | 2
		["1"]            | Operation.EQUALS        || [-11L]                               | 1
		["0"]            | Operation.EQUALS        || [-16L, -14L, -13L]                   | 3
		["2"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["2"]            | Operation.LOWER         || [-16L, -14L, -13L, -11L]             | 4
		["1"]            | Operation.GREATER       || [-15L, -12L]                         | 2
		["1"]            | Operation.GREATER_EQUAL || [-15L, -12L, -11L]                   | 3
	}

	@Unroll
	def "should find test case by dataSetCount"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_DATASETCOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["3"]            | Operation.EQUALS        || []                                   | 0
		["2"]            | Operation.EQUALS        || [-12L]                               | 1
		["1"]            | Operation.EQUALS        || [-15L]                               | 1
		["0"]            | Operation.EQUALS        || [-16L, -14L, -13L, -11L]             | 4
		["2"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["2"]            | Operation.LOWER         || [-16L, -15L, -14L, -13L, -11L]       | 5
		["1"]            | Operation.GREATER       || [-12L]                               | 1
		["1"]            | Operation.GREATER_EQUAL || [-15L, -12L]                         | 2
	}

	@Unroll
	def "should find test case by callStepCount"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CALLSTEPCOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["2"]            | Operation.EQUALS        || []                                   | 0
		["1"]            | Operation.EQUALS        || [-11L]                               | 1
		["0"]            | Operation.EQUALS        || [-16L, -15L, -14L, -13L, -12L]       | 5
		["1"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["1"]            | Operation.LOWER         || [-16L, -15L, -14L, -13L, -12L]       | 5
		["0"]            | Operation.GREATER       || [-11L]                               | 1
		["0"]            | Operation.GREATER_EQUAL || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
	}

	@Unroll
	def "should find test case by coverages"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_VERSCOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["1"]            | Operation.EQUALS        || [-12L]                               | 1
		["0"]            | Operation.EQUALS        || [-16L, -15L, -14L, -13L, -11L]       | 5
		["1"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
		["1"]            | Operation.LOWER         || [-16L, -15L, -14L, -13L, -11L]       | 5
		["0"]            | Operation.GREATER       || [-12L]                               | 1
		["0"]            | Operation.GREATER_EQUAL || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
	}

	@Unroll
	def "should find test case by iterations"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_ITERCOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["3"]            | Operation.EQUALS        || []                                   | 0
		["2"]            | Operation.EQUALS        || [-13L]                               | 1
		["1"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -12L, -11L]       | 5
		["1"]            | Operation.LOWER         || [-16L, -15L, -14L]                   | 3
		["0"]            | Operation.GREATER       || [-13L, -12L, -11L]                   | 3
		["0"]            | Operation.GREATER_EQUAL || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
	}

	@Unroll
	def "should find test case by executions"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_EXECOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["5"]            | Operation.EQUALS        || [-11L]                               | 1
		["1"]            | Operation.EQUALS        || [-12L]                               | 1
		["1"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L]       | 5
		["1"]            | Operation.LOWER         || [-16L, -15L, -14L, -13L]             | 4
		["0"]            | Operation.GREATER       || [-12L, -11L]                         | 2
		["0"]            | Operation.GREATER_EQUAL || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
	}

	@Ignore
	@Unroll
	def "should find test case by issues"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "EXECUTION_ISSUECOUNT"
		filterValue.values = researchedValues
		filterValue.operation = operation
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedValues | operation               || expectedIds                          | expectedCount
		["2"]            | Operation.EQUALS        || [-11L]                               | 1
		["1"]            | Operation.EQUALS        || [-12L]                               | 1
		["1"]            | Operation.LOWER_EQUAL   || [-16L, -15L, -14L, -13L, -12L]       | 5
		["1"]            | Operation.LOWER         || [-16L, -15L, -14L, -13L]             | 4
		["0"]            | Operation.GREATER       || [-12L, -11L]                         | 2
		["0"]            | Operation.GREATER_EQUAL || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
	}

	@IgnoreIf({ TestCaseSearchServiceIT.isH2() })
	@Unroll
	def "should find test case by date cuf"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CUF_DATE"
		filterValue.values = researchedNames
		filterValue.operation = operation
		filterValue.cufId = -5L
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames | operation || expectedIds  | expectedCount
		["2020-05-18"]  | "EQUALS"  || [-12L, -11L] | 2
		["2020-04-18"]  | "EQUALS"  || [-12L, -11L] | 2
		["2020-05-18"]  | "LOWER"   || [-12L, -11L] | 2
		["2020-04-18"]  | "GREATER" || [-12L, -11L] | 2
	}

	@Unroll
	def "should find test case by list cuf"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CUF_LIST"
		filterValue.values = researchedNames
		filterValue.operation = operation
		filterValue.cufId = -3L
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:

		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames        | operation || expectedIds        | expectedCount
		["Option1"]            | "IN"      || [-12L, -11L]       | 2
		["Option2"]            | "IN"      || [-13L]             | 1
		["Option3"]            | "IN"      || []                 | 0
		["Option2", "Option1"] | "IN"      || [-13L, -12L, -11L] | 3
	}

	@Unroll
	def "should find test case by checkbox cuf"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "TEST_CASE_CUF_CHECKBOX"
		filterValue.values = researchedNames
		filterValue.operation = operation
		filterValue.cufId = -7L
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:

		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames   | operation || expectedIds                          | expectedCount
		["true"]          | "IN"      || [-16L, -13L, -12L, -11L]             | 4
		["false"]         | "IN"      || [-15L, -14L]                         | 2
		["true", "false"] | "IN"      || [-16L, -15L, -14L, -13L, -12L, -11L] | 6
	}

}
