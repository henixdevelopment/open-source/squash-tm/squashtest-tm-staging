/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.testcase

import org.jooq.DSLContext
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class TestCaseSessionGridIT extends DbunitServiceSpecification {

    @Inject
    DSLContext dslContext

    def "should fetch sessions grid"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 5
        gridRequest.page = 0
        def grid = new TestCaseSessionsGrid(-1)

        when:
        def response = grid.getRows(gridRequest, dslContext)

        then:
        response.count == 1
        response.dataRows.size() == 1
        def data = response.dataRows[0].data
        data["executionPath"] == "Project > Campaign > Iteration"
        data["executionStatus"] == "SUCCESS"
        data["executionCount"] == 1
        data["nbIssues"] == 0
    }
}
