/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport
import org.squashtest.tm.service.internal.display.grid.GridRequest

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class TestCaseSearchGridDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private TestCaseSearchGridDisplayService service

	def "should return a grid response" () {

		given:
		def ids = [-11L, -12L, -13L, -14L]
		def researchResult = new ResearchResult(ids, 4)
        def gridId = "test-case-search"
        def gridRequest = new GridRequest()
        gridRequest.setGridId(gridId)
        gridRequest.setSimplifiedColumnDisplayGridIds([])

		when:
		def response = service.fetchResearchRows(researchResult,gridRequest)
		then:
		response.count == 4
		def dataRows = response.dataRows
		def firstTestCase = dataRows[0]
		firstTestCase.data["id"] == -11L
		firstTestCase.data["name"] == "test case 1"
		firstTestCase.data["attachments"] == 1
		firstTestCase.data["steps"] == 2
		firstTestCase.data["milestones"] == 1
		firstTestCase.data["coverages"] == 1
		firstTestCase.data["iterations"] == 1
        firstTestCase.data["draftedByAi"] == true
		def lastTestCase = dataRows[3]
		lastTestCase.data["id"] == -14L
		lastTestCase.data["name"] == "test case 4"
		lastTestCase.data["attachments"] == 0
		lastTestCase.data["steps"] == 1
		lastTestCase.data["milestones"] == 0
		lastTestCase.data["coverages"] == 0
		lastTestCase.data["iterations"] == 0
        lastTestCase.data["draftedByAi"] == false
	}
}
