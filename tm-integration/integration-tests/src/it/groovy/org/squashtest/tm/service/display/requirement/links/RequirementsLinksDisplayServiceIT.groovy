/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.requirement.links

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.display.requirements.links.RequirementsLinksDisplayService
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class RequirementsLinksDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private RequirementsLinksDisplayService requirementsLinksDisplayService

	def "should fetch requirements links"() {

		when:
		def requirementsLinks = requirementsLinksDisplayService.findAllRequirementsLinksType()

		then:
		requirementsLinks.size() == 2
		def dataRequirementsLink1 = requirementsLinks[0];
		def dataRequirementsLink2 = requirementsLinks[1];

		dataRequirementsLink1.id == -2L
		dataRequirementsLink1.role == "role3"
		dataRequirementsLink1.role1Code == "code3"
		dataRequirementsLink1.role2 == "role4"
		dataRequirementsLink1.role2Code == "code4"
		dataRequirementsLink1.default == false

		dataRequirementsLink2.id == -1L
		dataRequirementsLink2.role == "role1"
		dataRequirementsLink2.role1Code == "code1"
		dataRequirementsLink2.role2 == "role2"
		dataRequirementsLink2.role2Code == "code2"
		dataRequirementsLink2.default == true
	}
}
