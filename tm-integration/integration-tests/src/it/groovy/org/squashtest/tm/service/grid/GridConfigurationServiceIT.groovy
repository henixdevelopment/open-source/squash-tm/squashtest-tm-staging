/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.grid

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.service.internal.repository.GridColumnDisplayConfigurationDao
import org.squashtest.tm.service.internal.repository.GridColumnDisplayReferenceDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class GridConfigurationServiceIT extends DbunitServiceSpecification {

    @Inject
    private GridConfigurationService gridConfigurationService

    @Inject
    private GridColumnDisplayReferenceDao gridColumnDisplayReferenceDao

    @Inject
    private GridColumnDisplayConfigurationDao gridColumnDisplayConfigurationDao

    def "should update a grid configuration for a given user"() {
        given:
        def user = findEntity(User.class, -1L)
        def gridId = "requirement-search"
        def columnIds = ["attachments", "coverages", "milestones"]

        when:
        this.gridConfigurationService.addOrUpdateGridColumnConfig(user, gridId, columnIds)

        then:
        def gridColumnDisplayReference = this.gridColumnDisplayReferenceDao.findByUserAndGridId(user, gridId)
        def activeColumnIds = this.gridConfigurationService.findActiveColumnIdsByGridColumnDisplayReferenceId(gridColumnDisplayReference.id)

        activeColumnIds.size() == 5
        activeColumnIds.sort()
        activeColumnIds.get(0) == "attachments"
        activeColumnIds.get(1) == "coverages"
        activeColumnIds.get(2) == "milestones"
        activeColumnIds.get(3) == "name"
        activeColumnIds.get(4) == "reqVersionBoundToItem"
    }

    def "should fetch a list of column ids for a given reference id"() {
        given:
        def user = findEntity(User.class, -1L)
        def gridId = "requirement-search"
        def gridColumnDisplayReference = this.gridColumnDisplayReferenceDao.findByUserAndGridId(user, gridId)

        when:
        def activeColumnIds = this.gridConfigurationService.findActiveColumnIdsByGridColumnDisplayReferenceId(gridColumnDisplayReference.id)

        then:
        activeColumnIds.size() == 3
        activeColumnIds.sort()
        activeColumnIds.get(0) == "attachments"
        activeColumnIds.get(1) == "projectName"
        activeColumnIds.get(2) == "versionsCount"
    }

}
