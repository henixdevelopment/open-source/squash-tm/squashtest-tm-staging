/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.user

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.domain.users.ApiToken
import org.squashtest.tm.domain.users.ApiTokenPermission
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.service.internal.repository.ApiTokenDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@UnitilsSupport
@Transactional
class ApiTokenServiceIT extends DbunitServiceSpecification {

    @Inject
    private ApiTokenService apiTokenService

    @Inject
    private ApiTokenDao apiTokenDao

    def "should persist an API token"() {
        given:
        User user = findEntity(User.class, -154L)

        when:
        apiTokenService.persistApiToken(
            user,
            UUID.randomUUID().toString(),
            'my_new_token',
            new Date(),
            ApiTokenPermission.READ_WRITE.name()
        )

        then:
        List<ApiToken> tokensByUser = apiTokenDao.findAllByUserId(-154L)
        tokensByUser.size() == 2
    }

    def "should delete an API token"() {
        given:
        UserContextHelper.setUsername('Gnocchi')

        when:
        apiTokenService.deletePersonalApiToken(-1L)

        then:
        !found(ApiToken.class, -1L)
    }
}
