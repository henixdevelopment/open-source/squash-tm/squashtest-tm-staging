/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display


import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.basespecs.QueryCountTracker
import org.squashtest.tm.domain.NodeReference
import org.squashtest.tm.domain.NodeType
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@NotThreadSafe
@DataSet("DatasetDuplicationTreeBrowserDaoIT.xml")
@QueryCountTracker
class DatasetDuplicationTreeBrowserDaoIT extends DbunitServiceSpecification {

	@Inject
	DatasetDuplicationTreeBrowserDao datasetDuplicationTreeBrowserDao

	def "should get eligible test case node references"() {
		given:
		Set<NodeReference> nodesToFilter = new HashSet<NodeReference>([
			new NodeReference(NodeType.TEST_CASE, -1L),
			new NodeReference(NodeType.TEST_CASE, -2L),
			new NodeReference(NodeType.TEST_CASE, -3L),
			new NodeReference(NodeType.TEST_CASE_FOLDER, -4L),
		])

		def parameters = List.of("param1", "param2")

		Set<NodeReference> rootNodes = new HashSet<NodeReference>([new NodeReference(NodeType.TEST_CASE_LIBRARY, -1L)])

		when:
		def eligibleTestCases = datasetDuplicationTreeBrowserDao.getEligibleTestCaseNodeReferences(nodesToFilter, parameters)

		then:
		eligibleTestCases.size() == 3
	}

}
