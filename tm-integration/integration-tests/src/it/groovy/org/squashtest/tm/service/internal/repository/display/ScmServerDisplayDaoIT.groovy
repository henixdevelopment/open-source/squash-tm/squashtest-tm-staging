/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class ScmServerDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	ScmServerDisplayDao dao


	def "find all scm servers"() {
		when:
		def scmServers = dao.findAll()
		then:
		scmServers.size() == 2
		def firstServer = scmServers.get(0)
		firstServer.name == "Gitlab"
		firstServer.url == "https://gitlab.com/RecetteSquash"
		firstServer.kind == "git"
		firstServer.repositories.size() == 1
		def firstServerRepo = firstServer.repositories.get(0)
		firstServerRepo.name == "Repo3"
		firstServerRepo.workingBranch == "master"

		def secondServer = scmServers.get(1)
		secondServer.name == "Github"
		secondServer.url == "https://github.com/RecetteSquash"
		secondServer.kind == "git"
		secondServer.repositories.size() == 2
		def firstServerFirstRepo = secondServer.repositories.get(0)
		firstServerFirstRepo.name == "Repo2"
		firstServerFirstRepo.workingBranch == "test-github"
		def firstServerSecondRepo = secondServer.repositories.get(1)
		firstServerSecondRepo.name == "Repo1"
		firstServerSecondRepo.workingBranch == "master"

	}
}
