/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter.parsers


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.projectimporter.PivotFormatImport
import org.squashtest.tm.domain.requirement.RequirementCriticality
import org.squashtest.tm.domain.requirement.RequirementStatus
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata
import org.squashtest.tm.service.internal.dto.projectimporter.SquashCustomFieldInfo
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class RequirementWorkspaceParserIT extends DbunitServiceSpecification {

    @Inject RequirementWorkspaceParser requirementWorkspaceParser

    def pivotImportMetadata = new PivotImportMetadata()
    def pivotFormatImport = new PivotFormatImport()


    def setup() {
        pivotImportMetadata.customFieldIdsMap.put("cuf1", new SquashCustomFieldInfo(1L, InputType.CHECKBOX))
        pivotImportMetadata.customFieldIdsMap.put("cuf2", new SquashCustomFieldInfo(2L, InputType.PLAIN_TEXT))
        pivotFormatImport.id = 1
    }

    def "should parse a requirement"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/requirement.json")

        when:
            def requirementToImport = requirementWorkspaceParser.parseRequirement(jsonParser,pivotImportMetadata, pivotFormatImport)

        then:
            requirementToImport.internalId == "RAA"
            requirementToImport.status == RequirementStatus.UNDER_REVIEW
            requirementToImport.parentType == EntityType.REQUIREMENT
            requirementToImport.parentId == "RA"

            requirementToImport.requirement.name == "reqA.A"
            requirementToImport.requirement.description == "desc reqA.A"
            requirementToImport.requirement.reference == "reqA.A.AAA"
            requirementToImport.requirement.criticality == RequirementCriticality.MINOR
            requirementToImport.requirement.category == "CAT_UNDEFINED"
            requirementToImport.requirement.customFields.size() == 2
            requirementToImport.requirement.customFields.get(1L).value == "false"
            requirementToImport.requirement.customFields.get(2L).value == "a value"
            requirementToImport.attachments[0].zipImportFileName == "AT06-attachment.jpg"
            requirementToImport.attachments[0].originalFileName == "attachment.jpg"
    }

}
