/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.campaign.Sprint
import org.squashtest.tm.domain.campaign.SprintStatus
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class CustomSprintModificationServiceIT extends DbunitServiceSpecification {

    @Inject
    private CustomSprintModificationService customSprintModificationService

    @Inject
    private SprintReqVersionDao sprintReqVersionDao

    def "should rename a sprint"() {
        given:
        def sprintId = -1L

        when:
        customSprintModificationService.rename(sprintId, "UltimateSprintName")
        def sprint = findEntity(Sprint.class, sprintId)

        then:
        sprint.getName() == "UltimateSprintName"
    }

    def "should denormalize sprint req versions if mode native"() {
        given:
        def sprintId = -1L
        def reqVersionIds = [-1L, -2L]

        when:
        customSprintModificationService.denormalizeSprintReqVersions(sprintId)
        def sprintReqVersions =
            sprintReqVersionDao
                .findAllBySprintIdAndRequirementVersionIdInOrderByRequirementVersionId(sprintId, reqVersionIds)

        then:
        sprintReqVersions[idx].reference == reference
        sprintReqVersions[idx].name == name
        sprintReqVersions[idx].status == status
        sprintReqVersions[idx].criticality == criticality
        sprintReqVersions[idx].category == category
        sprintReqVersions[idx].description == description

        where:
        idx | reference     | name            | status             | criticality | category                            | description
        1   | "Reference 1" | "a requirement" | "WORK_IN_PROGRESS" | "MAJOR"     | "requirement.category.CAT_BUSINESS" | "Description 1"
        0   | null          | null            | null               | null        | null                                | null
    }
}
