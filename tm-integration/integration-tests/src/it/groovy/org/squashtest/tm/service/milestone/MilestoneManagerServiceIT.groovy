/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.milestone

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.domain.milestone.Milestone
import org.squashtest.tm.domain.milestone.MilestoneStatus
import org.squashtest.tm.exception.milestone.MilestoneLabelAlreadyExistsException
import org.squashtest.tm.service.internal.dto.UserDto
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao
import org.squashtest.tm.service.user.UserAccountService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class MilestoneManagerServiceIT extends DbunitServiceSpecification {

	@Inject private MilestoneManagerService manager
    @Inject private MilestoneDisplayDao milestoneDisplayDao
    @Inject private UserAccountService userAccountService

	def setup() {
		UserContextHelper.setUsername("admin")
	}


	def "should find all milestones"(){
		given :

		when :
		def result = manager.findAll()
		then :
		result.size() == 4
		result.collect{it.id} as Set == [-1, -2, -3, -4] as Set
		result.collect{it.label} as Set == ["My milestone", "My milestone 2", "My milestone 3", "My milestone 4"] as Set
		result.collect{it.status} as Set == [MilestoneStatus.PLANNED, MilestoneStatus.PLANNED, MilestoneStatus.IN_PROGRESS, MilestoneStatus.LOCKED] as Set
	}

	def "should change status"(){

		given :
		when :
		manager.changeStatus(-1L, MilestoneStatus.IN_PROGRESS)
		def milestone = manager.findById(-1L);
		then :
		milestone.status == MilestoneStatus.IN_PROGRESS
	}

	def "should delete milestone"(){

		given :
		def ids = [-1L, -4L]
		when :
		manager.removeMilestones(ids)
		def result = manager.findAll()
		then :
		result.size() == 2
		result.collect{it.id} as Set == [-2, -3] as Set
		result.collect{it.label} as Set == ["My milestone 2", "My milestone 3"]  as Set
		result.collect{it.status}  as Set == [MilestoneStatus.PLANNED, MilestoneStatus.IN_PROGRESS]  as Set
	}

	def "label should be unique"(){
		given :
		def duplicateLabel = "My milestone 2"
		Milestone milestone = new Milestone(label:duplicateLabel)

		when :
		manager.addMilestone(milestone)

		then :
		thrown(MilestoneLabelAlreadyExistsException)
	}

	def "label should not contain forbiden characters"(){
		given :
		def badLabel = "Milestone |"
		Milestone milestone = new Milestone(label:badLabel)

		when :
		manager.addMilestone(milestone)
		manager.findByName("Milestone |")

		then :
		thrown(DataIntegrityViolationException)
	}

	@Unroll("for project : #id is bound to template : #boundToTemplate")
	def "should know if the milestone is bound to a template"(){
		given :

		when :
		def result = manager.isBoundToATemplate(id)
		then :
		result == boundToTemplate
		where :
		id || boundToTemplate
		-1L || false
		-2L || true
		-3L || true
		-4L || false
	}

	def "should find milestones by names and status"() {
		given:
		def names = ["My milestone", "My milestone 3", "Whatever"]

		when:
		def res = manager.findAllByNamesAndStatus(names, MilestoneStatus.IN_PROGRESS);

		then:
		res*.label == ["My milestone 3"]
	}

	def "should find existing milestones names"() {
		given:
		def names = ["My milestone", "My milestone 3", "Whatever"]

		when:
		def res = manager.findExistingNames(names);

		then:
		res.containsAll(["My milestone", "My milestone 3"])
		res.size() == 2
	}

	def "should find existing, in preogress milestones names"() {
		given:
		def names = ["My milestone", "My milestone 3", "Whatever"]
		def status = MilestoneStatus.getAllStatusAllowingObjectBind()

		when:
		def res = manager.findBindableExistingNames(names, status, "template");

		then:
		res == ["My milestone 3"]
	}

	def "should return all milestone labels by given milestone ids ordered by labels"() {
        given:
        def milestoneIds = [-4L, -3L, -2L, -1L]
        def projectIds = [-1L, -2L]

		when:
		def result = manager.findMilestoneLabelByIds(milestoneIds, projectIds)
		then:
		result.size() == 4
		result == ["My milestone", "My milestone 2", "My milestone 3", "My milestone 4"]
	}

	/*
	 * Please see MilestoneManagerAsProjectLeaderServiceIT for informations about the dataset
	 */
	@Unroll("should  synchronize for admin : source id : #sourceId, targetId :  #targetId union : #isUnion")
	@DataSet("MilestoneManagerService2IT.xml")
	def "should synchronize as Admin"(){
		given :
		when :
		manager.synchronize(sourceId, targetId, extendPerimeter, isUnion)
		def target = manager.findById(targetId)
		def source = manager.findById(sourceId)
		then :
		target.perimeter*.id as Set == targetProjectIds as Set
		target.projects*.id as Set == targetProjectIds as Set
		target.testCases*.id as Set == targetObjIds as Set

		target.requirementVersions*.id as Set == targetObjIds as Set

		source.perimeter*.id as Set == sourceProjectIds as Set
		source.projects*.id as Set == sourceProjectIds as Set
		source.testCases*.id as Set == sourceObjIds as Set

		source.requirementVersions*.id as Set == sourceObjIds as Set
		where :
		sourceId | targetId | extendPerimeter | isUnion  ||  sourceProjectIds           |  targetProjectIds           |    sourceObjIds                   |    targetObjIds
		-1    |     -2    |       false     |   false ||      [-1, -2, -3, -4]|  [-1, -2, -3, -4, -5, -6]|	  [-1, -3, -5, -7]|     [-1, -3, -5, -6, -7, -8, -9, -11]
		-1    |     -7    |       false     |   false ||      [-1, -2, -3, -4]|  [-3, -4, -5, -6]|	  [-1, -3, -5, -7]|     [-5, -6, -7, -8, -9, -11]
		-6    |     -7    |       false     |   false ||      [-1, -2, -3, -4]|  [-3, -4, -5, -6]|	  [-1, -3, -5, -7]|     [-5, -6, -7, -8, -9, -11]


		-1    |     -2    |       false     |   true  ||   [-1, -2, -3, -4, -5, -6]|  [-1, -2, -3, -4, -5, -6]| [-1, -3, -5, -6, -7, -8, -9, -11]|     [-1, -3, -5, -6, -7, -8, -9, -11]
		-1    |     -7    |       false     |   true  ||      [-1, -2, -3, -4]|  [-3, -4, -5, -6]|    [-1, -3, -5, -6, -7, -8]|     [-5, -6, -7, -8, -9, -11]
		-6    |     -7    |       false     |   true  ||      [-1, -2, -3, -4]|  [-3, -4, -5, -6]|    [-1, -3, -5, -6, -7, -8]|     [-5, -6, -7, -8, -9, -11]
	}

    def "should forbid access to milestone view to unauthorized project managers"() {
        UserDto user = Mock()
        user.isAdmin() >> false
        user.getUsername() >> "jane.doe"
        user.getUserId() >> -2
        userAccountService.findCurrentUserDto() >> user

        manager.canManageMilestoneOrAdmin(-1L)
        manager.canManageMilestoneOrAdmin(-2L)
        !manager.canManageMilestoneOrAdmin(-3L)
        manager.canManageMilestoneOrAdmin(-4L)
    }
}
