/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@UnitilsSupport
@DataSet
@Transactional
class RequirementVersionCoverageDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	private RequirementVersionCoverageDisplayDao dao

	@PersistenceContext
	private EntityManager entityManager

	def setup() {
		def ids = [-11L, -21L, -31L, -41L, -51L]
		ids.each {
			setBidirectionalReqReqVersion(it, it)
		}
	}

	def setBidirectionalReqReqVersion(Long reqVersionId, Long reqId) {
		def reqVer = entityManager.find(RequirementVersion.class, reqVersionId)
		def req = entityManager.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		// Flush is necessary as the dao use native query through jooq.
		entityManager.flush()
	}

	def "should find coverages for direct coverage"() {

		when:
		def coverages = dao.findByTestCaseIds(-11L, [] as Set)

		then:
		coverages.size() == 1
		def coverage = coverages.get(0)
		coverage.name == "Requirement1"
		coverage.requirementVersionId == -11L
		coverage.criticality == "CRITICAL"
		coverage.status == "WORK_IN_PROGRESS"
		coverage.projectId == -1L
		coverage.projectName == "Project1"
		coverage.verifyingCalledTestCaseIds.size() == 0
		coverage.directlyVerified
		!coverage.unDirectlyVerified
		coverage.coverageStepInfos.size() == 0
	}

	def "should find and aggregate coverages for direct coverage on steps"() {
		when:
		def coverages = dao.findByTestCaseIds(-21L, [] as Set)

		then:
		coverages.size() == 3

		//checking coverage on requirement -31. This coverage is linked to one ActionStep
		def coverage31 = coverages.get(0)
		coverage31.requirementVersionId == -31L
		coverage31.directlyVerified
		!coverage31.unDirectlyVerified
		coverage31.verifyingCalledTestCaseIds.size() == 0
		coverage31.coverageStepInfos.size() == 1
		coverage31.coverageStepInfos.get(0).id == -2L
		coverage31.coverageStepInfos.get(0).index == 1

		//checking coverage on requirement -31. This coverage is linked to two ActionStep
		def coverage21 = coverages.get(1)
		coverage21.requirementVersionId == -21L
		coverage21.directlyVerified
		!coverage21.unDirectlyVerified
		coverage21.verifyingCalledTestCaseIds.size() == 0
		coverage21.coverageStepInfos.size() == 2
		coverage21.coverageStepInfos.collect{it.id}.sort() == [-2L, -1L]

		//checking coverage on requirement -11. This coverage is just on TestCase, no StepVerifying
		def coverage11 = coverages.get(2)
		coverage11.requirementVersionId == -11L
		coverage11.directlyVerified
		!coverage11.unDirectlyVerified
		coverage11.verifyingCalledTestCaseIds.size() == 0
		coverage11.coverageStepInfos.size() == 0
	}

	def "should find and aggregate coverages through call steps"() {
		when:
		def coverages = dao.findByTestCaseIds(-31L, [-41L, -21L] as Set)

		then:
		coverages.size() == 5

		// checking coverage on requirement version -51.
		// simple direct coverage
		def coverage51 = coverages.get(0)
		coverage51.requirementVersionId == -51L
		coverage51.directlyVerified
		!coverage51.unDirectlyVerified
		coverage51.verifyingCalledTestCaseIds.size() == 0
		coverage51.coverageStepInfos.size() == 0

		// checking coverage on requirement version -41.
		// this reqVersion is directly covered by TC -41 so it should be an indirect call
		def coverage41 = coverages.get(1)
		coverage41.requirementVersionId == -41L
		!coverage41.directlyVerified
		coverage41.unDirectlyVerified
		coverage41.verifyingCalledTestCaseIds == [-41L] as Set
		coverage41.coverageStepInfos.size() == 0

		// checking coverage on requirement version -31.
		// this reqVersion is covered to TC -21 so it's should be an indirect coverage through call chain.
		// this reqVersion is covered through step -5 of tc -31 so it should also be a direct step coverage
		def coverage31 = coverages.get(2)
		coverage31.requirementVersionId == -31L
		coverage31.directlyVerified
		coverage31.unDirectlyVerified
		coverage31.verifyingCalledTestCaseIds == [-21L] as Set
		coverage31.coverageStepInfos.size() == 1
		coverage31.coverageStepInfos.get(0).id == -5L
		coverage31.coverageStepInfos.get(0).index == 1

		// checking coverage on requirement version -21.
		// this reqVersion is covered to TC -21 so it's should be an indirect coverage through call chain.
		def coverage21 = coverages.get(3)
		coverage21.requirementVersionId == -21L
		!coverage21.directlyVerified
		coverage21.unDirectlyVerified
		coverage21.verifyingCalledTestCaseIds == [-21L] as Set
		// empty even if TC -21 cover reqVersion -21 by it's step
		coverage21.coverageStepInfos.size() == 0

		// checking coverage on requirement version -11
		// simple direct coverage and coverage through call chain
		def coverage11 = coverages.get(4)
		coverage11.requirementVersionId == -11L
		coverage11.directlyVerified
		coverage11.unDirectlyVerified
		coverage11.verifyingCalledTestCaseIds == [-21L] as Set
		coverage11.coverageStepInfos.size() == 0
	}

}
