/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.project

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.api.security.acls.Permissions
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.service.internal.dto.UserDto
import org.squashtest.tm.service.user.UserAccountService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class CustomProjectModificationServiceIT extends DbunitServiceSpecification {

	@Inject
	CustomProjectModificationService service

    UserAccountService userAccountService = Mock()


	@DataSet("CustomProjectModificationService.sandbox.xml")
	def "should find projects models"() {
		given:
		UserDto user = new UserDto("robert", -2L, [-100L, -300L], false)

		when:
		def jsonProjects = service.findAllProjects([-14L, -15L, -16L, -19L, -21L], user)

		then:
		jsonProjects.size() == 4
		jsonProjects.collect { it.name }.sort() == ["Projet 1", "Projet 2", "Projet 5", "Test Project-1"]

		def jsonProject15 = jsonProjects[2]
		jsonProject15.getId() == -15L
		jsonProject15.getName() == "Projet 1"
		jsonProject15.getRequirementCategories().id == -1L
		jsonProject15.getTestCaseNatures().id == -2L
		jsonProject15.getTestCaseTypes().id == -3L

		def customFieldBindings = jsonProject15.getCustomFieldBindings()
		customFieldBindings.size() == 14
		def customFieldBindingModels = customFieldBindings.get("CAMPAIGN")
		customFieldBindingModels.size() == 3
		customFieldBindingModels.collect { it.id }.sort() == [-3L, -2L, -1L]
		customFieldBindingModels.collect { it.customField.id }.sort() == [-3L, -2L, -1L]
		customFieldBindingModels.collect { it.customField.name }.sort() == ["Liste", "Liste 2", "Lot"]

		def jsonMilestones = jsonProject15.getMilestones()
		jsonMilestones.size() == 2
		jsonMilestones.collect { it.label }.sort() == ["Jalon 1", "Jalon 2"]
	}

	@Unroll
	@DataSet("CustomProjectModificationServiceIT.hasProjectData.xml")
	def "should check if a project has data"() {

		when:
		def hasData = service.hasProjectData(projectId)

		then:
		hasData == result

		where:

		projectId | result
		-101L     | false
		-102L     | true
	}

	@DataSet("CustomProjectModificationServiceIT.hasProjectData.xml")
	def "should return all requirement version names by given requirement ids ordered by names"() {
		when:
		def result = service.findProjectNamesByIds([-103L, -102L, -101L])
		then:
		result.size() == 3
		result == ["Project-101", "Project-102", "Project-103"]
	}

    @DataSet("CustomProjectModificationServiceIT.hasProjectData.xml")
    def "should get all the project Ids that contain test cases eligible for dataset copy"() {
        given:
        UserDto user = Mock()
        user.isAdmin() >> false
        user.getPartyIds() >> [-5L]
        userAccountService.findCurrentUserDto() >> user

        when:
        def projectIds = service.findAllProjectIdsByEligibleTCPermission(Permissions.WRITE.mask)

        then:
        projectIds.size() == 2
        projectIds == [-103, -101]

    }

    @DataSet("ProjectModificationServiceIT.xml")
    def "should change bugtrackerProjectName" () {
        when:
        service.updateBugTrackerProjectNames(-1L, ["this", "that"])

        then:
        em.flush()
        em.clear()
        Project project = findEntity(Project.class, -1L)
        project.getBugtrackerProjects().collect { it.getBugtrackerProjectName() } as Set == ["this", "that"] as Set
    }

    @DataSet("ProjectModificationServiceIT.xml")
    def "should find project id by requirement library id" () {
        when:
        def projectId = service.findByRequirementLibraryId(-2L)

        then:
        projectId == -1L
    }
}
