/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.sprint


import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.config.EnabledAclSpecConfig
import org.squashtest.tm.domain.campaign.SprintReqVersionValidationStatus
import org.squashtest.tm.domain.campaign.SprintStatus
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
@ContextHierarchy([
    @ContextConfiguration(classes = [EnabledAclSpecConfig], name = "aclcontext", inheritLocations = false),
])
class SprintDisplayServiceIT extends DbunitServiceSpecification {

    @Inject
    private SprintDisplayService sprintDisplayService

    def "should return sprint views by given ids"() {
        given:
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

        when:
        def dto = sprintDisplayService.getSprintView(-1L)

        then:
        dto.name == "a sprint"
        dto.reference == "test"
        dto.attachmentListId == -123
        dto.createdBy == "DBU"
        dto.createdOn == dateFormat.parse("2010-02-01")
        dto.lastModifiedBy == "DBU"
        dto.lastModifiedOn == dateFormat.parse("2024-01-17")
        dto.startDate == dateFormat.parse("2024-01-01")
        dto.endDate == dateFormat.parse("2024-01-12")
        dto.description == "Quelle incroyable description"
        dto.status == SprintStatus.UPCOMING
        dto.nbIssues == 2
    }

    def "should find sprintReqVersionDtos by given sprint id"() {
        given:
        def sprintId = -1L

        when:
        def sprintReqVersions = sprintDisplayService.findSprintReqVersionDtosBySprintId(sprintId).sort { it.id }

        then:
        sprintReqVersions.size() == 3

        sprintReqVersions[idx].id == id
        sprintReqVersions[idx].projectName == projectName
        sprintReqVersions[idx].projectId == projectId
        sprintReqVersions[idx].reference == reference
        sprintReqVersions[idx].name == name
        sprintReqVersions[idx].criticality == criticality
        sprintReqVersions[idx].categoryId == categoryId
        sprintReqVersions[idx].status == status
        sprintReqVersions[idx].description == description
        sprintReqVersions[idx].versionId == versionId
        sprintReqVersions[idx].requirementId == requirementId
        sprintReqVersions[idx].sprintId == rsvSprintId
        sprintReqVersions[idx].nbTests == nbTests
        sprintReqVersions[idx].validationStatus == SprintReqVersionValidationStatus.valueOf(validationStatus)
        sprintReqVersions[idx].remoteReqStatusAndState == remoteReqStatusAndState

        where:
        idx | id | projectName     | projectId | reference           | name                    | criticality | categoryId | status             | remoteReqStatusAndState | description    | versionId | requirementId | rsvSprintId | nbTests | validationStatus
        0   | -3 | "first project" | -1        | "another reference" | "a synchro requirement" | "MINOR"     | 3          | "WORK_IN_PROGRESS" | "custom_status, opened" | ""             | -3        | -3            | -1          | 0       | "TO_BE_CORRECTED"
        1   | -2 | "first project" | -1        | "another reference" | "a second requirement"  | "MINOR"     | 3          | "WORK_IN_PROGRESS" | null                    | "Persévérance" | -2        | -2            | -1          | 1       | "TO_BE_CORRECTED"
        2   | -1 | "first project" | -1        | "a reference"       | "a first requirement"   | "MINOR"     | 4          | "WORK_IN_PROGRESS" | null                    | "Éphémère"     | -1        | -1            | -1          | 1       | "IN_PROGRESS"
    }

    @DataSet("SprintDisplayServiceIT.should get sprintReqVersion ordered by sprintReqVersionIds.xml")
    def "should get sprintReqVersion ordered by sprintReqVersionIds"() {
        when:
        def sprintReqVersionDtos = sprintDisplayService.findSprintReqVersionDtosBySprintId(-8L)

        then:
        sprintReqVersionDtos.get(index).name == versionName

        where:
        index | versionName
        0     | "First position"
        1     | "Second position"
        2     | "Third position"
    }

    def "should find specific information for denormalization of sprintReqVersions"() {
        given:
        def sprintId = -1L

        when:
        def sprintReqVersions = sprintDisplayService.findSprintReqVersionDtosForDenormalization(sprintId).sort { it.id }

        then:
        sprintReqVersions[idx].id == id
        sprintReqVersions[idx].reference == reference
        sprintReqVersions[idx].name == name
        sprintReqVersions[idx].criticality == criticality
        sprintReqVersions[idx].categoryLabel == categoryLabel
        sprintReqVersions[idx].status == status
        sprintReqVersions[idx].description == description

        sprintReqVersions[idx].projectName == null
        sprintReqVersions[idx].categoryId == null
        sprintReqVersions[idx].projectId == null
        sprintReqVersions[idx].versionId == null
        sprintReqVersions[idx].requirementId == null
        sprintReqVersions[idx].sprintId == null

        where:
        idx | id | reference           | name                   | criticality | categoryLabel                       | status             | description
        1   | -2 | "another reference" | "a second requirement" | "MINOR"     | "requirement.category.CAT_USE_CASE" | "WORK_IN_PROGRESS" | "Persévérance"
        2   | -1 | "a reference"       | "a first requirement"  | "MINOR"     | "requirement.category.CAT_BUSINESS" | "WORK_IN_PROGRESS" | "Éphémère"
    }

    def "should return a sprint requirement version by its id"() {
        given:

        when:
        def sprintReqVersion = sprintDisplayService.findSprintReqVersionViewById(id)

        then:
        sprintReqVersion.id == id
        sprintReqVersion.projectId == projectId
        sprintReqVersion.projectName == projectName
        sprintReqVersion.name == name
        sprintReqVersion.reference == reference
        sprintReqVersion.criticality == criticality
        sprintReqVersion.status == status
        sprintReqVersion.versionId == versionId
        sprintReqVersion.requirementId == requirementId
        sprintReqVersion.sprintId == sprintId
        sprintReqVersion.description == description
        sprintReqVersion.nbIssues == nbIssues

        where:
        id | projectName     | projectId | reference           | name                   | criticality | status             | versionId | requirementId | sprintId | description    | nbIssues
        -2 | "first project" | -1        | "another reference" | "a second requirement" | "MINOR"     | "WORK_IN_PROGRESS" | -2        | -2            | -1       | "Persévérance" | 2
        -1 | "first project" | -1        | "a reference"       | "a first requirement"  | "MINOR"     | "WORK_IN_PROGRESS" | -1        | -1            | -1       | "Éphémère"     | 0
    }

    def "should return test cases that cover a sprint requirement version and that have not been added to its execution plan yet"() {
        given:

        when:
        def testCases = sprintDisplayService.findAvailableTestPlanItems(-1)

        then:
        testCases.size() == 1
        testCases[0].testCaseId == -2
        testCases[0].testCaseName == "TC2"
    }
}
