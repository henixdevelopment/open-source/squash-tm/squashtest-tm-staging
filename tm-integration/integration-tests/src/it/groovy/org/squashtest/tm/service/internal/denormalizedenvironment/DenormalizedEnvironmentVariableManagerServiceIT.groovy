/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.denormalizedenvironment


import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable
import org.unitils.dbunit.annotation.DataSet
import org.springframework.transaction.annotation.Transactional
import spock.unitils.UnitilsSupport
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.service.internal.repository.DenormalizedEnvironmentVariableDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType
import org.squashtest.tm.domain.environmentvariable.DenormalizedEnvironmentVariable
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentVariableManagerService
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao
import javax.inject.Inject

@Transactional
@UnitilsSupport
@DataSet("DenormalizedEnvironmentManagerService.xml")
class DenormalizedEnvironmentVariableManagerServiceIT extends DbunitServiceSpecification {

	@Inject
	DenormalizedEnvironmentVariableManagerService denormalizedEnvironmentVariableManagerService

	@Inject
	DenormalizedEnvironmentVariableDao denormalizedEnvironmentVariableDao

	@Inject
	EnvironmentVariableDao environmentVariableDao

	@Inject
	AutomatedExecutionExtenderDao automatedExecutionExtenderDao

	def "should create denormalized environment variable for automated execution"() {

		given:
		Long executionExtenderId = -1L

		Map<String, String> environmentVariablesMap = new HashMap<>()
		environmentVariablesMap.put("Simple environment variable", "my test value")
		environmentVariablesMap.put("List environment variable", "OPT2")

		AutomatedExecutionExtender executionExtender = automatedExecutionExtenderDao.findById(executionExtenderId)

		when:
		denormalizedEnvironmentVariableManagerService.createAllDenormalizedEnvironmentVariablesForAutomatedExecution(environmentVariablesMap, executionExtender)

		then:
		List<DenormalizedEnvironmentVariable> result = denormalizedEnvironmentVariableDao.findAllByHolderIdAndHolderType(-1L, DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER)
		result.size() == 2
	}

	def "should delete all denormalized environment variables when deleting an execution"() {
		given:
		Long executionExtenderId = -3L
		DenormalizedEnvironmentHolderType holderType = DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER
		AutomatedExecutionExtender automatedExecutionExtender = automatedExecutionExtenderDao.findById(executionExtenderId)
		List<DenormalizedEnvironmentVariable> devs = denormalizedEnvironmentVariableDao.findAllByHolderIdAndHolderType(executionExtenderId, holderType)
		List<Long> ids = devs.collect {it.id}

		when:
		denormalizedEnvironmentVariableManagerService.removeDenormalizedEnvironmentVariablesOnExecutionDelete(automatedExecutionExtender)

		then:
		List<DenormalizedEnvironmentVariable> variables = denormalizedEnvironmentVariableDao.findAllByIds(ids)
		variables.isEmpty()

	}

	def "should set ev id to null when handle environment variable deletion"() {
		given:
		EnvironmentVariable environmentVariable = environmentVariableDao.getReferenceById(-1L)
		List<DenormalizedEnvironmentVariable> devs = denormalizedEnvironmentVariableDao.findAllByEnvironmentVariable(environmentVariable)
		List<Long> ids = devs.collect {it.id}

		when:
		denormalizedEnvironmentVariableManagerService.handleEnvironmentVariableDeletion(environmentVariable)

		then:
		List<DenormalizedEnvironmentVariable> variables = denormalizedEnvironmentVariableDao.findAllByIds(ids)
		variables.each {variable -> variable.environmentVariable == null}
	}

}
