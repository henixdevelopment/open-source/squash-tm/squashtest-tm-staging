/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.execution

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport
import javax.inject.Inject

import org.squashtest.tm.service.display.execution.AutomatedExecutionDisplayService
import org.squashtest.tm.service.internal.display.execution.AutomatedExecutionDisplayServiceImpl
import org.squashtest.tm.service.internal.repository.display.AutomatedExecutionDisplayDao
import org.squashtest.tm.service.plugin.PluginFinderService

@Transactional
@DataSet
@UnitilsSupport
class AutomatedExecutionDisplayServiceIT extends DbunitServiceSpecification {

    @Inject
    AutomatedExecutionDisplayService service

    @Inject
    AutomatedExecutionDisplayDao automatedExecutionDisplayDao

    PluginFinderService pluginFinderService = Mock()


    def setup() {
        service = new AutomatedExecutionDisplayServiceImpl(
            automatedExecutionDisplayDao,
            pluginFinderService)
    }

    def "should send empty list of failure details if extender has no failure details"() {

        given:
            pluginFinderService.isPremiumPluginInstalled() >> true

        when:
            def response = service.findFailureDetailList(-4L)

        then:
            response.size() == 0
    }

    def "should retrieve automated executions failure detail list"() {

        given:
            pluginFinderService.isPremiumPluginInstalled() >> true

        when:
            def response = service.findFailureDetailList(-3L)

        then:
            response.size() == 2
            response*.message.containsAll([
                "Test has failed, 2 != 3",
                "Test has failed, 'aaa' != 'bbb'"
            ])
    }

    def "should not retrieve automated executions failure detail list if community"() {
        given:
            pluginFinderService.isPremiumPluginInstalled() >> false

        when:
            def response = service.findFailureDetailList(-3L)

        then:
            response.size() == 0
    }
}
