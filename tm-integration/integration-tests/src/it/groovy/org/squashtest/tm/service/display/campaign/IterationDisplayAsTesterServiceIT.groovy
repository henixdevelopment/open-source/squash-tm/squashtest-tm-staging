/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.campaign


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.StubPermissionEvaluationService
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.toCamelCase

@UnitilsSupport
@Transactional
@DataSet
class IterationDisplayAsTesterServiceIT extends DbunitServiceSpecification {

	@Inject
	private IterationDisplayService iterationDisplayService
	@Inject
	private TestSuiteDisplayService testSuiteDisplayService
    @Inject
    private StubPermissionEvaluationService stubPermissionEvaluationService

	def setup() {
		UserContextHelper.setUsername("JP01")
        stubPermissionEvaluationService.emptyPermissionsToRefuse()
	}

    def "should fetch a test plan without read unassigned permission"() {
        given:
        stubPermissionEvaluationService.addPermissionToRefuse("READ_UNASSIGNED", Iteration.class.getName(), -10012L)

        def gridRequest = new GridRequest()
        gridRequest.size = 25

        when:
        def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

        then:
        gridResponse.count == 2
        gridResponse.dataRows.collect{it.data[toCamelCase(ASSIGNEE_LOGIN)]}.containsAll([null, "JP01"])
        gridResponse.dataRows.collect{it.data[toCamelCase(INFERRED_EXECUTION_MODE)]}.containsAll(["MANUAL", "EXPLORATORY"])
    }

    def "should fetch a test plan with read unassigned permission"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 25

        when:
        def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

        then:
        gridResponse.count == 12
    }
}
