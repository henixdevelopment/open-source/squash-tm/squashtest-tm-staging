/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd

import org.springframework.context.MessageSource
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.bdd.BddImplementationTechnology
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology
import org.squashtest.tm.domain.testcase.KeywordTestCase
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.domain.testcase.TestCaseImportance
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest
import org.squashtest.tm.service.testcase.bdd.KeywordTestCaseFinder
import org.squashtest.tm.service.testcase.bdd.KeywordTestCaseService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.bdd.BddImplementationTechnology.CUCUMBER_4
import static org.squashtest.tm.domain.bdd.BddImplementationTechnology.CUCUMBER_5_PLUS
import static org.squashtest.tm.domain.bdd.BddImplementationTechnology.ROBOT

@UnitilsSupport
@Transactional
@DataSet
class KeywordTestCaseServiceIT extends DbunitServiceSpecification {

	@Inject
	KeywordTestCaseFinder keywordTestCaseFinder

	@Inject
	KeywordTestCaseService keywordTestCaseService

	def messageSource = Mock(MessageSource)

	def setup(){
		keywordTestCaseService.messageSource = messageSource
	}

	def setupCucumber4Project(KeywordTestCase tc) {
		tc.getProject().bddImplementationTechnology = BddImplementationTechnology.CUCUMBER_4
		tc.automatedTestTechnology = new AutomatedTestTechnology()
		tc.automatedTestTechnology.name = "Cucumber 4"
	}

	def setupRobotFrameworkData(KeywordTestCase tc) {
		setupRobotProject(tc)
		setupAutomationPriorityAndImportance(tc)
		mockRobotMessageSourceCalls()
	}

	def setupRobotProject(KeywordTestCase tc) {
		tc.getProject().bddImplementationTechnology = BddImplementationTechnology.ROBOT
	}

	def setupAutomationPriorityAndImportance(KeywordTestCase tc) {
		tc.automationRequest = Mock(AutomationRequest)
		tc.automationRequest.getAutomationPriority() >> 42

		tc.importance = TestCaseImportance.LOW
	}

	def mockRobotMessageSourceCalls() {
		messageSource.getMessage("testcase.bdd.script.label.test-case-importance", null, _) >> "Test case importance"
		messageSource.getMessage("testcase.bdd.script.label.automation-priority", null, _) >> "Automation priority"
		messageSource.getMessage("test-case.importance.LOW", null, _) >> "Low"
		messageSource.getMessage("label.id", null, _) >> "ID"
		messageSource.getMessage("test-case.reference.label", null, _) >> "Reference"
	}

	/* ----- Cucumber Scripts ----- */
	def "Should generate a Gherkin script without test steps from a KeywordTestCase"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-6L)
			setupCucumber4Project(keywordTestCase)
		when:
			messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
			"# language: en\n" +
			"Feature: empty test"
	}

	def "Should generate a Gherkin script from a KeywordTestCase"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-4L)
			setupCucumber4Project(keywordTestCase)
		when:
			messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
			messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
			messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		    messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
			messageSource.getMessage("testcase.bdd.script.label.scenario",null, _ as Locale) >> "Scenario: "
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""# language: en
Feature: Disconnection test

	Scenario: Disconnection test
		Given I am connected
		When I sign oùt
		Then Je suis déconnecté"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-param-value-as-free-text.xml")
	def "Should generate a Gherkin script with test steps containing parameter value as free text from a KeywordTestCase"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario",null, _ as Locale) >> "Scenario: "
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario: Daily test
		Given Today is Monday
		When It is "8 AM"
		Then I am working"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-param-value-as-number.xml")
	def "Should generate a Gherkin script with test steps containing parameter value as number from a KeywordTestCase"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario",null, _ as Locale) >> "Scenario: "
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario: Daily test
		Given Today is Monday
		When It is "8.5"
		Then I am working"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-TC-param-value-no-dataset.xml")
	def "Should generate a Gherkin script with test steps containing parameter associated with a TC param as value from a KeywordTestCase but no dataset"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario",null, _ as Locale) >> "Scenario: "
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario: Daily test
		Given Today is Monday
		When It is &lt;time&gt;
		Then I am working"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-no-TC-param-value-but-dataset.xml")
	def "Should generate a Gherkin script with test steps from a KeywordTestCase with dataset but no TC param"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario",null, _ as Locale) >> "Scenario: "
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario: Daily test
		Given Today is Monday
		When It is "time"
		Then I am working"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-1-TC-param-value-1-dataset.xml")
	def "Should generate a Gherkin script with test steps containing parameter associated with 1 TC param value and 1 dataset"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario-outline",null, _ as Locale) >> "Scenario Outline: "
		messageSource.getMessage("testcase.bdd.script.label.examples",null, _ as Locale) >> "Examples:"
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario Outline: Daily test
		Given Today is Monday
		When It is &lt;time&gt;
		Then I am working

		@dataset1
		Examples:
		| time |
		| "12 AM" |"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-1-TC-param-value-1-dataset.xml")
	def "Should generate a Gherkin script with test steps containing parameter associated with 1 TC param value and 1 dataset without escaping arrow symbols"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("label.id",null, _ as Locale) >> "ID"
		messageSource.getMessage("testcase.reference.label",null, _ as Locale) >> "Reference"
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario-outline",null, _ as Locale) >> "Scenario Outline: "
		messageSource.getMessage("testcase.bdd.script.label.examples",null, _ as Locale) >> "Examples:"
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), false)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario Outline: Daily test
		Given Today is Monday
		When It is <time>
		Then I am working

		@dataset1
		Examples:
		| time |
		| "12 AM" |"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-2-TC-param-value-1-dataset.xml")
	def "Should generate a Gherkin script with test steps containing parameter associated with 2 TC param value and 1 dataset"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario-outline",null, _ as Locale) >> "Scenario Outline: "
		messageSource.getMessage("testcase.bdd.script.label.examples",null, _ as Locale) >> "Examples:"
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario Outline: Daily test
		Given Today is Monday
		When It is &lt;time&gt; in &lt;place&gt;
		Then I am working

		@dataset1
		Examples:
		| place | time |
		| "Nice" | 12 |"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-2-TC-param-value-1-dataset-name-with-spaces.xml")
	def "Should generate a Gherkin script with test steps containing parameter associated with 2 TC param value and 1 dataset whose name contains spaces"() {
		given:
		KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
		setupCucumber4Project(keywordTestCase)
		when:
		messageSource.getMessage("testcase.bdd.keyword.name.given",null, _ as Locale) >> "Given"
		messageSource.getMessage("testcase.bdd.keyword.name.when",null, _ as Locale) >> "When"
		messageSource.getMessage("testcase.bdd.keyword.name.then",null, _ as Locale) >> "Then"
		messageSource.getMessage("testcase.bdd.script.label.feature",null, _ as Locale) >> "Feature: "
		messageSource.getMessage("testcase.bdd.script.label.scenario-outline",null, _ as Locale) >> "Scenario Outline: "
		messageSource.getMessage("testcase.bdd.script.label.examples",null, _ as Locale) >> "Examples:"
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
			"""# language: en
Feature: Daily test

	Scenario Outline: Daily test
		Given Today is Monday
		When It is &lt;time&gt; in &lt;place&gt;
		Then I am working

		@dataset_1
		Examples:
		| place | time |
		| "Nice" | 12 |"""
	}

	/* ----- Robot Framework ----- */
	def "Should generate a Robot script without test steps from a KeywordTestCase"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-6L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    empty test
Metadata         ID                           -6
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
empty test
    [Documentation]    empty test


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-6_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-6_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =       Get Variable Value    \${TEST_SETUP}
    \${TEST_-6_SETUP_VALUE} =    Get Variable Value    \${TEST_-6_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-6_SETUP_VALUE is not None
        Run Keyword    \${TEST_-6_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-6_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-6_TEARDOWN}.

    \${TEST_-6_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-6_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =       Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-6_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-6_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}

	def "Should generate a Robot script from a KeywordTestCase"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-4L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Disconnection test
Metadata         ID                           -4
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Disconnection test
    [Documentation]    Disconnection test

    Given I am connected
    When I sign oùt
    Then Je suis déconnecté


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-4_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-4_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =       Get Variable Value    \${TEST_SETUP}
    \${TEST_-4_SETUP_VALUE} =    Get Variable Value    \${TEST_-4_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-4_SETUP_VALUE is not None
        Run Keyword    \${TEST_-4_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-4_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-4_TEARDOWN}.

    \${TEST_-4_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-4_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =       Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-4_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-4_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-param-value-as-free-text.xml")
	def "Should generate a Robot script with test steps containing parameter value as free text from a KeywordTestCase"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    Given Today is Monday
    When It is "8 AM"
    Then I am working


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-param-value-as-number.xml")
	def "Should generate a Robot script with test steps containing parameter value as number from a KeywordTestCase"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    Given Today is Monday
    When It is "8.5"
    Then I am working


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-TC-param-value-no-dataset.xml")
	def "Should generate a Robot script with test steps containing parameter associated with a TC param as value from a KeywordTestCase but no dataset"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]"
    Then I am working


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                \${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=\${city}    country=\${country}    currency=\${currency}

    \${time} =    Get Test Param    DS_time

    &{dataset} =    Create Dictionary    time=\${time}

    RETURN    &{dataset}
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-no-TC-param-value-but-dataset.xml")
	def "Should generate a Robot script with test steps from a KeywordTestCase with dataset but no TC param"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    Given Today is Monday
    When It is "time"
    Then I am working


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-1-TC-param-value-1-dataset.xml")
	def "Should generate a Robot script with test steps containing parameter associated with 1 TC param value and 1 dataset"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]"
    Then I am working


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                \${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=\${city}    country=\${country}    currency=\${currency}

    \${time} =    Get Test Param    DS_time

    &{dataset} =    Create Dictionary    time=\${time}

    RETURN    &{dataset}
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-1-TC-param-value-1-dataset.xml")
	def "Should generate a Robot script with test steps containing parameter associated with 1 TC param value and 1 dataset without escaping arrow symbols"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
		def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), false)
		then:
			res	==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]"
    Then I am working


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                \${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=\${city}    country=\${country}    currency=\${currency}

    \${time} =    Get Test Param    DS_time

    &{dataset} =    Create Dictionary    time=\${time}

    RETURN    &{dataset}
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-2-TC-param-value-1-dataset.xml")
	def "Should generate a Robot script with test steps containing parameter associated with 2 TC param value and 1 dataset"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]" in "\${dataset}[place]"
    Then I am working


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                \${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=\${city}    country=\${country}    currency=\${currency}

    \${place} =    Get Test Param    DS_place
    \${time} =     Get Test Param    DS_time

    &{dataset} =    Create Dictionary    place=\${place}    time=\${time}

    RETURN    &{dataset}
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-step-containing-2-TC-param-value-used-twice.xml")
	def "Should generate a Robot script with test steps containing parameter associated with 1 TC param value used twice and 1 dataset"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-14L)
			setupRobotFrameworkData(keywordTestCase)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Daily test
Metadata         ID                           -14
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Daily test
    [Documentation]    Daily test

    &{dataset} =    Retrieve Dataset

    Given Today is Monday
    When It is "\${dataset}[time]" in "\${dataset}[place]"
    Then I am working at "\${dataset}[time]" in "\${dataset}[place]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-14_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =        Get Variable Value    \${TEST_SETUP}
    \${TEST_-14_SETUP_VALUE} =    Get Variable Value    \${TEST_-14_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-14_SETUP_VALUE is not None
        Run Keyword    \${TEST_-14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-14_TEARDOWN}.

    \${TEST_-14_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-14_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =        Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-14_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-14_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                \${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=\${city}    country=\${country}    currency=\${currency}

    \${place} =    Get Test Param    DS_place
    \${time} =     Get Test Param    DS_time

    &{dataset} =    Create Dictionary    place=\${place}    time=\${time}

    RETURN    &{dataset}
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-datatable-docstring-comment.xml")
	def "Should generate a Robot script with a test step containing a datatable"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-1L)
			setupRobotFrameworkData(keywordTestCase)
			String datatable = "| Henry | Dupont | henry.dupont@mail.com |\n" +
				"| Louis | Dupond | louis.dupond@mail.com |\n" +
				"| Charles | Martin | charles.martin@mail.com |"
			((KeywordTestStep) keywordTestCase.getSteps().get(0)).setDatatable(datatable)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
		res ==
"""*** Settings ***
Documentation    User table test
Metadata         ID                           -1
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
User table test
    [Documentation]    User table test

    &{datatables} =    Retrieve Datatables

    Given following users are listed "\${datatables}[datatable_1]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-1_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-1_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =       Get Variable Value    \${TEST_SETUP}
    \${TEST_-1_SETUP_VALUE} =    Get Variable Value    \${TEST_-1_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-1_SETUP_VALUE is not None
        Run Keyword    \${TEST_-1_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-1_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-1_TEARDOWN}.

    \${TEST_-1_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-1_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =       Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-1_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-1_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}    datatable_2=\${datatable_2}

    @{row_1_1} =    Create List    Henry      Dupont    henry.dupont\\@mail.com
    @{row_1_2} =    Create List    Louis      Dupond    louis.dupond\\@mail.com
    @{row_1_3} =    Create List    Charles    Martin    charles.martin\\@mail.com
    @{datatable_1} =    Create List    \${row_1_1}    \${row_1_2}    \${row_1_3}

    &{datatables} =    Create Dictionary    datatable_1=\${datatable_1}

    RETURN    &{datatables}
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-datatable-docstring-comment.xml")
	def "Should generate a Robot script with a test step containing a docstring"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-2L)
			setupRobotFrameworkData(keywordTestCase)
			String docString = "\tDear Jack,\n" +
				"I have arrived in London this morning. Everything went well!\n" +
				"Looking forward to seeing you on Friday.\n" +
				"\n\tYour friend, John."
			((KeywordTestStep) keywordTestCase.getSteps().get(0)).setDocstring(docString)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
"""*** Settings ***
Documentation    Letter test
Metadata         ID                           -2
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Letter test
    [Documentation]    Letter test

    &{docstrings} =    Retrieve Docstrings

    Given following letter is displayed "\${docstrings}[docstring_1]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-2_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-2_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =       Get Variable Value    \${TEST_SETUP}
    \${TEST_-2_SETUP_VALUE} =    Get Variable Value    \${TEST_-2_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-2_SETUP_VALUE is not None
        Run Keyword    \${TEST_-2_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-2_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-2_TEARDOWN}.

    \${TEST_-2_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-2_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =       Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-2_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-2_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END

Retrieve Docstrings
    [Documentation]    Retrieves Squash TM's docstrings and stores them in a dictionary.
    ...
    ...                For instance, two docstrings have been defined in Squash TM,
    ...                the first one containing the string
    ...                "I am the
    ...                FIRST    docstring",
    ...                the second one containing the string "I am the second docstring"
    ...
    ...                First, this keyword retrieves values and converts them to an inline string :
    ...                \${docstring_1} =    Set Variable    I am the\\nFIRST\\tdocstring"
    ...
    ...                Then, this keyword stores the docstrings into the &{docstrings} dictionary
    ...                with each docstring name as key, and each docstring value as value :
    ...                \${docstrings} =    Create Dictionary    docstring_1=\${docstring_1}    docstring_2=\${docstring_2}

    \${docstring_1} =    Set Variable    \\tDear Jack,\\nI have arrived in London this morning. Everything went well!\\nLooking forward to seeing you on Friday.\\n\\n\\tYour friend, John.

    &{docstrings} =    Create Dictionary    docstring_1=\${docstring_1}

    RETURN    &{docstrings}
"""
	}

	@DataSet("KeywordTestCaseServiceIT.test-case-with-datatable-docstring-comment.xml")
	def "Should generate a Robot script with a test step containing a comment"() {
		given:
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-3L)
			setupRobotFrameworkData(keywordTestCase)
			String comment = "the action can be anything here\nANYTHING !"
			((KeywordTestStep) keywordTestCase.getSteps().get(0)).setComment(comment)
		when:
			def res = keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), true)
		then:
			res ==
			"""*** Settings ***
Documentation    Comment test
Metadata         ID                           -3
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Comment test
    [Documentation]    Comment test

    Given I do something
    # the action can be anything here
    # ANYTHING !


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the \${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the \${TEST_-3_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, \${TEST_-3_SETUP} will be run after \${TEST_SETUP}.

    \${TEST_SETUP_VALUE} =       Get Variable Value    \${TEST_SETUP}
    \${TEST_-3_SETUP_VALUE} =    Get Variable Value    \${TEST_-3_SETUP}
    IF    \$TEST_SETUP_VALUE is not None
        Run Keyword    \${TEST_SETUP}
    END
    IF    \$TEST_-3_SETUP_VALUE is not None
        Run Keyword    \${TEST_-3_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the \${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the \${TEST_-3_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, \${TEST_TEARDOWN} will be run after \${TEST_-3_TEARDOWN}.

    \${TEST_-3_TEARDOWN_VALUE} =    Get Variable Value    \${TEST_-3_TEARDOWN}
    \${TEST_TEARDOWN_VALUE} =       Get Variable Value    \${TEST_TEARDOWN}
    IF    \$TEST_-3_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_-3_TEARDOWN}
    END
    IF    \$TEST_TEARDOWN_VALUE is not None
        Run Keyword    \${TEST_TEARDOWN}
    END
"""
	}


	/* ----- File System Methods ----- */

	@Unroll("Should create a file name for #bddTechnology")
	def "Should create a File name for a Keyword Test case"() {
		given: "a keyword test case"
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-4L)
			keywordTestCase.project.bddImplementationTechnology = bddTechnology
		when: "I create the file name"
			def result = keywordTestCaseService.createFileName(keywordTestCase)
		then: "the result is as expected"
			result == expectedResult
		where:
			bddTechnology	| expectedResult
			CUCUMBER_4 		| "-4_Disconnection_test.feature"
			CUCUMBER_5_PLUS | "-4_Disconnection_test.feature"
			ROBOT 			| "-4_Disconnection_test.robot"
	}

	@Unroll("Should create a backup file name for #bddTechnology")
	def "Should create a backup File name for a Keyword Test case"(){
		given: "a keyword test case"
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-4L)
			keywordTestCase.project.bddImplementationTechnology = bddTechnology
		when: "I create the backup file name"
			def result = keywordTestCaseService.createBackupFileName(keywordTestCase)
		then: "the result is as expected"
			result == expectedResult
		where:
			bddTechnology	| expectedResult
			CUCUMBER_4		| "-4.feature"
			CUCUMBER_5_PLUS | "-4.feature"
			ROBOT			| "-4.robot"
	}

	@Unroll("Should build name pattern for #bddTechnology")
	def "Should build Pattern for a Keyword Test case"(){
		given: "a keyword test case"
			KeywordTestCase keywordTestCase = keywordTestCaseFinder.findById(-4L)
			keywordTestCase.project.bddImplementationTechnology = bddTechnology
		when: "I build the file name pattern"
			def result = keywordTestCaseService.buildFilenameMatchPattern(keywordTestCase)
		then: "the result is as expected"
			result == expectedResult
		where:
			bddTechnology	| expectedResult
			CUCUMBER_4		| "-4(_.*)?\\.feature"
			CUCUMBER_5_PLUS | "-4(_.*)?\\.feature"
			ROBOT			| "-4(_.*)?\\.robot"
	}
}
