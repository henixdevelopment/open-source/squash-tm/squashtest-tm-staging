/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.hibernate.query.Query
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.core.foundation.lang.PathUtils
import org.squashtest.tm.domain.NodeReference
import org.squashtest.tm.domain.NodeType
import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.CampaignFolder
import org.squashtest.tm.domain.campaign.CampaignLibraryNode
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.Sprint
import org.squashtest.tm.domain.campaign.SprintGroup
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.domain.customfield.RawValue
import org.squashtest.tm.domain.library.NewFolderDto
import org.squashtest.tm.exception.DuplicateNameException
import org.squashtest.tm.exception.RequiredFieldException
import org.squashtest.tm.exception.campaign.IllegalSprintGroupHierarchyException
import org.squashtest.tm.exception.library.CannotMoveInHimselfException
import org.squashtest.tm.exception.sync.PathAlreadyInUseException
import org.squashtest.tm.exception.sync.PathContainsASprintGroupException
import org.squashtest.tm.exception.sync.PathValidationDomainException
import org.squashtest.tm.service.clipboard.model.ClipboardPayload
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.repository.CampaignFolderDao
import org.squashtest.tm.service.internal.repository.hibernate.HibernateCampaignLibraryNodeDao
import org.squashtest.tm.service.project.GenericProjectManagerService
import org.unitils.dbunit.annotation.DataSet
import org.unitils.dbunit.annotation.ExpectedDataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.customfield.BindableEntity.CAMPAIGN_FOLDER

@UnitilsSupport
@Transactional
@DataSet
class CampaignLibraryNavigationServiceIT extends DbunitServiceSpecification {


    @Inject
    private CampaignLibraryNavigationService navService

    @Inject
    private CampaignFolderDao folderDao

    @Inject
    private HibernateCampaignLibraryNodeDao campaignLibraryNodeDao

    @Inject
    GenericProjectManagerService genericProjectManager

    @Inject
    CustomFieldValueFinderService customFieldValueService

    private Long libId = -1
    private Long campId = -10
    private Long folderId = -1
    private Long sprintGroupId = -11

    // Queries
    private List<Iteration> findIterationsByCampaignId(long targetCampaignId) {
        return em.createNamedQuery("iterationDao.findAllByCampaignId")
            .setParameter("campaignId", targetCampaignId)
            .getResultList()
    }

    private List<Campaign> findCampaignInsideFolder(Long targetCampaignFolderId, Long[] existingCampaignIds) {
        return em.createQuery("select c from CampaignFolder cf join cf.content ct, Campaign c where ct = c and cf.id = :campaignFolderId and c.id not in :existingCampaignIds", Campaign)
            .setParameter("campaignFolderId", targetCampaignFolderId)
            .setParameter("existingCampaignIds", existingCampaignIds.toList())
            .getResultList()
    }

    private findCufValuesForEntity(BindableEntity tctype, long tcId) {
        return em.createQuery("from CustomFieldValue cv where cv.boundEntityType = :type and cv.boundEntityId = :id")
            .setParameter("id", tcId)
            .setParameter("type", tctype)
            .getResultList()
    }



    def "should not persist a nameless campaign"() {
        given:
        Campaign camp = new Campaign()
        when:
        navService.addCampaignToCampaignLibrary(libId, camp)
        session.flush()
        then:

        thrown RuntimeException
    }

    def "should add folder to library and fetch it back"() {
        given:
        def folder = new CampaignFolder(name: "folder 2")
        navService.addFolderToLibrary(libId, folder)
        when:
        def obj = navService.findFolder(folder.id)

        then:
        obj != null
        obj.id != null
        obj.name == folder.name
    }

    def "should not add a folder to library"() {
        given:
        def folder = new CampaignFolder(name: "a folder")    //same as the one in the dataset

        when:
        navService.addFolderToLibrary(libId, folder)

        then:
        thrown(DuplicateNameException)

    }

    def "should add folder to folder and fetch it back"() {
        given:
        def folder = new CampaignFolder(name: "folder 2")
        navService.addFolderToFolder(folderId, folder)


        when:
        def obj = navService.findFolder(folder.id)

        then:
        obj != null
        obj.id != null
        obj.name == folder.name
    }

    def "should add campaign to campaign folder and fetch it back"() {
        given:
        def campaign = new Campaign(name: "new campaign", description: "test campaign")

        when:
        navService.addCampaignToCampaignFolder(folderId, campaign, [:])
        def obj = findCampaign(campaign.id)
        then:
        obj != null
        obj.name == "new campaign"
        obj.description == "test campaign"
    }

    def "should not add campaign to campaign folder"() {
        given:
        def campaign = new Campaign(name: "campaign 1", description: "test campaign") //same as in the setup() clause

        when:
        navService.addCampaignToCampaignFolder(folderId, campaign, [:])

        then:
        thrown(DuplicateNameException)
    }

    def "should add campaign to campaign library and fetch it back"() {
        given:
        def campaign = new Campaign(name: "test campaign", description: "test campaign")
        when:
        navService.addCampaignToCampaignLibrary(libId, campaign, [:])
        def obj = findCampaign(campaign.id)
        then:
        obj != null
        obj.name == "test campaign"
        obj.description == "test campaign"
    }

    def "should not add campaign to campaign library"() {
        given:
        def campaign1 = new Campaign(name: "test campaign 1", description: "test campaign")
        navService.addCampaignToCampaignLibrary(libId, campaign1, [:])
        when:
        def campaign2 = new Campaign(name: "test campaign 1", description: "test campaign")
        navService.addCampaignToCampaignLibrary(libId, campaign2, [:])
        then:
        thrown(DuplicateNameException)
    }

    def "should find test campaign"() {
        given:
        true

        when:
        def obj = findCampaign(campId)

        then:
        obj != null
        obj.name == "campaign 1"
        obj.description == "the first campaign"
    }

    def "should add a sprint to library"() {
        given:
        def sprintName = "sprint 1"
        def sprintDescription = "sprint description"
        def sprint = new Sprint(name: sprintName, description: sprintDescription)

        when:
        navService.addSprintToCampaignLibrary(libId, sprint)
        def obj = findEntity(Sprint.class, sprint.id)

        then:
        obj != null
        obj.name == sprintName
        obj.description == sprintDescription
    }

    def "should add two sprints with the same name to library"() {
        given:
        def firstSprintName = "sprint 1"
        def firstSprintDescription = "sprint description"
        def firstSprint = new Sprint(name: firstSprintName, description: firstSprintDescription)

        def secondSprintName = "sprint 1"
        def secondSprintDescription = "sprint description"
        def secondSprint = new Sprint(name: secondSprintName, description: secondSprintDescription)

        when:
        navService.addSprintToCampaignLibrary(libId, firstSprint)
        navService.addSprintToCampaignLibrary(libId, secondSprint)
        def firstObj = findEntity(Sprint.class, firstSprint.id)
        def secondObj = findEntity(Sprint.class, secondSprint.id)

        then:
        firstObj != null
        firstObj.name == firstSprintName
        firstObj.description == firstSprintDescription
        secondObj != null
        secondObj.name == secondSprintName
        secondObj.description == secondSprintDescription
    }

    def "should add a sprint to folder"() {
        given:
        def sprintName = "sprint 1"
        def sprintDescription = "sprint description"
        def sprint = new Sprint(name: sprintName, description: sprintDescription)

        when:
        navService.addSprintToCampaignFolder(folderId, sprint)
        def obj = findEntity(Sprint.class, sprint.id)

        then:
        obj != null
        obj.name == sprintName
        obj.description == sprintDescription
    }

    def "should add two sprints with the same name to folder"() {
        given:
        def firstSprintName = "sprint 1"
        def firstSprintDescription = "sprint description"
        def firstSprint = new Sprint(name: firstSprintName, description: firstSprintDescription)

        def secondSprintName = "sprint 1"
        def secondSprintDescription = "sprint description"
        def secondSprint = new Sprint(name: secondSprintName, description: secondSprintDescription)

        when:
        navService.addSprintToCampaignFolder(libId, firstSprint)
        navService.addSprintToCampaignFolder(libId, secondSprint)
        def firstObj = findEntity(Sprint.class, firstSprint.id)
        def secondObj = findEntity(Sprint.class, secondSprint.id)

        then:
        firstObj != null
        firstObj.name == firstSprintName
        firstObj.description == firstSprintDescription
        secondObj != null
        secondObj.name == secondSprintName
        secondObj.description == secondSprintDescription
    }

    def "should add a sprint group to library"() {
        given:
        def sprintGroupName = "sprint group 2"
        def sprintGroupDescription = "sprint group description"
        def sprintGroup = new SprintGroup(name: sprintGroupName, description: sprintGroupDescription)

        when:
        SprintGroup createdSprintGroup = navService.addSprintGroupToCampaignLibrary(libId, sprintGroup)
        def obj = findEntity(SprintGroup.class, sprintGroup.id)

        then:
        obj != null
        createdSprintGroup == obj
        obj.name == sprintGroupName
        obj.description == sprintGroupDescription
    }

    def "should add a sprint group to folder"() {
        given:
        def sprintGroupName = "sprint group 1"
        def sprintGroupDescription = "sprint group description"
        def sprintGroup = new SprintGroup(name: sprintGroupName, description: sprintGroupDescription)

        when:
        SprintGroup createdSprintGroup = navService.addSprintGroupToCampaignFolder(folderId, sprintGroup)
        def obj = findEntity(SprintGroup.class, sprintGroup.id)

        then:
        obj != null
        createdSprintGroup == obj
        obj.name == sprintGroupName
        obj.description == sprintGroupDescription
    }

    def "should create a sprintGroup at root of library"() {
        given:
        def path = "/p1/the sprint group"

        when:
        def id = navService.createSprintGroupAndFoldersHierarchy(path)
        def obj = findEntity(SprintGroup.class, id)

        then:
        obj.getName() == "the sprint group"
    }

    def "should create two folders and a sprintGroup starting at root of library"() {
        given:
        def path = "/p1/folder 1/folder 2/sprint group"
        def paths = PathUtils.scanPath(path)

        when:
        def id = navService.createSprintGroupAndFoldersHierarchy(path)
        def nodeIds = campaignLibraryNodeDao.findNodeIdsByPath(paths)
        def folder1 = campaignLibraryNodeDao.findById(nodeIds.get(0))
        def folder2 = campaignLibraryNodeDao.findById(nodeIds.get(1))
        def sprintGroup = findEntity(SprintGroup.class, id)

        then:
        nodeIds.size() == 3
        folder1.getName() == "folder 1"
        folder2.getName() == "folder 2"
        sprintGroup != null
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should create one folder and one sprint group inside already existing folders.xml")
    def "should create one folder and one sprint group inside already existing folders"() {
        given:
        def path = "/p1/folder 1/folder 2/the new folder/sprint group"
        def paths = PathUtils.scanPath(path)

        when:
        def id = navService.createSprintGroupAndFoldersHierarchy(path)
        def nodeIds = campaignLibraryNodeDao.findNodeIdsByPath(paths)
        def newFolder = campaignLibraryNodeDao.findById(nodeIds.get(2))
        def sprintGroup = findEntity(SprintGroup.class, id)

        then:
        nodeIds.get(0) == -85
        nodeIds.get(1) == -915
        newFolder.getName() == "the new folder"
        sprintGroup != null
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should return already existing sprintGroupId.xml")
    def "should return already existing sprintGroupId"() {
        given:
        def path = "/p1/folder 1/sprint group"

        when:
        def id = navService.createSprintGroupAndFoldersHierarchy(path)

        then:
        id == -5135
    }

    def "should add a sprint to sprint group"() {
        given:
        def sprintName = "sprint 1"
        def sprintDescription = "sprint description"
        def sprint = new Sprint(name: sprintName, description: sprintDescription)

        when:
        navService.addSprintToSprintGroup(sprintGroupId, sprint)
        def obj = findEntity(Sprint.class, sprint.id)

        then:
        obj != null
        obj.name == sprintName
        obj.description == sprintDescription
    }

    def "should add two sprints with the same name to sprint group"() {
        given:
        def firstSprintName = "sprint 1"
        def firstSprintDescription = "sprint description"
        def firstSprint = new Sprint(name: firstSprintName, description: firstSprintDescription)

        def secondSprintName = "sprint 1"
        def secondSprintDescription = "sprint description"
        def secondSprint = new Sprint(name: secondSprintName, description: secondSprintDescription)

        when:
        navService.addSprintToSprintGroup(sprintGroupId, firstSprint)
        navService.addSprintToSprintGroup(sprintGroupId, secondSprint)
        def firstObj = findEntity(Sprint.class, firstSprint.id)
        def secondObj = findEntity(Sprint.class, secondSprint.id)

        then:
        firstObj != null
        firstObj.name == firstSprintName
        firstObj.description == firstSprintDescription
        secondObj != null
        secondObj.name == secondSprintName
        secondObj.description == secondSprintDescription
    }


    @DataSet("CampaignLibraryNavigationServiceIT.should copy paste iterations to campaign.xml")
    def "should copy paste iterations to campaign"() {
        given:
        Long[] iterationList = [-1L, -2L]
        Long targetCampaignId = -11L
        List<NodeReference> sourceNodeReference = iterationList.collect { new NodeReference(NodeType.ITERATION, it) }
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(iterationList))
        clipboardPayload.setSelectedNode(sourceNodeReference)

        when:
        navService.copyIterationsToCampaign(targetCampaignId, iterationList, clipboardPayload)

        then:
        def iterations = findIterationsByCampaignId(targetCampaignId)

        iterations.size() == 2
        iterations.find { it.getName() == "iter - tc1" } != null
        iterations.find { it.getName() == "iter - tc1 -2" } != null

    }

    @DataSet("CampaignLibraryNavigationServiceIT.should copy paste iterations with testSuites.xml")
    def "should copy paste iterations with testSuites"() {
        given:
        Long[] iterationList = [-10012L, -2L]
        Long targetCampaignId = -11L
        List<NodeReference> sourceNodeReference = iterationList.collect { new NodeReference(NodeType.ITERATION, it) }
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(iterationList))
        clipboardPayload.setSelectedNode(sourceNodeReference)

        when:
        navService.copyIterationsToCampaign(targetCampaignId, iterationList, clipboardPayload)

        then: "2 iterations are copied"
        def iterations = findIterationsByCampaignId(targetCampaignId)

        iterations.size() == 2
        and: "the copy 'iter-tc1' has 2 test-suites"
        iterations.find { it.getName() == "iter - tc1" } != null
        Iteration iteration1 = iterations.find { it.getName() == "iter - tc1" }
        iteration1.getTestSuites().size() == 2
        and: "the 'test-suite1' has been bound to the item-test-plan already copied with the iteration"
        iteration1.getTestSuites().find { it.getName() == "testSuite1" } != null
        TestSuite testsSuite1 = iteration1.getTestSuites().find { it.getName() == "testSuite1" }
        testsSuite1.getTestPlan().size() == 1
        iteration1.getTestPlans().size() == 1
        and: "the 'test-suite2' is found and has no test-plan-item"
        iteration1.getTestSuites().find { it.getName() == "testSuite2" } != null
        TestSuite testsSuite2 = iteration1.getTestSuites().find { it.getName() == "testSuite2" }
        testsSuite2.getTestPlan().size() == 0
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should copy paste iterations with testSuites containing CUF.xml")
    def "should copy paste iterations with testSuites containing CUF"() {
        given:
        Long iteration = -10012L
        Long targetCampaignId = -11L
        Long customFieldValueId = -42L
        List<NodeReference> sourceNodeReference = [new NodeReference(NodeType.ITERATION, iteration)]
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(iteration))
        clipboardPayload.setSelectedNode(sourceNodeReference)

        when:
        navService.copyIterationsToCampaign(targetCampaignId, [iteration] as Long[], clipboardPayload)
        def iterations = findIterationsByCampaignId(targetCampaignId)
        Iteration iteration1 = iterations.find { it.name == "iter - tc1" }
        TestSuite copiedTestSuite = iteration1.getTestSuites().get(0)
        List<CustomFieldValue> customFieldValues = customFieldValueService.findAllCustomFieldValues(copiedTestSuite)
        CustomFieldValue updatedCustomField = customFieldValues.get(0)

        then: "an iterations is copied"
        iterations.size() == 1
        and: "the copy 'iter-tc1' has 1 test-suite"
        iterations.find { it.getName() == "iter - tc1" } != null
        iteration1.getTestSuites().size() == 1
        and: "the test-suite has a CUF (plain text) with an updated value"
        updatedCustomField.getValue().equals("updated value")

    }

    @DataSet("CampaignLibraryNavigationServiceIT.should copy paste campaigns with iterations.xml")
    def "should copy paste campaigns with iterations"() {
        given:
        Long[] sourceIds = [-10L]
        Long destinationId = -1L
        Long[] existingCampaignIds = [-10L,-11L]
        List<NodeReference> sourceNodeReference = [new NodeReference(NodeType.CAMPAIGN, sourceIds[0])]
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        clipboardPayload.setSelectedNode(sourceNodeReference);

        when:
        navService.copyNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then:
        List<Campaign> campaigns = findCampaignInsideFolder(destinationId, existingCampaignIds);
        campaigns.size() == 1
        def iterations = campaigns.get(0).getIterations()
        iterations.size() == 2
        iterations.find { it.getName() == "iter - tc1" } != null
        iterations.find { it.getName() == "iter - tc1 -2" } != null
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should copy paste campaigns with testSuites.xml")
    def "should copy paste campaigns with testSuites"() {
        given:
        Long[] sourceIds = [-10L]
        Long destinationId = -1L
        Long[] existingCampaignIds = [-10L,-11L]
        List<NodeReference> sourceNodeReference = [new NodeReference(NodeType.CAMPAIGN, sourceIds[0])]
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        clipboardPayload.setSelectedNode(sourceNodeReference);

        when:
        navService.copyNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then:
        List<Campaign> campaigns = findCampaignInsideFolder(destinationId, existingCampaignIds);
        campaigns.size() == 1
        def iterations = campaigns.get(0).getIterations()
        iterations.size() == 2
        Iteration iteration1 = iterations.find { it.getName() == "iter - tc1" }
        iteration1.getTestSuites().size() == 2
        iteration1.getTestSuites().find { it.getName() == "testSuite1" } != null
        TestSuite testsSuite1 = iteration1.getTestSuites().find { it.getName() == "testSuite1" }
        testsSuite1.getTestPlan().size() == 1
        iteration1.getTestSuites().find { it.getName() == "testSuite2" } != null
        TestSuite testsSuite2 = iteration1.getTestSuites().find { it.getName() == "testSuite2" }
        testsSuite2.getTestPlan().size() == 0
        Iteration iteration2 = iterations.find { it.getName() == "iter - tc1 -2" }
        iteration2.getTestSuites().isEmpty()
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should copy paste folder with campaigns, iterations, suite.xml")
    def "should copy paste folder with campaigns, iterations, suites"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        List<NodeReference> sourceNodeReference = [new NodeReference(NodeType.CAMPAIGN_FOLDER, sourceIds[0])]
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        clipboardPayload.setSelectedNode(sourceNodeReference);

        when:
        navService.copyNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "campaign folder has 2 campaigns"
        List<CampaignLibraryNode> nodes = folderDao.findById(destinationId).content
        nodes.size() > 0
        nodes.get(0) instanceof CampaignFolder
        CampaignFolder folderCopy = (CampaignFolder) nodes.get(0)
        folderCopy.content.size() == 2
        folderCopy.content.find { it.name == "campaign10" } != null
        folderCopy.content.find { it.name == "campaign11" } != null

        and: "campaign 1 has 2 iterations"
        Campaign campaign10Copy = folderCopy.content.find { it.name == "campaign10" }
        campaign10Copy.iterations.size() == 2
        campaign10Copy.iterations.find { it.name == "iter - tc1" } != null
        campaign10Copy.iterations.find { it.name == "iter - tc1 -2" } != null

        and: "iteration 1 has 2 test suites"
        Iteration iteration10012 = campaign10Copy.iterations.find { it.name == "iter - tc1" }
        iteration10012.testSuites.size() == 2
        iteration10012.testSuites.find { it.name == "testSuite1" } != null
        iteration10012.testSuites.find { it.name == "testSuite2" } != null

        and: "iteration 2 has no test suites"
        Iteration iteration2 = campaign10Copy.iterations.find { it.name == "iter - tc1 -2" }
        iteration2.testSuites.isEmpty()

        and: "campaign 2 is empty"
        Campaign campaign11Copy = folderCopy.content.find { it.name == "campaign11" }
        campaign11Copy.iterations.isEmpty()
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should move to same project f+c.xml")
    @ExpectedDataSet("CampaignLibraryNavigationServiceIT.should move to same project f+c-result.xml")
    def "should move folder + campaigns to same project"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "expected dataset is verified"
        session.flush()
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should not move in himself.xml")
    def "should not move in himself"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -1L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then:
        thrown(CannotMoveInHimselfException)
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should not move in himself.xml")
    def "should not move in his decendents"() {
        given:
        Long[] sourceIds = [-13L]
        Long destinationId = -1L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then:
        thrown(CannotMoveInHimselfException)
    }


    @DataSet("CampaignLibraryNavigationServiceIT.should move to another project f+c.xml")
    @ExpectedDataSet("CampaignLibraryNavigationServiceIT.should move to another project f+c-result.xml")
    def "should move folder + campaigns to another project"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "expected dataset is verified"
        session.flush()
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should move to another project f+c + cufs.xml")
    @ExpectedDataSet("CampaignLibraryNavigationServiceIT.should move to another project f+c + cufs-result.xml")
    def "should move folder + campaigns  with cufs to another project"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "expected dataset is verified"
        session.flush()
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should move to another project f+c+i+s + cufs + execs.xml")
    @ExpectedDataSet("CampaignLibraryNavigationServiceIT.should move to another project f+c+i+s + cufs + execs-result.xml")
    def "should move folder + campaigns + iterations + suites with cufs and issues to another project"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "expected dataset is verified"
        session.flush()
    }


    @DataSet("CampaignLibraryNavigationServiceIT.should move a grap of folders.xml")
    @ExpectedDataSet("CampaignLibraryNavigationServiceIT.should move a grap of folders-result.xml")
    def "should move a grap of folder in a new project"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "expected dataset is verified"
        session.flush()
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should move to another project and keep issues.xml")
    @ExpectedDataSet("CampaignLibraryNavigationServiceIT.should move to another project and keep issues-result.xml")
    def "should move to another project and keep issues"() {
        given: "a dataset with 2 projects having the same bugtracker"
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "issues are kept"
        session.flush()
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should move to another project and remove issues.xml")
    @ExpectedDataSet("CampaignLibraryNavigationServiceIT.should move to another project and remove issues-result.xml")
    def "should move to another project and remove issues"() {
        given: "a dataset with 2 projects having different bugtrackers"
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then: "issues are removed"
        session.flush()
    }


    @DataSet("CampaignLibraryNavigationServiceIT.should move to same project at right position.xml")
    def "should move folder with campaigns to the right position - first"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, 0, clipboardPayload)

        then:
        CampaignFolder parentFolder = (CampaignFolder) folderDao.findById(-2L)
        parentFolder.content*.id.containsAll([-1L, -20L, -21L])
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should move to same project at right position.xml")
    def "should move folder with campaigns to the right position - middle"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, 1, clipboardPayload)

        then:
        CampaignFolder parentFolder = (CampaignFolder) folderDao.findById(-2L)
        parentFolder.content*.id.containsAll([-20L, -1L, -21L])
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should move to same project at right position.xml")
    def "should move folder with campaigns to the right position - last"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))

        when:
        navService.moveNodesToFolder(destinationId, sourceIds, 2, clipboardPayload)

        then:
        CampaignFolder parentFolder = (CampaignFolder) folderDao.findById(-2L)
        parentFolder.content*.id.containsAll([-20L, -21L, -1L])
    }


    def findCampaign(id) {
        session.load(Campaign, id)
    }

    /* Hierarchy:
    Library
    ├─ Sprint group #1
    ├─ Folder #2
    │  ├─ Campaign #3
    ├─ Folder #4
    │  ├─ Sprint group #5
    ├─ Sprint group #6
    │  ├─ Folder #7
    ├─ Sprint #8
     */

    @DataSet("CampaignLibraryNavigationServiceIT.sprint group hierarchy.xml")
    def "should copy sprint"() {
        given:
        Long[] sourceIds = [-8L]
        List<NodeReference> sourceNodeReference = [new NodeReference(NodeType.SPRINT, sourceIds[0])]
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        clipboardPayload.setSelectedNode(sourceNodeReference);

        when:
        navService.copyNodesToSprintGroup(destinationId, sourceIds, clipboardPayload)

        then:
        notThrown(IllegalSprintGroupHierarchyException)

        where:
        destinationId << [-1L, -5L, -6L]
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should keep sprintReqVersion order when copying sprint.xml")
    def "should keep sprintReqVersion order when copying sprint"() {
        given:
        Long destinationId = -5L
        Long[] sourceIds = [-8L]
        List<NodeReference> sourceNodeReference = [new NodeReference(NodeType.SPRINT, sourceIds[0])]
        SprintGroup destination = findEntity(SprintGroup.class, destinationId) as SprintGroup
        List<Long> destinationContentIds = destination.content.id
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        clipboardPayload.setSelectedNode(sourceNodeReference);

        when:
        navService.copyNodesToSprintGroup(destinationId, sourceIds, clipboardPayload)

        then:
        List<CampaignLibraryNode> nodes = destination.content.findAll { !destinationContentIds.contains(it.id) }
        nodes.size() > 0
        nodes.get(0) instanceof Sprint
        Sprint copiedSprint = (Sprint) nodes.get(0)
        def sprintReqVersions = copiedSprint.getSprintReqVersions()
        sprintReqVersions[versionIndex].name == versionName

        where:
        versionIndex | versionName
        0            | "First position"
        1            | "Second position"
        2            | "Third position"
    }


    @DataSet("CampaignLibraryNavigationServiceIT.sprint group hierarchy.xml")
    def "should reject illegal sprint group hierarchies after moving into folder"() {
        given:
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        Long[] sources = sourceIds

        when:
        navService.moveNodesToSprintGroup(7L, sources, clipboardPayload)

        then:
        thrown(IllegalSprintGroupHierarchyException)

        where:
        sourceIds << [[-1L], [-2L], [-3L], [-4L]]
    }

    @DataSet("CampaignLibraryNavigationServiceIT.sprint group hierarchy.xml")
    def "should reject illegal sprint group hierarchies after moving into sprint group"() {
        given:
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        Long[] sources = sourceIds

        when:
        navService.moveNodesToSprintGroup(1L, sources, clipboardPayload)

        then:
        thrown(IllegalSprintGroupHierarchyException)

        where:
        sourceIds << [[-2L], [-3L], [-4L], [-5L], [-6L]]
    }

    @DataSet("CampaignLibraryNavigationServiceIT.sprint group hierarchy.xml")
    def "should reject illegal sprint group hierarchies after copying into sprint group"() {
        given:
        Long destinationId = 1L
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        Long[] sources = sourceIds

        when:
        navService.copyNodesToSprintGroup(destinationId, sources, clipboardPayload)

        then:
        thrown(IllegalSprintGroupHierarchyException)

        where:
        sourceIds << [[-2L], [-3L], [-4L], [-5L], [-6L]]
    }

    @DataSet("CampaignLibraryNavigationServiceIT.should copy to other project.xml")
    def "should copy paste campaign folder with cuf values to other project"() {
        given:
        Long[] sourceIds = [-1L]
        Long destinationId = -2L
        List<NodeReference> sourceNodeReference = [new NodeReference(NodeType.CAMPAIGN_FOLDER, sourceIds[0])]
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(Arrays.asList(sourceIds))
        clipboardPayload.setSelectedNode(sourceNodeReference)

        when:
        navService.copyNodesToFolder(destinationId, sourceIds, clipboardPayload)

        then:
        List<CampaignLibraryNode> nodes = folderDao.findById(destinationId).content
        nodes.size() > 0
        nodes.get(0) instanceof CampaignFolder
        CampaignFolder folderCopy = (CampaignFolder) nodes.get(0)

        and: "cufs are updated to match destination project's config"
        def copiedFolderCUFValues = findCufValuesForEntity(BindableEntity.CAMPAIGN_FOLDER, folderCopy.id)
        copiedFolderCUFValues.size() == 2
        copiedFolderCUFValues.find { it.getBinding().id == -9L }.value == "campaign-1-cuf1"
        copiedFolderCUFValues.find { it.getBinding().id == -11L }.value == "Monday"
    }

    @DataSet("CampaignLibraryNavigationServiceIT.create folder with cufs.xml")
    def "should create a folder with cufs in an existing folder"() {
        given:
        def folderDto = new NewFolderDto()
        folderDto.name = "new folder"
        folderDto.description = "new description"
        def customFields = new HashMap<Long, RawValue>()
        customFields.put(-2L, new RawValue("new cuf value"))
        customFields.put(-3L, new RawValue("2020-08-24"))
        folderDto.customFields = customFields

        when:
        def campaignFolder = navService.addFolderToFolder(-10L, folderDto)

        then:
        campaignFolder.name == "new folder"
        campaignFolder.description == "new description"
        campaignFolder.boundEntityType == CAMPAIGN_FOLDER

        and:
        def folderCUFValues = findCufValuesForEntity(CAMPAIGN_FOLDER, campaignFolder.id)
        folderCUFValues.size() == 2
        folderCUFValues.find { it.getBinding().id == -6L }.value == "new cuf value"
        folderCUFValues.find { it.getBinding().id == -7L }.value == "2020-08-24"
    }

    @DataSet("CampaignLibraryNavigationServiceIT.create folder with cufs.xml")
    def "should create a folder with cufs in a library"() {
        given:
        def folderDto = new NewFolderDto()
        folderDto.name = "new folder"
        folderDto.description = "new description"
        def customFields = new HashMap<Long, RawValue>()
        customFields.put(-2L, new RawValue("new cuf value"))
        customFields.put(-3L, new RawValue("2020-08-24"))
        folderDto.customFields = customFields

        when:
        def campaignFolder = navService.addFolderToLibrary(-1L, folderDto)

        then:
        campaignFolder.name == "new folder"
        campaignFolder.description == "new description"
        campaignFolder.boundEntityType == CAMPAIGN_FOLDER

        and:
        def folderCUFValues = findCufValuesForEntity(CAMPAIGN_FOLDER, campaignFolder.id)
        folderCUFValues.size() == 2
        folderCUFValues.find { it.getBinding().id == -6L }.value == "new cuf value"
        folderCUFValues.find { it.getBinding().id == -7L }.value == "2020-08-24"
    }

    @DataSet("CampaignLibraryNavigationServiceIT.xml")
    def "should reject wrong sprint path"() {
        when:
        navService.validatePathForSync("p1", sprintPath)

        then:
        thrown(expectedException)

        where:
        sprintPath                                                            | expectedException
        ""                                                                    | RequiredFieldException
        "ah//ha"                                                              | PathValidationDomainException
        "ah/"                                                                 | PathValidationDomainException
        "sprint group 1/I am after a sprint group"                            | PathContainsASprintGroupException
        "Already/Existing/Sprint Path/In Sync Conf"                           | PathAlreadyInUseException
        "Already/Existing/Sprint Path/In Sync Conf/I am after a sprint group" | PathContainsASprintGroupException
    }

    @DataSet("CampaignLibraryNavigationServiceIT.xml")
    def "should validate sprint path"() {
        when:
        navService.validatePathForSync("p1", sprintPath)

        then:
        noExceptionThrown()

        where:
        sprintPath << ["available/path", "another/available/path", "availablePath"]
    }
}
