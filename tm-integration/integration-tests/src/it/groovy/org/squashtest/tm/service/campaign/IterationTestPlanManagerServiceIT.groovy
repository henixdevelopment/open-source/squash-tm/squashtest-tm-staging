/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
class IterationTestPlanManagerServiceIT extends DbunitServiceSpecification {

	@Inject
	private IterationTestPlanManagerService service


	//		TODO make it work
	//		@DataSet("IterationTestPlanManagerServiceIT.1execution.noEDRight.xml")
	//		def "should remove executed Test plan from iteration because has not EXTENDED_DELETE rights"(){
	//			given :
	//			def iterationId = -1L
	//			def testPlanItem = -1L
	//			when :
	//			service.removeTestPlanFromIteration(testPlanItem)
	//
	//			then :
	//			found(IterationTestPlanItem.class, -1L)
	//		}

	@DataSet("IterationTestPlanManagerServiceIT.0execution.xml")
	def "should remove not executed Test plan from iteration"(){
		given :
		def iterationId = -1L
		def testPlanItem = -1L
        IterationTestPlanItem item = findEntity(IterationTestPlanItem.class, testPlanItem)
		when :
		service.removeTestPlanFromIteration(item)

		then :
		!found(IterationTestPlanItem.class, -1L)
	}


}
