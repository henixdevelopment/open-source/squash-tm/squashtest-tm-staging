/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
@Transactional
class ExecutionDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	ExecutionDisplayDao executionDisplayDao

	def "should retrieve on execution view"() {

		when:
		def view = executionDisplayDao.findExecutionView(executionId)

		then:
		view.id == executionId
		view.projectId == expectedProjectId
		view.executionOrder == expectedExecutionOrder

		where:
		executionId || expectedProjectId | expectedExecutionOrder
		-1L         || -1L               | 0
		-4L         || -1L               | 2
	}

	def "should retrieve a detailed execution view"() {

		when:
		def view = executionDisplayDao.findExecutionView(-1L)

		then:
		view.id == -1L
		view.projectId == -1L
		view.prerequisite == 'none'
		view.tcImportance == 'VERY_HIGH'
		view.tcStatus == 'APPROVED'
		view.comment == "a comment not a description !!!"
		view.testCaseId == -1
	}
}
