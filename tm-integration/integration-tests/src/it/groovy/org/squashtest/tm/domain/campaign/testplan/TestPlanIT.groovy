/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign.testplan

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.testcase.TestCase
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@UnitilsSupport
@Transactional
@DataSet
class TestPlanIT extends DbunitServiceSpecification {

	@PersistenceContext
	EntityManager em

	def "should add a test plan item"() {
		given:
		TestPlan testPlan = em.find(TestPlan.class, -1L)
        TestCase testCase = Mock()
        testCase.getName() >> "mocked test case"

		when:
        testPlan.createAndAddTestPlanItems([testCase])

        then:
        testPlan.getTestPlanItems().size() == 1
        testPlan.getTestPlanItems().get(0).getLabel() == "mocked test case"
	}

	def "should remove test plan item"() {
		given:
		TestPlan testPlan = em.find(TestPlan.class, -2L)
		TestPlanItem item = em.find(TestPlanItem.class, -2L)

		when:
        testPlan.removeTestPlanItem(item)

        then:
        testPlan.getTestPlanItems().size() == 0
	}

	def "should add exploratory session"() {
		given:
		TestPlan testPlan = em.find(TestPlan.class, -1L)
		TestCase testCase = em.find(TestCase.class, -3L)

		when:
        testPlan.createAndAddTestPlanItems([testCase])

        then:
        testPlan.getTestPlanItems().size() == 1
        testPlan.getTestPlanItems().get(0).getExploratorySessionOverview() != null
	}
}
