/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.requirement.HighLevelRequirementService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.time.LocalDateTime
import java.time.Month
import java.time.ZoneId

@UnitilsSupport
@DataSet
@Transactional
class HighLevelRequirementDisplayDaoIT extends DbunitServiceSpecification {

	@Inject
	private HighLevelRequirementDisplayDao dao

	@Inject
	private HighLevelRequirementService highLevelRequirementService

	def setup() {
		def ids = [[-11L, -11L], [-21L, -21L], [-31L, -31L], [-321L, -32L]]
		ids.each {
			setBidirectionalReqReqVersion(it[0], it[1])
		}
		highLevelRequirementService.linkToHighLevelRequirement(-21L, -31L)
//		em.flush()
	}

	def setBidirectionalReqReqVersion(Long reqVersionId, Long reqId) {
		def reqVer = em.find(RequirementVersion.class, reqVersionId)
		def req = em.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		req.setCurrentVersion(reqVer)
	}

	def "should not find non existing links"() {

		when:
		def versionLinkDtos = dao.findLinkedLowLevelRequirements(-11L)

		then:
		versionLinkDtos.size() == 0
	}

	def "should find links"() {

		when:
		def versionLinkDtos = dao.findLinkedLowLevelRequirements(-21L)
			.sort({ it.requirementId })
			.reverse() //order is not specified, i prefer test the mother requirement first

		then:
		versionLinkDtos.size() == 2

		def firstDto = versionLinkDtos.get(0)
		firstDto.projectName == 'Project1'
		firstDto.requirementId == -31L
		firstDto.requirementVersionId == -31L
		firstDto.name == 'Requirement3'
		firstDto.reference == ''
		firstDto.milestoneLabels == null
		firstDto.milestoneMinDate == null
		firstDto.milestoneMaxDate == null
		firstDto.versionNumber == 1

		def secondDto = versionLinkDtos.get(1)
		secondDto.projectName == 'Project1'
		secondDto.requirementId == -32L
		secondDto.requirementVersionId == -321L
		secondDto.name == 'Requirement321'
		secondDto.reference == 'ref2'
		secondDto.milestoneLabels == 'jalon1, jalon2'
		def milestoneMinDate = LocalDateTime.ofInstant(secondDto.milestoneMinDate.toInstant(), ZoneId.systemDefault());
		milestoneMinDate.getDayOfMonth() == 31
		milestoneMinDate.getMonth() == Month.AUGUST
		milestoneMinDate.getYear() == 2020
		def milestoneMaxDate = LocalDateTime.ofInstant(secondDto.milestoneMaxDate.toInstant(), ZoneId.systemDefault());
		milestoneMaxDate.getDayOfMonth() == 31
		milestoneMaxDate.getMonth() == Month.OCTOBER
		milestoneMaxDate.getYear() == 2020
		secondDto.versionNumber == 1
	}
}
