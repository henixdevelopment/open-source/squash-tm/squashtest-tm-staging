/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@UnitilsSupport
class CustomDenormalizedEnvironmentTagDaoIT extends DbunitDaoSpecification {

	@Inject
	CustomDenormalizedEnvironmentTagDao customDenormalizedEnvironmentTagDao

	def "should fetch only test technology"() {
		when:
		def tags = customDenormalizedEnvironmentTagDao.getAllByExecutionId(-2)

		then:
		tags.value == "foobar"
	}

	def "should fetch technology with denormalized tags"() {
		when:
		def tags = customDenormalizedEnvironmentTagDao.getAllByExecutionId(-3)

		then:
		tags.value == "bar, baz, foo, zarbi, technology"
	}
}

