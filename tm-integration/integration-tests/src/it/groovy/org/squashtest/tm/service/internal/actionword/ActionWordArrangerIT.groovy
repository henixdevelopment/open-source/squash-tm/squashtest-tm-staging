/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.actionword

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.bdd.ActionWord
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.unitils.dbunit.annotation.DataSet
import org.squashtest.tm.service.internal.repository.ActionWordDao
import org.squashtest.tm.service.internal.repository.KeywordTestStepDao
import spock.unitils.UnitilsSupport
import javax.inject.Inject


@UnitilsSupport
@Transactional
class ActionWordArrangerIT extends DbunitServiceSpecification {

	@Inject
	ActionWordDao actionWordDao

	@Inject
	KeywordTestStepDao keywordTestStepDao

	@Inject
	ActionWordArranger arranger

	@DataSet("ActionWordArrangerIT.xml")
	def "arrangeActionWordsBeforeProjectDeleting(Long) - Should arrange action word "() {
		given:

		Collection<KeywordTestStep> keywordTestSteps =  keywordTestStepDao.findByActionWord(-4L)
		KeywordTestStep keywordTestStepWithActionChange = keywordTestSteps.get(0)
		ActionWord actionWordWithProjectChange = actionWordDao.findByTokenInCurrentProject("T-just an action word-", -2L)

		when:
		arranger.arrangeActionWordsBeforeProjectDeleting(-2L)

		then:
		keywordTestStepWithActionChange.getActionWord().getId() == -2L
		actionWordWithProjectChange.getProject().getId() == -1L

	}
}
