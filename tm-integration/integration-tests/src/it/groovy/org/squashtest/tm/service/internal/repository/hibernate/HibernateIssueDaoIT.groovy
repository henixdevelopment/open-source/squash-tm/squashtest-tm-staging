/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.springframework.data.domain.Pageable
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.core.foundation.collection.PagingAndSorting
import org.squashtest.tm.core.foundation.collection.SortOrder
import org.squashtest.tm.core.foundation.collection.SpringPaginationUtils
import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.CampaignFolder
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStep
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.internal.repository.IssueDao
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Ignore
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@SuppressWarnings("GroovyUnusedDeclaration")
@UnitilsSupport
class HibernateIssueDaoIT
 extends DbunitDaoSpecification {
	@Inject
	IssueDao issueDao

	@PersistenceContext
	EntityManager em

/**
 *
 * @param ppt Map of {issue, issueListId, remoteIssueId, issueId, bugtrackerId}* @return
 */
	def expected(Map ppt) {
		assert ppt.issue.id == ppt.issueId
		assert ppt.issue.remoteIssueId == ppt.remoteIssueId
		assert ppt.issue.issueList.id == ppt.issueListId
		if (ppt.bugtrackerId != null) {
			assert ppt.issue.bugtracker.id == ppt.bugtrackerId
		}
		return true // used in "then", return true so that assertion does not fail
	}

	PagingAndSorting sorter(Map props = [:]) {
		new PagingAndSorting() {
			@Override
			int getFirstItemIndex() {
				(props.firstItemIndex ?: 0)
			}

			@Override
			int getPageSize() {
				(props.pageSize ?: 10)
			}

			@Override
			boolean shouldDisplayAll() {
				(props.shouldDisplayAll ?: false)
			}

			@Override
			String getSortedAttribute() {
				"Issue.id"
			}

			@Override
			SortOrder getSortOrder() {
				(props.sortOrder ?: SortOrder.ASCENDING)
			}

			Pageable toPageable(){
				return SpringPaginationUtils.toPageable(this);
			}
		}
	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "should count issues for execution and execution steps"() {
		given:
		List<Long> execIds = [10000101L, 10000400L, 10000201L, 10000100L]
		List<Long> execStepIds = [100001010L, 100001011L, 100002010L, 100001000L]

		when:
		def result = issueDao.countIssuesfromExecutionAndExecutionSteps(execIds, execStepIds)

		then:
		result == 4
	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "should count issues for issue list ids and bugtracker id"() {
		given:
		List<Long> issueListIds = [
			10000101L,
			10000400L,
			10000201L,
			10000100L,
			100001010L,
			100001011L,
			100002010L,
			100001000L
		]
		def bugTrackerId = 100001L

		when:
		def result = issueDao.countIssuesfromIssueList(issueListIds, bugTrackerId)

		then:
		result == 4
	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "should return issues for iteration"() {
		given:
		def iterationId = 100001L

		when:
		def result = issueDao.findAllForIteration(iterationId)

		then:
		result.size() == 3;
		result*.id.containsAll([100001L, 100002L, 100003L]);
	}

	@DataSet("HibernateIssueDaoIT.test suite.xml")
	def "should return issues for test suite"() {
		given:
		def testSuiteId = 1000030L

		when:
		def result = issueDao.findAllForTestSuite(testSuiteId)

		then:
		result.size() == 3;
		result*.id.containsAll([100001L, 100002L, 100003L]);
	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "should return execution as issue detector"() {
		given:
		def issueId = 100007L

		when:
		def result = issueDao.findIssueDetectorByIssue(issueId)

		then:
		result != null
		result.issueListId == 10000400L
	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "should return execution step as issue detector"() {
		given:
		def issueId = 100005L

		when:
		def result = issueDao.findIssueDetectorByIssue(issueId)

		then:
		result != null
		result.issueListId == 100002010L
	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "[#6062] should count issues for a campaign"() {
		given:
		def camp = em.getReference(Campaign, 100001L)

		expect:
		issueDao.countByCampaign(camp) == 7

	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "[#6062] should count issues for an execution"() {
		given:
		def exec = em.getReference(Execution, 10000100L)

		expect:
		issueDao.countByExecutionAndSteps(exec) == 2

	}

	@DataSet("HibernateIssueDaoIT.xml")
	def "[#6062] should count issues for an iteration"() {
		given:
		def iter = em.getReference(Iteration, 100001L)

		expect:
		issueDao.countByIteration(iter) == 3

	}

	@DataSet("HibernateIssueDaoIT.test suite.xml")
	def "[#6062] should count issues for a test suite"() {
		given:
		TestSuite ts = em.getReference(TestSuite, 1000030L)

		expect:
		issueDao.countByTestSuite(ts) == 3

	}

	@DataSet("HibernateIssueDaoIT.folders.xml")
	def "[#6062] should count issues for a folder"() {
		given:
		CampaignFolder folder = em.getReference(CampaignFolder, 100000104L)

		expect:
		issueDao.countByCampaignFolder(folder) == 14

	}

	@DataSet("HibernateIssueDaoIT.testcases.xml")
	def "[#6062] should count issues for a test case"() {
		given:
		def testCase = em.getReference(TestCase, 100000238L)

		expect:
		issueDao.countByTestCase(testCase) == 14

	}
}
