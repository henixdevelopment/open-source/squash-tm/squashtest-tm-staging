/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.campaign

import org.jooq.DSLContext
import org.springframework.context.MessageSource
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.config.EnabledAclSpecConfig
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService
import org.squashtest.tm.service.customreport.CustomReportDashboardService
import org.squashtest.tm.service.display.testcase.TestCasePathFinderService
import org.squashtest.tm.service.grid.GridConfigurationService
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.IterationKnownIssueFinder
import org.squashtest.tm.service.internal.display.campaign.AvailableDatasetAppender
import org.squashtest.tm.service.internal.display.campaign.IterationDisplayServiceImpl
import org.squashtest.tm.service.internal.display.campaign.ReadUnassignedTestPlanHelper
import org.squashtest.tm.service.internal.display.campaign.IterationTestPlanItemSuccessRateCalculator
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.filters.InConditionBuilder
import org.squashtest.tm.service.internal.library.EntityPathHeaderService
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao
import org.squashtest.tm.service.internal.repository.hibernate.HibernateIterationDao
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.security.UserContextService
import org.squashtest.tm.service.user.PartyPreferenceService



import javax.persistence.EntityManager

import static org.squashtest.tm.service.internal.display.campaign.CampaignDisplayServiceImpl.DEACTIVATED_USER_I18N_KEY
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.toCamelCase
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
@ContextHierarchy([
	@ContextConfiguration(classes = [EnabledAclSpecConfig], name = "aclcontext", inheritLocations = false),
])
class IterationDisplayServiceIT extends DbunitServiceSpecification {

	private IterationDisplayService iterationDisplayService

	@Inject
	private TestSuiteDisplayService testSuiteDisplayService

	@Inject IterationDisplayDao iterationDisplayDao
	@Inject DSLContext dslContext
	@Inject AttachmentDisplayDao attachmentDisplayDao
	@Inject CustomFieldValueDisplayDao customFieldValueDisplayDao
	@Inject MilestoneDisplayDao milestoneDisplayDao
	@Inject HibernateIterationDao hibernateIterationDao
	@Inject IterationKnownIssueFinder iterationKnownIssueFinder
	@Inject IterationTestPlanManagerService iterationTestPlanManagerService
	@Inject TestSuiteDisplayDao testSuiteDisplayDao
	@Inject IterationTestPlanItemSuccessRateCalculator rateCalculator
	@Inject AutomatedSuiteDisplayDao automatedSuiteDisplayDao
	@Inject CustomReportDashboardService customReportDashboardService
	@Inject ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper
	@Inject AvailableDatasetAppender availableDatasetAppender
	@Inject PartyPreferenceService partyPreferenceService
	@Inject UserContextService userContextService
	@Inject PermissionEvaluationService permissionEvaluationService
	@Inject EntityPathHeaderDao entityPathHeaderDao
	@Inject GridConfigurationService gridConfigurationService
	@Inject EntityManager entityManager
    @Inject CustomItpiLastExecutionFilterDao itpiLastExecutionFilterDao
	def messageSource = Mock(MessageSource)

	def setup() {
		UserContextHelper.setUsername("JP01")
		iterationDisplayService = new IterationDisplayServiceImpl(
				iterationDisplayDao,
				dslContext,
				attachmentDisplayDao,
				customFieldValueDisplayDao,
				milestoneDisplayDao,
				hibernateIterationDao,
				iterationKnownIssueFinder,
				iterationTestPlanManagerService,
				testSuiteDisplayDao,
				rateCalculator,
				automatedSuiteDisplayDao,
				customReportDashboardService,
				readUnassignedTestPlanHelper,
				availableDatasetAppender,
				partyPreferenceService,
				userContextService,
				permissionEvaluationService,
                entityPathHeaderDao,
				gridConfigurationService,
				entityManager,
				messageSource,
                itpiLastExecutionFilterDao
		)
		messageSource.getMessage(DEACTIVATED_USER_I18N_KEY, null, _ as Locale) >> "désactivé"
	}


	def "should fetch an iteration view data"() {
		when:
		def iterationView = this.iterationDisplayService.findIterationView(-10012L)
		def secondIterationView = this.iterationDisplayService.findIterationView(-2L)

		then:
		iterationView.id == -10012L
		iterationView.name == "iter - tc1"
		iterationView.projectId == -1L
		iterationView.attachmentListId == -12L
		iterationView.hasDatasets

		secondIterationView.id == -2L
		!secondIterationView.hasDatasets
	}

	def "should fetch an iteration test plan"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == 12
		def rows = gridResponse.dataRows
		def itpi = rows.get(0)
		itpi.id == "-1"
		itpi.data.get("itemTestPlanId") == -1L
		itpi.data.get("projectName") == "pro"
		itpi.data.get(toCamelCase(INFERRED_EXECUTION_MODE)) == "MANUAL"
		itpi.data.get("testCaseReference") == "REF"
		itpi.data.get("testCaseName") == "test case 1"
		itpi.data.get("importance") == "VERY_HIGH"
		itpi.data.get("datasetName") == "Dataset1"
		itpi.data.get("testSuites") == "Suite1, Suite2"
		itpi.data.get("executionStatus") == "SUCCESS"
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")
		itpi.data.get("milestoneMinDate") == dateFormat.parse("2010-02-01")
		itpi.data.get("milestoneMaxDate")  == dateFormat.parse("2020-02-01")
		itpi.data.get("milestoneLabels") == "M1, M3, M2"
		Math.floor((Integer) itpi.data.get("successRate")) == 66
		itpi.data.get("assigneeFullName") == "Paul (JP01)"
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		itpi.data.get("lastExecutedOn") == dateTimeFormat.parse("2020-04-03 09:32:35")

		def otherItpi = rows.get(1)
		otherItpi.id == "-2"
		otherItpi.data.get("itemTestPlanId") == -2L
		otherItpi.data.get("executionStatus") == "FAILURE"
		otherItpi.data.get("successRate") == 0.0
		otherItpi.data.get("lastExecutedOn") == null
		otherItpi.data.get("assigneeFullName") == "Jean-Paul Sartre (désactivé)"
	}

	@DataSet("IterationDisplayServiceIT.automation-configurations.xml")
	def "should fetch iteration test plan with items of different automation configurations"() {
		given:
        def gridRequest = new GridRequest()
		gridRequest.size = 25
		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-11L, gridRequest)
		then:
		gridResponse.count == 6
		def rows = gridResponse.dataRows
		rows.find { it.id == "-1" }.data.get(toCamelCase(INFERRED_EXECUTION_MODE)) == "AUTOMATED"
		rows.find { it.id == "-2" }.data.get(toCamelCase(INFERRED_EXECUTION_MODE)) == "AUTOMATED"
		rows.find { it.id == "-3" }.data.get(toCamelCase(INFERRED_EXECUTION_MODE)) == "MANUAL"
		rows.find { it.id == "-4" }.data.get(toCamelCase(INFERRED_EXECUTION_MODE)) == "MANUAL"
		rows.find { it.id == "-5" }.data.get(toCamelCase(INFERRED_EXECUTION_MODE)) == "MANUAL"
		rows.find { it.id == "-6" }.data.get(toCamelCase(INFERRED_EXECUTION_MODE)) == "MANUAL"
	}

	@Unroll
	def "should fetch a paginated iteration test plan"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = size
		gridRequest.page = page

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == 12
		def rows = gridResponse.dataRows
		rows.size() == expectedRowsSize
		rows.get(0).data.get("itemTestPlanId") == expectedFirstId
		rows.get(expectedRowsSize - 1).data.get("itemTestPlanId") == expectedLastId

		where:
		size | page || expectedRowsSize | expectedFirstId | expectedLastId
		5    | 0    || 5                | -1L             | -5L
		5    | 1    || 5                | -6L             | -10L
		5    | 2    || 2                | -11L            | -12L
		10   | 0    || 10               | -1L             | -10L
		10   | 1    || 2                | -11L            | -12L
		50   | 0    || 12               | -1L             | -12L
	}

	@Unroll
	def "should fetch an iteration test plan filtered on project name"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "projectName"
		filterValue.operation = "LIKE"
		filterValue.values = [lookedValue]
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValue || expectedCount
		"ect 2"     || 1
		"pro"       || 12
		"hello"     || 0
	}

	@Unroll
	def "should fetch an iteration test plan filtered on test suites"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "testSuites"
		filterValue.operation = "LIKE"
		filterValue.values = [lookedValue]
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValue || expectedCount
		"Suite1"    || 1
	}

	@Unroll
	def "should fetch an iteration test plan filtered on test case name"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "testCaseName"
		filterValue.operation = "LIKE"
		filterValue.values = [lookedValue]
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValue   || expectedCount
		"test case 1" || 10
		"test ca"     || 12
		"test case 2" || 1
		"project"     || 1
		"2"           || 2
	}

	@Unroll
	def "should fetch an iteration test plan filtered on test case reference"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "testCaseReference"
		filterValue.operation = "LIKE"
		filterValue.values = [lookedValue]
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValue   || expectedCount
		"test case 1" || 0
		"REF"         || 12
		"REFE"        || 1
		"REFERENCE 2" || 1
	}

	@Unroll
	def "should fetch an iteration test plan filtered on test case dataset"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "datasetName"
		filterValue.operation = "LIKE"
		filterValue.values = [lookedValue]
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValue   || expectedCount
		"test case 1" || 0
		"Dataset1"    || 1
	}

	@Unroll
	def "should fetch an iteration test plan filtered on user login"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "assigneeLogin"
		filterValue.operation = "IN"
		filterValue.values = lookedValues
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValues                                                 || expectedCount
		["JP01"]                                                     || 1
		["toto"]                                                     || 0
		["JP01", "toto"]                                             || 1
		[InConditionBuilder.SQUASH_TM_NULL_VALUE_IN_FILTERS]         || 10
		[InConditionBuilder.SQUASH_TM_NULL_VALUE_IN_FILTERS, "JP01"] || 11
	}

	@Unroll
	def "should fetch an iteration test plan filtered on execution date"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "lastExecutedOn"
		filterValue.operation = "BETWEEN"
		filterValue.values = lookedValues
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValues                 || expectedCount
		["2020-01-08", "2021-01-08"] || 2
		["2020-01-08", "2020-04-03"] || 1
		["2021-01-08", "2021-05-08"] || 0
	}


	@Unroll
	def "should fetch an iteration test plan filtered on execution status"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def filterValue = new GridFilterValue()
		filterValue.id = "executionStatus"
		filterValue.operation = "IN"
		filterValue.values = lookedValues
		gridRequest.filterValues = [filterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == expectedCount

		where:
		lookedValues           || expectedCount
		["SUCCESS", "FAILURE"] || 2
		["SUCCESS"]            || 1
		["READY"]              || 9
		["RUNNING"]            || 0
	}

	def "should fetch an iteration test plan filtered on test case reference and name"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		def nameFilterValue = new GridFilterValue()
		nameFilterValue.id = "testCaseName"
		nameFilterValue.operation = "LIKE"
		nameFilterValue.values = ["2"]

		def referenceFilterValue = new GridFilterValue()
		referenceFilterValue.id = "testCaseReference"
		referenceFilterValue.operation = "LIKE"
		referenceFilterValue.values = ["REFERENCE 2"]
		gridRequest.filterValues = [nameFilterValue, referenceFilterValue]

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.count == 1
	}

	def "should fetch an iteration test plan and append rates"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		Math.floor(gridResponse.dataRows.get(0).data.get("successRate") as Float) == 66
		gridResponse.dataRows.get(1).data.get("successRate") == 0f
	}


	def "should fetch an iteration test plan and append available datasets"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.iterationDisplayService.findTestPlan(-10012L, gridRequest)

		then:
		gridResponse.dataRows.get(0).data.get("datasetName") == "Dataset1"
		gridResponse.dataRows.get(0).data.get("availableDatasets").size() == 2

		gridResponse.dataRows.get(11).data.get("datasetName") == null
		gridResponse.dataRows.get(11).data.get("availableDatasets").size() == 0
	}

	def "should return ordered iteration full names by given iteration ids"() {
        given:
        def iterationIds = [-2L, -10012L]
        def projectIds = [-1L]

		when:
		def result = iterationDisplayService.retrieveFullNameByIterationIds(iterationIds, projectIds)

		then:
		result.size() == 2
		result == ["azerty - iter - tc1", "iter - tc1 -2"]
	}

}
