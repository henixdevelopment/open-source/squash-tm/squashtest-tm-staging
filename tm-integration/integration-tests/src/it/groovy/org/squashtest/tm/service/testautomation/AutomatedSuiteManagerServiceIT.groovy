/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.service.attachment.AttachmentManagerService
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.internal.repository.ExecutionDao
import org.squashtest.tm.service.internal.repository.ExecutionStepDao
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration
import org.squashtest.tm.service.testautomation.testplanretriever.RestTestPlanFinder
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import org.squashtest.tm.service.testautomation.supervision.model.SquashAutomExecutionView
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification
import org.squashtest.tm.service.testautomation.supervision.model.AutomatedSuiteOverview
import org.squashtest.tm.domain.EntityReference
import org.squashtest.tm.domain.EntityType
import spock.lang.Unroll

@NotThreadSafe
@UnitilsSupport
@Transactional
class AutomatedSuiteManagerServiceIT extends DbunitServiceSpecification {

	@Inject
	AutomatedSuiteManagerService service

	@Inject
	AutomatedSuiteDao automatedSuiteDao

	@Inject
	AutomatedExecutionExtenderDao automatedExecutionExtenderDao

	@Inject
	ExecutionDao executionDao

	@Inject
	ExecutionStepDao executionStepDao

    @Inject
    private AttachmentManagerService attachmentManagerService

    @Inject
    private RestTestPlanFinder restTestPlanFinder

	@DataSet("TestAutomationService.TFtrigger.xml")
	def "should return automated test suite associated to an iteration given its id (Squash TF executions only)"() {
        given:
        def iterationId = -11L


		when:
        def testPlanIds = restTestPlanFinder.getItemTestPlanIdsByIterationId(iterationId)
		AutomatedSuite suite = service.createFromIterationTestPlan(iterationId, testPlanIds)

		then:
		suite.executionExtenders.size() == 3
		suite.executionExtenders[0].id == 1L
		suite.executionExtenders[0].automatedTest.id == -71L
	}

	@DataSet("TestAutomationService.deleteOldAutomatedSuites.xml")
	def "Should delete all old automated suites according to projects configurations"() {
		when:
			service.cleanOldSuites()
		then:
			automatedSuiteDao.findAll().size() == 3
			executionDao.findAll().size() == 2
			automatedExecutionExtenderDao.findAll().size() == 2
			executionStepDao.findAll().size() == 3
	}

    @DataSet("TestAutomationService.deleteOldAutomatedSuites.xml")
    def "Should delete all old automated suites according to project configurations"() {
        when:
            service.cleanOldSuitesForProject(-3L)
        then:
            automatedSuiteDao.findAll().size() == 6
            executionDao.findAll().size() == 6
            automatedExecutionExtenderDao.findAll().size() == 6
            executionStepDao.findAll().size() == 6
    }

	@DataSet("TestAutomationService.deleteEmptyOldAutomatedSuites.xml")
	def "Should delete all old automated suite even if they have no execution"() {
		when:
			service.cleanOldSuites()
		then:
			automatedSuiteDao.findAll().size() == 0
	}

    @DataSet("TestAutomationService.deleteOldAutomatedSuites.xml")
    def "Should count the old automated suites and the automated execution extenders according to projects configurations"() {
        when:
            AutomationDeletionCount resultCount = service.countOldAutomatedSuitesAndExecutions()
        then:
            resultCount.getOldAutomatedSuiteCount() == 4
            resultCount.getOldAutomatedExecutionCount() == 5
    }

    @DataSet("TestAutomationService.deleteOldAutomatedSuites.xml")
    def "Should count the old automated suites and the automated execution extenders according to the project configuration"() {
        when:
            AutomationDeletionCount resultCountForProject2 = service.countOldAutomatedSuitesAndExecutionsForProject(-2L)
            AutomationDeletionCount resultCountForProject3 = service.countOldAutomatedSuitesAndExecutionsForProject(-3L)
        then:
            resultCountForProject2.getOldAutomatedSuiteCount() == 3
            resultCountForProject2.getOldAutomatedExecutionCount() == 4
            resultCountForProject3.getOldAutomatedSuiteCount() == 1
            resultCountForProject3.getOldAutomatedExecutionCount() == 1
    }

	def "Should not throw any Exception if no old automated suites are to delete"() {
		given: "an empty dataset"
		when:
			service.cleanOldSuites()
		then:
			noExceptionThrown()
	}

    @Unroll
    @DataSet("AutomatedSuiteManager.createFromSpecificationSquashAutom.xml")
    def "Should create an AutomatedSuite and executions from specification"() {
        given:

        AutomatedSuiteCreationSpecification specification = new AutomatedSuiteCreationSpecification()
        SquashAutomExecutionConfiguration squashAutomConfig = new SquashAutomExecutionConfiguration(
            -1L,
            new ArrayList<>(),
            new ArrayList<>(),
            new HashMap<>(),
            null
        )
        specification.setSquashAutomExecutionConfigurations(Collections.singletonList(squashAutomConfig))
        specification.setContext(new EntityReference(contextType, -1L))
        specification.setTestPlanSubsetIds(testPlanSubset)

        def testPlanIds = restTestPlanFinder.getItemTestPlanIdsFromSpecification(specification)

        when:
        AutomatedSuiteWithSquashAutomAutomatedITPIs suite = service.createFromSpecification(specification, testPlanIds)

        then:
        Collection<AutomatedExecutionExtender> extenders = automatedSuiteDao.findAllExtenders(suite.getSuite().getId())
        extenders.size() == expectedExecutionCount

        List<IterationTestPlanItem> squashAutomItems = suite.getSquashAutomAutomatedItems()
        squashAutomItems.size() == expectedIds.size()
        expectedIds.each {
            id -> assert squashAutomItems.any {
                it.id == id
            } : "Did not find id " + id + " among squashAutomItems"
        }

        where:

        contextType             | testPlanSubset | expectedIds      | expectedExecutionCount
        EntityType.ITERATION    | []             | [-1L, -2L, -3L]  | 3
        EntityType.ITERATION    | [-1L]          | [-1L]            | 1
        EntityType.TEST_SUITE   | []             | [-1L, -2L]       | 2
        EntityType.TEST_SUITE   | [-1L]          | [-1L]            | 1
    }

    @Unroll
    @DataSet("AutomatedSuiteManager.createFromSpecificationSquashAutom.xml")
    def "Should create and execute an AutomatedSuite from a specification"() {
        given:

        AutomatedSuiteCreationSpecification specification = new AutomatedSuiteCreationSpecification()
        specification.setContext(new EntityReference(contextType, -1L))
        List<String> tags = List.of("linux", "ssh")

        SquashAutomExecutionConfiguration squashAutomConfig = new SquashAutomExecutionConfiguration(
            -1L,
            new ArrayList<>(),
            tags,
            new HashMap<>(),
            null
        )
        specification.setSquashAutomExecutionConfigurations(Collections.singletonList(squashAutomConfig))

        when:
        def testPlanIds = restTestPlanFinder.getItemTestPlanIdsFromSpecification(specification);
        AutomatedSuiteOverview suite = service.createAndExecute(specification, testPlanIds)

        then:
        Collection<AutomatedExecutionExtender> extenders = automatedSuiteDao.findAllExtenders(suite.getSuiteId())
        extenders.size() == expectedExecutionCount
        extenders.each {
            assert it.getDenormalizedEnvironmentTags().size() == tags.size()
        }
        List<SquashAutomExecutionView> squashAutomExecViews = suite.getAutomExecutions()
        squashAutomExecViews.size() == expectedItems
        squashAutomExecViews.find {
            it.id == -1L
        }.datasetLabel == "dataset_1"
        squashAutomExecViews.find {
            it.id == -2L
        }.datasetLabel == null

        where:

        contextType             | expectedItems  | expectedExecutionCount
        EntityType.ITERATION    | 3              | 3
        EntityType.TEST_SUITE   | 2              | 2
    }

    @DataSet("AutomatedSuiteManager.archiving.xml")
    def "Should do complete prune of automated suite"() {
        given:
        def executionIds = List.of(-1L, -2L, -3L)

        when:
        service.pruneAutomatedSuites(Collections.singletonList("4028818d8abd08b50dcez2r"), true)

        then:
        attachmentManagerService.getListPairContentIDListIDForExecutionIds(executionIds).size() == 0
    }

    @DataSet("AutomatedSuiteManager.archiving.xml")
    def "Should do partial prune of automated suite"() {
        given:
        def successExecId = List.of(-1L)
        def notSuccessExecId = List.of(-2L, -3L)

        when:
        service.pruneAutomatedSuites(Collections.singletonList("4028818d8abd08b50dcez2r"), false)

        then:
        attachmentManagerService.getListPairContentIDListIDForExecutionIds(successExecId).size() == 0
        attachmentManagerService.getListPairContentIDListIDForExecutionIds(notSuccessExecId).size() != 0
    }
}
