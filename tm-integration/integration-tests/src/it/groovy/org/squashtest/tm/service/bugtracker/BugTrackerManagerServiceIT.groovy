/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.bugtracker

import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.bugtracker.BugtrackerProject
import org.squashtest.tm.domain.bugtracker.Issue
import org.squashtest.tm.domain.requirement.RequirementSyncExtender
import org.squashtest.tm.domain.servers.StoredCredentials
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet("BugTrackerManagerServiceIT.xml")
class BugTrackerManagerServiceIT extends DbunitServiceSpecification {

	@Inject BugTrackerManagerService bugTrackerManagerService

    def "should delete bugTrackers"() {
        when:
        bugTrackerManagerService.deleteBugTrackers([-1L, -3L])

        then:
        em.flush()
        em.clear()

        findEntity(Issue.class, -1L) == null
        findEntity(Issue.class, -2L) == null
        findEntity(Issue.class, -3L) == null
        findEntity(BugtrackerProject.class, -1L) == null
        findEntity(StoredCredentials.class, -1L) == null
        findEntity(BugTracker.class, -1L) == null

        findEntity(Issue.class, -111L) == null
        findEntity(Issue.class, -222L) == null
        findEntity(Issue.class, -333L) == null
        findEntity(BugtrackerProject.class, -3L) == null
        findEntity(StoredCredentials.class, -3L) == null
        findEntity(RequirementSyncExtender.class, -3L) == null
        findEntity(BugTracker.class, -3L) == null
    }
}
