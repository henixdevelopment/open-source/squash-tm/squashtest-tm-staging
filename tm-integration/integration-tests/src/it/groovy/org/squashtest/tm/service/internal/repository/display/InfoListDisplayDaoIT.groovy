/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.display.dto.InfoListDto
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class InfoListDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	InfoListDisplayDao dao

	def "Should find InfoList with items"() {

		when:
		List<InfoListDto> infoLists = dao.findAllWithItems()

		then:

		infoLists.size() == 3
		infoLists.collect({ it.id}) == [-182L, -181L, -180L]
		for (InfoListDto infoList in infoLists) {
			!infoList.items.isEmpty()
		}
	}
}
