/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.artificialintelligence.server

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.aiserver.AiServer
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.servers.AuthenticationPolicy
import org.squashtest.tm.domain.servers.AuthenticationProtocol
import org.squashtest.tm.exception.NameAlreadyInUseException
import org.squashtest.tm.exception.artificialintelligence.server.MalformedJsonPathException;
import java.text.SimpleDateFormat
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport
import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class AiServerManagerServiceIT extends DbunitServiceSpecification {

    @Inject
    private AiServerManagerService aiServerService

    def "should create a new ai server"() {
        given:
        def dateFormat = new SimpleDateFormat("yyyy-MM-dd")
        Date parsedDate = dateFormat.parse("2023-02-15")
        def aiServer = new AiServer()
        aiServer.setUrl("https://servertest.com")
        aiServer.setName("Un serveur")
        aiServer.setCreatedBy("manager")
        aiServer.setCreatedOn(parsedDate)
        aiServer.setDescription("Vous savez, moi je ne crois pas qu’il y ait de bonne ou de mauvaise situation. " +
            "Moi, si je devais résumer ma vie aujourd’hui avec vous, je dirais que c’est d’abord des rencontres. " +
            "Des gens qui m’ont tendu la main, peut-être à un moment où je ne pouvais pas, où j’étais seul chez moi. " +
            "Et c’est assez curieux de se dire que les hasards, les rencontres forgent une destinée...")

        when:
        aiServerService.persist(aiServer)

        then:
        aiServer.id != null
        AiServer createdAiServer = findEntity(AiServer.class, aiServer.getId()) as AiServer

        createdAiServer.url == "https://servertest.com"
        createdAiServer.description.contains("Vous savez, moi je ne crois pas qu’il y ait de bonne ou de mauvaise situation. ")
        createdAiServer.name == "Un serveur"
        createdAiServer.lastModifiedBy == null
        createdAiServer.lastModifiedOn == null
        createdAiServer.createdBy != null
        createdAiServer.createdOn != null
        createdAiServer.authenticationProtocol == AuthenticationProtocol.TOKEN_AUTH
        createdAiServer.authenticationPolicy == AuthenticationPolicy.APP_LEVEL
    }

    def "should delete AI servers not linked to any project"() {
        when:
        aiServerService.deleteAiServers([-1L, -3L])

        then:
        !found(AiServer, -1L)
        !found(AiServer, -3L)
        found(AiServer, -2L)
    }

    def "should delete an AI server linked to two projects"() {
        when:
        aiServerService.deleteAiServers([-1L])

        then:
        Project project1 = findEntity(Project.class, -1L)
        Project project2 = findEntity(Project.class, -2L)
        project1.getAiServer() == null
        project2.getAiServer() == null
        !found(AiServer, -1L)
    }

    def "should delete two AI servers linked to different projects"() {
        when:
        aiServerService.deleteAiServers([-1L, -2L])

        then:
        Project project2 = findEntity(Project.class, -2L)
        Project project3 = findEntity(Project.class, -3L)
        project2.getAiServer() == null
        project3.getAiServer() == null
        !found(AiServer, -1L)
        !found(AiServer, -2L)
    }

    def "should not update AI server name if the new name already exists"() {
        when:
        aiServerService.updateName(-1L, "OpenAi 1")

        then:
        thrown(NameAlreadyInUseException)
    }

    def "should update AI server info"() {
        when:
        aiServerService.updateName(-1L, "New Mixtral")
        aiServerService.updateUrl(-1L, "https://new-mixtral.com")
        aiServerService.updateDescription(-1L, "Mixtral latest version")

        then:
        AiServer server = findEntity(AiServer.class, -1L)
        server.name == "New Mixtral"
        server.url == "https://new-mixtral.com"
        server.description == "Mixtral latest version"
    }

    def "updateJsonPath should update AI server's JSON path when valid JSON path is provided"() {
        when:
        aiServerService.updateJsonPath(-1L, "toto")

        then:
        AiServer server = findEntity(AiServer.class, -1L)
        server.jsonPath == "toto"
        noExceptionThrown()
    }

    def "updateJsonPath should throw exception when invalid JSON path is provided"() {
        when:
        aiServerService.updateJsonPath(-1L, "invalid_json_path]]]")

        then:
        AiServer server = findEntity(AiServer.class, -1L)
        server.jsonPath == "jsonpath" // Verify that AI server's JSON path is not updated
        thrown(MalformedJsonPathException)
    }
}
