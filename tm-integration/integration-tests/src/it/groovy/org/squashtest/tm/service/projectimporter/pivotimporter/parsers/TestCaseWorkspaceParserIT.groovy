/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter.parsers


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.projectimporter.PivotFormatImport
import org.squashtest.tm.domain.testcase.ParameterAssignationMode
import org.squashtest.tm.domain.testcase.TestCaseImportance
import org.squashtest.tm.domain.testcase.TestCaseKind
import org.squashtest.tm.domain.testcase.TestCaseStatus
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata
import org.squashtest.tm.service.internal.dto.projectimporter.SquashCustomFieldInfo
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class TestCaseWorkspaceParserIT extends DbunitServiceSpecification {

    @Inject TestCaseWorkspaceParser testCaseWorkspaceParser

    def pivotImportMetadata = new PivotImportMetadata()
    def pivotFormatImport = new PivotFormatImport()


    def setup() {
        pivotImportMetadata.customFieldIdsMap.put("cuf1", new SquashCustomFieldInfo(1L, InputType.NUMERIC))
        pivotImportMetadata.customFieldIdsMap.put("cuf2", new SquashCustomFieldInfo(2L, InputType.PLAIN_TEXT))
        pivotFormatImport.id = 1
    }

    def "should parse a standard test case"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/standard_test_case.json")

        when:
            def testCaseToImport = testCaseWorkspaceParser.parseTestCase(jsonParser,pivotImportMetadata, pivotFormatImport)

        then:
            testCaseToImport.internalId == "TCA"
            testCaseToImport.testCaseKind == TestCaseKind.STANDARD.name()
            testCaseToImport.name == "TC Standard"
            testCaseToImport.description == "desc TC Standard"
            testCaseToImport.reference == "refA"
            testCaseToImport.importance == TestCaseImportance.VERY_HIGH
            testCaseToImport.status == TestCaseStatus.WORK_IN_PROGRESS
            testCaseToImport.nature == "FUNCTIONAL_TESTING"
            testCaseToImport.type == "UNDEFINED"
            testCaseToImport.verifiedRequirementIds == ["RA", "RB"]
            testCaseToImport.prerequisite == "prerequisite 1"
            testCaseToImport.parentType == EntityType.TEST_CASE_LIBRARY
            testCaseToImport.parentId == null

            testCaseToImport.customFields.size() == 2
            testCaseToImport.customFields.get(1L).value == "123"
            testCaseToImport.customFields.get(2L).value == "another cuf value"

            testCaseToImport.attachments[0].zipImportFileName == "AT08-attachment.xls"
            testCaseToImport.attachments[0].originalFileName == "attachment.xls"

            testCaseToImport.datasetParams.size() == 3
            testCaseToImport.datasetParams[0].internalId == "P1"
            testCaseToImport.datasetParams[0].name == "Param_1"
            testCaseToImport.datasetParams[0].description == "param 1 description"

            testCaseToImport.dataSets.size() == 2
            testCaseToImport.dataSets[0].internalId == "DS1"
            testCaseToImport.dataSets[0].name == "Dataset 1"

            testCaseToImport.dataSets[0].paramValues.size() == 3
            testCaseToImport.dataSets[0].paramValues[0].internalParamId == "P1"
            testCaseToImport.dataSets[0].paramValues[0].value == "value1"

            testCaseToImport.actionTestSteps.size() == 2
            testCaseToImport.actionTestSteps[0].internalId == "AS1"
            testCaseToImport.actionTestSteps[0].action == "One fist action"
            testCaseToImport.actionTestSteps[0].expectedResult == "The first expected result"
            testCaseToImport.actionTestSteps[0].customFields.size() == 2
            testCaseToImport.actionTestSteps[0].customFields.get(1L).value == "456"
            testCaseToImport.actionTestSteps[0].customFields.get(2L).value == "this is a value"
    }

    def "should parse a gherkin test case"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/gherkin_test_case.json")

        when:
            def testCasToImport = testCaseWorkspaceParser.parseTestCase(jsonParser,pivotImportMetadata, pivotFormatImport)

        then:
            testCasToImport.internalId == "TCB"
            testCasToImport.testCaseKind == TestCaseKind.GHERKIN.name()
            testCasToImport.name == "TC Gherkin"
            testCasToImport.description == "desc B"
            testCasToImport.reference == "refB"
            testCasToImport.importance == TestCaseImportance.HIGH
            testCasToImport.status == TestCaseStatus.UNDER_REVIEW
            testCasToImport.nature == "BUSINESS_TESTING"
            testCasToImport.type == "COMPLIANCE_TESTING"
            testCasToImport.customFields.size() == 0
            testCasToImport.verifiedRequirementIds == ["RA", "RB"]
            testCasToImport.script == "# language: fr\n    Fonctionnalité: TC Gherkin\n    Scénario: Je veux un café\n    Soit Un distibuteur automatique de café\n    Quand J'insère 1 euro dans la machine\n    Alors La machine me sert un café\n    Et me rends 5 centimes de monnaie"
            testCasToImport.parentType == EntityType.TEST_CASE_LIBRARY
            testCasToImport.parentId == null
    }

    def "should parse a keyword test case"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/keyword_test_case.json")

        when:
            def testCasToImport = testCaseWorkspaceParser.parseTestCase(jsonParser,pivotImportMetadata, pivotFormatImport)

        then:
            testCasToImport.internalId == "TCC"
            testCasToImport.testCaseKind == TestCaseKind.KEYWORD.name()
            testCasToImport.name == "TC Keyword"
            testCasToImport.description == "desc C"
            testCasToImport.reference == "refC"
            testCasToImport.importance == TestCaseImportance.MEDIUM
            testCasToImport.status == TestCaseStatus.APPROVED
            testCasToImport.nature == "USER_TESTING"
            testCasToImport.type == "CORRECTION_TESTING"
            testCasToImport.customFields.size() == 0
            testCasToImport.verifiedRequirementIds == ["RA", "RB", "RC"]
            testCasToImport.parentType == EntityType.TEST_CASE_FOLDER
            testCasToImport.parentId == "F1"

            testCasToImport.keywordTestSteps.size() == 4
            testCasToImport.keywordTestSteps[0].internalId == "KS1"
            testCasToImport.keywordTestSteps[0].keyword == Keyword.GIVEN
            testCasToImport.keywordTestSteps[0].docString == "a docstring"
            testCasToImport.keywordTestSteps[0].comment == "a comment"

            testCasToImport.keywordTestSteps[1].internalId == "KS2"
            testCasToImport.keywordTestSteps[1].keyword == Keyword.WHEN
            testCasToImport.keywordTestSteps[1].docString == null
            testCasToImport.keywordTestSteps[1].dataTable == "| produit | prix |\n| Expresso | 0.40 |"
            testCasToImport.keywordTestSteps[1].comment == "another comment"
    }

    def "should parse an exploratory test case"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/exploratory_test_case.json")

        when:
            def testCasToImport = testCaseWorkspaceParser.parseTestCase(jsonParser,pivotImportMetadata, pivotFormatImport)

        then:
            testCasToImport.internalId == "TCD"
            testCasToImport.testCaseKind == TestCaseKind.EXPLORATORY.name()
            testCasToImport.name == "TC Exploratory"
            testCasToImport.description == "desc D"
            testCasToImport.reference == "refD"
            testCasToImport.importance == TestCaseImportance.LOW
            testCasToImport.status == TestCaseStatus.OBSOLETE
            testCasToImport.nature == "NON_FUNCTIONAL_TESTING"
            testCasToImport.type == "EVOLUTION_TESTING"
            testCasToImport.customFields.size() == 0
            testCasToImport.verifiedRequirementIds == ["RA", "RB", "RC", "RD"]
            testCasToImport.charter == "<p>this is a charter</p>"
            testCasToImport.sessionDuration == 30
            testCasToImport.parentType == EntityType.TEST_CASE_LIBRARY
            testCasToImport.parentId == null
    }

    def "should parse a called test case"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/called_test_case.json")

        when:
            def calledTestCaseStepToImport = testCaseWorkspaceParser.parseCalledTestCase(jsonParser, pivotFormatImport)

        then:
            calledTestCaseStepToImport.internalId == "CTC1"
            calledTestCaseStepToImport.calledTestCaseInternalId == "TCE"
            calledTestCaseStepToImport.callerId == "TCA"
            calledTestCaseStepToImport.index == 1
            calledTestCaseStepToImport.parameterAssignationMode == ParameterAssignationMode.CALLED_DATASET
            calledTestCaseStepToImport.internalDatasetId == "DSE1"
    }

}
