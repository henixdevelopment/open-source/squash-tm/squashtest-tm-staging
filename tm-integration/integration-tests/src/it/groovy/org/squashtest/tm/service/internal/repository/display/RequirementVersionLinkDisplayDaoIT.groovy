/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
@Transactional
class RequirementVersionLinkDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	private RequirementVersionLinkDisplayDao dao

	def "should find requirement version links by id"() {

		when:
		def versionLinkDtos = dao.findLinksByRequirementVersionId(-11L)

		then:
		versionLinkDtos.size() == 2

		def firstDto = versionLinkDtos.get(0)
		firstDto.projectName == 'Project1'
		firstDto.id == -31L
		firstDto.name == 'Requirement3'
		firstDto.reference == ''
		firstDto.milestoneLabels == null
		firstDto.milestoneMinDate == null
		firstDto.milestoneMaxDate == null
		firstDto.versionNumber == 1
		firstDto.role == 'PARENT'

		def secondDto = versionLinkDtos.get(1)
		secondDto.projectName == 'Project1'
		secondDto.id == -21L
		secondDto.name == 'Requirement2'
		secondDto.reference == 'ref2'
		secondDto.milestoneLabels == 'jalon1, jalon2'
		def milestoneMinDate = secondDto.milestoneMinDate
		milestoneMinDate.getDate() == 31
		milestoneMinDate.getMonth() == 7
		milestoneMinDate.getYear() == 120
		def milestoneMaxDate = secondDto.milestoneMaxDate
		milestoneMaxDate.getDate() == 31
		milestoneMaxDate.getMonth() == 9
		milestoneMaxDate.getYear() == 120
		secondDto.versionNumber == 1
		secondDto.role == 'RELATIVE'
	}
}
