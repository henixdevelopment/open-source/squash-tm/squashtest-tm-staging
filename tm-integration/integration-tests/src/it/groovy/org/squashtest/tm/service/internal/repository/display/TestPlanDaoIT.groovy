/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import org.squashtest.tm.service.internal.repository.TestPlanDao
import org.squashtest.tm.domain.campaign.testplan.TestPlan
import spock.unitils.UnitilsSupport
import javax.inject.Inject

@UnitilsSupport
@DataSet
class TestPlanDaoIT extends DbunitDaoSpecification {

    @Inject
    TestPlanDao testPlanDao

    def "should delete test plans"() {
        given:
        when:
        def sprintDto = testPlanDao.deleteByIds([-1L, -2L, -3L])
        then:
        !found(TestPlan.class, -1L)
        !found(TestPlan.class, -2L)
        !found(TestPlan.class, -3L)
    }
}
