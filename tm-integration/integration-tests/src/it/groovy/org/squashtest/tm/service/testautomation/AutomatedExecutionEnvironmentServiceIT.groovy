/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
@DataSet("AutomatedExecutionEnvironmentServiceIT.xml")
/**
 * The test data is partly defined in TestAutomationServerTestConfig (that's where the mocks for automated
 * execution servers are defined)
 */
class AutomatedExecutionEnvironmentServiceIT extends DbunitServiceSpecification {


    @Inject
    AutomatedExecutionEnvironmentService service

    def "should determine if a server supports execution environments"() {
        expect:
            service.doesServerSupportAutomatedExecutionEnvironments(-1L)
            !service.doesServerSupportAutomatedExecutionEnvironments(-2L)
    }

    def "should fetch available execution environments"() {
        when:
            def environments = service.getAllAccessibleEnvironments(-1L)

        then:
            environments.size() == 5
            environments.collect { env -> env.name }.containsAll(["env1", "env2", "env3", "env4", "env5"])
            environments.collect { env -> env.tags }.containsAll(["windows", "cypress"], ["linux", "robot"], ["windows", "cucumber", "env3"], ["linux", "robot"], ["linux", "robot"])
            environments.collect { env -> env.status }.containsAll(["IDLE", "BUSY", "BUSY", "IDLE", "PENDING"])
    }

    def "should fetch available execution environments with credentials"() {
        given:
            def credentials = new TokenAuthCredentials()

        when:
            def environments = service.getAllAccessibleEnvironments(-1L, credentials)

        then:
            environments.size() == 3
            environments.collect { env -> env.name }.containsAll(["env6", "env7", "env8"])
            environments.collect { env -> env.tags }.containsAll(["windows", "postman"], ["linux", "robot"], ["windows", "cucumber", "env8"])
            environments.collect { env -> env.status }.containsAll(["UNREACHABLE", "IDLE", "BUSY"])
    }

    def "should throw if fetching execution environments when non supported"() {
        when:
            service.getAllAccessibleEnvironments(-2L)

        then:
            thrown(UnsupportedOperationException)
    }

    def "should return automated execution count for empty map of projects and test automation servers"() {
        given:
            def projectsAndServersMap = new HashMap<Long, Long>()
        when:
            def executionEnvironmentCountDto = service.getExecutionEnvironmentCountDto(projectsAndServersMap)

        then:
            executionEnvironmentCountDto != null
            executionEnvironmentCountDto.projects() == null
            executionEnvironmentCountDto.statuses() == null
            executionEnvironmentCountDto.nbOfSquashOrchestratorServers() == 0
            executionEnvironmentCountDto.nbOfUnreachableSquashOrchestrators() == null
            executionEnvironmentCountDto.nbOfServersWithMissingToken() == null
    }

    def "should return automated execution count and project informations"() {
        when:
            def projectsAndServersMap = [(-3L): (-1L), (-4L): (-40L), (-6L): (-50L)]
            def executionEnvironmentCountDto = service.getExecutionEnvironmentCountDto(projectsAndServersMap)

        then:
            executionEnvironmentCountDto.statuses() == ["BUSY": 4L, "IDLE": 4L, "PENDING": 1L, "UNREACHABLE": 2L]
            executionEnvironmentCountDto.projects.collect { project -> project.projectName }.containsAll(["Project 04", "Project 03", "Project 06"])
            executionEnvironmentCountDto.projects.collect { project -> project.projectId }.containsAll([(-4L), (-3L), (-6L)])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorName }.containsAll(["Gnocchi-40", "Roberto-1", "Tokenless Orchestrator"])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorId }.containsAll([(-40L), (-1L), (-50L)])
            executionEnvironmentCountDto.nbOfSquashOrchestratorServers == 3
            executionEnvironmentCountDto.nbOfUnreachableSquashOrchestrators == 0
            executionEnvironmentCountDto.nbOfServersWithMissingToken == 0
    }

    def "should count number of missing token servers"() {
        when:
            def projectsAndServersMap = [(-3L): (-1L), (-7L): (-50L), (-5L): (-50L)]
            def executionEnvironmentCountDto = service.getExecutionEnvironmentCountDto(projectsAndServersMap)

        then:
            executionEnvironmentCountDto.statuses() == ["BUSY": 2L, "IDLE": 2L, "PENDING": 1L]
            executionEnvironmentCountDto.projects.collect { project -> project.projectName }.containsAll(["Project 03", "Project 05", "Project 07"])
            executionEnvironmentCountDto.projects.collect { project -> project.projectId }.containsAll([(-3L), (-5L), (-7L)])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorName }.containsAll(["Roberto-1", "Tokenless Orchestrator", "Tokenless Orchestrator"])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorId }.containsAll([(-50L), (-1L), (-50L)])
            executionEnvironmentCountDto.nbOfSquashOrchestratorServers == 3
            executionEnvironmentCountDto.nbOfUnreachableSquashOrchestrators == 0
            executionEnvironmentCountDto.nbOfServersWithMissingToken == 2
    }

    def "should return automated execution count for a reachable automated server with no execution environments"() {
        when:
            def projectsAndServersMap = [(-8L): (-77L)]
            def executionEnvironmentCountDto = service.getExecutionEnvironmentCountDto(projectsAndServersMap)

        then:
            executionEnvironmentCountDto.statuses() == null
            executionEnvironmentCountDto.projects.collect { project -> project.projectName }.containsAll(["Project 08"])
            executionEnvironmentCountDto.projects.collect { project -> project.projectId }.containsAll([(-8L)])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorName }.containsAll(["Orchestrator without environments"])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorId }.containsAll([(-77L)])
            executionEnvironmentCountDto.nbOfSquashOrchestratorServers == 1
            executionEnvironmentCountDto.nbOfUnreachableSquashOrchestrators == 0
            executionEnvironmentCountDto.nbOfServersWithMissingToken == 0
    }

    def "should count number of unreachable servers"() {
        when:
            def projectsAndServersMap = [(-3L): (-1L), (-9L): (-88L)]
            def executionEnvironmentCountDto = service.getExecutionEnvironmentCountDto(projectsAndServersMap)

        then:
            executionEnvironmentCountDto.statuses() == ["BUSY": 2L, "IDLE": 2L, "PENDING": 1L]
            executionEnvironmentCountDto.projects.collect { project -> project.projectName }.containsAll(["Project 03", "Project 09"])
            executionEnvironmentCountDto.projects.collect { project -> project.projectId }.containsAll([(-3L), (-9L)])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorName }.containsAll(["Roberto-1", "Unreachable orchestrator"])
            executionEnvironmentCountDto.projects.collect { project -> project.squashOrchestratorId }.containsAll([(-88L), (-1L)])
            executionEnvironmentCountDto.nbOfSquashOrchestratorServers == 2
            executionEnvironmentCountDto.nbOfUnreachableSquashOrchestrators == 1
            executionEnvironmentCountDto.nbOfServersWithMissingToken == 0
    }

}
