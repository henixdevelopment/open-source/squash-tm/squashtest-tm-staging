/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport
import javax.inject.Inject

@UnitilsSupport
@DataSet
class TestSuiteDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	TestSuiteDisplayDao testSuiteDisplayDao

	@Unroll
	def "should fetch number of test plan items in test suite"() {
		given:
		when:
		def count = testSuiteDisplayDao.getNbTestPlanItem(testSuiteId, login)
		then:
		count == expectedResult

		where:

		testSuiteId | login 	|| expectedResult
		-1L 		| null		|| 6
		-1L 		| "JP01"	|| 5
	}
}
