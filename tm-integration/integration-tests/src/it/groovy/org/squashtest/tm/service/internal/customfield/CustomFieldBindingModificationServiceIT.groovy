/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customfield

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.service.customfield.CustomFieldBindingModificationService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet("CustomFieldVariousIT.sandbox.xml")
class CustomFieldBindingModificationServiceIT extends DbunitServiceSpecification {

    @Inject
    CustomFieldBindingModificationService service

    def "when creating a new custom field binding, should cascade the creation of cuf values for test cases"(){
        when :
        service.addNewCustomFieldBinding(-1L, BindableEntity.TEST_CASE, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.TEST_CASE, -111L],
            [-6L, BindableEntity.TEST_CASE, -112L],
            [-6L, BindableEntity.TEST_CASE, -113L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for test case folders"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.TESTCASE_FOLDER, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.contains(
            [-6L, BindableEntity.TESTCASE_FOLDER, -186L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for test steps"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.TEST_STEP, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.contains(
            [-6L, BindableEntity.TEST_STEP, -55L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for requirement versions"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.REQUIREMENT_VERSION, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.contains(
            [-6L, BindableEntity.REQUIREMENT_VERSION, -1L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for requirement folders"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.REQUIREMENT_FOLDER, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.REQUIREMENT_FOLDER, -5L],
            [-6L, BindableEntity.REQUIREMENT_FOLDER, -87L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for campaigns"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.CAMPAIGN, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.CAMPAIGN, -12L],
            [-6L, BindableEntity.CAMPAIGN, -15L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for campaign folders"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.CAMPAIGN_FOLDER, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.CAMPAIGN_FOLDER, -17L],
            [-6L, BindableEntity.CAMPAIGN_FOLDER, -58L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for iterations"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.ITERATION, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.ITERATION, -17L],
            [-6L, BindableEntity.ITERATION, -67L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for test suites"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.TEST_SUITE, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.TEST_SUITE, -19L],
            [-6L, BindableEntity.TEST_SUITE, -59L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for executions"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.EXECUTION, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.EXECUTION, -42L],
            [-6L, BindableEntity.EXECUTION, -43L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for execution steps"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.EXECUTION_STEP, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.EXECUTION_STEP, -2L],
            [-6L, BindableEntity.EXECUTION_STEP, -3L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for sprints"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.SPRINT, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.SPRINT, -36L],
            [-6L, BindableEntity.SPRINT, -37L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for sprint groups"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.SPRINT_GROUP, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.SPRINT_GROUP, -38L],
            [-6L, BindableEntity.SPRINT_GROUP, -39L]
        )
    }

    def "when creating a new custom field binding, should cascade the creation of cuf values for custom report folders"(){
        when :
        service.addNewCustomFieldBinding(-2L, BindableEntity.CUSTOM_REPORT_FOLDER, -6L)
        def customFields = listQuery("from CustomFieldValue")

        then :
        def allValuesDef = customFields
            .collect{ return [it.binding.customField.id,
                              it.boundEntityType,
                              it.boundEntityId]
            } as Set

        allValuesDef.containsAll(
            [-6L, BindableEntity.CUSTOM_REPORT_FOLDER, -25L],
            [-6L, BindableEntity.CUSTOM_REPORT_FOLDER, -26L]
        )
    }

    def "when removing a field binding, should cascade delete the concerned custom field values"(){
        when :
        service.removeCustomFieldBindings([-111L, -112L, -113L, -114L, -115L])
        def customFields = listQuery("from CustomFieldValue")

        then :
        customFields.size() == 0
    }

    def listQuery={
        return getSession().createQuery(it).list()
    }
}
