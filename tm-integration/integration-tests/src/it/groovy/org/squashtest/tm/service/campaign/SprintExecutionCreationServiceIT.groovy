/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import javax.inject.Inject
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.exception.execution.TestSuiteTestPlanHasDeletedTestCaseException
import org.squashtest.tm.exception.execution.ExecutionHasNoStepsException
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

@UnitilsSupport
class SprintExecutionCreationServiceIT extends DbunitServiceSpecification {

    @Inject
    SprintExecutionCreationService service

    @DataSet("SprintExecutionCreationServiceIT.should create a standard execution.xml")
    def "should create a standard execution"() {
        when:
        def execution = service.createExecution(-1L)

        then:
        execution != null
        execution.getSteps().size() == 1
    }

    @DataSet("SprintExecutionCreationServiceIT.should create a exploratory execution.xml")
    def "should create a exploratory execution"() {
        when:
        def execution = service.createExecution(-1L)

        then:
        execution != null
        execution.getSteps().size() == 0
        execution.getExecutionMode().name() == "EXPLORATORY"
    }

    @DataSet("SprintExecutionCreationServiceIT.should create a keyword execution.xml")
    def "should create a keyword execution"() {
        when:
        def execution = service.createExecution(-1L)

        then:
        execution != null
        execution.getSteps().size() == 1
    }

    @DataSet("SprintExecutionCreationServiceIT.should create a scripted execution.xml")
    def "should create a scripted execution"() {
        when:
        def execution = service.createExecution(-1L)

        then:
        execution != null
        execution.getSteps().size() == 1
    }

    @DataSet("SprintExecutionCreationServiceIT.should not create execution if test case was deleted.xml")
    def "should not create execution if test case was deleted"() {
        when:
        service.createExecution(-1L)

        then:
        thrown TestSuiteTestPlanHasDeletedTestCaseException
    }

    @DataSet("SprintExecutionCreationServiceIT.should not create execution if test case has no steps.xml")
    def "should not create execution if test case has no steps"() {
        when:
        service.createExecution(-1L)

        then:
        thrown ExecutionHasNoStepsException
    }

    @DataSet("SprintExecutionCreationServiceIT.should reset execution status when adding execution.xml")
    def "should reset execution status when adding execution"() {
        when:
        service.createExecution(-1L)

        then:
        TestPlanItem testPlanItem = findEntity(TestPlanItem.class, -1L)
        testPlanItem.getExecutionStatus() == ExecutionStatus.READY
    }
}
