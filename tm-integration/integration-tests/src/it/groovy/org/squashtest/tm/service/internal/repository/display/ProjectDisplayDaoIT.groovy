/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.api.workspace.WorkspaceType
import org.squashtest.tm.service.internal.display.dto.ProjectDto
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class ProjectDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	ProjectDisplayDao dao

	def "Should find InfoList with items"() {
		when:
		def status = dao.getDisabledExecutionStatus([-1L, -2L] as Set<Long>)

		then:
		status.size() == 2

	}

	def "Should append activated plugins in project DTO"() {
		given:
		ProjectDto project = new ProjectDto()
		project.setId(-1L)
		List<ProjectDto> projects = [project];

		when:
		dao.appendActivatedPlugins(projects);

		then:
		project.activatedPlugins.size() == 3;
		def activatedPluginsForCampaign = project.activatedPlugins.get(WorkspaceType.CAMPAIGN_WORKSPACE)
		activatedPluginsForCampaign.size() == 1;
		def activatedPluginsForRequirement = project.activatedPlugins.get(WorkspaceType.REQUIREMENT_WORKSPACE)
		activatedPluginsForRequirement.size() == 2;
	}
}
