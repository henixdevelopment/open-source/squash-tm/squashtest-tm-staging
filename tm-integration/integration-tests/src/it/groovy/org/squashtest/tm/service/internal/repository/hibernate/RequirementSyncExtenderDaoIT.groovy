/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.requirement.RequirementSyncExtender
import org.squashtest.tm.service.internal.repository.RequirementSyncExtenderDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

/**
 * @author aguilhem
 */
@UnitilsSupport
@DataSet
class RequirementSyncExtenderDaoIT extends DbunitDaoSpecification {

	@Inject
	RequirementSyncExtenderDao requirementSyncExtenderDao

	@DataSet("RequirementSyncExtenderDaoIT.should find remote req ids.xml")
	def "should find remote requirement ids"(){
		when:
		def result = requirementSyncExtenderDao.findAllRemoteReqIdVerifiedByATestCaseByServerUrl("http://jira.fr/", -1L)

		then:
		result.size() == 1
		result.contains("REQ-24")
	}

    def "should return empty map if no issue key is given (findByRemoteKeysAndSyncId)"() {
        when:
        def result = requirementSyncExtenderDao.findByRemoteKeysAndSyncId(remoteIds, -1L)

        then:
        result == Collections.emptyMap()

        where:
        remoteIds << [null, []]
    }

    def "should find two extenders by key thanks to issue keys and remote sync id"() {
        given:
        def remoteIds = ["JIRA-1", "JIRA-2", "JIRA-3"]

        when:
        def result = requirementSyncExtenderDao.findByRemoteKeysAndSyncId(remoteIds, -1L)

        then:
        result.size() == 2
        result.get("JIRA-1").requirement.id == -1L
        result.get("JIRA-2").requirement.id == -2L
    }

    def "should return empty map if no issue key is given (findByRemoteKeysAndProjectId)"() {
        when:
        def result = requirementSyncExtenderDao.findByRemoteKeysAndProjectId(remoteIds, -1L)

        then:
        result == Collections.emptyMap()

        where:
        remoteIds << [null, []]
    }

    def "should find two extenders by key thanks to issue keys and project id"() {
        given:
        def remoteIds = ["JIRA-1", "JIRA-2", "JIRA-3"]

        when:
        def result = requirementSyncExtenderDao.findByRemoteKeysAndProjectId(remoteIds, -1L)

        then:
        result.size() == 2
        result.get("JIRA-1").requirement.id == -1L
        result.get("JIRA-2").requirement.id == -2L
    }

    def "should find requirement id by issue key and sync id"() {
        when:
        def id = requirementSyncExtenderDao.findRequirementIdByIssueKeyAndRemoteSyncId(issueKey, remoteSyncId)

        then:
        id == resultId

        where:
        issueKey | remoteSyncId | resultId
        "JIRA-1" | -1L          | -1L
        "JIRA-2" | -2L          | null
        "JIRA-2" | -1L          | -2L
        null     | -1L          | null
    }

    def "should reset last sync date of a remote synchronisation"() {
        when:
        requirementSyncExtenderDao.resetRemoteLastUpdated(remoteSyncId)

        then:
        em.find(RequirementSyncExtender.class, extenderId).remoteLastUpdated.toString() == "1970-01-01 01:00:00.0"

        where:
        remoteSyncId | extenderId
        -1L          | -1L
        -1L          | -2L
        -2L          | -3L
    }
}
