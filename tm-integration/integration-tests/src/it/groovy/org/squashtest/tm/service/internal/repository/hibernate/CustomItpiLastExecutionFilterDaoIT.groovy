/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport
import spock.lang.Unroll

import javax.inject.Inject

@UnitilsSupport
class CustomItpiLastExecutionFilterDaoIT extends DbunitDaoSpecification {

    @Inject
    CustomItpiLastExecutionFilterDao itpiLastExecFilterDao

    @DataSet("CustomItpiLastExecutionFilterDaoIT.xml")
    def "should find last executed itpi for each tc/dataset pair in iteration scope"() {
        given:
        def iterationIds = Arrays.asList(-2L)

        when:
        def result = itpiLastExecFilterDao.gatherLatestItpiIdsForTCInScopeForIteration(iterationIds)

        then:
        def expectedResultIds = Arrays.asList(-2L, -4L)
        result.size() == expectedResultIds.size()
        result.containsAll(expectedResultIds)
    }

    @DataSet("CustomItpiLastExecutionFilterDaoIT.xml")
    def "should find last executed or created itpi ids for each tc/dataset pair in campaign scope"() {
        given:
        def campaignIds = Arrays.asList(-10L)

        when:
        def result = itpiLastExecFilterDao.gatherLatestItpiIdsForTCInScopeForCampaign(campaignIds)

        then:
        def expectedResultIds = Arrays.asList(-2L, -4L, -6L, -9L)
        result.size() == expectedResultIds.size()
        result.containsAll(expectedResultIds)

    }

    @DataSet("CustomItpiLastExecutionFilterDaoIT.should find latest itpi ids.xml")
    @Unroll
    def "should find last executed itpi for each tc/dataset pair in simple dynamic scope"() {
        when:
        def result = itpiLastExecFilterDao.gatherLatestItpiIdsForTCInDynamicScope(entityMap)

        then:
        result.size() == expectedResultIds.size()
        result.containsAll(expectedResultIds)

        where:
        entityMap                                                       | expectedResultIds
        [(EntityType.ITERATION): Arrays.asList(-2L, -3L)]               | Arrays.asList(-11L, -13L)
        [(EntityType.CAMPAIGN): Arrays.asList(-10L)]                    | Arrays.asList(-2L, -4L, -6L, -9L)
        [(EntityType.CAMPAIGN_FOLDER): Arrays.asList(-1L)]              | Arrays.asList(-11L, -13L, -6L, -9L )
        [(EntityType.CAMPAIGN_LIBRARY): Arrays.asList(-2L)]             | Arrays.asList(-14L, -15L, -16L, -17L)
        [(EntityType.PROJECT): Arrays.asList(-1L, -2L)]                 | Arrays.asList(-11L, -13L, -16L, -17L)
        [(EntityType.TEST_SUITE): Arrays.asList(-1L)]                   | Arrays.asList(-17L)
        [(EntityType.CAMPAIGN): Arrays.asList(-11L), (EntityType.PROJECT): Arrays.asList(-2L)]    | Arrays.asList(-11L, -13L, -16L, -17L)
        [(EntityType.CAMPAIGN_FOLDER): Arrays.asList(-1L), (EntityType.CAMPAIGN_LIBRARY): Arrays.asList(-2L)]    | Arrays.asList(-11L, -13L, -16L, -17L)
    }
}
