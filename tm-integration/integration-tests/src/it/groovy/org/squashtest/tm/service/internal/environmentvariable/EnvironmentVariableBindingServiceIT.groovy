/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable

import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity
import org.unitils.dbunit.annotation.DataSet
import org.springframework.transaction.annotation.Transactional
import spock.unitils.UnitilsSupport
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService
import org.squashtest.tm.service.internal.repository.CustomEnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding
import javax.persistence.EntityExistsException

import javax.inject.Inject

@Transactional
@UnitilsSupport
@DataSet("EnvironmentVariableBindingService.xml")
class EnvironmentVariableBindingServiceIT extends DbunitServiceSpecification {

	@Inject
	EnvironmentVariableBindingService environmentVariableBindingService

	@Inject
	EnvironmentVariableDao environmentVariableDao

	@Inject
	EnvironmentVariableBindingDao environmentVariableBindingDao

	@Inject
	TestAutomationServerDao serverDao

	@Inject
	CustomEnvironmentVariableBindingDao customEnvironmentVariableBindingDao


	def "should create new server binding and then create new projects binding"() {
		given:
		Long environmentVariableId = -4L
		List<Long> environmentVariableIds = Arrays.asList(environmentVariableId)
		Long serverId = -1L
		Long projectIdLinkedToServer = -2L
		EnvironmentVariableBinding bindingToCheck = environmentVariableBindingDao.findByEntityIdTypeAndEvId(projectIdLinkedToServer,
			EVBindableEntity.PROJECT, environmentVariableId)

		when:
		environmentVariableBindingService.createNewBindings(serverId, EVBindableEntity.TEST_AUTOMATION_SERVER, environmentVariableIds)

		then:
		EnvironmentVariableBinding newBinding = environmentVariableBindingDao.findByEntityIdTypeAndEvId(serverId,
			EVBindableEntity.TEST_AUTOMATION_SERVER, environmentVariableId)
		newBinding.entityId == -1L
		newBinding.environmentVariable.id == environmentVariableId

		EnvironmentVariableBinding newProjectBinding = environmentVariableBindingDao.findByEntityIdTypeAndEvId(projectIdLinkedToServer,
			EVBindableEntity.PROJECT, environmentVariableId)
		bindingToCheck == null
		newProjectBinding.environmentVariable.id == environmentVariableId
	}

	def "should create new project binding"() {
		given:
		List<Long> environmentVariableIds = Arrays.asList(-2L, -3L)
		Long projectId = -3L

		when:
		environmentVariableBindingService.createNewBindings(projectId, EVBindableEntity.PROJECT, environmentVariableIds)

		then:
		List<EnvironmentVariableBinding> createdBindings = environmentVariableBindingDao.findAllByEntityIdAndEntityType(projectId, EVBindableEntity.PROJECT)
		createdBindings.size() == 2
		createdBindings.each {binding -> environmentVariableIds.contains(binding.environmentVariable.id) }
	}

	def "should fail when create new binding"() {
		given:
		List<Long> environmentVariableIds = Arrays.asList(-1L)

		when:
		environmentVariableBindingService.createNewBindings(-1L, EVBindableEntity.TEST_AUTOMATION_SERVER, environmentVariableIds)

		then:
		thrown EntityExistsException
	}

	def "should unbind environment variable"() {
		given:
		Long serverId = -1L
		List<Long> environmentVariableIds = Arrays.asList(-1L)

		when:
		environmentVariableBindingService.unbind(serverId, EVBindableEntity.TEST_AUTOMATION_SERVER, environmentVariableIds)

		then:
		EnvironmentVariableBinding deletedBinding = environmentVariableBindingDao.findByEntityIdTypeAndEvId(serverId, EVBindableEntity.TEST_AUTOMATION_SERVER, -1L)
		deletedBinding == null
	}

	def "should unbind environment variable by evId"() {
		given:
		Long environmentVariableId = -1L
		List<Long> bindingIdsToDelete = environmentVariableBindingDao.findAllBindingIdsByEnvironmentVariableId(environmentVariableId)

		when:
		environmentVariableBindingService.unbindByEnvironmentVariableId(environmentVariableId)

		then:
		List<Long> result = environmentVariableBindingDao.findAllBindingIdsByEnvironmentVariableId(environmentVariableId)
		!bindingIdsToDelete.isEmpty()
		result.isEmpty()
	}

	def "should fetch project default environment variable value"() {
		given:
		Long projectId = -1L

		when:
		def result = environmentVariableBindingService.getBoundEnvironmentVariablesByEntity(projectId, EVBindableEntity.PROJECT)

		then:
		result.size() == 1
		def projectValueBoundEnvironmentVariableDto = result.find { it -> (it.id == -3L) }
		projectValueBoundEnvironmentVariableDto.value == "project value"
	}


}
