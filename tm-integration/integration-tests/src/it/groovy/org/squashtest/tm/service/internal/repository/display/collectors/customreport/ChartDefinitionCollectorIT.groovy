/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors.customreport

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.display.grid.DataRow
import org.squashtest.tm.service.internal.repository.display.impl.collectors.customreports.ChartDefinitionCollector
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet()
class ChartDefinitionCollectorIT extends DbunitDaoSpecification {

	@Inject
	ChartDefinitionCollector chartDefinitionCollector

	@Unroll
	def "Should collect chart definition"() {
		when:
		def nodes = chartDefinitionCollector.collect(ids)

		then:
		nodes.size() == expectedNodes.size();
		for (id in ids) {
			def expectedNode = expectedNodes.get(id);
			def actualNode = nodes.get(id)

			assert expectedNode.id == actualNode.id;
			Map<String, Object> expectedData = expectedNode.data;
			def actualData = actualNode.data
			assert actualData.size() == expectedData.size()
			expectedData.forEach { key, expectedValue ->
				def actualValue = actualData.get(key)
				assert actualValue == expectedValue;
			}
		}

		where:
		ids        || expectedNodes
		null       || []
		[]         || []
		[-4L, -6L] || [
			(-4L): [id       : "ChartDefinition--4",
					state    : DataRow.State.leaf,
					projectId: -1L,
					data     : [
						CHART_ID : -1L,
						CRLN_ID  : -4L,
						NAME     : "chart",
						projectId: -1L,
						BOUND_TO_BLOCKING_MILESTONE: false,
					]
			],
			(-6L): [id       : "ChartDefinition--6",
					projectId: -1L,
					state    : DataRow.State.leaf,
					data     : [
						CHART_ID : -2L,
						CRLN_ID  : -6L,
						NAME     : "chart in folder",
						projectId: -1L,
						BOUND_TO_BLOCKING_MILESTONE: false,
					]
			],

		]
	}
}
