/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion

import org.jooq.DSLContext
import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.bugtracker.Issue
import org.squashtest.tm.domain.bugtracker.IssueList
import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStep
import org.squashtest.tm.domain.execution.ExploratoryExecution
import org.squashtest.tm.domain.execution.ExploratoryExecutionEvent
import org.squashtest.tm.domain.execution.SessionNote
import org.squashtest.tm.service.internal.deletion.jdbc.JdbcCampaignDeletionHandlerFactory
import org.squashtest.tm.service.internal.deletion.jdbc.JdbcIterationDeletionHandlerFactory
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
class JdbcCampaignNodeDeletionHandlerIT extends DbunitServiceSpecification {

	@Inject
	private JdbcIterationDeletionHandlerFactory iterationDeletionHandlerFactory

    @Inject JdbcCampaignDeletionHandlerFactory campaignDeletionHandlerFactory

	@Inject
	private DSLContext dslContext

	@DataSet
	def "Should delete iterations and all contents"() {
		when:
		def deletionHandler = iterationDeletionHandlerFactory.build([-11L])
		deletionHandler.deleteIterations()

		then:

		em.find(DenormalizedFieldValue.class, -1L) == null
		em.find(CustomFieldValue.class, -113L) == null
		em.find(CustomFieldValue.class, -1001L) == null

		em.find(IssueList.class, -1L) == null
		em.find(Issue.class, -1L) == null

		em.find(ExecutionStep.class, -1L) == null
		em.find(Execution.class, -1L) == null

		em.find(DenormalizedFieldValue.class, -10L) == null

		em.find(IterationTestPlanItem.class, -1L) == null
		em.find(IterationTestPlanItem.class, -2L) == null

		// stuff that should be preserved aka not in deleted iterations
		em.find(DenormalizedFieldValue.class, -3L) != null
		em.find(Campaign.class, -1L) != null
		em.find(CustomFieldValue.class, -123L) != null
		em.find(CustomFieldValue.class, -1002L) != null

		em.find(IterationTestPlanItem.class, -3L) != null
		em.find(IterationTestPlanItem.class, -4L) != null

	}

    @DataSet
    def "Should delete a campaign that includes an iteration with an exploratory execution"() {
        when:
        def campaignDeletionHandler = campaignDeletionHandlerFactory.build([-2L])
        campaignDeletionHandler.delete()
        then:

        em.find(IterationTestPlanItem.class, -5L) == null
        em.find(SessionNote.class, -1L) == null
        em.find(ExploratoryExecution.class, -5L) == null
        em.find(ExploratoryExecutionEvent.class, -1L) == null
        em.find(ExploratorySessionOverview.class, -1L) == null
    }


    @DataSet
    def "Should delete an iteration with an exploratory execution"() {
        when:
        def deletionHandler = iterationDeletionHandlerFactory.build([-14L])
        deletionHandler.deleteIterations()

        then:

        em.find(IterationTestPlanItem.class, -5L) == null
        em.find(SessionNote.class, -1L) == null
        em.find(ExploratoryExecution.class, -5L) == null
        em.find(ExploratoryExecutionEvent.class, -1L) == null
        em.find(ExploratorySessionOverview.class, -1L) == null
    }
}
