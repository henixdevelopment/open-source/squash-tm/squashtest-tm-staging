/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.scmserver

import org.springframework.context.MessageSource
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.core.scm.api.exception.ScmNoCredentialsException
import org.squashtest.tm.service.spi.ScmConnector;
import org.squashtest.tm.domain.scm.ScmRepository
import org.squashtest.tm.domain.servers.AuthenticationProtocol
import org.squashtest.tm.domain.servers.Credentials
import org.squashtest.tm.exception.scm.NameAndBranchAlreadyInUseException
import org.squashtest.tm.service.internal.display.dto.ScmRepositoryDto
import org.squashtest.tm.service.internal.scmserver.ScmConnectorRegistry
import org.squashtest.tm.service.servers.CredentialsProvider
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@Transactional
@UnitilsSupport
class ScmRepositoryManagerServiceIT extends DbunitServiceSpecification {

	@Inject
	ScmRepositoryManagerService scmRepositoryManagerService

	private MessageSource i18nHelper = Mock()

	private CredentialsProvider credentialsProvider = Mock()
	private ScmConnectorRegistry scmRegistry = Mock()
	private ScmRepositoryFilesystemService scmRepositoryFileSystemService = Mock()

	def setup() {
		scmRepositoryManagerService.i18nHelper = i18nHelper
		scmRepositoryManagerService.credentialsProvider = credentialsProvider
		scmRepositoryManagerService.scmRegistry = scmRegistry
		scmRepositoryManagerService.scmRepositoryFileSystemService = scmRepositoryFileSystemService
	}

	@DataSet
	def "#createNewScmRepository(long, ScmRepository, boolean) - [Nominal] should create a new scm repository and clone it"() {
		given:
			Credentials credentials = Mock(Credentials)
			credentials.getImplementedProtocol() >> AuthenticationProtocol.BASIC_AUTH
			Optional<Credentials> optionalCredentials = Optional.of(credentials)
		and:
			ScmConnector scmConnector = Mock()
			1 * scmConnector.supports(_) >> true
		when:
			scmRepositoryManagerService.createNewScmRepository(-1, "myPublicRepo", "development", "/src/resources/squash/", true)
		then:
			1 * credentialsProvider.getAppLevelCredentials(_) >> optionalCredentials
			1 * scmRegistry.createConnector(_) >> scmConnector
			1 * scmConnector.createRepository(credentials)
			1 * scmConnector.prepareRepository(credentials)
			1 * scmRepositoryFileSystemService.createWorkingFolderIfAbsent(_)
		and:
			def allScmRepositories = findAll("ScmRepository")
			allScmRepositories.size() == 5
			allScmRepositories.any {
				it.name == "myPublicRepo"
				it.repositoryPath == "/myTestRepoAtRoot"
				it.workingFolderPath == "/src/resources/squash/"
				it.workingBranch == "development"
			}
	}

	@DataSet
	def "#createNewScmRepository(long, ScmRepository, boolean) - [Nominal] should create a new scm repository with an existing name but different branch and clone it"() {
		given:
			Credentials credentials = Mock(Credentials)
			credentials.getImplementedProtocol() >> AuthenticationProtocol.BASIC_AUTH
			Optional<Credentials> optionalCredentials = Optional.of(credentials)
		and:
			ScmConnector scmConnector = Mock()
			1 * scmConnector.supports(_) >> true
		when:
			scmRepositoryManagerService.createNewScmRepository(-1, "FirstRepository", "development", "/src/resources/squash/", true)
		then:
			1 * credentialsProvider.getAppLevelCredentials(_) >> optionalCredentials
			1 * scmRegistry.createConnector(_) >> scmConnector
			1 * scmConnector.createRepository(credentials)
			1 * scmConnector.prepareRepository(credentials)
			1 * scmRepositoryFileSystemService.createWorkingFolderIfAbsent(_)
		and:
			def allScmRepositories = findAll("ScmRepository")
			allScmRepositories.size() == 5
			allScmRepositories.findAll { it.name == "FirstRepository" && it.scmServer.id == -1L }.size() == 2
			allScmRepositories.any {
				it.name == "FirstRepository"
				it.repositoryPath == "/myTestRepoAtRoot"
				it.workingFolderPath == "/src/resources/squash/"
				it.workingBranch == "development"
			}
	}

	@DataSet
	def "#createNewScmRepository(long, ScmRepository, boolean) - [Nominal] should create a new scm repository and NOT clone it"() {
		when:
			scmRepositoryManagerService.createNewScmRepository(-1, "myPublicRepo", "development", "/src/resources/squash/", false)
		then:
			def allScmRepositories = findAll("ScmRepository")
			allScmRepositories.size() == 5
			allScmRepositories.any {
				it.name == "myPublicRepo"
				it.repositoryPath == "/myTestRepoAtRoot"
				it.workingFolderPath == "/src/resources/squash/"
				it.workingBranch == "development"
			}
	}

	@DataSet
	def "#createNewScmRepository(long, ScmRepository, boolean) - [Exception] should try to create a new scm repository and throw an error because name and branch are already used"() {
		when:
			scmRepositoryManagerService.createNewScmRepository(-1, "FirstRepository", "master", "/src/resources/squash/", true)
		then:
			thrown NameAndBranchAlreadyInUseException
	}

	@DataSet
	def "#createNewScmRepository(long, ScmRepository, boolean) - [Exception] should try to create a new scm repository and clone it without credentials and throw an exception"() {
		when:
			scmRepositoryManagerService.createNewScmRepository(-1, "myPublicRepo", "development", "/src/resources/squash/", true)
		then:
			1 * credentialsProvider.getAppLevelCredentials(_) >> Optional.empty()
			1 * i18nHelper.getMessage(_, _, _) >> "No Credentials !"
		and:
			thrown ScmNoCredentialsException
	}

	@DataSet
	def "#deleteScmRepositories(Collection<Long>) - should delete scm repositories after unbinding them"() {
		given:
			def scmRepoIds = [-11L, -27L]
			scmRepoIds.each {
				assert found(ScmRepository.class, it)
			}
		when:
			scmRepositoryManagerService.deleteScmRepositories(scmRepoIds)
			flushAndClear()
		then:
			scmRepoIds.each {
				assert !found(ScmRepository.class, it)
			}
	}

	@DataSet
	def "#getAllDeclaredScmRepositories(Locale) - Should get all scm repositories formatted into ScmRepositoryDto"() {
		given:
			Locale locale = new Locale("fr")
			i18nHelper.getMessage("label.None",_,_) >> "Aucun"
		when:
			List<ScmRepositoryDto> result = scmRepositoryManagerService.getAllDeclaredScmRepositories(locale)
		then:
			result != null
			result.size() == 5
			result.any {it.id == 0L && it.friendlyUrl == "Aucun" }
			result.any {it.id == -11L && it.friendlyUrl == "http://github.com/toto/FirstRepository (master)" }
			result.any {it.id == -27L && it.friendlyUrl == "http://bitbucket.com/toto/SecondRepository (master)" }
			result.any {it.id == -87L && it.friendlyUrl == "http://bitbucket.com/toto/SquashAutomRepository (main)" }
			result.any {it.id == -102 && it.friendlyUrl == "http://bitbucket.com/toto/ThirdRepository (main)" }
	}

	@DataSet
	def "#findClonedByScmServerOrderByName(long) - should get all cloned scm repositories in the given scm server ordered by name"() {
		when:
			List<ScmRepository> result = scmRepositoryManagerService.findClonedByScmServerOrderByName(-2L)
		then:
			result != null
			result.size() == 2
			def repo1 = result[0]
			repo1.id == -27L && repo1.name == "SecondRepository" && repo1.workingBranch == "master"
			def repo2 = result[1]
			repo2.id == -102L && repo2.name == "ThirdRepository" && repo2.workingBranch == "main"
	}

}
