/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport
import javax.inject.Inject
import javax.persistence.EntityManager
import org.apache.commons.io.IOUtils
import org.squashtest.tm.service.internal.repository.AttachmentDao
import org.squashtest.tm.service.internal.dto.AutomatedExecutionUpdateData

import org.squashtest.tm.service.testautomation.resultpublisher.ExecutionService
import org.squashtest.tm.service.internal.repository.ExecutionDao
import org.squashtest.tm.service.testautomation.resultpublisher.AutomatedSuitePublisherService
import org.squashtest.tm.service.testautomation.AutomatedExecutionManagerService
import org.squashtest.tm.service.execution.automatedexecution.AutomatedExecutionFailureDetailService
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao
import org.squashtest.tm.service.plugin.PluginFinderService
import org.squashtest.tm.service.internal.repository.FailureDetailDao
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState
import org.squashtest.tm.service.testautomation.model.TfTestExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.service.testautomation.model.Attachment

@DataSet
@UnitilsSupport
@Transactional
class ExecutionServiceIT extends DbunitServiceSpecification {

    ExecutionService service

    @Inject
    ExecutionDao executionDao

    AutomatedSuitePublisherService automatedSuitePublisherService = Mock()

    AutomatedExecutionManagerService automatedExecutionManager  = Mock()

    AutomatedExecutionFailureDetailService failureDetailService  = Mock()

    @Inject
    AutomatedExecutionExtenderDao automatedExecutionExtenderDao

    PluginFinderService pluginFinderService  = Mock()

    @Inject
    FailureDetailDao failureDetailDao

    EntityManager entityManager = Mock()

    @Inject
    AttachmentDao attachmentDao

    def "setup"() {
        service = new ExecutionService(
            executionDao : executionDao,
            automatedSuitePublisherService : automatedSuitePublisherService,
            automatedExecutionManager : automatedExecutionManager,
            failureDetailService : failureDetailService,
            automatedExecutionExtenderDao : automatedExecutionExtenderDao,
            pluginFinderService : pluginFinderService,
            failureDetailDao : failureDetailDao,
            entityManager : entityManager
        )
    }

    def "Should not update failure details if no failure message in state change"() {
        given:
            def stateChange1 = createStateChange([])
            def stateChange2 = createStateChange(null)
            def updater = createUpdater()

        when:
            service.updateExecutionFailureDetails(stateChange1, updater)
            service.updateExecutionFailureDetails(stateChange2, updater)

        then:
            0 * failureDetailService.upsertFailureDetail(_,_,_,_)
    }

    def "Should update failure details if premium"() {
        given:
            def stateChange = createStateChange(["erreur : 2 != 3", "could not divide by 0", "error error"])
            def updater = createUpdater()

        when:
            service.updateExecutionFailureDetails(stateChange, updater)

        then:
            3 * failureDetailService.upsertFailureDetail(_,_,_,_)
    }

    private AutomatedExecutionState createStateChange(ArrayList<String> failureDetailList) {
        def testExecutionStatus = new TfTestExecutionStatus(
            new Date(),
            new Date(),
            ExecutionStatus.FAILURE,
            1245,
            "result url",
            failureDetailList
        )

        def outputAttachment = new Attachment(
            "output.xml",
            IOUtils.toInputStream("output.xml report content")
        )

        return new AutomatedExecutionState(
            testExecutionStatus,
            null,
            ["robotframework"],
            null,
            [outputAttachment],
            "Robot Framework",
            true
        )
    }

    private AutomatedExecutionUpdateData createUpdater() {
        return new AutomatedExecutionUpdateData(-1L, -456L, -748L)
    }


}
