/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.internal.repository.MilestoneDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@Transactional
@UnitilsSupport
class MilestoneDaoIT extends DbunitDaoSpecification {

	@Inject
	MilestoneDao milestoneDao

	def "isTestStepBoundToBlockingMilestone(long) - Should not find any milestone blocking the test step modification"() {
		expect:
		!milestoneDao.isTestStepBoundToBlockingMilestone(-1L)
	}
	/* The TestStep belongs to a TestCase bound to a Planned|Locked Milestone */
	def "isTestStepBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking the test step modification"() {
		expect:
		milestoneDao.isTestStepBoundToBlockingMilestone(-2L)
	}
	/* The TestStep belongs to a TestCase verifying a RequirementVersion bound to a Planned|Locked Milestone */
	/* From TM 2.0, this is no longer a blocking situation (see SQUASH-1929) */
	def "isTestStepBoundToBlockingMilestone(long) - Should find a locked milestone indirectly blocking the test step modification"() {
		expect:
		!milestoneDao.isTestStepBoundToBlockingMilestone(-3L)
	}

	def "isParameterBoundToBlockingMilestone(long) - Should not find any milestone blocking the parameter modification"() {
		expect:
		!milestoneDao.isParameterBoundToBlockingMilestone(-1L)
	}
	/* The Parameter belongs to a TestCase bound to a Planned|Locked Milestone */
	def "isParameterBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking the parameter modification"() {
		expect:
		milestoneDao.isParameterBoundToBlockingMilestone(-2L)
	}
	/* The Parameter belongs to a TestCase verifying a RequirementVersion bound to a Planned|Locked Milestone */
	/* From TM 2.0, this is no longer a blocking situation (see SQUASH-1929) */
	def "isParameterBoundToBlockingMilestone(long) - Should find a locked milestone indirectly blocking the parameter modification"() {
		expect:
		!milestoneDao.isParameterBoundToBlockingMilestone(-3L)
	}

	def "isDatasetBoundToBlockingMilestone(long) - Should not find any milestone blocking the dataset modification"() {
		expect:
		!milestoneDao.isDatasetBoundToBlockingMilestone(-1L)
	}
	/* The Dataset belongs to a TestCase bound to a Planned|Locked Milestone */
	def "isDatasetBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking the dataset modification"() {
		expect:
		milestoneDao.isDatasetBoundToBlockingMilestone(-2L)
	}
	/* The Dataset belongs to a TestCase verifying a RequirementVersion bound to a Planned|Locked Milestone */
	/* From TM 2.0, this is no longer a blocking situation (see SQUASH-1929) */
	def "isDatasetBoundToBlockingMilestone(long) - Should find a locked milestone indirectly blocking the dataset modification"() {
		expect:
		!milestoneDao.isDatasetBoundToBlockingMilestone(-3L)
	}

	def "isDatasetParamValueBoundToBlockingMilestone(long) - Should not find any milestone blocking the dataset parameter value modification"() {
		expect:
		!milestoneDao.isDatasetParamValueBoundToBlockingMilestone(-1L)
	}
	/* The DatasetParamValue belongs to a TestCase bound to a Planned|Locked Milestone */
	def "isDatasetParamValueBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking the dataset parameter value modification"() {
		expect:
		milestoneDao.isDatasetParamValueBoundToBlockingMilestone(-2L)
	}
	/* The DatasetParamValue belongs to a TestCase verifying a RequirementVersion bound to a Planned|Locked Milestone */
	/* From TM 2.0, this is no longer a blocking situation (see SQUASH-1929) */
	def "isDatasetParamValueBoundToBlockingMilestone(long) - Should find a locked milestone indirectly blocking the dataset parameter value modification"() {
		expect:
		!milestoneDao.isDatasetParamValueBoundToBlockingMilestone(-3L)
	}

	def "isAttachmentListBoundToBlockingMilestone(long) - Should not find any milestone blocking a test case attachment list modification"() {
		expect:
		!milestoneDao.isAttachmentListBoundToBlockingMilestone(-1L)
	}
	/* The AttachmentList belongs to a TestCase bound to a Planned|Locked Milestone */
	def "isAttachmentListBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking a test case attachment list modification"() {
		expect:
		milestoneDao.isAttachmentListBoundToBlockingMilestone(-2L)
	}
	/*
	 * The AttachmentList belongs to a TestCase verifying a RequirementVersion bound to a Planned|Locked Milestone
	 * From TM 2.0, test cases don't inherit blocking milestones from their verified RV (SQUASH-1929)
	 */
	def "isAttachmentListBoundToBlockingMilestone(long) - Should find a locked milestone indirectly blocking a test case attachment list modification"() {
		expect:
		! milestoneDao.isAttachmentListBoundToBlockingMilestone(-3L)
	}

	def "isAttachmentListBoundToBlockingMilestone(long) - Should not find any milestone blocking a requirement version attachment list modification"() {
		expect:
		!milestoneDao.isAttachmentListBoundToBlockingMilestone(-4L)
	}
	/* The AttachmentList belongs to a RequirementVersion bound to a Planned|Locked Milestone */
	def "isAttachmentListBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking a requirement version attachment list modification"() {
		expect:
		milestoneDao.isAttachmentListBoundToBlockingMilestone(-5L)
	}

	def "isAttachmentListBoundToBlockingMilestone(long) - Should not find any milestone blocking a campaign attachment list modification"() {
		expect:
		!milestoneDao.isAttachmentListBoundToBlockingMilestone(-6L)
	}
	/* The AttachmentList belongs to a Campaign bound to a Planned|Locked Milestone */
	def "isAttachmentListBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking a campaign attachment list modification"() {
		expect:
		milestoneDao.isAttachmentListBoundToBlockingMilestone(-7L)
	}

	def "isAttachmentBoundToBlockingMilestone(long) - Should not find any milestone blocking a test case attachment modification"() {
		expect:
		!milestoneDao.isAttachmentBoundToBlockingMilestone(-1L)
	}
	/* The Attachment belongs to a TestCase bound to a Planned|Locked Milestone */
	def "isAttachmentBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking a test case attachment modification"() {
		expect:
		milestoneDao.isAttachmentBoundToBlockingMilestone(-2L)
	}
	/*
	 * The Attachment belongs to a TestCase verifying a RequirementVersion bound to a Planned|Locked Milestone
	 * From TM 2.0, test cases don't inherit blocking milestones from their verified RV (SQUASH-1929). Same apply for
	 * TC attachments.
	 */
	def "isAttachmentBoundToBlockingMilestone(long) - Should find a locked milestone indirectly blocking a test case attachment modification"() {
		expect:
		!milestoneDao.isAttachmentBoundToBlockingMilestone(-3L)
	}

	def "isAttachmentBoundToBlockingMilestone(long) - Should not find any milestone blocking a requirement version attachment modification"() {
		expect:
		!milestoneDao.isAttachmentBoundToBlockingMilestone(-4L)
	}
	/* The Attachment belongs to a RequirementVersion bound to a Planned|Locked Milestone */
	def "isAttachmentBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking a requirement version attachment modification"() {
		expect:
		milestoneDao.isAttachmentBoundToBlockingMilestone(-5L)
	}

	def "isAttachmentBoundToBlockingMilestone(long) - Should not find any milestone blocking a campaign attachment modification"() {
		expect:
		!milestoneDao.isAttachmentBoundToBlockingMilestone(-6L)
	}
	/* The Attachment belongs to a Campaign bound to a Planned|Locked Milestone */
	def "isAttachmentBoundToBlockingMilestone(long) - Should find a locked milestone directly blocking a campaign attachment modification"() {
		expect:
		milestoneDao.isAttachmentBoundToBlockingMilestone(-7L)
	}

	def "isRequirementVersionBoundToBlockingMilestone(long) - Should not find any milestone blocking a requirement version modification"() {
		expect:
		!milestoneDao.isRequirementVersionBoundToBlockingMilestone(-2L)
	}
	def "isRequirementVersionBoundToBlockingMilestone(long) - Should find a locked milestone blocking a requirement version modification"() {
		expect:
		milestoneDao.isRequirementVersionBoundToBlockingMilestone(-1L)
	}

	def "isRequirementBoundToBlockingMilestone(long) - Should not find any milestone blocking a requirement modification"() {
		given:
		setupRequirementsAndVersions()
		expect:
		!milestoneDao.isRequirementBoundToBlockingMilestone(-2L)
	}
	def "isRequirementBoundToBlockingMilestone(long) - Should find a locked milestone blocking a requirement modification"() {
		given:
		setupRequirementsAndVersions()
		expect:
		milestoneDao.isRequirementBoundToBlockingMilestone(-1L)
	}

	def "areRequirementsBoundToBlockingMilestone(Collection<long>) - Should not find any milestone blocking requirements modification"() {
		given:
		setupRequirementsAndVersions()
		expect:
		!milestoneDao.areRequirementsBoundToBlockingMilestone([-2L])
	}
	def "areRequirementsBoundToBlockingMilestone(Collection<long>) - Should find a locked milestone blocking requirements modification"() {
		given:
		setupRequirementsAndVersions()
		expect:
		milestoneDao.areRequirementsBoundToBlockingMilestone([-1L, -2L])
	}

	def linkRequirementAndVersion(requirementId, versionId) {
		Requirement requirement = findEntity(Requirement.class, requirementId)
		RequirementVersion version = findEntity(RequirementVersion.class, versionId)
		version.setRequirement(requirement)
		requirement.setCurrentVersion(version)
	}

	def setupRequirementsAndVersions() {
		linkRequirementAndVersion(-1L, -1L)
		linkRequirementAndVersion(-2L, -2L)
	}

}
