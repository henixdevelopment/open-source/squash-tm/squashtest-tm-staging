/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.artificialintelligence.server

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class AiServerDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private AiServerDisplayService aiServerService

	def "should fetch an ai server grid"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.aiServerService.findAll(gridRequest)

		then:
		gridResponse.count == 3
		def rows = gridResponse.dataRows

		def aiserver1 = rows.get(0)
		aiserver1.id == "-1"
		aiserver1.data.get("serverId") == -1
		aiserver1.data.get("name") == "Mixstral"
		aiserver1.data.get("url") == "http://easteregg.com"
		aiserver1.data.get("description") == "La pizza ananas c'est pas fou-fou."

		def aiserver2 = rows.get(1)
		aiserver2.id == "-2"
		aiserver2.data.get("serverId") == -2
		aiserver2.data.get("name") == "OpenAi 1"
		aiserver2.data.get("url") == "https://openai.com"

		def aiserver3 = rows.get(2)
		aiserver3.id == "-3"
		aiserver3.data.get("serverId") == -3
		aiserver3.data.get("name") == "OpenAi 2"
		aiserver3.data.get("url") == "https://openai.com"
		aiserver3.data.get("description") == "Le poisson, LE PETIT POISSON!"
	}
}
