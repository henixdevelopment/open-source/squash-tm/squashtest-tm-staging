/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.bugtracker

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class BugTrackerDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private BugTrackerDisplayService bugTrackerDisplayService

	def "should fetch a bugtracker grid"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.bugTrackerDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 3
		def rows = gridResponse.dataRows

		def bt01 = rows.get(0)
		bt01.id == "-1"
		bt01.data.get("serverId") == -1
		bt01.data.get("name") == "BT1"
		bt01.data.get("kind") == "jira"
		bt01.data.get("url") == "http://mon-bt.org/BT1"
		bt01.data.get("synchronisationCount") == 2

		def bt02 = rows.get(1)
		bt02.id == "-2"
		bt02.data.get("serverId") == -2
		bt02.data.get("name") == "BT2"
		bt02.data.get("kind") == "jira.cloud"
		bt02.data.get("url") == "127.0.0.1/BT2"
		bt02.data.get("synchronisationCount") == 0
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.bugTrackerDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["name",
				   "kind",
				   "url"]
	}
}
