/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.testcase

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.dto.testcase.ActionTestStepDto
import org.squashtest.tm.service.internal.display.dto.testcase.CalledTestStepDto
import org.squashtest.tm.service.internal.display.dto.testcase.CalledTestCaseDto
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseDto
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.service.internal.display.dto.testcase.ActionTestStepDto.ACTION_STEP
import static org.squashtest.tm.service.internal.display.dto.testcase.CalledTestStepDto.CALL_STEP

@UnitilsSupport
@Transactional
@DataSet
class TestCaseDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private TestCaseDisplayService service;

	def "should fetch test case steps"() {

		given:
		long testCaseId = -1L;

		when:
		TestCaseDto testCase = this.service.getTestCaseView(testCaseId)

		then:
		testCase.id == -1L

		def steps = testCase.testSteps
		steps.size() == 5

		ActionTestStepDto firstStep = steps.get(0) as ActionTestStepDto
		firstStep.id == -1L
		firstStep.kind == ACTION_STEP
		firstStep.action == "Step - 1"
		firstStep.expectedResult == "Result - 1"
		firstStep.getAttachmentListId() == -4L
		def attachmentIds = new HashSet(firstStep.getAttachmentList().getAttachments().collect { it.id })
		attachmentIds == [-1L, -2L, -3L] as Set
		def cufValueIds = new HashSet(firstStep.getCustomFieldValues().collect { it.id })
		cufValueIds == [-1L, -2L, -3L] as Set

		CalledTestStepDto secondStep = steps.get(1) as CalledTestStepDto
		secondStep.kind == CALL_STEP
		secondStep.calledTcId == -2L
		secondStep.calledTcName == "test-case-2"
		secondStep.calledDatasetId == null
		secondStep.delegateParam
		secondStep.calledTestCaseSteps.size() == 3
		def firstCalledStep = secondStep.calledTestCaseSteps.get(0) as ActionTestStepDto
		firstCalledStep.id == -5L
		firstCalledStep.action == "TC-2:Step-1"
		firstCalledStep.expectedResult == "TC-2:Result-1"

		def secondCalledStep = secondStep.calledTestCaseSteps.get(1) as ActionTestStepDto
		secondCalledStep.id == -6L
		secondCalledStep.action == "TC-2:Step-2"
		secondCalledStep.expectedResult == "TC-2:Result-2"

		def thirdCalledStep = secondStep.calledTestCaseSteps.get(2) as CalledTestStepDto
		thirdCalledStep.calledTcId == -3L

		ActionTestStepDto thirdStep = steps.get(2) as ActionTestStepDto
		thirdStep.id == -4L
		thirdStep.kind == ACTION_STEP
		thirdStep.action == "Step - 3"
		thirdStep.expectedResult == "Result - 3"
		def thirdStepAttachmentIds = new HashSet(thirdStep.getAttachmentList().getAttachments().collect { it.id })
		thirdStepAttachmentIds == [-4, -5] as Set
		def thirdStepCufValueIds = new HashSet(thirdStep.getCustomFieldValues().collect { it.id })
		thirdStepCufValueIds == [-4] as Set

		ActionTestStepDto forthStep = steps.get(3) as ActionTestStepDto
		forthStep.id == -3L
		forthStep.kind == ACTION_STEP
		forthStep.action == "Step - 4"
		forthStep.expectedResult == "Result - 4"

		CalledTestStepDto fifthStep = steps.get(4) as CalledTestStepDto
		fifthStep.calledTcName == "test-case-4"
		fifthStep.calledTcId == -4L
		def forthCalledStep = fifthStep.calledTestCaseSteps.get(0) as ActionTestStepDto
		forthCalledStep.id == -10L
		forthCalledStep.action == "TC-4:Step-1"
		forthCalledStep.expectedResult == "TC-4:Result-1"

		def fifthCalledStep = fifthStep.calledTestCaseSteps.get(1) as ActionTestStepDto
		fifthCalledStep.id == -11L
		fifthCalledStep.action == "TC-4:Step-2"
		fifthCalledStep.expectedResult == "TC-4:Result-2"
	}

	def "should fetch calling test cases"() {
		given:
		long testCaseId = -2L

		when:
		TestCaseDto testCaseDto = this.service.getTestCaseView(testCaseId)

		then:

		testCaseDto.getId() == -2L

		List<CalledTestCaseDto> callings = testCaseDto.calledTestCases

		!callings.isEmpty()
		callings.size() == 1
		callings[0].getId() == -1L
		callings[0].getName() == "test-case-1"
		callings[0].getStepOrder() == 1
	}

}
