/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.campaign


import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.config.EnabledAclSpecConfig
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
@ContextHierarchy([
	@ContextConfiguration(classes = [EnabledAclSpecConfig], name = "aclcontext", inheritLocations = false),
])
class CampaignDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private CampaignDisplayService campaignDisplayService


	def "should return ordered full names by given campaign library node ids"() {
		given:
        def nodeIds = [-1L, -10L, -11L]
        def projectIds = [-1L]

        when:
		def result = campaignDisplayService.retrieveFullNameByCampaignLibraryNodeIds(nodeIds, projectIds)

		then:
		result.size() == 3
		result == ["a folder", "bar", "squash - foo"]
	}

}
