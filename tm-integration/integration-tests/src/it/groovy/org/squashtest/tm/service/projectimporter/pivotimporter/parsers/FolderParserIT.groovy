/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter.parsers


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.projectimporter.PivotFormatImport
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata
import org.squashtest.tm.service.internal.dto.projectimporter.SquashCustomFieldInfo
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class FolderParserIT extends DbunitServiceSpecification {

    @Inject FolderParser folderParser

    def pivotImportMetadata = new PivotImportMetadata()
    def pivotFormatImport = new PivotFormatImport()


    def setup() {
        pivotImportMetadata.customFieldIdsMap.put("cuf1", new SquashCustomFieldInfo(1L, InputType.CHECKBOX))
        pivotImportMetadata.customFieldIdsMap.put("cuf2", new SquashCustomFieldInfo(2L, InputType.RICH_TEXT))
        pivotFormatImport.id = 1
    }

    def "should parse a test case folder"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/test_case_folder.json")

        when:
            def folderToImport = folderParser.parseFolder(jsonParser, JsonImportFile.TEST_CASE_FOLDERS, pivotImportMetadata,pivotFormatImport)

        then:
            folderToImport.internalId == "F2"
            folderToImport.folder.name == "folder 2"
            folderToImport.folder.description == "desc 2"
            folderToImport.parentType == EntityType.TEST_CASE_FOLDER
            folderToImport.parentId == "F1"
            folderToImport.folder.customFields.size() == 2
            folderToImport.folder.customFields.get(1L).value == "true"
            folderToImport.folder.customFields.get(2L).value == "my first value"
            folderToImport.attachments[0].zipImportFileName == "AT09-attachment.log"
            folderToImport.attachments[0].originalFileName == "attachment.log"
    }

    def "should parse a requirement folder"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/requirement_folder.json")

        when:
            def folderToImport = folderParser.parseFolder(jsonParser, JsonImportFile.REQUIREMENT_FOLDERS, pivotImportMetadata,pivotFormatImport)

        then:
            folderToImport.internalId == "F1"
            folderToImport.folder.name == "folder 1"
            folderToImport.folder.description == "desc 1"
            folderToImport.parentType == EntityType.REQUIREMENT_LIBRARY
            folderToImport.parentId == null
            folderToImport.folder.customFields.size() == 0
            folderToImport.attachments[0].zipImportFileName == "AT07-attachment.zip"
            folderToImport.attachments[0].originalFileName == "attachment.zip"
    }

    def "should parse a campaign folder"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/campaign_folder.json")

        when:
            def folderToImport = folderParser.parseFolder(jsonParser, JsonImportFile.CAMPAIGN_FOLDERS, pivotImportMetadata,pivotFormatImport)

        then:
            folderToImport.internalId == "FC1"
            folderToImport.folder.name == "Campaign folder 1"
            folderToImport.folder.description == "desc 1"
            folderToImport.parentType == EntityType.CAMPAIGN_LIBRARY
            folderToImport.parentId == null
            folderToImport.folder.customFields.size() == 0
            folderToImport.attachments[0].zipImportFileName == "AT03-attachment.odp"
            folderToImport.attachments[0].originalFileName == "attachment.odp"
    }

}
