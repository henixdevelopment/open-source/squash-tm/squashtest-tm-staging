/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter.parsers

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonParser

class JsonParserTestHelper {

    static JsonParser getJsonParserFromFilePath(String filePath) {
        def url = JsonParserTestHelper.getClassLoader().getResource(filePath)
        def jsonFile = new File(url.toURI())

        def jsonInputStream = new FileInputStream(jsonFile)

        def jsonFactory = new JsonFactory()
        def jsonParser = jsonFactory.createParser(jsonInputStream)

        // We do nextToken() to skip the START_OBJECT of json
        jsonParser.nextToken()
        return jsonParser
    }
}
