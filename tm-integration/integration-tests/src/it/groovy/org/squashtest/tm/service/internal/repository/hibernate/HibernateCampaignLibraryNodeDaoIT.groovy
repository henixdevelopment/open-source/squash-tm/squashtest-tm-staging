/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class HibernateCampaignLibraryNodeDaoIT extends DbunitDaoSpecification {

	@Inject
	HibernateCampaignLibraryNodeDao dao

    @DataSet("HibernateCampaignLibraryNodeDaoIT.long path.xml")
    def "should return the list of the parents names"(){
        when :
        def res = dao.getParentsName(-159L)

        then :
        res == ["folder 1", "folder 2"]
    }

    @DataSet("HibernateCampaignLibraryNodeDaoIT.long path.xml")
    def "should return the list of the parents ids"(){
        when :
        def res = dao.getParentsIds(-159L)

        then :
        res == [-85L, -159L]
    }

	@DataSet("HibernateCampaignLibraryNodeDaoIT.single sprintGroup.xml")
	def "should return the id of the existing sprint group at root"(){
        given:
        def paths = Collections.singletonList("/p1/ze sprint group")

		when :
			def ids = dao.findNodeIdsByPath(paths)

		then :
			ids.size() == 1
            ids.get(0) == -5
	}

    @DataSet("HibernateCampaignLibraryNodeDaoIT.single sprintGroup.xml")
    def "should return an empty list"() {
        given:
        def paths = Collections.singletonList("/p1/not existing node")

        when :
        def ids = dao.findNodeIdsByPath(paths)

        then :
        ids.isEmpty()
    }

    @DataSet("HibernateCampaignLibraryNodeDaoIT.long path.xml")
    def "should return existing nodes ids from path"(){
        given:
        def paths = Collections.singletonList("/p1/folder 1/folder 2/not existing node 1/note existing node 2")

        when :
        def ids = dao.findNodeIdsByPath(paths)

        then :
        ids.size() == 2
        ids.get(0) == -85
        ids.get(1) == -159
    }
}
