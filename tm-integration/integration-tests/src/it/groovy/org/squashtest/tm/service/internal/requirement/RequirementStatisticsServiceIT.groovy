/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.RequirementCriticality
import org.squashtest.tm.service.internal.repository.RequirementDao
import org.squashtest.tm.service.internal.repository.RequirementVersionDao
import org.squashtest.tm.service.requirement.HighLevelRequirementService
import org.squashtest.tm.service.requirement.RequirementStatisticsService
import org.squashtest.tm.service.statistics.requirement.RequirementBoundDescriptionStatistics
import org.squashtest.tm.service.statistics.requirement.RequirementBoundTestCasesStatistics
import org.squashtest.tm.service.statistics.requirement.RequirementCoverageStatistics
import org.squashtest.tm.service.statistics.requirement.RequirementCriticalityStatistics
import org.squashtest.tm.service.statistics.requirement.RequirementStatusesStatistics
import org.squashtest.tm.service.statistics.requirement.RequirementValidationStatistics
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
class RequirementStatisticsServiceIT extends DbunitServiceSpecification {

	@Inject
	private RequirementVersionDao requirementVersionDao

	@Inject
	private RequirementDao requirementDao

	@Inject
	private RequirementStatisticsService service;

	@Inject
	private HighLevelRequirementService highLevelRequirementService;

	def setup (){
		def ids = [-11L,-21L,-31,-51L,-61L,-71L,-91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L]
		ids.each {
			setBidirectionalReqReqVersion(it, it)
		}
		setBidirectionalReqReqVersion(-42L, -41L)
		setBidirectionalReqReqVersion(-83L, -81L)
	}

	def setBidirectionalReqReqVersion(Long reqVersionId, Long reqId) {
		def reqVer = requirementVersionDao.getReferenceById(reqVersionId)
		def req = requirementDao.findById(reqId)
		if (reqVer != null && req != null) reqVer.setRequirement(req)
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should count how many requirements are bound to 0 testCases, 1 testCase, or above"(){

		given :
			def reqIds = [-11L,-21L,-31L, -41L, -51L,-61L,-71L, -81L, -91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L,-152L]

		and :
			highLevelRequirementService.linkToHighLevelRequirement(-152L, -41L)

		when :
			RequirementBoundTestCasesStatistics stats = service.gatherBoundTestCaseStatistics(reqIds)

		then :
			stats.getZeroTestCases() == 1
			stats.getOneTestCase() == 2
			stats.getManyTestCases() == 15
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should count requirements sorted by status"(){

		given :
			def reqIds = [-11L,-21L,-31L, -41L, -51L,-61L,-71L, -81L, -91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L]

		when :
			RequirementStatusesStatistics stats = service.gatherRequirementStatusesStatistics(reqIds)

		then :
			stats.getWorkInProgress() == 3
			stats.getUnderReview() == 4
			stats.getApproved() == 8
			stats.getObsolete() == 2
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should count requirements sorted by criticality"(){

		given :
			def reqIds = [-11L,-21L,-31L, -41L, -51L,-61L,-71L, -81L, -91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L]

		when :
			RequirementCriticalityStatistics stats = service.gatherRequirementCriticalityStatistics(reqIds)

		then :
			stats.getUndefined() == 4
			stats.getMinor() == 6
			stats.getMajor() == 4
			stats.getCritical() == 3
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should count how many requirements have a description and how many haven't"(){

		given :
		def reqIds = [-11L,-21L,-31L, -41L, -51L,-61L,-71L, -81L, -91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L]

		when :
		RequirementBoundDescriptionStatistics stats = service.gatherRequirementBoundDescriptionStatistics(reqIds)

		then :
		stats.getHasDescription() == 13
		stats.getHasNoDescription() == 4
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should calculate the coverage rate sorted by criticality"(){

		given :
		def reqIds = [-11L,-21L,-31L, -41L, -51L,-61L,-71L, -81L, -91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L]

		when :
		RequirementCoverageStatistics stats = service.gatherRequirementCoverageStatistics(reqIds)

		then :
		stats.getUndefined() == 4
		stats.getTotalUndefined() == 4
		stats.getMinor() == 5
		stats.getTotalMinor() == 6
		stats.getMajor() == 4
		stats.getTotalMajor() == 4
		stats.getCritical() == 3
		stats.getTotalCritical() == 3
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should calculate the validation rate sorted by criticality"(){

		given :
		def reqIds = [-11L,-21L,-31L, -41L, -51L,-61L,-71L, -81L, -91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L]

		when :
		RequirementValidationStatistics stats = service.gatherRequirementValidationStatistics(reqIds)

		then :
		stats.getConclusiveUndefined() == 1
		stats.getInconclusiveUndefined() == 3
		stats.getUndefinedUndefined() == 0

		stats.getConclusiveMinor() == 2
		stats.getInconclusiveMinor() == 1
		stats.getUndefinedMinor() == 3

		stats.getConclusiveMajor() == 2
		stats.getInconclusiveMajor() == 1
		stats.getUndefinedMajor() == 1

		stats.getConclusiveCritical() == 0
		stats.getInconclusiveCritical() == 3
		stats.getUndefinedCritical() == 0

		!stats.isLegacy()
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should return the requirement ids from a click on a validation chart part"() {
		given:
			def reqIds = [-11L,-21L,-31L, -41L, -51L,-61L,-71L, -81L, -91L,-101L,-111L,-121L,-131L, -1311L, -1321L, -141L,-151L]
			def RequirementCriticality criticalitySearched = criticality
			def String validationStatusSearched = validationStatus
		when:
			List<Long> reqIdsFromValidation = service.gatherRequirementIdsFromValidation(reqIds, criticality, validationStatus);
			Set<Long> reqIdsSet = new HashSet<Long>(reqIdsFromValidation);
		then:
			reqIdsSet == expectedSet
		where:
			criticality 					 | validationStatus | expectedSet
			RequirementCriticality.UNDEFINED | ["SUCCESS"] 		| [-141l] as Set
			RequirementCriticality.UNDEFINED | ["FAILURE"] 		| [-121l, -131l, -151l] as Set

			RequirementCriticality.MINOR 	 | ["SUCCESS"] 		| [-81l, -1321l] as Set
			RequirementCriticality.MINOR 	 | ["FAILURE"] 		| [-111l] as Set

			RequirementCriticality.MAJOR 	 | ["SUCCESS"] 		| [-41l, -51l] as Set
			RequirementCriticality.MAJOR 	 | ["FAILURE"] 		| [-61l] as Set

			RequirementCriticality.CRITICAL  | ["SUCCESS"] 		| [] as Set
			RequirementCriticality.CRITICAL  | ["FAILURE"] 		| [-21l, -11l, -31l] as Set

			RequirementCriticality.UNDEFINED | ["READY", "RUNNING", "WARNING", "BLOCKED", "ERROR", "NOT_RUN", "NOT_FOUND", "SETTLED", "UNTESTABLE"] as Set| [] as Set
			RequirementCriticality.MINOR 	 | ["READY", "RUNNING", "WARNING", "BLOCKED", "ERROR", "NOT_RUN", "NOT_FOUND", "SETTLED", "UNTESTABLE"] as Set| [-91l, -101l, -1311l] as Set
			RequirementCriticality.MAJOR 	 | ["READY", "RUNNING", "WARNING", "BLOCKED", "ERROR", "NOT_RUN", "NOT_FOUND", "SETTLED", "UNTESTABLE"] as Set| [-71l] as Set
			RequirementCriticality.CRITICAL  | ["READY", "RUNNING", "WARNING", "BLOCKED", "ERROR", "NOT_RUN", "NOT_FOUND", "SETTLED", "UNTESTABLE"] as Set| [] as Set
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should return the correct statistic bundle"() {
		given :

		when :
		def stats = service.findSimplifiedCoverageStats(kind)

		then :
		stats.getRequirementStats().values().sort({it.reqId}).collect({it.redactionRate})  == expectedRedactionRates
		stats.getRequirementStats().values().sort({it.reqId}).collect({it.verificationRate})  == expectedVerifcationRates
		stats.getRequirementStats().values().sort({it.reqId}).collect({it.validationRate})  == expectedValidationRates

		where :
		kind      		    || expectedRedactionRates | expectedVerifcationRates | expectedValidationRates
		"bar"     		    ||[0.00d]                 |[0d]                      |[0d]
		"foo"     			||[50.00d]                |[100d]                    |[50d]
		"foobar"            ||[0.00d,33.33d,66.67d]   |[100.00d,50.00d,100.00d]  |[100d,50d,20d]
	}

	@DataSet("RequirementStatisticsServiceIT.xml")
	def "Should return the correct statistic bundle from requirement version"() {
		given:

		when:
		def stats = service.findCoveragesStatsByRequirementVersionId(-1321L)

		then:
		stats.total.allTestCaseCount == 1
		stats.total.executedTestCase == 1
		stats.total.plannedTestCase == 1
		stats.total.redactedTestCase == 0
		stats.total.validatedTestCases == 1
		stats.total.verifiedTestCase == 1
		stats.nonObsoleteDescendantsCount == 0
		stats.coveredDescendantsCount == 0
	}

	@DataSet("RequirementStatisticsServiceIT-Coverage.xml")
	def "Should compute coverage"() {
		given:
		[-1L,-11L,-12L,-13L].each {
			setBidirectionalReqReqVersion(it, it)
		}

		when:
		def stats = service.findCoveragesStatsByRequirementVersionId(-1L)

		then:
		stats.nonObsoleteDescendantsCount == 2
		stats.coveredDescendantsCount == 1
	}
}
