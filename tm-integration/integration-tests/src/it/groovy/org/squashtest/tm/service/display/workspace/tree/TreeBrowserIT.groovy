/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.workspace.tree

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.NodeReference
import org.squashtest.tm.domain.NodeType
import org.squashtest.tm.domain.NodeWorkspace
import org.squashtest.tm.domain.Workspace
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService
import org.squashtest.tm.service.internal.display.dto.testcase.ActionTestStepDto
import org.squashtest.tm.service.internal.display.dto.testcase.CalledTestCaseDto
import org.squashtest.tm.service.internal.display.dto.testcase.CalledTestStepDto
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseDto
import org.squashtest.tm.service.internal.display.grid.DataRow
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.NodeWorkspace.*
import static org.squashtest.tm.service.internal.display.dto.testcase.ActionTestStepDto.ACTION_STEP
import static org.squashtest.tm.service.internal.display.dto.testcase.CalledTestStepDto.CALL_STEP

@UnitilsSupport
@Transactional
@DataSet
class TreeBrowserIT extends DbunitServiceSpecification {

	@Inject
	private MultipleHierarchyTreeBrowser treeBrowser;

	def "should fetch some libraries"() {
		when:
		def tree = this.treeBrowser.getInitialTree(TEST_CASE, new HashSet<>(), new HashSet<>())

		then:
		tree.dataRows.size() == 3

	}

	def "should fetch some libraries and one opened node"() {
		when:
		def tree = this.treeBrowser.getInitialTree(TEST_CASE, new HashSet<NodeReference>([new NodeReference(NodeType.TEST_CASE_LIBRARY, -1L)]), new HashSet<>())

		then:
		tree.dataRows.size() == 6

	}

	def "should refresh one closed node"() {
		when:
		def tree = this.treeBrowser.findSubHierarchy(new HashSet<NodeReference>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L)]), new HashSet<>())

		then:
		tree.dataRows.size() == 1
		tree.dataRows.get(0).id == 'TestCaseFolder--5'

	}

	def "should not refresh illegal nodes"() {
		when:
		def tree = this.treeBrowser.findSubHierarchy(new HashSet<NodeReference>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L), new NodeReference(NodeType.TEST_CASE_FOLDER, -666L)]), new HashSet<>())

		then:
		tree.dataRows.size() == 1
		tree.dataRows.get(0).id == 'TestCaseFolder--5'

	}

	def "should refresh one opened node"() {
		when:
		def tree = this.treeBrowser.findSubHierarchy(
			new HashSet<NodeReference>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L)]),
			new HashSet<>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L)]))

		then:
		tree.dataRows.size() == 4
		DataRow folder1 = tree.dataRows.find { it.id == 'TestCaseFolder--5' }
		DataRow folder2 = tree.dataRows.find { it.id == 'TestCaseFolder--6' }
		DataRow folder3 = tree.dataRows.find { it.id == 'TestCaseFolder--7' }
		folder1.children == ['TestCaseFolder--6', 'TestCaseFolder--7', 'TestCase--11']
		folder2.parentRowId == 'TestCaseFolder--5'
		folder3.parentRowId == 'TestCaseFolder--5'
	}


	def "should refresh one opened node and some descendant opened"() {
		when:
		def tree = this.treeBrowser.findSubHierarchy(
			new HashSet<NodeReference>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L)]),
			new HashSet<>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L),
						   new NodeReference(NodeType.TEST_CASE_FOLDER, -6L),
						   // this node is illegal, it is out of root node hierarchy
						   new NodeReference(NodeType.TEST_CASE_FOLDER, -9L),
						   // this node is illegal, it just doesn't exist in dataset
						   new NodeReference(NodeType.TEST_CASE_FOLDER, -666L)]))

		then:
		tree.dataRows.size() == 5
		println(tree.dataRows.collect { it.id })
		DataRow folder1 = tree.dataRows.find { it.id == 'TestCaseFolder--5' }
		DataRow folder2 = tree.dataRows.find { it.id == 'TestCaseFolder--6' }
		DataRow testCase2 = tree.dataRows.find { it.id == 'TestCase--12' }
		folder1.children == ['TestCaseFolder--6', 'TestCaseFolder--7', 'TestCase--11']
		folder2.children == ['TestCase--12']
		testCase2.parentRowId == 'TestCaseFolder--6'
		DataRow notInHierarchy = tree.dataRows.find { it.id == 'TestCaseFolder--9' }
		notInHierarchy == null
	}

	def "should refresh two sibling opened node and some descendant opened"() {
		when:
		def tree = this.treeBrowser.findSubHierarchy(
			new HashSet<NodeReference>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L),
										new NodeReference(NodeType.TEST_CASE_FOLDER, -9L)]),
			new HashSet<>([new NodeReference(NodeType.TEST_CASE_FOLDER, -5L),
						   new NodeReference(NodeType.TEST_CASE_FOLDER, -6L),
						   new NodeReference(NodeType.TEST_CASE_FOLDER, -9L),
			]))

		then:
		tree.dataRows.size() == 7
		DataRow folder1 = tree.dataRows.find { it.id == 'TestCaseFolder--5' }
		DataRow folder2 = tree.dataRows.find { it.id == 'TestCaseFolder--6' }
		DataRow folder5 = tree.dataRows.find { it.id == 'TestCaseFolder--9' }
		DataRow testCase2 = tree.dataRows.find { it.id == 'TestCase--12' }
		DataRow testCase4 = tree.dataRows.find { it.id == 'TestCase--14' }
		folder1.children == ['TestCaseFolder--6', 'TestCaseFolder--7', 'TestCase--11']
		folder2.children == ['TestCase--12']
		testCase2.parentRowId == 'TestCaseFolder--6'
		folder5.children == ['TestCase--14']
		testCase4.parentRowId == 'TestCaseFolder--9'
	}

	def "should gracefully ignore illegal nodes"() {
		when:
		def tree = this.treeBrowser.getInitialTree(TEST_CASE,
			new HashSet<NodeReference>([
				new NodeReference(NodeType.TEST_CASE_LIBRARY, -111L),
				new NodeReference(NodeType.TEST_CASE_FOLDER, -666L)]),
			new HashSet<>())

		then:
		tree.dataRows.size() == 3

	}

}
