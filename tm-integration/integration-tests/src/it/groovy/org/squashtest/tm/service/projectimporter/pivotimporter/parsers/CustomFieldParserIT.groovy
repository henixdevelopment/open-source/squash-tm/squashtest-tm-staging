/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter.parsers


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.customfield.SingleSelectField
import org.squashtest.tm.domain.projectimporter.PivotFormatImport
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class CustomFieldParserIT extends DbunitServiceSpecification {

    @Inject CustomFieldParser customFieldParser

    def pivotFormatImport = new PivotFormatImport()


    def setup() {
        pivotFormatImport.id = 1
    }

    def "should parse a custom field"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/custom_field.json")

        when:
            def customFieldToImport = customFieldParser.parseCustomField(jsonParser, pivotFormatImport)

        then:
            customFieldToImport.internalId == "cuf4"
            customFieldToImport.boundEntities == [BindableEntity.REQUIREMENT_VERSION]

            SingleSelectField customField = (SingleSelectField) customFieldToImport.customField
            customField.name == "cf_dropdown"
            customField.inputType == InputType.DROPDOWN_LIST
            customField.label == "Dropdown custom field"
            customField.code == "CF_DDL"
            customField.defaultValue == "opt1"
            customField.optional
            customField.options[0].label == "opt1"
            customField.options[0].code == "optCode1"
            customField.options[0].colour == "#FF0000"
            customField.options[1].label == "opt2"
            customField.options[1].code == "optCode2"
            customField.options[1].colour == null
    }

}
