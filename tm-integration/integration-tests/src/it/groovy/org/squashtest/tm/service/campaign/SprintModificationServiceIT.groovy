/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.junit.jupiter.api.BeforeAll
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.campaign.Sprint
import org.squashtest.tm.domain.campaign.SprintStatus
import org.squashtest.tm.service.display.sprint.SprintDisplayService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
class SprintModificationServiceIT extends DbunitServiceSpecification {

	@Inject
	private SprintModificationService sprintModificationService

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")
    private long sprintId = -1L

    def "should change the reference of a sprint"() {
        when:
        sprintModificationService.changeReference(sprintId, "betterReference")
        def sprint = findEntity(Sprint.class, sprintId)

        then:
        sprint.getReference() == "betterReference"
    }

    def "should change the description of a sprint"() {
        when:
        sprintModificationService.changeDescription(sprintId, "This new description is cleaner")
        def sprint = findEntity(Sprint.class, sprintId)

        then:
        sprint.getDescription() == "This new description is cleaner"
    }

    def "should change the startDate of a sprint"() {
        when:
        sprintModificationService.changeStartDate(sprintId, dateFormat.parse("2021-02-01"))
        def sprint = findEntity(Sprint.class, sprintId)

        then:
        sprint.getStartDate() == dateFormat.parse("2021-02-01")
    }

    def "should change the endDate of a sprint"() {
        when:
        sprintModificationService.changeEndDate(sprintId, dateFormat.parse("2022-02-01"))
        def sprint = findEntity(Sprint.class, sprintId)

        then:
        sprint.getEndDate() == dateFormat.parse("2022-02-01")
    }

    def "should change the status of a sprint"() {
        when:
        sprintModificationService.changeStatus(sprintId, SprintStatus.OPEN)
        def sprint = findEntity(Sprint.class, sprintId)

        then:
        sprint.getStatus() == SprintStatus.OPEN
    }
}
