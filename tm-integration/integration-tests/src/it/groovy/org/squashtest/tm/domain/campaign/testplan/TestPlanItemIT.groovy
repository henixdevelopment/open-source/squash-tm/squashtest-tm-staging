/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign.testplan

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@UnitilsSupport
@Transactional
@DataSet
class TestPlanItemIT extends DbunitServiceSpecification {

	@PersistenceContext
	EntityManager em

	def "should add an execution"() {
		given:
		TestPlanItem testPlanItem = em.find(TestPlanItem.class, -1L)
        Execution execution = Mock()

		when:
        testPlanItem.addExecution(execution)

        then:
        testPlanItem.getExecutions().size() == 1
	}

	def "should remove latest execution"() {
		given:
		TestPlanItem testPlanItem = em.find(TestPlanItem.class, -2L)

		when:
        testPlanItem.removeExecutionsById([-3L])

        then:
        testPlanItem.getExecutions().size() == 1
        testPlanItem.getExecutionStatus() == ExecutionStatus.SUCCESS
	}

	def "should remove execution that is not latest"() {
		given:
		TestPlanItem testPlanItem = em.find(TestPlanItem.class, -2L)

		when:
        testPlanItem.removeExecutionsById([-2L])

        then:
        testPlanItem.getExecutions().size() == 1
        testPlanItem.getExecutionStatus() == ExecutionStatus.BLOCKED
	}

	def "should remove all executions"() {
		given:
		TestPlanItem testPlanItem = em.find(TestPlanItem.class, -2L)

		when:
        testPlanItem.removeExecutionsById([-2L, -3L])

        then:
        testPlanItem.getExecutions().size() == 0
        testPlanItem.getExecutionStatus() == ExecutionStatus.READY
	}
}
