/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.exploratory

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.testcase.ExploratoryTestCase
import org.squashtest.tm.service.testcase.exploratory.ExploratoryTestCaseService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@UnitilsSupport
@Transactional
@DataSet
class ExploratoryTestCaseServiceIT extends DbunitServiceSpecification {

    @PersistenceContext
    EntityManager em

    @Inject
    private ExploratoryTestCaseService exploratoryTestCaseService


    def "Should update charter"() {
        when:
            exploratoryTestCaseService.updateCharter(-7L, 'banane')

        then:
            def result = em.find(ExploratoryTestCase.class, -7L)
            result.charter == 'banane'
    }

    def "Should update session duration"() {
        when:
            exploratoryTestCaseService.updateSessionDuration(-7L, 136)

        then:
            def result = em.find(ExploratoryTestCase.class, -7L)
            result.sessionDuration == 136
    }

}
