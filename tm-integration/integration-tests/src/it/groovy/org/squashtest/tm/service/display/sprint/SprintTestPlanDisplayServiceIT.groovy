/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.sprint


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.StubPermissionEvaluationService
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.domain.campaign.Sprint
import org.squashtest.tm.service.display.testplan.SprintTestPlanDisplayService
import org.squashtest.tm.service.internal.display.grid.DataRow
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class SprintTestPlanDisplayServiceIT extends DbunitServiceSpecification {

    @Inject private SprintTestPlanDisplayService sprintTestPlanDisplayService
    @Inject private StubPermissionEvaluationService stubPermissionEvaluationService

    def "should return sprint overall execution plan grid for admin user"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 25
        gridRequest.page = 0

        when:
        def grid = sprintTestPlanDisplayService.findSprintOverallExecPlan(-1L, gridRequest)

        then:
        grid.count == 4

        DataRow row4 = grid.dataRows.get(0)
        row4.data.get("sprintReqVersionId") == -2L
        row4.data.get("sprintReqVersionNameWithRef") == "SRV2"

        DataRow row1 = grid.dataRows.get(1)
        row1.data.get("sprintReqVersionId") == -1L
        row1.data.get("sprintReqVersionNameWithRef") == "001 - SRV1"
    }

    def "should return sprint overall execution plan grid for test runner user"() {
        given:
        def gridRequest = new GridRequest()
        UserContextHelper.setUsername("test_runner")
        stubPermissionEvaluationService.addPermissionToRefuse("READ_UNASSIGNED", Sprint.class.getName(), -1L)

        when:
        def grid = sprintTestPlanDisplayService.findSprintOverallExecPlan(-1L, gridRequest)

        then:
        grid.count == 3
    }

    def "should display correct linked requirement name if sprint requirement version not already denormalized"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 25
        gridRequest.page = 0

        when:
        def grid = sprintTestPlanDisplayService.findSprintOverallExecPlan(-2L, gridRequest)

        then:
        grid.count == 1
        DataRow row1 = grid.dataRows.get(0)
        row1.data.get("sprintReqVersionNameWithRef") == "xxx - a third requirement"
    }
}
