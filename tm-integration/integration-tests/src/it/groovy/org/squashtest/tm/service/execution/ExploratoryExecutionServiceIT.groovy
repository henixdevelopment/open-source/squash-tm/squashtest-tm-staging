/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.execution

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.execution.ExploratoryExecution
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager

@UnitilsSupport
@Transactional
@DataSet("SessionNoteServiceIT.xml")
class ExploratoryExecutionServiceIT extends DbunitServiceSpecification {

    @Inject
    ExploratoryExecutionService exploratorySessionService

    @Inject
    EntityManager entityManager

    def "should assign a user"() {
        when:
        exploratorySessionService.assignUser(-1L, -1L)

        then:
        ExploratoryExecution exploratoryExecution = entityManager.find(ExploratoryExecution.class, -1L)
        exploratoryExecution.assigneeUser.id == -1L
    }

    def "should update a task division"() {
        when:
        exploratorySessionService.updateTaskDivision(-1L, "L fait ci et R fait ça")

        then:
        ExploratoryExecution exploratoryExecution = entityManager.find(ExploratoryExecution.class, -1L)
        exploratoryExecution.taskDivision == "L fait ci et R fait ça"
    }

    def "should update review status"() {
        when:
        exploratorySessionService.updateReviewStatus(-1L, true)

        then:
        ExploratoryExecution exploratoryExecution = entityManager.find(ExploratoryExecution.class, -1L)
        exploratoryExecution.reviewed
    }


}
