/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto
import org.squashtest.tm.service.requirement.HighLevelRequirementService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
@Transactional
class RequirementVersionDisplayDaoIT extends DbunitServiceSpecification {

	@Inject
	private RequirementVersionDisplayDao dao;

	@Inject
	private HighLevelRequirementService highLevelRequirementService;

	def setup() {
		def ids = [-11L, -21L, -31L, -41L, -51L]
		ids.each {
			setBidirectionalReqReqVersion(it, it)
		}

		highLevelRequirementService.linkToHighLevelRequirement(-21L, -31L)
	}

	def setBidirectionalReqReqVersion(Long reqVersionId, Long reqId) {
		def reqVer = em.find(RequirementVersion.class, reqVersionId)
		def req = em.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		// Flush is necessary as the dao use native query through jooq.
		em.flush()
	}

	def "should find requirement version details"() {

		when:
		def requirementVersionDto = dao.findById(-11L)

		then:
		requirementVersionDto.description == 'my nice requirement'
		requirementVersionDto.versionNumber == 1
		requirementVersionDto.criticality == 'CRITICAL'
		requirementVersionDto.category == 1
		requirementVersionDto.status == 'WORK_IN_PROGRESS'

	}

	def "should find linked high level requirement details"() {

		when:
		RequirementVersionDto requirementVersionDto = dao.findRequirementVersion(-31L)

		then:
		requirementVersionDto.id == -31L
		requirementVersionDto.name == 'Requirement3'
		requirementVersionDto.description == 'Une Description.'
		requirementVersionDto.versionNumber == 1
		requirementVersionDto.criticality == 'CRITICAL'
		requirementVersionDto.category == 1
		requirementVersionDto.status == 'UNDER_REVIEW'
		requirementVersionDto.linkedHighLevelRequirement.requirementId == -21L
		requirementVersionDto.linkedHighLevelRequirement.requirementVersionId == -21L
		requirementVersionDto.linkedHighLevelRequirement.name == "Requirement2"
		!requirementVersionDto.isHighLevelRequirement()
	}

	def "should find high level requirement version "() {

		when:
		HighLevelRequirementVersionDto requirementVersionDto = dao.findHighLevelRequirementVersion(-21L)

		then:
		requirementVersionDto.id == -21L
		requirementVersionDto.name == 'Requirement2'
		requirementVersionDto.isHighLevelRequirement()
	}
}
