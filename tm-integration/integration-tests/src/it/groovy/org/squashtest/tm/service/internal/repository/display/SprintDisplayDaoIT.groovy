/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.campaign.SprintStatus
import org.unitils.dbunit.annotation.DataSet

import spock.unitils.UnitilsSupport
import javax.inject.Inject
import java.time.LocalDate

@UnitilsSupport
@DataSet
class SprintDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
    SprintDisplayDao sprintDisplayDao

	def "should fetch a sprint dto"() {
		given:
		when:
		def sprintDto = sprintDisplayDao.getSprintDtoById(-1L)
		then:
		sprintDto.id == -1L
        sprintDto.projectId == -1L
        sprintDto.name == 'Sprint 1'
        sprintDto.reference == 'SprintRef'
        sprintDto.createdOn == LocalDate.of(2024, 1, 12).toDate()
        sprintDto.createdBy == 'Bruce Lee'
        sprintDto.lastModifiedOn == LocalDate.of(2024, 2, 25).toDate()
        sprintDto.lastModifiedBy == 'JCVD'
        sprintDto.startDate == LocalDate.of(2024, 3, 25).toDate()
        sprintDto.endDate == LocalDate.of(2024, 4, 05).toDate()
        sprintDto.description == 'Bravo pour cette superbe description très explicite'
        sprintDto.attachmentListId == -988L
        sprintDto.status == SprintStatus.UPCOMING
	}
}
