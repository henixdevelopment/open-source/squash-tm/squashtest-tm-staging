/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository

import org.squashtest.tm.domain.campaign.testplan.TestPlanItem
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class CustomSprintReqVersionDaoIT extends DbunitDaoSpecification {
    @Inject
    CustomSprintReqVersionDao dao

    @DataSet()
    def "should delete test plan items without executions and nullify test case for items with executions"() {
        when:
        dao.removeTestPlanItemOrNullifyReferencedTestCase([-1L, -2L, -3L])

        then:
        ! found(TestPlanItem.class, -1L)
        def keptItem = findEntity(TestPlanItem.class, -2L)
        keptItem.referencedTestCase == null
    }
}
