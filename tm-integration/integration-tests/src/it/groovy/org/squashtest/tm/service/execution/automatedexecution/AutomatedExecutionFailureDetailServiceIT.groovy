/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.execution.automatedexecution

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.security.UserContextHolder
import org.squashtest.tm.service.internal.execution.automatedexecution.AutomatedExecutionFailureDetailServiceImpl
import org.squashtest.tm.service.internal.repository.ExecutionDao
import org.squashtest.tm.service.internal.repository.FailureDetailDao
import org.squashtest.tm.service.internal.repository.IssueDao
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao
import org.squashtest.tm.service.internal.repository.ProjectDao
import org.squashtest.tm.service.internal.repository.display.AutomatedExecutionDisplayDao
import org.squashtest.tm.service.internal.repository.display.FailureDetailDisplayDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@UnitilsSupport
@Transactional

class AutomatedExecutionFailureDetailServiceIT extends DbunitServiceSpecification {

    AutomatedExecutionFailureDetailService service

    @Inject
    FailureDetailDao failureDetailDao

    @Inject
    AutomatedExecutionDisplayDao automatedExecutionDisplayDao

    @Inject
    AutomatedExecutionExtenderDao automatedExecutionExtenderDao

    @Inject
    FailureDetailDisplayDao failureDetailDisplayDao

    @Inject
    IssueDao issueDao

    @Inject
    ProjectDao projectDao

    @Inject
    ExecutionDao executionDao

    def setup() {
        service = new AutomatedExecutionFailureDetailServiceImpl(
            failureDetailDao,
            failureDetailDisplayDao,
            issueDao,
            projectDao,
            executionDao)
        UserContextHolder.username() >> "admin"

    }

    def "should create new FailureDetail when test plan item does not have a FailureDetail with this message"() {
        given:
            def newFailureDetail = "I am a new failure detail"
            def newFailureDetail2 = "I also am a new failure detail"
            def newFailureDetailList = [newFailureDetail, newFailureDetail2]
            def beforeUpsertItpiFailureDetailList = failureDetailDao.findByItpiIdAndMessageIn(-3L, newFailureDetailList)
            def beforeUpsertExtenderFailureDetailList = automatedExecutionDisplayDao.findAllFailureDetailsByAutomatedExecutionExtenderId(-3L)
            def extender = automatedExecutionExtenderDao.findById(-3L);
            def itpi = extender.getIterationTestPlanItem()

        when:
            service.upsertFailureDetail(newFailureDetail, beforeUpsertItpiFailureDetailList, extender, itpi)
            service.upsertFailureDetail(newFailureDetail2, beforeUpsertItpiFailureDetailList, extender, itpi)

        then:
            def afterInsertFailureDetailList = failureDetailDao.findByItpiIdAndMessageIn(-3L, newFailureDetailList)
            def extenderFailureDetails = automatedExecutionDisplayDao.findAllFailureDetailsByAutomatedExecutionExtenderId(-3L)

            beforeUpsertItpiFailureDetailList.size() == 0
            afterInsertFailureDetailList.size() == 2

            afterInsertFailureDetailList*.message.containsAll(newFailureDetailList)

            beforeUpsertExtenderFailureDetailList.size() == 1
            extenderFailureDetails.size() == 3
    }

    def "should not create new FailureDetail when test plan item already have a FailureDetail with this message"() {
        given:
            def existingFailureDetail = "Test has failed, cannot divide by 0"
            def beforeUpsertItpiFailureDetailList = failureDetailDao.findByItpiIdAndMessageIn(-3L, [existingFailureDetail])
            def beforeUpsertExtenderFailureDetailList = automatedExecutionDisplayDao.findAllFailureDetailsByAutomatedExecutionExtenderId(-4L)
            def extender = automatedExecutionExtenderDao.findById(-4L);
            def itpi = extender.getIterationTestPlanItem()

        when:
            service.upsertFailureDetail(existingFailureDetail, beforeUpsertItpiFailureDetailList, extender, itpi)

        then:
            def afterInsertFailureDetailList = failureDetailDao.findByItpiIdAndMessageIn(-3L, [existingFailureDetail])
            def extenderFailureDetails = automatedExecutionDisplayDao.findAllFailureDetailsByAutomatedExecutionExtenderId(-4L)

            beforeUpsertItpiFailureDetailList.size() == 1
            afterInsertFailureDetailList.size() == 1

            afterInsertFailureDetailList*.message.contains(existingFailureDetail)

            beforeUpsertExtenderFailureDetailList.size() == 0
            extenderFailureDetails.size() == 1
    }



}
