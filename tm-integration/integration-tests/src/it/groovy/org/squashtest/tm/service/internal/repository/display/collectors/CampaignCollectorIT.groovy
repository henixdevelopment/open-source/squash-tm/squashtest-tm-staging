/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.repository.display.impl.collectors.CampaignCollector
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet()
class CampaignCollectorIT extends DbunitDaoSpecification {

	@Inject
	CampaignCollector campaignCollector

	@Inject
	ActiveMilestoneHolder activeMilestoneHolder

	def setup() {
		activeMilestoneHolder.setActiveMilestone(-1L)
	}

	def cleanup() {
		activeMilestoneHolder.clearContext()
	}

	@Unroll
	def "Should collect campaigns"() {
		when:
		def nodes = campaignCollector.collect(ids)

		then:
		nodes.size() == expectedNodes.size();
		for (id in ids) {
			def expectedNode = expectedNodes.get(id);
			def actualNode = nodes.get(id)

			assert expectedNode.id == actualNode.id;
			Map<String, Object> expectedData = expectedNode.data;
			def actualData = actualNode.data
			assert actualData.size() == expectedData.size()
			expectedData.forEach { key, expectedValue ->
				def actualValue = actualData.get(key)
				assert actualValue == expectedValue;
			}
		}

		where:
		ids             || expectedNodes
		null            || []
		[]              || []
		[-2L, -4L, -5L] || [
			(-2L): [id       : "Campaign--2",
					projectId: -1L,
					data     : [
						CLN_ID     : -2L,
						NAME       : "REF-001 - campaign-1",
						CHILD_COUNT: 2,
						projectId  : -1L,
						REFERENCE  : "REF-001",
						MILESTONES : [],
						BOUND_TO_BLOCKING_MILESTONE: false
					]
			],
			(-4L): [id       : "Campaign--4",
					projectId: -1L,
					data     : [
						CLN_ID     : -4L,
						NAME       : "campaign-2",
						CHILD_COUNT: 2,
						projectId  : -1L,
						REFERENCE  : "",
						MILESTONES : [-1],
						BOUND_TO_BLOCKING_MILESTONE: false
					]
			],
			(-5L): [id       : "Campaign--5",
					projectId: -1L,
					data     : [
						CLN_ID     : -5L,
						NAME       : "campaign-3",
						CHILD_COUNT: 2,
						projectId  : -1L,
						REFERENCE  : "",
						MILESTONES : [-2L],
						BOUND_TO_BLOCKING_MILESTONE: true
					]
			]
		]
	}
}
