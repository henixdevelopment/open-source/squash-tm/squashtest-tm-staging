/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable

import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingValueService
import spock.unitils.UnitilsSupport
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao
import org.unitils.dbunit.annotation.DataSet
import org.springframework.transaction.annotation.Transactional

import javax.inject.Inject


@Transactional
@UnitilsSupport
@DataSet("EnvironmentVariableBindingService.xml")
class EnvironmentVariableValueServiceIT extends DbunitServiceSpecification {

	@Inject
	EnvironmentVariableBindingValueService environmentVariableValueService

	@Inject
	EnvironmentVariableBindingDao environmentVariableBindingDao

	def "Should edit environment variable value from server"() {
		given:
		Long bindingId = -1L
		String newValue = "test new value"

		when:
		environmentVariableValueService.editEnvironmentVariableValue(bindingId, newValue)

		then:
		EnvironmentVariableBinding binding = environmentVariableBindingDao.getReferenceById(bindingId)
		binding.value == newValue
	}

	def "should edit environment variable value from project"() {
		given:
		Long bindingId = -3L
		String updatedValue = "updatedValue"

		when:
		environmentVariableValueService.editEnvironmentVariableValue(bindingId, updatedValue)

		then:
		EnvironmentVariableBinding binding = environmentVariableBindingDao.getReferenceById(bindingId)
		binding.value == updatedValue
	}

	def "should reset default value from project view"() {
		given:
		String projectType = "PROJECT"
		Long projectBindingId = -3L
		Long serverBindingId = -2L

		when:
		def result = environmentVariableValueService.resetDefaultValue(projectBindingId, projectType)

		then:
		EnvironmentVariableBinding serverBinding = environmentVariableBindingDao.getReferenceById(serverBindingId)
		result == serverBinding.value
		result instanceof String
	}

	def "should reset default value from execution dialog"() {
		given:
		String executionDialogType = "EXECUTION_DIALOG"
		Long projectBindingId = -3L

		when:
		def result = environmentVariableValueService.resetDefaultValue(projectBindingId, executionDialogType)

		then:
		EnvironmentVariableBinding projectBinding = environmentVariableBindingDao.getReferenceById(projectBindingId)
		result == projectBinding.value
		result instanceof String
	}

	def "should reinitialize binding values after options removed"() {
		given:
		Long testAutomationServerId = -1L
		List<String> deletedLabels = Arrays.asList("OPTION1")
		Long environmentVariableId = -2L

		when:
		environmentVariableValueService.reinitializeEnvironmentVariableValuesByValueAndEvId(deletedLabels, environmentVariableId)

		then:
		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByEntityIdTypeAndEvId(testAutomationServerId, EVBindableEntity.TEST_AUTOMATION_SERVER, environmentVariableId)
		binding.value == ""
	}

	def "should replace option values"() {
		given:
		Long testAutomationServerId = -1L
		String oldValue = "OPTION1"
		String newValue = "UpdatedValue"
		Long environmentVariableId = -2L

		when:
		environmentVariableValueService.replaceAllExistingValuesByEvId(environmentVariableId, oldValue, newValue)

		then:
		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByEntityIdTypeAndEvId(testAutomationServerId, EVBindableEntity.TEST_AUTOMATION_SERVER, environmentVariableId)
		binding.value == newValue
	}




}
