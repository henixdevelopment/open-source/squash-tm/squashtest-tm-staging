/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.EntityReference
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.service.internal.display.search.HighLevelRequirementScopeExtender
import org.squashtest.tm.service.requirement.HighLevelRequirementService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager

@UnitilsSupport
@Transactional
@DataSet
class HighLevelRequirementScopeExtenderIT extends DbunitServiceSpecification {

	@Inject
	private HighLevelRequirementScopeExtender extender

	@Inject
	private HighLevelRequirementService highLevelRequirementService

	@Inject
	EntityManager entityManager

	def setup() {
		def ids = [
			[-11L, -22L],
			[-12L, -14L], // linked requirement already in hierarchy of his high level req, ie a trap for the extender code
			[-12L, -20L],
		]
		ids.each {
			highLevelRequirementService.linkToHighLevelRequirement(it[0], it[1])
		}
	}

	@Unroll
	def "should extends perimeter"() {
		given:
		def scope = initialScope

		when:
		def extendedScope = extender.extendScope(scope)


		def sorter = { ref1, ref2 -> return ref1.id <=> ref2.id }
		then:
		extendedScope.sort(sorter) == expectedFinalScope.sort(sorter)

		where:
		initialScope                                               || expectedFinalScope
		[]                                                         || []
		// simple case one HLReq linked to a req in another project
		[new EntityReference(EntityType.REQUIREMENT, -12L)]        || [new EntityReference(EntityType.REQUIREMENT, -12L),
																	   new EntityReference(EntityType.REQUIREMENT, -20L)]
		// simple case one library which contains two HLReq linked to a req in another project
		[new EntityReference(EntityType.REQUIREMENT_LIBRARY, -1L)] || [new EntityReference(EntityType.REQUIREMENT_LIBRARY, -1L),
																	   new EntityReference(EntityType.REQUIREMENT, -20L),
																	   new EntityReference(EntityType.REQUIREMENT, -22L)]
		// simple case one folder which contains two HLReq linked to a req in another project
		[new EntityReference(EntityType.REQUIREMENT_FOLDER, -1L)]  || [new EntityReference(EntityType.REQUIREMENT_FOLDER, -1L),
																	   new EntityReference(EntityType.REQUIREMENT, -20L),
																	   new EntityReference(EntityType.REQUIREMENT, -22L)]
		// case two HLReqs linked each one to a req in another project
		[new EntityReference(EntityType.REQUIREMENT, -11L),
		 new EntityReference(EntityType.REQUIREMENT, -12L)]        || [new EntityReference(EntityType.REQUIREMENT, -11L),
																	   new EntityReference(EntityType.REQUIREMENT, -12L),
																	   new EntityReference(EntityType.REQUIREMENT, -20L),
																	   new EntityReference(EntityType.REQUIREMENT, -22L)]
		// testing that requirement already present in perimeter are excluded for libraries in perimeter
		[new EntityReference(EntityType.REQUIREMENT_LIBRARY, -1L),
		 new EntityReference(EntityType.REQUIREMENT_LIBRARY, -2L)] || [new EntityReference(EntityType.REQUIREMENT_LIBRARY, -1L),
																	   new EntityReference(EntityType.REQUIREMENT_LIBRARY, -2L)]
		// testing that requirement already present in perimeter are excluded for folder in perimeter
		[new EntityReference(EntityType.REQUIREMENT_LIBRARY, -1L),
		 new EntityReference(EntityType.REQUIREMENT_FOLDER, -21L)] || [new EntityReference(EntityType.REQUIREMENT_LIBRARY, -1L),
																	   new EntityReference(EntityType.REQUIREMENT, -20L),
																	   new EntityReference(EntityType.REQUIREMENT_FOLDER, -21L)]
		// testing that requirement already present in perimeter are not excluded for requirements in perimeter
		[new EntityReference(EntityType.REQUIREMENT, -12L),
		 new EntityReference(EntityType.REQUIREMENT, -20L)]        || [new EntityReference(EntityType.REQUIREMENT, -12L),
																	   new EntityReference(EntityType.REQUIREMENT, -20L)]
		// testing that it do not extends if no high level req
		[new EntityReference(EntityType.REQUIREMENT_FOLDER, -21L)] || [new EntityReference(EntityType.REQUIREMENT_FOLDER, -21L)]
	}

}
