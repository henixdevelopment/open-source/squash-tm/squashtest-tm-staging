/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.StubPermissionEvaluationService
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.exception.execution.EmptyIterationTestPlanException
import org.squashtest.tm.exception.execution.TestPlanTerminatedOrNoStepsException
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
class IterationExecutionProcessingServiceIT extends DbunitServiceSpecification {

	@Inject
	private TestPlanExecutionProcessingService<TestPlanResume.IterationTestPlanResume> service

	@Inject
	private StubPermissionEvaluationService stubPermissionEvaluationService

	def setupSpec(){
		UserContextHelper.setUsername("Joe")
	}

	@DataSet("IterationExecutionProcessingServiceIT.should not find exec step cause no item.xml")
	def "should try to start and not find execution because test plan empty"(){
		given :
		long iterationId = -1L
		when :
		service.startResume(iterationId)

		then :
		thrown EmptyIterationTestPlanException
	}

	@DataSet("IterationExecutionProcessingServiceIT.should not find exec step cause no item for tester.xml")
	def "should try to start and not find execution because test plan empty for tester"(){
		given :
		def iterationId = -1L
		stubPermissionEvaluationService.addPermissionToRefuse("READ_UNASSIGNED", Iteration.class.getName(), -1L)

		when:
		service.startResume(iterationId)

		then:
		thrown EmptyIterationTestPlanException
		stubPermissionEvaluationService.emptyPermissionsToRefuse()
	}

	@DataSet("IterationExecutionProcessingServiceIT.should not find exec step cause all term.xml")
	def "should try to resume and not find execution because all terminated"(){
		given :
		long iterationId = -1L

		when :
		service.startResume(iterationId)

		then :
		thrown TestPlanTerminatedOrNoStepsException
	}

	@DataSet("IterationExecutionProcessingServiceIT.should not find exec step cause all term for tester.xml")
	def "should try to resume and not find execution because all terminated for tester"(){
		given :
		long iterationId = -1L
		stubPermissionEvaluationService.addPermissionToRefuse("READ_UNASSIGNED", Iteration.class.getName(), -1L)

		when :
		service.startResume(iterationId)

		then :
		thrown TestPlanTerminatedOrNoStepsException
		stubPermissionEvaluationService.emptyPermissionsToRefuse()
	}

	@DataSet("IterationExecutionProcessingServiceIT.should not find exec step cause no step.xml")
	def "should try to resume and not find execution because all have no step"(){
		given :
		long iterationId = -1L

		when :
		service.startResume(iterationId)

		then :
		thrown TestPlanTerminatedOrNoStepsException
	}

	@DataSet("IterationExecutionProcessingServiceIT.should not find exec step cause no step for tester.xml")
	def "should try to resume and not find execution because all have no step for tester"(){
		given :
		long iterationId = -1L
		stubPermissionEvaluationService.addPermissionToRefuse("READ_UNASSIGNED", Iteration.class.getName(), -1L)

		when :
		service.startResume(iterationId)

		then :
		thrown TestPlanTerminatedOrNoStepsException
		stubPermissionEvaluationService.emptyPermissionsToRefuse()
	}


	@DataSet("IterationExecutionProcessingServiceIT.should find exec step through new exec.xml")
	def "should try to resume and create new execution"(){
		given :
		long iterationId = -1L

		when :
        TestPlanResume.IterationTestPlanResume iterationTestPlanResume = service.startResume(iterationId)

		then :
        iterationTestPlanResume != null
        iterationTestPlanResume.initialStepIndex == 0
	}


	@DataSet("IterationExecutionProcessingServiceIT.should find exec step through old exec.xml")
	def "should try to resume and find old execution"(){
		given :
		long iterationId = -1L

		when :
        TestPlanResume.IterationTestPlanResume resume = service.startResume(iterationId)

		then :
        resume != null
        resume.initialStepIndex == 1
	}


     @DataSet("IterationExecutionProcessingServiceIT.should find no more executable item.xml")
     def "should find no more executable item"(){
         given :
         Iteration iteration = em.find(Iteration.class, -1L)
         IterationTestPlanItem item = em.find(IterationTestPlanItem.class, -1L)
         Execution execution = new Execution()
         execution.notifyAddedTo(item)

         when :
         boolean more = service.hasNextTestCase(iteration, null, execution)

         then :
         !more
     }

     @DataSet("IterationExecutionProcessingServiceIT.should find more executable item.xml")
     def "should find more executable item"(){
         given :
         Iteration iteration = em.find(Iteration.class, -1L)
         IterationTestPlanItem item = em.find(IterationTestPlanItem.class, -1L)
         Execution execution = new Execution()
         execution.notifyAddedTo(item)

         when :
         boolean more = service.hasNextTestCase(iteration, null, execution)

         then :
         more
     }


     @DataSet("IterationExecutionProcessingServiceIT.with active filters.xml")
     def "should not find execution because filtered test plan is empty"() {
         given:
             GridFilterValue filterValue = getFilterTestCaseNameLikeWithSingleValue("no-match")
         when:
             service.resumeWithFilteredTestPlan(-1, [filterValue])
         then:
             thrown EmptyIterationTestPlanException
     }

     @DataSet("IterationExecutionProcessingServiceIT.with active filters.xml")
     def "should not find execution because filtered test plan is empty for tester"() {
         given:
             GridFilterValue filterValue = getFilterTestCaseNameLikeWithSingleValue("no-match")
             stubPermissionEvaluationService.addPermissionToRefuse("READ_UNASSIGNED", Iteration.class.getName(), -1L)
         when:
             service.resumeWithFilteredTestPlan(-1, [filterValue])
         then:
             thrown EmptyIterationTestPlanException
             stubPermissionEvaluationService.emptyPermissionsToRefuse()
     }

     @DataSet("IterationExecutionProcessingServiceIT.with active filters.xml")
     def "should not find execution because all items are terminated in filtered test plan"() {
         given:
             GridFilterValue filterValue = getFilterTestCaseNameLikeWithSingleValue("1")
         when:
             service.resumeWithFilteredTestPlan(-1, [filterValue])
         then:
             thrown TestPlanTerminatedOrNoStepsException
     }

     @DataSet("IterationExecutionProcessingServiceIT.with active filters.xml")
     def "should find new execution in filtered test plan"() {
         given:
             GridFilterValue filterValue = getFilterTestCaseNameLikeWithSingleValue("2")
         when:
             TestPlanResume testPlanResume = service.resumeWithFilteredTestPlan(-1, [filterValue])
         then:
             testPlanResume.getTestPlanItemId() == -2
     }

     @DataSet("IterationExecutionProcessingServiceIT.with active filters.xml")
     def "should find running execution in filtered test plan"() {
         given:
             GridFilterValue filterValue = getFilterTestCaseNameLikeWithSingleValue("3")
         when:
             TestPlanResume testPlanResume = service.resumeWithFilteredTestPlan(-1, [filterValue])
         then:
             testPlanResume.getTestPlanItemId() == -3
             testPlanResume.getExecutionId() == -3
     }


     @DataSet("IterationExecutionProcessingServiceIT.with active filters.xml")
     def "should not find execution because item in filtered test plan has no steps"() {
         given:
             GridFilterValue filterValue = getFilterTestCaseNameLikeWithSingleValue("4")
         when:
             service.resumeWithFilteredTestPlan(-1, [filterValue])
         then:
             thrown TestPlanTerminatedOrNoStepsException
     }


	private static GridFilterValue getFilterTestCaseNameLikeWithSingleValue(String value) {
		GridFilterValue filterValue = new GridFilterValue()
		filterValue.setId("testCaseName")
		filterValue.setOperation("LIKE")
		filterValue.setValues([value])
		return filterValue;
	}

}
