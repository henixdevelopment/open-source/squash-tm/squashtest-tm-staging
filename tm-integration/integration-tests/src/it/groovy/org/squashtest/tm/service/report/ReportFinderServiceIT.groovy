/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.report

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class ReportFinderServiceIT extends DbunitServiceSpecification {

	@Inject
	ReportFinderService service


	@Unroll("should fetch template file name from parameters with #context")
	def "should fetch template file name from parameters"() {
		when :
		def template = service.fetchTemplateFromParametersByReportDefinitionId(reportDefinitionId)

		then:
		template == templateFileName

		where:
		context             | reportDefinitionId | templateFileName
        "template saved"    | -1L                | "report.books.requirements.template.docx"
		"no template saved" | -2L                | ""
	}

    @Unroll("should check if template is used in report")
    def "should check if template is used in report"() {

        when :
        def isUsed = service.isTemplateUsedInReport(templateName, namespace)

        then:
        isUsed == expectedResult

        where:
        templateName                        | namespace                                     | expectedResult
        "report.books.requirements.template.docx"   | "report.books.requirements.requirements.report.label" | true
        "report.books.requirements.template.docx"   | "report.iteration.report.label"                 | false
    }

}
