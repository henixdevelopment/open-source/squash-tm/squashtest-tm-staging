/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.dto.executioncomparator.TestExecutionInfo
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
class TestExecutionInfoServiceIT extends DbunitServiceSpecification {

    @Inject
    TestExecutionInfoService service

    @DataSet("TestExecutionInfoService.retrieveComparisonData.xml")
    def "Should retrieve comparison data"() {

        given:
        List<String> suiteIds = Arrays.asList("4028818d8abd08b50dcez2r", "4028800e8acc7fbc018acc8")

        when:
        List<TestExecutionInfo> infos = service.compareExecutionsBySuites(suiteIds)

        then:
        infos.size() == 1
        infos.get(0).statusBySuite.size() == 2
    }
}
