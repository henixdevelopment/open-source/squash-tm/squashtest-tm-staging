/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors.libraries

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.display.grid.DataRow
import org.squashtest.tm.service.internal.repository.display.impl.collectors.libraries.CustomReportLibraryCollector
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet("CustomReportLibraryCollectorIT.xml")
class CustomReportLibraryCollectorIT extends DbunitDaoSpecification {

	@Inject
	CustomReportLibraryCollector customReportLibraryCollector

	@Inject
	ActiveMilestoneHolder activeMilestoneHolder

	def setup() {
		activeMilestoneHolder.setActiveMilestone(-1L)
	}

	def cleanup() {
		activeMilestoneHolder.clearContext()
	}

	@Unroll
	def "Should collect custom report libraries"() {
		when:
		def collectedNodes = customReportLibraryCollector.collect(ids)
		then:
		collectedNodes.size() == expectedNodes.size()
		for (id in ids) {
			def expectedNode = expectedNodes.get(id)
			def collectedNode = collectedNodes.get(id)

			def expectedData = expectedNode.data
			def collectedData = collectedNode.data

			assert expectedNode.id == collectedNode.id
			assert expectedNode.state == collectedNode.state
			assert expectedData.size() == collectedData.size()
			expectedData.forEach { key, expectedValue ->
				def collectedValue = collectedData.get(key)
				assert expectedValue == collectedValue
			}
		}
		where:
		ids        | expectedNodes
		null       | []
		[]         | []
		[-1L, -5L] | [
			(-1L): [
				id   : "CustomReportLibrary--1",
				state: DataRow.State.closed,
				NAME : "Project_1",
				data : [
					CRLN_ID    : -1L,
					CRL_ID     : -1L,
					NAME       : "project-1",
					projectId  : -1L,
					CHILD_COUNT: 2,
					MILESTONES : [-1L],
					BOUND_TO_BLOCKING_MILESTONE: false,
				]
			],
			(-5L): [
				id       : "CustomReportLibrary--5",
				state    : DataRow.State.leaf,
				projectId: -2L,
				data     : [
					CRLN_ID    : -5L,
					CRL_ID     : -2L,
					NAME       : "project-2",
					projectId  : -2L,
					CHILD_COUNT: 0,
					MILESTONES : [],
					BOUND_TO_BLOCKING_MILESTONE: false,
				]
			]
		]
	}
}
