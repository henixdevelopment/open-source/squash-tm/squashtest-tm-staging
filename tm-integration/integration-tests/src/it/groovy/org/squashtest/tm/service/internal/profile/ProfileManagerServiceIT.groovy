/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.profile

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.domain.acl.AclGroup
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.exception.NameAlreadyInUseException
import org.squashtest.tm.exception.profile.CannotDeleteProfileException
import org.squashtest.tm.service.internal.display.dto.NewProfileDto
import org.squashtest.tm.service.internal.display.dto.ActivePermissionRecord
import org.squashtest.tm.service.internal.display.dto.ProfileActivePermissionsRecord
import org.squashtest.tm.service.profile.ProfileManagerService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.api.security.acls.Permissions.IMPORT
import static org.squashtest.tm.api.security.acls.Permissions.MANAGE_MILESTONE
import static org.squashtest.tm.api.security.acls.Permissions.MANAGE_PROJECT
import static org.squashtest.tm.api.security.acls.Permissions.MANAGE_PROJECT_CLEARANCE

@UnitilsSupport
@Transactional
@DataSet
class ProfileManagerServiceIT extends DbunitServiceSpecification {

    def projectManagerSystemProfileId = -18L
    def customProfileId = -11L

    @Inject
    ProfileManagerService profileManagerService

    def setup() {
        UserContextHelper.setUsername("admin")
    }

    def "should create a custom profile"() {
        given:
        def name = "CDP"
        def description = "Custom profile from Project Manager system profile."
        def profile = new NewProfileDto(name, description, projectManagerSystemProfileId)

        when:
        def newProfileId = profileManagerService.persist(profile)

        then:
        def newProfile = findEntity(AclGroup.class, newProfileId) as AclGroup
        newProfile != null

        newProfile.qualifiedName == name
        newProfile.description == description
        newProfile.createdBy == "admin"
        newProfile.createdOn != null
        newProfile.active

        def permissions = executeSQL("select PERMISSION_MASK from ACL_GROUP_PERMISSION where class_id = 1 and acl_group_id = " + newProfileId) as Set
        permissions.size() == 4
        permissions == [MANAGE_PROJECT.mask, IMPORT.mask, MANAGE_MILESTONE.mask, MANAGE_PROJECT_CLEARANCE.mask] as Set
    }

    def "should throw exception if profile name already exists"() {
        given:
        def profile = new NewProfileDto("Auditor", "", projectManagerSystemProfileId)

        when:
        profileManagerService.persist(profile)

        then:
        thrown(NameAlreadyInUseException)
    }

    def "should not delete a system profile"() {
        when:
        profileManagerService.deleteProfile(projectManagerSystemProfileId)

        then:
        thrown(UnsupportedOperationException)
    }

    def "should delete a unused custom profile"() {
        given:
        def unusedCustomProfileId = -12L

        when:
        profileManagerService.deleteProfile(unusedCustomProfileId)

        then:
        !found(AclGroup.class, unusedCustomProfileId)
    }

    def "should not delete a custom profile that is used"() {
        when:
        profileManagerService.deleteProfile(customProfileId)

        then:
        thrown(CannotDeleteProfileException)
    }

    def "should not modify the name of a system profile"() {
        given:
        def newName = "CDP"

        when:
        profileManagerService.changeName(projectManagerSystemProfileId, newName)

        then:
        thrown(UnsupportedOperationException)
    }

    def "should throw exception if name already in use"() {
        given:
        def newName = "Auditor"

        when:
        profileManagerService.changeName(customProfileId, newName)

        then:
        thrown(NameAlreadyInUseException)
    }

    def "should modify the name of a custom profile"() {
        given:
        def newName = "New Profile"

        when:
        profileManagerService.changeName(customProfileId, newName)

        then:
        def profile = findEntity(AclGroup.class, customProfileId) as AclGroup
        profile.qualifiedName == newName
    }

    def "should not modify the description of a system profile"() {
        given:
        def newDescription = "description new"

        when:
        profileManagerService.changeName(projectManagerSystemProfileId, newDescription)

        then:
        thrown(UnsupportedOperationException)
    }

    def "should modify the description of a custom profile"() {
        given:
        def newDescription = "description new"

        when:
        profileManagerService.changeDescription(customProfileId, newDescription)

        then:
        def profile = findEntity(AclGroup.class, customProfileId) as AclGroup
        profile.description == newDescription
    }

    def "should deactivate a profile"() {
        when:
        profileManagerService.deactivateProfile(customProfileId)

        then:
        def profile = findEntity(AclGroup.class, customProfileId) as AclGroup
        !profile.active
    }

    def "should activate a profile"() {
        when:
        profileManagerService.activateProfile(customProfileId)

        then:
        def profile = findEntity(AclGroup.class, customProfileId) as AclGroup
        profile.active
    }

    def "should not modify permissions of a system profile"() {
        when:
        profileManagerService.changePermissions(projectManagerSystemProfileId, [])

        then:
        thrown(UnsupportedOperationException)
    }

    def "should modify permissions of a custom profile"() {
        given:
        def projectPermissionList = [
                new ActivePermissionRecord(MANAGE_PROJECT, true),
                new ActivePermissionRecord(MANAGE_MILESTONE, false),
                new ActivePermissionRecord(MANAGE_PROJECT_CLEARANCE, true),
                new ActivePermissionRecord(IMPORT, false)
        ]
        def profileProjectPermissions = new ProfileActivePermissionsRecord(Project.class.getName(), projectPermissionList)

        when:
        profileManagerService.changePermissions(customProfileId, [profileProjectPermissions])

        then:
        def projectPermissions = executeSQL("select PERMISSION_MASK from ACL_GROUP_PERMISSION where class_id = 1 and acl_group_id = -11") as Set
        projectPermissions == [MANAGE_PROJECT.mask, MANAGE_PROJECT_CLEARANCE.mask] as Set
    }
}
