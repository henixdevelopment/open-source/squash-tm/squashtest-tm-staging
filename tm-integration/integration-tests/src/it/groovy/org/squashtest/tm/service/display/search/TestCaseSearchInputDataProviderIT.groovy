/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.dto.UserView
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class TestCaseSearchInputDataProviderIT extends DbunitServiceSpecification {

	@Inject
	private TestCaseSearchInputDataProvider inputDataProvider;

	def "should find users from present and past"() {

		when:
		def researchInputData = inputDataProvider.provideTestCaseResearchInputData()
		Map<String, UserView> userWhoCreatedTestCase = researchInputData.usersWhoCreatedTestCases.collectEntries({ it -> [it.login, it]})

		then:
		userWhoCreatedTestCase.size() == 4
		def bob = userWhoCreatedTestCase.get('bob')
		bob.id == -1L
		def john = userWhoCreatedTestCase.get('john')
		john.id == null // no id for him, user deleted from database

	}

}
