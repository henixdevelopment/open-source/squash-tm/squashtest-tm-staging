/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.user

import org.jooq.DSLContext
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.core.foundation.collection.Filtering
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder
import org.squashtest.tm.core.foundation.collection.PagingAndSorting
import org.squashtest.tm.domain.servers.StoredCredentials
import org.squashtest.tm.domain.users.ApiToken
import org.squashtest.tm.domain.users.Team
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService
import org.unitils.dbunit.annotation.DataSet
import org.unitils.dbunit.annotation.ExpectedDataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.core.foundation.collection.SortOrder.ASCENDING
import static org.squashtest.tm.core.foundation.collection.SortOrder.DESCENDING

/**
 * @author mpagnon
 *
 */
@DataSet
@UnitilsSupport
@Transactional
class UserAdministrationServiceIT extends DbunitServiceSpecification {

	@Inject private DSLContext DSL;
	@Inject UserAdministrationService service
	UltimateLicenseAvailabilityService ultimateLicenseAvailabilityService = Mock()

	def "setup"() {
		service.ultimateLicenseAvailabilityService = ultimateLicenseAvailabilityService
	}

	@DataSet("UserModificationServiceIT.should deassociate user to team.xml")
	@ExpectedDataSet("UserModificationServiceIT.should deassociate user to team result.xml")
	def "should deassociate user to team"(){
		given : "the dataset"
		when :
		service.deassociateTeams(-20L, new ArrayList<Long>([-10L]))
		then : "expected dataset is verified"
		getSession().flush();
	}

	@DataSet("UserModificationServiceIT.should associate user to team.xml")
	@ExpectedDataSet("UserModificationServiceIT.should associate user to team result.xml")
	def "should associate user to team"() {
		given :
		ultimateLicenseAvailabilityService.isAvailable() >> true

		when :
		def report = service.associateToTeams(-20L, [-10L, -11L])

		then : "expected dataset is verified"
		getSession().flush();
		report == [] as Set
	}

	@DataSet("UserModificationServiceIT.should associate user to team.xml")
	@ExpectedDataSet("UserModificationServiceIT.should associate user to team result without license.xml")
	def "should associate user to team without license"() {
		given :
		ultimateLicenseAvailabilityService.isAvailable() >> false

		when :
		def report = service.associateToTeams(-20L, [-10L, -11L])

		then : "expected dataset is verified"
		getSession().flush();
		report == ["team1"] as Set
	}

	@DataSet("UserModificationServiceIT.should find non associated teams.xml")
	def "should find non associated teams"(){
		given : "the dataset"
		when :
		def result = service.findAllNonAssociatedTeams(-30L)
		then :
		result.size() == 2
		result.find({it.id == -12L}) != null
		result.find({it.id == -11L}) != null
	}

	@DataSet("AdministrationServiceIT.should count all active users.xml")
	def "should count all active users"() {
		given: "the dataset"

		when:
		def res = service.countAllActiveUsers()

		then:
		res == 7
	}

	@DataSet("AdministrationServiceIT.should delete an user with stored credentials.xml")
	def "should delete an user with stored credentials"() {
		given : "the dataset"

		when :
		service.deleteUsers(Collections.singleton(-154L))

		then :
		!found(StoredCredentials.class, -1L)
		!found(ApiToken.class, -1L)
		!found(ApiToken.class, -2L)
		!found(User.class, -154L)
	}

	@DataSet("UserAdministrationServiceIT.should change delete front front permissions.xml")
	@ExpectedDataSet("UserAdministrationServiceIT.should change delete front front permissions result.xml")
	def "should change delete front front permissions"() {
		given : "the dataset"

		when :
		service.changeCanDeleteFromFront(-61L, false)
		service.changeCanDeleteFromFront(-62L, true)
		service.changeCanDeleteFromFront([-63L, -64L, -65L], true)
		service.changeCanDeleteFromFront([-66L, -64L], false)

		then : "expected dataset is verified"
		getSession().flush();
	}

    def "should find all admin or manager"() {
        when:
        def users = service.findAllAdminOrManager()

        then:
        users.sort { it.id() }
        users.size() == 5
        users.get(idx).id() == userId
        users.get(idx).login() == userLogin
        users.get(idx).firstName() == userFirstName
        users.get(idx).lastName() == userLastName

        where:
        idx | userId | userLogin            | userFirstName | userLastName
        0   | -12    | "User-team1-team2"   | ""            | ""
        1   | -8     | "User-team1-02"      | ""            | ""
        2   | -7     | "User-team1-01"      | ""            | ""
        3   | -4     | "admin2"             | "admin"       | "2"
        4   | -1     | "User-1"             | "John"        | "Doe"
    }
}
