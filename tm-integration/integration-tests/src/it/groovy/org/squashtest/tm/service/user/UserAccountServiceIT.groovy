/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.user

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.domain.users.ApiToken
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@UnitilsSupport
@Transactional
class UserAccountServiceIT extends DbunitServiceSpecification {

    @Inject
    private UserAccountService userAccountService

    def setup() {
        UserContextHelper.setUsername("Gnocchi")
    }

    def "should find personal API tokens"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 10

        when:
        def gridResponse = userAccountService.findPersonalApiTokens(gridRequest)

        then:
        gridResponse.count == 2
        def apiToken1 = gridResponse.dataRows.get(0).data
        def apiToken2 = gridResponse.dataRows.get(1).data

        if (apiToken1.id == -1L) {
            checkFirstTokenData(apiToken1)
        } else if (apiToken1.id == -2L) {
            checkSecondTokenData(apiToken1)
        }

        if (apiToken2.id == -2L) {
            checkSecondTokenData(apiToken2)
        } else if (apiToken2.id == -1L) {
            checkFirstTokenData(apiToken1)
        }
    }

    def "should delete personal API tokens when a user is deactivated"() {
        when:
        userAccountService.deactivateUser(-154L)

        then:
        !findEntity(ApiToken.class, -1L)
        !findEntity(ApiToken.class, -2L)
    }

    private static void checkFirstTokenData(apiToken) {
        apiToken.name == "token 1"
        apiToken.expiryDate.toString() == "2024-10-10"
        apiToken.createdOn.toString() == "2024-10-02 00:00:00.0"
        apiToken.lastUsage == null
        apiToken.permissions == "READ"
    }

    private static void checkSecondTokenData(apiToken) {
        apiToken.name == "token 2"
        apiToken.expiryDate.toString() == "2024-10-12"
        apiToken.createdOn.toString() == "2024-10-02 00:00:00.0"
        apiToken.lastUsage == null
        apiToken.permissions == "READ_WRITE"
    }
}
