/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.domain.campaign.CampaignStatus
import org.squashtest.tm.domain.campaign.IterationStatus
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.domain.projectimporter.PivotFormatImport
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata
import org.squashtest.tm.service.internal.dto.projectimporter.SquashCustomFieldInfo
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.ExecutionWorkspaceParser
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.JsonParserTestHelper
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat


@UnitilsSupport
@Transactional
class ExecutionWorkspaceParserIT extends DbunitServiceSpecification {

    @Inject ExecutionWorkspaceParser executionWorkspaceParser

    def pivotImportMetadata = new PivotImportMetadata()
    def pivotFormatImport = new PivotFormatImport()


    def setup() {
        pivotImportMetadata.customFieldIdsMap.put("cuf1", new SquashCustomFieldInfo(1L, InputType.CHECKBOX))
        pivotImportMetadata.customFieldIdsMap.put("cuf2", new SquashCustomFieldInfo(2L, InputType.RICH_TEXT))
        pivotFormatImport.id = 1
    }

    def "should parse a campaign"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/campaign.json")

        when:
            def campaignToImport = executionWorkspaceParser.parseCampaign(jsonParser, pivotImportMetadata, pivotFormatImport)

        then:
            def dateFormat = new SimpleDateFormat("yyyy-MM-dd")

            campaignToImport.internalId == "CP1"
            campaignToImport.name == "Campaign 1"
            campaignToImport.description == "desc CP1"
            campaignToImport.reference == "ref CP1"
            campaignToImport.status == CampaignStatus.UNDEFINED
            campaignToImport.customFields.get(1L).value == "true"
            campaignToImport.customFields.get(2L).value == "my first value"
            dateFormat.format(campaignToImport.scheduledStartDate) == "2024-05-11"
            dateFormat.format(campaignToImport.scheduledEndDate) ==  "2024-05-17"
            dateFormat.format(campaignToImport.actualStartDate) == "2024-05-12"
            dateFormat.format(campaignToImport.actualEndDate) == "2024-05-18"
            campaignToImport.testPlanTestCaseIds == ["TCA", "TCB", "TCD", "TCE"]
            campaignToImport.parentType == EntityType.CAMPAIGN_LIBRARY
            campaignToImport.attachments[0].zipImportFileName == "AT02-attachment.gif"
            campaignToImport.attachments[0].originalFileName == "attachment.gif"
    }

    def "should parse an iteration"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/iteration.json")

        when:
            def iterationToImport = executionWorkspaceParser.parseIteration(jsonParser, pivotImportMetadata, pivotFormatImport)

        then:
            def dateFormat = new SimpleDateFormat("yyyy-MM-dd")

            iterationToImport.internalId == "IT1"
            iterationToImport.name == "Iteration 1"
            iterationToImport.description == "desc IT1"
            iterationToImport.reference == "ref IT1"
            iterationToImport.status == IterationStatus.PLANNED
            iterationToImport.customFields.get(2L).value == "my first value"
            dateFormat.format(iterationToImport.scheduledStartDate) == "2024-04-11"
            dateFormat.format(iterationToImport.scheduledEndDate) ==  "2024-04-17"
            iterationToImport.actualStartAuto == true
            iterationToImport.actualEndAuto == true
            iterationToImport.testPlanTestCaseIds == ["TCA", "TCB"]
            iterationToImport.parentId == "CP1"
            iterationToImport.attachments[0].zipImportFileName == "AT01-attachment.txt"
            iterationToImport.attachments[0].originalFileName == "attachment.txt"
    }

    def "should parse a test suite"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/test_suite.json")

        when:
            def testSuiteToImport = executionWorkspaceParser.parseTestSuite(jsonParser, pivotImportMetadata, pivotFormatImport)

        then:

            testSuiteToImport.internalId == "TS1"
            testSuiteToImport.name == "Test Suite 1"
            testSuiteToImport.description == "desc TS1"
            testSuiteToImport.executionStatus == ExecutionStatus.READY
            testSuiteToImport.customFields.get(1L).value == "true"
            testSuiteToImport.testPlanTestCaseIds == ["TCA", "TCB", "TCD"]
            testSuiteToImport.parentId == "IT1"
            testSuiteToImport.attachments[0].zipImportFileName == "AT10-attachment.xml"
            testSuiteToImport.attachments[0].originalFileName == "attachment.xml"
    }

    def "should parse an execution"() {
        given:
            def jsonParser = JsonParserTestHelper.getJsonParserFromFilePath("projectimport/pivotimporter/execution.json")
        when:
            def executionToImport = executionWorkspaceParser.parseExecution(jsonParser, pivotImportMetadata, pivotFormatImport)

        then:
            def firstStep = executionToImport.executionSteps.get(0)
            def secondStep = executionToImport.executionSteps.get(1)

            executionToImport.internalId == "EX1"
            executionToImport.comment == "a comment"
            executionToImport.executionStatus == ExecutionStatus.FAILURE
            executionToImport.customFields.get(1L).value == "true"
            executionToImport.testCaseId == "TCA"
            executionToImport.datasetId == "DS1"
            executionToImport.parentId == "IT1"
            executionToImport.parentType == EntityType.ITERATION
            executionToImport.attachments[0].zipImportFileName == "AT04-attachment.odp"
            executionToImport.attachments[0].originalFileName == "attachment.odp"

            firstStep.testStepId == "AS1"
            firstStep.status == ExecutionStatus.SUCCESS
            firstStep.comment == "a step comment 1"
            firstStep.customFields.get(1L).value == "true"
            firstStep.customFields.get(2L).value == "cuf value 1"
            firstStep.attachments[0].zipImportFileName == "AT05-attachment.ppt"
            firstStep.attachments[0].originalFileName == "attachment.ppt"

            secondStep.testStepId == "AS2"
            secondStep.status == ExecutionStatus.FAILURE
            secondStep.comment == "a step comment 2"
            secondStep.customFields.size() == 0
            secondStep.attachments.size() == 0
    }

}
