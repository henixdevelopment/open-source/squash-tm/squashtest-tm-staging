/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.execution

import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.exception.UnknownEntityException
import org.squashtest.tm.domain.execution.SessionNoteKind
import org.springframework.transaction.annotation.Transactional
import spock.unitils.UnitilsSupport
import javax.inject.Inject
import org.unitils.dbunit.annotation.DataSet

@UnitilsSupport
@Transactional
@DataSet
class SessionNoteServiceIT extends DbunitServiceSpecification {

    @Inject
    SessionNoteService sessionNoteService

    def "should find a project from a session note id"() {
        when:
        def project = sessionNoteService.findProjectBySessionNoteId(-1)

        then:
        project.id == -1
        project.name == "project"
    }

    def "should throw when trying to find a project with a non existing session note id"() {
        when:
        def project = sessionNoteService.findProjectBySessionNoteId(-14)

        then:
        thrown UnknownEntityException
    }

    def "should find a session note by id"() {
        when:
        def sessionNote = sessionNoteService.findSessionNoteById(-1)

        then:
        sessionNote.id == -1
        sessionNote.execution.id == -1
        sessionNote.kind == SessionNoteKind.SUGGESTION
        sessionNote.content == "A suggestion"
        sessionNote.createdBy == "dbunit"
    }
}
