/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.ManagementMode
import org.squashtest.tm.exception.campaign.SprintNotLinkableException
import org.squashtest.tm.domain.campaign.SprintStatus
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao
import org.squashtest.tm.service.internal.repository.RequirementDao
import org.squashtest.tm.service.internal.repository.RequirementVersionDao
import org.squashtest.tm.service.internal.dto.SprintReqVersionsBindingExceptionSummary
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
class SprintManagerServiceIT extends DbunitServiceSpecification {

	@Inject
	private SprintManagerService sprintService

    @Inject
    private SprintReqVersionDao sprintReqVersionDao

    @Inject
    RequirementDao requirementDao

    @Inject
    RequirementVersionDao requirementVersionDao

    def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
        def reqVer = requirementVersionDao.getReferenceById(reqVersionId)
        def req = requirementDao.findById(reqId)
        reqVer.setRequirement(req)
    }

    def setup() {
        def ids = [
            [-1L, -1L], [-2L, -2L]
        ]
        ids.each {
            setBidirectionalReqReqVersion(it[0], it[1])
        }
    }

	@DataSet("SprintManagerServiceIT.should not bind a req to a sprint if already bound.xml")
	def "should not bind a requirement to a sprint if it is already bound"() {
		given:
        Long[] requirementIds = [-1L]
        SprintReqVersionsBindingExceptionSummary summary = new SprintReqVersionsBindingExceptionSummary()

		when:
        def reqIdsToSave = sprintService.bindRequirementsByRlnIds(-1L, Arrays.asList(requirementIds), summary)

		then:
        !reqIdsToSave.contains(-1)
	}

    @DataSet("SprintManagerServiceIT.should not bind a hlr to a sprint.xml")
    def "should not bind a high level requirement to a sprint"() {
        given:
        Long[] requirementIds = [-1L]
        SprintReqVersionsBindingExceptionSummary summary = new SprintReqVersionsBindingExceptionSummary()

        when:
        def reqIdsToSave = sprintService.bindRequirementsByRlnIds(-1L, Arrays.asList(requirementIds), summary)

        then:
        !reqIdsToSave.contains(-1)
    }

    @DataSet("SprintManagerServiceIT.should bind a req to a sprint and fetch it.xml")
    def "should bind a requirement to a sprint by rlnId and fetch it back"() {
        given:
        Long[] rlnIds = [-1L, -2L]
        SprintReqVersionsBindingExceptionSummary summary = new SprintReqVersionsBindingExceptionSummary()

        when:
        sprintService.bindRequirementsByRlnIds(-1L, Arrays.asList(rlnIds), summary)

        then:
        def sprintReqVersions = sprintReqVersionDao.findAllBySprintIdAndRequirementVersionIdInOrderByRequirementVersionId(-1L, Arrays.asList(rlnIds))
        sprintReqVersions.size() == 2
        sprintReqVersions[0].requirementVersion.id == -2
        sprintReqVersions[0].sprint.id == -1
        sprintReqVersions[1].requirementVersion.id == -1
        sprintReqVersions[1].sprint.id == -1
    }

    @DataSet("SprintManagerServiceIT.should throw an exception if bind a req to a closed sprint.xml")
    def "should throw an exception if bind a rlnId to a closed sprint"() {
        given:
        Long[] rlnIds = [-1L, -2L]
        SprintReqVersionsBindingExceptionSummary summary = new SprintReqVersionsBindingExceptionSummary()

        when:
        sprintService.bindRequirementsByRlnIds(-1L, Arrays.asList(rlnIds), summary)

        then:
        thrown SprintNotLinkableException
    }

    @DataSet("SprintManagerServiceIT.should bind a req to a sprint and fetch it.xml")
    def "should bind a requirement to a sprint by reqVersionId and fetch it back"() {
        given:
        Long[] rlnIds = [-1L, -2L]
        SprintReqVersionsBindingExceptionSummary summary = new SprintReqVersionsBindingExceptionSummary()

        when:
        sprintService.bindRequirementsByReqVersionIds(-1L, Arrays.asList(rlnIds), summary)

        then:
        def sprintReqVersions = sprintReqVersionDao.findAllBySprintIdAndRequirementVersionIdInOrderByRequirementVersionId(-1L, Arrays.asList(rlnIds))
        sprintReqVersions.size() == 2
        sprintReqVersions[0].requirementVersion.id == -2
        sprintReqVersions[0].sprint.id == -1
        sprintReqVersions[1].requirementVersion.id == -1
        sprintReqVersions[1].sprint.id == -1
    }

    @DataSet("SprintManagerServiceIT.should populate test plan.xml")
    def "should populate sprint req version test plan when adding a requirement"() {
        given:
        Long[] rlnIds = [-1L]
        SprintReqVersionsBindingExceptionSummary summary = new SprintReqVersionsBindingExceptionSummary()

        when:
        sprintService.bindRequirementsByReqVersionIds(-1L, Arrays.asList(rlnIds), summary)

        then:
        def sprintReqVersions = sprintReqVersionDao.findAllBySprintIdAndRequirementVersionIdInOrderByRequirementVersionId(-1L, Arrays.asList(rlnIds))
        sprintReqVersions.size() == 1
        sprintReqVersions[0].requirementVersion.id == -1
        sprintReqVersions[0].sprint.id == -1
        sprintReqVersions[0].testPlan.getTestPlanItems().size() == 3 // 1 for TC1, 2 for TC2 and its datasets
    }

    @DataSet("SprintManagerServiceIT.should throw an exception if bind a req to a closed sprint.xml")
    def "should throw an exception if bind a reqVersionId to a closed sprint"() {
        given:
        Long[] rlnIds = [-1L, -2L]
        SprintReqVersionsBindingExceptionSummary summary = new SprintReqVersionsBindingExceptionSummary()

        when:
        sprintService.bindRequirementsByReqVersionIds(-1L, Arrays.asList(rlnIds), summary)

        then:
        thrown SprintNotLinkableException
    }

    @DataSet("SprintManagerServiceIT.should remove a sprintReqVersion.xml")
    def "should remove a sprintReqVersion"() {
        given:
        def sprintReqVersionIds = [-1L, -2L]
        def reqVersionsIds = [-1L, -2L, -3L]
        def sprintId = -1L

        when:
        sprintService.deleteSprintReqVersions(sprintId, sprintReqVersionIds)

        then:
        def sprintReqVersions =
                sprintReqVersionDao.findAllBySprintIdAndRequirementVersionIdInOrderByRequirementVersionId(sprintId, reqVersionsIds)
        sprintReqVersions.size() == 1
        sprintReqVersions[0].requirementVersion.id == -3
    }

    @DataSet("SprintManagerServiceIT.should throw an exception if trying to delete a sprintReqVersion from a closed sprint.xml")
    def "should throw an exception if trying to delete a sprintReqVersion from a closed sprint"() {
        given:
        def sprintReqVersionIds = [-1L, -2L]
        def sprintId = -1L

        when:
        sprintService.deleteSprintReqVersions(sprintId, sprintReqVersionIds)

        then:
        thrown SprintNotLinkableException
    }

    @DataSet("SprintManagerServiceIT.should find a sprint by its id.xml")
    def "should find a sprint by its id"() {
        given:
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")
        def sprintId = -1L

        when:
        def sprint = sprintService.findById(sprintId)

        then:
        sprint.name == "a sprint"
        sprint.reference == "test"
        sprint.startDate == dateFormat.parse("2024-01-01")
        sprint.endDate == dateFormat.parse("2024-01-12")
        sprint.status == SprintStatus.UPCOMING
        def sprintReqVersions = sprint.sprintReqVersions.sort { it.id }
        sprintReqVersions.size() == 3
        sprintReqVersions[0].id == -3
        sprintReqVersions[1].id == -2
        sprintReqVersions[2].id == -1
    }

    @DataSet("SprintManagerServiceIT.should delete sprint synchronisation from a sprint.xml")
    def "should delete sprint synchronisation from a sprint"() {
       when:
        sprintService.deleteSprintSynchronisation(-1L)

        then:
        def sprint = sprintService.findById(-1L)
        sprint.remoteSynchronisation == null
        sprint.remoteSprintId == null
        sprint.remoteName == null
        sprint.remoteState == null
        sprint.sprintReqVersions[0].name == null
        sprint.sprintReqVersions[0].mode == ManagementMode.NATIVE
        sprint.sprintReqVersions[1].name == null
        sprint.sprintReqVersions[1].mode == ManagementMode.NATIVE
        sprint.sprintReqVersions[2].name == null
        sprint.sprintReqVersions[2].mode == ManagementMode.NATIVE
    }
}
