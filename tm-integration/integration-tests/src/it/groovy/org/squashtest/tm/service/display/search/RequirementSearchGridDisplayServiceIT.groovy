/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport
import org.squashtest.tm.service.internal.display.grid.GridRequest

import javax.inject.Inject
import javax.persistence.EntityManager

@UnitilsSupport
@Transactional
@DataSet
class RequirementSearchGridDisplayServiceIT extends DbunitServiceSpecification {

	def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
		def reqVer = entityManager.find(RequirementVersion.class, reqVersionId)
		def req = entityManager.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		// Flush is necessary as the dao use native query through jooq.
		entityManager.flush()
	}

	def setup() {
		def ids = [
			[-2L, -2L],
			[-3L, -3L],
			[-3L, -33L],
			[-4L, -4L],
			[-5L, -5L],
			[-5L, -15L],
			[-7L, -7L],
		]
		ids.each {
			setBidirectionalReqReqVersion(it[0], it[1])
		}
	}

	@Inject
	private RequirementSearchGridDisplayService service

	@Inject
	EntityManager entityManager


	def "should return a grid response"() {

		given:
		def ids = [-33L, -5L, -4L, -15L]
		def researchResult = new ResearchResult(ids, 4)
        def gridRequest = new GridRequest()
        def gridId = "requirement-search"
        gridRequest.setSimplifiedColumnDisplayGridIds([])
        gridRequest.setGridId(gridId)

		when:
		def response = service.fetchResearchRows(researchResult,gridRequest)

		then:
		response.count == 4
		response.dataRows.collect { it.data["id"] } == [-33L, -5L, -4L, -15L]
		response.dataRows.collect { it.data["name"] } == ["Requirement_2_v1","Requirement_4", "Requirement_3", "Requirement_4_milestone"]
		def dataRows = response.dataRows
		def firstRequirementVersion = dataRows[0]
		firstRequirementVersion.data["id"] == -33L
		firstRequirementVersion.data["reference"] == "ref-1"
		firstRequirementVersion.data["status"] == "UNDER_REVIEW"
		firstRequirementVersion.data["criticality"] == "CRITICAL"
		firstRequirementVersion.data["createdBy"] == "admin"
        firstRequirementVersion.data["projectId"] == -1L
        firstRequirementVersion.data["nature"] == "STANDARD"
        firstRequirementVersion.data["description"] == false
        firstRequirementVersion.data["hasLinkType"] == false
		firstRequirementVersion.data["milestones"] == null
		firstRequirementVersion.data["reqMilestoneLocked"] == null
		firstRequirementVersion.data["versionNumber"] == 0
		firstRequirementVersion.data["versionsCount"] == 2
		firstRequirementVersion.data["coverages"] == 2
		firstRequirementVersion.data["attachments"] == 2
        firstRequirementVersion.data["cuf|-6"] == "Lille, Roubaix, Tourcoing"
        firstRequirementVersion.data["cuf|-5"] == "true"
        firstRequirementVersion.data["cuf|-1"] == "abcde"

        def otherRequirement = dataRows[1]
        otherRequirement.data["id"] == -5L
        otherRequirement.data["hasLinkType"] == true
        otherRequirement.data["hasParent"] == true
        otherRequirement.data["nature"] == "HIGH_LEVEL"

        def anotherRequirement = dataRows[2]
        anotherRequirement.data["id"] == -4L
        anotherRequirement.data["childOfRequirement"] == 1

        def lastRequirementVersion = dataRows[3]
		lastRequirementVersion.data["id"] == -15L
		lastRequirementVersion.data["reference"] == ""
        lastRequirementVersion.data["nature"] == "HIGH_LEVEL"
        lastRequirementVersion.data["description"] == true
		lastRequirementVersion.data["status"] == "WORK_IN_PROGRESS"
		lastRequirementVersion.data["criticality"] == "UNDEFINED"
		lastRequirementVersion.data["milestones"] == 2
		lastRequirementVersion.data["reqMilestoneLocked"] == 1
        lastRequirementVersion.data["milestoneLabels"] == "Locked, My milestone"
        lastRequirementVersion.data["milestoneStatus"] == "LOCKED, PLANNED"
        lastRequirementVersion.data["milestoneEndDate"] == "2014-10-10 00:00:00, 2014-10-10 00:00:00"
		lastRequirementVersion.data["versionNumber"] == 0
		lastRequirementVersion.data["versionsCount"] == 2
		lastRequirementVersion.data["coverages"] == null
		lastRequirementVersion.data["attachments"] == 0
	}
}
