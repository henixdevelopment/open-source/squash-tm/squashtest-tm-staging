/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport
import javax.inject.Inject

@UnitilsSupport
@DataSet
class CampaignDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	CampaignDisplayDao campaignDisplayDao


	@Unroll
	def "should fetch number of test plan items in campaign"() {
		given:
		when:
		def count = campaignDisplayDao.getNbTestPlanItem(campaignId, login)
		then:
		count == expectedResult

		where:

		campaignId 	| login 	|| expectedResult
		-11L 		| null		|| 6
		-11L 		| "JP01"	|| 3
	}

    @Unroll
    def "should get the project ids by selected campaign library nodes"() {
        given:
        when:
        def projectIds = campaignDisplayDao.findDistinctProjectIdsByCampaignLibraryIds(campaignLibraryIds as Set)
        then:
        projectIds.containsAll(expectedResult)

        where:

        campaignLibraryIds        || expectedResult
        [-1L, -3L]                || [-3L, -1L]
        [-2L, -1L]                || [-2L, -1L]
    }

    @Unroll
    def "should find the distinct project ids by selected campaign nodes"() {
        given:
        when:
        def projectIds = campaignDisplayDao.findDistinctProjectIdsByCampaignLibraryNodeIds(campaignNodeIds as Set)
        then:
        projectIds.containsAll(expectedResult)

        where:

        campaignNodeIds     || expectedResult
        [-1L, -11L]         || [-1L]
        [-12L, -10L]        || [-2L, -1L,]
    }

    @Unroll
    def "should find all disabled execution statuses in perimeter by Campaign Library Nodes"() {
        given:
        when:
        def disabledExecutionStatusList =
            campaignDisplayDao.findAllDisabledExecutionStatusByProjectIds(projectIds as Set)
        then:
        disabledExecutionStatusList.containsAll(expectedResult)

        where:

        projectIds          || expectedResult
        [-1L]               || ["SETTLED"]
        [-2L]               || ["SETTLED", "UNTESTABLE"]
        [-1L, -2L]          || ["SETTLED", "UNTESTABLE", "SETTLED"]
        [-3L]               || []
        [-1L, -3L]          || ["SETTLED"]
    }
}
