/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.execution

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.exception.campaign.SprintClosedException
import org.squashtest.tm.service.display.execution.ExecutionDisplayService
import org.squashtest.tm.service.security.UserContextService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
class ExploratorySessionOverviewModificationServiceIT extends DbunitServiceSpecification {

    @Inject
    ExploratorySessionOverviewModificationService sessionOverviewService

    @Inject
    ExecutionProcessingService executionService

    @Inject
    UserContextService userContextService

    @Inject
    ExecutionDisplayService executionDisplayService

    def "should update due date"() {
        given:
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy")
        Date date = formatter.parse("25-12-2023")

        when:
        sessionOverviewService.updateDueDate(-1L, date)

        then:
        ExploratorySessionOverview overview = findEntity(ExploratorySessionOverview.class, -1L)
        overview.getDueDate() == date
    }

    def "should update ITPI execution status"() {
        when:
        sessionOverviewService.updateExecutionStatus(-1L, "READY")

        then:
        ExploratorySessionOverview overview = findEntity(ExploratorySessionOverview.class, -1L)
        overview.iterationTestPlanItem.executionStatus == ExecutionStatus.READY
        overview.iterationTestPlanItem.lastExecutedBy == userContextService.getUsername()
        final long timeDelta = overview.iterationTestPlanItem.lastExecutedOn.getTime() - new Date().getTime()
        Math.abs(timeDelta) < 1000
    }

    def "should update session duration"() {
        when:
        sessionOverviewService.updateSessionDuration(-1L, 90)

        then:
        ExploratorySessionOverview overview = findEntity(ExploratorySessionOverview.class, -1L)
        overview.sessionDuration == 90
    }

    def "should add execution"() {
        when:
        sessionOverviewService.addNewExecution(-1L)

        then:
        ExploratorySessionOverview overview = findEntity(ExploratorySessionOverview.class, -1L)
        overview.iterationTestPlanItem.executions.size() == 2
    }

    def "should start session"() {
        when:
        sessionOverviewService.startSessionAndUpdateExecutionStatus(-1L)

        then:
        ExploratorySessionOverview overview = findEntity(ExploratorySessionOverview.class, -1L)
        IterationTestPlanItem itpi = findEntity(IterationTestPlanItem.class, -1L)
        overview.sessionStatus == 'RUNNING'
        itpi.executionStatus == ExecutionStatus.RUNNING
    }

    def "should end session"() {
        when:
        sessionOverviewService.endSession(-1L)

        then:
        ExploratorySessionOverview overview = findEntity(ExploratorySessionOverview.class, -1L)
        overview.sessionStatus == 'FINISHED'
    }

    def "should update exploratory execution info after creation to fit session overview"() {
        when:
        sessionOverviewService.addNewExecution(-1L)

        then:
        ExploratorySessionOverview sessionOverview = findEntity(ExploratorySessionOverview.class, -1L)
        def executionId = sessionOverview.getIterationTestPlanItem().getExecutions().get(1).getId()
        Execution execution = findEntity(Execution.class, executionId)

        execution.getName() == "ref2 - our_first_session"
        execution.getReference() == "ref2"
    }

    def "should add several executions with their assignee user at the same time" () {
        when:
        sessionOverviewService.addExecutionsWithUsers(-1L, [-1L, -2L, -3L, -4L, -5L])

        then:
        ExploratorySessionOverview sessionOverview = findEntity(ExploratorySessionOverview.class, -1L)
        sessionOverview.getIterationTestPlanItem().getExecutions().size() == 6

        def executions = sessionOverview.getIterationTestPlanItem().getExecutions()
        def assigneeUsers = executions.collect { it.getAssigneeUser()?.getLogin() }
        assigneeUsers.containsAll(["user1", "user2", "user3", "user4", "user5", null])
    }


    def "should update exploratory execution when updating session overview" () {
        when:
        sessionOverviewService.updateSessionDuration(-1L, 60000)
        def view = executionDisplayService.findOne(-1L)

        then:
        view.exploratorySessionOverviewInfo.sessionDuration == 60000
    }

    @DataSet("ExploratorySessionOverviewModificationServiceIT.should-not-be-able-to-perform-operations-in-a-closed-sprint.xml")
    def "should not be able to perform operations if the session is in a closed sprint" () {
        when:
        sessionOverviewService.updateSessionDuration(-45L, 3000)

        then:
        thrown SprintClosedException
    }
}
