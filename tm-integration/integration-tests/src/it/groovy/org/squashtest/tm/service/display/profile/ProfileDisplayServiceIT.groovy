/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.profile

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.acl.AclGroup
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.service.internal.display.dto.PermissionsDto
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityNotFoundException

import static org.squashtest.tm.api.security.acls.Permissions.IMPORT
import static org.squashtest.tm.api.security.acls.Permissions.MANAGE_MILESTONE
import static org.squashtest.tm.api.security.acls.Permissions.MANAGE_PROJECT
import static org.squashtest.tm.api.security.acls.Permissions.MANAGE_PROJECT_CLEARANCE


@UnitilsSupport
@Transactional
@DataSet
class ProfileDisplayServiceIT extends DbunitServiceSpecification {

    @Inject	private ProfileDisplayService profileDisplayService


    def "should find all profiles"() {
        when:
            def profiles = profileDisplayService.findAll()
        then:
            profiles.size() == 9

            def testEditor = profiles.get(0)
            testEditor.qualifiedName == "squashtest.acl.group.tm.TestEditor"
            testEditor.partyCount == 0
            testEditor.active
            AclGroup.isSystem(testEditor.qualifiedName)

            def projectManager = profiles.get(2)
            projectManager.qualifiedName == "squashtest.acl.group.tm.ProjectManager"
            projectManager.partyCount == 2
            projectManager.active
            AclGroup.isSystem(projectManager.qualifiedName)

            def customProfile = profiles.get(8)
            customProfile.qualifiedName == "Auditor"
            customProfile.partyCount == 0
            customProfile.active
            !AclGroup.isSystem(customProfile.qualifiedName)
    }

    def "should display a profile by id"() {
        when:
            def profileView = profileDisplayService.getProfileView(5L)
        then:
            profileView.qualifiedName == "squashtest.acl.group.tm.ProjectManager"
            profileView.partyCount == 2
            profileView.active
            profileView.description == "Project Manager description"
            profileView.createdBy == "Upgrade Squash 8.0"
            AclGroup.isSystem(profileView.qualifiedName)

            checkProjectPermissionsForProjectManager(profileView.permissions)

            profileView.partyProfileAuthorizations.size() == 2
    }

    def "should throw exception when profile does not exist"() {
        when:
            profileDisplayService.getProfileView(99)
        then:
            thrown(EntityNotFoundException)
    }

    def "should find profile permissions"() {
        when:
            def profilePermissions = profileDisplayService.getPermissions(5L)
        then:
            checkProjectPermissionsForProjectManager(profilePermissions)
    }

    def "should find permissions for all profiles"() {
        when:
        def profilePermissions = profileDisplayService.getProfilesAndPermissions()
        then:
        profilePermissions.size() == 1
        def projectManagerPermissions = profilePermissions.find({ it.profileId() == 5L }).permissions()
        checkProjectPermissionsForProjectManager(projectManagerPermissions)
    }

    private static void checkProjectPermissionsForProjectManager(List<PermissionsDto> profilePermissions) {
        def projectPermissions = profilePermissions.find({ it.simplifiedClassName() == Project.class.getSimpleName() })
        projectPermissions.permissions().size() == 4
        projectPermissions.permissions() == [MANAGE_PROJECT.name(), MANAGE_MILESTONE.name(), MANAGE_PROJECT_CLEARANCE.name(), IMPORT.name()]
    }
}
