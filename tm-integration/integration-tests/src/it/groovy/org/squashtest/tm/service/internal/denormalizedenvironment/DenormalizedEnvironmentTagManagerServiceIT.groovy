/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.denormalizedenvironment

import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentTag
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentTagManagerService
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao
import org.squashtest.tm.service.internal.repository.DenormalizedEnvironmentTagDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.transaction.Transactional

@org.springframework.transaction.annotation.Transactional
@UnitilsSupport
@DataSet("DenormalizedEnvironmentManagerService.xml")
class DenormalizedEnvironmentTagManagerServiceIT extends  DbunitServiceSpecification {

	@Inject
	DenormalizedEnvironmentTagManagerService denormalizedEnvironmentTagManagerService

	@Inject
	DenormalizedEnvironmentTagDao denormalizedEnvironmentTagDao

	@Inject
	AutomatedExecutionExtenderDao executionExtenderDao

	def "should create denormalized environment tags for automated execution"() {

		given:
		Long executionExtenderId = -1L

		List<String> environmentTagValues = Arrays.asList("robotframework", "linux")

		AutomatedExecutionExtender executionExtender = executionExtenderDao.findById(executionExtenderId)

		when:
		denormalizedEnvironmentTagManagerService.createAllDenormalizedEnvironmentTagsForAutomatedExecution(environmentTagValues, executionExtender)

		then:
		List<DenormalizedEnvironmentTag> result = denormalizedEnvironmentTagDao.findAllByHolderIdAndHolderType(-1L, DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER)
		result.size() == 2

	}

	def "should delete all denormalized environment tags when deleting an execution"() {
		given:
		Long executionExtenderId = -3L
		DenormalizedEnvironmentHolderType holderType = DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER
		AutomatedExecutionExtender automatedExecutionExtender = executionExtenderDao.findById(executionExtenderId)
		List<DenormalizedEnvironmentTag> denormalizedTags = denormalizedEnvironmentTagDao.findAllByHolderIdAndHolderType(executionExtenderId, holderType)
		List<Long> ids = denormalizedTags.collect {it.id}

		when:
		denormalizedEnvironmentTagManagerService.removeDenormalizedTagsOnExecutionDelete(automatedExecutionExtender)

		then:
		List<DenormalizedEnvironmentTag> result = denormalizedEnvironmentTagDao.findAllById(ids)
		result.isEmpty()
	}
}
