/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class CustomRemoteSynchronisationDaoIT extends DbunitDaoSpecification {
    @Inject
    CustomRemoteSynchronisationDao dao

    def "should find all req sync extender keys from a remote synchronisation"() {
        when:
        def keys = dao.findAllReqSyncExtenderKeys(remoteSyncId)

        then:
        keys.size() == result
        keys.containsAll()

        where:
        remoteSyncId | result | expectedKeys
        -1L          | 2      | ["JIRA-1", "JIRA-2"]
        -2L          | 1      | ["JIRA-3"]
        -3L          | 0      | []
    }

    def "should find active remote synchronisation ids by plugin id"() {
        when:
        def ids = dao.findActiveRemoteSyncIdsByPluginId(pluginId)

        then:
        ids.size() == result
        ids.containsAll(expectedIds)

        where:
        pluginId                          | result | expectedIds
        "squash.tm.plugin.jirasync"       | 2      | [-1L, -2L]
        "squash.tm.plugin.xsquash4gitlab" | 1      | [-3L]
        "wrong id"                        | 0      | []
    }
}
