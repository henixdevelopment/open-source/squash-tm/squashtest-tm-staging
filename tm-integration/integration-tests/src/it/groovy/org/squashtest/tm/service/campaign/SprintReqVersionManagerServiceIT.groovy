/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.springframework.transaction.annotation.Transactional
import spock.unitils.UnitilsSupport
import javax.inject.Inject
import org.unitils.dbunit.annotation.DataSet
import org.squashtest.tm.domain.requirement.ManagementMode

@UnitilsSupport
@Transactional
@DataSet
class SprintReqVersionManagerServiceIT extends DbunitServiceSpecification {

    @Inject
    SprintReqVersionManagerService sprintReqVersionManagerService

    def "should find a sprint req version from its id"() {
        when:
        def sprintReqVersion = sprintReqVersionManagerService.findById(-1)

        then:
        sprintReqVersion.id == -1
        sprintReqVersion.requirementVersion.id == -1
        sprintReqVersion.sprint.name == "a sprint"
        sprintReqVersion.reference == "a reference"
        sprintReqVersion.name == "a first requirement"
        sprintReqVersion.status == "UNDER_REVIEW"
        sprintReqVersion.criticality == "MINOR"
        sprintReqVersion.mode == ManagementMode.NATIVE
        sprintReqVersion.testPlan.id == -1
    }
}
