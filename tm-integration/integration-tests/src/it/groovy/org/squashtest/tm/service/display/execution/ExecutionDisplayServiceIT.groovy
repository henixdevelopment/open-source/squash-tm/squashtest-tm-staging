/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.execution

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionView
import org.squashtest.tm.service.internal.dto.UserDto
import org.squashtest.tm.service.user.UserAccountService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat
import java.time.LocalDateTime

@Transactional
@DataSet
@UnitilsSupport
class ExecutionDisplayServiceIT extends DbunitServiceSpecification {

    UserAccountService userAccountService = Mock()

	@Inject
	ExecutionDisplayService executionDisplayService

	def "should retrieve executions by test case"() {

		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def response = executionDisplayService.findByTestCaseId(-1L, gridRequest)

		then:
		def rows = response.dataRows
		rows.size() == 3
		rows.get(0).data["executionPath"] == "project > campaign > iteration"

	}

	def "should retrieve executions by test case order by last executed on"() {

		given:
		def gridRequest = new GridRequest()
		gridRequest.sort = Arrays.asList(new GridSort("lastExecutedOn", GridSort.SortDirection.DESC))
		gridRequest.size = 25

		when:
		def response = executionDisplayService.findByTestCaseId(-2L, gridRequest)

		then:
		def rows = response.dataRows
		rows.size() == 3
		def dateRow1 = (Date) rows.get(0).data["lastExecutedOn"]
		def dateRow2 = (Date) rows.get(1).data["lastExecutedOn"]
		def dateRow3 = (Date) rows.get(2).data["lastExecutedOn"]
		dateRow1 > dateRow2
		dateRow2 > dateRow3
	}

    def "should find an exploratory execution by ID"() {
        given:
        UserDto user = Mock()
        user.isAdmin() >> true
        user.getPartyIds() >> [-5L]
        user.getId() >> -1L
        userAccountService.findCurrentUserDto() >> user

        when:
        ExecutionView executionView = executionDisplayService.findOne(-5)

        then:
        executionView.executionMode == "EXPLORATORY"
        executionView.sessionNotes.size() == 4

        def note1 = executionView.sessionNotes[0]
        note1.noteId == -2
        note1.content == "May I ask a question?"
        note1.kind == "QUESTION"

        executionView.sessionNotes[1].noteId == -25
        executionView.sessionNotes[2].noteId == -1
        executionView.sessionNotes[3].noteId == -12

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        executionView.latestExploratoryExecutionEvent.timestamp == dateFormat.parse("2023-08-31 09:18:15")
        executionView.latestExploratoryExecutionEvent.timeElapsed == 165

    }

    def "should find an automated execution by ID"() {
        given:
            UserDto user = Mock()
            user.isAdmin() >> true
            user.getPartyIds() >> [-5L]
            user.getId() >> -1L
            userAccountService.findCurrentUserDto() >> user

        when:
            ExecutionView executionView = executionDisplayService.findOne(-6L)

        then:
            executionView.executionMode == "AUTOMATED"
            executionView.extenderId == -4L
            executionView.automatedExecutionDuration == 1245
            executionView.executionStatus == "SUCCESS"
            executionView.testAutomationServerKind == "squashOrchestrator"
            executionView.denormalizedEnvironmentTags.value == "Robot Framework"
    }

    @DataSet("ExecutionDisplayServiceIT.findCustomFieldsForExploratoryExecution.xml")
    def "should find custom fields for exploratory execution"() {
        given:
        UserDto user = Mock()
        user.isAdmin() >> true
        user.getPartyIds() >> [-5L]
        user.getId() >> -1L
        userAccountService.findCurrentUserDto() >> user

        when:
        ExecutionView executionView = executionDisplayService.findOne(-1)

        then:
        executionView.denormalizedCustomFieldValues.size() == 1
        executionView.customFieldValues.size() == 4

        executionView.denormalizedCustomFieldValues[0].getValue() == "2023-12-31"
        executionView.customFieldValues*.getValue().containsAll(["Equipe citrouille", "4", "Mozilla", "false"])
    }
}
