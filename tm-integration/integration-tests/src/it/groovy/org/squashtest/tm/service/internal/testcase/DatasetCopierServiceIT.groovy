/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.basespecs.QueryCountTracker
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.internal.repository.DatasetDao
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader
import org.squashtest.tm.service.testcase.DatasetCopierService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@NotThreadSafe
@DataSet
@QueryCountTracker
class DatasetCopierServiceIT extends DbunitServiceSpecification {

	@Inject
	private DatasetCopierService customTestCaseModificationService

    @Inject
    private TestCaseLoader testCaseLoader

    @Inject
    private DatasetDao datasetDao

	def "should copy a dataset in same project, in a test case with no parameters"() {

		when:
		customTestCaseModificationService.copyDataset(-1L, -1L, [-2L])
		em.flush()
		em.clear()

		then:
		TestCase testCase =  testCaseLoader.load(-2L,EnumSet.of(TestCaseLoader.Options.FETCH_DATASETS, TestCaseLoader.Options.FETCH_PARAMETERS))

		testCase.getDatasets().size() == 1

        def dataset = datasetDao.loadWithParams(testCase.getDatasets()[0].id)

        dataset.name == 'Dataset'

		testCase.getParameters().size() == 2
		testCase.getParameters()[0].name == 'param2'
		testCase.getParameters()[1].name == 'param1'

		def paramValues = dataset.getParameterValues()
		paramValues.size() == 2
		paramValues[0].getParamValue() == 'fraise' || 'coco'
		paramValues[1].getParamValue() == 'fraise' || 'coco'
	}


	def "should copy a dataset in same project, in a test case with same parameters"() {

		when:
		customTestCaseModificationService.copyDataset(-1L, -1L, [-10L])
		em.flush()
		em.clear()

		then:
		TestCase testCase = testCaseLoader.load(-10L,EnumSet.of(TestCaseLoader.Options.FETCH_DATASETS, TestCaseLoader.Options.FETCH_PARAMETERS))

		testCase.getDatasets().size() == 1

        def dataset = datasetDao.loadWithParams(testCase.getDatasets()[0].id)
		dataset.name == 'Dataset'

		testCase.getParameters().size() == 2
		testCase.getParameters()[0].name == 'param2'
		testCase.getParameters()[1].name == 'param1'

		def paramValues = dataset.getParameterValues()
		paramValues.size() == 2
		paramValues[0].getParamValue() == 'fraise' || 'coco'
		paramValues[1].getParamValue() == 'fraise' || 'coco'

	}

	def "should copy a dataset in another project, in a test case with no parameters"() {

		when:
		customTestCaseModificationService.copyDataset(-1L, -1L, [-6L])
		em.flush()
		em.clear()

		then:
		TestCase testCase = testCaseLoader.load(-6L,EnumSet.of(TestCaseLoader.Options.FETCH_DATASETS, TestCaseLoader.Options.FETCH_PARAMETERS))

		testCase.getDatasets().size() == 1

        def dataset = datasetDao.loadWithParams(testCase.getDatasets()[0].id)
        dataset.name == 'Dataset'

		testCase.getParameters().size() == 2
		testCase.getParameters()[0].name == 'param2'
		testCase.getParameters()[1].name == 'param1'

		def paramValues = dataset.getParameterValues()
		paramValues.size() == 2
		paramValues[0].getParamValue() == 'fraise' || 'coco'
		paramValues[1].getParamValue() == 'fraise' || 'coco'
	}

	def "should not copy a dataset in a test case with different parameters"() {

		when:
		def successReportMap = customTestCaseModificationService.copyDataset(-1L, -1L, [-7L])
		em.flush()
		em.clear()

		then:
		successReportMap.isEmpty();
	}

	def "should add a suffix when trying to copy a dataset with a name that already exists"() {

		when:
		customTestCaseModificationService.copyDataset(-1L, -1L, [-10L])
		customTestCaseModificationService.copyDataset(-1L, -1L, [-10L])
		customTestCaseModificationService.copyDataset(-1L, -1L, [-10L])
		em.flush()
		em.clear()

		then:
		TestCase testCase =  testCaseLoader.load(-10L,EnumSet.of(TestCaseLoader.Options.FETCH_DATASETS))

		testCase.getDatasets().size() == 3
		testCase.getDatasets()[1].name.contains('1')
		testCase.getDatasets()[2].name .contains('2')
	}
}
