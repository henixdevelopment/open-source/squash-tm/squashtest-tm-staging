/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import com.google.common.collect.ListMultimap
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class CustomFieldValueDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	CustomFieldValueDisplayDao dao;

	@DataSet
	@Unroll
	def "Should find custom field values"() {
		given:

		when:
		ListMultimap<Long, CustomFieldValueDto> customFieldValues = dao.findCustomFieldValues(entityType, ids)

		then:
		for (id in ids) {
			def customFieldValuesForEntity = customFieldValues.get(id).sort { it.id }
			def expectedValuesForEntity = expectedValues.get(id).sort { it.id }
			customFieldValuesForEntity.size() == expectedValuesForEntity.size()
			for (int i = 0; i < customFieldValuesForEntity.size(); i++) {
				def customFieldValueDto = customFieldValuesForEntity.get(i)
				def expectedValue = expectedValuesForEntity.get(i)
				assert customFieldValueDto.getId() == expectedValue.id
				assert customFieldValueDto.getBoundEntityId() == expectedValue.boundEntityId
				assert customFieldValueDto.getCufId() == expectedValue.cufId
				assert customFieldValueDto.getValue() == expectedValue.value
				assert customFieldValueDto.getFieldType() == expectedValue.fieldType

			}
		}

		where:
		entityType               | ids            || expectedValues
		BindableEntity.TEST_CASE | null           || []
		BindableEntity.TEST_CASE | []             || []
		BindableEntity.TEST_CASE | [-113L]        || [(-113L):[]]
		BindableEntity.TEST_CASE | [-111L]        || [(-111L): [["id": -1111L, "boundEntityId": -111L, "cufId": -1L, "value": "", "fieldType":"CF"], ["id": -1112L, "boundEntityId": -111L, "cufId": -2L, "value": "true", "fieldType":"CF"]]]
		BindableEntity.TEST_CASE | [-111L, -112L] || [
			(-111L): [["id": -1111L, "boundEntityId": -111L, "cufId": -1L, "value": "","fieldType":"CF"], ["id": -1112L, "boundEntityId": -111L, "cufId": -2L, "value": "true","fieldType":"CF"]],
			(-112L): [["id": -1121L, "boundEntityId": -112L, "cufId": -1L, "value": "SEC-2","fieldType":"CF"], ["id": -1122L, "boundEntityId": -112L, "cufId": -2L, "value": "false","fieldType":"CF"]]]
		BindableEntity.TEST_CASE | [-114L]        || [
			(-114L): [["id": -1141L, "boundEntityId": -114L, "cufId": -3L, "value": "tag0|toto|ahaha|value0","fieldType":"TAG",],["id": -1142L, "boundEntityId": -114L, "cufId": -1L, "value": "hello","fieldType":"CF"]],
		]
	}
}
