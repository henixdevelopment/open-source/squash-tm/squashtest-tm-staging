/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration

import org.jooq.DSLContext
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@UnitilsSupport
@DataSet
class ConnectionLogGridIT extends DbunitServiceSpecification {

    @Inject
    DSLContext dslContext

    def "should fetch connection log grid with default order"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 5
        gridRequest.page = 0
        def grid = new ConnectionLogGrid()

        when:
        def response = grid.getRows(gridRequest, dslContext)

        then:
        response.count == 8
        response.dataRows.size() == 5

        checkRow(response.dataRows.get(0), "user5", "2023-10-22T09:30:00", true)
        checkRow(response.dataRows.get(1), "user4", "2023-10-22T09:00:00", false)
        checkRow(response.dataRows.get(2), "user1", "2023-10-21T09:15:00", true)
        checkRow(response.dataRows.get(3), "user3", "2023-10-21T09:00:00", true)
        checkRow(response.dataRows.get(4), "user2", "2023-10-20T09:45:00", true)
    }

    def "should fetch connection log grid with custom sorting"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 5
        gridRequest.page = 0
        gridRequest.sort = [
            new GridSort("connectionDate", GridSort.SortDirection.ASC)
        ]
        def grid = new ConnectionLogGrid()

        when:
        def response = grid.getRows(gridRequest, dslContext)

        then:
        response.count == 8
        response.dataRows.size() == 5

        checkRow(response.dataRows.get(0), "user1", "2023-10-20T09:30:00", false)
        checkRow(response.dataRows.get(1), "user1", "2023-10-20T09:30:20", false)
        checkRow(response.dataRows.get(2), "user1", "2023-10-20T09:30:50", true)
        checkRow(response.dataRows.get(3), "user2", "2023-10-20T09:45:00", true)
        checkRow(response.dataRows.get(4), "user3", "2023-10-21T09:00:00", true)
    }

    def "should fetch connection log with filtering"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 5
        gridRequest.page = 0

        def filterValue = new GridFilterValue()
        filterValue.id = "login"
        filterValue.operation = "LIKE"
        filterValue.values = ["user1"]
        gridRequest.filterValues = [filterValue]

        def grid = new ConnectionLogGrid()

        when:
        def response = grid.getRows(gridRequest, dslContext)

        then:
        response.count == 4
        response.dataRows.size() == 4

        checkRow(response.dataRows.get(0), "user1", "2023-10-21T09:15:00", true)
        checkRow(response.dataRows.get(1), "user1", "2023-10-20T09:30:50", true)
        checkRow(response.dataRows.get(2), "user1", "2023-10-20T09:30:20", false)
        checkRow(response.dataRows.get(3), "user1", "2023-10-20T09:30:00", false)
    }

    def "should fetch connection log grid with out of bounds page"() {
        given:
        def gridRequest = new GridRequest()
        gridRequest.size = 5
        gridRequest.page = 2
        def grid = new ConnectionLogGrid()

        when:
        def response = grid.getRows(gridRequest, dslContext)

        then:
        response.count == 8
        response.page == 1
        response.dataRows.size() == 3

        checkRow(response.dataRows.get(0), "user1", "2023-10-20T09:30:50", true)
        checkRow(response.dataRows.get(1), "user1", "2023-10-20T09:30:20", false)
        checkRow(response.dataRows.get(2), "user1", "2023-10-20T09:30:00", false)
    }

    def checkRow(row, login, connectionDate, success) {
        def dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
        with(row) {
            data["login"] == login
            data["connectionDate"].toLocalDateTime() == LocalDateTime.parse(connectionDate, dateTimeFormatter)
            data["success"] == success
        }
        true // to avoid false failure in "then:" block
    }
}
