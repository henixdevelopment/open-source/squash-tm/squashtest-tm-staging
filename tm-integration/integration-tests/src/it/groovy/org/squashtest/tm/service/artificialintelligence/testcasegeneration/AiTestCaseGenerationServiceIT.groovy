/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.artificialintelligence.testcasegeneration

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseLibrary
import org.squashtest.tm.service.internal.dto.ActionTestStepFromAiDto
import org.squashtest.tm.service.internal.dto.TestCasesCreationFromAiDto
import org.squashtest.tm.service.internal.dto.TestCaseFromAiDto
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao
import org.squashtest.tm.service.internal.repository.TestCaseDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class AiTestCaseGenerationServiceIT extends DbunitServiceSpecification {

    @Inject
    private AiTestCaseGenerationService aiTestCaseGenerationService

    @Inject
    private TestCaseDao testCaseDao

    @Inject
    private CustomFieldValueDisplayDao customFieldValueDisplayDao

    def "should create two test cases from a requirement using artificial intelligence"() {
        given:
        Long projectId = -1L

        TestCasesCreationFromAiDto testCasesData = new TestCasesCreationFromAiDto();
        testCasesData.setTargetProjectId(projectId)
        testCasesData.setDestinationFolderId('TestCaseLibrary--1')

        ActionTestStepFromAiDto actionTestStepFromAiDto1 = new ActionTestStepFromAiDto()
        actionTestStepFromAiDto1.setAction('Action 1')
        actionTestStepFromAiDto1.setExpectedResult('Expected result 1')
        actionTestStepFromAiDto1.setIndex(0)

        TestCaseFromAiDto testCaseFromAiDto1 = new TestCaseFromAiDto()
        testCaseFromAiDto1.setName('TestCaseAi 1')
        testCaseFromAiDto1.setDescription('A description 1')
        testCaseFromAiDto1.setPrerequisites('A prerequisite 1')
        testCaseFromAiDto1.setSteps([actionTestStepFromAiDto1])

        ActionTestStepFromAiDto actionTestStepFromAiDto2 = new ActionTestStepFromAiDto()
        actionTestStepFromAiDto2.setAction('Action 2')
        actionTestStepFromAiDto2.setExpectedResult('Expected result 2')
        actionTestStepFromAiDto2.setIndex(0)
        ActionTestStepFromAiDto actionTestStepFromAiDto3 = new ActionTestStepFromAiDto()
        actionTestStepFromAiDto3.setAction('Action 3')
        actionTestStepFromAiDto3.setExpectedResult('Expected result 3')
        actionTestStepFromAiDto3.setIndex(0)

        TestCaseFromAiDto testCaseFromAiDto2 = new TestCaseFromAiDto()
        testCaseFromAiDto2.setName('TestCaseAi 2')
        testCaseFromAiDto2.setDescription('A description 2')
        testCaseFromAiDto2.setPrerequisites('A prerequisite 2')
        testCaseFromAiDto2.setSteps([actionTestStepFromAiDto2, actionTestStepFromAiDto3])

        testCasesData.setTestCasesFromAi([testCaseFromAiDto1, testCaseFromAiDto2])

        when:
        aiTestCaseGenerationService.persistTestCaseFromAi(-1L, -13L, testCasesData)

        then:
        TestCaseLibrary testCaseLibrary = findEntity(TestCaseLibrary.class, -1L)
        testCaseLibrary.content.size() == 2
        List<Long> newTestCaseIds = testCaseDao.findAllTestCaseIdsByLibraries([-1L])
        newTestCaseIds.size() == 2
        TestCase testCase1 = findEntity(TestCase.class, newTestCaseIds.get(0))
        TestCase testCase2 = findEntity(TestCase.class, newTestCaseIds.get(1))
        List<TestCase> newTestCases = new ArrayList<>()
        newTestCases.addAll([testCase1, testCase2])

        for (def newTestCase in newTestCases) {
            if (newTestCase.name == 'TestCaseAi 1') {
                newTestCase.description == 'A description 1'
                newTestCase.prerequisite == 'A prerequisite 1'
                newTestCase.steps.size() == 1
                newTestCase.steps[0].action == 'Action 1'
                newTestCase.steps[0].expectedResult == 'Expected result 1'
                newTestCase.steps[0].index == 0
                newTestCase.draftedByAi == true
                customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.TEST_CASE, newTestCase.id)[0].value == 'chat'
            } else if (newTestCase.name == 'TestCaseAi 2') {
                newTestCase.description == 'A description 2'
                newTestCase.prerequisite == 'A prerequisite 2'
                newTestCase.steps.size() == 2
                newTestCase.draftedByAi == true
            }
        }
    }

    def "should suffix the generated test case names if they already exist"() {
        given:
        Long projectId = -2L

        TestCasesCreationFromAiDto testCasesData = new TestCasesCreationFromAiDto();
        testCasesData.setTargetProjectId(projectId)
        testCasesData.setDestinationFolderId('TestCaseLibrary--2')

        ActionTestStepFromAiDto actionTestStepFromAiDto1 = new ActionTestStepFromAiDto()
        actionTestStepFromAiDto1.setAction('')
        actionTestStepFromAiDto1.setExpectedResult('')
        actionTestStepFromAiDto1.setIndex(0)

        TestCaseFromAiDto testCaseFromAiDto1 = new TestCaseFromAiDto()
        testCaseFromAiDto1.setName('Mon cas de test')
        testCaseFromAiDto1.setDescription('')
        testCaseFromAiDto1.setPrerequisites('')
        testCaseFromAiDto1.setSteps([actionTestStepFromAiDto1])

        ActionTestStepFromAiDto actionTestStepFromAiDto2 = new ActionTestStepFromAiDto()
        actionTestStepFromAiDto2.setAction('')
        actionTestStepFromAiDto2.setExpectedResult('')
        actionTestStepFromAiDto2.setIndex(0)

        TestCaseFromAiDto testCaseFromAiDto2 = new TestCaseFromAiDto()
        testCaseFromAiDto2.setName('Mon cas de test')
        testCaseFromAiDto2.setDescription('')
        testCaseFromAiDto2.setPrerequisites('')
        testCaseFromAiDto2.setSteps([actionTestStepFromAiDto2])

        testCasesData.setTestCasesFromAi([testCaseFromAiDto1, testCaseFromAiDto2])

        when:
        aiTestCaseGenerationService.persistTestCaseFromAi(-2L, -13L, testCasesData)

        then:
        TestCaseLibrary testCaseLibrary = findEntity(TestCaseLibrary.class, -2L)
        testCaseLibrary.content.size() == 4
        List<Long> newTestCaseIds = testCaseDao.findAllTestCaseIdsByLibraries([-2L])
        TestCase testCase1 = findEntity(TestCase.class, newTestCaseIds.get(0))
        TestCase testCase2 = findEntity(TestCase.class, newTestCaseIds.get(1))
        TestCase testCase3 = findEntity(TestCase.class, newTestCaseIds.get(2))
        TestCase testCase4 = findEntity(TestCase.class, newTestCaseIds.get(3))
        List<String> newTestCaseNames = new ArrayList<>()
        newTestCaseNames.addAll([testCase1.name, testCase2.name, testCase3.name, testCase4.name])
        newTestCaseNames.containsAll(['Mon cas de test', 'Mon cas de test 2', 'Mon cas de test(1)', 'Mon cas de test(2)'])
    }
}
