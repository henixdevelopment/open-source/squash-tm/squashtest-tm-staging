/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.user

import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.config.EnabledAclSpecConfig
import org.squashtest.tm.service.display.team.TeamDisplayService
import org.squashtest.tm.service.internal.display.dto.testcase.TeamMemberDto
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@UnitilsSupport
@Transactional
@DataSet
@ContextHierarchy([
	// enabling the ACL management that was disabled in DbunitServiceSpecification
	@ContextConfiguration(name="aclcontext", classes = [EnabledAclSpecConfig], inheritLocations=false)
])
class TeamDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private TeamDisplayService teamDisplayService

	def "should fetch teams"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		def dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

		when:
		def gridResponse = this.teamDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 3
		def rows = gridResponse.dataRows
		rows.get(0).data.name == "team 1"
		rows.get(0).data.partyId == -400
		rows.get(0).data.description == "My description is better than yours"
		rows.get(0).data.createdBy == "dbu"
		rows.get(0).data.createdOn.toLocalDateTime() == LocalDateTime.parse("2020-04-15 09:32:35", dateFormat)
		rows.get(0).data.lastModifiedBy == null
		rows.get(0).data.lastModifiedOn == null
		rows.get(0).data.teamMembersCount == 4

		rows.get(1).data.name == "team 2"
		rows.get(1).data.partyId == -123
		rows.get(1).data.teamMembersCount == 1

		rows.get(2).data.name == "team 3"
		rows.get(2).data.partyId == -321
	}

	def "should fetch team view dto for a given team id"() {

		when:
		def teamViewDto = teamDisplayService.getTeamView(-400L)

		then:

		teamViewDto.name == "team 1"
		teamViewDto.createdBy == "dbu"
		teamViewDto.description == "My description is better than yours"
		teamViewDto.lastModifiedBy ==  null
		teamViewDto.lastModifiedOn == null
		List<TeamMemberDto> members = teamViewDto.members
		members.size() == 4
		members.sort{m1,m2 -> m1.partyId <=> m2.partyId}
		members.get(0).firstName == "Juste"
		members.get(0).lastName == "Leblanc"
		members.get(0).fullName == "Juste Leblanc (four)"
	}

	def "should fetch team members for a given team"() {

		when:
		def members = teamDisplayService.getTeamMembers(-123L)

		then:
		members.size() == 1
		members.get(0).login == "three"
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.teamDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["name",
				   "description",
				   "teamMembersCount",
				   "createdOn",
				   "createdBy",
				   "lastModifiedOn",
				   "lastModifiedBy"]
	}
}
