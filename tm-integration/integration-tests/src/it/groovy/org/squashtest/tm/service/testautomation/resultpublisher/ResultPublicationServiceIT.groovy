/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.testautomation.resultpublisher.ExecutionService
import org.squashtest.tm.service.internal.repository.ApiTokenDao
import javax.inject.Inject


@DataSet
@UnitilsSupport
@Transactional
class ResultPublicationServiceIT extends DbunitServiceSpecification {

    ResultPublicationService service

    @Inject
    AutomatedSuiteDao autoSuiteDao

    ExecutionService executionService = Mock()

    @Inject
    ApiTokenDao apiTokenDao

    AutomatedExecutionState state = Mock()

    def "setup"() {
        service = new ResultPublicationServiceImpl(
            autoSuiteDao : autoSuiteDao,
            executionService : executionService,
            apiTokenDao : apiTokenDao
        )
    }

    def "Should delete workflows when automated suite last update"() {
        given:
            def autoSuiteId = "4028819f91745fbc019174657d440000"
            state.lastUpdate >> true
        when:
            service.publishResult(autoSuiteId,-1L,state)
            def autoSuite = autoSuiteDao.findById(autoSuiteId)

        then:
            autoSuite.getWorkflows() == null
    }

    def "Should not delete workflows until automated suite last update"() {
        given:
            def autoSuiteId = "4028819f9175113301917531d35c0001"
            state.lastUpdate >> false
        when:
            service.publishResult(autoSuiteId,-1L,state)
            def autoSuite = autoSuiteDao.findById(autoSuiteId)

        then:
            autoSuite.getWorkflows() != null
            autoSuite.getWorkflows().size() == 2
    }

    def "Should not try to delete Token when automated suite was launched by a test automation server (devops mode)"() {
        given:
            def autoSuiteId = "4028819f9175113301917531d35c0001"
            state.lastUpdate >> true
        when:
            def isCreatedByTestAutoServer = autoSuiteDao.isAutomatedSuiteCreatedByTestAutomationServerUser(autoSuiteId)
            service.publishResult(autoSuiteId,-1L,state)

        then:
            isCreatedByTestAutoServer == true
            1 * executionService.updateExecutionAndClearSession(-1L,autoSuiteId, state)
            0 * apiTokenDao.findAllByNameContains(_)
    }

}
