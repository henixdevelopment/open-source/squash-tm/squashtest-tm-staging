/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.requirement

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.RequirementVersionLinkType
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@UnitilsSupport
@Transactional
@DataSet
class RequirementVersionLinkTypeManagerServiceIT extends DbunitServiceSpecification {

    @Inject RequirementVersionLinkTypeManagerService requirementVersionLinkTypeManagerService
    @PersistenceContext EntityManager em

    def "should edit requirement link type"() {
        when:
        RequirementVersionLinkTypeRecord newValues = new RequirementVersionLinkTypeRecord("role1Updated", "role1CodeUpdated", "role2", "role2Code")
        requirementVersionLinkTypeManagerService.editLinkType(-1L, newValues)

        then:
        RequirementVersionLinkType requirementVersionLinkType = em.find(RequirementVersionLinkType, -1L)
        requirementVersionLinkType.role1 == "role1Updated"
        requirementVersionLinkType.role1Code == "role1CodeUpdated"
        requirementVersionLinkType.role2 == "role2"
        requirementVersionLinkType.role2Code == "role2Code"
    }
}
