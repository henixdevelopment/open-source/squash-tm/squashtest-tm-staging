/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class IterationStatisticsServiceIT extends DbunitServiceSpecification {


	@Inject
	private IterationStatisticsService service

	@DataSet("IterationStatisticsServiceIT.xml")
	def "should gather single iteration test inventory statistics" (){

		given :
		def iterationId = -1L
		def nameAndRef = (1..5).collect{"ts " + it}

		when :
		def result = service.gatherSingleTestSuiteTestInventoryStatistics(iterationId)
		then :
	    result.testsuiteName == nameAndRef

	}

    @DataSet("IterationStatisticsServiceIT.xml")
    def "should gather many iteration test inventory statistics" (){

        given :
        def iterationIds = [-1L, -2L]
        def nameAndRef = [
            "ref A - iter - tc1 / ts 1",
            "ref A - iter - tc1 / ts 2",
            "ref A - iter - tc1 / ts 3",
            "ref A - iter - tc1 / ts 4",
            "ref A - iter - tc1 / ts 5",
            "ref B - iter - tc2 / ts 6",
            "ref B - iter - tc2 / ts 7",
            "ref B - iter - tc2 / ts 8"
        ]

        when :
        def result = service.gatherManyTestSuiteTestInventoryStatistics(iterationIds)
        then :
        result.testsuiteName == nameAndRef

    }



}
