/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.connection.log

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@UnitilsSupport
@Transactional
@DataSet
class ConnectionLogDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private ConnectionLogDisplayService connectionLogDisplayService

	def "should fetch a connection log grid"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.connectionLogDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 20
		def rows = gridResponse.dataRows

		def dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

		def first = rows.get(0)
		first.id == "-20"						// Attempts are sorted by date DESC
		first.data.get("attemptId") == -20
		first.data.get("login") == "jane.doe"
		first.data.get("connectionDate").toLocalDateTime() == LocalDateTime.parse("2020-10-06 03:00:00", dateFormat)
		first.data.get("success") == false

		def last = rows.get(19)
		last.id == "-1"
		last.data.get("attemptId") == -1
		last.data.get("login") == "admin"
		last.data.get("connectionDate").toLocalDateTime() == LocalDateTime.parse("2020-10-01 01:00:00", dateFormat)
		last.data.get("success") == true
	}
}
