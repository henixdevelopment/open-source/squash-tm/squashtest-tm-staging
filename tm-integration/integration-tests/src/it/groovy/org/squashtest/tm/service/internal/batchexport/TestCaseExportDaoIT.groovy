/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.batchexport.models.ExportModel
import org.squashtest.tm.service.internal.batchexport.models.TestCaseModel
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.testcase.TestCaseKind.GHERKIN
import static org.squashtest.tm.domain.testcase.TestCaseKind.STANDARD

@UnitilsSupport
@Transactional
class TestCaseExportDaoIT extends DbunitServiceSpecification{

	@Inject
	private TestCaseExportDao exporter


	@DataSet("ExportDaoIT.should create models.xml")
	def "should create models"(){
		given :
		def testCaseIds = [-10L, -11L]

		when :
		ExportModel result = exporter.findModel(testCaseIds)

		then :
		result.testCases.size() == 2
		result.datasets.size() == 1
		result.parameters.size() == 1
		result.testSteps.size() == 2

	}

	@DataSet("ExportDaoIT.should create scripted test case model.xml")
	def "should create scripted test case models"(){
		given :
		def testCaseIds = [-10L, -11L]

		when :
		ExportModel result = exporter.findModel(testCaseIds)
		List<TestCaseModel> testCases = result.testCases
		TestCaseModel standardTestCaseModel = testCases.find{it.id == -10L}
		TestCaseModel scriptedTestCaseModel = testCases.find{it.id == -11L}


		then :
		testCases.size() == 2
		standardTestCaseModel.getTestCaseKind() == STANDARD

		scriptedTestCaseModel.getTestCaseKind() == GHERKIN
		scriptedTestCaseModel.getTcScript() == "Feature: three cucumbers and two tomatoes"

	}
}
