/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.it.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.squashtest.tm.core.foundation.lang.Couple
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.servers.AuthenticationProtocol
import org.squashtest.tm.domain.servers.Credentials
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow
import org.squashtest.tm.domain.testautomation.AutomatedTest
import org.squashtest.tm.domain.testautomation.TestAutomationProject
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import org.squashtest.tm.service.internal.dto.WorkflowDto
import org.squashtest.tm.service.internal.testautomation.model.IterationTestPlanItemWithCustomFields
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment
import org.squashtest.tm.service.testautomation.spi.AccessDenied
import org.squashtest.tm.service.testautomation.spi.BadConfiguration
import org.squashtest.tm.service.testautomation.spi.NotFoundException
import org.squashtest.tm.service.testautomation.spi.ServerConnectionFailed
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector
import org.squashtest.tm.service.testautomation.spi.TestAutomationException
import org.squashtest.tm.service.testautomation.spi.UnreadableResponseException
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException

@Configuration
class TestAutomationServerTestConfig {
	@Bean
	TestAutomationConnector dummyJenkinsConnector() {
		return new DummyJenkinsConnector()
	}

	@Bean
	TestAutomationConnector dummySquashAutomConnector() {
		return new DummySquashAutomConnector()
	}

	class DummyJenkinsConnector implements TestAutomationConnector {

		@Override
		TestAutomationServerKind getConnectorKind() {
			return TestAutomationServerKind.jenkins
		}

		@Override
		boolean checkCredentials(TestAutomationServer server, String login, String password) throws TestAutomationException {
			return false
		}

		@Override
		boolean checkCredentials(TestAutomationServer server, Credentials credentials) throws TestAutomationException {
			return false
		}

		@Override
		Collection<TestAutomationProject> listProjectsOnServer(TestAutomationServer server, Credentials credentials) throws ServerConnectionFailed, AccessDenied, UnreadableResponseException, NotFoundException, BadConfiguration, TestAutomationException {
			return null
		}

		@Override
		Collection<AutomatedTest> listTestsInProject(TestAutomationProject project, String username) throws ServerConnectionFailed, AccessDenied, UnreadableResponseException, NotFoundException, BadConfiguration, TestAutomationException {
			return null
		}

		@Override
		void executeParameterizedTests(Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests, String externalId) {

		}

		@Override
		List<AutomatedSuiteWorkflow> executeParameterizedTestsBasedOnITPICollection(Collection<IterationTestPlanItemWithCustomFields> tests, String externalId, Collection<SquashAutomExecutionConfiguration> squashAutomExecutionCbonfigurations) {
			return null
		}

		@Override
		URL findTestAutomationProjectURL(TestAutomationProject testAutomationProject) {
			return null
		}

		@Override
		boolean testListIsOrderGuaranteed(Collection<AutomatedTest> tests) {
			return false
		}

		@Override
		boolean supports(AuthenticationProtocol protocol) {
			return false
		}

		@Override
		AuthenticationProtocol[] getSupportedProtocols() {
			return new AuthenticationProtocol[0]
		}

		@Override
		boolean supportsAutomatedExecutionEnvironments() {
			return false
		}

		@Override
		List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(TestAutomationServer server) {
			throw new UnsupportedOperationException()
		}

		@Override
		List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(TestAutomationServer testAutomationServer, Credentials credentials) {
			throw new UnsupportedOperationException()
		}

		@Override
		OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfVersions(TestAutomationServer server) {
			throw new UnsupportedOperationException()
		}

		@Override
		OrchestratorResponse<List<WorkflowDto>> getProjectWorkflows(TestAutomationServer server, Long projectId, Credentials credentials) {
			throw new UnsupportedOperationException()
		}

		@Override
		OrchestratorResponse<Void> killWorkflow(TestAutomationServer server, Long projectId, TokenAuthCredentials credentials, String workflowId) {
			throw new UnsupportedOperationException()
		}
	}

	class DummySquashAutomConnector implements TestAutomationConnector {

		@Override
		TestAutomationServerKind getConnectorKind() {
			return TestAutomationServerKind.squashOrchestrator
		}

		@Override
		boolean checkCredentials(TestAutomationServer server, String login, String password) throws TestAutomationException {
			return false
		}

		@Override
		boolean checkCredentials(TestAutomationServer server, Credentials credentials) throws TestAutomationException {
			return false
		}

		@Override
		Collection<TestAutomationProject> listProjectsOnServer(TestAutomationServer server, Credentials credentials) throws ServerConnectionFailed, AccessDenied, UnreadableResponseException, NotFoundException, BadConfiguration, TestAutomationException {
			return null
		}

		@Override
		Collection<AutomatedTest> listTestsInProject(TestAutomationProject project, String username) throws ServerConnectionFailed, AccessDenied, UnreadableResponseException, NotFoundException, BadConfiguration, TestAutomationException {
			return null
		}

		@Override
		void executeParameterizedTests(Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests, String externalId) {

		}

		@Override
		List<AutomatedSuiteWorkflow> executeParameterizedTestsBasedOnITPICollection(Collection<IterationTestPlanItemWithCustomFields> tests, String externalId, Collection<SquashAutomExecutionConfiguration> squashAutomExecutionCbonfigurations) {
			return null
		}

		@Override
		URL findTestAutomationProjectURL(TestAutomationProject testAutomationProject) {
			return null
		}

		@Override
		boolean testListIsOrderGuaranteed(Collection<AutomatedTest> tests) {
			return false
		}

		@Override
		boolean supports(AuthenticationProtocol protocol) {
			return false
		}

		@Override
		AuthenticationProtocol[] getSupportedProtocols() {
			return new AuthenticationProtocol[0]
		}

		@Override
		boolean supportsAutomatedExecutionEnvironments() {
			return true
		}

		@Override
		List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(TestAutomationServer server) {

            switch (server.id) {
                case -77L:
                    return []
                case -88L:
                    throw new InvalidSquashOrchestratorConfigurationException()
                default:
                    return [
                        new AutomatedExecutionEnvironment("env1", ["windows", "cypress"], "IDLE"),
                        new AutomatedExecutionEnvironment("env2", ["linux", "robot"], "BUSY"),
                        new AutomatedExecutionEnvironment("env3", ["windows", "cucumber", "env3"], "BUSY"),
                        new AutomatedExecutionEnvironment("env4", ["linux", "robot"], "IDLE"),
                        new AutomatedExecutionEnvironment("env5", ["linux", "robot"], "PENDING")
                    ]
            }
		}

		@Override
		List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(TestAutomationServer testAutomationServer, Credentials credentials) {
            return [
                new AutomatedExecutionEnvironment("env6", ["windows", "postman"], "UNREACHABLE"),
                new AutomatedExecutionEnvironment("env7", ["linux", "robot"], "IDLE"),
                new AutomatedExecutionEnvironment("env8", ["windows", "cucumber", "env8"], "BUSY"),
            ]
		}

		@Override
		OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfVersions(TestAutomationServer server) {
			throw new UnsupportedOperationException()
		}

		@Override
		OrchestratorResponse<List<WorkflowDto>> getProjectWorkflows(TestAutomationServer server, Long projectId, Credentials credentials) {
			throw new UnsupportedOperationException()
		}

		@Override
		OrchestratorResponse<Void> killWorkflow(TestAutomationServer server, Long projectId, TokenAuthCredentials credentials, String workflowId) {
			throw new UnsupportedOperationException()
		}
	}
}
