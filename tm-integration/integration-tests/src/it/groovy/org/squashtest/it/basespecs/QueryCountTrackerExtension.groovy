/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
// QueryCountTrackerExtension.groovy
package org.squashtest.it.basespecs

import net.ttddyy.dsproxy.QueryCountHolder
import org.hibernate.SessionFactory
import org.hibernate.stat.Statistics
import org.spockframework.runtime.extension.IAnnotationDrivenExtension
import org.spockframework.runtime.extension.IMethodInterceptor
import org.spockframework.runtime.extension.IMethodInvocation
import org.spockframework.runtime.model.FeatureInfo
import org.spockframework.runtime.model.SpecInfo
import org.squashtest.it.infrastructure.JooqQueryCounterListener

class QueryCountTrackerExtension implements IAnnotationDrivenExtension<QueryCountTracker> {

    @Override
    void visitSpecAnnotation(QueryCountTracker annotation, SpecInfo spec) {
        spec.getAllFeatures().each { feature ->
            feature.addInterceptor(new QueryCountInterceptor())
        }
    }

    @Override
    void visitFeatureAnnotation(QueryCountTracker annotation, FeatureInfo feature) {
        feature.addInterceptor(new QueryCountInterceptor())
    }

    private class QueryCountInterceptor implements IMethodInterceptor {
        @Override
        void intercept(IMethodInvocation invocation) throws Throwable {
            def em = invocation.instance.em
            def stats = em.getEntityManagerFactory().unwrap(SessionFactory).getStatistics()
            stats.setStatisticsEnabled(true)

            beforeEach(stats)
            try {
                invocation.proceed()
            } finally {
                afterEach(stats)
            }
        }

        private void beforeEach(Statistics hibernateStats) {
            QueryCountHolder.clear()
            hibernateStats.clear()
            JooqQueryCounterListener.clear()
        }

        private void afterEach(Statistics hibernateStats) {
            def databaseQueryCount = QueryCountHolder.getGrandTotal().getSelect()
            def jpaQueryCount = hibernateStats.getQueryExecutionCount()
            def jooqQueryCount = JooqQueryCounterListener.getSelectQueryCount()

            def codeCount = jpaQueryCount + jooqQueryCount

            assert codeCount == databaseQueryCount : "Mismatch in query count: Expected $codeCount queries, but $databaseQueryCount were executed. Check for lazy loading or missing joins"
            println "Total database query count: $databaseQueryCount"
        }
    }
}
