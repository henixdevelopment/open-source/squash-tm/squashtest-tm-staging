/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.it.infrastructure;

import java.util.List;
import javax.sql.DataSource;
import net.ttddyy.dsproxy.ExecutionInfo;
import net.ttddyy.dsproxy.QueryInfo;
import net.ttddyy.dsproxy.listener.ChainListener;
import net.ttddyy.dsproxy.listener.DataSourceQueryCountListener;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.unitils.database.config.PropertiesDataSourceFactory;

public class ProxyDataSourceFactory extends PropertiesDataSourceFactory {

    @Override
    public DataSource createDataSource() {
        DataSource dataSource = super.createDataSource();

        return ProxyDataSourceBuilder.create(dataSource)
            .listener(new CustomQueryCountListener())
            .build();
    }

    private static class CustomQueryCountListener extends DataSourceQueryCountListener {
        @Override
        public void afterQuery(ExecutionInfo execInfo, List<QueryInfo> queryInfoList) {
            queryInfoList.forEach(
                    queryInfo -> {
                        String query = queryInfo.getQuery();
                        if (!query.toLowerCase().startsWith("select nextval")) {
                            super.afterQuery(execInfo, List.of(queryInfo));
                        }
                    });
        }
    }
}
