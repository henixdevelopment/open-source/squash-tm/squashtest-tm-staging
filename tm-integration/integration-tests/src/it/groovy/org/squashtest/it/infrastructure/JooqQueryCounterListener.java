/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.it.infrastructure;

import org.jooq.ExecuteContext;
import org.squashtest.tm.service.internal.api.repository.JooqSessionGuard;

import javax.persistence.EntityManagerFactory;

public class JooqQueryCounterListener extends JooqSessionGuard {
    private static final ThreadLocal<Integer> selectQueryCount = ThreadLocal.withInitial(() -> 0);

    public JooqQueryCounterListener(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public void executeStart(ExecuteContext ctx) {
        String sql = ctx.sql();
        if (sql != null && sql.trim().toLowerCase().startsWith("select")) {
            selectQueryCount.set(selectQueryCount.get() + 1);
        }
        super.executeStart(ctx);
    }

    public static void clear() {
        selectQueryCount.set(0);
    }

    public static int getSelectQueryCount() {
        return selectQueryCount.get();
    }
}
