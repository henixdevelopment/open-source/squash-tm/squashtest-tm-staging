<?xml version='1.0' encoding='UTF-8'?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<!DOCTYPE dataset SYSTEM "../../../../../dtd/squash-tm.10.0.0.dtd">
<dataset>

  <CORE_PARTY PARTY_ID="-2"/>
  <CORE_USER PARTY_ID="-2" CREATED_ON="2020-04-15" CREATED_BY="dbu" LOGIN="test_runner" LAST_NAME="Runner" ACTIVE="true"/>
  <CORE_PARTY PARTY_ID="-3"/>
  <CORE_USER PARTY_ID="-3" CREATED_ON="2020-04-15" CREATED_BY="dbu" LOGIN="user2" LAST_NAME="SiriusBlack" ACTIVE="true"/>

  <CORE_GROUP ID="-2" QUALIFIED_NAME="squashtest.authz.group.tm.User"/>
  <CORE_GROUP_AUTHORITY GROUP_ID="-2" AUTHORITY="ROLE_TM_USER"/>
  <CORE_GROUP_MEMBER PARTY_ID="-2" GROUP_ID="-2"/>

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-555"/>
  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-988"/>
  <CAMPAIGN_LIBRARY ATTACHMENT_LIST_ID="-988" CL_ID="-1"/>
  <PROJECT PROJECT_ID="-1" ATTACHMENT_LIST_ID="-555" CREATED_ON="2012-02-01" CREATED_BY="DBU"
           NAME="first project" CL_ID="-1"/>

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-1238"/>
  <TEST_CASE_LIBRARY_NODE TCLN_ID="-1" NAME="CT1 explo" CREATED_ON="2010-02-01" CREATED_BY="DBU"
                          ATTACHMENT_LIST_ID="-1238" PROJECT_ID="-1" LAST_MODIFIED_BY="DBU"
                          LAST_MODIFIED_ON="2024-01-17" DESCRIPTION="Un cas de test exploratoire"/>
  <TEST_CASE TCLN_ID="-1" PREREQUISITE="a prerequisite" UUID="42d63d7e-11dd-44b0-b584-565b6f791fa9"
             EXECUTION_MODE="EXPLORATORY"/>

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-1224"/>
  <TEST_CASE_LIBRARY_NODE TCLN_ID="-2" NAME="CT2 classique" CREATED_ON="2010-02-01" CREATED_BY="DBU"
                          ATTACHMENT_LIST_ID="-1224" PROJECT_ID="-1" LAST_MODIFIED_BY="DBU"
                          LAST_MODIFIED_ON="2024-01-17" DESCRIPTION="Un cas de test classique"/>
  <TEST_CASE TCLN_ID="-2" PREREQUISITE="a prerequisite" UUID="42d67d7e-11dd-44b0-b584-565b6f791fa9"/>

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-584"/>
  <TEST_CASE_LIBRARY_NODE TCLN_ID="-3" NAME="CT3 classique" CREATED_ON="2010-02-01" CREATED_BY="DBU"
                          ATTACHMENT_LIST_ID="-584" PROJECT_ID="-1" LAST_MODIFIED_BY="DBU"
                          LAST_MODIFIED_ON="2024-01-17" DESCRIPTION="Un autre cas de test classique"/>
  <TEST_CASE TCLN_ID="-3" PREREQUISITE="a prerequisite" UUID="42d63d7e-11dd-44b0-b581-565b6f791fa9"/>

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-123"/>
  <CAMPAIGN_LIBRARY_NODE
    CLN_ID="-1"
    NAME="a sprint"
    CREATED_ON="2010-02-01"
    CREATED_BY="DBU"
    ATTACHMENT_LIST_ID="-123"
    PROJECT_ID="-1"
    LAST_MODIFIED_BY="DBU"
    LAST_MODIFIED_ON="2024-01-17"
    DESCRIPTION="Quelle incroyable description"
  />
  <CAMPAIGN_LIBRARY_CONTENT LIBRARY_ID="-1" CONTENT_ID="-1" CONTENT_ORDER="1"/>

  <SPRINT CLN_ID="-1" REFERENCE="test" START_DATE="2024-01-01" END_DATE="2024-01-12"/>

  <!-- a first requirement bound to the sprint -->
  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-456"/>
  <RESOURCE RES_ID="-1" CREATED_BY="creator" CREATED_ON="2024-01-26" ATTACHMENT_LIST_ID="-456" NAME="a first requirement" DESCRIPTION="Éphémère"/>
  <REQUIREMENT_LIBRARY_NODE RLN_ID="-1" CREATED_BY="creator" CREATED_ON="2024-01-26" PROJECT_ID="-1"/>
  <REQUIREMENT RLN_ID="-1"/>
  <REQUIREMENT_VERSION RES_ID="-1" REFERENCE="a reference" CRITICALITY="MINOR" REQUIREMENT_ID="-1" CATEGORY="4"/>

  <TEST_PLAN TEST_PLAN_ID="-1" CL_ID="-1"/>
  <SPRINT_REQ_VERSION SPRINT_REQ_VERSION_ID="-1" REQ_VERSION_ID="-1" SPRINT_ID="-1" TEST_PLAN_ID="-1"
                      CREATED_BY="dbunit" NAME="SRV1" REFERENCE="001"
                      CREATED_ON="2024-07-31"/>

  <TEST_PLAN_ITEM TEST_PLAN_ITEM_ID="-1" TEST_PLAN_ID="-1" ITEM_ORDER="0" LABEL="TPI1"
                  CREATED_BY="me" CREATED_ON="2024-01-26" ASSIGNEE_ID="-2" TCLN_ID="-2"/>

  <!-- exploratory test case -->
  <TEST_PLAN_ITEM TEST_PLAN_ITEM_ID="-2" TEST_PLAN_ID="-1" ITEM_ORDER="1" LABEL="TPI2"
                  CREATED_BY="me" CREATED_ON="2024-01-26" EXECUTION_STATUS="SUCCESS" TCLN_ID="-1"/>
  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-544"/>
  <EXPLORATORY_SESSION_OVERVIEW OVERVIEW_ID="-1" NAME="overview" SESSION_STATUS="RUNNING" COMMENTS=""
                                ATTACHMENT_LIST_ID="-544" TEST_PLAN_ITEM_ID="-2"/>

  <TEST_PLAN_ITEM TEST_PLAN_ITEM_ID="-3" TEST_PLAN_ID="-1" ITEM_ORDER="2" LABEL="TPI3"
                  CREATED_BY="me" CREATED_ON="2024-01-26" ASSIGNEE_ID="-3"
                  EXECUTION_STATUS="FAILURE" TCLN_ID="-3"/>

  <!-- a second requirement bound to the sprint -->
  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-789"/>
  <RESOURCE RES_ID="-2" CREATED_BY="creator" CREATED_ON="2024-01-26" ATTACHMENT_LIST_ID="-789" NAME="a second requirement" DESCRIPTION="Persévérance"/>
  <REQUIREMENT_LIBRARY_NODE RLN_ID="-2" CREATED_BY="creator" CREATED_ON="2024-01-26" PROJECT_ID="-1"/>
  <REQUIREMENT RLN_ID="-2"/>
  <REQUIREMENT_VERSION RES_ID="-2" REFERENCE="another reference" CRITICALITY="MINOR" REQUIREMENT_ID="-2" CATEGORY="3"/>

  <TEST_PLAN TEST_PLAN_ID="-2" CL_ID="-1"/>
  <SPRINT_REQ_VERSION SPRINT_REQ_VERSION_ID="-2" REQ_VERSION_ID="-2" SPRINT_ID="-1" TEST_PLAN_ID="-2"
                      CREATED_BY="dbunit" CREATED_ON="2024-03-28" NAME="SRV2"/>

  <!-- exploratory test case -->
  <TEST_PLAN_ITEM TEST_PLAN_ITEM_ID="-4" TEST_PLAN_ID="-2" ITEM_ORDER="0" LABEL="TPI4"
                  CREATED_BY="me" CREATED_ON="2024-01-26" TCLN_ID="-1"/>
  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-541"/>
  <EXPLORATORY_SESSION_OVERVIEW OVERVIEW_ID="-2" NAME="overview 2" SESSION_STATUS="RUNNING" COMMENTS=""
                                ATTACHMENT_LIST_ID="-541" TEST_PLAN_ITEM_ID="-4"/>

  <!-- another sprint -->

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-1283"/>
  <CAMPAIGN_LIBRARY_NODE
    CLN_ID="-2"
    NAME="a sprint 2"
    CREATED_ON="2010-02-01"
    CREATED_BY="DBU"
    ATTACHMENT_LIST_ID="-1283"
    PROJECT_ID="-1"
    LAST_MODIFIED_BY="DBU"
    LAST_MODIFIED_ON="2024-01-17"
    DESCRIPTION="Quelle incroyable description 2"
  />
  <CAMPAIGN_LIBRARY_CONTENT LIBRARY_ID="-1" CONTENT_ID="-2" CONTENT_ORDER="2"/>

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-54"/>
  <TEST_CASE_LIBRARY_NODE TCLN_ID="-4" NAME="CT4 classique" CREATED_ON="2010-02-01" CREATED_BY="DBU"
                          ATTACHMENT_LIST_ID="-54" PROJECT_ID="-1" LAST_MODIFIED_BY="DBU"
                          LAST_MODIFIED_ON="2024-01-17" DESCRIPTION=""/>
  <TEST_CASE TCLN_ID="-4" PREREQUISITE="a prerequisite" UUID="42d63c7e-11dd-44b0-b581-565b6f791fa9"/>

  <SPRINT CLN_ID="-2" REFERENCE="test2" START_DATE="2024-01-01" END_DATE="2024-01-12"/>

  <ATTACHMENT_LIST ATTACHMENT_LIST_ID="-41"/>
  <RESOURCE RES_ID="-3" CREATED_BY="creator" CREATED_ON="2024-01-26" ATTACHMENT_LIST_ID="-41"
            NAME="a third requirement" DESCRIPTION=""/>
  <REQUIREMENT_LIBRARY_NODE RLN_ID="-3" CREATED_BY="creator" CREATED_ON="2024-01-26" PROJECT_ID="-1"/>
  <REQUIREMENT RLN_ID="-3"/>
  <REQUIREMENT_VERSION RES_ID="-3" REFERENCE="xxx" CRITICALITY="MINOR" REQUIREMENT_ID="-3" CATEGORY="3"/>

  <TEST_PLAN TEST_PLAN_ID="-3" CL_ID="-1"/>
  <SPRINT_REQ_VERSION SPRINT_REQ_VERSION_ID="-3" REQ_VERSION_ID="-3" SPRINT_ID="-2" TEST_PLAN_ID="-3"
                      CREATED_BY="dbunit" CREATED_ON="2024-03-28"/>

  <TEST_PLAN_ITEM TEST_PLAN_ITEM_ID="-5" TEST_PLAN_ID="-3" ITEM_ORDER="0" LABEL="TPI5"
                  CREATED_BY="me" CREATED_ON="2024-01-26" TCLN_ID="-4"/>
</dataset>
