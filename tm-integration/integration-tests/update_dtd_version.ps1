# Usage: .\update_dtd_version.ps1 -NewVersion "10.0.0"

param (
    [string]$NewVersion
)

if (-not $NewVersion) {
    Write-Output "Usage: .\update_dtd_version.ps1 -NewVersion <version>"
    exit
}

$FolderPath = "src/it/resources/"

# Get all XML files in the specified folder and its subfolders
$xmlFiles = Get-ChildItem -Path $FolderPath -Recurse -Filter *.xml

# Counter for modified files
$modifiedFilesCount = 0

foreach ($file in $xmlFiles) {
    # Read file content
    $content = Get-Content -Path $file.FullName -Raw -Encoding UTF8

    # Replace the DTD path with the new version
    $newContent = [regex]::Replace($content, '<!DOCTYPE dataset SYSTEM "(?<path>(?:\.\./)+)dtd/squash-tm\.\d+\.\d+\.\d+(?<result>\.result)?\.dtd">', {
        param($match)
        "<!DOCTYPE dataset SYSTEM `"$($match.Groups['path'].Value)dtd/squash-tm.$NewVersion$($match.Groups['result'].Value).dtd`">"
    })
    # Check if the content was modified
    if ($newContent -ne $content) {
        # Write the updated content back to the file without adding a newline
        [IO.File]::WriteAllText($file.FullName, $newContent)
        Write-Output "Updated DTD version in $($file.FullName)"
        $modifiedFilesCount++
    }
}

Write-Output "DTD version updated to $NewVersion in $modifiedFilesCount files in $FolderPath."
