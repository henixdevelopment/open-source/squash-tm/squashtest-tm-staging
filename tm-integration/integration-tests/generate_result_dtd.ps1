# Usage: .\make_fields_optional.ps1

# Fixed path for DTD files
$FolderPath = "src/it/resources/dtd/"

# Get all DTD files matching the pattern squash-tm.X.Y.Z.dtd
$dtdFiles = Get-ChildItem -Path $FolderPath -Filter "squash-tm.*.*.*.dtd" | Where-Object { $_.Name -match "^squash-tm\.\d+\.\d+\.\d+\.dtd$" }

# Create a UTF-8 encoding object without BOM
$utf8NoBOM = New-Object System.Text.UTF8Encoding $false

foreach ($file in $dtdFiles) {
    # Read the DTD file content with explicit UTF-8 encoding (no BOM)
    $content = [System.IO.File]::ReadAllText($file.FullName, $utf8NoBOM)

    # Replace all occurrences of "#REQUIRED" with "#IMPLIED"
    $newContent = $content -replace "#REQUIRED", "#IMPLIED"

    # Construct the new filename with .result.dtd suffix
    $newFileName = $file.Name -replace "\.dtd$", ".result.dtd"
    $newFilePath = Join-Path -Path $file.DirectoryName -ChildPath $newFileName

    # Write the modified content to the new .result.dtd file with UTF-8 encoding (without BOM)
    [System.IO.File]::WriteAllText($newFilePath, $newContent, $utf8NoBOM)
    Write-Output "Generated $newFileName with all fields set to #IMPLIED."
}

Write-Output "Processing complete. All matching DTD files have corresponding .result.dtd files with optional fields."
