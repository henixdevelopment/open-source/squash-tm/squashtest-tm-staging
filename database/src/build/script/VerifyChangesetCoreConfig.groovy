import javax.xml.stream.XMLInputFactory

/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
List.metaClass.collectWithIndex = { body ->
	def i = 0
	delegate.collect { body(it, i++) }
}

File.metaClass.collectLines

def root = new File("${project.basedir}")

def src = new File(root, "src/main/liquibase/tm")

def changelogPattern = /tm\.changelog-(\d+)\.(\d+)\.(\d+)(-.+)?\.xml/

def minimalVersion = 90000

log.info("Applying core config check to database changelogs");

def errlog = src.listFiles()
    .findAll { it.name ==~ changelogPattern }
    .collectMany { validateFile(it, minimalVersion, changelogPattern) }

def validateFile(file, minimalVersion, changelogPattern) {
    def m = file.name =~ changelogPattern
    if (!m.find()) {
        return []
    }

    def version = Integer.valueOf(m.group(1)) * 10000 + Integer.valueOf(m.group(2)) * 100 + Integer.valueOf(m.group(3))
    if (version < minimalVersion) {
        return []
    }

    def lines = file.readLines()
    def lastIndexChangeset = lines.findLastIndexOf { it.contains('<changeSet') }
    if (lastIndexChangeset == -1) {
        return [[n: file.name, err: true, msg: 'No changeset found in file']]
    }

    def coreConfigLast = lines.subList(lastIndexChangeset, lines.size()).any { it.contains('squashtest.tm.database.version') }
    return coreConfigLast ? [] : [[n: file.name, err: true, msg: 'The squashtest.tm.database.version is not the last changeset']]
}

errlog.each {
	log.error("file '${it.n}' : ${it.msg}")
}

if (errlog.size() > 0) {
	fail("ERROR : Validation failed. The update of squashtest.tm.database.version must be the last changeset")
}
