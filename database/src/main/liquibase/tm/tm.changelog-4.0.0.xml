<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-2.0.xsd">

  <changeSet id="tm-4.0.0-SQUASH-5232-add-databasechangelog-new-columns" author="jprioux">
    <validCheckSum>8:aeea36396a9ec2a5a73082a8a593b123</validCheckSum>
    <comment>Add Contexts, labels and deployment_id columns to DATABASECHANGELOG Table</comment>
    <sql>
      ALTER TABLE DATABASECHANGELOG ADD COLUMN IF NOT EXISTS CONTEXTS VARCHAR(255) DEFAULT NULL;
      ALTER TABLE DATABASECHANGELOG ADD COLUMN IF NOT EXISTS LABELS VARCHAR(255) DEFAULT NULL;
      ALTER TABLE DATABASECHANGELOG ADD COLUMN IF NOT EXISTS DEPLOYMENT_ID VARCHAR(10) DEFAULT NULL;
    </sql>
  </changeSet>

  <changeSet id="tm-4.0.0" author="jlor">
    <comment>Update TM database version number</comment>
    <update tableName="CORE_CONFIG">
      <column name="VALUE" value="4.0.0"/>
      <where>STR_KEY = 'squashtest.tm.database.version'</where>
    </update>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-4931-projects-data-migration" author="jlor">
    <comment>Migrate PROJECT.BDD_IMPLEMENTATION_TECHNOLOGY from value 'CUCUMBER' to 'CUCUMBER_4'</comment>
    <update tableName="PROJECT">
      <column name="BDD_IMPLEMENTATION_TECHNOLOGY" value="CUCUMBER_4"/>
      <where>BDD_IMPLEMENTATION_TECHNOLOGY = 'CUCUMBER'</where>
    </update>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-4931-projects-default-value-modification" author="jlor">
    <comment>Modify default value of PROJECT.BDD_IMPLEMENTATION_TECHNOLOGY from value 'CUCUMBER' to 'CUCUMBER_5_PLUS'</comment>
      <addDefaultValue tableName="PROJECT" columnName="BDD_IMPLEMENTATION_TECHNOLOGY" defaultValue="CUCUMBER_5_PLUS"/>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-4931-action-words-data-migration" author="jlor">
    <comment>Migrate ACTION_WORD.LAST_IMPLEMENTATION_TECHNOLOGY from value 'CUCUMBER' to 'CUCUMBER_4'</comment>
    <update tableName="ACTION_WORD">
      <column name="LAST_IMPLEMENTATION_TECHNOLOGY" value="CUCUMBER_4"/>
      <where>LAST_IMPLEMENTATION_TECHNOLOGY = 'CUCUMBER'</where>
    </update>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-5025-token-auth-migration-for-mantis" author="bms" dbms="mariadb">
    <comment>Migrate BASIC_AUTH TO TOKEN_AUTH in third_party_server table</comment>
    <sql>
        UPDATE THIRD_PARTY_SERVER server
        INNER JOIN BUGTRACKER bt ON server.SERVER_ID = bt.BUGTRACKER_ID
        SET AUTH_PROTOCOL = 'TOKEN_AUTH'
        WHERE bt.KIND = 'mantis';
    </sql>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-5025-token-auth-migration-for-mantis" author="bms" dbms="postgresql">
    <comment>Migrate BASIC_AUTH TO TOKEN_AUTH in third_party_server table</comment>
    <sql>
      UPDATE THIRD_PARTY_SERVER server
      SET AUTH_PROTOCOL = 'TOKEN_AUTH'
      FROM BUGTRACKER bt
      WHERE bt.KIND = 'mantis' AND server.SERVER_ID = bt.BUGTRACKER_ID;
    </sql>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-5025-token-auth-migration-for-mantis" author="bms" dbms="h2">
    <comment>Migrate BASIC_AUTH TO TOKEN_AUTH in third_party_server table</comment>
    <sql>
      UPDATE THIRD_PARTY_SERVER server
      SET AUTH_PROTOCOL = 'TOKEN_AUTH'
      WHERE server.SERVER_ID IN (SELECT bt.BUGTRACKER_ID FROM BUGTRACKER bt WHERE bt.KIND = 'mantis');
    </sql>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-5035-add-owner-column-to-remote-synchronisation" author="cduvigneau">
  <comment>Add Owner Column to REMOTE_SYNCHRONISATION Table</comment>
  <addColumn tableName="REMOTE_SYNCHRONISATION">
    <column name="OWNER_ID" type="BIGINT">
      <constraints nullable="true" foreignKeyName="fk_remote_sync_owner" references="CORE_USER(PARTY_ID)"/>
    </column>
  </addColumn>
    <createIndex tableName="REMOTE_SYNCHRONISATION" indexName="idx_fk_remote_sync_owner">
      <column name="OWNER_ID" />
    </createIndex>
  </changeSet>

  <changeSet id="tm-4.0.0-delete-column-test-case-version" author="jlor">
    <comment>Delete TEST_CASE.VERSION which is not and was never used</comment>
    <dropColumn tableName="TEST_CASE" columnName="VERSION" />
  </changeSet>

  <changeSet id="tm-4.0.0-sqmap-879-automation-environment-tag" author="jlor">
    <comment>Create table AUTOMATION_ENVIRONMENT_TAG</comment>
    <createTable tableName="AUTOMATION_ENVIRONMENT_TAG">
      <column name="VALUE" type="VARCHAR(255)" />
      <column name="ENTITY_TYPE" type="VARCHAR(50)">
        <constraints nullable="false" />
      </column>
      <column name="ENTITY_ID" type="BIGINT">
        <constraints nullable="false" />
      </column>
    </createTable>
    <addUniqueConstraint constraintName="uniq_value_entity_type_entity_id"
                         tableName="AUTOMATION_ENVIRONMENT_TAG" columnNames="VALUE, ENTITY_TYPE, ENTITY_ID" />
  </changeSet>

  <changeSet id="tm-4.0.0-sqmap-879-add-project-inherits-env-tag-column" author="pckerneis">
    <addColumn tableName="PROJECT">
      <column name="INHERITS_ENVIRONMENT_TAGS" type="BOOLEAN" defaultValueBoolean="true">
        <constraints nullable="false" />
      </column>
    </addColumn>
  </changeSet>

  <changeSet id="tm-4.0.0-sqmap-879-add-project-level-credentials" author="pckerneis">
    <addColumn tableName="STORED_CREDENTIALS">
      <column name="PROJECT_ID" type="BIGINT" defaultValue="null"
              remarks="Foreign key to the project bound to these credentials. NULL for app-level and user-level credentials.">
        <constraints nullable="true"
                     foreignKeyName="fk_stored_credentials_project" references="PROJECT(PROJECT_ID)"
                     deleteCascade="true"/>
      </column>
    </addColumn>
  </changeSet>

  <changeSet id="tm-4.0.0-SQUASH-4923-remove-orphan-project-filters" author="dclaerhout">
    <sql>
      DELETE FROM PROJECT_FILTER_ENTRY
      WHERE FILTER_ID in(
          SELECT pf.PROJECT_FILTER_ID
          FROM PROJECT_FILTER AS pf
          LEFT JOIN CORE_USER AS cu ON cu.LOGIN = pf.USER_LOGIN
          WHERE cu.LOGIN IS NULL
      );

      DELETE FROM PROJECT_FILTER
      WHERE USER_LOGIN in(
        SELECT * FROM (
                        SELECT pf.USER_LOGIN
                        FROM PROJECT_FILTER AS pf
                               LEFT JOIN CORE_USER AS cu ON cu.LOGIN = pf.USER_LOGIN
                        WHERE cu.LOGIN IS NULL
                      ) AS user_logins
      );
    </sql>
  </changeSet>

  <changeSet id="tm-4.0.0-squash-5384-taserver-observer-url-column" author="pckerneis">
    <comment>Add OBSERVER_URL column to TEST_AUTOMATION_SERVER</comment>
    <addColumn tableName="TEST_AUTOMATION_SERVER">
      <column name="OBSERVER_URL" type="VARCHAR(255)" defaultValue="NULL">
        <constraints nullable="true"/>
      </column>
    </addColumn>
  </changeSet>
</databaseChangeLog>
