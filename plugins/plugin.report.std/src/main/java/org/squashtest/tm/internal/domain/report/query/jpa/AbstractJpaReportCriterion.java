/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.query.jpa;

import java.util.Arrays;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public abstract class AbstractJpaReportCriterion {
    protected Object[] parameters = new Object[0];

    public void setParameter(Object... parameters) {
        if (parameters != null) {
            this.parameters = Arrays.copyOf(parameters, parameters.length);
        }
    }

    public Object[] getParameters() {
        return parameters;
    }

    public abstract <X> Predicate makePredicate(CriteriaBuilder cb, Root<X> root);

    protected Object requireUniqueParameter() {
        if (parameters.length != 1) {
            throw new IllegalArgumentException(
                    "Criterion of type %s cannot have more than one argument"
                            .formatted(this.getClass().getSimpleName()));
        }

        return parameters[0];
    }

    protected Predicate noopPredicate(CriteriaBuilder cb) {
        return cb.isTrue(cb.literal(true));
    }
}
