/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.common.jpa;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.squashtest.tm.domain.campaign.Campaign;

public class CampaignBelongToMilestoneJpaCriterion extends AbstractIsInSetJpaCriterion<Long> {

    public CampaignBelongToMilestoneJpaCriterion() {
        super("id");
    }

    @Override
    public <X> Predicate makePredicate(CriteriaBuilder cb, Root<X> root) {
        List<?> milestoneIds = getTypedParameters();

        if (milestoneIds.isEmpty()) {
            return noopPredicate(cb);
        } else {
            CriteriaQuery<Long> subQuery = cb.createQuery(Long.class);
            Root<Campaign> subRoot = subQuery.from(Campaign.class);
            subQuery.select(subRoot.get("id"));
            subQuery.where(subRoot.join("milestones").get("id").in(milestoneIds));

            return cb.in(root.get(attributePath)).value(subQuery);
        }
    }

    @Override
    public Long fromValueToTypedValue(Object o) {
        return Long.parseLong(o.toString());
    }
}
