/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.common.jpa;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.internal.domain.report.query.jpa.AbstractJpaReportCriterion;

public class AboveDateJpaCriterion extends AbstractJpaReportCriterion {
    private static final Logger LOGGER = LoggerFactory.getLogger(AboveDateJpaCriterion.class);

    private final String attributePath;

    public AboveDateJpaCriterion(String attributePath) {
        this.attributePath = attributePath;
    }

    private Date makeDate() throws IllegalArgumentException {
        final Date date = (Date) requireUniqueParameter();
        final Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }

    @Override
    public <X> Predicate makePredicate(CriteriaBuilder cb, Root<X> root) {
        try {
            Date arg = makeDate();
            return cb.greaterThan(root.get(attributePath), arg);
        } catch (IllegalArgumentException | ClassCastException e) {
            LOGGER.error("Error while building criterion, defaulting to no-op predicate.", e);
            return noopPredicate(cb);
        }
    }
}
