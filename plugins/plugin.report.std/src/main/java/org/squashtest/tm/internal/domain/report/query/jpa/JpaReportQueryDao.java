/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.query.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.internal.domain.report.query.ReportQuery;
import org.squashtest.tm.internal.repository.ReportQueryDao;

@Repository
public class JpaReportQueryDao implements ReportQueryDao {

    @PersistenceContext private EntityManager entityManager;

    @Override
    public List<Object> executeQuery(ReportQuery query) {
        AbstractJpaReportQuery<?> jpaQuery = (AbstractJpaReportQuery<?>) query;

        if (jpaQuery.isCriteriaQueryBased()) {
            CriteriaQuery<?> criteriaQuery = jpaQuery.buildCriteriaQuery(entityManager);
            List<?> result = entityManager.createQuery(criteriaQuery).getResultList();
            return jpaQuery.convertToDto(result);
        } else {
            List<?> result = jpaQuery.execute(entityManager);
            return jpaQuery.convertToDto(result);
        }
    }
}
