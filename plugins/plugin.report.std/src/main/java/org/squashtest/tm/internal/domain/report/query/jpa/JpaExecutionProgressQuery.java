/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.query.jpa;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.internal.domain.report.common.dto.ExProgressCampaignDto;
import org.squashtest.tm.internal.domain.report.common.dto.ExProgressIterationDto;
import org.squashtest.tm.internal.domain.report.common.dto.ExProgressProjectDto;
import org.squashtest.tm.internal.domain.report.common.dto.ExProgressTestPlanDto;
import org.squashtest.tm.internal.domain.report.common.jpa.AboveDateJpaCriterion;
import org.squashtest.tm.internal.domain.report.common.jpa.AbstractIsInSetJpaCriterion;
import org.squashtest.tm.internal.domain.report.common.jpa.BelowDateJpaCriterion;
import org.squashtest.tm.internal.domain.report.common.jpa.CampaignBelongToMilestoneJpaCriterion;
import org.squashtest.tm.internal.domain.report.common.jpa.IsRunningCampaignJpaCriterion;

public class JpaExecutionProgressQuery extends AbstractJpaReportQuery<Campaign> {

    private static final String CRIT_NAME_MILESTONE = "milestones";

    public JpaExecutionProgressQuery() {
        criteriaByName.put(
                "scheduledStart", new AboveDateJpaCriterion("scheduledPeriod.scheduledStartDate"));

        criteriaByName.put("actualStart", new AboveDateJpaCriterion("actualPeriod.actualStartDate"));

        criteriaByName.put(
                "scheduledEnd", new BelowDateJpaCriterion("scheduledPeriod.scheduledEndDate"));

        criteriaByName.put("actualEnd", new BelowDateJpaCriterion("actualPeriod.actualEndDate"));

        criteriaByName.put("campaignStatus", new IsRunningCampaignJpaCriterion());

        criteriaByName.put("campaignIds[]", new CampaignIdIsInIds());

        criteriaByName.put(CRIT_NAME_MILESTONE, new CampaignBelongToMilestoneJpaCriterion());
    }

    @Override
    public boolean isCriteriaQueryBased() {
        return true;
    }

    @Override
    protected Class<Campaign> getEntityType() {
        return Campaign.class;
    }

    private static class CampaignIdIsInIds extends AbstractIsInSetJpaCriterion<Long> {

        public CampaignIdIsInIds() {
            super("id");
        }

        @Override
        public Long fromValueToTypedValue(Object o) {
            return Long.parseLong(o.toString());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <R, D> List<D> convertToDto(List<R> rawData) {
        List<Campaign> unfilteredList = (List<Campaign>) rawData;

        // phase 0 : let's filter the unwanted data out.
        List<Campaign> campaignList = filterUnwantedDataOut(unfilteredList);

        // bonus phase : if the perimeter is milestone based, find the milestone label.
        String milestone = findMilestoneLabel(campaignList);

        // phase 1 : let's iterate over campaigns to find the projects (see note above)
        // we'll create the corresponding ExProgressProjectDto and store them in a map, of which the Id
        // of the project
        // is the key.

        Map<Long, ExProgressProjectDto> projectMap = new HashMap<>();

        for (Campaign campaign : campaignList) {

            Project project = campaign.getProject();

            if (!projectMap.containsKey(project.getId())) {
                ExProgressProjectDto projectDto = new ExProgressProjectDto();
                projectDto.setName(project.getName());
                projectDto.setId(project.getId());
                projectDto.setAllowsSettled(
                        project.getCampaignLibrary().allowsStatus(ExecutionStatus.SETTLED));
                projectDto.setAllowsUntestable(
                        project.getCampaignLibrary().allowsStatus(ExecutionStatus.UNTESTABLE));
                if (milestone != null) {
                    projectDto.setMilestone(milestone);
                }
                projectMap.put(project.getId(), projectDto);
            }
        }

        // phase 2 : we generate the other dtos (campaigns and so on) and add them to the right
        // instances of
        // the project dto, using the map defined above. Again, we build here the projects using the
        // campaigns.

        for (Campaign campaign : campaignList) {
            ExProgressCampaignDto campDto = makeCampaignDto(campaign);

            Project project = campaign.getProject();

            ExProgressProjectDto projectDto = projectMap.get(project.getId());
            campDto.setProject(projectDto);
            projectDto.addCampaignDto(campDto);
        }

        // phase 3 : return the list and we're done !

        fillProjectStatusInfos(projectMap);

        List<ExProgressProjectDto> projectList = new LinkedList<>(projectMap.values());

        if (projectList.isEmpty()) {
            projectList.add(new ExProgressProjectDto());
        }

        return (List<D>) projectList;
    }

    private void fillProjectStatusInfos(Map<Long, ExProgressProjectDto> projectMap) {
        for (Entry<Long, ExProgressProjectDto> entry : projectMap.entrySet()) {
            ExProgressProjectDto projectDto = entry.getValue();
            projectDto.fillStatusInfosWithChildren(projectDto.getCampaigns());
        }
    }

    protected List<Campaign> filterUnwantedDataOut(List<Campaign> list) {
        List<Campaign> toReturn = new LinkedList<>();
        for (Campaign campaign : list) {
            if (getDataFilteringService().isFullyAllowed(campaign)) {
                toReturn.add(campaign);
            }
        }
        return toReturn;
    }

    private ExProgressCampaignDto makeCampaignDto(Campaign campaign) {
        ExProgressCampaignDto campDto = new ExProgressCampaignDto().fillBasicInfos(campaign);

        for (Iteration iteration : campaign.getIterations()) {
            ExProgressIterationDto iterDto = makeIterationDto(iteration);
            campDto.addIterationDto(iterDto);
            iterDto.setCampaign(campDto);
        }
        campDto.fillStatusInfosWithChildren(campDto.getIterations());
        return campDto;
    }

    private ExProgressIterationDto makeIterationDto(Iteration iteration) {
        ExProgressIterationDto iterDto = new ExProgressIterationDto(iteration);

        for (IterationTestPlanItem testPlan : iteration.getTestPlans()) {
            ExProgressTestPlanDto testPlanDto = makeTestPlanDto(testPlan);
            iterDto.addTestPlanDto(testPlanDto);
            testPlanDto.setIteration(iterDto);
        }

        return iterDto;
    }

    private ExProgressTestPlanDto makeTestPlanDto(IterationTestPlanItem testPlan) {
        return new ExProgressTestPlanDto().fillBasicInfo(testPlan);
    }

    private String findMilestoneLabel(List<Campaign> campaignList) {
        AbstractJpaReportCriterion milestoneCrit =
                (AbstractJpaReportCriterion) criteriaByName.get(CRIT_NAME_MILESTONE);

        if (milestoneCrit != null) {
            Object[] ids = milestoneCrit.getParameters();
            return findMilestoneLabelFromCriteria(ids, campaignList);
        }

        return null;
    }

    private String findMilestoneLabelFromCriteria(Object[] ids, List<Campaign> campaignList) {
        if (ids != null && ids.length > 0 && !campaignList.isEmpty()) {
            // for now we support only one milestone
            Long milestoneId = Long.valueOf(ids[0].toString());

            Campaign c = campaignList.get(0);
            for (Milestone m : c.getMilestones()) {
                if (m.getId().equals(milestoneId)) {
                    return m.getLabel();
                }
            }
        }
        return null;
    }
}
