/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.query.jpa;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.squashtest.tm.internal.domain.report.query.ReportQuery;
import org.squashtest.tm.plugin.report.std.service.DataFilteringService;

// TODO separate between criteria-based and non-criteria-based queries
public abstract class AbstractJpaReportQuery<T> implements ReportQuery {

    protected Map<String, AbstractJpaReportCriterion> criteriaByName = new HashMap<>();

    private DataFilteringService filterService;

    @Override
    public void setCriterion(String name, Object... parameters) {
        AbstractJpaReportCriterion criterion = criteriaByName.get(name);

        if (criterion != null) {
            criterion.setParameter(parameters);
        } else {
            throw new IllegalArgumentException(
                    "parameter %s does not exists for query %s"
                            .formatted(name, this.getClass().getSimpleName()));
        }
    }

    @Override
    public void setDataFilteringService(DataFilteringService service) {
        this.filterService = service;
    }

    protected Class<T> getEntityType() {
        throw new UnsupportedOperationException("Not implemented. You should override this method.");
    }

    public <D> List<D> execute(EntityManager entityManager) {
        throw new UnsupportedOperationException("Not implemented. You should override this method.");
    }

    public abstract <R, D> List<D> convertToDto(List<R> rawData);

    protected DataFilteringService getDataFilteringService() {
        return filterService;
    }

    @Override
    public Collection<String> getCriterionNames() {
        return criteriaByName.keySet();
    }

    @Override
    public Object[] getValue(String key) {
        AbstractJpaReportCriterion criterion = criteriaByName.get(key);
        if (criterion != null) {
            return criterion.getParameters();
        } else {
            return new Object[0];
        }
    }

    public abstract boolean isCriteriaQueryBased();

    public CriteriaQuery<T> buildCriteriaQuery(EntityManager entityManager) {
        final Class<T> entityType = this.getEntityType();

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityType);
        final Root<T> root = criteriaQuery.from(entityType);

        final List<Predicate> predicates = buildAllPredicates(criteriaBuilder, root);

        criteriaQuery.select(root).where(predicates.toArray(new Predicate[0]));

        return criteriaQuery;
    }

    private List<Predicate> buildAllPredicates(CriteriaBuilder cb, Root<?> root) {
        return criteriaByName.values().stream()
                .map(abstractJpaReportCriterion -> abstractJpaReportCriterion.makePredicate(cb, root))
                .toList();
    }
}
