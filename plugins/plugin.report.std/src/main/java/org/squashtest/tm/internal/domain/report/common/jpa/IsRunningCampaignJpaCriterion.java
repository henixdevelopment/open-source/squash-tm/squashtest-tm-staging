/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.common.jpa;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.internal.domain.report.common.dto.ExProgressCampaignStatus;
import org.squashtest.tm.internal.domain.report.query.jpa.AbstractJpaReportCriterion;

/*
 * This ReportCriterion cannot be reused due to its very specific content. It'll basically check that a campaign have
 * at least one iteration having a state of ExecutionStatus.READY or ExecutionStatus.RUNNING, or not, depending on the
 * parameter (an ExProgressCampaignStatus).
 *
 * Actually it uses a sub query that returns the list of ids of campaigns like described above, then embed the sub query in
 * a Criterion testing if a given campaign (from the main query and identified by its alias) is
 * 	- among them (if the parameter is CAMPAIGN_RUNNING),
 *  - or not among them (parameter is CAMPAIGN_OVER),
 *  - or if we don't care at all (parameter is CAMPAIGN_ALL).
 */
public class IsRunningCampaignJpaCriterion extends AbstractJpaReportCriterion {

    private static final Logger LOGGER = LoggerFactory.getLogger(IsRunningCampaignJpaCriterion.class);

    @Override
    public <X> Predicate makePredicate(CriteriaBuilder cb, Root<X> root) {
        try {
            Object[] values = getParameters();

            if (values != null && values.length > 0) {
                final ExProgressCampaignStatus status =
                        ExProgressCampaignStatus.valueOf(values[0].toString());

                final CriteriaQuery<Long> cq = cb.createQuery(Long.class);
                final Subquery<Long> subquery = cq.subquery(Long.class);
                final Root<Campaign> subRoot = subquery.from(Campaign.class);
                subquery.select(subRoot.get("id"));
                subquery.where(
                        cb.or(
                                cb.equal(
                                        subRoot.join("iterations").join("testPlans").get("executionStatus"),
                                        ExecutionStatus.READY),
                                cb.equal(
                                        subRoot.join("iterations").join("testPlans").get("executionStatus"),
                                        ExecutionStatus.RUNNING)));

                if (status == ExProgressCampaignStatus.CAMPAIGN_RUNNING) {
                    return cb.in(root.get("id")).value(subquery);
                } else if (status == ExProgressCampaignStatus.CAMPAIGN_OVER) {
                    return cb.not(cb.in(root.get("id")).value(subquery));
                } else {
                    return noopPredicate(cb);
                }
            }

            return null;
        } catch (Exception e) {
            LOGGER.error("Error while building criterion, defaulting to no-op predicate.", e);
            return noopPredicate(cb);
        }
    }
}
