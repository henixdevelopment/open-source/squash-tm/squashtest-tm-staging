/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.std.query;

import static org.squashtest.tm.plugin.report.std.query.ExecutionProgressQueryAdapter.CAMPAIGN_SELECTION_MODE;
import static org.squashtest.tm.plugin.report.std.query.ExecutionProgressQueryAdapter.MILESTONE_IDS;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Provider;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.internal.domain.report.query.jpa.AbstractJpaReportQuery;
import org.squashtest.tm.plugin.report.std.service.ReportService;
import org.squashtest.tm.service.milestone.MilestoneFinderService;

public abstract class AbstractJpaQueryAdapter<T, QUERY extends AbstractJpaReportQuery<T>>
        implements ReportQuery {

    @Inject private ReportService reportService;

    @Inject private MilestoneFinderService milestoneFinderService;

    protected AbstractJpaQueryAdapter() {
        super();
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void executeQuery(Map<String, Criteria> criteria, Map<String, Object> model) {
        AbstractJpaReportQuery<T> jpaReportQuery = getLegacyQueryProvider().get();

        processNonStandardCriteria(criteria, jpaReportQuery);
        processStandardCriteria(criteria, jpaReportQuery);

        Collection<Object> data = reportService.executeQuery(jpaReportQuery);
        model.put("data", data);

        /*
         * Feat 3629 : we need to put the milestone label as a parameter of the reports.
         *
         * SQUASH-3521: we get the milestone id from the criteria to display the report
         * title in milestone picker mode.
         * At present, we only support the selection of one milestone in the report criteria
         * so getting the first element is ok. May need to evolve if multi-selection becomes possible
         */
        Criteria selectionMode = criteria.get(CAMPAIGN_SELECTION_MODE);

        if (selectionMode != null && "MILESTONE_PICKER".equals(selectionMode.getValue())) {
            List<Integer> milestoneIds = (List<Integer>) criteria.get(MILESTONE_IDS).getValue();
            Milestone milestone = milestoneFinderService.findById(milestoneIds.get(0).longValue());
            model.put("milestoneLabel", milestone.getLabel());
        }
    }

    /** Should add any non-standard criteria to the legacy query. */
    protected abstract void processNonStandardCriteria(
            Map<String, Criteria> criteria, AbstractJpaReportQuery<T> jpaReportQuery);

    /**
     * Adds any standard criteria as is to the legacy query. "standardness" is defined by
     * #isStandardCriteria()
     */
    private void processStandardCriteria(
            Map<String, Criteria> criteria, AbstractJpaReportQuery<T> jpaReportQuery) {
        for (Map.Entry<String, Criteria> entry : criteria.entrySet()) {
            if (isStandardCriteria(entry.getKey())) {
                jpaReportQuery.setCriterion(entry.getKey(), entry.getValue().getValue());
            }
        }
    }

    /**
     * Should return true if criteria is standard, meaning it will be passed as is to the legacy
     * query.
     */
    protected abstract boolean isStandardCriteria(String criterionName);

    /**
     * @return the legacyQueryProvider
     */
    protected abstract Provider<QUERY> getLegacyQueryProvider();
}
