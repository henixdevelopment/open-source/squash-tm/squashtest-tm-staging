/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.domain.report.common.jpa;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.squashtest.tm.internal.domain.report.query.jpa.AbstractJpaReportCriterion;

/*
 * This ReportCriterion will check if the value at the given propertyPath is an element of the list of parameters.
 * This ReportCriterion is special because we need to define a callback for it, see fromValueToTypedValue(). This callback
 * exists to define how to cast each element of the parameter list to the right type.
 */
public abstract class AbstractIsInSetJpaCriterion<T extends Number>
        extends AbstractJpaReportCriterion {

    protected final String attributePath;

    protected AbstractIsInSetJpaCriterion(String attributePath) {
        this.attributePath = attributePath;
    }

    @Override
    public <X> Predicate makePredicate(CriteriaBuilder cb, Root<X> root) {
        List<?> typedParameters = getTypedParameters();

        if (typedParameters.isEmpty()) {
            return noopPredicate(cb);
        } else {
            return cb.in(root.get(attributePath)).value(getTypedParameters());
        }
    }

    protected List<T> getTypedParameters() {
        Object[] rawParameters = getParameters();

        if (rawParameters == null || rawParameters.length == 0) {
            return Collections.emptyList();
        }

        return Arrays.stream(rawParameters).map(this::fromValueToTypedValue).toList();
    }

    public abstract T fromValueToTypedValue(Object o);
}
