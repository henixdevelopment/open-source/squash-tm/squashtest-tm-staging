/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.testautomation.jenkins.internal.net

import org.squashtest.tm.service.internal.configuration.CallbackUrlProvider
import org.squashtest.tm.service.testautomation.spi.BadConfiguration
import spock.lang.Specification

class CallbackUrlProviderTest extends Specification {

	private CallbackUrlProvider urlProvider = new CallbackUrlProvider()

	def "getCallbackUrl() - Should throw BadConfiguration Exception because the property is not set correctly."() {
		given:
		urlProvider.callbackUrlFromDatabase = urlFromDatabase

		when:
		urlProvider.getCallbackUrl()

		then:
		BadConfiguration bc = thrown(BadConfiguration)
		bc.messageArgs()[0] == exceptionProperty

		where:
		urlFromDatabase 				| exceptionProperty
		null							| null
		""								| null
		"invalid/url"					| "squashtest.tm.callbackurl"
	}

	def "getCallbackUrl() - Should find the property set correctly in Database or in Configuration file and return it."() {
		given:
		urlProvider.callbackUrlFromDatabase = "http://database:8080/squash"

		when:
		URL result = urlProvider.getCallbackUrl()

		then:
		result == new URL("http://database:8080/squash")
	}
}
