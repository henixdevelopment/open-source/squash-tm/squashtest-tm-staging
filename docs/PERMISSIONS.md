# Permissions

## Introduction
This documentation provides detailed information about the permission management
system classes in the application.
This includes classes and enums related to defining and managing permissions.
On one hand, the permissions system manages user actions based on their rights,
while on the other hand, there are permissions in place to control access
to specific areas of the application's user interface (UI).

## Permission classes

### Spring Security permissions
In the context of Spring Security, the permissions system plays a crucial role in controlling access to resources within the application.

#### BasePermission (class)
The `BasePermission` class is the one defined in Spring Security, with the basic permissions:
- READ
- WRITE
- CREATE
- DELETE
- ADMINISTRATION

We do not use the `ADMINISTRATION` permission.


#### CustomPermission (class)
`CustomPermission` class enables the creation of specialized permissions tailored to specific requirements or unique scenarios within the Squash application.
_This is the `CustomPermissionFactory` which adds these custom permissions to Spring Security and our Permission Evaluator Service._

The custom permissions are:
- MANAGE_PROJECT
- EXPORT
- EXECUTE
- LINK
- IMPORT
- ATTACH
- EXTENDED_DELETE → for example, the possibility to delete an iteration test plan item with executions
- READ_UNASSIGNED → for example, test runner profile do not have this permission because they only need to see their tests in the execution workspace
- WRITE_AS_FUNCTIONAL
- WRITE_AS_AUTOMATION
- MANAGE_MILESTONE
- MANAGE_PROJECT_CLEARANCE

#### Permissions (enum)
Using an enum is preferred, and we consolidate the previous two classes into a single enum.
All elements are dependant on the `Permission` class from Spring Security.

### UI permissions

#### AccessPermission (enum)
Introducted in Squash 8, this enum consolidates the existing permissions to determine the rights of the current user.
Based on this information, we can determine if the user:
- is a project manager (has access to the administration workspace),
- is an automation programmer (has access to the automation workspace automation tester view),
- is a functional tester (has access to the automation workspace tester view)
- has any READ permission, enabling navigation on other workspaces

In the case of an automation programmer who is not a functional tester,
they will see only the automation workspace in the navigation bar.
This scenario corresponds to a user with clearances limited to automation test writer profile.

Prior to Squash 8, we used the following roles to manage this: (`ROLE_TM_PROJECT_MANAGER`, `ROLE_TF_FUNCTIONAL_TESTER`, `ROLE_TF_AUTOMATION_PROGRAMMER`).


## Roles
There are two roles defined:
- ROLE_ADMIN for administrator users
- ROLE_TA_API_CLIENT for test automation server technical users

Additionally, there is a `ROLE_TM_USER`. I don't think this is used anymore, but this may be useful.


## Profiles
_It would be beneficial to fetch permissions directly from the database. Additionally, we can find more detailed information in the tables documented online for Squash._

The following data in the database will undergo adjustments to address any inconsistencies or misunderstandings.

- **Project:**

| PROFILE | ID | PERMISSIONS                                                              |
| ------- | -- |--------------------------------------------------------------------------|
| Test editor | 2 | READ                                                                     |
| Project viewer | 4 | READ                                                                     |
| Project leader | 5 | READ, IMPORT, MANAGE_PROJECT, MANAGE_MILESTONE, MANAGE_PROJECT_CLEARANCE |
| Test runner | 6 | READ                                                                     |
| Test designer | 7 | READ                                                                     |
| Advance tester | 8 | READ                                                                     |
| Validator | 9 | READ                                                                     |
| Automated test writer | 10 | READ                                                                     |

- **Requirement library:**

| PROFILE | ID | PERMISSIONS |
| ------- | -- | ----------- |
| Test editor | 2 | READ, WRITE, CREATE, DELETE, EXPORT, LINK, ATTACH |
| Project viewer | 4 | READ |
| Project leader | 5 | READ, WRITE, CREATE, DELETE, EXPORT, LINK, IMPORT, ATTACH |
| Test runner | 6 | READ, EXPORT |
| Test designer | 7 | READ, WRITE, CREATE, DELETE, EXPORT, LINK, ATTACH |
| Advance tester | 8 | READ, EXPORT, LINK |
| Validator | 9 | READ, WRITE, EXPORT, ATTACH |
| Automated test writer | 10 | READ |

- **Test case library:**

| PROFILE | ID | PERMISSIONS |
| ------- | -- | ----------- |
| Test editor | 2 | READ, WRITE, CREATE, DELETE, EXPORT, LINK, ATTACH |
| Project viewer | 4 | READ |
| Project leader | 5 | READ, WRITE, CREATE, DELETE, EXPORT, LINK, IMPORT, ATTACH |
| Test runner | 6 | READ, EXPORT |
| Test designer | 7 | READ, WRITE, CREATE, DELETE, EXPORT, LINK, ATTACH |
| Advance tester | 8 | READ, WRITE, EXPORT, LINK |
| Validator | 9 | READ, WRITE, EXPORT, ATTACH |
| Automated test writer | 10 | READ, WRITE_AS_AUTOMATION |

- **Campaign library:**

| PROFILE | ID | PERMISSIONS |
| ------- | -- | ----------- |
| Test editor | 2 | READ, WRITE, CREATE, DELETE, EXPORT, EXECUTE, LINK, ATTACH, READ_UNASSIGNED |
| Project viewer | 4 | READ, READ_UNASSIGNED |
| Project leader | 5 | READ, WRITE, CREATE, DELETE, EXPORT, EXECUTE, LINK, IMPORT, ATTACH, EXTENDED_DELETE, READ_UNASSIGNED |
| Test runner | 6 | READ, EXECUTE, ATTACH |
| Test designer | 7 | READ, READ_UNASSIGNED |
| Advance tester | 8 | READ, EXPORT, EXECUTE, ATTACH, READ_UNASSIGNED |
| Validator | 9 | READ, READ_UNASSIGNED |
| Automated test writer | 10 | READ |

- **Project template:**

no profile has any permission

- **Custom report library:**

| PROFILE | ID | PERMISSIONS |
| ------- | -- | ----------- |
| Test editor | 2 | READ, WRITE, CREATE, DELETE, ATTACH |
| Project viewer | 4 | READ |
| Project leader | 5 | READ, WRITE, CREATE, DELETE, ATTACH |
| Test runner | 6 | READ |
| Test designer | 7 | READ |
| Advance tester | 8 | READ |
| Validator | 9 | READ |
| Automated test writer | 10 | READ |

- **Automation request library:**

| PROFILE | ID | PERMISSIONS |
| ------- | -- | ----------- |
| Test editor | 2 | WRITE_AS_FUNCTIONAL |
| Project viewer | 4 |  |
| Project leader | 5 | WRITE_AS_FUNCTIONAL, WRITE_AS_AUTOMATION |
| Test runner | 6 |  |
| Test designer | 7 | WRITE_AS_FUNCTIONAL |
| Advance tester | 8 | WRITE_AS_FUNCTIONAL |
| Validator | 9 | WRITE_AS_FUNCTIONAL |
| Automated test writer | 10 | WRITE_AS_AUTOMATION |

- **Action word library:**

| PROFILE | ID | PERMISSIONS |
| ------- | -- | ----------- |
| Test editor | 2 | READ, WRITE, CREATE, DELETE, EXPORT, ATTACH |
| Project viewer | 4 | READ |
| Project leader | 5 | READ, WRITE, CREATE, DELETE, EXPORT, IMPORT, ATTACH |
| Test runner | 6 | READ, EXPORT |
| Test designer | 7 | READ, WRITE, CREATE, DELETE, EXPORT, ATTACH |
| Advance tester | 8 | READ, WRITE, EXPORT |
| Validator | 9 | READ, WRITE, EXPORT,ATTACH|
| Automated test writer | 10 | READ |
