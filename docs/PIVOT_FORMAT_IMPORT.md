# Pivot Format Import

## Introduction

This document describes how the pivot format works to import all kind of entities in Squash.

The file used for pivot format is a Zip file containing multiple json files and an optional "attachments" folder. Each json file will handle one type of entity for the import.

You can name the zip file used for pivot format as you wish. On the other hand, the json files inside must have specific names which are the following:

  - `called_test_cases.json`
  - `campaign_folders.json`
  - `campaigns.json`
  - `custom_fields.json`
  - `executions.json`
  - `iterations.json`
  - `requirement_folders.json`
  - `requirements.json`
  - `test_case_folders.json`
  - `test_cases.json`
  - `test_suites.json`

Each entity described in a json file will have its own internal ID so that it can be referenced by another (for example, to link a requirement to a test case).


## How the importer is processing the files

In order to avoid memory problems, the importer will stream each json files inside the zip and will progressively create entities while streaming.

If an error occurs during the process, all the created entities in the database will be roll backed.

In order to be able to create links between entities, the importer will read the json files inside the zip file in a specific order to make sure that the linked entity has already been created in squash database.

The files will be read in the following order:

custom_fields.json > requirement_folders.json > requirements.json > test_case_folders.json > test_cases.json > called_test_cases.json > campaign_folders.json > campaigns.json > iterations.json > test_suites.json > executions.json


Even if the zip file does not contain every json files mentioned above, the import will still work perfectly fine.

For example, you could have a zip file containing only a test_case_folders.json and a test_cases.json files. In that scenario, none of the test cases in test_cases.json file must have a linked requirement because if the linked requirement is not described in a requirements.json file, the import will break.


The current format pivot does not allow to link an imported entity to an entity which already exists in Squash. You can only make links between entities described in json files by using their internal ID.


## Expected format for each json file

We will now describe each json file and which format is expected. 

## Custom fields

The example below will contain each type of custom field existing in squash:

```json
{
    "custom_fields": [
        {
            "id": "cuf1",
            "input_type": "CHECKBOX",
            "name": "cf_checkBox",
            "label": "Checkbox custom field",
            "code": "CF_CHECK",
            "default_value": "true",
            "optional": false,
            "bound_entities": ["REQUIREMENT_VERSION", "REQUIREMENT_FOLDER", "TESTCASE_FOLDER", "TEST_CASE", "TEST_STEP", "EXECUTION", "EXECUTION_STEP"]
        },
        {
            "id": "cuf2",
            "input_type": "PLAIN_TEXT",
            "name": "cf_plain_text",
            "label": "Plain text custom field",
            "code": "CF_PT",
            "optional": true,
            "bound_entities": ["REQUIREMENT_VERSION", "TESTCASE_FOLDER", "TEST_CASE", "TEST_STEP", "EXECUTION", "EXECUTION_STEP"]
        },
        {
            "id": "cuf3",
            "input_type": "RICH_TEXT",
            "name": "cf_rich_text",
            "label": "Rich text custom field",
            "code": "CF_RT",
            "optional": true,
            "bound_entities": ["REQUIREMENT_VERSION", "REQUIREMENT_FOLDER"]
        },
        {
            "id": "cuf4",
            "input_type": "DROPDOWN_LIST",
            "name": "cf_dropdown",
            "label": "Dropdown custom field",
            "code": "CF_DDL",
            "default_value": "opt1",
            "optional": true,
            "options": [
                {
                    "name": "opt1",
                    "code": "optCode1",
                    "color": "#FF0000"
                },
                {
                    "name": "opt2",
                    "code": "optCode2",
                    "color": null
                }
            ],
            "bound_entities": ["REQUIREMENT_VERSION"]
        },
	{
            "id": "cuf5",
            "input_type": "TAG",
            "name": "cf_tag",
            "label": "Tag custom field",
            "code": "CF_TAG",
            "default_value": "tag1|tag2",
            "optional": true,
            "bound_entities": ["REQUIREMENT_VERSION"]
        },
        {
            "id": "cuf6",
            "input_type": "NUMERIC",
            "name": "cf_numeric",
            "label": "Numeric custom field",
            "code": "CF_NUM",
            "optional": true,
            "bound_entities": ["REQUIREMENT_VERSION"]
        },
		{
            "id": "cuf7",
            "input_type": "DATE_PICKER",
            "name": "cf_date",
            "label": "Date custom field",
            "code": "CF_DATE",
            "optional": true,
            "bound_entities": ["REQUIREMENT_VERSION"]
        }
    ]
}
```

### Possible values for specifics fields

#### "input_type" field

The input_type field will determine which kind of squash custom field you wish to import.

Here are the different input types (must be written in uppercase):

- "CHECKBOX"
- "PLAIN_TEXT"
- "RICH_TEXT"
- "DROPDOWN_LIST"
- "TAG"
- "NUMERIC"
- "DATE_PICKER"

#### "bound_entities" field

This field will determine to which kind of entity inside the project you wish to attach the custom field.

Here are all the possible values for Squash entities (must be written in uppercase):

- "TEST_CASE_LIBRARY"
- "TEST_CASE_FOLDER"
- "TEST_CASE"
- "TEST_CASE_STEP"
- "REQUIREMENT_LIBRARY"
- "REQUIREMENT_FOLDER"
- "REQUIREMENT_VERSION"
- "CAMPAIGN_LIBRARY"
- "CAMPAIGN_FOLDER"
- "CAMPAIGN"
- "ITERATION"
- "EXECUTION"
- "TEST_SUITE"
- "EXECUTION_STEP"

### Optional json fields for a custom field

Here are the fields that are not mandatory to write in the json to describe a custom field:

- "default_value" : this field is optional except for CHECKBOX custom fields which require to have a default value set to true or false.
- "options": this field is only used by DROPDOWN_LIST custom fields.


## Requirement folder

Json example:

```json
{
	"requirement_folders": [
		{
			"id": "F1",
			"name": "folder 1",
			"description": "desc 1",
			"custom_fields": [
				{
					"id": "cuf1",
					"value": "true"
				},
				{
					"id": "cuf2",
					"value": "my first value"
				}				
			],
			"parent_type": "REQUIREMENT_LIBRARY"
		},
		{
			"id": "F11",
			"name": "folder 1.1",
			"parent_type": "REQUIREMENT_FOLDER",
			"parent_id": "F1"
		},
		{
			"id": "F111",
			"name": "folder 1.1.1",
			"parent_type": "REQUIREMENT_FOLDER",
			"parent_id": "F11"
		}
	]
}
```
### Possible values for specifics fields

#### "parent_type" field

A requirement folder has two possible parent types (must be written in uppercase):

- "REQUIREMENT_LIBRARY"
- "REQUIREMENT_FOLDER"


### Optional json fields for a requirement folder

Here are the fields that are not mandatory to write in the json to describe a requirement folder:

- "description"
- "custom_fields": the field is used to set a specific value to a chosen custom field. Be cautious that to use this field, in the custom_field.json file, the concerned custom field must have been correctly bound to a requirement folder (see previous section for more details)
- "attachments"

## Requirement

Json example:

````json
{
	"requirements": [
		{
			"id": "RA",
			"name": "req A",
			"description": "desc A",
			"reference": "reqA",
			"status": "APPROVED",
			"criticality": "MINOR",
			"category": "FUNCTIONAL",
			"custom_fields": [],
			"parent_type": "REQUIREMENT_LIBRARY"
		},
		{
			"id": "RAA",
			"name": "reqA.A",
			"description": "desc reqA.A",
			"reference": "reqA.A.AAA",
			"status": "UNDER_REVIEW",
			"criticality": "MINOR",
			"category": "UNDEFINED",
			"parent_type": "REQUIREMENT",
			"parent_id": "RA"
		},
		{
			"id": "RB",
			"name": "req B",
			"custom_fields": [
				{
					"id": "cuf1",
					"value": "true"
				},
				{
					"id": "cuf2",
					"value": "my value 3"
				},
				{
					"id": "cuf3",
					"value": "<p>mon texte riche</p>"
				},
				{
					"id": "cuf4",
					"value": "opt2"
				},
				{
					"id": "cuf5",
					"value": "tag1|tag2|tag3"
				},
				{
					"id": "cuf6",
					"value": "8"
				},
				{
					"id": "cuf7",
					"value": "2023-03-01"
				}
			],
			"parent_type": "REQUIREMENT_LIBRARY"
		},
        {
            "id": "RD",
            "name": "req D",
            "parent_type": "REQUIREMENT_FOLDER",
            "parent_id": "F11"
        }
	]
}
````

### Possible values for specifics fields

#### "status" field

Possible values (must be written in uppercase):

- "WORK_IN_PROGRESS"
- "UNDER_REVIEW"
- "APPROVED"
- "OBSOLETE"

#### "criticality" field

Possible values (must be written in uppercase):

- "MINOR"
- "MAJOR"
- "CRITICAL"
- "UNDEFINED"

#### "category" field

Possible values (must be written in uppercase):

- "UNDEFINED"
- "FUNCTIONAL"
- "NON_FUNCTIONAL"
- "USE_CASE"
- "BUSINESS"
- "TEST_REQUIREMENT"
- "ERGONOMIC"
- "PERFORMANCE"
- "TECHNICAL"
- "USER_STORY"
- "SECURITY"

#### "parent_type" field

A requirement has three possible parent types (must be written in uppercase):

- REQUIREMENT_LIBRARY
- REQUIREMENT_FOLDER
- REQUIREMENT

### Optional json fields for a requirement

Here are the fields that are not mandatory to write in the json to describe a requirement:

- "description"
- "reference"
- "status": If not specified, the following default value will be applied: "WORK_IN_PROGRESS"
- "criticality": If not specified, the following default value will be applied: "MINOR"
- "category": If not specified, the following default value will be applied: "UNDEFINED"
- "custom_fields"
- "attachments"


## Test case folder

Json example:

```json
{
    "test_case_folders": [
        {
            "id": "F1",
            "name": "folder 1",
            "description": "desc 1",
            "custom_fields": [
                {
                    "id": "cuf1",
                    "value": "true"
                },
                {
                    "id": "cuf2",
                    "value": "my first value"
                }
            ],
            "parent_type": "TEST_CASE_LIBRARY"
        },
        {
            "id": "F2",
            "name": "folder 2",
            "description": "desc 2",
            "custom_fields": [],
            "parent_type": "TEST_CASE_FOLDER",
            "parent_id": "F1"
        }
    ]
}
```
### Possible values for specifics fields

#### "parent_type" field

A test case folder has two possible parent types (must be written in uppercase):

- TEST_CASE_LIBRARY
- TEST_CASE_FOLDER


### Optional json fields for a test case folder

Here are the fields that are not mandatory to write in the json to describe a test case folder:

- "description"
- "custom_fields"
- "attachments"

## Test case

In this json, there will be one example for each type of test case (standard, gherkin, keyword, exploratory):

```json
{
	"test_cases": [
		{
			"id": "TCA",
			"test_case_kind": "STANDARD",
			"name": "TC Standard",
			"description": "desc A",
			"reference": "refA",
			"importance": "VERY_HIGH",
			"status": "WORK_IN_PROGRESS",
			"nature": "FUNCTIONAL_TESTING",
			"type": "UNDEFINED",
			"custom_fields": [
				{
					"id": "cuf1",
					"value": "true"
				},
				{
					"id": "cuf2",
					"value": "my cuf value"
				}
			],
			"dataset_params": [
				{
					"id": "P1",
					"name": "Param_1",
					"description": "param 1 decription"
				},
				{
					"id": "P2",
					"name": "Param_2",
					"description": "param 2 decription"
				},
				{
					"id": "P3",
					"name": "Param_3",
					"description": "param 3 decription"
				}
			],
			"datasets": [
				{
					"id": "DS1",
					"name": "Dataset 1",
					"param_values": [
						{
							"param_id": "P1",
							"value": "value1"
						},
						{
							"param_id": "P2",
							"value": "value2"
						},
						{
							"param_id": "P3",
							"value": "value3"
						}
					]
				},
				{
					"id": "DS2",
					"name": "Dataset 2",
					"param_values": [
						{
							"param_id": "P1",
							"value": "anotherValue1"
						},
						{
							"param_id": "P2",
							"value": "anotherValue2"
						},
						{
							"param_id": "P3",
							"value": "anotherValue3"
						}
					]
				}
			],
			"verified_requirements": ["RA", "RB"],
			"prerequisite": "prerequisite 1",
			"action_steps": [
				{
					"id": "AS1",
					"action": "One fist action",
					"expected_result": "The first expected result",
					"custom_fields": [
						{
							"id": "cuf1",
							"value": "true"
						},
						{
							"id": "cuf2",
							"value": "my cuf value"
						}
					],
					"verified_requirements": ["RC", "RD"]
				},
				{
					"id": "AS2",
					"action": "One second action",
					"expected_result": "The second expected result"
				}
			],
			"parent_type": "TEST_CASE_LIBRARY"
		},
		{
			"id": "TCB",
			"test_case_kind": "GHERKIN",
			"name": "TC Gherkin",
			"description": "desc B",
			"reference": "refB",
			"importance": "HIGH",
			"status": "UNDER_REVIEW",
			"nature": "BUSINESS_TESTING",
			"type": "COMPLIANCE_TESTING",
			"verified_requirements": ["RA", "RB"],
			"script": "# language: fr\n    Fonctionnalité: TC Gherkin\n    Scénario: Je veux un café\n    Soit Un distibuteur automatique de café\n    Quand J'insère 1 euro dans la machine\n    Alors La machine me sert un café\n    Et me rends 5 centimes de monnaie",
			"parent_type": "TEST_CASE_LIBRARY"
		},
		{
			"id": "TCC",
			"test_case_kind": "KEYWORD",
			"name": "TC Keyword",
			"description": "desc C",
			"reference": "refC",
			"importance": "MEDIUM",
			"status": "APPROVED",
			"nature": "USER_TESTING",
			"type": "CORRECTION_TESTING",
			"verified_requirements": ["RA", "RB"],
			"keyword_steps": [
				{
					"id": "KS1",
					"keyword": "GIVEN",
					"action_word": "I want to buy a coffee in the machine",
					"doc_string": "a docstring",
					"comment": "a comment"
				},
				{
					"id": "KS2",					
					"keyword": "WHEN",
					"action_word": "I insert money in the machine",
					"data_table": "| produit | prix |\n| Expresso | 0.40 |",
					"comment": "another comment"
				},
				{
					"id": "KS3",					
					"keyword": "THEN",
					"action_word": "It should serve a coffee",
					"doc_string": "a new docstring",
					"comment": "a new comment"
				},
				{
					"id": "KS4",					
					"keyword": "AND",
					"action_word": "It should give me the change"
				}
			],
			"parent_type": "TEST_CASE_LIBRARY"
		},
		{
			"id": "TCD",
			"test_case_kind": "EXPLORATORY",
			"name": "TC D",
			"description": "desc D",
			"reference": "refD",
			"importance": "LOW",
			"status": "OBSOLETE",
			"nature": "NON_FUNCTIONAL_TESTING",
			"type": "EVOLUTION_TESTING",
			"custom_fields": [],
			"verified_requirements": ["RA", "RB"],
			"charter": "<p>this is a charter</p>",
			"session_duration": 30,
			"parent_type": "TEST_CASE_LIBRARY"
		}
	]
}
```

### Possible values for specifics fields

#### "importance" field

Possible values (must be written in uppercase):

- "LOW"
- "MEDIUM"
- "HIGH"
- "VERY_HIGH"

#### "status" field

Possible values (must be written in uppercase):

- "WORK_IN_PROGRESS"
- "UNDER_REVIEW"
- "APPROVED"
- "OBSOLETE"
- "TO_BE_UPDATED"

#### "nature" field

Possible values (must be written in uppercase):

- "UNDEFINED"
- "FUNCTIONAL_TESTING"
- "BUSINESS_TESTING"
- "USER_TESTING"
- "NON_FUNCTIONAL_TESTING"
- "PERFORMANCE_TESTING"
- "SECURITY_TESTING"
- "ATDD"

#### "type" field

Possible values (must be written in uppercase):

- "UNDEFINED"
- "COMPLIANCE_TESTING"
- "CORRECTION_TESTING"
- "EVOLUTION_TESTING"
- "REGRESSION_TESTING"
- "END_TO_END_TESTING"
- "PARTNER_TESTING"

#### "parent_type" field

A test case has two possible parent types (must be written in uppercase):

- "TEST_CASE_LIBRARY"
- "TEST_CASE_FOLDER"

### Optional json fields for a test case

Here are the fields that are not mandatory to write in the json to describe a test case:

- "description"
- "reference"
- "importance": If not specified, the following default value will be applied: "LOW"
- "status": If not specified, the following default value will be applied: "WORK_IN_PROGRESS"
- "nature": If not specified, the following default value will be applied: "UNDEFINED"
- "type": If not specified, the following default value will be applied: "UNDEFINED"
- "custom_fields"
- "dataset_param"
- "datasets"
- "verified_requirements"
- "prerequisite"
- "attachments"

### Specific json fields for a gherkin, keyword and exploratory test cases

- "script": This field is only used by gherkin test cases
- "keyword_steps": This field is only used by keyword test cases
- "session_duration": This optional field is only used by exploratory test cases


## Campaign folder

Json example:

```json
{
    "campaign_folders": [
        {
            "id": "FC1",
            "name": "Campaign folder 1",
            "description": "desc 1",
            "custom_fields": [],
            "parent_type": "CAMPAIGN_LIBRARY"
        },
        {
            "id": "FC2",
            "name": "Campaign folder 2",
            "description": "desc 2",
            "custom_fields": [],
            "parent_type": "CAMPAIGN_LIBRARY"
        },
        {
            "id": "FC21",
            "name": "Campaign folder 2.1",
            "description": "desc 2.1",
            "custom_fields": [],
            "parent_type": "CAMPAIGN_FOLDER",
            "parent_id": "FC2"
        }
    ]
}
```

### Possible values for specifics fields

#### "parent_type" field

A campaign folder has two possible parent types (must be written in uppercase):

- CAMPAIGN_LIBRARY
- CAMPAIGN_FOLDER


### Optional json fields for a test case folder

Here are the fields that are not mandatory to write in the json to describe a campaign folder:

- "description"
- "custom_fields"
- "attachments"

## Campaign

Json example:

````json
{
    "campaigns": [
        {
            "id": "CP1",
            "name": "Campaign 1",
            "description": "desc CP1",
            "reference": "ref CP1",
            "status": "UNDEFINED",
            "custom_fields": [],
            "scheduled_start_date": "2024-05-11",
            "scheduled_end_date": "2024-05-17",
            "actual_start_date": "2024-05-12",
            "actual_end_date": "2024-05-18",
            "test_plan_test_case_ids": ["TCA", "TCB", "TCD"],
            "parent_type": "CAMPAIGN_LIBRARY"
        },
        {
            "id": "CP2",
            "name": "Campaign 2",
            "description": "desc CP2",
            "reference": "ref CP2",
            "status": "PLANNED",
            "custom_fields": [],
            "scheduled_start_date": "2024-05-11",
            "scheduled_end_date": "2024-05-17",
            "actual_start_date_auto": true,
            "actual_end_date_auto": true,
            "test_plan_test_case_ids": [],
            "parent_type": "CAMPAIGN_LIBRARY"
        },
        {
            "id": "CP3",
            "name": "Campagne 3",
            "description": "desc CP3",
            "reference": "ref CP3",
            "status": "IN_PROGRESS",
            "parent_type": "CAMPAIGN_LIBRARY"
        },
        {
            "id": "CP4",
            "name": "Campaign 4",
            "description": "desc CP4",
            "reference": "ref CP4",
            "status": "FINISHED",
            "parent_type": "CAMPAIGN_FOLDER",
            "parent_id": "FC1"
        }
    ]
}
````

### Possible values for specifics fields

#### "status" field

Possible values (must be written in uppercase):

- "UNDEFINED"
- "PLANNED"
- "IN_PROGRESS"
- "FINISHED"
- "ARCHIVED"

#### "parent_type" field

A campaign has two possible parent types (must be written in uppercase):

- "CAMPAIGN_LIBRARY"
- "CAMPAIGN_FOLDER"

### Optional json fields for a campaign

Here are the fields that are not mandatory to write in the json to describe a campaign:

- "description"
- "reference"
- "status": If not specified, the following default value will be applied: "UNDEFINED"
- "custom_fields"
- "scheduled_start_date"
- "scheduled_end_date"
- "actual_start_date": If specified, do not add "actual_start_date_auto" in the json, otherwise this value will not be taken into account
- "actual_end_date": If specified, do not add "actual_end_date_auto" in the json, otherwise this value will not be taken into account
- "actual_start_date_auto": If not specified, the following default value will be applied: false
- "actual_end_date_auto": If not specified, the following default value will be applied: false
- "test_plan_test_case_ids"
- "attachments"

## Iteration

Json example:

````json
{
    "iterations": [
        {
            "id": "IT1",
            "name": "Iteration 1",
            "description": "desc IT1",
            "reference": "ref IT1",
            "status": "UNDEFINED",
            "custom_fields": [],
            "scheduled_start_date": "2024-05-11",
            "scheduled_end_date": "2024-05-17",
            "actual_start_date": "2024-05-12",
            "actual_end_date": "2024-05-18",
            "test_plan_test_case_ids": ["TCA", "TCB", "TCC", "TCD"],
            "parent_id": "CP1"
        },
        {
            "id": "IT2",
            "name": "Iteration 2",
            "description": "desc IT2",
            "reference": "ref IT2",
            "status": "PLANNED",
            "custom_fields": [],
            "scheduled_start_date": "2024-04-11",
            "scheduled_end_date": "2024-04-17",
            "actual_start_date_auto": true,
            "actual_end_date_auto": true,
            "test_plan_test_case_ids": ["TCA", "TCB", "TCD"],
            "parent_id": "CP1"
        },
        {
            "id": "IT3",
            "name": "Iteration 3",
            "description": "desc IT3",
            "reference": "ref IT3",
            "status": "FINISHED",
            "parent_id": "CP1"
        }
    ]
}
````

### Possible values for specifics fields

#### "status" field

Possible values are the same as for campaign status (must be written in uppercase):

- "UNDEFINED"
- "PLANNED"
- "IN_PROGRESS"
- "FINISHED"
- "ARCHIVED"

### Optional json fields for an iteration

Here are the fields that are not mandatory to write in the json to describe an iteration:

- "description"
- "reference"
- "status": If not specified, the following default value will be applied: "UNDEFINED"
- "custom_fields"
- "scheduled_start_date"
- "scheduled_end_date"
- "actual_start_date": If specified, do not add "actual_start_date_auto" in the json, otherwise this value will not be taken into account
- "actual_end_date": If specified, do not add "actual_end_date_auto" in the json, otherwise this value will not be taken into account
- "actual_start_date_auto": If not specified, the following default value will be applied: false
- "actual_end_date_auto": If not specified, the following default value will be applied: false
- "test_plan_test_case_ids"
- "attachments"

## Test suite

Json example:

````json
{
    "test_suites": [
        {
            "id": "TS1",
            "name": "Test Suite 1",
            "description": "desc TS1",
            "status": "READY",
            "custom_fields": [],
            "test_plan_test_case_ids": ["TCA", "TCB", "TCD"],
            "parent_id": "IT1"
        },
        {
            "id": "TS2",
            "name": "Test Suite 2",
            "description": "desc TS2",
            "status": "READY",
            "parent_id": "IT1"
        },
        {
            "id": "TS3",
            "name": "Test Suite 3",
            "description": "desc TS3",
            "status": "READY",
            "parent_id": "IT1"
        },
        {
            "id": "TS4",
            "name": "Test Suite 4",
            "description": "desc TS4",
            "status": "READY",
            "test_plan_test_case_ids": ["TCA", "TCB", "TCD"],
            "parent_id": "IT2"
        }
    ]
}
````

### Possible values for specifics fields

#### "status" field

Possible values (must be written in uppercase):

- "READY"
- "RUNNING"
- "SUCCESS"
- "SETTLED"
- "SKIPPED"
- "CANCELLED"
- "FAILURE"
- "BLOCKED"
- "UNTESTABLE"

### Optional json fields for a test suite

Here are the fields that are not mandatory to write in the json to describe a test suite:

- "description"
- "status": If not specified, the following default value will be applied: "READY"
- "custom_fields"
- "test_plan_test_case_ids"
- "attachments"

## Execution

Json example:

````json
{
	"executions": [
		{
			"id": "EX1",
			"comment": "a comment",
			"status": "SUCCESS",
			"custom_fields": [
				{
					"id": "cuf1",
					"value": "true"
				},
				{
					"id": "cuf2",
					"value": "cuf value 1"
				}
			],
			"execution_steps": [
				{
					"test_step_id": "AS1",
					"status": "SUCCESS",
					"comment": "a step comment 1",
					"custom_fields": [
						{
							"id": "cuf1",
							"value": "true"
						},
						{
							"id": "cuf2",
							"value": "cuf value 1"
						}
					]
				},
				{
					"test_step_id": "AS2",
					"status": "SUCCESS",
					"comment": "a step comment 2",
					"custom_fields": []
				}
			],
			"test_case_id": "TCA",
			"dataset_id": "DS1",
			"parent_id": "IT1",
			"parent_type": "ITERATION"
		},
		{
			"id": "EX2",
			"comment": "another comment",
			"status": "FAILURE",
			"custom_fields": [],
			"execution_steps": [
				{
					"test_step_id": "AS1",
					"status": "SUCCESS",
					"comment": "a step comment 1",
					"custom_fields": [
						{
							"id": "cuf1",
							"value": "true"
						},
						{
							"id": "cuf2",
							"value": "cuf value 1"
						}
					]
				},
				{
					"test_step_id": "AS2",
					"status": "SUCCESS"
				}
			],
			"test_case_id": "TCA",
			"dataset_id": "DS1",
            "parent_id": "TS4",
            "parent_type": "TEST_SUITE"
		}
	]
}
```` 

### Possible values for specifics fields

#### "status" field

Possible values for execution or execution step (must be written in uppercase):

- "READY"
- "RUNNING"
- "SUCCESS"
- "SETTLED"
- "SKIPPED"
- "CANCELLED"
- "FAILURE"
- "BLOCKED"
- "UNTESTABLE"

### Optional json fields for execution or execution steps

Here are the fields that are not mandatory to write in the json to describe a test suite:

- "comment"
- "status": If not specified, the following default value will be applied: "READY"
- "custom_fields"
- "attachments"

#### "parent_type" field

An execution has two possible parent types (must be written in uppercase):

- "ITERATION"
- "TEST SUITE"

## How to add attachments to Squash entities with pivot importer

As stated in introduction, inside the zip file you can add an "attachments" folder.

Inside this folder you need to add sub-folders for each kind of entity you want to add attachments to.

Here are how the sub-folders must be named:

- campaign_folders
- campaigns
- executions
- iterations
- requirement_folders
- requirements
- test_case_folders
- test_cases
- test_suites

Inside those folders, you put the attachments you need to import to Squash entities.

For each entity that can have attachments in Squash, in the json file, you can add an "attachments" field on the imported entity.

For example, if you want to import a test case that has attachments and which also has attachments on its steps, the test_cases.json file will look like that:

```json
{
	"test_cases": [
        {
            "id": "TCA",
            "test_case_kind": "STANDARD",
            "name": "TC Standard",
            "description": "desc A",
            "reference": "refA",
            "importance": "VERY_HIGH",
            "status": "WORK_IN_PROGRESS",
            "nature": "FUNCTIONAL_TESTING",
            "type": "UNDEFINED",
            "prerequisite": "prerequisite 1",
            "action_steps": [
                {
                    "id": "AS1",
                    "action": "One fist action",
                    "expected_result": "The first expected result",
                    "custom_fields": [
                        {
                            "id": "cuf1",
                            "value": "true"
                        },
                        {
                            "id": "cuf2",
                            "value": "my cuf value"
                        }
                    ],
                    "verified_requirements": ["RC", "RD"],
                    "attachments": [
                        {
                            "zip_import_file_name": "AT255-rules.txt",
                            "original_file_name":  "rules.txt"
                        }
                    ]
                },
                {
                    "id": "AS2",
                    "action": "One second action",
                    "expected_result": "The second expected result",
                    "custom_fields": [],
                    "verified_requirements": []
                }
            ],
            "parent_type": "TEST_CASE_LIBRARY",
            "attachments": [
                {
                    "zip_import_file_name": "AT256-chart.jpg",
                    "original_file_name":  "chart.jpg"
                },
                {
                    "zip_import_file_name": "AT257-report.pdf",
                    "original_file_name": "report.pdf"
                }
            ]
        }
	]
}
```

To describe an attachment in the json, you need to fill in two fields:

- "zip_import_file_name": The name of the attachment that is stored inside the entity folder of the zip file. In our example, there should be an attachment called "AT256-chart.jpg" inside the "test_cases" sub-folder.
- "original_file_name": This is the name of the attachment as it will appear in Squash after import





