# Experimental Feature Flags

## Introduction

This document describes how are implemented the experimental feature flags in the project.

Experimental feature flags are a way to enable or disable features that are still in development or are not yet ready for production use.
This allows developers to work on new features without making them available to users.

## How to enable/disable an experimental feature flag

### In the backend

To enable an experimental feature flag, you need to add the flag to the `squashtm.feature.experimental` property in a property file.

For example, to enable the `new-feature` flag, you need to add the following line to the `application.properties` file:

```properties
squashtm.feature.experimental.new-feature=true
```

The flag will be enabled after restarting the application.

> Note: in the property files, the flags are case-insensitive and both hyphens and undersocres can be used to separate words. For example, the following lines are equivalent:
> - `squashtm.feature.experimental.new-feature=true`
> - `squashtm.feature.experimental.new_feature=true`
> - `squashtm.feature.experimental.NEW_FEATURE=yes` (using Spring's relaxed binding for booleans)
> 
> Try to stay consistent with the naming convention to avoid confusion. The canonical format for Spring properties is `squashtm.feature.experimental.new-feature=true`,
> so we recommend using this format when adding new flags. Unlike with Spring's relaxed binding rules, camel-case and Pascal-case are not supported.
>  
> See the [Spring documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config-relaxed-binding) for more information.

### In the frontend's development mode

When accessing the frontend in production mode (i.e., when the frontend is served by the backend on port `8080`), the frontend will know about the backend flags and will enable/disable the features accordingly.

When starting the frontend in development mode (i.e. with Angular CLI on port `4200`), the backend flags are not used (see section "How frontend knows about the backend flags" below).
Instead, you should add the flag to the `experimentalFeatureFlags` array in the `tm/tm-front/projects/sqtm-app/src/environments/experimental-feature-flags.ts` file.

## How to define a new experimental feature flag

To define a new experimental feature flag, you need to follow these steps.

### Add the flag to the `ExperimentalFeature` backend enum

Add a value to the enum in the  `core/core.foundation/src/main/java/org/squashtest/tm/core/foundation/feature/ExperimentalFeature.java` file. e.g.:

```java
public enum ExperimentalFeature {
    NEW_FEATURE
}
```

### Add the flag to the frontend array

Add the flag to the `AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS` array in the `tm/tm-front/projects/sqtm-core/src/lib/core/services/experimental-feature/experimental-feature.constants.ts` file. e.g.:

```typescript
export const AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS = ['NEW_FEATURE'] as const;
```

> Node: The `as const` is used for TypeScript to infer the type of the array as a tuple of string literals.
> This way, the type of the array is `['NEW_FEATURE']` instead of `string[]`.
> This is useful to prevent typos when adding or removing flags.

### Enable the flag in backend dev properties

Add a property in the `application-dev.properties` file. e.g.:

```properties
squashtm.feature.experimental.new-feature=true
```

### Enable the flag in frontend dev environment

Add the flag to the `experimentalFeatureFlags` array in the `tm/tm-front/projects/sqtm-app/src/environments/experimental-feature-flags.ts` file. e.g.:

```typescript
export const experimentalFeatureFlags = ['NEW_FEATURE'];
```

## How frontend knows about the backend flags

When the frontend is served by the backend, the backend will store the list of enabled experimental feature flags with Thymeleaf in the `tm/tm-front/projects/sqtm-app/src/index.html` file.

This is done by adding a readonly property to the `window` object:

```html
const flags = /*[[${#strings.arraySplit(@environment.getProperty('squashtm.feature.experimental'), ',')}]]*/ [];
Object.defineProperty(window, 'sqtmExperimentalFeatureFlags', {
  value: flags,
  writable: false,
  enumerable: true,
  configurable: false,
});
```

This global variable is then read by `ExperimentalFeatureService` when the frontend is started in production mode.

This approach allows the frontend to know about the backend flags without having to make an additional request to the backend.

## Backend guards

To prevent the use of an experimental feature flag in production, you can add a guard in the backend code. For example:

```java
@ExperimentalFeatureEnabled(ExperimentalFeature.NEW_FEATURE)
public void newFeature() {
    // Your code here
}
```

Calling this method will throw an `FeatureFlagNotEnabledException` if the `NEW_FEATURE` flag is not enabled.

## Frontend guards

### In typescript code

To block the use of an experimental feature flag in the frontend, you can add a guard in the frontend code. For example:

```typescript
import { ExperimentalFeatureService } from 'sqtm-core';

class FooComponent {
    constructor(private experimentalFeatureService: ExperimentalFeatureService) {}

    featureSpecificMethod() {
        this.experimentalFeatureService.check('NEW_FEATURE');
        // Your code here
    }
}
```

Calling the `check` method will throw an `FeatureFlagNotEnabledException` if the `NEW_FEATURE` flag is not enabled.

### In HTML templates

To conditionally show elements in the UI based on flags, you can use the `[sqtmCoreExperimentalFeature]` directive. For example:

```html
<div [sqtmCoreExperimentalFeature]="'NEW_FEATURE'">
    <!-- Content here -->
</div>
```

The `div` element will only be displayed if the `NEW_FEATURE` flag is enabled.

## Naming conventions

In general, the naming conventions for experimental feature flags are as follows:

- In property files, use lowercase letters and separate words with hyphens. e.g. `new-feature`
- In the backend enum, use uppercase letters and separate words with underscores. e.g. `NEW_FEATURE`
- In frontend code, use uppercase letters and separate words with underscores. e.g. `NEW_FEATURE`

## Add experimental flags support in a plugin

### Backend

Experimental Feature Flags are supported in the backend by default as long as you have the `tm.service` module as a dependency.

### Frontend in prod builds

If your plugin has a frontend, you'll need to update the index controller to include the experimental feature flags in the `index.html` file. You can do this by adding the following code to the controller:

```java
@GetMapping("/plugin/{PLUGIN_NAME}/index")
public String index(Model model) {
    final List<String> flags = experimentalFeatureManager.getEnabledFeatures().stream().map(Enum::name).toList();
    model.addAttribute("experimentalFeatureFlags", flags);
    return "index";
}
```

You'll also need to update the `index.html` file to include the flags in the `window` object:

```html
<!doctype html>
<html
  lang="en"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:th="http://www.thymeleaf.org"
>
<head>
  <!-- ... other head elements -->
  <script th:inline="javascript">
    /*<![CDATA[*/
    (function () {
      const flags = /*[[${experimentalFeatureFlags}]]*/ [];
      // Set as readonly
      Object.defineProperty(window, 'sqtmExperimentalFeatureFlags', {
        value: flags,
        writable: false,
        enumerable: true,
        configurable: false,
      });
    })();
    /*]]>*/
  </script>
</head>
<body>
<app-root></app-root>
</body>
</html>
```

You can then use the `ExperimentalFeatureService` in your frontend code to check if a flag is enabled.

### Frontend in dev builds

There are a few extra steps for enabling experimental feature flags in the frontend in development mode:

Add the `experimental-feature-flags.ts` next to your environment files (you can just copy the one from the `sqtm-app` project) and update the environment files:

In `environment.ts`:

```typescript
import {experimentalFeatureFlags} from "./experimental-feature-flags";

export const environment = {
  production: false,
  sqtmExperimentalFeatureFlags: experimentalFeatureFlags,
};
```

In `environment.prod.ts`:

```typescript
import {experimentalFeatureFlags} from "./experimental-feature-flags";

export const environment = {
  production: false,
  sqtmExperimentalFeatureFlags: null,
};
```

Then update the AppModule to provide the values needed by the `ExperimentalFeatureService`:

```typescript
import { provideExperimentalFeatureFlags } from 'sqtm-core';
import { environment } from '../environments/environment';

// ...

@NgModule({
  // ...
  providers: [
    // ...
    provideExperimentalFeatureFlags(environment.sqtmExperimentalFeatureFlags)
  ], 
  // ...
})
export class AppModule {}
```
